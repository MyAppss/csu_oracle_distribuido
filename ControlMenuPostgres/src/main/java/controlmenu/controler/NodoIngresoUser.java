/*    */ package controlmenu.controler;
/*    */ 
/*    */ import java.util.Date;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class NodoIngresoUser
/*    */ {
/* 12 */   private int count = 0;
/*    */   private Date date;
/*    */   
/*    */   public String getUser() {
/* 16 */     return this.user;
/*    */   }
/*    */   private String user;
/*    */   public void setUser(String user) {
/* 20 */     this.user = user;
/*    */   }
/*    */   
/*    */   public Date getDate() {
/* 24 */     return this.date;
/*    */   }
/*    */   
/*    */   public void setDate(Date date) {
/* 28 */     this.date = date;
/*    */   }
/*    */   
/*    */   public void increment() {
/* 32 */     this.count++;
/*    */   }
/*    */   
/*    */   public int getCount() {
/* 36 */     return this.count;
/*    */   }
/*    */   
/*    */   public void setCount(int count) {
/* 40 */     this.count = count;
/*    */   }
/*    */   
/*    */   public long getTime() {
/* 44 */     return this.date.getTime();
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\controler\NodoIngresoUser.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */