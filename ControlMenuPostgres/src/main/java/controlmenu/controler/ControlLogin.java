package controlmenu.controler;

import java.util.List;

public interface ControlLogin {
  public static final int MENU_ITEM = 3;
  
  public static final int MENU_MENU = 1;
  
  public static final int MENU_RAIZ = 2;
  
  void addBitacoraLogin(String paramString1, String paramString2, int paramInt);
  
  String controlError(String paramString);
  
  int existUser(String paramString1, String paramString2);
  
  int getIdRole(String paramString, List paramList);
  
  List getListaGrupo();
  
  List getListaMenu(int paramInt);
  
  void salirBitacora(String paramString1, String paramString2);
  
  String validateIngreso(String paramString1, String paramString2);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\controler\ControlLogin.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */