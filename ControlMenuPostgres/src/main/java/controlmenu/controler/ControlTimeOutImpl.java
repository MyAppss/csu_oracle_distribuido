/*     */ package controlmenu.controler;
/*     */ 
/*     */ import controlmenu.bussines.BitacoraBL;
/*     */ import controlmenu.model.Bitacora;
/*     */ import controlmenu.util.GuiceInjectorSingletonCM;
/*     */ import controlmenu.util.ParametersCM;
/*     */ import java.sql.Timestamp;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ControlTimeOutImpl
/*     */   implements ControlTimeOut
/*     */ {
/*     */   private static int indexPath;
/*     */   private static BitacoraBL logBL;
/*  21 */   private static Map<String, NodoClient> mapClient = new HashMap<>();
/*  22 */   private static Map<String, NodoIngresoUser> mapIngresClient = new HashMap<>(); static {
/*  23 */     logBL = (BitacoraBL)GuiceInjectorSingletonCM.getInstance(BitacoraBL.class);
/*  24 */     timeOut = ParametersCM.MINUTOS_FUERA;
/*  25 */     indexPath = 0;
/*     */   }
/*     */   private static int timeOut;
/*     */   
/*     */   public void addRutaBase(String path) {
/*  30 */     indexPath = path.lastIndexOf("/") + 1;
/*     */   }
/*     */ 
/*     */   
/*     */   public void addNodoClient(NodoClient nd) {
/*  35 */     mapClient.put(nd.getUser(), nd);
/*     */   }
/*     */ 
/*     */   
/*     */   public NodoClient getNodoClient(String user) {
/*  40 */     return mapClient.get(user);
/*     */   }
/*     */ 
/*     */   
/*     */   public void createNewClient(String user, String addressIp) {
/*  45 */     NodoClient nd = new NodoClient();
/*  46 */     nd.setUser(user);
/*  47 */     nd.setAddressIp(addressIp);
/*  48 */     nd.setTime((new Date()).getTime());
/*  49 */     mapClient.put(user, nd);
/*     */   }
/*     */ 
/*     */   
/*     */   public void setDatos(String user, long d) {
/*  54 */     ((NodoClient)mapClient.get(user)).setTime(d);
/*     */   }
/*     */   
/*     */   public void remove(String user) {
/*  58 */     mapClient.remove(user);
/*     */   }
/*     */ 
/*     */   
/*     */   public String getAddressIP(String user) {
/*  63 */     return ((NodoClient)mapClient.get(user)).getAddressIp();
/*     */   }
/*     */ 
/*     */   
/*     */   public void registerOutTime(long timeMax) {
/*  68 */     long tp = (new Date()).getTime();
/*  69 */     for (String key : mapClient.keySet()) {
/*     */       try {
/*  71 */         long value = ((NodoClient)mapClient.get(key)).getTime();
/*  72 */         if (tp - value > timeMax) {
/*  73 */           Bitacora logg = new Bitacora();
/*  74 */           logg.setForm("");
/*  75 */           logg.setAction("Salio del Sistema por expiracion de Tiempo");
/*  76 */           logg.setUser(key);
/*  77 */           logg.setStartDate(new Timestamp(value + timeMax));
/*  78 */           logg.setAddress(((NodoClient)mapClient.get(key)).getAddressIp());
/*  79 */           logBL.insertLogWebTimeOut(logg);
/*  80 */           mapClient.remove(key);
/*     */         } 
/*  82 */       } catch (Exception e) {
/*     */         return;
/*     */       } 
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void clearUserCliente() {
/*  90 */     long timeMax = (60000 * timeOut);
/*  91 */     long tp = (new Date()).getTime();
/*  92 */     for (String key : mapIngresClient.keySet()) {
/*     */       try {
/*  94 */         if (tp - ((NodoIngresoUser)mapIngresClient.get(key)).getTime() > timeMax) {
/*  95 */           mapIngresClient.remove(key);
/*     */         }
/*  97 */       } catch (Exception e) {
/*     */         return;
/*     */       } 
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public boolean exisUserIngreso(String user) {
/* 105 */     return mapIngresClient.containsKey(user);
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertUserIngreso(String user) {
/* 110 */     NodoIngresoUser nd = new NodoIngresoUser();
/* 111 */     nd.setDate(new Date());
/* 112 */     nd.setCount(1);
/* 113 */     nd.setUser(user);
/* 114 */     mapIngresClient.put(user, nd);
/*     */   }
/*     */ 
/*     */   
/*     */   public void deleteUserIngreso(String user) {
/* 119 */     mapIngresClient.remove(user);
/*     */   }
/*     */ 
/*     */   
/*     */   public int countIntentoUserIngreso(String user) {
/* 124 */     return ((NodoIngresoUser)mapIngresClient.get(user)).getCount();
/*     */   }
/*     */ 
/*     */   
/*     */   public void ingrementIntentoUser(String user) {
/* 129 */     NodoIngresoUser nd = mapIngresClient.get(user);
/* 130 */     nd.setDate(new Date());
/* 131 */     nd.increment();
/*     */   }
/*     */ 
/*     */   
/*     */   public NodoIngresoUser getNodoIngresoUser(String user) {
/* 136 */     return mapIngresClient.get(user);
/*     */   }
/*     */ 
/*     */   
/*     */   public boolean isPaginaUsuario(String user, String path) {
/* 141 */     return getNodoClient(user).existeUrl(path.substring(indexPath));
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\controler\ControlTimeOutImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */