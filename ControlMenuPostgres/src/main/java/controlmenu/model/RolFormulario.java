/*    */ package controlmenu.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class RolFormulario
/*    */   implements Serializable
/*    */ {
/*    */   private Integer depende;
/*    */   private String estado;
/*    */   private Integer formularioId;
/*    */   private String name;
/*    */   private String nivel;
/*    */   private Integer rolId;
/*    */   private String url;
/*    */   
/*    */   public RolFormulario() {}
/*    */   
/*    */   public RolFormulario(Integer depende, String estado, Integer formularioId, String nivel, Integer rolId, String url) {
/* 19 */     this.depende = depende;
/* 20 */     this.estado = estado;
/* 21 */     this.formularioId = formularioId;
/* 22 */     this.nivel = nivel;
/* 23 */     this.rolId = rolId;
/* 24 */     this.url = url;
/*    */   }
/*    */   
/*    */   public Integer getRolId() {
/* 28 */     return this.rolId;
/*    */   }
/*    */   
/*    */   public void setRolId(Integer rolId) {
/* 32 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public Integer getFormularioId() {
/* 36 */     return this.formularioId;
/*    */   }
/*    */   
/*    */   public void setFormularioId(Integer formularioId) {
/* 40 */     this.formularioId = formularioId;
/*    */   }
/*    */   
/*    */   public String getEstado() {
/* 44 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(String estado) {
/* 48 */     this.estado = estado;
/*    */   }
/*    */   
/*    */   public String getUrl() {
/* 52 */     return this.url;
/*    */   }
/*    */   
/*    */   public void setUrl(String url) {
/* 56 */     this.url = url;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 60 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 64 */     this.name = name;
/*    */   }
/*    */   
/*    */   public String getNivel() {
/* 68 */     return this.nivel;
/*    */   }
/*    */   
/*    */   public void setNivel(String nivel) {
/* 72 */     this.nivel = nivel;
/*    */   }
/*    */   
/*    */   public Integer getDepende() {
/* 76 */     return this.depende;
/*    */   }
/*    */   
/*    */   public void setDepende(Integer depende) {
/* 80 */     this.depende = depende;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\RolFormulario.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */