/*    */ package controlmenu.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class Rol
/*    */   implements Serializable
/*    */ {
/*    */   private String descripcion;
/*    */   private String estado;
/*    */   private String nombre;
/*    */   private Integer rolId;
/*    */   
/*    */   public Rol() {}
/*    */   
/*    */   public Rol(String descripcion, String estado, String nombre, Integer rolId) {
/* 16 */     this.descripcion = descripcion;
/* 17 */     this.estado = estado;
/* 18 */     this.nombre = nombre;
/* 19 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public Integer getRolId() {
/* 23 */     return this.rolId;
/*    */   }
/*    */   
/*    */   public void setRolId(Integer rolId) {
/* 27 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public String getDescripcion() {
/* 31 */     return this.descripcion;
/*    */   }
/*    */   
/*    */   public void setDescripcion(String descripcion) {
/* 35 */     this.descripcion = descripcion;
/*    */   }
/*    */   
/*    */   public String getEstado() {
/* 39 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(String estado) {
/* 43 */     this.estado = estado;
/*    */   }
/*    */   
/*    */   public String getNombre() {
/* 47 */     return this.nombre;
/*    */   }
/*    */   
/*    */   public void setNombre(String nombre) {
/* 51 */     this.nombre = nombre;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\Rol.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */