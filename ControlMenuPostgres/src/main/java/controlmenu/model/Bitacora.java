/*    */ package controlmenu.model;
/*    */ 
/*    */ import java.sql.Timestamp;
/*    */ 
/*    */ 
/*    */ public class Bitacora
/*    */ {
/*    */   private String action;
/*    */   private String address;
/*    */   private String form;
/*    */   private Timestamp startDate;
/*    */   private String user;
/*    */   
/*    */   public Bitacora() {}
/*    */   
/*    */   public Bitacora(String action, String address, String form, Timestamp startDate, String user) {
/* 17 */     this.action = action;
/* 18 */     this.address = address;
/* 19 */     this.form = form;
/* 20 */     this.startDate = startDate;
/* 21 */     this.user = user;
/*    */   }
/*    */   
/*    */   public Timestamp getStartDate() {
/* 25 */     return this.startDate;
/*    */   }
/*    */   
/*    */   public void setStartDate(Timestamp startDate) {
/* 29 */     this.startDate = startDate;
/*    */   }
/*    */   
/*    */   public String getUser() {
/* 33 */     return this.user;
/*    */   }
/*    */   
/*    */   public void setUser(String user) {
/* 37 */     this.user = user;
/*    */   }
/*    */   
/*    */   public String getForm() {
/* 41 */     return this.form;
/*    */   }
/*    */   
/*    */   public void setForm(String form) {
/* 45 */     this.form = form;
/*    */   }
/*    */   
/*    */   public String getAction() {
/* 49 */     return this.action;
/*    */   }
/*    */   
/*    */   public void setAction(String action) {
/* 53 */     this.action = action;
/*    */   }
/*    */   
/*    */   public String getAddress() {
/* 57 */     return this.address;
/*    */   }
/*    */   
/*    */   public void setAddress(String address) {
/* 61 */     this.address = address;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\Bitacora.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */