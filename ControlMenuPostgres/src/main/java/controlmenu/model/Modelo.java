/*    */ package controlmenu.model;
/*    */ 
/*    */ import java.sql.Time;
/*    */ import java.sql.Timestamp;
/*    */ import java.util.Date;
/*    */ 
/*    */ public class Modelo
/*    */ {
/*    */   private Integer cantidad;
/*    */   private Boolean estado;
/*    */   private Date fecha;
/*    */   private Timestamp fechaHora;
/*    */   private Time hora;
/*    */   private Integer modeloId;
/*    */   private String nombre;
/*    */   private Integer porciento;
/*    */   
/*    */   public Integer getModeloId() {
/* 19 */     return this.modeloId;
/*    */   }
/*    */   
/*    */   public void setModeloId(Integer dato) {
/* 23 */     this.modeloId = dato;
/*    */   }
/*    */   
/*    */   public String getNombre() {
/* 27 */     return this.nombre;
/*    */   }
/*    */   
/*    */   public void setNombre(String dato) {
/* 31 */     this.nombre = dato;
/*    */   }
/*    */   
/*    */   public Timestamp getFechaHora() {
/* 35 */     return this.fechaHora;
/*    */   }
/*    */   
/*    */   public void setFechaHora(Timestamp dato) {
/* 39 */     this.fechaHora = dato;
/*    */   }
/*    */   
/*    */   public Date getFecha() {
/* 43 */     return this.fecha;
/*    */   }
/*    */   
/*    */   public void setFecha(Date dato) {
/* 47 */     this.fecha = dato;
/*    */   }
/*    */   
/*    */   public Time getHora() {
/* 51 */     return this.hora;
/*    */   }
/*    */   
/*    */   public void setHora(Time dato) {
/* 55 */     this.hora = dato;
/*    */   }
/*    */   
/*    */   public Integer getCantidad() {
/* 59 */     return this.cantidad;
/*    */   }
/*    */   
/*    */   public void setCantidad(Integer dato) {
/* 63 */     this.cantidad = dato;
/*    */   }
/*    */   
/*    */   public Integer getPorciento() {
/* 67 */     return this.porciento;
/*    */   }
/*    */   
/*    */   public void setPorciento(Integer dato) {
/* 71 */     this.porciento = dato;
/*    */   }
/*    */   
/*    */   public Boolean getEstado() {
/* 75 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(Boolean dato) {
/* 79 */     this.estado = dato;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\Modelo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */