/*    */ package controlmenu.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class Usuario implements Serializable {
/*    */   private String estado;
/*    */   private String login;
/*    */   private String nameRole;
/*    */   private String nombre;
/*    */   private Integer rolId;
/*    */   private Integer usuarioId;
/*    */   
/*    */   public Integer getUsuarioId() {
/* 14 */     return this.usuarioId;
/*    */   }
/*    */   
/*    */   public void setUsuarioId(Integer usuarioId) {
/* 18 */     this.usuarioId = usuarioId;
/*    */   }
/*    */   
/*    */   public String getEstado() {
/* 22 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(String estado) {
/* 26 */     this.estado = estado;
/*    */   }
/*    */   
/*    */   public String getLogin() {
/* 30 */     return this.login;
/*    */   }
/*    */   
/*    */   public void setLogin(String login) {
/* 34 */     this.login = login;
/*    */   }
/*    */   
/*    */   public String getNombre() {
/* 38 */     return this.nombre;
/*    */   }
/*    */   
/*    */   public void setNombre(String nombre) {
/* 42 */     this.nombre = nombre;
/*    */   }
/*    */   
/*    */   public Integer getRolId() {
/* 46 */     return this.rolId;
/*    */   }
/*    */   
/*    */   public void setRolId(Integer rolId) {
/* 50 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public String getNameRole() {
/* 54 */     return this.nameRole;
/*    */   }
/*    */   
/*    */   public void setNameRole(String nameRole) {
/* 58 */     this.nameRole = nameRole;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\Usuario.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */