/*    */ package controlmenu.util;
/*    */ 
/*    */ import com.google.inject.Guice;
/*    */ import com.google.inject.Injector;
/*    */ import com.google.inject.Module;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class GuiceInjectorSingletonCM<T>
/*    */ {
/*    */   private static Injector injector;
/*    */   
/*    */   public static Injector getInjector() {
/* 15 */     if (injector == null) {
/* 16 */       injector = Guice.createInjector(new Module[] { (Module)new GuiceMainModule() });
/*    */     }
/* 18 */     return injector;
/*    */   }
/*    */   
/*    */   public static <T> T getInstance(Class<T> t) {
/* 22 */     return (T)getInjector().getInstance(t);
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmen\\util\GuiceInjectorSingletonCM.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */