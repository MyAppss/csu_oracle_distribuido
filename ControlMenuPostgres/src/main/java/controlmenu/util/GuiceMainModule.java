/*    */ package controlmenu.util;
/*    */ 
/*    */ import com.google.inject.AbstractModule;
/*    */ import controlmenu.bussines.BitacoraBL;
/*    */ import controlmenu.bussines.BitacoraBLImpl;
/*    */ import controlmenu.bussines.GroupBL;
/*    */ import controlmenu.bussines.GroupBLImpl;
/*    */ import controlmenu.bussines.RoleBL;
/*    */ import controlmenu.bussines.RoleBLImpl;
/*    */ import controlmenu.bussines.UserBL;
/*    */ import controlmenu.bussines.UserBLImpl;
/*    */ import controlmenu.controler.ControlLogin;
/*    */ import controlmenu.controler.ControlLoginImpl;
/*    */ import controlmenu.controler.ControlTimeOut;
/*    */ import controlmenu.controler.ControlTimeOutImpl;
/*    */ import controlmenu.dao.BitacoraDAO;
/*    */ import controlmenu.dao.BitacoraDAOImpl;
/*    */ import controlmenu.dao.FormularioDAO;
/*    */ import controlmenu.dao.FormularioDAOImpl;
/*    */ import controlmenu.dao.GroupDAO;
/*    */ import controlmenu.dao.GroupDAOImpl;
/*    */ import controlmenu.dao.RolFormularioDAO;
/*    */ import controlmenu.dao.RolFormularioDAOImpl;
/*    */ import controlmenu.dao.RoleDAO;
/*    */ import controlmenu.dao.RoleDAOImpl;
/*    */ import controlmenu.dao.UserDAO;
/*    */ import controlmenu.dao.UserDAOImpl;
/*    */ 
/*    */ public class GuiceMainModule
/*    */   extends AbstractModule
/*    */ {
/*    */   protected void configure() {
/* 33 */     bind(UserBL.class).to(UserBLImpl.class);
/* 34 */     bind(UserDAO.class).to(UserDAOImpl.class);
/* 35 */     bind(GroupBL.class).to(GroupBLImpl.class);
/* 36 */     bind(GroupDAO.class).to(GroupDAOImpl.class);
/* 37 */     bind(RoleBL.class).to(RoleBLImpl.class);
/* 38 */     bind(RoleDAO.class).to(RoleDAOImpl.class);
/* 39 */     bind(BitacoraBL.class).to(BitacoraBLImpl.class);
/* 40 */     bind(BitacoraDAO.class).to(BitacoraDAOImpl.class);
/* 41 */     bind(FormularioDAO.class).to(FormularioDAOImpl.class);
/* 42 */     bind(RolFormularioDAO.class).to(RolFormularioDAOImpl.class);
/* 43 */     bind(ControlTimeOut.class).to(ControlTimeOutImpl.class);
/* 44 */     bind(ControlLogin.class).to(ControlLoginImpl.class);
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmen\\util\GuiceMainModule.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */