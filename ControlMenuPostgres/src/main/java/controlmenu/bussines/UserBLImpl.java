/*    */ package controlmenu.bussines;
/*    */ 
/*    */ import com.google.inject.Inject;
/*    */ import controlmenu.dao.GroupDAO;
/*    */ import controlmenu.dao.RolFormularioDAO;
/*    */ import controlmenu.dao.UserDAO;
/*    */ import controlmenu.model.GrupoAd;
/*    */ import controlmenu.model.Usuario;
/*    */ import java.util.List;
/*    */ 
/*    */ public class UserBLImpl
/*    */   implements UserBL
/*    */ {
/*    */   @Inject
/*    */   private UserDAO dao;
/*    */   @Inject
/*    */   private GroupDAO daoGr;
/*    */   @Inject
/*    */   private RolFormularioDAO daoRF;
/*    */   
/*    */   public Usuario getUser(int user_id) {
/* 22 */     return this.dao.getUserId(user_id);
/*    */   }
/*    */ 
/*    */   
/*    */   public List<Usuario> getUsers() {
/* 27 */     return this.dao.getUsers();
/*    */   }
/*    */ 
/*    */   
/*    */   public void updateUser(Usuario user) {
/* 32 */     this.dao.updateUser(user);
/*    */   }
/*    */ 
/*    */   
/*    */   public void insertUser(Usuario user) {
/* 37 */     user.setEstado("t");
/* 38 */     this.dao.insertUser(user);
/*    */   }
/*    */ 
/*    */   
/*    */   public void deleteUser(int user_id) {
/* 43 */     this.dao.deleteUser(user_id);
/*    */   }
/*    */ 
/*    */   
/*    */   public String validate(Usuario user, String idStr) {
/* 48 */     if (idStr != null && !idStr.isEmpty() && Integer.parseInt(idStr) == 1) {
/* 49 */       return "El Usuario no se puede modificar es de Administracion";
/*    */     }
/* 51 */     if (user.getLogin().isEmpty()) {
/* 52 */       return "El campo Login esta Vacio";
/*    */     }
/* 54 */     Usuario usAux = this.dao.getUserName(user.getLogin());
/* 55 */     if (usAux == null) {
/* 56 */       return "";
/*    */     }
/* 58 */     if (idStr != null && !idStr.isEmpty()) {
/* 59 */       int id = Integer.parseInt(idStr);
/* 60 */       if (id == 1) {
/* 61 */         return "El Usuario no se puede modificar es de Administracion";
/*    */       }
/* 63 */       if (id == usAux.getUsuarioId().intValue() && user.getLogin().equals(usAux.getLogin())) {
/* 64 */         return "";
/*    */       }
/*    */     } 
/* 67 */     return "este login existe";
/*    */   }
/*    */ 
/*    */   
/*    */   public int getIdRole(String login, List userGroups) {
/* 72 */     Usuario user = this.dao.getUserName(login);
/* 73 */     if (user != null) {
/* 74 */       return user.getRolId().intValue();
/*    */     }
/* 76 */     for (Object object : userGroups) {
/* 77 */       GrupoAd gr = this.daoGr.getGroupName(object.toString());
/* 78 */       if (gr != null) {
/* 79 */         return gr.getRolId().intValue();
/*    */       }
/*    */     } 
/* 82 */     return -1;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\UserBLImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */