package controlmenu.bussines;

import controlmenu.model.Usuario;
import java.util.List;

public interface UserBL {
  void deleteUser(int paramInt);
  
  int getIdRole(String paramString, List paramList);
  
  Usuario getUser(int paramInt);
  
  List<Usuario> getUsers();
  
  void insertUser(Usuario paramUsuario);
  
  void updateUser(Usuario paramUsuario);
  
  String validate(Usuario paramUsuario, String paramString);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\UserBL.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */