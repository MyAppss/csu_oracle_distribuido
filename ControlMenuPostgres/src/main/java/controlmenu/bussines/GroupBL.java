package controlmenu.bussines;

import controlmenu.model.GrupoAd;
import java.util.List;

public interface GroupBL {
  void deleteGroup(int paramInt);
  
  GrupoAd getGroup(int paramInt);
  
  List<GrupoAd> getGroups();
  
  void insertGroup(GrupoAd paramGrupoAd);
  
  void updateGroup(GrupoAd paramGrupoAd);
  
  String validate(GrupoAd paramGrupoAd, String paramString);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\GroupBL.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */