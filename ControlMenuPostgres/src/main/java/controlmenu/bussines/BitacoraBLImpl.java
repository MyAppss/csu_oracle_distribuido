/*    */ package controlmenu.bussines;
/*    */ 
/*    */ import com.google.inject.Inject;
/*    */ import controlmenu.dao.BitacoraDAO;
/*    */ import controlmenu.model.Bitacora;
/*    */ import java.sql.Timestamp;
/*    */ import java.util.Date;
/*    */ import java.util.List;
/*    */ 
/*    */ public class BitacoraBLImpl
/*    */   implements BitacoraBL
/*    */ {
/*    */   @Inject
/*    */   private BitacoraDAO dao;
/*    */   
/*    */   public List<Bitacora> getLogWebs() {
/* 17 */     return this.dao.getLogWebs();
/*    */   }
/*    */ 
/*    */   
/*    */   public void insertLogWeb(Bitacora logWeb) {
/* 22 */     logWeb.setStartDate(new Timestamp((new Date()).getTime()));
/* 23 */     this.dao.insertLogWeb(logWeb);
/*    */   }
/*    */ 
/*    */   
/*    */   public void insertLogWebTimeOut(Bitacora logWeb) {
/* 28 */     this.dao.insertLogWeb(logWeb);
/*    */   }
/*    */ 
/*    */   
/*    */   public void accionInsert(String user, String ip, Enum dato, String id, String name) {
/* 33 */     String ele = dato.name();
/* 34 */     String formulario = dato.toString();
/* 35 */     this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se adiciono:" + ele + " con Id:" + id + "; Nombre:" + name, ip);
/*    */   }
/*    */ 
/*    */   
/*    */   public void accionDelete(String user, String ip, Enum dato, String id, String name) {
/* 40 */     String ele = dato.name();
/* 41 */     String formulario = dato.toString();
/* 42 */     this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se elimino:" + ele + " con Id:" + id + "; Nombre:" + name, ip);
/*    */   }
/*    */ 
/*    */   
/*    */   public void accionUpdate(String user, String ip, Enum dato, String id, String name) {
/* 47 */     String ele = dato.name();
/* 48 */     String formulario = dato.toString();
/* 49 */     this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se modifico:" + ele + " con Id:" + id + "; Nombre:" + name, ip);
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\BitacoraBLImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */