package controlmenu.bussines;

import controlmenu.model.Rol;
import controlmenu.model.RolFormulario;
import java.util.List;

public interface RoleBL {
  void deleteRole(int paramInt);
  
  List<RolFormulario> getListRolFormulario(int paramInt);
  
  Rol getRole(int paramInt);
  
  List<Rol> getRoles();
  
  void insertRole(Rol paramRol);
  
  void updateRole(Rol paramRol);
  
  void updateRoleFormulario(RolFormulario paramRolFormulario);
  
  void updateRoleFormularioList(List<String> paramList, int paramInt);
  
  String validate(Rol paramRol, String paramString);
  
  String validateDelete(int paramInt);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\RoleBL.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */