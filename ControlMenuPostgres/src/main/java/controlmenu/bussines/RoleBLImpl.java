/*     */
package controlmenu.bussines;
/*     */
/*     */

import com.google.inject.Inject;
/*     */ import controlmenu.dao.FormularioDAO;
/*     */ import controlmenu.dao.GroupDAO;
/*     */ import controlmenu.dao.RolFormularioDAO;
/*     */ import controlmenu.dao.RoleDAO;
/*     */ import controlmenu.dao.UserDAO;
/*     */ import controlmenu.model.Formulario;
/*     */ import controlmenu.model.Rol;
/*     */ import controlmenu.model.RolFormulario;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;

/*     */
/*     */ public class RoleBLImpl
        /*     */ implements RoleBL
        /*     */ {
    /*     */
    @Inject
    /*     */ private RoleDAO dao;
    /*     */
    @Inject
    /*     */ private FormularioDAO daoFor;
    /*     */
    @Inject
    /*     */ private GroupDAO daoGroup;
    /*     */
    @Inject
    /*     */ private RolFormularioDAO daoRolFor;
    /*     */
    @Inject
    /*     */ private UserDAO daoUser;

    /*     */
    /*     */
    public String validate(Rol role, String idStr) {
        /*  32 */
        if (idStr != null && !idStr.isEmpty() && Integer.parseInt(idStr) == 1) {
            /*  33 */
            return "Este ROL no se puede modificar es de Administracion";
            /*     */
        }
        /*  35 */
        if (role.getNombre().isEmpty()) {
            /*  36 */
            return "El campo Nombre esta Vacio";
            /*     */
        }
        /*  38 */
        Rol rolAux = this.dao.getRoleName(role.getNombre());
        /*  39 */
        if (rolAux == null) {
            /*  40 */
            return "";
            /*     */
        }
        /*  42 */
        if (idStr != null && !idStr.isEmpty()) {
            /*  43 */
            int id = Integer.parseInt(idStr);
            /*  44 */
            if (id == 1) {
                /*  45 */
                return "Este ROL no se puede modificar es de Administracion";
                /*     */
            }
            /*  47 */
            if (id == rolAux.getRolId().intValue() && role.getNombre().equals(rolAux.getNombre())) {
                /*  48 */
                return "";
                /*     */
            }
            /*     */
        }
        /*  51 */
        return "este nombre existe";
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public String validateDelete(int roleId) {
        /*  56 */
        if (roleId == 1) {
            /*  57 */
            return "Este ROL no se puede eliminar es de Administracion";
            /*     */
        }
        /*  59 */
        if (!this.daoUser.getUsersDeleteRole(roleId).isEmpty()) {
            /*  60 */
            return "Existen Usuarios relacionados a este ROL.";
            /*     */
        }
        /*  62 */
        if (this.daoGroup.getGroupsDeleteRole(roleId).isEmpty()) {
            /*  63 */
            return "";
            /*     */
        }
        /*  65 */
        return "Existen Grupos relacionados a este ROL.";
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public Rol getRole(int roleId) {
        /*  70 */
        return this.dao.getRoleId(roleId);
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public List<Rol> getRoles() {
        /*  75 */
        return this.dao.getRoles();
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void insertRole(Rol role) {
        /*  80 */
        role.setEstado("t");
        /*  81 */
        Integer ID = this.dao.getLastId();
        /*  82 */
        role.setRolId(ID);
        /*  83 */
        this.dao.insertRole(role);
        /*  84 */
        for (Formulario formulario : this.daoFor.getFormularios()) {
            /*  85 */
            RolFormulario rolFor = new RolFormulario();
            /*  86 */
            rolFor.setRolId(role.getRolId());
            /*  87 */
            rolFor.setFormularioId(formulario.getFormularioId());
            /*  88 */
            rolFor.setEstado("t");
            /*  89 */
            this.daoRolFor.insert(rolFor);
            /*     */
        }
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void updateRole(Rol role) {
        /*  95 */
        this.dao.updateRole(role);
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void deleteRole(int roleId) {
        /* 100 */
        this.dao.deleteRole(roleId);
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public List<RolFormulario> getListRolFormulario(int idRol) {
        /* 105 */
        return this.daoRolFor.getList(idRol);
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void updateRoleFormulario(RolFormulario roleFor) {
        /* 110 */
        this.daoRolFor.update(roleFor);
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void updateRoleFormularioList(List<String> listaAvil, int idRol) {
        List<RolFormulario> listRolFor = getListRolFormulario(idRol);
        List<RolFormulario> listaValida = new ArrayList<>();

        for (String grupo : listaAvil) {
            for (RolFormulario rolFormulario : listRolFor) {
                if (grupo.equals(rolFormulario.getName())) {
                    listaValida.add(rolFormulario);
                }
            }

        }


        for (RolFormulario rolFormularioValido : listaValida) {
            for (RolFormulario rolFormulario : listRolFor) {
                if (rolFormulario.getDepende() != null && rolFormulario.getDepende() == rolFormularioValido.getFormularioId()) {
                    rolFormulario.setEstado("t");

                    daoRolFor.update(rolFormulario);

                }
            }
        }
        /*     */
    }
    /*     */
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\RoleBLImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */