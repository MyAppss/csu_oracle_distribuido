/*    */
package controlmenu;
/*    */
/*    */

import controlmenu.bussines.RoleBL;
import controlmenu.dao.RolFormularioDAO;
/*    */ import controlmenu.model.RolFormulario;
/*    */ import controlmenu.util.GuiceInjectorSingletonCM;
import controlmenu.util.ParametersCM;

import java.util.ArrayList;
import java.util.List;
/*    */

/*    */ public class ControlMenu
        /*    */ {
    /*    */
    public static void main(String[] args) {

        ParametersCM.HOST = "localhost";
        ParametersCM.DB = "CONSULTA_SALDO";
        ParametersCM.PORT = "5432";
        ParametersCM.USER = "postgres";
        ParametersCM.PASSWORD = "openpgpwd";

        ParametersCM.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = "60000";
        ParametersCM.DB_MAX_ACTIVE = "58";
        ParametersCM.DB_MIN_IDLE = "55";
        ParametersCM.DB_MAX_IDLE = "56";
        ParametersCM.DB_MAX_WAIT = "57";

/*        RolFormulario aux = new RolFormulario(null, "t",85, "6", Integer.valueOf(37), "VEHIMAR");

        ((RolFormularioDAO) GuiceInjectorSingletonCM.getInstance(RolFormularioDAO.class)).update(aux);*/

        RoleBL roleBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
        List<String> lista = new ArrayList<>();
        lista.add("Configuraciones para Billeteras");
        roleBL.updateRoleFormularioList(lista, 1);
    }
}


