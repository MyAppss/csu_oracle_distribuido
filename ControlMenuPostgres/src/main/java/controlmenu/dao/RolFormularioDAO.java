package controlmenu.dao;

import controlmenu.model.RolFormulario;
import java.util.List;

public interface RolFormularioDAO {
  List<RolFormulario> getList(int paramInt);
  
  void insert(RolFormulario paramRolFormulario);
  
  void update(RolFormulario paramRolFormulario);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\RolFormularioDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */