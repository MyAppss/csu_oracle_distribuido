/*     */ package controlmenu.dao;
/*     */ 
/*     */ import controlmenu.model.Bitacora;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.sql.Timestamp;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BitacoraDAOImpl
/*     */   implements BitacoraDAO
/*     */ {
/*  19 */   private static final Logger LOG = Logger.getLogger(BitacoraDAOImpl.class);
/*     */ 
/*     */ 
/*     */   
/*     */   public List<Bitacora> getLogWebs() {
/*  24 */     List<Bitacora> list = null;
/*  25 */     Statement stmt = null;
/*  26 */     Connection conn = null;
/*  27 */     ResultSet rs = null;
/*     */     try {
/*  29 */       String sql = "SELECT * FROM ( SELECT * FROM bitacora ORDER BY fecha DESC ) as auxBitacora limit 1000";
/*  30 */       LOG.debug("[getUsers] SQL: " + sql);
/*  31 */       conn = FactoryNative.getConnection();
/*  32 */       if (conn != null) {
/*  33 */         stmt = conn.createStatement();
/*  34 */         rs = stmt.executeQuery(sql);
/*  35 */         if (rs != null) {
/*  36 */           List<Bitacora> result = new ArrayList<>();
/*  37 */           while (rs.next()) {
/*  38 */             Bitacora bitacora = new Bitacora();
/*  39 */             bitacora.setStartDate(rs.getTimestamp("fecha"));
/*  40 */             bitacora.setUser(rs.getString("usuario"));
/*  41 */             bitacora.setForm(rs.getString("formulario"));
/*  42 */             bitacora.setAction(rs.getString("accion"));
/*  43 */             bitacora.setAddress(rs.getString("direccion_ip"));
/*  44 */             result.add(bitacora);
/*     */           } 
/*  46 */           list = result;
/*     */         } 
/*     */       } 
/*  49 */     } catch (SQLException e3) {
/*  50 */       LOG.error("[getLogWebs] Error al intentar obtener el plan de consumo SQLException:" + e3.getMessage());
/*     */     } finally {
/*     */       
/*  53 */       if (stmt != null) {
/*     */         try {
/*  55 */           stmt.close();
/*  56 */         } catch (SQLException ex2) {
/*  57 */           LOG.warn("[getLogWebs] Error al intentar cerrar Statement despues de obtener la lista de Bitacora SQLException:", ex2);
/*     */         } 
/*     */       }
/*     */       
/*  61 */       if (rs != null) {
/*     */         try {
/*  63 */           rs.close();
/*  64 */         } catch (SQLException ex2) {
/*  65 */           LOG.warn("[getLogWebs] Error al intentar cerrar ResultSet despues de obtener la lista de Bitacora SQLException:", ex2);
/*     */         } 
/*     */       }
/*     */       
/*  69 */       if (conn != null) {
/*     */         try {
/*  71 */           conn.close();
/*  72 */         } catch (SQLException ex) {
/*  73 */           LOG.warn("[getLogWebs] Error al intentar cerrar Connection despues de obtener la lista de Bitacora SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*  77 */     return list;
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertLogWeb(Bitacora logWeb) {
/*  82 */     Connection conn = null;
/*  83 */     PreparedStatement pstmt = null;
/*     */     try {
/*  85 */       String sql = "INSERT INTO bitacora (fecha, usuario, formulario, accion, direccion_ip) VALUES (?,?,?,?,?)";
/*  86 */       LOG.debug("[insertLogWeb] SQL: " + sql);
/*  87 */       conn = FactoryNative.getConnection();
/*  88 */       if (conn != null) {
/*  89 */         pstmt = conn.prepareStatement(sql);
/*  90 */         pstmt.setTimestamp(1, logWeb.getStartDate());
/*  91 */         pstmt.setString(2, logWeb.getUser());
/*  92 */         pstmt.setString(3, logWeb.getForm());
/*  93 */         pstmt.setString(4, logWeb.getAction());
/*  94 */         pstmt.setString(5, logWeb.getAddress());
/*  95 */         pstmt.execute();
/*     */       } 
/*  97 */     } catch (SQLException e) {
/*  98 */       LOG.error("[insertLogWeb] Error al insertar en la DB SQLException:" + e.getMessage());
/*     */     } finally {
/* 100 */       if (pstmt != null) {
/*     */         try {
/* 102 */           pstmt.close();
/* 103 */         } catch (SQLException ex) {
/* 104 */           LOG.warn("[insertLogWeb] Error al intentar cerrar ", ex);
/*     */         } 
/*     */       }
/*     */       
/* 108 */       if (conn != null) {
/*     */         try {
/* 110 */           conn.close();
/* 111 */         } catch (SQLException ex) {
/* 112 */           LOG.warn("[insertLogWeb] Error al intentar cerrar:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertLogWebBase(Timestamp startDate, String user, String form, String action, String address) {
/* 120 */     Connection conn = null;
/* 121 */     PreparedStatement pstmt = null;
/*     */     try {
/* 123 */       String sql = "INSERT INTO bitacora (fecha, usuario, formulario, accion, direccion_ip) VALUES (?,?,?,?,?)";
/* 124 */       LOG.debug("[insertLogWebBase] SQL: " + sql);
/* 125 */       conn = FactoryNative.getConnection();
/* 126 */       if (conn != null) {
/*     */         
/* 128 */         pstmt = conn.prepareStatement(sql);
/* 129 */         pstmt.setTimestamp(1, startDate);
/* 130 */         pstmt.setString(2, user);
/* 131 */         pstmt.setString(3, form);
/* 132 */         pstmt.setString(4, action);
/* 133 */         pstmt.setString(5, address);
/* 134 */         pstmt.execute();
/*     */       } 
/* 136 */     } catch (SQLException e) {
/* 137 */       LOG.error("[insertLogWebBase] Error al intentar adicionar a la SQLException:", e);
/*     */     } finally {
/* 139 */       if (pstmt != null) {
/*     */         try {
/* 141 */           pstmt.close();
/* 142 */         } catch (SQLException ex22) {
/* 143 */           LOG.warn("[insertLogWebBase] Error al intentar cerrar ", ex22);
/*     */         } 
/*     */       }
/*     */       
/* 147 */       if (conn != null)
/*     */         try {
/* 149 */           conn.close();
/* 150 */         } catch (SQLException ex) {
/* 151 */           LOG.warn("[insertLogWeb] Error al intentar cerrar:", ex);
/*     */         }  
/*     */     } 
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\BitacoraDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */