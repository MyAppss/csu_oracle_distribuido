package controlmenu.dao;

import controlmenu.model.Formulario;
import java.util.List;

public interface FormularioDAO {
  List<Formulario> getFormularios();
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\FormularioDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */