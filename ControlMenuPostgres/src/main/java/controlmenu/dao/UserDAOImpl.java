/*     */ package controlmenu.dao;
/*     */ 
/*     */ import controlmenu.model.Usuario;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UserDAOImpl
/*     */   implements UserDAO
/*     */ {
/*  18 */   private static final Logger LOG = Logger.getLogger(UserDAOImpl.class);
/*     */ 
/*     */ 
/*     */   
/*     */   public Usuario getUserId(int user_id) {
/*  23 */     Statement stmt = null;
/*  24 */     Usuario usuario = null;
/*  25 */     Connection conn = null;
/*  26 */     ResultSet rs = null;
/*     */     try {
/*  28 */       String sql = "SELECT * FROM usuario WHERE usuario_id=" + user_id;
/*  29 */       LOG.debug("[getUserId] SQL: " + sql);
/*  30 */       conn = FactoryNative.getConnection();
/*  31 */       if (conn != null) {
/*  32 */         stmt = conn.createStatement();
/*  33 */         rs = stmt.executeQuery(sql);
/*  34 */         if (rs != null && rs.next()) {
/*  35 */           Usuario result = new Usuario();
/*  36 */           result.setUsuarioId(Integer.valueOf(rs.getInt("usuario_id")));
/*  37 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  38 */           result.setLogin(rs.getString("login"));
/*  39 */           result.setNombre(rs.getString("nombre"));
/*  40 */           result.setEstado(rs.getString("estado"));
/*  41 */           usuario = result;
/*     */         } 
/*     */       } 
/*  44 */     } catch (SQLException e3) {
/*  45 */       LOG.error("[getUserId] Error al intentar obteneral usuario con id " + user_id + "SQLException:", e3);
/*     */     } finally {
/*  47 */       if (rs != null) {
/*     */         try {
/*  49 */           rs.close();
/*  50 */         } catch (SQLException ex22) {
/*  51 */           LOG.warn("[getUserId] Error al intentar cerrar el ResultSet usuario con Id: " + user_id + " SQLException:", ex22);
/*     */         } 
/*     */       }
/*  54 */       if (stmt != null) {
/*     */         try {
/*  56 */           stmt.close();
/*  57 */         } catch (SQLException ex222) {
/*  58 */           LOG.warn("[getUserId] Error al intentar cerrar Statemental obtener al usuario con id:" + user_id + "SQLException:", ex222);
/*     */         } 
/*     */       }
/*  61 */       if (conn != null) {
/*     */         try {
/*  63 */           conn.close();
/*  64 */         } catch (SQLException ex) {
/*  65 */           LOG.warn("[getUserId] Error al intentar cerrar Connection despues de obtener userId SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*  69 */     return usuario;
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public Usuario getUserName(String name) {
/*  75 */     Usuario usuario = null;
/*  76 */     Statement stmt = null;
/*  77 */     Connection conn = null;
/*  78 */     ResultSet rs = null;
/*     */     try {
/*  80 */       String sql = "SELECT * FROM usuario WHERE login='" + name + "' AND (estado='t' OR estado='true')";
/*  81 */       LOG.debug("[getUserName] SQL: " + sql);
/*  82 */       conn = FactoryNative.getConnection();
/*  83 */       if (conn != null) {
/*  84 */         stmt = conn.createStatement();
/*  85 */         rs = stmt.executeQuery(sql);
/*  86 */         if (rs != null && rs.next()) {
/*  87 */           Usuario result = new Usuario();
/*  88 */           result.setUsuarioId(Integer.valueOf(rs.getInt("usuario_id")));
/*  89 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  90 */           result.setLogin(rs.getString("login"));
/*  91 */           result.setNombre(rs.getString("nombre"));
/*  92 */           result.setEstado(rs.getString("estado"));
/*  93 */           usuario = result;
/*     */         } 
/*     */       } 
/*  96 */     } catch (SQLException e3) {
/*  97 */       LOG.error("[getUserName] Error al intentar obteneral usuario con login: " + name + "SQLException:", e3);
/*     */     } finally {
/*  99 */       if (rs != null) {
/*     */         try {
/* 101 */           rs.close();
/* 102 */         } catch (SQLException ex22) {
/* 103 */           LOG.warn("[getUserName] Error al intentar cerrar el ResultSet usuario con login: " + name + " SQLException:", ex22);
/*     */         } 
/*     */       }
/* 106 */       if (stmt != null) {
/*     */         try {
/* 108 */           stmt.close();
/* 109 */         } catch (SQLException ex222) {
/* 110 */           LOG.warn("[getUserName] Error al intentar cerrar Statemental obtener al usuario con login: " + name + "SQLException:", ex222);
/*     */         } 
/*     */       }
/*     */       
/* 114 */       if (conn != null) {
/*     */         try {
/* 116 */           conn.close();
/* 117 */         } catch (SQLException ex) {
/* 118 */           LOG.warn("[getUserName] Error al intentar cerrar Connection despues de obtener userName SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 122 */     return usuario;
/*     */   }
/*     */ 
/*     */   
/*     */   public List<Usuario> getUsers() {
/* 127 */     List<Usuario> list = null;
/* 128 */     Statement stmt = null;
/* 129 */     Connection conn = null;
/* 130 */     ResultSet rs = null;
/*     */     try {
/* 132 */       String sql = "SELECT usuario.usuario_id,usuario.rol_id as rol_id,usuario.login,usuario.nombre as nombre,usuario.estado as estado_user, rol.nombre as nombre_rol\nFROM usuario INNER JOIN rol ON usuario.rol_id=rol.rol_id \nWHERE usuario.estado='true' OR usuario.estado='t' ORDER BY  nombre_rol";
/*     */ 
/*     */       
/* 135 */       LOG.debug("[getUsers] SQL: " + sql);
/* 136 */       conn = FactoryNative.getConnection();
/* 137 */       if (conn != null) {
/* 138 */         stmt = conn.createStatement();
/* 139 */         rs = stmt.executeQuery(sql);
/* 140 */         if (rs != null) {
/* 141 */           List<Usuario> result = new ArrayList<>();
/* 142 */           while (rs.next()) {
/* 143 */             Usuario usuario = new Usuario();
/* 144 */             usuario.setUsuarioId(Integer.valueOf(rs.getInt("usuario_id")));
/* 145 */             usuario.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/* 146 */             usuario.setLogin(rs.getString("login"));
/* 147 */             usuario.setNombre(rs.getString("nombre"));
/* 148 */             usuario.setEstado(rs.getString("estado_user"));
/* 149 */             usuario.setNameRole(rs.getString("nombre_rol"));
/* 150 */             result.add(usuario);
/*     */           } 
/* 152 */           list = result;
/*     */         } 
/*     */       } 
/* 155 */     } catch (SQLException e3) {
/* 156 */       LOG.error("[getUsers] Error al intentar obtenerSQLException:", e3);
/*     */     } finally {
/* 158 */       if (rs != null) {
/*     */         try {
/* 160 */           rs.close();
/* 161 */         } catch (SQLException ex) {
/* 162 */           LOG.warn("[getUsers] Error al intentar cerrar el ResultSet SQLException:", ex);
/*     */         } 
/*     */       }
/* 165 */       if (stmt != null) {
/*     */         try {
/* 167 */           stmt.close();
/* 168 */         } catch (SQLException ex2) {
/* 169 */           LOG.warn("[getUsers] Error al intentar cerrar StatementSQLException:", ex2);
/*     */         } 
/*     */       }
/*     */       
/* 173 */       if (conn != null) {
/*     */         try {
/* 175 */           conn.close();
/* 176 */         } catch (SQLException ex) {
/* 177 */           LOG.warn("[getUsers] Error al intentar cerrar Connection despues de obtener user SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 181 */     return list;
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertUser(Usuario user) {
/* 186 */     PreparedStatement pstmt = null;
/* 187 */     Connection conn = null;
/*     */     try {
/* 189 */       String sql = "INSERT INTO usuario (usuario_id,rol_id, login, nombre,estado) VALUES ( nextval('usuario_usuario_id_seq'),?,?,?,?)";
/* 190 */       LOG.debug("[insertUser] SQL: " + sql);
/* 191 */       conn = FactoryNative.getConnection();
/* 192 */       if (conn != null) {
/* 193 */         pstmt = conn.prepareStatement(sql);
/* 194 */         pstmt.setInt(1, user.getRolId().intValue());
/* 195 */         pstmt.setString(2, user.getLogin());
/* 196 */         pstmt.setString(3, user.getNombre());
/* 197 */         pstmt.setString(4, user.getEstado());
/* 198 */         pstmt.execute();
/*     */       }
/*     */     
/* 201 */     } catch (SQLException e) {
/* 202 */       LOG.error("[insertUser] Error al intentar adicionaral usuario con id:" + user.getUsuarioId() + "SQLException:", e);
/*     */     } finally {
/* 204 */       if (pstmt != null) {
/*     */         try {
/* 206 */           pstmt.close();
/* 207 */         } catch (SQLException ex) {
/* 208 */           LOG.warn("[insertUser] Error al intentar cerrar al usuario con id:" + user.getUsuarioId() + "SQLException:", ex);
/*     */         } 
/*     */       }
/* 211 */       if (conn != null) {
/*     */         try {
/* 213 */           conn.close();
/* 214 */         } catch (SQLException ex) {
/* 215 */           LOG.warn("[insertUser] Error al intentar cerrar Connection despues de insertar user SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void updateUser(Usuario user) {
/* 223 */     Connection conn = null;
/* 224 */     Statement st = null;
/* 225 */     if (user.getNombre() == null) {
/* 226 */       user.setNombre("");
/*     */     }
/*     */     try {
/* 229 */       String sql = "UPDATE usuario SET rol_id=" + user.getRolId() + ", login ='" + user.getLogin() + "', nombre ='" + user.getNombre() + "' WHERE usuario_id=" + user.getUsuarioId();
/* 230 */       LOG.debug("[updateUser] SQL: " + sql);
/* 231 */       conn = FactoryNative.getConnection();
/* 232 */       if (conn != null) {
/* 233 */         st = conn.createStatement();
/* 234 */         st.executeUpdate(sql);
/*     */       }
/*     */     
/* 237 */     } catch (SQLException e) {
/* 238 */       LOG.warn("[updateUser] Error al intentar actualizar al usuario con id" + user.getUsuarioId() + "SQLException:", e);
/*     */     } finally {
/*     */       
/* 241 */       if (st != null) {
/*     */         try {
/* 243 */           st.close();
/* 244 */         } catch (SQLException ex) {
/* 245 */           LOG.warn("[updateUser] Error al intentar cerrar Statement despues de actualizarusuario con id:" + user.getUsuarioId() + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 248 */       if (conn != null) {
/*     */         try {
/* 250 */           conn.close();
/* 251 */         } catch (SQLException ex) {
/* 252 */           LOG.warn("[updateUser] Error al intentar cerrar Connection despues de actualizar user SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void deleteUser(int user_id) {
/* 260 */     Connection conn = null;
/* 261 */     Statement st = null;
/*     */     try {
/* 263 */       String sql = "UPDATE usuario SET estado = 'f' WHERE usuario_id=" + user_id;
/* 264 */       LOG.debug("[deleteUser] SQL: " + sql);
/* 265 */       conn = FactoryNative.getConnection();
/* 266 */       if (conn != null) {
/* 267 */         st = conn.createStatement();
/* 268 */         st.executeUpdate(sql);
/*     */       } 
/* 270 */     } catch (SQLException e) {
/* 271 */       LOG.warn("[deleteUser] Error al intentar actualizar al usuario con id" + user_id + "SQLException:", e);
/*     */     } finally {
/*     */       
/* 274 */       if (st != null) {
/*     */         try {
/* 276 */           st.close();
/* 277 */         } catch (SQLException ex) {
/* 278 */           LOG.warn("[deleteUser] Error al intentar cerrar Statement despues de actualizarusuario con id:" + user_id + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 281 */       if (conn != null) {
/*     */         try {
/* 283 */           conn.close();
/* 284 */         } catch (SQLException ex) {
/* 285 */           LOG.warn("[deleteUser] Error al intentar cerrar Connection despues de eliminar user SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public List<Usuario> getUsersDeleteRole(int idRole) {
/* 293 */     List<Usuario> list = null;
/* 294 */     Statement stmt = null;
/* 295 */     Connection conn = null;
/* 296 */     ResultSet rs = null;
/*     */     try {
/* 298 */       String sql = "SELECT * FROM usuario WHERE (estado='true' OR estado='t') and rol_id=" + idRole;
/* 299 */       LOG.debug("[getUsersDeleteRole] SQL: " + sql);
/* 300 */       conn = FactoryNative.getConnection();
/* 301 */       if (conn != null) {
/* 302 */         stmt = conn.createStatement();
/* 303 */         rs = stmt.executeQuery(sql);
/* 304 */         if (rs != null) {
/* 305 */           List<Usuario> result = new ArrayList<>();
/* 306 */           while (rs.next()) {
/* 307 */             Usuario usuario = new Usuario();
/* 308 */             usuario.setUsuarioId(Integer.valueOf(rs.getInt("usuario_id")));
/* 309 */             usuario.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/* 310 */             usuario.setLogin(rs.getString("login"));
/* 311 */             usuario.setNombre(rs.getString("nombre"));
/* 312 */             usuario.setEstado("t");
/* 313 */             result.add(usuario);
/*     */           } 
/* 315 */           list = result;
/*     */         } 
/*     */       } 
/* 318 */     } catch (SQLException e3) {
/* 319 */       LOG.error("[getUsersDeleteRole] Error al intentar obtenerSQLException:", e3);
/*     */     } finally {
/* 321 */       if (rs != null) {
/*     */         try {
/* 323 */           rs.close();
/* 324 */         } catch (SQLException ex) {
/* 325 */           LOG.warn("[getUsersDeleteRole] Error al intentar cerrar el ResultSet SQLException:", ex);
/*     */         } 
/*     */       }
/* 328 */       if (stmt != null) {
/*     */         try {
/* 330 */           stmt.close();
/* 331 */         } catch (SQLException ex2) {
/* 332 */           LOG.warn("[getUsersDeleteRole] Error al intentar cerrar StatementSQLException:", ex2);
/*     */         } 
/*     */       }
/*     */       
/* 336 */       if (conn != null) {
/*     */         try {
/* 338 */           conn.close();
/* 339 */         } catch (SQLException ex) {
/* 340 */           LOG.warn("[getUsersDeleteRole] Error al intentar cerrar Connection despues de obtener la lista user role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 344 */     return list;
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\UserDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */