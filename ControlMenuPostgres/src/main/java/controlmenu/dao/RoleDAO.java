package controlmenu.dao;

import controlmenu.model.Rol;
import java.util.List;

public interface RoleDAO {
  Integer getLastId();
  
  void deleteRole(int paramInt);
  
  Rol getRoleId(int paramInt);
  
  Rol getRoleName(String paramString);
  
  List<Rol> getRoles();
  
  void insertRole(Rol paramRol);
  
  void updateRole(Rol paramRol);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\RoleDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */