/*     */ package controlmenu.dao;
/*     */ 
/*     */ import controlmenu.model.Rol;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RoleDAOImpl
/*     */   implements RoleDAO
/*     */ {
/*  18 */   private static final Logger LOG = Logger.getLogger(RoleDAOImpl.class);
/*     */ 
/*     */ 
/*     */   
/*     */   public Rol getRoleId(int roleId) {
/*  23 */     Rol rol = null;
/*  24 */     Statement stmt = null;
/*  25 */     Connection conn = null;
/*  26 */     ResultSet rs = null;
/*     */     try {
/*  28 */       String sql = "SELECT * FROM rol WHERE rol_id=" + roleId;
/*  29 */       LOG.debug("[getUsers] SQL: " + sql);
/*  30 */       conn = FactoryNative.getConnection();
/*  31 */       if (conn != null) {
/*  32 */         stmt = conn.createStatement();
/*  33 */         rs = stmt.executeQuery(sql);
/*  34 */         if (rs != null && rs.next()) {
/*  35 */           Rol result = new Rol();
/*  36 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  37 */           result.setNombre(rs.getString("nombre"));
/*  38 */           result.setDescripcion(rs.getString("descripcion"));
/*  39 */           result.setEstado(rs.getString("estado"));
/*  40 */           rol = result;
/*     */         } 
/*     */       } 
/*  43 */     } catch (SQLException e3) {
/*  44 */       LOG.error("[getRoleId] Error al intentar obtener el Roles  con id  SQLException:", e3);
/*     */     } finally {
/*  46 */       if (stmt != null) {
/*     */         try {
/*  48 */           stmt.close();
/*  49 */         } catch (SQLException ex) {
/*  50 */           LOG.warn("[getRoleId] Error al intentar cerrar Statement despues de obtener  Role SQLException:", ex);
/*     */         } 
/*     */       }
/*  53 */       if (rs != null) {
/*     */         try {
/*  55 */           rs.close();
/*  56 */         } catch (SQLException ex22) {
/*  57 */           LOG.warn("[getRoleId] Error al intentar cerrar el ResultSet despues de obtener el Roles por  SQLException:", ex22);
/*     */         } 
/*     */       }
/*  60 */       if (conn != null) {
/*     */         try {
/*  62 */           conn.close();
/*  63 */         } catch (SQLException ex) {
/*  64 */           LOG.warn("[getRoleId] Error al intentar cerrar Connection despues obtener un role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*  68 */     return rol;
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public Rol getRoleName(String name) {
/*  74 */     Rol rol = null;
/*  75 */     Statement stmt = null;
/*  76 */     Connection conn = null;
/*  77 */     ResultSet rs = null;
/*     */     try {
/*  79 */       String sql = "SELECT * FROM rol WHERE nombre='" + name + "'AND (estado='true' OR estado='t')";
/*  80 */       LOG.debug("[getRoleName] SQL: " + sql);
/*  81 */       conn = FactoryNative.getConnection();
/*  82 */       if (conn != null) {
/*  83 */         stmt = conn.createStatement();
/*  84 */         rs = stmt.executeQuery(sql);
/*  85 */         if (rs != null && rs.next()) {
/*  86 */           Rol result = new Rol();
/*  87 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  88 */           result.setNombre(rs.getString("nombre"));
/*  89 */           result.setDescripcion(rs.getString("descripcion"));
/*  90 */           result.setEstado(rs.getString("estado"));
/*  91 */           rol = result;
/*     */         } 
/*     */       } 
/*  94 */     } catch (SQLException e3) {
/*  95 */       LOG.error("[getRoleName] Error al intentar obtener de Roles  SQLException:", e3);
/*     */     } finally {
/*  97 */       if (stmt != null) {
/*     */         try {
/*  99 */           stmt.close();
/* 100 */         } catch (SQLException ex) {
/* 101 */           LOG.warn("[getRoleName] Error al intentar cerrar Statement despues de obtener la  Roles SQLException:", ex);
/*     */         } 
/*     */       }
/*     */       
/* 105 */       if (rs != null) {
/*     */         try {
/* 107 */           rs.close();
/* 108 */         } catch (SQLException ex22) {
/* 109 */           LOG.warn("[getRoleName] Error al intentar cerrar el ResultSet despues de obtener  de Roles SQLException:", ex22);
/*     */         } 
/*     */       }
/* 112 */       if (conn != null) {
/*     */         try {
/* 114 */           conn.close();
/* 115 */         } catch (SQLException ex) {
/* 116 */           LOG.warn("[getRoleName] Error al intentar cerrar Connection despues obtener nombre del role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 120 */     return rol;
/*     */   }
/*     */ 
/*     */   
/*     */   public List<Rol> getRoles() {
/* 125 */     List<Rol> list = null;
/* 126 */     Statement stmt = null;
/* 127 */     Connection conn = null;
/* 128 */     ResultSet rs = null;
/*     */     try {
/* 130 */       String sql = "SELECT * FROM rol WHERE estado='true' OR estado='t' ORDER BY nombre";
/* 131 */       LOG.debug("[getRoles] SQL: " + sql);
/* 132 */       conn = FactoryNative.getConnection();
/* 133 */       if (conn != null) {
/* 134 */         stmt = conn.createStatement();
/* 135 */         rs = stmt.executeQuery(sql);
/* 136 */         if (rs != null) {
/* 137 */           List<Rol> result = new ArrayList<>();
/* 138 */           while (rs.next()) {
/* 139 */             Rol grupo = new Rol();
/* 140 */             grupo.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/* 141 */             grupo.setNombre(rs.getString("nombre"));
/* 142 */             grupo.setDescripcion(rs.getString("descripcion"));
/* 143 */             grupo.setEstado(rs.getString("estado"));
/* 144 */             result.add(grupo);
/*     */           } 
/* 146 */           list = result;
/*     */         } 
/*     */       } 
/* 149 */     } catch (SQLException e3) {
/* 150 */       LOG.error("[getRoles] Error al intentar obtener la lista de Roles SQLException:", e3);
/*     */     } finally {
/* 152 */       if (rs != null) {
/*     */         try {
/* 154 */           rs.close();
/* 155 */         } catch (SQLException ex) {
/* 156 */           LOG.warn("[getRoles] Error al intentar cerrar el ResultSet despues de obtener la lista de Roles  SQLException:", ex);
/*     */         } 
/*     */       }
/* 159 */       if (stmt != null) {
/*     */         try {
/* 161 */           stmt.close();
/* 162 */         } catch (SQLException ex2) {
/* 163 */           LOG.warn("[getRoles] Error al intentar cerrar Statement despues de obtener la lista de Roles SQLException:", ex2);
/*     */         } 
/*     */       }
/* 166 */       if (conn != null) {
/*     */         try {
/* 168 */           conn.close();
/* 169 */         } catch (SQLException ex) {
/* 170 */           LOG.warn("[getRoles] Error al intentar cerrar Connection despues obtener lista de role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */     
/* 175 */     return list;
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertRole(Rol role) {
/* 180 */     PreparedStatement pstmt = null;
/* 181 */     Connection conn = null;
/*     */     try {
/* 183 */       conn = FactoryNative.getConnection();
/* 184 */       if (conn != null) {
/* 185 */         pstmt = conn.prepareStatement("INSERT INTO rol (rol_id,nombre,descripcion,estado) VALUES (?,?,?,?)");
/* 186 */         pstmt.setInt(1, role.getRolId().intValue());
/* 187 */         pstmt.setString(2, role.getNombre());
/* 188 */         pstmt.setString(3, role.getDescripcion());
/* 189 */         pstmt.setString(4, role.getEstado());
/* 190 */         pstmt.execute();
/*     */       } 
/* 192 */     } catch (SQLException e) {
/* 193 */       LOG.error("[insertRole] Error al intentar adicionar a la SQLException:", e);
/*     */     } finally {
/* 195 */       if (pstmt != null) {
/*     */         try {
/* 197 */           pstmt.close();
/* 198 */         } catch (SQLException ex) {
/* 199 */           LOG.warn("[insertRole] Error al intentar cerrar ", ex);
/*     */         } 
/*     */       }
/* 202 */       if (conn != null) {
/*     */         try {
/* 204 */           conn.close();
/* 205 */         } catch (SQLException ex) {
/* 206 */           LOG.warn("[insertRole] Error al intentar cerrar Connection despues de insertar role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public void updateRole(Rol role) {
/* 215 */     Connection conn = null;
/* 216 */     Statement st = null;
/*     */     
/*     */     try {
/* 219 */       String sql = "UPDATE rol SET nombre ='" + role.getNombre() + "',descripcion ='" + role.getDescripcion() + "' WHERE rol_id=" + role.getRolId();
/* 220 */       LOG.debug("[updateRole] SQL: " + sql);
/* 221 */       conn = FactoryNative.getConnection();
/* 222 */       if (conn != null) {
/* 223 */         st = conn.createStatement();
/* 224 */         st.executeUpdate(sql);
/*     */       } 
/* 226 */     } catch (SQLException e) {
/* 227 */       LOG.warn("[updateRole] Error al intentar actualizar la Role con id  " + role.getRolId() + ". SQLException:", e);
/*     */     } finally {
/* 229 */       if (st != null) {
/*     */         try {
/* 231 */           st.close();
/* 232 */         } catch (SQLException ex) {
/* 233 */           LOG.warn("[updateRole] Error al intentar cerrar Statement despues de actualizar Role" + role.getRolId() + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 236 */       if (conn != null) {
/*     */         try {
/* 238 */           conn.close();
/* 239 */         } catch (SQLException ex) {
/* 240 */           LOG.warn("[updateRole] Error al intentar cerrar Connection despues de actualizar role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void deleteRole(int roleId) {
/* 248 */     Connection conn = null;
/* 249 */     Statement st = null;
/*     */     try {
/* 251 */       String sql = "UPDATE rol SET estado = 'f' WHERE rol_id=" + roleId;
/* 252 */       LOG.debug("[deleteRole] SQL: " + sql);
/* 253 */       conn = FactoryNative.getConnection();
/* 254 */       if (conn != null) {
/* 255 */         st = conn.createStatement();
/* 256 */         st.executeUpdate(sql);
/*     */       } 
/* 258 */     } catch (SQLException e) {
/* 259 */       LOG.error("[deleteRole] Error al intentar actualizar Role " + roleId + ". SQLException:", e);
/*     */     } finally {
/* 261 */       if (st != null) {
/*     */         try {
/* 263 */           st.close();
/* 264 */         } catch (SQLException ex) {
/* 265 */           LOG.warn("[deleteRole] Error al intentar cerrar Statement despues de actualizar Role " + roleId + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 268 */       if (conn != null) {
/*     */         try {
/* 270 */           conn.close();
/* 271 */         } catch (SQLException ex) {
/* 272 */           LOG.warn("[deleteRole] Error al intentar cerrar Connection despues de eliminar role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public Integer getLastId() {
/* 281 */     Statement stmt = null;
/* 282 */     Connection conn = null;
/* 283 */     Integer RoleID = Integer.valueOf(0);
/* 284 */     ResultSet rs = null;
/*     */     try {
/* 286 */       String sql = "SELECT nextval('rol_rol_id_seq')";
/* 287 */       LOG.debug("[getLastId] SQL: " + sql);
/* 288 */       conn = FactoryNative.getConnection();
/* 289 */       if (conn != null) {
/* 290 */         stmt = conn.createStatement();
/* 291 */         rs = stmt.executeQuery(sql);
/* 292 */         if (rs != null && rs.next()) {
/* 293 */           RoleID = Integer.valueOf(rs.getInt("nextval"));
/*     */         }
/*     */       }
/*     */     
/* 297 */     } catch (SQLException e3) {
/* 298 */       LOG.error("[getLastId] Error al intentar obtener el ID del Rol  SQLException:", e3);
/*     */     } finally {
/*     */       
/* 301 */       if (rs != null) {
/*     */         try {
/* 303 */           rs.close();
/* 304 */         } catch (SQLException ex22) {
/* 305 */           LOG.warn("[getLastId] Error al intentar cerrar el ResultSet despues de obtener el ID del Rol SQLException:", ex22);
/*     */         } 
/*     */       }
/* 308 */       if (stmt != null) {
/*     */         try {
/* 310 */           stmt.close();
/* 311 */         } catch (SQLException ex222) {
/* 312 */           LOG.warn("[getLastId] Error al intentar cerrar Statement despues de obtener el ID del ROL SQLException:", ex222);
/*     */         } 
/*     */       }
/* 315 */       if (conn != null) {
/*     */         try {
/* 317 */           conn.close();
/* 318 */         } catch (SQLException ex) {
/* 319 */           LOG.warn("[getLastId] Error al intentar cerrar Connection despues de obtener role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 323 */     return RoleID;
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\RoleDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */