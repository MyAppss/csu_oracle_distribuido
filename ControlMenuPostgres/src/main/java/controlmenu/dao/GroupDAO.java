package controlmenu.dao;

import controlmenu.model.GrupoAd;
import java.util.List;

public interface GroupDAO {
  void deleteGroup(int paramInt);
  
  GrupoAd getGroupId(int paramInt);
  
  GrupoAd getGroupName(String paramString);
  
  List<GrupoAd> getGroups();
  
  List<GrupoAd> getGroupsDeleteRole(int paramInt);
  
  void insertGroup(GrupoAd paramGrupoAd);
  
  void updateGroup(GrupoAd paramGrupoAd);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\GroupDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */