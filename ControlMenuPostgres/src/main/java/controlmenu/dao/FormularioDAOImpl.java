/*    */ package controlmenu.dao;
/*    */ 
/*    */ import controlmenu.model.Formulario;
/*    */ import java.sql.Connection;
/*    */ import java.sql.ResultSet;
/*    */ import java.sql.SQLException;
/*    */ import java.sql.Statement;
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FormularioDAOImpl
/*    */   implements FormularioDAO
/*    */ {
/* 17 */   private static final Logger LOG = Logger.getLogger(FormularioDAOImpl.class);
/*    */ 
/*    */ 
/*    */   
/*    */   public List<Formulario> getFormularios() {
/* 22 */     List<Formulario> list = null;
/* 23 */     Statement stmt = null;
/* 24 */     Connection conn = null;
/* 25 */     ResultSet rs = null;
/*    */     try {
/* 27 */       String sql = "SELECT * FROM formulario";
/* 28 */       LOG.debug("[getFormularios] SQL: " + sql);
/* 29 */       conn = FactoryNative.getConnection();
/* 30 */       if (conn != null) {
/* 31 */         stmt = conn.createStatement();
/* 32 */         rs = stmt.executeQuery(sql);
/* 33 */         if (rs != null) {
/* 34 */           List<Formulario> result = new ArrayList<>();
/* 35 */           while (rs.next()) {
/* 36 */             Formulario usuario = new Formulario();
/* 37 */             usuario.setFormularioId(Integer.valueOf(rs.getInt("formulario_id")));
/* 38 */             usuario.setNombre(rs.getString("nombre"));
/* 39 */             usuario.setEstado(Boolean.valueOf(rs.getBoolean("estado")));
/* 40 */             usuario.setUrl(rs.getString("url"));
/* 41 */             result.add(usuario);
/*    */           } 
/* 43 */           list = result;
/*    */         } 
/*    */       } 
/* 46 */     } catch (SQLException e3) {
/* 47 */       LOG.error("[getFormularios] Error al intentar obtener la lista de formularios  SQLException:", e3);
/*    */     } finally {
/* 49 */       if (rs != null) {
/*    */         try {
/* 51 */           rs.close();
/* 52 */         } catch (SQLException ex) {
/* 53 */           LOG.warn("[getFormularios] Error al intentar cerrar el ResultSet despues de obtener la lista de formularios por  SQLException:", ex);
/*    */         } 
/*    */       }
/* 56 */       if (stmt != null) {
/*    */         try {
/* 58 */           stmt.close();
/* 59 */         } catch (SQLException ex2) {
/* 60 */           LOG.warn("[getFormularios] Error al intentar cerrar Statement despues de obtener la lista de formularios SQLException:", ex2);
/*    */         } 
/*    */       }
/* 63 */       if (conn != null) {
/*    */         try {
/* 65 */           conn.close();
/* 66 */         } catch (SQLException ex) {
/* 67 */           LOG.warn("[getFormularios] Error al intentar cerrar Connection despues de obtener la lista de formularios SQLException:", ex);
/*    */         } 
/*    */       }
/*    */     } 
/*    */     
/* 72 */     return list;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\FormularioDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */