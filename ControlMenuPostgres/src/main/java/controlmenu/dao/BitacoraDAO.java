package controlmenu.dao;

import controlmenu.model.Bitacora;
import java.sql.Timestamp;
import java.util.List;

public interface BitacoraDAO {
  List<Bitacora> getLogWebs();
  
  void insertLogWeb(Bitacora paramBitacora);
  
  void insertLogWebBase(Timestamp paramTimestamp, String paramString1, String paramString2, String paramString3, String paramString4);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\BitacoraDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */