/*     */ package controlmenu.dao;
/*     */ 
/*     */ import controlmenu.model.GrupoAd;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GroupDAOImpl
/*     */   implements GroupDAO
/*     */ {
/*  18 */   private static final Logger LOG = Logger.getLogger(GroupDAOImpl.class);
/*     */ 
/*     */ 
/*     */   
/*     */   public GrupoAd getGroupId(int group_id) {
/*  23 */     GrupoAd grupoAd = null;
/*  24 */     Statement stmt = null;
/*  25 */     Connection conn = null;
/*  26 */     ResultSet rs = null;
/*     */     try {
/*  28 */       String sql = "SELECT * FROM grupo_ad WHERE grupo_id=" + group_id;
/*  29 */       LOG.debug("[getGroupId] SQL: " + sql);
/*  30 */       conn = FactoryNative.getConnection();
/*  31 */       if (conn != null) {
/*  32 */         stmt = conn.createStatement();
/*  33 */         rs = stmt.executeQuery(sql);
/*  34 */         if (rs != null && rs.next()) {
/*  35 */           GrupoAd result = new GrupoAd();
/*  36 */           result.setGrupoId(Integer.valueOf(rs.getInt("grupo_id")));
/*  37 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  38 */           result.setNombre(rs.getString("nombre"));
/*  39 */           result.setDetalle(rs.getString("detalle"));
/*  40 */           result.setEstado(rs.getString("ESTADO"));
/*  41 */           grupoAd = result;
/*     */         } 
/*     */       } 
/*  44 */     } catch (SQLException e3) {
/*  45 */       LOG.error("[getGroupId] Error al intentar obtener grupo con Id: " + group_id + "SQLException:", e3);
/*     */     } finally {
/*  47 */       if (stmt != null) {
/*     */         try {
/*  49 */           stmt.close();
/*  50 */         } catch (SQLException ex) {
/*  51 */           LOG.warn("[getGroupId] Error al intentar cerrar Statemental obtener al grupo con id:" + group_id + "SQLException:", ex);
/*     */         } 
/*     */       }
/*     */       
/*  55 */       if (rs != null) {
/*     */         try {
/*  57 */           rs.close();
/*  58 */         } catch (SQLException ex22) {
/*  59 */           LOG.warn("[getGroupId] Error al intentar cerrar el ResultSet grupo con Id: " + group_id + " SQLException:", ex22);
/*     */         } 
/*     */       }
/*     */       
/*  63 */       if (conn != null) {
/*     */         try {
/*  65 */           conn.close();
/*  66 */         } catch (SQLException ex) {
/*  67 */           LOG.warn("[getFormularios] Error al intentar cerrar Connection despues de obtener grupoAD SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*  71 */     return grupoAd;
/*     */   }
/*     */ 
/*     */   
/*     */   public GrupoAd getGroupName(String name) {
/*  76 */     GrupoAd grupoAd = null;
/*  77 */     Statement stmt = null;
/*  78 */     Connection conn = null;
/*  79 */     ResultSet rs = null;
/*     */     try {
/*  81 */       String sql = "SELECT * FROM grupo_ad WHERE grupo_ad.nombre='" + name + "' AND (grupo_ad.estado='true' OR grupo_ad.estado='t')";
/*  82 */       LOG.debug("[getGroupName] SQL: " + sql);
/*  83 */       conn = FactoryNative.getConnection();
/*  84 */       if (conn != null) {
/*  85 */         stmt = conn.createStatement();
/*  86 */         rs = stmt.executeQuery(sql);
/*  87 */         if (rs != null && rs.next()) {
/*  88 */           GrupoAd result = new GrupoAd();
/*  89 */           result.setGrupoId(Integer.valueOf(rs.getInt("grupo_id")));
/*  90 */           result.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  91 */           result.setNombre(rs.getString("nombre"));
/*  92 */           result.setDetalle(rs.getString("detalle"));
/*  93 */           result.setEstado(rs.getString("estado"));
/*  94 */           grupoAd = result;
/*     */         } 
/*     */       } 
/*  97 */     } catch (SQLException e2) {
/*  98 */       LOG.error("[getGroupName] Error al intentar obtener nombre del grupo  SQLException:", e2);
/*     */     } finally {
/*     */       
/* 101 */       if (stmt != null) {
/*     */         try {
/* 103 */           stmt.close();
/* 104 */         } catch (SQLException ex222) {
/* 105 */           LOG.warn("[getGroupName] Error al intentar cerrar Statemental obtener al usuario con nombre: " + name + "SQLException:", ex222);
/*     */         } 
/*     */       }
/* 108 */       if (rs != null) {
/*     */         try {
/* 110 */           rs.close();
/* 111 */         } catch (SQLException ex22) {
/* 112 */           LOG.warn("[getGroupName] Error al intentar cerrar el ResultSet usuario con nombre: " + name + " SQLException:", ex22);
/*     */         } 
/*     */       }
/*     */       
/* 116 */       if (conn != null) {
/*     */         try {
/* 118 */           conn.close();
/* 119 */         } catch (SQLException ex) {
/* 120 */           LOG.warn("[getGroupName] Error al intentar cerrar Connection despues de obtener el nombre del grupo SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 124 */     return grupoAd;
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public List<GrupoAd> getGroups() {
/* 130 */     List<GrupoAd> list = null;
/* 131 */     Statement stmt = null;
/* 132 */     Connection conn = null;
/* 133 */     ResultSet rs = null;
/*     */     try {
/* 135 */       String sql = "SELECT grupo_ad.grupo_id,grupo_ad.rol_id as rol_id,grupo_ad.nombre as nombre,grupo_ad.detalle,grupo_ad.estado as estado_grupo, rol.nombre as nombre_rol \nFROM grupo_ad INNER JOIN rol ON grupo_ad.rol_id=rol.rol_id\nWHERE grupo_ad.estado='true' OR grupo_ad.estado='t'  ORDER BY  nombre_rol";
/*     */ 
/*     */       
/* 138 */       LOG.debug("[getGroups] SQL: " + sql);
/* 139 */       conn = FactoryNative.getConnection();
/* 140 */       if (conn != null) {
/* 141 */         stmt = conn.createStatement();
/* 142 */         rs = stmt.executeQuery(sql);
/* 143 */         if (rs != null) {
/* 144 */           List<GrupoAd> result = new ArrayList<>();
/* 145 */           while (rs.next()) {
/* 146 */             GrupoAd grupo = new GrupoAd();
/* 147 */             grupo.setGrupoId(Integer.valueOf(rs.getInt("grupo_id")));
/* 148 */             grupo.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/* 149 */             grupo.setNombre(rs.getString("nombre"));
/* 150 */             grupo.setDetalle(rs.getString("detalle"));
/* 151 */             grupo.setEstado(rs.getString("estado_grupo"));
/* 152 */             grupo.setNameRole(rs.getString("nombre_rol"));
/* 153 */             result.add(grupo);
/*     */           } 
/* 155 */           list = result;
/*     */         }
/*     */       
/*     */       } 
/* 159 */     } catch (SQLException e3) {
/* 160 */       LOG.error("[getGroups] Error al intentar obtenerSQLException:", e3);
/*     */     } finally {
/* 162 */       if (rs != null) {
/*     */         try {
/* 164 */           rs.close();
/* 165 */         } catch (SQLException ex) {
/* 166 */           LOG.warn("[getGroups] Error al intentar cerrar el ResultSet SQLException:", ex);
/*     */         } 
/*     */       }
/* 169 */       if (stmt != null) {
/*     */         try {
/* 171 */           stmt.close();
/* 172 */         } catch (SQLException ex2) {
/* 173 */           LOG.warn("[getGroups] Error al intentar cerrar StatementSQLException:", ex2);
/*     */         } 
/*     */       }
/* 176 */       if (conn != null) {
/*     */         try {
/* 178 */           conn.close();
/* 179 */         } catch (SQLException ex) {
/* 180 */           LOG.warn("[getGroups] Error al intentar cerrar Connection despues de obtener la lista de grupos SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */     
/* 185 */     return list;
/*     */   }
/*     */ 
/*     */   
/*     */   public void insertGroup(GrupoAd group) {
/* 190 */     PreparedStatement pstmt = null;
/* 191 */     Connection conn = null;
/*     */     try {
/* 193 */       String sql = "INSERT INTO usuario (usuario_id,rol_id, login, nombre,estado) VALUES ( nextval('usuario_usuario_id_seq'),?,?,?,?)";
/* 194 */       LOG.debug("[insertGroup] SQL: " + sql);
/* 195 */       conn = FactoryNative.getConnection();
/* 196 */       if (conn != null) {
/* 197 */         pstmt = conn.prepareStatement(sql);
/* 198 */         pstmt.setInt(1, group.getRolId().intValue());
/* 199 */         pstmt.setString(2, group.getNombre());
/* 200 */         pstmt.setString(3, group.getDetalle());
/* 201 */         pstmt.setString(4, group.getEstado());
/* 202 */         pstmt.execute();
/*     */       } 
/* 204 */     } catch (SQLException e) {
/* 205 */       LOG.error("[insertGroup] Error al intentar adicionarSQLException:", e);
/*     */     } finally {
/* 207 */       if (pstmt != null) {
/*     */         try {
/* 209 */           pstmt.close();
/* 210 */         } catch (SQLException ex) {
/* 211 */           LOG.warn("[insertGroup] Error al intentar cerrar SQLException:", ex);
/*     */         } 
/*     */       }
/* 214 */       if (conn != null) {
/*     */         try {
/* 216 */           conn.close();
/* 217 */         } catch (SQLException ex) {
/* 218 */           LOG.warn("[insertGroup] Error al intentar cerrar Connection despues de insertar gurpo SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void updateGroup(GrupoAd group) {
/* 226 */     Connection conn = null;
/* 227 */     Statement st = null;
/*     */     try {
/* 229 */       String sql = "UPDATE grupo_ad SET rol_id =" + group.getRolId() + ", nombre ='" + group.getNombre() + "', detalle ='" + group.getDetalle() + "' WHERE grupo_id=" + group.getGrupoId();
/* 230 */       LOG.debug("[updateGroup] SQL: " + sql);
/* 231 */       conn = FactoryNative.getConnection();
/* 232 */       if (conn != null) {
/* 233 */         st = conn.createStatement();
/* 234 */         st.executeUpdate(sql);
/*     */       } 
/* 236 */     } catch (SQLException e) {
/* 237 */       LOG.warn("[updateGroup] Error al intentar actualizar al grupo con id" + group.getGrupoId() + "SQLException:", e);
/*     */     } finally {
/* 239 */       if (st != null) {
/*     */         try {
/* 241 */           st.close();
/* 242 */         } catch (SQLException ex) {
/* 243 */           LOG.warn("[updateGroup] Error al intentar cerrar Statement despues de actualizar grupo con id" + group.getGrupoId() + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 246 */       if (conn != null) {
/*     */         try {
/* 248 */           conn.close();
/* 249 */         } catch (SQLException ex) {
/* 250 */           LOG.warn("[updateGroup] Error al intentar cerrar Connection despues de actualizar grupo SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void deleteGroup(int group_id) {
/* 258 */     Connection conn = null;
/* 259 */     Statement st = null;
/*     */     try {
/* 261 */       String sql = "UPDATE grupo_ad SET estado = 'f' WHERE grupo_id=" + group_id;
/* 262 */       LOG.debug("[deleteGroup] SQL: " + sql);
/* 263 */       conn = FactoryNative.getConnection();
/* 264 */       if (conn != null) {
/* 265 */         st = conn.createStatement();
/* 266 */         st.executeUpdate(sql);
/*     */       } 
/* 268 */     } catch (SQLException e) {
/* 269 */       LOG.error("[deleteGroup] Error al intentar actualizar al grupo con id" + group_id + "SQLException:", e);
/*     */     } finally {
/* 271 */       if (st != null) {
/*     */         try {
/* 273 */           st.close();
/* 274 */         } catch (SQLException ex) {
/* 275 */           LOG.warn("[deleteGroup] Error al intentar cerrar Statement despues de actualizaral grupo con id" + group_id + ". SQLException:", ex);
/*     */         } 
/*     */       }
/*     */       
/* 279 */       if (conn != null) {
/*     */         try {
/* 281 */           conn.close();
/* 282 */         } catch (SQLException ex) {
/* 283 */           LOG.warn("[deleteGroup] Error al intentar cerrar Connection despues de eliminar grupo SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public List<GrupoAd> getGroupsDeleteRole(int idRole) {
/* 292 */     List<GrupoAd> list = null;
/* 293 */     Statement stmt = null;
/* 294 */     Connection conn = null;
/* 295 */     ResultSet rs = null;
/*     */     try {
/* 297 */       String sql = "SELECT * FROM grupo_ad WHERE (estado='true' OR estado='t') and rol_id=" + idRole;
/* 298 */       LOG.debug("[getGroupsDeleteRole] SQL: " + sql);
/* 299 */       conn = FactoryNative.getConnection();
/* 300 */       if (conn != null) {
/* 301 */         stmt = conn.createStatement();
/* 302 */         rs = stmt.executeQuery(sql);
/* 303 */         if (rs != null) {
/* 304 */           List<GrupoAd> result = new ArrayList<>();
/* 305 */           while (rs.next()) {
/* 306 */             GrupoAd grupo = new GrupoAd();
/* 307 */             grupo.setGrupoId(Integer.valueOf(rs.getInt("grupo_id")));
/* 308 */             grupo.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/* 309 */             grupo.setNombre(rs.getString("nombre"));
/* 310 */             grupo.setDetalle(rs.getString("detalle"));
/* 311 */             grupo.setEstado(rs.getString("estado"));
/* 312 */             result.add(grupo);
/*     */           } 
/* 314 */           list = result;
/*     */         } 
/*     */       } 
/* 317 */     } catch (SQLException e3) {
/* 318 */       LOG.error("[getGroupsDeleteRole] Error al intentar obtenerSQLException:", e3);
/*     */     } finally {
/* 320 */       if (rs != null) {
/*     */         try {
/* 322 */           rs.close();
/* 323 */         } catch (SQLException ex) {
/* 324 */           LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar el ResultSet SQLException:", ex);
/*     */         } 
/*     */       }
/* 327 */       if (stmt != null) {
/*     */         try {
/* 329 */           stmt.close();
/* 330 */         } catch (SQLException ex2) {
/* 331 */           LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar StatementSQLException:", ex2);
/*     */         } 
/*     */       }
/* 334 */       if (conn != null) {
/*     */         try {
/* 336 */           conn.close();
/* 337 */         } catch (SQLException ex) {
/* 338 */           LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar Connection despues de obtener eliminar gurpo por roles SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/* 342 */     return list;
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\GroupDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */