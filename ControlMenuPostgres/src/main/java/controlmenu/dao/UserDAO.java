package controlmenu.dao;

import controlmenu.model.Usuario;
import java.util.List;

public interface UserDAO {
  void deleteUser(int paramInt);
  
  Usuario getUserId(int paramInt);
  
  Usuario getUserName(String paramString);
  
  List<Usuario> getUsers();
  
  List<Usuario> getUsersDeleteRole(int paramInt);
  
  void insertUser(Usuario paramUsuario);
  
  void updateUser(Usuario paramUsuario);
}


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\UserDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */