/*    */ package controlmenu.dao;
/*    */ 
/*    */ import controlmenu.util.ParametersCM;
/*    */ import java.sql.Connection;
/*    */ import org.apache.log4j.Logger;
/*    */ import org.apache.tomcat.jdbc.pool.DataSource;
/*    */ import org.apache.tomcat.jdbc.pool.PoolConfiguration;
/*    */ import org.apache.tomcat.jdbc.pool.PoolProperties;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConexionBD
/*    */ {
/* 20 */   private static final Logger LOG = Logger.getLogger(ConexionBD.class);
/* 21 */   private static final String HOST = ParametersCM.HOST;
/* 22 */   private static final String DB = ParametersCM.DB;
/* 23 */   private static final String PORT = ParametersCM.PORT;
/* 24 */   private static final String USER = ParametersCM.USER;
/* 25 */   private static final String PASSWORD = ParametersCM.PASSWORD;
/* 26 */   public static DataSource datasource = null;
/*    */   
/*    */   public static synchronized Connection getConnection() throws Exception {
/* 29 */     if (datasource == null) {
/* 30 */       datasource = setupDataSource();
/*    */     }
/* 32 */     return datasource.getConnection();
/*    */   }
/*    */   
/*    */   private static DataSource setupDataSource() {
/* 36 */     PoolProperties p = new PoolProperties();
/* 37 */     String url = "jdbc:postgresql://" + HOST + ":" + PORT + "/" + DB;
/* 38 */     p.setUrl(url);
/* 39 */     p.setDriverClassName("org.postgresql.Driver");
/* 40 */     LOG.info(url);
/*    */     
/* 42 */     p.setUsername(USER);
/* 43 */     p.setPassword(PASSWORD);
/* 44 */     p.setJmxEnabled(true);
/* 45 */     p.setTestWhileIdle(false);
/* 46 */     p.setTestOnBorrow(true);
/* 47 */     p.setTestOnReturn(false);
/* 48 */     p.setValidationQuery("SELECT 1");
/* 49 */     p.setValidationInterval(30000L);
/* 50 */     p.setTimeBetweenEvictionRunsMillis(30000);
/* 51 */     p.setMinEvictableIdleTimeMillis(Integer.parseInt(ParametersCM.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
/* 52 */     p.setMaxActive(Integer.parseInt(ParametersCM.DB_MAX_ACTIVE));
/* 53 */     p.setMinIdle(Integer.parseInt(ParametersCM.DB_MIN_IDLE));
/* 54 */     p.setMaxIdle(Integer.parseInt(ParametersCM.DB_MAX_IDLE));
/* 55 */     p.setMaxWait(Integer.parseInt(ParametersCM.DB_MAX_WAIT));
/* 56 */     p.setRemoveAbandonedTimeout(60);
/* 57 */     p.setLogAbandoned(true);
/* 58 */     p.setRemoveAbandoned(true);
/* 59 */     p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
/*    */     
/* 61 */     datasource = new DataSource();
/* 62 */     datasource.setPoolProperties((PoolConfiguration)p);
/* 63 */     return datasource;
/*    */   }
/*    */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\ConexionBD.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */