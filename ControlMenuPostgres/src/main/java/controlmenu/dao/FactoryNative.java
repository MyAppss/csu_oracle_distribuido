/*    */ package controlmenu.dao;
/*    */ 
/*    */ import controlmenu.util.ParametersCM;
/*    */ import java.sql.Connection;
/*    */ import java.sql.SQLException;
/*    */ import org.apache.log4j.Logger;
/*    */ import org.apache.tomcat.jdbc.pool.DataSource;
/*    */ import org.apache.tomcat.jdbc.pool.PoolConfiguration;
/*    */ import org.apache.tomcat.jdbc.pool.PoolProperties;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FactoryNative
/*    */ {
/* 15 */   private static final Logger LOG = Logger.getLogger(FactoryNative.class);

/* 28 */   private static final String HOST = ParametersCM.HOST;
/* 29 */   private static final String DB = ParametersCM.DB;
/* 30 */   private static final String PORT = ParametersCM.PORT;
/* 31 */   private static final String USER = ParametersCM.USER;
/* 32 */   private static final String PASSWORD = ParametersCM.PASSWORD;
/* 33 */   public static DataSource datasource = null;
/*    */   
/*    */   public static synchronized Connection getConnection() throws SQLException {
/* 36 */     if (datasource == null) {
/* 37 */       datasource = setupDataSource();
/*    */     }
/* 39 */     return datasource.getConnection();
/*    */   }
/*    */   
/*    */   private static DataSource setupDataSource() {
/* 43 */     PoolProperties p = new PoolProperties();
/* 44 */     String url = "jdbc:postgresql://" + HOST + ":" + PORT + "/" + DB;
/* 45 */     p.setUrl(url);
/* 46 */     p.setDriverClassName("org.postgresql.Driver");
/* 47 */     LOG.info(url);
/*    */     
/* 49 */     p.setUsername(USER);
/* 50 */     p.setPassword(PASSWORD);
/* 51 */     p.setJmxEnabled(true);
/* 52 */     p.setTestWhileIdle(false);
/* 53 */     p.setTestOnBorrow(true);
/* 54 */     p.setTestOnReturn(false);
/* 55 */     p.setValidationQuery("SELECT 1");
/* 56 */     p.setValidationInterval(30000L);
/* 57 */     p.setTimeBetweenEvictionRunsMillis(30000);
/* 58 */     p.setMinEvictableIdleTimeMillis(Integer.parseInt(ParametersCM.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
/* 59 */     p.setMaxActive(Integer.parseInt(ParametersCM.DB_MAX_ACTIVE));
/* 60 */     p.setMinIdle(Integer.parseInt(ParametersCM.DB_MIN_IDLE));
/* 61 */     p.setMaxIdle(Integer.parseInt(ParametersCM.DB_MAX_IDLE));
/* 62 */     p.setMaxWait(Integer.parseInt(ParametersCM.DB_MAX_WAIT));
/* 63 */     p.setRemoveAbandonedTimeout(60);
/* 64 */     p.setLogAbandoned(true);
/* 65 */     p.setRemoveAbandoned(true);
/* 66 */     p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
/*    */     
/* 68 */     datasource = new DataSource();
/* 69 */     datasource.setPoolProperties((PoolConfiguration)p);
/* 70 */     return datasource;
/*    */   }
/*    */ }


