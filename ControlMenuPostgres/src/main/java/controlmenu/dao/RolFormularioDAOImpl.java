/*     */ package controlmenu.dao;
/*     */ 
/*     */ import controlmenu.model.RolFormulario;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RolFormularioDAOImpl
/*     */   implements RolFormularioDAO
/*     */ {
/*  18 */   private static final Logger LOG = Logger.getLogger(RolFormularioDAOImpl.class);
/*     */ 
/*     */ 
/*     */   
/*     */   public List<RolFormulario> getList(int idRol) {
/*  23 */     List<RolFormulario> list = null;
/*  24 */     Statement stmt = null;
/*  25 */     Connection conn = null;
/*  26 */     ResultSet rs = null;
/*     */     try {
/*  28 */       String sql = "SELECT rol_formulario.rol_id, rol_formulario.formulario_id as formulario,rol_formulario.estado as estado,formulario.url, formulario.nombre , formulario.depende,formulario.nivel\nfrom rol_formulario INNER JOIN formulario ON rol_formulario.formulario_id=formulario.formulario_id\nwhere rol_formulario.rol_id= " + idRol + " ORDER BY formulario.nivel";
/*     */ 
/*     */       
/*  31 */       LOG.debug("[getList] SQL: " + sql);
/*  32 */       conn = FactoryNative.getConnection();
/*  33 */       if (conn != null) {
/*  34 */         stmt = conn.createStatement();
/*  35 */         rs = stmt.executeQuery(sql);
/*  36 */         if (rs != null) {
/*  37 */           List<RolFormulario> result = new ArrayList<>();
/*  38 */           while (rs.next()) {
/*  39 */             RolFormulario grupo = new RolFormulario();
/*  40 */             grupo.setRolId(Integer.valueOf(rs.getInt("rol_id")));
/*  41 */             grupo.setFormularioId(Integer.valueOf(rs.getInt("formulario")));
/*  42 */             grupo.setEstado(rs.getString("estado"));
/*  43 */             grupo.setUrl(rs.getString("url"));
/*  44 */             grupo.setName(rs.getString("nombre"));
/*  45 */             grupo.setDepende(Integer.valueOf(rs.getInt("depende")));
/*  46 */             grupo.setNivel(rs.getString("nivel"));
/*  47 */             result.add(grupo);
/*     */           } 
/*  49 */           list = result;
/*     */         } 
/*     */       } 
/*  52 */     } catch (SQLException e3) {
/*  53 */       LOG.error("[getList] Error al intentar obtener rol con Id: " + idRol + "SQLException:", e3);
/*     */     } finally {
/*  55 */       if (rs != null) {
/*     */         try {
/*  57 */           rs.close();
/*  58 */         } catch (SQLException ex) {
/*  59 */           LOG.warn("[getList] Error al intentar cerrar el ResultSet rol con Id: " + idRol + " SQLException:", ex);
/*     */         } 
/*     */       }
/*  62 */       if (stmt != null) {
/*     */         try {
/*  64 */           stmt.close();
/*  65 */         } catch (SQLException ex2) {
/*  66 */           LOG.warn("[getList] Error al intentar cerrar Statemental obtener lista de rol con id:" + idRol + "SQLException:", ex2);
/*     */         } 
/*     */       }
/*  69 */       if (conn != null) {
/*     */         try {
/*  71 */           conn.close();
/*  72 */         } catch (SQLException ex) {
/*  73 */           LOG.warn("[getList] Error al intentar cerrar Connection despues de obtener lista de rol SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */     
/*  78 */     return list;
/*     */   }
/*     */ 
/*     */   
/*     */   public void insert(RolFormulario group) {
/*  83 */     Connection conn = null;
/*  84 */     PreparedStatement pstmt = null;
/*     */     try {
/*  86 */       String sql = "INSERT INTO rol_formulario (rol_id,formulario_id,estado) VALUES (?,?,?)";
/*  87 */       LOG.debug("[insert] SQL: " + sql);
/*  88 */       conn = FactoryNative.getConnection();
/*  89 */       if (conn != null) {
/*  90 */         pstmt = conn.prepareStatement(sql);
/*  91 */         pstmt.setInt(1, group.getRolId().intValue());
/*  92 */         pstmt.setInt(2, group.getFormularioId().intValue());
/*  93 */         pstmt.setString(3, group.getEstado());
/*  94 */         pstmt.execute();
/*     */       } 
/*  96 */     } catch (SQLException e) {
/*  97 */       LOG.error("[insert] Error al intentar adicionar a la SQLException:", e);
/*     */     } finally {
/*  99 */       if (pstmt != null) {
/*     */         try {
/* 101 */           pstmt.close();
/* 102 */         } catch (SQLException ex) {
/* 103 */           LOG.warn("[insert] Error al intentar cerrar ", ex);
/*     */         } 
/*     */       }
/* 106 */       if (conn != null) {
/*     */         try {
/* 108 */           conn.close();
/* 109 */         } catch (SQLException ex) {
/* 110 */           LOG.warn("[insert] Error al intentar cerrar Connection despues de insertar un role SQLException:", ex);
/*     */         } 
/*     */       }
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   public void update(RolFormulario group) {
/* 119 */     Connection conn = null;
/* 120 */     Statement st = null;
/*     */     try {
/* 122 */       conn = FactoryNative.getConnection();
/* 123 */       String sql = "UPDATE rol_formulario SET estado='" + group.getEstado() + "' WHERE rol_id=" + group.getRolId() + " AND formulario_id=" + group.getFormularioId();
/* 124 */       LOG.debug("[update] SQL: " + sql);
/* 125 */       if (conn != null) {
/* 126 */         st = conn.createStatement();
/* 127 */         st.executeUpdate(sql);
/*     */       } 
/* 129 */     } catch (SQLException e) {
/* 130 */       LOG.warn("[update] Error al intentar actualizar la acreditacion con id ciclo " + group.getRolId() + ". SQLException:", e);
/*     */     } finally {
/*     */       
/* 133 */       if (st != null) {
/*     */         try {
/* 135 */           st.close();
/* 136 */         } catch (SQLException ex) {
/* 137 */           LOG.warn("[update] Error al intentar cerrar Statement despues de actualizar la acreditacion con id ciclo " + group.getRolId() + ". SQLException:", ex);
/*     */         } 
/*     */       }
/* 140 */       if (conn != null)
/*     */         try {
/* 142 */           conn.close();
/* 143 */         } catch (SQLException ex) {
/* 144 */           LOG.warn("[update] Error al intentar cerrar Connection despues de actualizar un roleFormulario SQLException:", ex);
/*     */         }  
/*     */     } 
/*     */   }
/*     */ }


/* Location:              D:\Myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\dao\RolFormularioDAOImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */