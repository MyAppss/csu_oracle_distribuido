package myapps.context;

import java.util.UUID;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import myapps.activeMQ.ColaThreadMax;
import myapps.activeMQ.ConexionJMS;
import myapps.activeMQ.ThreadJms;
import myapps.cmn.util.Propiedades;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import myapps.dao.MapeoOfertasSingleton;
import myapps.pool.cws.ColaDesconectado;
import myapps.pool.cws.ConexionCcws;
import myapps.pool.cws.ConexionServiceCcws;
import myapps.pool.cws.Pool;
import myapps.pool.cws.ThreadReconexion;
import myapps.pool.cws.ThreadValidar;
import myapps.utils.PropiedadesGQL;

import org.apache.log4j.Logger;

@WebListener
public class InitContextListener implements ServletContextListener {

    public static final Logger LOG = Logger.getLogger(InitContextListener.class);
    private GestorConfiguraciones configuraciones;
    private ThreadValidar threadValidar;
    private ThreadReconexion threadReconexion;
    private Boolean iniciado = false;

    private HilosCbs cbs = new HilosCbs();

    @Override
    public void contextInitialized(ServletContextEvent ce) {
        iniciado = true;
        LOG.info("...::: INICIO :::...");
        LOG.info("...::: CARGAR PARAMETROS - CONFIGURACIONES :::...");
        this.configuraciones = GestorConfiguraciones.getInstance();
        LOG.info("...::: FIN CARGAR PARAMETROS - CONFIGURACIONES :::...");
        LOG.info("");

        if (Propiedades.GQL_BANDERA) {
            LOG.info("...::: CARGANDO PROPIEDADES PARA EL CONSUMO DE GQL :::...");
            PropiedadesGQL.setGqlTIMEOUT(Propiedades.GQL_TIMEOUT);
            PropiedadesGQL.setGqlDATEFORMAT(Propiedades.GQL_DATEFORMAT);
            PropiedadesGQL.setGqlENDPOINT(Propiedades.GQL_ENDPOINT);
            PropiedadesGQL.setGqlMESSAGE(Propiedades.GQL_MESSAGE);
            PropiedadesGQL.setGqlFECHAMINIMA(Propiedades.GQL_FECHAMINIMA);
            String url = "jdbc:oracle:thin:@" + Propiedades.BD_SERVER + ":" + Propiedades.BD_PORT + "/" + Propiedades.BD_NAME;
            PropiedadesGQL.setUrlBD(url);
            PropiedadesGQL.setUsuarioBD(Propiedades.DB_USER);
            PropiedadesGQL.setPasswordBD(Propiedades.DB_PASSWORD);
            PropiedadesGQL.setSetminevictableidletimemillisBD(Integer.parseInt(Propiedades.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
            PropiedadesGQL.setSetmaxactiveBD(Integer.parseInt(Propiedades.DB_MAX_ACTIVE) / 10);
            PropiedadesGQL.setSetminidleBD(Integer.parseInt(Propiedades.DB_MIN_IDLE) / 10);
            PropiedadesGQL.setSetMaxIdleBD(Integer.parseInt(Propiedades.DB_MAX_IDLE) / 10);
            PropiedadesGQL.setSetMaxWaitBD(Integer.parseInt(Propiedades.DB_MAX_WAIT));

            PropiedadesGQL.setGqlBOB(Propiedades.GQL_BOB);
            PropiedadesGQL.setGqlKB(Propiedades.GQL_KB);
            PropiedadesGQL.setPrecioKB(Propiedades.GQL_PrecioKB);
            PropiedadesGQL.setGqlItem(Propiedades.GQL_Item);
            PropiedadesGQL.setGqlDecimales(Propiedades.GQL_Decimales);
            PropiedadesGQL.setGqlNofound(Propiedades.GQL_NoFound);
            PropiedadesGQL.setGqlChilProdutActivo(Propiedades.GQL_CPACTIVO);
            PropiedadesGQL.setGqlChilProdutSuspendido(Propiedades.GQL_CPSUSPENDIDO);
            PropiedadesGQL.setGqlTYPE(Propiedades.GQL_TYPE);
            PropiedadesGQL.setImprimirBilleteras(Propiedades.GQL_IMPRIMIR_BILLETERAS);
            PropiedadesGQL.setNombreReseva(Propiedades.GQL_NOMBRE_RESERVA);
            PropiedadesGQL.setFechaImprimirBIlleteras(Propiedades.GQL_FORMATOFECHA_IB);
            MapeoOfertasSingleton.getInstance();
            LOG.info("...::: FIN DE LA CARGA DE PROPIEDADES PARA EL CONSUMO DE GQL :::...");
        } else {
            LOG.info("...::: INICIAR A CREAR CONEXIONES :::...");
            crearConexiones();
            iniciarThread();
            if (Boolean.TRUE.equals(Propiedades.VM_BANDERA)) {
                LOG.info("Bandera Vigencia Maxima Activada");
                cbs.crearConexiones();
            }
            LOG.info("...::: FIN DE CREAR CONEXIONES :::...");
            LOG.info("");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent ce) {
        if (Boolean.TRUE.equals(Propiedades.VM_BANDERA)) {
            LOG.info("Bandera Vigencia Maxima Activada");
            cbs.cerrarConexiones();
        }

        if (!Propiedades.GQL_BANDERA) {
            LOG.info("...::: CERRANDO CONEXIONES :::...");
            cerrarConexiones();
            LOG.info(" ");
        }
        LOG.info("...::: LIMPIANDO LOS PARAMETROS DE CONFIGURACIONES :::...");
        if (this.configuraciones != null) {
            this.configuraciones = null;
            LOG.info("...::: DESTRUIR LAS CONFIGURACIONES :::...");
        }
        iniciado = false;
        ConexionJMS.getInstance().cerrarConexion();
        LOG.info("...::: FIN :::...");
    }

    private void crearConexiones() {
        try {
            if (Pool.size() == 0) {
                ConexionServiceCcws cc = new ConexionServiceCcws();
                int poolSize = Propiedades.cantidadConexionsPool;

                for (int i = 1; i <= poolSize; i++) {
                    ConexionCcws con = cc.crearConexionCcws(i);
                    if (con != null) {
                        LOG.info("Conexion creada satisfactoriamente id CCWS Services: " + i);
                        boolean put = Pool.put(con);
                        if (put) {
                            Pool.addCantidadRealConexiones();
                        } else {
                            LOG.info("Falla al encolar la conexion id CCWS Services: " + i);
                            cc.cerrarConexion(con);
                        }
                    } else {
                        LOG.info("Conexion fallida id CCWS Services: " + i);
                        ColaDesconectado.put(i);
                    }
                }
                this.threadValidar = new ThreadValidar();
                this.threadValidar.start();
                threadReconexion = new ThreadReconexion();
                threadReconexion.start();
            }
        } catch (Exception e) {
            LOG.error("Error al iniciar contexto: ", e);
        }
    }

    private void cerrarConexiones() {
        try {
            if (threadReconexion != null) {
                threadReconexion.setLive(false);
            }

            if (threadValidar != null) {
                threadValidar.setLive(false);
            }

            LOG.info("CANTIDAD CONEXION CCWS SERVICES EN EL POOL : " + Pool.size());
            LOG.info("TAMANHO DE LA COLA : " + ColaDesconectado.size());
            LOG.info("CANTIDAD REAL DE CCWS CONEXIONES: " + Pool.getCantidadRealConexiones());

            ConexionServiceCcws cc = new ConexionServiceCcws();
            ConexionCcws conexion = Pool.poll();

            int contador = 0;
            int contadorLimite = 0;
            int limite = 1000;

            while ((conexion != null || contador < Pool.getCantidadRealConexiones()) && contadorLimite < limite) {
                if (conexion != null) {
                    cc.cerrarConexion(conexion);
                    contador++;
                } else {
                    try {
                        Thread.sleep(Propiedades.dormirHiloAlNoCerrarConexion);
                    } catch (InterruptedException e) {
                        LOG.warn("[cerrarConexiones] ERROR SPLEEP CCWSSERVICES : " + e.getMessage());
                        Thread.currentThread().interrupt();
                    }
                }
                contadorLimite++;
                conexion = Pool.poll();
            }
        } catch (Exception e) {
            LOG.error("Error al destruir contexto: ", e);
        }
    }

    public GestorConfiguraciones getConfiguraciones() {
        return configuraciones;
    }

    public void setConfiguraciones(GestorConfiguraciones configuraciones) {
        this.configuraciones = configuraciones;
    }

    public ThreadValidar getThreadValidar() {
        return threadValidar;
    }

    public void setThreadValidar(ThreadValidar threadValidar) {
        this.threadValidar = threadValidar;
    }

    public ThreadReconexion getThreadReconexion() {
        return threadReconexion;
    }

    public void setThreadReconexion(ThreadReconexion threadReconexion) {
        this.threadReconexion = threadReconexion;
    }

    public Boolean getIniciado() {
        return iniciado;
    }

    public void setIniciado(Boolean iniciado) {
        this.iniciado = iniciado;
    }

    public void iniciarThread() {
        for (int i = 1; i <= Propiedades.MQ_MAX_THREAD; i++) {
            ColaThreadMax.put(i);
        }
        LOG.info("Se  cartgaron " + Propiedades.MQ_MAX_THREAD + " elementos a la cola");

        LOG.info("Iniciando Hilos ThreadJMS: " + Propiedades.MQ_HILOS + " Nro Hilos");
        for (int i = 1; i <= Propiedades.MQ_HILOS; i++) {
            ThreadJms jms = new ThreadJms(i, UUID.randomUUID().toString(), this);
            jms.start();
        }
    }

}
