/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.context;

import java.io.Serializable;
import myapps.cmn.util.Propiedades;
import myapps.pool.cbs.ColaDesconectadoCbs;
import myapps.pool.cbs.ConexionCbs;
import myapps.pool.cbs.GestorConexionCbs;
import myapps.pool.cbs.PoolCbs;
import myapps.pool.cbs.ThreadReconexionCbs;
import myapps.pool.cbs.ThreadValidarCbs;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class HilosCbs implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(HilosCbs.class);

    private transient ThreadValidarCbs threadValidar;
    private transient ThreadReconexionCbs threadReconexion;

    protected void crearConexiones() {
        try {
            if (PoolCbs.size() == 0) {
                GestorConexionCbs gestorConexion = new GestorConexionCbs();
                int poolSize = Propiedades.SOAP_CBS_CANTIDADCONEXIONSPOOL;

                for (int i = 1; i <= poolSize; i++) {
                    ConexionCbs conexion = gestorConexion.crearConexion(i);
                    if (conexion != null) {
                        LOG.info("Conexion creada satisfactoriamente id CBS_SERVICES Services: " + i);
                        boolean put = PoolCbs.put(conexion);
                        if (Boolean.TRUE.equals(put)) {
                            PoolCbs.addCantidadRealConexiones();
                        } else {
                            LOG.info("Falla al encolar la conexion id CBS_SERVICES Services: " + i);
                            gestorConexion.cerrarConexion(conexion);
                        }
                    } else {
                        LOG.info("Conexion fallida id CBS_SERVICES Services: " + i);
                        ColaDesconectadoCbs.put(i);
                    }
                }

                if (Propiedades.SOAP_CBS_HILO_VALIDAR) {
                    LOG.debug("Habilitando Hilo Validar Conexion");
                    this.threadValidar = new ThreadValidarCbs();
                    this.threadValidar.start();
                }
                if (Propiedades.SOAP_CBS_HILO_RECONECTAR) {
                    LOG.debug("Habilitando Hilo Reconectar");
                    threadReconexion = new ThreadReconexionCbs();
                    threadReconexion.start();
                }
            }
        } catch (Exception e) {
            LOG.error("Error al iniciar contexto: ", e);
        }
    }

    protected void cerrarConexiones() {
        try {
            if (threadReconexion != null) {
                threadReconexion.setLive(false);
            }

            if (threadValidar != null) {
                threadValidar.setLive(false);
            }

            LOG.info("CANTIDAD CONEXION CBS_SERVICES SERVICES EN EL POOL : " + PoolCbs.size());
            LOG.info("TAMANHO DE LA COLA : " + ColaDesconectadoCbs.size());
            LOG.info("CANTIDAD REAL DE CBS_SERVICES CONEXIONES: " + PoolCbs.getCantidadRealConexiones());

            GestorConexionCbs gestorConexion = new GestorConexionCbs();
            ConexionCbs conexion = PoolCbs.poll();

            int contador = 0;
            int contadorLimite = 0;
            int limite = 1000;

            while ((conexion != null || contador < PoolCbs.getCantidadRealConexiones()) && contadorLimite < limite) {
                if (conexion != null) {
                    gestorConexion.cerrarConexion(conexion);
                    contador++;
                } else {
                    try {
                        Thread.sleep(Propiedades.SOAP_CBS_DORMIRHILOALNOCERRARCONEXION);
                    } catch (InterruptedException e) {
                        LOG.warn("[cerrarConexiones] ERROR SPLEEP CBS_SERVICES : " + e.getMessage(), e);
                        Thread.currentThread().interrupt();
                    }
                }
                contadorLimite++;
                conexion = PoolCbs.poll();
            }
        } catch (Exception e) {
            LOG.error("Error al destruir contexto: ", e);
        }
    }

    public ThreadValidarCbs getThreadValidar() {
        return threadValidar;
    }

    public void setThreadValidar(ThreadValidarCbs threadValidar) {
        this.threadValidar = threadValidar;
    }

    public ThreadReconexionCbs getThreadReconexion() {
        return threadReconexion;
    }

    public void setThreadReconexion(ThreadReconexionCbs threadReconexion) {
        this.threadReconexion = threadReconexion;
    }
}
