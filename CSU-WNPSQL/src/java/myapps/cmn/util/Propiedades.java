package myapps.cmn.util;

import java.util.Properties;
import java.util.ResourceBundle;

public class Propiedades {

    // Lee los datos del Archivo de configuración
    protected final static String CONFIG_FILE = "myapps.properties.CSU";
    protected static final Properties prop = new Properties();
    // Datos para Conexion a la BD.
    // public static final String DB_URL;

    public static String BD_SERVER;
    public static String BD_NAME;
    public static int BD_PORT;

    public static final String DB_USER;
    public static final String DB_PASSWORD;
    public static final String CCWS_HOST;
    public static final String CCWS_USER;
    public static final String CCWS_PASS;
    public static final String CCWS_MODO_PRUEBA;
    //public static final List<String> ListaCodesErrorCMV;
    public static final String DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS;
    public static final String DB_MAX_ACTIVE;
    public static final String DB_MIN_IDLE;
    public static final String DB_MAX_IDLE;
    public static final String DB_MAX_WAIT;
    public static final String DEFAUL_ERROR_CONEXION;
    public static final String DEFAUL_CODIGO_CONEXION;
    public static final String SHORT;
    public static final String CHANNEL;

    public static final String SEPARADOR_INDICE_MENU;
    public static final String TEXTO_NOMBRE_BILLETERA;
    public static final String TEXTO_DESCRIPCION;
    public static final String TEXTO_SALDO_PAQUETIGOS_VIGENTE;
    public static final String TEXTO_SALDO_PAQUETIGOS_FORMATO_FECHA;
    public static final String TEXTO_PAQUETIGO_ACUMULADOS_NO_VIGENTE;
    public static final String TEXTO_PAQUETIGOS_ACUMULADOS_MOSTRAR_FECHA_EXPIRACION;
    public static final String TEXTO_PAQUETIGOS_ACUMULADOS_FECHA_EXPIRACION;
    public static final String textoPaquetigosAcumuladosFormatoFecha;
    public static final String textoVenceOtrosSaldosMostrarFechaExpiracion;
    public static final String textoVenceOtrosSaldosFechaExpiracion;
    public static final String textoVenceOtrosSaldosFormatoFecha;
    public static final String TEXTO_CURRENCY;
    public static final String TEXTO_SEGUNDO;

    public static final String textoVenceVigenteMostrarFechaExpiracion;
    public static final String textoVenceVigenteFechaExpiracion;
    public static final String ccwsConnectionTimeout;
    public static final String ccwsReceiveTimeout;

    public static final Integer cantidadConexionsPool;
    public static final long dormirHiloAlNoCerrarConexion;
    public static final long tareaValidarConexionMilisegundos;
    public static final long tareaReconexionMilisegundos;
    public static final long timeoutConexion;
    public static final boolean realizarValidacion;
    public static final boolean conexionPorIP;

    public static final String GQL_ENDPOINT;
    public static final String GQL_MESSAGE;
    public static final String GQL_DATEFORMAT;
    public static final Integer GQL_TIMEOUT;
    public static final boolean GQL_BANDERA;
    public static final String GQL_FECHAMINIMA;
    public static final Double GQL_BOB;
    public static final Double GQL_KB;
    public static final Double GQL_PrecioKB;
    public static final Double GQL_Item;
    public static final Integer GQL_Decimales;
    public static final String GQL_NoFound;
    public static final String GQL_TYPE;
    public static final String GQL_CPACTIVO;
    public static final String GQL_CPSUSPENDIDO;
    public static final String GQL_RESERVA;
    public static final boolean GQL_IMPRIMIR_BILLETERAS;
    public static final String GQL_NOMBRE_RESERVA;
    public static final String GQL_FORMATOFECHA_IB;

    public static String MQ_USER;
    public static String MQ_PASSWORD;
    public static String MQ_COLA;
    public static String MQ_IP;
    public static int MQ_PUERTO;
    public static String MQ_TIPO_CONEXION;
    public static int MQ_TIMEOUT;
    public static int MQ_HILOS;
    public static long MQ_HILOS_SLEEP;
    public static int MQ_MAX_THREAD;

    public static boolean BANDERA1_MONTO_MINIMO;
    public static boolean BANDERA2_MONTO_MINIMO;
    public static boolean BANDERA3_MONTO_MINIMO;
    public static String UNIT_TYPE_MONTO_MINIMO;

    public static String PATTERN_TELEFONO;
    public static String PATTERN_SESSION;
    public static String PATTERN_CHANNEL;
    public static String PATTERN_SHORT;

    public static String VALOR;
 //   public static String MY_EXPRESION;

    public static boolean VM_BANDERA;
    //CBS

    public static String SOAP_CBS_HOST;
    public static Integer SOAP_CBS_CONNECTIONTIMEOUT;
    public static Integer SOAP_CBS_CANTIDADCONEXIONSPOOL;
    public static long SOAP_CBS_DORMIRHILOALNOCERRARCONEXION;
    public static long SOAP_CBS_TAREAVALIDARCONEXIONMILISEGUNDOS;
    public static long SOAP_CBS_TAREARECONEXIONMILISEGUNDOS;
    public static long SOAP_CBS_TIMEOUTCONEXION;
    public static boolean SOAP_CBS_REALIZARVALIDACION;

    public static String SOAP_CBS_NOMBRESERVICIO;
    public static String SOAP_CBS_CODIGOS_ACEPTADOS;
    public static String SOAP_CBS_USERNAME;
    public static String SOAP_CBS_PASSWORD;
    public static String SOAP_CBS_VERSION;
    public static String SOAP_CBS_BEID;

    public static String SOAP_CBS_PREFIJO_MSISDN;
    public static Boolean SOAP_CBS_HILO_VALIDAR;
    public static Boolean SOAP_CBS_HILO_RECONECTAR;
    public static Boolean SOAP_CBS_URL_TEST;

    public static String VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO;
    public static String VM_CBS_EXPIRE_TIME_FORMAT;
    public static String VM_CBS_EXPIRE_TIME_FORMAT_STRING;
    public static String VM_CBS_BILLETERA_MODIFICAR;

    public static boolean VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONTODOS;
    public static boolean VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_CONMEGAS;
    public static boolean VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONDEMAND;
    public static String VIGENCIA_MAXIMA_FECHA_CABECERAS_MENSAJE;
    public static String VIGENCIA_MAXIMA_FECHA_CABECERAS_FORMATO;

    static {

        ResourceBundle rb = ResourceBundle.getBundle(CONFIG_FILE);

        cantidadConexionsPool = Integer.parseInt(rb.getString("CANTIDAD.CONEXIONES.POOL"));
        dormirHiloAlNoCerrarConexion = Long.parseLong(rb.getString("DORMIR.HILO.AL.NO.CERRAR.CONEXION"));
        tareaValidarConexionMilisegundos = Long.parseLong(rb.getString("TAREA.VALIDAR.CONEXION.MILISEGUNDOS"));
        tareaReconexionMilisegundos = Long.parseLong(rb.getString("TAREA.RECONEXION.MILISEGUNDOS"));
        timeoutConexion = Long.parseLong(rb.getString("TIMEOUT.CONEXION"));
        realizarValidacion = Boolean.parseBoolean(rb.getString("REALIZAR.VALIDACION"));
        conexionPorIP = Boolean.parseBoolean(rb.getString("CONEXION.POR.IP"));

        //DB_URL = rb.getString("bd.url");
        BD_SERVER = rb.getString("bd.servidor");
        BD_NAME = rb.getString("bd.nombre");
        BD_PORT = Integer.parseInt(rb.getString("bd.puerto"));
        DB_USER = rb.getString("bd.usuario");
        DB_PASSWORD = rb.getString("bd.contrasenna");

        CCWS_HOST = rb.getString("ccws.servidor");
        CCWS_USER = rb.getString("ccws.usuario");
        CCWS_PASS = rb.getString("ccws.contrasenna");
        CCWS_MODO_PRUEBA = rb.getString("ccws.modoprueba");
        ccwsConnectionTimeout = rb.getString("ccws.connectionTimeout");
        ccwsReceiveTimeout = rb.getString("ccws.receiveTimeout");

        SHORT = rb.getString("cs.shortDefault");
        CHANNEL = rb.getString("cs.channelDefault");

        DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = rb.getString("bd.setminevictableidletimemillis");
        DB_MAX_ACTIVE = rb.getString("bd.setmaxactive");
        DB_MIN_IDLE = rb.getString("bd.setminidle");
        DB_MAX_IDLE = rb.getString("bd.setMaxIdle");
        DB_MAX_WAIT = rb.getString("bd.setMaxWait");

        SEPARADOR_INDICE_MENU = rb.getString("separador.indice.menu");
        TEXTO_NOMBRE_BILLETERA = rb.getString("text.nombreBilletera");
        TEXTO_DESCRIPCION = rb.getString("texto.descripcion");
        TEXTO_SALDO_PAQUETIGOS_VIGENTE = rb.getString("texto.saldo.paquetigos.vigente");
        TEXTO_SALDO_PAQUETIGOS_FORMATO_FECHA = rb.getString("texto.saldo.paquetigos.formato.fecha");
        TEXTO_PAQUETIGO_ACUMULADOS_NO_VIGENTE = rb.getString("texto.paquetigos.acumulados.no.vigente");
        TEXTO_PAQUETIGOS_ACUMULADOS_MOSTRAR_FECHA_EXPIRACION = rb.getString("texto.paquetigos.acumulados.mostrar.fecha.expiracion");
        TEXTO_PAQUETIGOS_ACUMULADOS_FECHA_EXPIRACION = rb.getString("texto.paquetigos.acumulados.fecha.expiracion");
        textoPaquetigosAcumuladosFormatoFecha = rb.getString("texto.paquetigos.acumulados.formato.fecha");
        textoVenceOtrosSaldosMostrarFechaExpiracion = rb.getString("texto.vence.otros.saldos.mostrar.fecha.expiracion");
        textoVenceOtrosSaldosFechaExpiracion = rb.getString("texto.vence.otros.saldos.fecha.expiracion");
        textoVenceOtrosSaldosFormatoFecha = rb.getString("texto.vence.otros.saldos.formato.fecha");

        textoVenceVigenteMostrarFechaExpiracion = rb.getString("texto.vence.vigente.saldos.mostrar.fecha.expiracion");
        textoVenceVigenteFechaExpiracion = rb.getString("texto.vence.vigente.saldos.fecha.expiracion");

        TEXTO_CURRENCY = rb.getString("texto.moneda");
        TEXTO_SEGUNDO = rb.getString("texto.tiempo");

        DEFAUL_ERROR_CONEXION = rb.getString("default.respuesta.falla.servicio");
        DEFAUL_CODIGO_CONEXION = rb.getString("default.codigo.falla.servicio");

        GQL_ENDPOINT = rb.getString("GQL.ENDPOINT");
        GQL_MESSAGE = rb.getString("GQL.MESSAGE");
        GQL_DATEFORMAT = rb.getString("GQL.DATEFORMAT");
        GQL_TIMEOUT = Integer.parseInt(rb.getString("GQL.TIMEOUT"));
        GQL_BANDERA = Boolean.parseBoolean(rb.getString("GQL.BANDERA"));
        GQL_FECHAMINIMA = rb.getString("GQL.FECHAMINIMA");
        GQL_BOB = Double.parseDouble(rb.getString("GQL.BOB"));
        GQL_KB = Double.parseDouble(rb.getString("GQL.KB"));
        GQL_PrecioKB = Double.parseDouble(rb.getString("GQL.PrecioKB"));
        GQL_Item = Double.parseDouble(rb.getString("GQL.Item"));
        GQL_Decimales = Integer.parseInt(rb.getString("GQL.Decimales"));
        GQL_NoFound = rb.getString("GQL.NOFOUND");
        GQL_TYPE = rb.getString("GQL.TYPE");
        GQL_CPACTIVO = rb.getString("GQL.ChildProductACTIVO");
        GQL_CPSUSPENDIDO = rb.getString("GQL.ChildProductSUSPENDIDO");
        GQL_RESERVA = rb.getString("GQL.RESERVA");
        GQL_IMPRIMIR_BILLETERAS = Boolean.parseBoolean(rb.getString("GQL.imprimirBilleteras"));
        GQL_NOMBRE_RESERVA = rb.getString("GQL.RESERVA");
        GQL_FORMATOFECHA_IB = rb.getString("GQL.FechaimprimirBilleteras");

        MQ_USER = rb.getString("MQ.user");
        MQ_PASSWORD = rb.getString("MQ.password");
        MQ_COLA = rb.getString("MQ.nombreCola");
        MQ_IP = rb.getString("MQ.servidorIP");
        MQ_PUERTO = Integer.parseInt(rb.getString("MQ.servidorPuerto"));
        MQ_TIPO_CONEXION = rb.getString("MQ.tipoCOnexion");
        MQ_TIMEOUT = Integer.parseInt(rb.getString("MQ.timeout"));
        MQ_HILOS = Integer.parseInt(rb.getString("MQ.hilos"));
        MQ_HILOS_SLEEP = Long.parseLong(rb.getString("MQ.hilos.sleep"));
        MQ_MAX_THREAD = Integer.parseInt(rb.getString("MQ.maxThread"));

        BANDERA1_MONTO_MINIMO = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.pantalla1"));
        BANDERA2_MONTO_MINIMO = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.pantalla2"));
        BANDERA3_MONTO_MINIMO = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.pantalla3"));
        UNIT_TYPE_MONTO_MINIMO = rb.getString("unitType.montoMinimo");

        PATTERN_TELEFONO = rb.getString("PATTERN.TELEFONO");
        PATTERN_SESSION = rb.getString("PATTERN.SESSION");
        PATTERN_CHANNEL = rb.getString("PATTERN.CHANNEL");
        PATTERN_SHORT = rb.getString("PATTERN.SHORT");
        VALOR = rb.getString("VALOR");
        //MY_EXPRESION = rb.getString("SPLIT");

        VM_BANDERA = Boolean.parseBoolean(rb.getString("VM.CBS.BANDERA"));
        SOAP_CBS_HOST = rb.getString("VM.SOAP.CBS.SERVIDOR");
        SOAP_CBS_CONNECTIONTIMEOUT = Integer.parseInt(rb.getString("VM.SOAP.CBS.CONNECTION.TIMEOUT"));

        SOAP_CBS_TIMEOUTCONEXION = Long.parseLong(rb.getString("VM.SOAP.CBS.TIMEOUT.CONEXION"));
        SOAP_CBS_CANTIDADCONEXIONSPOOL = Integer.parseInt(rb.getString("VM.SOAP.CBS.CANTIDAD.CONEXIONES.POOL"));
        SOAP_CBS_DORMIRHILOALNOCERRARCONEXION = Long.parseLong(rb.getString("VM.SOAP.CBS.DORMIR.HILO.AL.NO.CERRAR.CONEXION"));
        SOAP_CBS_TAREAVALIDARCONEXIONMILISEGUNDOS = Long.parseLong(rb.getString("VM.SOAP.CBS.TAREA.RECONEXION.MILISEGUNDOS"));
        SOAP_CBS_TAREARECONEXIONMILISEGUNDOS = Long.parseLong(rb.getString("VM.SOAP.CBS.TAREA.VALIDAR.CONEXION.MILISEGUNDOS"));

        SOAP_CBS_REALIZARVALIDACION = Boolean.parseBoolean(rb.getString("VM.SOAP.CBS.REALIZAR.VALIDACION"));
        SOAP_CBS_NOMBRESERVICIO = rb.getString("VM.SOAP.CBS.NOMBRE.SERVICIO");

        SOAP_CBS_VERSION = rb.getString("VM.SOAP.CBS.REQUEST.VERSION");
        SOAP_CBS_BEID = rb.getString("VM.SOAP.CBS.REQUEST.BEID");
        SOAP_CBS_USERNAME = rb.getString("VM.SOAP.CBS.REQUEST.LOGIN");
        SOAP_CBS_PASSWORD = rb.getString("VM.SOAP.CBS.REQUEST.PASSWORD");
        SOAP_CBS_PREFIJO_MSISDN = rb.getString("VM.SOAP.CBS.REQUEST.PREFIJO.MSISDN");

        SOAP_CBS_CODIGOS_ACEPTADOS = rb.getString("VM.SOAP.CBS.CODIGOS.ACEPTADOS");

        SOAP_CBS_HILO_VALIDAR = Boolean.parseBoolean(rb.getString("VM.SOAP.CBS.HILO.VALIDAR"));
        SOAP_CBS_HILO_RECONECTAR = Boolean.parseBoolean(rb.getString("VM.SOAP.CBS.HILO.RECONECTAR"));
        SOAP_CBS_URL_TEST = Boolean.parseBoolean(rb.getString("VM.SOAP.CBS.URL.TEST"));

        VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO = rb.getString("VM.CBS.LIFE.CYCLE.STATUS.NAME");
        VM_CBS_EXPIRE_TIME_FORMAT = rb.getString("VM.CBS.EXPIRE.TIME.FORMAT");
        VM_CBS_EXPIRE_TIME_FORMAT_STRING = rb.getString("VM.CBS.EXPIRE.TIME.FORMAT.STRING");
        VM_CBS_BILLETERA_MODIFICAR = rb.getString("VM.CBS.BILLETERA.MODIFICAR");

        VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONTODOS = Boolean.parseBoolean(rb.getString("VM.FECHA.CABECERAS.BANDERA.ONTODOS"));
        VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_CONMEGAS = Boolean.parseBoolean(rb.getString("VM.FECHA.CABECERAS.BANDERA.CONMEGAS"));
        VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONDEMAND = Boolean.parseBoolean(rb.getString("VM.FECHA.CABECERAS.BANDERA.ONDEMAND"));
        VIGENCIA_MAXIMA_FECHA_CABECERAS_MENSAJE = rb.getString("VM.FECHA.CABECERAS.MENSAJE");
        VIGENCIA_MAXIMA_FECHA_CABECERAS_FORMATO = rb.getString("VM.FECHA.CABECERAS.FORMATO");
    }

}
