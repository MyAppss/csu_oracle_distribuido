/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import myapps.cmn.vo.ReporteConsulta;
import org.apache.log4j.Logger;

/**
 *
 * @author MyApps
 */
public class UtilUrl implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final Logger LOG = Logger.getLogger(UtilUrl.class);

    public static String getIp(String dir) throws Exception {
        // log.debug("dir input: " + dir);
        String response;
        URL url = new URL(dir);
        InetAddress inetAddress = java.net.InetAddress.getByName(url.getHost());
        String ip = inetAddress.getHostAddress();
        response = dir.replace(url.getHost(), ip);
        // log.debug("dir output: " + response);
        return response;
    }

    public static synchronized String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = (String) request.getSession().getAttribute("TEMP$IP_CLIENT");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//				 ip = request.getRemoteAddr();
            }
        }
        return ip;
    }

    public static boolean esTextoValido(String valor, String expresionRegular) {
        Pattern pattern = Pattern.compile(expresionRegular);
        Matcher matcher = pattern.matcher(valor);
        return matcher.find();
    }

    public static String obtenerAddres(String cadena) {
        String resultado;
        int uno = cadena.indexOf("@") + 1;
        int dos = cadena.indexOf("/");
        String aux = cadena.substring(uno, dos);
        int tres = aux.indexOf(":");
        resultado = aux.substring(0, tres);
        return resultado;
    }

    public static boolean probarConexion(String url, int puerto) {
        boolean valor = false;
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(url, puerto), 500);
            valor = socket.isConnected();
            socket.close();
        } catch (IOException e) {
            LOG.error("[probarConexion] No se pudo establecer conexion hacia " + url + ":" + puerto);
            LOG.error("[probarConexion] IOExceptiont: " + e.getMessage(), e);
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                LOG.error("[probarConexion] Error al cerrar sSocket: " + ex.getMessage(), ex);
            }
        }

        return valor;
    }

    public boolean probarServicios() {
        int valor = 0;
        if (probarConexion(Propiedades.MQ_IP, Propiedades.MQ_PUERTO)) {
            LOG.debug("Conexion Exitosa a Servidor JMS");
            valor++;
        } else {
            LOG.error("No hay Conexion hacia Servidor JMS " + Propiedades.MQ_IP + Propiedades.MQ_PUERTO);

        }
        if (probarConexion(Propiedades.BD_SERVER, Propiedades.BD_PORT)) {
            LOG.debug("Conexion Exitosa a Servidor de Base de Datos");
            valor++;
        } else {
            LOG.error("No hay Conexion hacia Servidor de Base de Datos " + Propiedades.BD_SERVER + Propiedades.BD_PORT);

        }

        return valor == 2;
    }

    public static String maptoString(HashMap<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, Object> mapa;
        try {
            if (map != null) {
                if (map.containsKey("CONSULTA")) {
                    ReporteConsulta rc = (ReporteConsulta) map.get("CONSULTA");
                    stringBuilder.append(rc.toString());
                } else {
                    mapa = map;
                    mapa.entrySet().forEach((entry) -> {
                        stringBuilder.append(entry.getKey()).append(" = ").append(entry.getValue()).append("; ");
                    });
                }

            }
        } catch (Exception e) {
            LOG.error("Exception: " + e.getMessage(), e);
        }

        return stringBuilder.toString();
    }

    /*  public static boolean validarMontoMinimo(boolean bandera, WalletComverse montoBilletera, Billetera montoMinimo, String logPrefijo) {
        boolean valor = true;
        if (bandera && montoBilletera!=null && montoMinimo!=null && montoMinimo.getPrefijoUnidad().equals("MB")) {
            LOG.info(logPrefijo + " Se validara el monto minimo para billetera: " + montoBilletera.toString() + " configuracion: " + montoMinimo);
            valor = montoBilletera.getAvailableBalance() > montoMinimo.getMontoMinimo();
        }
        return valor;
    }*/
}
