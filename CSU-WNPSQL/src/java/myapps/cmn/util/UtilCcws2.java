/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.ConfiguracionNombres;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class UtilCcws2 {

    private static final Logger LOG = Logger.getLogger(UtilCcws2.class);

    public static Menu obtenerMenu(Config actualConfig, int menuId) {
        if (actualConfig != null) {
            List<Menu> listMenu = actualConfig.getListMenus();
            if (listMenu != null) {
                for (Menu menu : listMenu) {
                    if (menu.getId() == menuId) {
                        return menu;
                    }
                }
            }
        }
        return null;
    }

    public static WalletComverse getWalletCmvAndMarcar(String nameWallet, List<WalletComverse> listWalletCMV) {
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    if (walletComverse.getNameWallet().equals(nameWallet)) {
                        walletComverse.setUsada(true);
                        return walletComverse;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public static double obtenerValorAvailableBalance(Billetera billeteraReservada, List<WalletComverse> listWalletCMV) {
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    if (walletComverse.getNameWallet().equals(billeteraReservada.getNombreComverse())) {

                        return walletComverse.getAvailableBalance();
                    }
                }
            }
        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public static double obtenerValorReserva(Billetera billetera, List<Billetera> billeteraReservada, List<WalletComverse> listWalletCMV) {
        try {
            if (billeteraReservada != null) {
                for (Billetera billeteraRerved : billeteraReservada) {
                    if (billeteraRerved.getNombreComverse().contains(billetera.getNombreComverse())) {
                        return obtenerValorAvailableBalance(billeteraRerved, listWalletCMV);
                    }
                }
            }
        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public static double getValueAplicandoOperator(Billetera billetera, double valueComverse, String logPrefijo) {
        try {
            double value = 0.0;
            double valueOperation = Truncar(billetera.getValor(), 2);
            switch (billetera.getOperador()) {
                case "SUMA":
                    value = valueComverse + valueOperation;
                    break;
                case "RESTA":
                    value = valueComverse - valueOperation;
                    break;
                case "DIVISION":
                    if (valueOperation != 0) {
                    //   value = valueComverse / valueOperation; Da error en decimales
                        value = (valueComverse * 10) / (valueOperation * 10);
                    } else {
                        value = 0;
                    }
                    break;
                case "MULTIPLICACION":
                    value = valueComverse * valueOperation;
                    break;
                case "NINGUNO":
                    value = valueComverse;
                    break;
            }
         if (billetera.getCantidadDecimales() == 0) {
                return Truncar(value, billetera.getCantidadDecimales());
            } else {
                return redondear(value, billetera.getCantidadDecimales());
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[getValueAplicandoOperator]NO se pudo aplicar operador-valor:", e);
            return 0;
        }
    }

    public static double Truncar(double nD, int nDec) {
        if (nD > 0) {
            nD = Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        } else {
            nD = Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        }
        return nD;
    }

    public static double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        double resultado = (Math.round(numero * mult)) / (double) mult;
        return resultado;
    }

    public static String formatValueWithDecimalFormat(double valor, int cantdec, String logPrefijo) {
        String res = "";
        try {
            DecimalFormat Formateador = new DecimalFormat("0.0000");
            if ((valor % 1) == 0) {
                Formateador.setMaximumFractionDigits(0);
            } else {
                Formateador.setMaximumFractionDigits(cantdec);
            }
            Formateador.setRoundingMode(RoundingMode.DOWN);
            // Formateador.setRoundingMode(RoundingMode.valueOf(1));
            res = Formateador.format(valor);
        } catch (Exception e) {
            LOG.error(logPrefijo + "[formatValueWithDecimal]error:" + e.getMessage());
        }
        return res;
    }

    public static String getMinutesSeconds(double time) {
        String resultado;

        double minutes = Math.floor(time / 60);
        double seconds = time % 60;

        /*//Anteponiendo un 0 a los minutos si son menos de 10
        String minutess = minutes < 10 ? String.valueOf((int) minutes) : String.valueOf((int) minutes);
        //Anteponiendo un 0 a los segundos si son menos de 10
        String secondss = seconds < 10 ? String.valueOf((int) seconds) : String.valueOf((int) seconds);
         */
        String minutess = String.valueOf((int) minutes);
        String secondss = String.valueOf((int) seconds);

        if (minutess.equals("0")) {
            if (secondss.equals("0")) {
                resultado = minutess + " min " + secondss + " seg";
            } else {
                resultado = secondss + " seg";
            }
        } else {
            if (secondss.equals("0")) {
                resultado = minutess + " min";
            } else {
                resultado = minutess + " min " + secondss + " seg";
            }
        }

        return resultado;
    }

    public static String getNombreComercial(List<ConfiguracionNombres> list, Integer idBilletera) {
        boolean sw = true;
        String aux = "";
        if (list != null) {
            for (int i = 0; i < list.size() && sw; i++) {
                ConfiguracionNombres config = list.get(i);
                if (config.getBilleteraID() == idBilletera) {
                    sw = false;
                    if (config.getNombreComercial() != null) {
                        aux = config.getNombreComercial();
                    } else {
                        aux = Propiedades.TEXTO_NOMBRE_BILLETERA;
                    }
                }
            }
        }
        return aux;
    }

    public static List<Menu> getBilleterasNivel2(List<Menu> listaMenu) {
        List<Menu> lista = new ArrayList<>();
        for (Menu me : listaMenu) {
            //for (int i = 0; i < listaMenu.size(); i++) {
            //Menu me = listaMenu.get(i);
            if (me.getNivel().equals(2)) {
                lista.add(me);
            }
        }
        return lista;
    }

    public static boolean mostrarMenu(Menu menu, SaldoUnidad su) {
        boolean sw = false;
        if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCero().equals("f")) {
            sw = false;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCero().equals("t")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCero().equals("f")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCero().equals("t")) {
            if (su.getSaldo() > 0.0) {
                sw = true;
            }
        }
        return sw;
    }

    public static boolean mostrarMenuCalculado(Menu menu, SaldoUnidad su, List<WalletComverse> lista, List<Billetera> listaBilletera) {
        boolean sw = false;

        if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("f")) {
            sw = false;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("t")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("f")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("t")) {
            boolean expira = HayExpirado(lista, listaBilletera);
            if (su.getSaldo() > 0.0 && expira) {
                sw = true;
            }
        }
        return sw;
    }

    public static boolean HayExpirado(List<WalletComverse> listaConver, List<Billetera> listaBilletera) {
        boolean sw = false;
        for (int i = 0; i < listaBilletera.size() && !sw; i++) {
            Billetera billetera = listaBilletera.get(i);

            for (int j = 0; j < listaConver.size() && !sw; j++) {
                WalletComverse billeteraComverse = listaConver.get(j);
                if (billetera.getNombreComverse().equals(billeteraComverse.getNameWallet())) {
                    if (billeteraComverse.getExpiration().getTimeInMillis() < System.currentTimeMillis()) {
                        sw = true;
                    }
                }
            }
        }
        return sw;
    }

}
