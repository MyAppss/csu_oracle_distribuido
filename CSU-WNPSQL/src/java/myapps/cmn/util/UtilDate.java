package myapps.cmn.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.sql.Time;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class UtilDate {

    private static final Logger LOG = Logger.getLogger(UtilDate.class);

    public static Timestamp dateToTimestamp(Date fecha, String logPrefijo) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = formatter.format(fecha);
        return stringToTimestamp(str, logPrefijo);
    }

    public static Calendar stringToCalendar(String fecha, String logprefijo) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error(logprefijo + "stringToCalendar: " + fecha + " - " + e.getMessage());
        }
        return cal;
    }

    public static Calendar stringToCalendarXML(String fecha, String logPrefijo) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error(logPrefijo + "stringToCalendarXML: " + fecha + " - " + e.getMessage());
        }
        return cal;
    }

    public static Calendar dateToCalendar(Date date, String logPrefijo) {
        Calendar cal = null;
        try {
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (Exception e) {
            LOG.error(logPrefijo + "dateToCalendar: " + e.getMessage());
        }
        return cal;
    }

    public static Calendar stringToCalendarPM_AM(String fecha, String logPrefijo) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error(logPrefijo + "stringToCalendarPM_AM: " + fecha + " - " + e.getMessage());
        }
        return cal;
    }

    public static Timestamp stringToTimestamp(String fecha, String logPrefijo) {
        Calendar date = stringToCalendar(fecha, logPrefijo);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Timestamp stringToTimestampPM_AM(String fecha, String logPrefijo) {
        Calendar date = stringToCalendarPM_AM(fecha, logPrefijo);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Date stringToDate(String fecha, String logPrefijo) {
        fecha = fecha + " 00:00:00";
        Calendar date = stringToCalendar(fecha, logPrefijo);
        Date dat = new Date();
        if (date != null) {
            dat = new Date(date.getTimeInMillis());
        }
        return dat;
    }

    public static Time stringToTime(String fecha) {
        fecha = fecha + ":00";
        Time t = Time.valueOf(fecha);
        return t;
    }

    public static String dateToString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Timestamp dateToTimetampSW_IF(Date fecha, boolean swIni, String logprefijo) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String str = formatter.format(fecha);
        if (swIni) {
            str = str + " 00:00:00";
        } else {
            str = str + " 23:59:59";
        }
        return stringToTimestamp(str, logprefijo);
    }

    public static Timestamp getTimetampActual() {
        /*Date dd = new Date();
         String str = dd.getTime() + "";
         str = str.substring(0, 10);
         str = str + "000";
         long tt = Long.parseLong(str);*/
        long tt = Calendar.getInstance().getTimeInMillis();
        Timestamp t = new Timestamp(tt);
        return t;
    }

    public static String timetampToString(Timestamp fecha) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static String dateToTimeString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("HH:mm");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static String dateToTimeString(Calendar fecha, String logPrefijo) {

        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");//("yyyy-MM-dd");
            // 01/01/0001 00:00:00
            return formatter.format(fecha.getTime());
        } catch (Exception e) {
            LOG.error(logPrefijo + "dateToTimeString: " + e.getMessage());
            return null;
        }

    }

    public static Date unirTwoDate(Date day, Date time, String logPrefijo) {

        String str1 = UtilDate.dateToString(day);

        String str = UtilDate.dateToTimeString(time);
        str = str1 + " " + str;
        return UtilDate.stringToTimestamp(str, logPrefijo);
    }

    public static boolean intervalo(Date mI, Date mF, Date fecha) {
        if (fecha.getTime() == mI.getTime()) {
            return true;
        }

        if (fecha.getTime() == mF.getTime()) {
            return true;
        }

        return fecha.after(mI) && (fecha.before(mF));
    }

    public static String dateToString(Date fecha, String format, String logPrefijo) {
        String date = "";
        try {
            if (fecha != null) {
                DateFormat formatter = new SimpleDateFormat(format);
                date = formatter.format(fecha);
            } else {
                date = "NULL";
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "Exception: " + e.getMessage(), e);
        }
        return date;
    }

    public static XMLGregorianCalendar getFechaXMLBSSAPI(Date fecha, String logPrefijo) {

        XMLGregorianCalendar result = null;
        Date date;
        SimpleDateFormat simpleDateFormat;
        GregorianCalendar gregorianCalendar;
        try {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String cadena = simpleDateFormat.format(fecha);
            Calendar calendar = stringToCalendarXML(cadena, logPrefijo);
            if (calendar != null) {
                gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                date = calendar.getTime();
                gregorianCalendar.setTime(date);
                result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
            }
        } catch (DatatypeConfigurationException ex) {
            LOG.error(logPrefijo + "Exception en String to Greg Calander Conversion: " + ex.getMessage(), ex);
        }

        return result;
    }

    public static XMLGregorianCalendar getFechaXMLBSSAPI(String fecha, String logPrefijo) {

        XMLGregorianCalendar result = null;
        if (!"".equals(fecha)) {
            Date date;
            SimpleDateFormat simpleDateFormat;
            GregorianCalendar gregorianCalendar;
            try {
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                date = simpleDateFormat.parse(fecha);
                gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();

                gregorianCalendar.setTime(date);
                result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
            } catch (ParseException | DatatypeConfigurationException ex) {
                LOG.error(logPrefijo + "Exception en String to Greg Calander Conversion: " + ex.getMessage(), ex);
            }
        }
        return result;
    }

    public static XMLGregorianCalendar getFechaXMLCSB(String fecha, String logPrefijo) {
        XMLGregorianCalendar result = null;
        if (!"".equals(fecha)) {
            GregorianCalendar gregorianCalendar;
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                Date datefecha = format.parse(fecha);

                gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();

                gregorianCalendar.setTime(datefecha);
                result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
            } catch (ParseException | DatatypeConfigurationException ex) {
                LOG.error(logPrefijo + "Exception en String to Greg Calander Conversion: ", ex.getCause());
            }
        }
        return result;
    }

    public static Calendar xmlGregoriaToCalendar(XMLGregorianCalendar fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(fecha.toGregorianCalendar().getTimeInMillis());
        return cal;
    }

    public static Calendar getFechaCalendarCSB(String fecha, String logPrefijo) {
        Calendar result = null;
        if (!"".equals(fecha)) {
            Calendar calendar;
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                Date datefecha = format.parse(fecha);

                calendar = Calendar.getInstance();
                calendar.setTimeInMillis(datefecha.getTime());
                result = calendar;
            } catch (ParseException ex) {
                LOG.error(logPrefijo + "Exception en String to Greg Calander Conversion: ", ex.getCause());
            }
        }
        return result;
    }

    public static String messageSeq(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        return format.format(date);
    }

    public static double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        double resultado = (Math.round(numero * mult)) / (double) mult;
        return resultado;
    }

    public static String stringToCalendarXMLL(String fecha, String logPrefijo) {
        String resultado = "";
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = (Date) formatter.parse(fecha);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat formatterr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            resultado = formatterr.format(calendar.getTime());
        } catch (ParseException e) {
            LOG.error(logPrefijo + "stringToCalendarXMLL : " + fecha + " - " + e.getMessage(), e);
        }
        return resultado;
    }

    public static String dateToTimeString(Calendar fecha, String format, String prefijo) {
        try {
            DateFormat formatter = new SimpleDateFormat(format);
            return formatter.format(fecha.getTime());
        } catch (Exception e) {
            LOG.error(prefijo + "dateToTimeString " + e.getMessage(), e);
            return null;
        }
    }

    public static Calendar stringToCalendar(String fecha, String format, String prefijo) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat(format);
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error(prefijo + " stringToCalendar " + e.getMessage(), e);
        }
        return cal;
    }

    public static Calendar fechaMayor(Calendar fechaUno, Calendar fechaDos) {
        Calendar cal;
        if (fechaUno == null) {
            cal = fechaDos;
        } else {
            if (fechaDos.after(fechaUno)) {
                cal = fechaDos;
            } else {
                cal = fechaUno;
            }

        }
        return cal;
    }

    public static String fechaCabeceras(Calendar fecha, boolean bandera, String prefijo) {
        String fechaExpiracion = "";
        if (Boolean.TRUE.equals(bandera) && fecha != null) {
            fechaExpiracion = " " + Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_MENSAJE + " " + dateToTimeString(fecha, Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_FORMATO, prefijo);
        }
        return fechaExpiracion;
    }
}
