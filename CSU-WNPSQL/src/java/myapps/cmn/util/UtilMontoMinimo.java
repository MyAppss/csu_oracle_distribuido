/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.util.ArrayList;
import java.util.List;
import myapps.cmn.dao.UnitTypeDao;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.UnitType;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class UtilMontoMinimo {
    
    private static final Logger LOG = Logger.getLogger(UtilMontoMinimo.class);
    private static final List<Integer> UNIT_TYPE = cadenaToList(Propiedades.UNIT_TYPE_MONTO_MINIMO);
    
    public static boolean validarMontoMinimo(boolean bandera, WalletComverse montoBilletera, Billetera billetera, String logPrefijo) {
        boolean valor = true;
        if (Boolean.TRUE.equals(bandera)) {
            if (montoBilletera != null && billetera != null && UNIT_TYPE != null && UNIT_TYPE.contains(billetera.getUnitTypeId())) {
                LOG.debug(logPrefijo + " Se validara el monto minimo para billetera: " + montoBilletera.toString() + " Billetera: " + billetera.toString());
                valor = montoBilletera.getAvailableBalance() > billetera.getMontoMinimo();
                LOG.debug(logPrefijo + "Resultado de la validación: " + valor);
            }            
        } else {
            LOG.debug(logPrefijo + " Bandera Validar Monto Minimo Desactivada");
        }
        return valor;
    }
    
    public static List<Integer> cadenaToList(String cadena) {
        List<Integer> lista = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split(";");
                for (String clave : arrayOne) {
                    List<UnitType> listatemporal = UnitTypeDao.getUniTypeforNombre(clave);
                    if (listatemporal != null) {
                        for (UnitType unit : listatemporal) {
                            if (unit != null) {
                                lista.add(unit.getUnitTypeId());
                            }
                            
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("[Exception] Error al cargar la lista de Push: " + cadena, e);
        }
        return lista;
    }
    
    public static boolean mostrarMenu() {
        
        return false;
    }
}
