/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import myapps.cmn.vo.Navega;
import myapps.cmn.vo.Pantalla;
import myapps.cmn.vo.Resultado;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class Convertidor {

    private static final Logger LOG = Logger.getLogger(UtilMontoMinimo.class);

    public static boolean consultaValida(Resultado resultado) {
        boolean valor = true;
        if (resultado != null && resultado.getPantallas() != null) {
            for (Pantalla pantalla : resultado.getPantallas()) {
                if (pantalla.getOpciones().equals("") || pantalla.getOpciones().equals(".")) {
                    valor = false;
                }
            }
        }
        return valor;
    }

    @SuppressWarnings({"unchecked", "deprecated"})
    public static Resultado modificarResultado(Resultado resultado) {
        Resultado valor = null;
        try {
            List<AuxiliarNivel> padres = new ArrayList<>();
            if (resultado != null && resultado.getPantallas() != null) {

                for (Pantalla pantalla : resultado.getPantallas()) {
                    if (pantalla.getIdPantalla() == 1) {
                        String[] array1 = obtenerArray(pantalla.getOpciones());
                        if (array1 != null) {
                            int i = 1;
                            for (String name : array1) {
                                String[] myArrayName = name.split("\\.-");
                                AuxiliarNivel padre = new AuxiliarNivel();
                                padre.setNivel(1);
                                padre.setNumero(i);
                                padre.setCabecera(pantalla.getCabecera());
                                if (myArrayName.length >= 2) {
                                    padre.setNombre(myArrayName[1]);
                                } else {
                                    padre.setNombre(myArrayName[0]);
                                }

                                padres.add(padre);
                                i++;
                            }
                        }

                    } else if (pantalla.getPantallaPadre() == 1) {
                        String[] array2 = obtenerArray(pantalla.getOpciones());
                        int i = 1;
                        for (AuxiliarNivel nivel : padres) {
                            AuxiliarNivel nivel2 = obtenerHijos(array2, nivel.getNumero(), pantalla.getOpcionPadre());
                            if (nivel2 != null) {
                                nivel2.setNivel(2);
                                nivel2.setNumero(i);
                                nivel2.setCabecera(pantalla.getCabecera());

                                nivel.getHijos().add(nivel2);
                                i++;
                            }
                        }

                    } else {
                        String[] array3 = obtenerArray(pantalla.getOpciones());
                        int i = 1;
                        for (AuxiliarNivel nivel : padres) {
                            for (AuxiliarNivel nive2 : nivel.getHijos()) {
                                AuxiliarNivel nivel3 = obtenerHijos(array3, nivel.getNumero(), pantalla.getOpcionPadre());
                                if (nivel3 != null) {
                                    nivel3.setNivel(3);
                                    nivel3.setNumero(i);
                                    nivel3.setCabecera(pantalla.getCabecera());
                                    nive2.getHijos().add(nivel3);
                                    i++;
                                }
                            }
                        }

                    }
                }
            }
            LOG.debug(padres.size());
            padres = limpiarLista(padres);
            LOG.debug(padres.size());
            valor = myPrueba(resultado, padres);
        } catch (Exception e) {
            LOG.error("Exception: " + e.getMessage(), e);
            valor = resultado;
        }

        // valor = crearResultado(padres);
        return valor;
    }

    private static String[] obtenerArray(String opciones) {
        String[] valor = new String[1];
        if (opciones.contains(".-")) {
            valor = opciones.split("\n");
        } else {
            valor[0] = opciones;
        }
        return valor;
    }

    private static AuxiliarNivel obtenerHijos(String[] array, int padre, int opcionPadre) {
        AuxiliarNivel valor = null;
        if (array != null) {
            for (String name : array) {
                if (padre == opcionPadre) {
                    valor = new AuxiliarNivel();
                    valor.setNombre(name);
                }
            }
        }

        return valor;
    }

    private static List<AuxiliarNivel> limpiarLista(List<AuxiliarNivel> myLista) {
        List<AuxiliarNivel> valor = new ArrayList<>();
        if (myLista != null) {
            Iterator<AuxiliarNivel> iterator1 = myLista.iterator();
            while (iterator1.hasNext()) {
                AuxiliarNivel nivel1 = iterator1.next();
                nivel1.setHijos(limpiarLista(nivel1.getHijos()));

                if (nivel1.getNivel() == 2 && nivel1.getNombre().equals(".")) {
                    iterator1.remove();

                }
            }
            valor = myLista;
        }
        return valor;
    }

    public static Resultado myPrueba(Resultado resultado, List<AuxiliarNivel> lista) {
        Resultado valor = new Resultado();
        if (resultado != null) {
            valor.setLinea(resultado.getLinea());
            valor.setCodigo(resultado.getCodigo());
            valor.setMensaje(resultado.getMensaje());
            List<Pantalla> pantallaList = new ArrayList<>();
            if (lista != null) {

                for (Pantalla myPantalla : resultado.getPantallas()) {
                    Pantalla pantallaAuxiliar = procesarPantalla(myPantalla, lista, myPantalla.getIdPantalla(), resultado.getPantallas());
                    if (pantallaAuxiliar != null) {
                        pantallaList.add(pantallaAuxiliar);
                    }
                }
            }
            valor.setPantallas(pantallaList);
        }
        return valor;
    }

    @SuppressWarnings({"unchecked", "deprecated"})
    public static String obtenerCabecera(String myCabecera, String opciones) {
        String valor = myCabecera;
        if (myCabecera != null && myCabecera.isEmpty()) {
            LOG.debug("sin cambios");
        } else if (myCabecera != null && !myCabecera.isEmpty()) {
            // String[] arrayOne = myCabecera.split(Propiedades.MY_EXPRESION);
            String[] arrayOne = myCabecera.split("\n1.-");
            // String[] arrayOne = myCabecera.split("\n1.-");
            if (arrayOne != null && arrayOne.length == 2) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(arrayOne[0]);
                if (opciones != null && !opciones.isEmpty()) {
                    stringBuilder.append("\n");
                    stringBuilder.append(opciones);
                }
                valor = stringBuilder.toString();
            } 
        }
        return valor;
    }

    private static Pantalla procesarPantalla(Pantalla pantalla, List<AuxiliarNivel> lista, int numeroPantalla, List<Pantalla> pantallaList) {
        Pantalla valor = null;

        if (null == pantalla.getPantallaPadre()) {
        } else {
            switch (pantalla.getPantallaPadre()) {
                case 0: {
                    String opciones = obtenerOpciones(lista, 1);
                    pantalla.setCabecera(obtenerCabecera(pantalla.getCabecera(), opciones));
                    pantalla.setOpciones(opciones);
                    if (!opciones.isEmpty()) {
                        pantalla.setListaNroOpcion(obtenerListaOpcion(lista, 1, pantalla.getListaNroOpcion()));
                        pantalla.setListaNavegacion(obtenerListaNavega(lista, 1, pantalla.getListaNavegacion()));
                    } else {
                        pantalla.setListaNroOpcion(new ArrayList<>());
                        pantalla.setListaNavegacion((new ArrayList<>()));
                    }
                    valor = pantalla;
                    break;
                }
                case 1: {
                    String opciones = obtenerOpciones(lista, 2);
                    if (!opciones.isEmpty()) {
                        pantalla.setCabecera(obtenerCabecera(pantalla.getCabecera(), opciones));
                        pantalla.setOpciones(opciones);
                        pantalla.setListaNroOpcion(obtenerListaOpcion(lista, 2, pantalla.getListaNroOpcion()));
                        pantalla.setListaNavegacion(obtenerListaNavega(lista, 2, pantalla.getListaNavegacion()));
                        valor = pantalla;
                    } else {
                        int contador = numeroPantalla++;
                        LOG.debug("pantalla: " + numeroPantalla + " contador: " + contador);
                        if (contador <= pantallaList.size() - 1) {
                            pantalla.setOpciones(pantallaList.get(contador).getOpciones());
                            List<AuxiliarNivel> listahijos = obtenerListaHijo(pantalla.getOpciones(), lista);
                            pantalla.setCabecera(obtenerCabecera(pantalla.getCabecera(), opciones));
                            pantalla.setListaNroOpcion(obtenerListaOpcion(listahijos, 2, pantalla.getListaNroOpcion()));
                            pantalla.setListaNavegacion(obtenerListaNavega(listahijos, 2, pantalla.getListaNavegacion()));
                            valor = pantalla;
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }

        return valor;
    }

    private static String obtenerOpciones(List<AuxiliarNivel> lista, int nivel) {
        StringBuilder valor = new StringBuilder();
        switch (nivel) {
            case 1:
                if (lista != null) {
                    int numero = 1;
                    for (AuxiliarNivel aux : lista) {
                        if (aux.getNivel() == nivel) {
                            if (aux.getHijos().isEmpty()) {
                                LOG.debug("");
                                //valor.append(aux.getNombre());
                            } else {
                                if (numero != 1) {
                                    valor.append("\n");
                                }
                                valor.append(numero).append(".-").append(aux.getNombre());
                                numero++;
                            }

                        }
                    }
                }
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;
        }
        return valor.toString();
    }

    private static List<Integer> obtenerListaOpcion(List<AuxiliarNivel> lista, int nivel, List<Integer> listaOpcion) {
        List<Integer> valor = new ArrayList<>();
        if (lista != null && listaOpcion != null) {
            int mycontador = 0;
            for (int i = 0; i <= lista.size() - 1; i++) {
                if (!lista.get(i).getHijos().isEmpty()) {
                    valor.add(listaOpcion.get(mycontador));
                    mycontador++;
                }

            }
        }
        return valor;
    }

    private static List<Navega> obtenerListaNavega(List<AuxiliarNivel> lista, int nivel, List<Navega> listaNavega) {
        List<Navega> valor = new ArrayList<>();
        if (lista != null && listaNavega != null) {
            int mycontador = 0;
            for (int i = 0; i <= lista.size() - 1; i++) {
                if (!lista.get(i).getHijos().isEmpty()) {
                    valor.add(listaNavega.get(mycontador));
                    mycontador++;
                }
            }
        }
        return valor;
    }

    private static List<AuxiliarNivel> obtenerListaHijo(String nombre, List<AuxiliarNivel> lista) {
        List<AuxiliarNivel> valor = new ArrayList<>();
        for (AuxiliarNivel a1 : lista) {
            if (nombre.equals(a1.getNombre())) {
                valor = a1.getHijos();
                break;
            }
            for (AuxiliarNivel a2 : a1.getHijos()) {
                if (nombre.equals(a2.getNombre())) {
                    valor = a2.getHijos();
                    break;
                }
            }
        }
        return valor;
    }
}
