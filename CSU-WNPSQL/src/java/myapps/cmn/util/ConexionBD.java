/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.sql.*;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 * @author Vehimar
 */
public class ConexionBD {

    public static final Logger LOG = Logger.getLogger(ConexionBD.class);
    private DataSource datasource;
    private static ConexionBD instance;

    private static DataSource setupDataSource() {

        PoolProperties p = new PoolProperties();
        String url = "jdbc:oracle:thin:@" + Propiedades.BD_SERVER + ":" + Propiedades.BD_PORT + "/" + Propiedades.BD_NAME;
        p.setUrl(url);
        p.setDriverClassName("oracle.jdbc.OracleDriver");
        p.setUsername(Propiedades.DB_USER);
        p.setPassword(Propiedades.DB_PASSWORD);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setTestOnReturn(false);
        p.setValidationQuery("SELECT 1 FROM DUAL");
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMinEvictableIdleTimeMillis(Integer.parseInt(Propiedades.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
        p.setMaxActive(Integer.parseInt(Propiedades.DB_MAX_ACTIVE));
        p.setInitialSize(2);
        p.setMinIdle(Integer.parseInt(Propiedades.DB_MIN_IDLE));
        p.setMaxIdle(Integer.parseInt(Propiedades.DB_MAX_IDLE));
        p.setMaxWait(Integer.parseInt(Propiedades.DB_MAX_WAIT));
        p.setRemoveAbandonedTimeout(60);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(false);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource dataSource = new DataSource();
        dataSource.setPoolProperties(p);

        return dataSource;
    }

    public ConexionBD() {
        datasource = setupDataSource();
    }

    public static final synchronized Connection openDBConnection() {
        Connection conexion = null;
        try {
            if (instance == null) {
                instance = new ConexionBD();
            }
            conexion = instance.datasource.getConnection();
        } catch (SQLException ex) {
            LOG.error("ERROR ABRIR CONEXION A LA BD: " + ex.getMessage());
        }
        return conexion;
    }

}
