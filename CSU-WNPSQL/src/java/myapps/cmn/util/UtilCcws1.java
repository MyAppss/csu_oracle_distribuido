/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Cabecera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class UtilCcws1 {

    private static final Logger LOG = Logger.getLogger(UtilCcws1.class);

    public static double Truncar(double nD, int nDec) {
        if (nD > 0) {
            nD = Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        } else {
            nD = Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        }
        return nD;
    }

    public static double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        double resultado = (Math.round(numero * mult)) / (double) mult;
        return resultado;
    }

    public static double getValueAplicandoOperator(Billetera billetera, double valueComverse, String logPrefijo) {
        try {
            double value = 0.0;
            double valueOperation = UtilCcws1.Truncar(billetera.getValor(), 2);
            switch (billetera.getOperador()) {
                case "SUMA":
                    value = valueComverse + valueOperation;
                    break;
                case "RESTA":
                    value = valueComverse - valueOperation;
                    break;
                case "DIVISION":
                    if (valueOperation != 0) {
                        //   value = valueComverse / valueOperation; Da error en decimales
                        value = (valueComverse * 10) / (valueOperation * 10);
                    } else {
                        value = 0;
                    }
                    break;
                case "MULTIPLICACION":
                    value = valueComverse * valueOperation;
                    break;
                case "NINGUNO":
                    value = valueComverse;
                    break;
            }
            if (billetera.getCantidadDecimales() == 0) {
                return UtilCcws1.Truncar(value, billetera.getCantidadDecimales());
            } else {
                return UtilCcws1.redondear(value, billetera.getCantidadDecimales());
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[getValueAplicandoOperator]NO se pudo aplicar operador-valor:", e);
            return 0;
        }
    }

    public static boolean contieneBilletera(List<Billetera> lista, Billetera billetera) {
        boolean resultado = false;
        boolean sw = true;
        for (int i = 0; i < lista.size() && sw; i++) {
            Billetera bill = lista.get(i);
            if (bill.getBilleteraId().equals(billetera.getBilleteraId())) {
                resultado = true;
                sw = false;
            }
        }
        return resultado;

    }

    public static List<Menu> obtenerBilleterasNivel1(List<Menu> listaMenu) {
        List<Menu> lista = new ArrayList<>();
        if (listaMenu != null) {
            for (Menu me : listaMenu) {
                //for (int i = 0; i < listaMenu.size(); i++) {
                //Menu me = listaMenu.get(i);
                if (me.getNivel().equals(1)) {
                    lista.add(me);
                }
            }
        }
        return lista;
    }

    public static boolean mostrarMenu(Menu menu, SaldoUnidad su) {
        boolean sw = false;
        if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCero().equals("f")) {
            sw = false;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCero().equals("t")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCero().equals("f")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCero().equals("t")) {
            if (su.getSaldo() > 0.0) {
                sw = true;
            }
        }
        return sw;
    }

    public static boolean mostrarMenuCalculado(Menu menu, SaldoUnidad su) {
        boolean sw = false;

        if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("f")) {

            sw = false;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("t")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("f")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("t")) {
            if (su.getSaldo() > 0.0) {
                sw = true;
            }
        }
        return sw;
    }

    public static boolean mostrarCabecera(Cabecera cabe, SaldoUnidad su) {
        boolean sw = false;
        if (cabe.getMostrarSiempre().equals("f") && cabe.getMayorCero().equals("f")) {
            sw = false;
        } else if (cabe.getMostrarSiempre().equals("t") && cabe.getMayorCero().equals("t")) {
            sw = true;
        } else if (cabe.getMostrarSiempre().equals("t") && cabe.getMayorCero().equals("f")) {
            sw = true;
        } else if (cabe.getMostrarSiempre().equals("f") && cabe.getMayorCero().equals("t")) {
            if (su.getSaldo() > 0.0) {
                sw = true;
            }
        }
        return sw;
    }

    public static SaldoUnidad obtenerSaldosDeBilleterasSinExpiracion(List<WalletComverse> listaBilleteraCMV,
            List<Billetera> listBilleterasCabecera, List<Offer> listOffert,
            List<Acumulador> listAcumulador, String logPrefijo) {

        if (listBilleterasCabecera != null) {
            String tipoUnidad = "";
            double total = 0;
            boolean whatsappIlimitado = false;
            // boolean tieneBilleteraExpirada = false;
            Integer cantidadDecimales = 0;
            for (Billetera billetera : listBilleterasCabecera) {

                WalletComverse billeteraComverse = null;
                if (billetera.isAlco().equals("t")) {
                    Alco:
                    if (listOffert != null) {
                        for (Offer oferta : listOffert) {
                            if (billetera.getNombreComverse().equals(oferta.getName())) {
                                billeteraComverse = new WalletComverse();
                                billeteraComverse.setNameWallet(oferta.getName());
                                billeteraComverse.setExpiration(oferta.getServiceEnd());
                                billeteraComverse.setUsada(true);
                                //billeteraComverse.setAvailableBalance(0);
                                billeteraComverse.setAvailableBalance(1);

                                for (Acumulador acum : listAcumulador) {
                                    LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                                    if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                        double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                        LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                        if (totalAcumulador < 0) {
                                            //billeteraComverse.setAvailableBalance(0);
                                            billeteraComverse.setAvailableBalance(1);
                                        } else {
                                            billeteraComverse.setAvailableBalance(totalAcumulador);
                                        }
                                        break Alco;
                                    }
                                }
                                oferta.setUsada(true);
                            }
                        }
                    }
                } else {
                    billeteraComverse = getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
                }

                if (billeteraComverse == null) {
                    // modificado el 15/02/2022
                    //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse() + ", nombreComercial: " + billetera.getNombreComercial());
                    LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse() + ", nombreComercial: " + billetera.getNombreComercial());
                } else {

                    //if (billeteraComverse.getExpiration().getTimeInMillis() > System.currentTimeMillis()) {
                    if (UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA1_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                    //if (billeteraComverse.getExpiration().getTimeInMillis() > System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA1_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                        double valueAplicadoOperador = getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance(), logPrefijo);
                        total = total + valueAplicadoOperador;
                        tipoUnidad = billetera.getPrefijoUnidad();
                        cantidadDecimales = billetera.getCantidadDecimales();
                    }

                }
            }
            if (!tipoUnidad.isEmpty()) {

                String valueStrBalance = formatValueWithDecimalFormat(total, cantidadDecimales, logPrefijo);
                SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                su.setCantidadDecimales(cantidadDecimales);
                su.setWhatsppIlimitado(whatsappIlimitado);
                su.setSaldo(total);
                su.setSaldoTexto(valueStrBalance);
                su.setUnidad(tipoUnidad);
                su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);
                return su;
            } else {
                //if (tieneBilleteraExpirada) { // PARA VALIDACION DE WHATSAPP ILIMITADO
                SaldoUnidad su = new SaldoUnidad(-1, "", "", "", 0, false);
                return su;
                //}
            }
        }
        return null;
    }

    public static String formatValueWithDecimalFormat(double valor, int cantdec, String logPrefijo) {
        String res = "";
        try {
            DecimalFormat Formateador = new DecimalFormat("0.0000");
            if ((valor % 1) == 0) {
                Formateador.setMaximumFractionDigits(0);
            } else {
                Formateador.setMaximumFractionDigits(cantdec);
            }
            //Formateador.setRoundingMode(RoundingMode.DOWN);
            res = Formateador.format(valor);
        } catch (Exception e) {
            LOG.error(logPrefijo + "[formatValueWithDecimal]error:" + e.getMessage());
        }
        return res;
    }

    public static List<Menu> getListaMenuHijos(List<Menu> listaMenu, Integer idPadre) {
        List<Menu> lista = new ArrayList<>();
        for (Menu me : listaMenu) {
            //for (int i = 0; i < listaMenu.size(); i++) {
            //Menu me = listaMenu.get(i);
            if (me.getPadre().equals(idPadre)) {
                lista.add(me);
            }
        }
        return lista;
    }

    public static String getMinutesSeconds(double time) {
        String resultado;

        double minutes = Math.floor(time / 60);
        double seconds = time % 60;
        /*//Anteponiendo un 0 a los minutos si son menos de 10
        String minutess = minutes < 10 ? String.valueOf((int) minutes) : String.valueOf((int) minutes);
        //Anteponiendo un 0 a los segundos si son menos de 10
        String secondss = seconds < 10 ? String.valueOf((int) seconds) : String.valueOf((int) seconds);
         */
        String minutess = String.valueOf((int) minutes);
        String secondss = String.valueOf((int) seconds);

        if (minutess.equals("0")) {
            if (secondss.equals("0")) {
                resultado = minutess + " min " + secondss + " seg";
            } else {
                resultado = secondss + " seg";
            }
        } else {
            if (secondss.equals("0")) {
                resultado = minutess + " min";
            } else {
                resultado = minutess + " min " + secondss + " seg";
            }
        }
        return resultado;
    }

    public static SaldoUnidad obtenerSaldosDeBilleteras(List<WalletComverse> listaBilleteraCMV,
            List<Billetera> listBilleterasCabecera, List<Offer> listOffert,
            List<Acumulador> listAcumulador, String logPrefijo) {

        if (listBilleterasCabecera != null) {
            Calendar fechaExpiracion = null;
            String tipoUnidad = "";
            double total = 0;
            boolean whatsappIlimitado = false;
            boolean tieneBilleteraExpirada = false;
            Integer cantidadDecimales = 0;
            for (Billetera billetera : listBilleterasCabecera) {

                WalletComverse billeteraComverse = null;
                if (billetera.isAlco().equals("t")) {
                    Alco:
                    for (Offer oferta : listOffert) {
                        if (billetera.getNombreComverse().equals(oferta.getName())) {
                            billeteraComverse = new WalletComverse();
                            billeteraComverse.setNameWallet(oferta.getName());
                            billeteraComverse.setExpiration(oferta.getServiceEnd());
                            billeteraComverse.setUsada(true);
                            //billeteraComverse.setAvailableBalance(0);
                            billeteraComverse.setAvailableBalance(1);

                            for (Acumulador acum : listAcumulador) {
                                LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                                if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                    double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                    LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                    if (totalAcumulador < 0) {
                                        //billeteraComverse.setAvailableBalance(0);
                                        billeteraComverse.setAvailableBalance(1);
                                    } else {
                                        billeteraComverse.setAvailableBalance(totalAcumulador);
                                    }
                                    break Alco;
                                }
                            }
                            oferta.setUsada(true);
                        }
                    }
                } else {
                    billeteraComverse = getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
                }

                if (billeteraComverse == null) {
                    //modificado el 15/02/2022
                    //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse() + ", nombreComercial: " + billetera.getNombreComercial());
                    LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse() + ", nombreComercial: " + billetera.getNombreComercial());
                } else {

                    //Habilitar para desactivar monoto minimo
                    // if (billeteraComverse.getExpiration().getTimeInMillis() > System.currentTimeMillis()) {
                    if (billeteraComverse.getExpiration().getTimeInMillis() > System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA1_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                        double valueAplicadoOperador = getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance(), logPrefijo);
                        total = total + valueAplicadoOperador;
                        tipoUnidad = billetera.getPrefijoUnidad();
                        cantidadDecimales = billetera.getCantidadDecimales();
                        if (billeteraComverse.getAvailableBalance() >= billetera.getMontoMaximo()) {
                            whatsappIlimitado = true;
                        }

                        fechaExpiracion = UtilDate.fechaMayor(fechaExpiracion, billeteraComverse.getExpiration());
                    } else {
                        tieneBilleteraExpirada = true;
                        LOG.debug(logPrefijo + " Billetera expirada: " + billetera.getNombreComercial() + ", nameComverse: " + billetera.getNombreComverse());
                    }

                }
            }
            if (!tipoUnidad.isEmpty()) {

                String valueStrBalance = formatValueWithDecimalFormat(total, cantidadDecimales, logPrefijo);
                SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                su.setCantidadDecimales(cantidadDecimales);
                su.setWhatsppIlimitado(whatsappIlimitado);
                su.setSaldo(total);
                su.setSaldoTexto(valueStrBalance);
                su.setUnidad(tipoUnidad);
                su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);
                su.setFechaExpiracion(fechaExpiracion);
                // su.setFechaExpiracionDos(fechaExpiracionDos);
                return su;
            } else {
                if (tieneBilleteraExpirada) { // PARA VALIDACION DE WHATSAPP ILIMITADO
                    SaldoUnidad su = new SaldoUnidad(-1, "", "", "", 0, false);
                    return su;
                }
            }
        }
        return null;
    }

    public static List<Billetera> obtenerBilleterarReservas(Config currentConfig) {
        List<Billetera> lista = new ArrayList<>();
        List<Cabecera> listCabecera = currentConfig.getListCabeceras();
        Integer unit = Integer.parseInt(Propiedades.TEXTO_CURRENCY);

        if (listCabecera != null) {
            for (Cabecera cabe : listCabecera) {
                //for (int i = 0; i < listCabecera.size(); i++) {
                //Cabecera cabe = listCabecera.get(i);
                List<Billetera> listaBilletera = cabe.getListBilleteras();
                if (listaBilletera != null) {

                    for (Billetera bill : listaBilletera) {
                        //for (int j = 0; j < listaBilletera.size(); j++) {
                        //Billetera bill = listaBilletera.get(j);

                        if (bill.isReserva() != null && bill.isReserva().equals("t") && bill.getUnitTypeId().equals(unit)) {
                            boolean sw = UtilCcws1.contieneBilletera(lista, bill);
                            if (!sw) {
                                lista.add(bill);
                            }
                        }
                    }
                }
            }
        }
        return lista;
    }

    public static WalletComverse getWalletCmvAndMarcar(String nameWallet, List<WalletComverse> listWalletCMV) {
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    if (walletComverse.getNameWallet().equals(nameWallet)) {
                        walletComverse.setUsada(true);
                        return walletComverse;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public static double obtenerReservas(Config currentConfig, List<WalletComverse> listaBilleteraCMV, String logPrefijo) {
        long z = System.currentTimeMillis();
        double resultado = 0;
        List<Billetera> ListaReserva = UtilCcws1.obtenerBilleterarReservas(currentConfig);
        for (Billetera billetera : ListaReserva) {
            //for (int j = 0; j < ListaReserva.size(); j++) {
            //Billetera billetera = ListaReserva.get(j);
            boolean sw = true;
            if (listaBilleteraCMV != null) {
                for (int i = 0; i < listaBilleteraCMV.size() && sw; i++) {
                    WalletComverse BilleteraComverse = listaBilleteraCMV.get(i);
                    if (billetera.getNombreComverse().equals(BilleteraComverse.getNameWallet())) {
                        BilleteraComverse = UtilCcws1.getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
                        if (BilleteraComverse != null) {
                            double valueAplicadoOperador = UtilCcws1.getValueAplicandoOperator(billetera, BilleteraComverse.getAvailableBalance(), logPrefijo);
                            resultado = resultado + valueAplicadoOperador;
                        } else {
                            LOG.warn(logPrefijo + "BILLETERA RESERVA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                        }
                        sw = false;
                    }
                }
            }
        }
        if (resultado == 0 && ListaReserva.isEmpty()) {
            return -1;
        }

        LOG.debug(logPrefijo + "Obtener Reservas en : " + (System.currentTimeMillis() - z) + " ms");
        return resultado;
    }

}
