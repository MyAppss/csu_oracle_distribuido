/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.ConfiguracionNombres;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class UtilCcws3 {

    private static final Logger LOG = Logger.getLogger(UtilCcws3.class);

    public static Menu obtenerMenu(Config actualConfig, int menuId) {
        if (actualConfig != null) {
            List<Menu> listMenu = actualConfig.getListMenus();
            if (listMenu != null) {
                for (Menu menu : listMenu) {
                    if (menu.getId() == menuId) {
                        return menu;
                    }
                }
            }
        }
        return null;
    }

    public static List<Billetera> obtenerListaBilleterasReservadas(List<Billetera> listBilleteras) {
        List<Billetera> listbilleteraAux = new ArrayList<>();
        for (Billetera billetera : listBilleteras) {
            if (billetera.isReserva() != null && billetera.isReserva().equals("t")) {
                listbilleteraAux.add(billetera);
            }
        }
        return listbilleteraAux;
    }

    public static WalletComverse getWalletCmvAndMarcar(String nameWallet, List<WalletComverse> listWalletCMV) {
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    if (walletComverse.getNameWallet().equals(nameWallet)) {
                        walletComverse.setUsada(true);
                        return walletComverse;
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public static double obtenerValorAvailableBalance(Billetera billeteraReservada, List<WalletComverse> listWalletCMV) {
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    if (walletComverse.getNameWallet().equals(billeteraReservada.getNombreComverse())) {

                        return walletComverse.getAvailableBalance();
                    }
                }
            }
        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public static double obtenerValorReserva(Billetera billetera, List<Billetera> billeteraReservada, List<WalletComverse> listWalletCMV) {
        try {
            if (billeteraReservada != null) {
                for (Billetera billeteraRerved : billeteraReservada) {
                    if (billeteraRerved.getNombreComverse().contains(billetera.getNombreComverse())) {
                        return obtenerValorAvailableBalance(billeteraRerved, listWalletCMV);
                    }
                }
            }
        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public static double getValueAplicandoOperator(Billetera billetera, double valueComverse, String logPrefijo) {
        try {
            double value = 0.0;
            double valueOperation = Truncar(billetera.getValor(), 2);
            switch (billetera.getOperador()) {
                case "SUMA":
                    value = valueComverse + valueOperation;
                    break;
                case "RESTA":
                    value = valueComverse - valueOperation;
                    break;
                case "DIVISION":
                    if (valueOperation != 0) {
                        //   value = valueComverse / valueOperation; Da error en decimales
                        value = (valueComverse * 10) / (valueOperation * 10);
                    } else {
                        value = 0;
                    }
                    break;
                case "MULTIPLICACION":
                    value = valueComverse * valueOperation;
                    break;
                case "NINGUNO":
                    value = valueComverse;
                    break;
            }
            if (billetera.getCantidadDecimales() == 0) {
                return Truncar(value, billetera.getCantidadDecimales());
            } else {
                return redondear(value, billetera.getCantidadDecimales());
            }

        } catch (Exception e) {
            LOG.error(logPrefijo + "[getValueAplicandoOperator]NO se pudo aplicar operador-valor:", e);
            return 0;
        }
    }

    public static double Truncar(double nD, int nDec) {
        if (nD > 0) {
            nD = Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        } else {
            nD = Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        }
        return nD;
    }

    public static double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        double resultado = (Math.round(numero * mult)) / (double) mult;
        return resultado;
    }

    public static String formatValueWithDecimalFormat(double valor, int cantdec, String logPrefijo) {
        String res = "";
        try {
            DecimalFormat Formateador = new DecimalFormat("0.0000");
            if ((valor % 1) == 0) {
                Formateador.setMaximumFractionDigits(0);
            } else {
                Formateador.setMaximumFractionDigits(cantdec);
            }
            Formateador.setRoundingMode(RoundingMode.DOWN);
            res = Formateador.format(valor);
        } catch (Exception e) {
            LOG.error(logPrefijo + "[formatValueWithDecimal]error:" + e.getMessage());
        }
        return res;
    }

    public static String getMinutesSeconds(double time) {
        String resultado;

        double minutes = Math.floor(time / 60);
        double seconds = time % 60;

        /*//Anteponiendo un 0 a los minutos si son menos de 10
        String minutess = minutes < 10 ? String.valueOf((int) minutes) : String.valueOf((int) minutes);
        //Anteponiendo un 0 a los segundos si son menos de 10
        String secondss = seconds < 10 ? String.valueOf((int) seconds) : String.valueOf((int) seconds);
         */
        String minutess = String.valueOf((int) minutes);
        String secondss = String.valueOf((int) seconds);

        if (minutess.equals("0")) {
            if (secondss.equals("0")) {
                resultado = minutess + " min " + secondss + " seg";
            } else {
                resultado = secondss + " seg";
            }
        } else {
            if (secondss.equals("0")) {
                resultado = minutess + " min";
            } else {
                resultado = minutess + " min " + secondss + " seg";
            }
        }

        return resultado;
    }

    public static String getNombreComercial(List<ConfiguracionNombres> list, Integer idBilletera) {
        boolean sw = true;
        String aux = "";

        if (list != null) {
            for (int i = 0; i < list.size() && sw; i++) {
                ConfiguracionNombres config = list.get(i);
                if (config.getBilleteraID() == idBilletera) {
                    sw = false;
                    if (config.getNombreComercial() != null) {
                        aux = config.getNombreComercial();
                    } else {
                        aux = Propiedades.TEXTO_NOMBRE_BILLETERA;
                    }
                }
            }
        }
        return aux;
    }

    public static boolean getSegundaFechaExp(List<ConfiguracionNombres> list, Integer idBilletera) {
        boolean sw = false;

        if (list != null) {
            for (int i = 0; i < list.size() && !sw; i++) {
                ConfiguracionNombres config = list.get(i);
                if (config.getBilleteraID() == idBilletera) {
                    if (config.getSegundaFechaExp() != null && config.getSegundaFechaExp().equals("t")) {
                        sw = true;
                    }
                }
            }
        }
        return sw;
    }

    public static SaldoUnidad obtenerSaldoDeBilleteraSinVerificarExpiracion(List<WalletComverse> listaBilleteraCMV, Billetera billetera, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        if (billetera != null) {

            WalletComverse billeteraComverse = null;
            if (billetera.isAlco().equals("t")) {
                Alco:
                for (Offer oferta : listOffer) {
                    if (billetera.getNombreComverse().equals(oferta.getName())) {
                        billeteraComverse = new WalletComverse();
                        billeteraComverse.setNameWallet(oferta.getName());
                        billeteraComverse.setExpiration(oferta.getServiceEnd());
                        billeteraComverse.setUsada(true);
                        //billeteraComverse.setAvailableBalance(0);
                        billeteraComverse.setAvailableBalance(1);

                        for (Acumulador acum : listAcumulador) {
                            LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                            if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                if (totalAcumulador < 0) {
                                    //billeteraComverse.setAvailableBalance(0);
                                    billeteraComverse.setAvailableBalance(1);
                                } else {
                                    billeteraComverse.setAvailableBalance(totalAcumulador);
                                }
                                break Alco;
                            }
                        }
                        oferta.setUsada(true);
                    }
                }
            } else {
                billeteraComverse = getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
            }

            if (billeteraComverse == null) {
                //modifiado 16/02/2022
                //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
            } else {
                //Inabilitar para quitar monto minimo
                if (UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA3_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                    double valueAplicadoOperador = getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance(), logPrefijo);
                    String tipoUnidad = billetera.getPrefijoUnidad();
                    String valueStrBalance = formatValueWithDecimalFormat(valueAplicadoOperador, billetera.getCantidadDecimales(), logPrefijo);
                    SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                    su.setCantidadDecimales(billetera.getCantidadDecimales());
                    su.setSaldo(valueAplicadoOperador);
                    su.setSaldoTexto(valueStrBalance);
                    su.setUnidad(tipoUnidad);
                    su.setFechaExpiracion(billeteraComverse.getExpiration());
                    su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);
                    return su;
                }

            }
        }
        return null;
    }

    public static boolean mostrarMenuCalculado(Menu menu, SaldoUnidad su) {
        boolean sw = false;

        if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("f")) {
            sw = false;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("t")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("t") && menu.getMostrarMayorCeroAcu().equals("f")) {
            sw = true;
        } else if (menu.getMostrarSimpre().equals("f") && menu.getMostrarMayorCeroAcu().equals("t")) {
            if (su.getSaldo() > 0.0) {
                sw = true;
            }
        }
        return sw;
    }

    public static String getNombreAcumulado(List<ConfiguracionNombres> list, Integer idBilletera) {
        boolean sw = true;
        String aux = "";
        if (list != null) {
            for (int i = 0; i < list.size() && sw; i++) {
                ConfiguracionNombres config = list.get(i);
                if (config.getBilleteraID() == idBilletera) {
                    sw = false;
                    if (config.getNombreAcumulado() != null) {
                        aux = config.getNombreAcumulado();
                    } else {
                        String nombrComercial = getNombreComercial(list, idBilletera);
                        aux = nombrComercial + " " + Propiedades.TEXTO_NOMBRE_BILLETERA;
                    }
                }
            }
        }
        return aux;
    }

}
