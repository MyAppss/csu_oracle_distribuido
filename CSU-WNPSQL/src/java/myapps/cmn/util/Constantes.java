/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

/**
 *
 * @author hp
 */
public class Constantes {

    public static final String VIA_USSD = "USSD";
    public static final String VIA_SMS = "SMS";
    public static final String VIA_TODOS = "TODOS";

    public static final String TN_ON_DEMAND = "ON_DEMAND";
    public static final String TN_CON_MB = "CON_MB";
    public static final String TN_TODOS = "TODOS";

    public static final String INFO_NINGUNO = "NINGUNO";
    public static final String INFO_SALDO_PAQUETIGO = "SALDO_PAQUETIGO";
    public static final String INFO_OTROS_SALDOS = "OTROS_SALDOS";
    public static final String INFO_INFO_DE_RESERVA = "INFO_DE_RESERVA";
    public static final String INFO_PAQUETIGOS_ACUMULADOS = "PAQUETIGOS_ACUMULADOS";

    public static final String SEPARADOR_DE_INDICE_MENU = Propiedades.SEPARADOR_INDICE_MENU;
    public static final String SEPARADOR_DE_MENU = "|";
    public static final String SEPARADOR_DE_ITEM = "®";

}
