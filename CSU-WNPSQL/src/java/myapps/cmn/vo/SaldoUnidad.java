/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author Jose Luis
 */
public class SaldoUnidad implements Serializable{
      private static final long serialVersionUID = 1L;

    private double saldo;
    private String saldoTexto;
    private String saldoUnidad;
    private String unidad;
    private int cantidadDecimales;
    private Calendar fechaExpiracion;
    private Calendar fechaExpiracionDos;
    private boolean whatsppIlimitado;

    public SaldoUnidad() {
    }

    public SaldoUnidad(double saldo, String saldoTexto, String saldoUnidad, String unidad, int cantidadDecimales, boolean whatsppIlimitado) {
        this.saldo = saldo;
        this.saldoTexto = saldoTexto;
        this.saldoUnidad = saldoUnidad;
        this.unidad = unidad;
        this.cantidadDecimales = cantidadDecimales;
        this.whatsppIlimitado = whatsppIlimitado;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getSaldoTexto() {
        return saldoTexto;
    }

    public void setSaldoTexto(String saldoTexto) {
        this.saldoTexto = saldoTexto;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public int getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(int cantidadDecimales) {
        this.cantidadDecimales = cantidadDecimales;
    }

    public String getSaldoUnidad() {
        return saldoUnidad;
    }

    public void setSaldoUnidad(String saldoUnidad) {
        this.saldoUnidad = saldoUnidad;
    }

    public Calendar getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Calendar fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public boolean isWhatsppIlimitado() {
        return whatsppIlimitado;
    }

    public void setWhatsppIlimitado(boolean whatsppIlimitado) {
        this.whatsppIlimitado = whatsppIlimitado;
    }

    public Calendar getFechaExpiracionDos() {
        return fechaExpiracionDos;
    }

    public void setFechaExpiracionDos(Calendar fechaExpiracionDos) {
        this.fechaExpiracionDos = fechaExpiracionDos;
    }

    @Override
    public String toString() {
        return "SaldoUnidad{" + "saldo=" + saldo + ", saldoTexto=" + saldoTexto + ", saldoUnidad=" + saldoUnidad + ", unidad=" + unidad + ", cantidadDecimales=" + cantidadDecimales + ", fechaExpiracion=" + fechaExpiracion + ", whatsppIlimitado=" + whatsppIlimitado + '}';
    }

}
