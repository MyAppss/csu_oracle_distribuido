/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Resultado implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Linea;
    private String codigo;
    private String mensaje;
    private List<Pantalla> pantallas;

    /**
     * @return the Linea
     */
    public String getLinea() {
        return Linea;
    }

    /**
     * @param Linea the Linea to set
     */
    public void setLinea(String Linea) {
        this.Linea = Linea;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the pantallas
     */
    public List<Pantalla> getPantallas() {
        return pantallas;
    }

    /**
     * @param pantallas the pantallas to set
     */
    public void setPantallas(List<Pantalla> pantallas) {
        this.pantallas = pantallas;
    }

    @Override
    public String toString() {
        return "Resultado{" + "Linea=" + Linea + ", codigo=" + codigo + ", mensaje=" + mensaje + ", pantallas=" + pantallas != null ? pantallas.toString() : "" + '}';
    }

}
