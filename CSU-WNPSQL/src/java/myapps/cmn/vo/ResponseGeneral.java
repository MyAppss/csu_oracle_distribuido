/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class ResponseGeneral implements Serializable{
      private static final long serialVersionUID = 1L;

    private Integer succcess;
    private String mensaje;
    private ResponseCCWS responseCCWS;

    public ResponseGeneral(Integer succcess, String mensaje, ResponseCCWS responseCCWS) {
        this.succcess = succcess;
        this.mensaje = mensaje;
        this.responseCCWS = responseCCWS;
    }

    public ResponseGeneral() {
    }

    public Integer getSucccess() {
        return succcess;
    }

    public void setSucccess(Integer succcess) {
        this.succcess = succcess;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public ResponseCCWS getResponseCCWS() {
        return responseCCWS;
    }

    public void setResponseCCWS(ResponseCCWS responseCCWS) {
        this.responseCCWS = responseCCWS;
    }

}
