/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vehimar
 */
public class Menu implements Serializable{
      private static final long serialVersionUID = 1L;

    private Integer id;
    private String nombre;
    private Integer posicion;
    private String mostrarSimpre;
    private Integer nivel;
    private Integer padre;
    private String descripcion;
    private String mostrarMayorCero;
    private String mostrarMayorCeroAcu;
    private String canal;
    private Integer configId;
    private String informacion;
    private Integer tipoUnidad;
    private String tieneHijos;

    private List<Billetera> listBilletera;

    public Menu() {
    }

    public Menu(Integer id, String nombre, Integer posicion, String mostraSiempre,
            Integer nivel, Integer padre, String descripcion, String mostrarMayorCero,
            String canal, Integer configId, String informacion) {
        this.id = id;
        this.nombre = nombre;
        this.posicion = posicion;
        this.mostrarSimpre = mostraSiempre;
        this.nivel = nivel;
        this.padre = padre;
        this.descripcion = descripcion;
        this.mostrarMayorCero = mostrarMayorCero;
        this.canal = canal;
        this.configId = configId;
        this.informacion = informacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getPadre() {
        return padre;
    }

    public void setPadre(Integer padre) {
        this.padre = padre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMostrarMayorCero() {
        return mostrarMayorCero;
    }

    public void setMostrarMayorCero(String mostrarMayorCero) {
        this.mostrarMayorCero = mostrarMayorCero;
    }

    public String getMostrarMayorCeroAcu() {
        return mostrarMayorCeroAcu;
    }

    public void setMostrarMayorCeroAcu(String mostrarMayorCeroAcu) {
        this.mostrarMayorCeroAcu = mostrarMayorCeroAcu;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public List<Billetera> getListBilletera() {
        return listBilletera;
    }

    public void setListBilletera(List<Billetera> listBilletera) {
        this.listBilletera = listBilletera;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public Integer getTipoUnidad() {
        return tipoUnidad;
    }

    public void setTipoUnidad(Integer tipoUnidad) {
        this.tipoUnidad = tipoUnidad;
    }

    public String getMostrarSimpre() {
        return mostrarSimpre;
    }

    public void setMostrarSimpre(String mostrarSimpre) {
        this.mostrarSimpre = mostrarSimpre;
    }

    public String getTieneHijos() {
        return tieneHijos;
    }

    public void setTieneHijos(String tieneHijos) {
        this.tieneHijos = tieneHijos;
    }

    @Override
    public String toString() {
        return "Menu{" + "id=" + id + ", nombre=" + nombre + ", posicion=" + posicion + ", visible=" + mostrarSimpre + ", nivel=" + nivel + ", padre=" + padre + ", descripcion=" + descripcion + ", mostrarMayorCero=" + mostrarMayorCero + ", canal=" + canal + ", configId=" + configId + ", informacion=" + informacion + ", listBilletera=" + listBilletera + '}';
    }

}
