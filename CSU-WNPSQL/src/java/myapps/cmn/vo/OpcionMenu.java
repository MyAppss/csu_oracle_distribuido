/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author hp
 */
public class OpcionMenu implements Serializable{
      private static final long serialVersionUID = 1L;

    private Integer opcion;
    private Integer menuId;

    public OpcionMenu(Integer opcion, Integer menuId) {
        this.opcion = opcion;
        this.menuId = menuId;
    }

    public Integer getOpcion() {
        return opcion;
    }

    public void setOpcion(Integer opcion) {
        this.opcion = opcion;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }
}
