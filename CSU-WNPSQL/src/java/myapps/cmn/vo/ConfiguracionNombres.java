/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class ConfiguracionNombres implements Serializable{
      private static final long serialVersionUID = 1L;

    private int configuracionNombresId;
    private int billeteraID;
    private int configuracionID;
    private String nombreComercial;
    private String nombreAcumulado;
    private String segundaFechaExp;

    public ConfiguracionNombres() {
    }

    public int getConfiguracionNombresId() {
        return configuracionNombresId;
    }

    public void setConfiguracionNombresId(int configuracionNombresId) {
        this.configuracionNombresId = configuracionNombresId;
    }

    public int getBilleteraID() {
        return billeteraID;
    }

    public void setBilleteraID(int billeteraID) {
        this.billeteraID = billeteraID;
    }

    public int getConfiguracionID() {
        return configuracionID;
    }

    public void setConfiguracionID(int configuracionID) {
        this.configuracionID = configuracionID;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNombreAcumulado() {
        return nombreAcumulado;
    }

    public void setNombreAcumulado(String nombreAcumulado) {
        this.nombreAcumulado = nombreAcumulado;
    }

    public String getSegundaFechaExp() {
        return segundaFechaExp;
    }

    public void setSegundaFechaExp(String segundaFechaExp) {
        this.segundaFechaExp = segundaFechaExp;
    }

}
