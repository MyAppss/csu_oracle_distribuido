/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Pantalla implements Serializable{
      private static final long serialVersionUID = 1L;

    private int idPantalla;
    private String cabecera;
    private String opciones;
    private List<Integer> ListaNroOpcion;
    private List<Navega> listaNavegacion;
    private Integer pantallaPadre;
    private Integer opcionPadre;

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    public String getOpciones() {
        return opciones;
    }

    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public Integer getPantallaPadre() {
        return pantallaPadre;
    }

    public void setPantallaPadre(Integer pantallaPadre) {
        this.pantallaPadre = pantallaPadre;
    }

    public Integer getOpcionPadre() {
        return opcionPadre;
    }

    public void setOpcionPadre(Integer opcionPadre) {
        this.opcionPadre = opcionPadre;
    }

    public int getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(int idPantalla) {
        this.idPantalla = idPantalla;
    }

    public List<Navega> getListaNavegacion() {
        return listaNavegacion;
    }

    public void setListaNavegacion(List<Navega> listaNavegacion) {
        this.listaNavegacion = listaNavegacion;
    }

    public List<Integer> getListaNroOpcion() {
        return ListaNroOpcion;
    }

    public void setListaNroOpcion(List<Integer> ListaNroOpcion) {
        this.ListaNroOpcion = ListaNroOpcion;
    }

    @Override
    public String toString() {
        return "Pantalla{" + "idPantalla=" + idPantalla + ", cabecera=" + cabecera + ", opciones=" + opciones + ", ListaNroOpcion=" + ListaNroOpcion + ", listaNavegacion=" + listaNavegacion + ", pantallaPadre=" + pantallaPadre + ", opcionPadre=" + opcionPadre + '}';
    }
    
    

}
