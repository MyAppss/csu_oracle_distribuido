package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

public class Config implements Serializable{
      private static final long serialVersionUID = 1L;

    private Integer configId;
    private String nombre;
    private String descripcion;
    private String saludoInicial;
    private String mostraVigencia;
    private String mostrarDpi;
    private String mostrarAcumuladosMegas;
    private String mostraBilleterasNoConfig;
    private String habilitado;
    private String estado;
    private String lineas;
    private String nombreAcumulado;
    //-------------------------------------------------------------------------
    private List<Billetera> listBilleterasSimples;
    private List<ComposicionBilletera> listBilleterasCompuestas;
    //-------------------------------------------------------------------------

    private List<Cabecera> listCabeceras;
    private List<Menu> listMenus;

    private List<ConfiguracionNombres> listConfiguracionNombres;

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String dato) {
        this.descripcion = dato;
    }

    public String getSaludoInicial() {
        return saludoInicial;
    }

    public void setSaludoInicial(String dato) {
        this.saludoInicial = dato;
    }

    public String getMostraVigencia() {
        return mostraVigencia;
    }

    public void setMostraVigencia(String dato) {
        this.mostraVigencia = dato;
    }

    public String getMostrarDpi() {
        return mostrarDpi;
    }

    public void setMostrarDpi(String dato) {
        this.mostrarDpi = dato;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String dato) {
        this.habilitado = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public List<ComposicionBilletera> getListBilleterasCompuestas() {
        return listBilleterasCompuestas;
    }

    public void setListBilleterasCompuestas(List<ComposicionBilletera> listBilleterasCompuestas) {
        this.listBilleterasCompuestas = listBilleterasCompuestas;
    }

    public List<Billetera> getListBilleterasSimples() {
        return listBilleterasSimples;
    }

    public void setListBilleterasSimples(List<Billetera> listBilleterasSimples) {
        this.listBilleterasSimples = listBilleterasSimples;
    }

    public String getMostraBilleterasNoConfig() {
        return mostraBilleterasNoConfig;
    }

    public void setMostraBilleterasNoConfig(String mostraBilleterasNoConfig) {
        this.mostraBilleterasNoConfig = mostraBilleterasNoConfig;
    }

    public String getMostrarAcumuladosMegas() {
        return mostrarAcumuladosMegas;
    }

    public void setMostrarAcumuladosMegas(String mostrarAcumuladosMegas) {
        this.mostrarAcumuladosMegas = mostrarAcumuladosMegas;
    }

    public List<Cabecera> getListCabeceras() {
        return listCabeceras;
    }

    public void setListCabeceras(List<Cabecera> listCabeceras) {
        this.listCabeceras = listCabeceras;
    }

    public List<Menu> getListMenus() {
        return listMenus;
    }

    public void setListMenus(List<Menu> listMenus) {
        this.listMenus = listMenus;
    }

    public String getLineas() {
        return lineas;
    }

    public void setLineas(String lineas) {
        this.lineas = lineas;
    }

    public String getNombreAcumulado() {
        return nombreAcumulado;
    }

    public void setNombreAcumulado(String nombreAcumulado) {
        this.nombreAcumulado = nombreAcumulado;
    }

    public List<ConfiguracionNombres> getListConfiguracionNombres() {
        return listConfiguracionNombres;
    }

    public void setListConfiguracionNombres(List<ConfiguracionNombres> listConfiguracionNombres) {
        this.listConfiguracionNombres = listConfiguracionNombres;
    }

}
