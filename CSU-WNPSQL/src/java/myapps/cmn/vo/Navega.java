/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author MyApps
 */
public class Navega implements Serializable{
      private static final long serialVersionUID = 1L;

    private int opcion;
    private int pantalla;

    public Navega(int opcion, int pantalla) {
        this.opcion = opcion;
        this.pantalla = pantalla;
    }

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    public int getPantalla() {
        return pantalla;
    }

    public void setPantalla(int pantalla) {
        this.pantalla = pantalla;
    }

}
