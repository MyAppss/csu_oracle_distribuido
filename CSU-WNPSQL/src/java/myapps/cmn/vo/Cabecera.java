/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vehimar Lopez
 */
public class Cabecera implements Serializable{
      private static final long serialVersionUID = 1L;

    private int cabeceraId;
    private String nombre;
    private String mostrarSiempre;
    private int posicion;
    private int configId;
    private String tipoNavegacion;
    private String mayorCero;
    private String whatsappIlimitado;
    private Integer unitType;

    private List<Billetera> listBilleteras;

    public Cabecera() {
    }

    public Cabecera(int cabeceraId, String nombre, String mostrarSiempre, int posicion, int configId,
            String tipoNavegacion, String mayorCero, String whatsappIlimitado) {
        this.cabeceraId = cabeceraId;
        this.nombre = nombre;
        this.mostrarSiempre = mostrarSiempre;
        this.posicion = posicion;
        this.configId = configId;
        this.tipoNavegacion = tipoNavegacion;
        this.mayorCero = mayorCero;
        this.whatsappIlimitado = whatsappIlimitado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMostrarSiempre() {
        return mostrarSiempre;
    }

    public void setMostrarSiempre(String visible) {
        this.mostrarSiempre = visible;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public int getCabeceraId() {
        return cabeceraId;
    }

    public void setCabeceraId(int cabeceraId) {
        this.cabeceraId = cabeceraId;
    }

    public List<Billetera> getListBilleteras() {
        return listBilleteras;
    }

    public void setListBilleteras(List<Billetera> listBilleteras) {
        this.listBilleteras = listBilleteras;
    }

    public String getTipoNavegacion() {
        return tipoNavegacion;
    }

    public void setTipoNavegacion(String tipoNavegacion) {
        this.tipoNavegacion = tipoNavegacion;
    }

    public String getMayorCero() {
        return mayorCero;
    }

    public void setMayorCero(String saldoCalculado) {
        this.mayorCero = saldoCalculado;
    }

    public String getWhatsappIlimitado() {
        return whatsappIlimitado;
    }

    public void setWhatsappIlimitado(String whatsappIlimitado) {
        this.whatsappIlimitado = whatsappIlimitado;
    }

    public Integer getUnitType() {
        return unitType;
    }

    public void setUnitType(Integer unitType) {
        this.unitType = unitType;
    }

    @Override
    public String toString() {
        return "Cabecera{" + "cabeceraId=" + cabeceraId + ", nombre=" + nombre + ", MostarSiempre=" + mostrarSiempre + ", posicion=" + posicion + ", configId=" + configId + ", tipoNavegacion=" + tipoNavegacion + ", MayorCero=" + mayorCero + ", whatsappIlimitado=" + whatsappIlimitado + ", listBilleteras=" + listBilleteras + '}';
    }

}
