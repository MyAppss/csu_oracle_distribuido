/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import myapps.cmn.vo.ResponseGeneral;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import myapps.activeMQ.ColaJMS;
import myapps.activeMQ.ColaThreadMax;
import myapps.activeMQ.EnviarMensajeJMS;

import myapps.cmn.dao.ParametrosDAO;
import myapps.cmn.util.Convertidor;
import myapps.cmn.util.ErrorCodes;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilDate;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Navega;
import myapps.cmn.vo.OpcionMenu;
import myapps.cmn.vo.Pantalla;
import myapps.cmn.vo.Parametro;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.Resultado;
import myapps.cmn.vo.ReporteConsulta;
import myapps.cmn.vo.WalletComverse;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import myapps.main.ClienteSpring;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class WvnConsultar {

    private static final Logger LOG = Logger.getLogger(WvnConsultar.class);
    private GestorConfiguraciones gestorConfiguraciones;
    private List<Pantalla> listaPantallas;
    private String sessionID = "";

    public WvnConsultar() {
        this.gestorConfiguraciones = GestorConfiguraciones.getInstance();
        this.listaPantallas = new ArrayList<>();
    }

    public String consultarSaldo(String sessionId, String msisdn, String canal, String corto, String logPrefijo, String ip) {
        long zz = System.currentTimeMillis();
        LOG.info("...:::" + logPrefijo + " INICIANDO METODO consultarSaldo para el msisdn: " + msisdn);
        this.sessionID = sessionId;
        String aux = gestorConfiguraciones.obtenerParametroBD("sistema_consulta_saldo_sw_enable", logPrefijo);
        boolean enable = Boolean.valueOf(aux);
        // System.out.println("...::: Session ID: " + sessionId + " TERMINO sistema_consulta_saldo_sw_enable: " + msisdn + " - " + (System.currentTimeMillis() - zz) + " ms :::...");
        LOG.debug("...:::" + logPrefijo + " TERMINO sistema_consulta_saldo_sw_enable: " + msisdn + " - " + (System.currentTimeMillis() - zz) + " milisegundos :::...");
        Resultado resultado = new Resultado();

        if (enable) {
            long a = System.currentTimeMillis();
            LOG.debug("...:::" + logPrefijo + "OBTENER RESPONSE GENERAL :::...");
            ResponseGeneral respGeneral = obtenerBilletaComverse(msisdn, this.sessionID, logPrefijo);
            LOG.debug("...:::" + logPrefijo + "RESPONSE GENERAL OBTENIDO EN: " + (System.currentTimeMillis() - a) + " milisegundos :::...");

            if (respGeneral != null) {
                if (respGeneral.getResponseCCWS() != null) {

                    ResponseCCWS responseComverse = respGeneral.getResponseCCWS();
                    LOG.debug(logPrefijo + " COS NAME:" + responseComverse.getNameCOS());
                    logPrefijo = logPrefijo + "[cos name: " + responseComverse.getNameCOS() + "]";
                    //Config actualConfig = gestorConfiguraciones.obtenerConfiguracionForOrigenCortoCos(msisdn, responseComverse.getNameCOS(), canal, corto, logPrefijo, sessionId);
                    Config actualConfig = gestorConfiguraciones.obtenerConfigByOrigenCortoCos(msisdn, responseComverse.getNameCOS(), canal, corto, logPrefijo, sessionId);

                    long a1 = System.currentTimeMillis();
                    LOG.debug("...:::" + logPrefijo + "CREAR PANTALLAS :::...");
                    if (actualConfig != null) {
                        listaPantallas = new ArrayList<>();
                        int idenficadorPantallas = 1;
                        long b = System.currentTimeMillis();
                        LOG.debug("...:::" + logPrefijo + "CREAR PANTALLA 1 :::....");
                        ConsultarPantallaNro1 consultar1 = new ConsultarPantallaNro1();
                        Pantalla pantallaNro1 = consultar1.consultarPantallaNro1(msisdn, canal, corto, 0, idenficadorPantallas, logPrefijo, responseComverse, actualConfig, this.sessionID);

                        listaPantallas.add(pantallaNro1);
                        LOG.debug("...:::" + logPrefijo + "PANTALLA 1 CREADA EN: " + (System.currentTimeMillis() - b) + " milisegundos :::...");

                        long c = System.currentTimeMillis();
                        LOG.debug("...:::" + logPrefijo + "CREAR PANTALLA 2 :::....");
                        HashMap<Integer, List<OpcionMenu>> MapOMP1 = new HashMap<>();
                        MapOMP1.put(pantallaNro1.getIdPantalla(), consultar1.getListaOMP());

                        HashMap<Integer, List<OpcionMenu>> MapOMP2 = new HashMap<>();
                        ConsultarPantallaNro2 consultar2 = new ConsultarPantallaNro2();
                        for (Map.Entry<Integer, List<OpcionMenu>> entry : MapOMP1.entrySet()) {
                            Integer idPantalla = entry.getKey();
                            List<OpcionMenu> listOM = entry.getValue();

                            for (OpcionMenu opcionMenu : listOM) {
                                //for (int i = 0; i < listOM.size(); i++) {
                                //OpcionMenu opcionMenu = listOM.get(i);

                                idenficadorPantallas++;
                                Pantalla pantallaNro2 = consultar2.consultarPantallaNro2(msisdn, canal, corto, opcionMenu.getOpcion(), opcionMenu.getMenuId(), idPantalla, idenficadorPantallas, logPrefijo, responseComverse, actualConfig, this.sessionID);
                                listaPantallas.add(pantallaNro2);
                                MapOMP2.put(pantallaNro2.getIdPantalla(), consultar2.getListaOMP());
                            }
                        }
                        LOG.debug("...:::" + logPrefijo + "PANTALLA 2 CREADA EN: " + (System.currentTimeMillis() - c) + " milisegundos :::...");

                        long d = System.currentTimeMillis();
                        LOG.debug("...:::" + logPrefijo + "CREAR PANTALLA 3 :::....");
                        ConsultarPantallaNro3 consultar3 = new ConsultarPantallaNro3();
                        for (Map.Entry<Integer, List<OpcionMenu>> entry : MapOMP2.entrySet()) {
                            Integer idPantalla = entry.getKey();
                            List<OpcionMenu> listOM = entry.getValue();
                            for (OpcionMenu opcionMenu : listOM) {
                                //for (int i = 0; i < listOM.size(); i++) {
                                //OpcionMenu opcionMenu = listOM.get(i);
                                idenficadorPantallas++;
                                Pantalla pantallaNro3 = consultar3.consultarPantallaNro3(msisdn, canal, corto, opcionMenu.getOpcion(), opcionMenu.getMenuId(), idPantalla, idenficadorPantallas, logPrefijo, responseComverse, actualConfig, this.sessionID);
                                listaPantallas.add(pantallaNro3);
                            }
                        }
                        LOG.debug("...:::" + logPrefijo + "PANTALLA 3 CREADA EN: " + (System.currentTimeMillis() - d) + " milisegundos :::...");

                        HashMap<Integer, List<Navega>> mapNavega = obtenerNavega(listaPantallas, logPrefijo);
                        mapNavega.entrySet().stream().forEach((entry) -> {
                            Integer idPantalla = entry.getKey();
                            List<Navega> listNavega = entry.getValue();
                            if (listNavega != null) {
                                colocarNavega(idPantalla, listNavega);
                            }
                        });
                        resultado.setCodigo(String.valueOf(ErrorCodes.CODE_CONSULTA_SATIFACTORIA));
                        resultado.setMensaje(ErrorCodes.MSG_CONSULTA_SATISFACTORIA);
                        resultado.setLinea(msisdn);
                        resultado.setPantallas(listaPantallas);

                    } else {
                        String msj = gestorConfiguraciones.obtenerParametroBD("default_respuesta_usuario_no_comverse", logPrefijo);
                        msj = msj.replace("%TELEFONO%", msisdn);
                        resultado.setCodigo(String.valueOf(ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE));
                        resultado.setMensaje(msj);
                        resultado.setLinea(msisdn);
                        resultado.setPantallas(null);
                        //resultado.setPantallas(new ArrayList());
                        LOG.debug(logPrefijo + " Resultado: " + msj);
                    }
                    LOG.debug("...:::" + logPrefijo + "PANTALAS CREADAS EN: " + (System.currentTimeMillis() - a1) + " milisegundos :::...");

                } else {
                    resultado = obtenerMensajes(respGeneral, msisdn, logPrefijo);
                }
            } else {
                LOG.error(logPrefijo + "ERROR AL OBTENER respGeneral ");
            }
        } else {
            /*MENSAJE DE FUERA DE SERVICIO*/
            String msj = gestorConfiguraciones.obtenerParametroBD("default_respuesta_fuera_servicio", logPrefijo);
            if ("".equals(msj)) {
                msj = Propiedades.DEFAUL_ERROR_CONEXION;
                msj = msj.replace("%TELEFONO%", msisdn);
            } else {
                msj = msj.replace("%TELEFONO%", msisdn);
            }
            resultado.setCodigo(Propiedades.DEFAUL_CODIGO_CONEXION);
            resultado.setMensaje(msj);
            resultado.setLinea(msisdn);
            resultado.setPantallas(null);
          //  resultado.setPantallas(new ArrayList<>());
            LOG.debug(msj);
        }

        long e = System.currentTimeMillis();
        LOG.debug("...:::" + logPrefijo + "CREAR JSON :::...");
        Gson gson = new Gson();

        if (resultado != null && resultado.getPantallas() != null) {
            for (Pantalla pantalla : resultado.getPantallas()) {
                if (pantalla.getOpciones() == null || pantalla.getOpciones().isEmpty()) {
                    pantalla.setOpciones(Propiedades.VALOR);
                }

            }
        }

        boolean resultado1 = Convertidor.consultaValida(resultado);
        if (Boolean.TRUE.equals(resultado1)) {
            LOG.debug("resultado valido");
        } else {
            LOG.info("VALOR ACTUAL: " + resultado.toString());
            LOG.info("resultado necesita moificar");
            resultado = Convertidor.modificarResultado(resultado);
        }
        String representacionJSON = gson.toJson(resultado);
        LOG.debug(logPrefijo + "JSON CREADO EN: " + (System.currentTimeMillis() - e) + " milisegundos :::...");
        //GUARDAR EN BD EL RESPONSE

        ReporteConsulta RC = new ReporteConsulta();
        RC.setFecha(UtilDate.getTimetampActual());
        RC.setIdentificador_servicio(corto);
        RC.setIp_cliente(ip);
        RC.setLongitud_max_solicitada(1000);
        RC.setMsisdn(msisdn);
        RC.setNombre_canal(canal);
        RC.setPublicidad_solicitada("f");
        if (resultado.getCodigo().equals(String.valueOf(ErrorCodes.CODE_CONSULTA_SATIFACTORIA))) {
            RC.setTexto_generado(representacionJSON);
        } else {
            RC.setTexto_generado(resultado.getMensaje());
        }
        RC.setSessionId(sessionId);
        RC.setOpcion("");
        HashMap<String, Object> mensaje = new HashMap<>();
        mensaje.put("CONSULTA", RC);

        if (ColaThreadMax.size() > 0) {
            LOG.debug(logPrefijo + "Iniciando Hilo");
            EnviarMensajeJMS jms = new EnviarMensajeJMS(mensaje, "[JMS]" + logPrefijo);
            jms.start();
        } else {
            LOG.warn(logPrefijo + "Mandando a la Cola");
            ColaJMS.put(mensaje);
        }

        /* long f = System.currentTimeMillis();
                    LOG.info("...::: GUARDAR CONSULTA :::...");
                    ReporteConsulta RC = new ReporteConsulta();
                    RC.setFecha(UtilDate.getTimetampActual());
                    RC.setIdentificador_servicio(corto);
                    RC.setIp_cliente(ip);
                    RC.setLongitud_max_solicitada(1000);
                    RC.setMsisdn(msisdn);
                    RC.setNombre_canal(canal);
                    RC.setPublicidad_solicitada("f");
                    RC.setTexto_generado(representacionJSON);
                    RC.setSessionId(sessionId);
                    RC.setOpcion("");
                    reporteConsultaDAO.saveReporteConsulta(RC);
                    LOG.info("...:::CONSULTA GUARDADA EN:" + (System.currentTimeMillis() - f) + " milisegundos :::...");
        }*/
        LOG.debug("...::: FINALIZO DE CONSULTAR :" + logPrefijo + " TIEMPO: " + (System.currentTimeMillis() - zz) + " milisegundos :::...");

        return representacionJSON;
    }

    private Navega obtenerNavega(Pantalla pantalla) {
        Navega aux = new Navega(pantalla.getOpcionPadre(), pantalla.getIdPantalla());
        return aux;
    }

    private HashMap<Integer, List<Navega>> obtenerNavega(List<Pantalla> listaPantallas, String logPrefijo) {
        long z = System.currentTimeMillis();
        HashMap<Integer, List<Navega>> hmap = new HashMap<>();
        for (Pantalla pantalla : listaPantallas) {
            //for (int i = 1; i < listaPantallas.size(); i++) {
            //Pantalla pantalla = listaPantallas.get(i);
            hmap.put(pantalla.getIdPantalla(), new ArrayList<>());
        }

        for (Pantalla pantalla : listaPantallas) {
            //for (int i = 1; i < listaPantallas.size(); i++) {
            //Pantalla pantalla = listaPantallas.get(i);
            Navega navega = obtenerNavega(pantalla);
            List<Navega> listAux = hmap.get(pantalla.getPantallaPadre());
            if (listAux == null) {
                listAux = new ArrayList<>();
            }
            listAux.add(navega);
            hmap.put(pantalla.getPantallaPadre(), listAux);
        }
        LOG.debug("...:::" + logPrefijo + "TERMINO CREAR LA LISTA DE NAVEGA: " + (System.currentTimeMillis() - z) + " ms :::...");
        // System.out.println("...::: Session ID: " + sessionId + " TERMINO CREAR LA LISTA DE NAVEGA: " + (System.currentTimeMillis() - z) + " ms :::...");
        return hmap;
    }

    private void colocarNavega(Integer idPantalla, List<Navega> navega) {
        long z = System.currentTimeMillis();
        for (Pantalla aux : listaPantallas) {
            //for (int i = 0; i < listaPantallas.size(); i++) {
            //Pantalla aux = listaPantallas.get(i);
            if (idPantalla == aux.getIdPantalla()) {
                aux.setListaNavegacion(navega);
            }
        }
        LOG.debug("...::: TERMINO DE SETERAR LOS NAVEGA A CADA PANT: " + (System.currentTimeMillis() - z) + " :::...");
    }

    private ResponseCCWS ObtenerData(String isdn, String logPrefijo) {
        ResponseCCWS responseCCws = null;
        try {
            if (Propiedades.GQL_BANDERA) {
                LOG.debug("....:::" + logPrefijo + "INICIANDO OBTENER BILLETERAS DEL SERVICIO DE GQL  Session ID:" + this.sessionID + " ISDN: " + isdn + " :::...");
                long tInicio = System.currentTimeMillis();
                ClienteSpring clienteSpring = new ClienteSpring();
                responseCCws = (ResponseCCWS) clienteSpring.obtenerResponseCCWS("591" + isdn, sessionID);
                LOG.debug(logPrefijo + "TIEMPO DE OBTENCION DE BILLETERAS DEL SERVICIO GQL " + (System.currentTimeMillis() - tInicio) + " ms");
            } else {
                responseCCws = obtenerDataCCWS(isdn, logPrefijo);
            }
            //Vigencia Maxima
            if (Boolean.TRUE.equals(Propiedades.VM_BANDERA) && responseCCws != null && responseCCws.getListWallet() != null) {
                long tInicio = System.currentTimeMillis();
                MethodsCBS cbs = new MethodsCBS();
                Calendar fecha = cbs.obtenerFechaCbs(logPrefijo, isdn, this.sessionID);
                if (fecha != null) {
                    LOG.debug("Fecha de Expiracion Obtenida de CBS: " + fecha);
                    for (WalletComverse billetera : responseCCws.getListWallet()) {
                        if (cbs.getBilleterasAfectadas().contains(billetera.getNameWallet())) {
                            LOG.debug(logPrefijo + "Billetera: " + billetera.toString() + " afectada por fecha cbs: " + fecha.toString());
                            billetera.setExpiration(fecha);
                        }
                    }
                } else {
                    LOG.warn(logPrefijo + "ERROR AL OBTENER LA FECHA DE CBS null");
                }

                LOG.debug(logPrefijo + " Tiempo total cambios Vigencia Maxima :" + (System.currentTimeMillis() - tInicio) + " ms.");
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[obtenerData] al intentar recuperar dat:" + e.getMessage(), e);
            String error = e.getMessage();
            if (error != null) {
                responseCCws = new ResponseCCWS();
                responseCCws.setNameCOS("ERROR");
                if (error.contains("Could not send Message")) {
                    responseCCws.setErrorDescription("Read timed out");
                    responseCCws.setListWallet(null);
                } else if (error.contains("Read timed out")) {
                    responseCCws.setErrorDescription("ConnectionErrorTimeOut");
                    responseCCws.setListWallet(null);
                } else if (error.contains("does not exist") || error.contains("Invalid QName in mapping: ns0:Client")) {
                    responseCCws.setErrorDescription("Subscriber not found");
                    responseCCws.setListWallet(null);
                } else {
                    responseCCws.setErrorDescription("ConnectionError");
                    responseCCws.setListWallet(null);
                }
            } else {
                if (responseCCws == null) {
                    responseCCws = new ResponseCCWS();
                } else {
                    responseCCws.setErrorDescription("ConnectionError");
                    responseCCws.setListWallet(null);
                }
            }
        }
        return responseCCws;

    }

    private ResponseCCWS obtenerDataCCWS(String isdn, String logPrefijo) {
        LOG.debug("....:::" + logPrefijo + "INICIANDO OBTENER BILLETERAS DE CCWS  Session ID:" + this.sessionID + " ISDN: " + isdn + " :::...");
        ResponseCCWS RCMV = null;
        try {
            if (Propiedades.CCWS_MODO_PRUEBA.equals("1")) {
                RCMV = MethodsCCWS.obtenerListatBalanceWalletSimular(isdn);
            } else {
                RCMV = MethodsCCWS.obtenerListaBalanceWallet(isdn, this.sessionID, logPrefijo);
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[obtenerDataCCWS] al intentar recuperar data de CCWS:" + e.getMessage());
        }
        return RCMV;
    }

    private ResponseGeneral obtenerBilletaComverse(String msisdn, String sessionId, String logPrefijo) {
        long z = System.currentTimeMillis();

        /*  if (isChangeConfiguraciones(sessionId)) {
            this.gestorConfiguraciones.reset();
            LOG.info("[isdn: " + msisdn + "] ***** SE ESTA CARGARDO NUEVAMENTE LAS CONFIGURACIONES+++++++++");
        }*/
        ResponseCCWS responseComverse = ObtenerData(msisdn, logPrefijo);

        ResponseGeneral respGeneral = new ResponseGeneral();
        if (responseComverse != null) {
            switch (responseComverse.getErrorDescription()) {
                case "Subscriber not found": {
                    String MsgUsuarioNoComverse = gestorConfiguraciones.obtenerParametroBD("default_respuesta_usuario_no_comverse", logPrefijo);
                    LOG.debug(logPrefijo + "[isdn: " + msisdn + "] |SUBSCRIBER NOT FOUND| devolviendo mensaje:|" + MsgUsuarioNoComverse + "|");
                    respGeneral.setSucccess(ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE);
                    respGeneral.setMensaje(ErrorCodes.MSG_ERROR_USUARIO_NO_COMVERSE);
                    MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                    respGeneral.setMensaje(MsgUsuarioNoComverse);
                    respGeneral.setResponseCCWS(null);
                    LOG.error(logPrefijo + "Cuenta: [" + msisdn + "] " + responseComverse.getErrorDescription());
                    LOG.debug("");
                    break;
                }
                case "ConnectionError": {
                    String MsgUsuarioNoComverse = gestorConfiguraciones.obtenerParametroBD("default_respuesta_falla_conex_comverse", logPrefijo);
                    LOG.debug(logPrefijo + "[isdn: " + msisdn + "] |TSB NOT AVAILABLE | devolviento mensaje:|" + MsgUsuarioNoComverse + "|");
                    respGeneral.setSucccess(ErrorCodes.CODE_ERROR_COMVERSE_CONEX);
                    respGeneral.setMensaje(ErrorCodes.MSG_ERROR_CONVERSE_CONEX);
                    MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                    respGeneral.setMensaje(MsgUsuarioNoComverse);
                    respGeneral.setResponseCCWS(null);
                    LOG.error(logPrefijo + "Cuenta: [" + msisdn + "] " + responseComverse.getErrorDescription());
                    LOG.debug("");
                    break;
                }
                case "Read timed out": {
                    String MsgUsuarioNoComverse = gestorConfiguraciones.obtenerParametroBD("default_respuesta_falla_conex_comverse", logPrefijo);
                    LOG.debug(logPrefijo + "[isdn: " + msisdn + "] |TSB TIMEOUT SUPERADO| devolviento mensaje:|" + MsgUsuarioNoComverse + "|");
                    respGeneral.setSucccess(ErrorCodes.CODE_ERROR_COMVERSE_CONEX);
                    respGeneral.setMensaje(ErrorCodes.MSG_ERROR_CONVERSE_CONEX);
                    MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                    respGeneral.setMensaje(MsgUsuarioNoComverse);
                    respGeneral.setResponseCCWS(null);
                    LOG.error(logPrefijo + "Cuenta: [" + msisdn + "] " + responseComverse.getErrorDescription());
                    LOG.debug("");
                    break;
                }
                case "ConnectionErrorTimeOut": {
                    String MsgUsuarioNoComverse = gestorConfiguraciones.obtenerParametroBD("default_respuesta_falla_conex_comverse", logPrefijo);
                    LOG.debug(logPrefijo + "[isdn: " + msisdn + "] |TSB TIMEOUT SUPERADO| devolviento mensaje:|" + MsgUsuarioNoComverse + "|");
                    respGeneral.setSucccess(ErrorCodes.CODE_ERROR_COMVERSE_CONEX);
                    respGeneral.setMensaje(ErrorCodes.MSG_ERROR_CONVERSE_CONEX);
                    MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                    respGeneral.setMensaje(MsgUsuarioNoComverse);
                    respGeneral.setResponseCCWS(null);
                    LOG.error(logPrefijo + "Cuenta: [" + msisdn + "] " + responseComverse.getErrorDescription());
                    LOG.debug("");
                    break;
                }
                default: {
                    respGeneral.setSucccess(ErrorCodes.CODE_CONSULTA_SATIFACTORIA);
                    respGeneral.setMensaje(ErrorCodes.MSG_CONSULTA_SATISFACTORIA);
                    respGeneral.setResponseCCWS(responseComverse);
                    LOG.debug(logPrefijo + "Cuenta: [" + msisdn + "] CONSULTA EXITOSA HACIA CCWS: " + msisdn);
                    LOG.debug("");
                    break;
                }
            }
            //   System.out.println("...::: Session ID: " + sessionId + " TERMINO CONSTRUIR RESPONSE GENERAL: " + (System.currentTimeMillis() - z) + " ms :::...");

        } else {
            respGeneral = null;
        }

        //  Gson gson = new Gson();
        //String representacionJSON = gson.toJson(responseComverse);
        LOG.debug("...:::" + logPrefijo + "TERMINO CONSTRUIR RESPONSE GENERAL: " + (System.currentTimeMillis() - z) + " ms :::...");
        return respGeneral;
    }

    private Resultado obtenerMensajes(ResponseGeneral respGeneral, String msisdn, String logPrefijo) {
        Resultado resultado = new Resultado();
        switch (respGeneral.getSucccess()) {
            case ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE: {
                String msj = gestorConfiguraciones.obtenerParametroBD("default_respuesta_usuario_no_comverse", logPrefijo);
                msj = msj.replace("%TELEFONO%", msisdn);
                resultado.setCodigo(String.valueOf(ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE));
                resultado.setMensaje(msj);
                resultado.setLinea(msisdn);
                resultado.setPantallas(null);
                break;
            }
            case ErrorCodes.CODE_ERROR_COMVERSE_CONEX: {
                String msj = gestorConfiguraciones.obtenerParametroBD("default_respuesta_fuera_servicio", logPrefijo);
                msj = msj.replace("%TELEFONO%", msisdn);
                resultado.setCodigo(String.valueOf(ErrorCodes.CODE_ERROR_FUERA_SERVICIO));
                resultado.setMensaje(msj);
                resultado.setLinea(msisdn);
                resultado.setPantallas(null);
                break;
            }
        }
        return resultado;
    }

    private boolean isChangeConfiguraciones(String sessionID, String logPrefijo) {
        long z = System.currentTimeMillis();
        boolean sw;

        try {
            Parametro param = ParametrosDAO.obtenerByName("sistema_consulta_saldo_sw_onchange");
            sw = param != null && param.getValor().equals("true");
        } catch (Exception e) {
            LOG.error(logPrefijo + "[IsChangeConfiguraciones] " + e.getMessage());
            sw = false;
        }
        LOG.info("...:::" + logPrefijo + "TERMINO CONSULTAR SI HAY CAMBIO SW_ONCHANGE: " + (System.currentTimeMillis() - z) + " ms :::...");
        //   System.out.println("...::: Session ID: " + sessionID + " TERMINO CONSULTAR SI HAY CAMBIO SW_ONCHANGE: " + (System.currentTimeMillis() - z) + " ms :::...");
        return sw;
    }

    /*public static void main(String[] args) {
        IniciarPool a = new IniciarPool();
        a.crearConexiones();
        String logPrefijo = "[isdn: " + "76316685" + "][canal: " + "USSD" + "][ip: " + "172.31.86.53" + "]";
        LOG.debug(logPrefijo);
        WvnConsultar jsonConsultar = new WvnConsultar();
        //  String json = jsonConsultar.consultarSaldo("000000111", "77089837", "USSD", "*189#", logPrefijo, "172.31.86.53");
        String json = jsonConsultar.consultarSaldo("000000111", "77816006", "USSD", "*189#", logPrefijo, "172.31.86.53");
         LOG.debug(json);
    }*/
}
