/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import com.comverse_in.prepaid.ccws.AccumulatorEntity;
import com.comverse_in.prepaid.ccws.ArrayOfBalanceEntity;
import com.comverse_in.prepaid.ccws.BalanceEntity;
import com.comverse_in.prepaid.ccws.RetrieveSubscriberWithIdentityNoHistory;
import com.comverse_in.prepaid.ccws.RetrieveSubscriberWithIdentityNoHistoryResponse;
import com.comverse_in.prepaid.ccws.SubscribedOffer;
import com.comverse_in.prepaid.ccws.SubscriberEntity;
import com.comverse_in.prepaid.ccws.SubscriberRetrieve;
import com.tigo.ea.ccws.iingresotarjetavoucher.IIngresoTarjetaVoucher;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilDate;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.WalletComverse;
import myapps.pool.cws.ConexionCcws;
import myapps.pool.cws.Pool;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class MethodsCCWS {

    public static final Logger LOG = Logger.getLogger(MethodsCCWS.class);

    public static ResponseCCWS obtenerListaBalanceWallet(String isdn, String sessionId, String logPrefijo) {

        LOG.debug(logPrefijo + "|ISDN=" + isdn + "|CCWS: Inicio de consulta para obtener billeteras");
        ResponseCCWS response = null;
        List<WalletComverse> list;
        List<Offer> listOffer;
        List<Acumulador> listAcumulador;
        ConexionCcws conexion = null;
        try {
            LOG.debug(logPrefijo + "Iniciando metodo obtenerListaBalanceWallet de COMVERSE para NroCliente=" + isdn);
            conexion = Pool.poll();
            long contador = 0;
            long sleep = 500;

            while (conexion == null && ((contador * sleep) < Propiedades.timeoutConexion)) {
                LOG.debug(logPrefijo + "ConexionObtenidadDelPool resuelto nulo, contador: " + contador);
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    LOG.error(logPrefijo + "Error en el spleep: " + e.getMessage());
                    Thread.currentThread().interrupt();
                }
                conexion = Pool.poll();
                contador++;
            }

            if (conexion != null) {
                LOG.debug(logPrefijo + "IdConexionObtenida: " + conexion.getId());
                long tiempoInicial = System.currentTimeMillis();
                IIngresoTarjetaVoucher asmx = conexion.getPort();
                RetrieveSubscriberWithIdentityNoHistory noHistory = new RetrieveSubscriberWithIdentityNoHistory();

                noHistory.setIdentity("Personal");
                noHistory.setSubscriberID(isdn);
                noHistory.setInformationToRetrieve(1);
                RetrieveSubscriberWithIdentityNoHistoryResponse responseNoHistory = asmx.retrieveSubscriberWithIdentityNoHistory(noHistory, null);
                LOG.info("...:::" + logPrefijo + " TERMINO CONSUMIR SERVICIO DEL CCWS: " + (System.currentTimeMillis() - tiempoInicial) + " ms :::...");

                if (responseNoHistory != null) {
                    SubscriberRetrieve suscriber = responseNoHistory.getRetrieveSubscriberWithIdentityNoHistoryResult();
                    SubscriberEntity entity = suscriber.getSubscriberData();
                    ArrayOfBalanceEntity arrayBalances = entity.getBalances();
                    List<BalanceEntity> listBalance = arrayBalances.getBalance();

                    response = new ResponseCCWS();
                    list = new ArrayList<>();

                    for (BalanceEntity balanceEntity : listBalance) {
                        //for (int i = 0; i < listBalance.size(); i++) {
                        //BalanceEntity balanceEntity = listBalance.get(i);

                        WalletComverse wallet = new WalletComverse();
                        wallet.setNameWallet(balanceEntity.getBalanceName());
                        wallet.setAvailableBalance(Double.valueOf(balanceEntity.getAvailableBalance()));
                        wallet.setExpiration(UtilDate.xmlGregoriaToCalendar(balanceEntity.getAccountExpiration()));
                        wallet.setUsada(false);
                        LOG.debug(logPrefijo + "Recuperado -> de CCWS para ISDN " + isdn + ": " + wallet.getNameWallet() + " valor=" + wallet.getAvailableBalance() + ", fechaExpiracion: " + UtilDate.dateToString(wallet.getExpiration().getTime(), "dd-MM-yyyy HH:mm:ss", logPrefijo));
                        list.add(wallet);
                    }

                    List<SubscribedOffer> listOffertSubscribed = entity.getOffer();
                    listOffer = new ArrayList<>();
                    if (listOffertSubscribed != null) {

                        for (SubscribedOffer subscribedOffer : listOffertSubscribed) {
                            //for (int i = 0; i < listOffertSubscribed.size(); i++) {
                            //SubscribedOffer subscribedOffer = listOffertSubscribed.get(i);

                            if (subscribedOffer.getState() != null && subscribedOffer.getState().value().equalsIgnoreCase("ENABLED")) {
                                Offer offer = new Offer();
                                offer.setName(subscribedOffer.getName());
                                offer.setServiceStart(UtilDate.xmlGregoriaToCalendar(subscribedOffer.getServiceStart()));
                                offer.setServiceEnd(UtilDate.xmlGregoriaToCalendar(subscribedOffer.getServiceEnd()));
                                offer.setState(subscribedOffer.getState().value());
                                offer.setInstantiationTime(UtilDate.xmlGregoriaToCalendar(subscribedOffer.getInstantiationTime()));
                                offer.setUsada(false);
                                LOG.debug(logPrefijo + "Recuperado> offert de CCWS para ISDN " + isdn + ": " + offer.getName() + " valor=" + offer.getState() + ", fechaExpiracion: " + UtilDate.dateToString(offer.getServiceEnd().getTime(), "dd-MM-yyyy HH:mm:ss", logPrefijo));
                                listOffer.add(offer);
                            }
                        }
                    }
                    List<AccumulatorEntity> listAccumulator = entity.getAccumulator();
                    listAcumulador = new ArrayList<>();
                    if (listAccumulator != null) {
                        for (AccumulatorEntity accumulatorEntity : listAccumulator) {
                            //for (int i = 0; i < listAccumulator.size(); i++) {
                            //AccumulatorEntity accumulatorEntity = listAccumulator.get(i);
                            Acumulador acumulador = new Acumulador();
                            acumulador.setAccumulatorName(accumulatorEntity.getAccumulatorName());
                            acumulador.setAmount(Double.valueOf(accumulatorEntity.getAmount()));
                            LOG.debug(logPrefijo + "Recuperado -> accumulator de CCWS para Nro " + isdn + ": " + acumulador.getAccumulatorName() + " valor=" + acumulador.getAmount());
                            listAcumulador.add(acumulador);
                        }
                    }

                    response.setErrorDescription("TransactionCorrect");
                    response.setListWallet(list);
                    response.setListOffer(listOffer);
                    response.setListAcumulador(listAcumulador);
                    response.setNameCOS(entity.getCOSName());
                    LOG.debug(logPrefijo + "|ISDN=" + isdn + "|COS: " + response.getNameCOS() + "|CCWS: Se recupera listaBilleteras con Size=" + list.size() + " elementos");


                }
            } else {
                LOG.error(logPrefijo + "No se pudo obtener una conexion disponible del pool en " + Propiedades.timeoutConexion + " ms");
                response = new ResponseCCWS();
                response.setNameCOS("ERROR");
                response.setErrorDescription("ConnectionErrorTimeOut");
                response.setListWallet(null);
            }
        } catch (RuntimeException e) {
            String error = e.getMessage();
            if (error != null) {
                response = new ResponseCCWS();
                response.setNameCOS("ERROR");
                if (error.contains("Could not send Message")) {
                    response.setErrorDescription("Read timed out");
                    response.setListWallet(null);
                } else if (error.contains("Read timed out")) {
                    response.setErrorDescription("ConnectionErrorTimeOut");
                    response.setListWallet(null);
                } else if (error.contains("does not exist") || error.contains("Invalid QName in mapping: ns0:Client")) {
                    response.setErrorDescription("Subscriber not found");
                    response.setListWallet(null);
                } else {
                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);
                }
            } else {
                if (response == null) {
                    response = new ResponseCCWS();
                } else {
                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);
                }
            }
            LOG.error(logPrefijo + "[obtenerListaBalanceWallet] SocketTimeoutException || RuntimeException : " + error, e);
        } catch (Exception e) {
            String error = e.getMessage();
            if (error != null) {
                response = new ResponseCCWS();
                response.setNameCOS("ERROR");
                if (error.contains("Subscriber not found")) {
                    response.setErrorDescription("Subscriber not found");
                    response.setListWallet(null);
                } else {

                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);

                }
            } else {
                if (response == null) {
                    response = new ResponseCCWS();
                } else {
                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);
                }
            }
            LOG.error(logPrefijo + "Error Exception: " + error);
        } finally {
            if (conexion != null) {
                Pool.put(conexion);
                LOG.debug(logPrefijo + "Conexion devuelta a la cola: " + conexion.getId());
            }
        }

        return response;
    }

    public static ResponseCCWS obtenerListatBalanceWalletSimular(String clientNumber) {
        LOG.debug("[getListBalanceWalletSimular] Iniciando..");
        ResponseCCWS RCWS = new ResponseCCWS();
        String message = "TransactionCorrect";

        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 1);

        List<WalletComverse> response = new LinkedList<>();
        WalletComverse w1 = new WalletComverse();
        w1.setNameWallet("CORE BALANCE");
        w1.setUsada(false);
        //w1.setAvailableBalance(0.4905901234);
        w1.setAvailableBalance(12);
        w1.setExpiration(c);
        response.add(w1);

        WalletComverse w2 = new WalletComverse();
        w2.setNameWallet("CORE_BALANCE_2");
        w2.setAvailableBalance(23);
        w2.setExpiration(c);
        w2.setUsada(false);
        response.add(w2);

        WalletComverse w3 = new WalletComverse();
        w3.setNameWallet("SMS_ICX");
        w3.setAvailableBalance(20);
        w3.setExpiration(c);
        w3.setUsada(false);
        response.add(w3);

        WalletComverse w4 = new WalletComverse();
        w4.setNameWallet("MMS");
        w4.setAvailableBalance(200);
        w4.setExpiration(c);
        w4.setUsada(false);
        response.add(w4);

        WalletComverse w5 = new WalletComverse();
        w5.setNameWallet("Broadband_Currency");
        w5.setAvailableBalance(34);
        w5.setExpiration(c);
        w5.setUsada(false);
        response.add(w5);

        WalletComverse w6 = new WalletComverse();
        w6.setNameWallet("Blackberry");
        w6.setAvailableBalance(8.587654);
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, 2);
        w6.setExpiration(c1);
        w6.setUsada(false);
        response.add(w6);

        WalletComverse w7 = new WalletComverse();
        w7.setNameWallet("SMS_PROMO_CARGA");
        w7.setAvailableBalance(150);
        w7.setExpiration(c);
        w7.setUsada(false);
        response.add(w7);

        WalletComverse w8 = new WalletComverse();
        w8.setNameWallet("SMS_TEMPORAL");
        w8.setAvailableBalance(20);
        w8.setExpiration(c);
        w8.setUsada(false);
        response.add(w8);

        WalletComverse w9 = new WalletComverse();
        w9.setNameWallet("PROMO_SMS_TEMP");
        w9.setAvailableBalance(3);
        w9.setExpiration(c);
        w9.setUsada(false);
        response.add(w9);

        WalletComverse w10 = new WalletComverse();
        w10.setNameWallet("Productos_VAS");
        w10.setExpiration(c);
        w10.setUsada(false);
        response.add(w10);

        WalletComverse w11 = new WalletComverse();
        w11.setNameWallet("SALDO_PROMO_CARGA");
        w11.setExpiration(c);
        w11.setUsada(false);
        response.add(w11);

        WalletComverse w12 = new WalletComverse();
        w12.setNameWallet("VOZ_SMS");
        w12.setExpiration(c);
        w12.setUsada(false);
        response.add(w12);

        WalletComverse w13 = new WalletComverse();
        w13.setNameWallet("LLAM_MOV_LOC");
        w13.setExpiration(c);
        w13.setAvailableBalance(20);
        w13.setUsada(false);
        response.add(w13);

        WalletComverse w14 = new WalletComverse();
        w14.setNameWallet("PROMO_SMS_ICX");
        w14.setExpiration(c);
        w14.setUsada(false);
        response.add(w14);

        WalletComverse w15 = new WalletComverse();
        w15.setNameWallet("GPRS_PROMO SALDO");
        w15.setExpiration(c);
        w15.setUsada(false);
        response.add(w15);

        WalletComverse w16 = new WalletComverse();
        w16.setNameWallet("PROMO_SMSP2P_XTREMO");
        w16.setExpiration(c);
        w16.setUsada(false);
        response.add(w16);

        WalletComverse w17 = new WalletComverse();
        w17.setNameWallet("PROMO_ANTICHURN");
        w17.setExpiration(c);
        w17.setUsada(false);
        response.add(w17);

        WalletComverse w18 = new WalletComverse();
        w18.setNameWallet("PROMO_LDI");
        w18.setExpiration(c);
        w18.setUsada(false);
        response.add(w18);

        WalletComverse w19 = new WalletComverse();
        w19.setNameWallet("GPRS_SUBSCRIPTIONS");
        w19.setAvailableBalance(50);
        w19.setExpiration(c);
        w19.setUsada(false);
        response.add(w19);

        WalletComverse w20 = new WalletComverse();
        w20.setNameWallet("LLAMADA_SEGUNDOS");
        w20.setAvailableBalance(130);
        w20.setExpiration(c);
        w20.setUsada(false);
        response.add(w20);

        WalletComverse w21 = new WalletComverse();
        w21.setNameWallet("LLAMADA_MINUTOS");
        w21.setAvailableBalance(20);
        w21.setExpiration(c);
        w21.setUsada(false);
        response.add(w21);

        RCWS.setErrorDescription(message);
        RCWS.setListWallet(response);
//        RCWS.setNameCOS("PRE_PAGO");
        RCWS.setNameCOS("TIGO_PREPAGO");
//        RCWS.setNameCOS("FACTURA_FIJA");
        LOG.debug("[getListBalanceWalletSimular] Finalizando..");

        List<Offer> listOferta = new ArrayList<>();
        List<Acumulador> listAcumulador = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        Offer offert = new Offer();
        offert.setName("MINUTOS_LIBRES");
        offert.setState("ENABLED");
        offert.setServiceEnd(cal);
        offert.setUsada(false);

        Offer offert1 = new Offer();
        offert1.setName("INTERNET_TOTAL");
        offert1.setState("ENABLED");
        offert1.setServiceEnd(cal);
        offert1.setUsada(false);

        listOferta.add(offert);
        listOferta.add(offert1);

        Acumulador acum = new Acumulador();
        acum.setAccumulatorName("ACUM_MINUTOS_LIBRES");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_A");
        acum.setAmount(10);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUM_INTERNET_TOTAL");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_B");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_C");
        acum.setAmount(30);

        listAcumulador.add(acum);

        RCWS.setListOffer(listOferta);
        RCWS.setListAcumulador(listAcumulador);

        return RCWS;
    }

    /* public static boolean ContainCodeError(String DescriptionError) {
        int i = 0;
        try {
            if (DescriptionError != null) {
                List<String> listErrors = Propiedades.ListaCodesErrorCMV;
                while (i < listErrors.size()) {
                    String AuxError = listErrors.get(i);
                    if (DescriptionError.contains(AuxError)) {
                        return true;
                    }
                    i++;
                }
            }
        } catch (Exception e) {
            LOG.error("[ContainCodeError] verificando si contiene error Code :" + e.getMessage(), e);
        }
        return false;
    }*/
 /*public static void main(String[] args) {
        IniciarPool aa = new IniciarPool();
        aa.crearConexiones();

        ResponseCCWS a = MethodsCCWS.obtenerListaBalanceWallet("76316685");
        List<WalletComverse> list = a.getListWallet();
         for (int i = 0; i < list.size(); i++) {
         WalletComverse get = list.get(i);
         }
    }*/
}
