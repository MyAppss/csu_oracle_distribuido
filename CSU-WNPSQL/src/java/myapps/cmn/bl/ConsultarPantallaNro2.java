/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.util.Constantes;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilCcws2;
import myapps.cmn.util.UtilDate;
import myapps.cmn.util.UtilMontoMinimo;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.OpcionMenu;
import myapps.cmn.vo.Pantalla;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.WalletComverse;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ConsultarPantallaNro2 {

    public static final Logger LOG = Logger.getLogger(ConsultarPantallaNro2.class);
    private final GestorConfiguraciones gestorConfiguraciones;
    private List<OpcionMenu> listaOMP;

    public ConsultarPantallaNro2() {
        this.gestorConfiguraciones = GestorConfiguraciones.getInstance();
    }

    public List<OpcionMenu> getListaOMP() {
        return listaOMP;
    }

    public void setListaOMP(List<OpcionMenu> listaOMP) {
        this.listaOMP = listaOMP;
    }

    public Pantalla consultarPantallaNro2(String msisdn, String canal,
            String corto, Integer opcionAnt, Integer menuAnteriorId, Integer idPantFather,
            Integer idPantalla, String logPrefijo, ResponseCCWS responseComverse, Config actualConfig, String sessionId) {
        Pantalla pantalla = new Pantalla();

        long z = System.currentTimeMillis();
        this.listaOMP = new ArrayList<>();

        if (responseComverse != null && actualConfig != null) {
            List<Integer> listOpciones = new ArrayList<>();

            logPrefijo = logPrefijo + "[COS: " + responseComverse.getNameCOS() + "]";

            List<WalletComverse> listaBilleterasCMV = responseComverse.getListWallet();
            List<Offer> listOffer = responseComverse.getListOffer();
            List<Acumulador> listAcumulador = responseComverse.getListAcumulador();

            Menu menuAnterior = UtilCcws2.obtenerMenu(actualConfig, menuAnteriorId);

            String resultado = "";

            if (menuAnterior != null && menuAnterior.getInformacion().equals(Constantes.INFO_INFO_DE_RESERVA)) {
                resultado = menuAnterior.getDescripcion();
            } else if (menuAnterior != null && menuAnterior.getInformacion().equals(Constantes.INFO_OTROS_SALDOS)) {

                List<Billetera> listBilleterasMenu = menuAnterior.getListBilletera();
                if (listBilleterasMenu != null) {
                    String separadorTextoBilleteraSaldo = gestorConfiguraciones.obtenerParametroBD("default_wallet_texto_separador", logPrefijo) + " ";

                    for (Billetera billetera : listBilleterasMenu) {

                        SaldoUnidad su = obtenerSaldoDeBilletera(listaBilleterasCMV, billetera, listOffer, listAcumulador, logPrefijo, null);
                        if (su != null) {
                            String fechaExpiracion = "";
                            if (Propiedades.textoVenceOtrosSaldosMostrarFechaExpiracion.equals("1")) {
                                fechaExpiracion = Propiedades.textoVenceOtrosSaldosFechaExpiracion.replace("%FECHA%", UtilDate.dateToString(su.getFechaExpiracion().getTime(), Propiedades.textoVenceOtrosSaldosFormatoFecha, logPrefijo));
                            }

                            String nameComercial = UtilCcws2.getNombreComercial(actualConfig.getListConfiguracionNombres(), billetera.getBilleteraId());

                            if (resultado.isEmpty()) {
                                if (su.getSaldo() > 0) { // mostar si el saldo es mayor a 0  Otros saldos
                                    resultado = nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + fechaExpiracion;
                                }
                            } else {
                                if (su.getSaldo() > 0) {
                                    resultado = resultado + "\n" + nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + fechaExpiracion;
                                }
                            }
                        } else {
                            //modificafo 16/02/2022
                            //LOG.warn(logPrefijo + " Billetera no encontrada: " + billetera.getNombreComverse());

                            LOG.debug(logPrefijo + " Billetera no encontrada: " + billetera.getNombreComverse());
                        }
                    }
                }
            } else {
                String menu = "";
                int indice = 1;
                //List<String> opcionesValidas = new ArrayList<>();
                List<Menu> listMenus = UtilCcws2.getBilleterasNivel2(actualConfig.getListMenus());
                if (listMenus != null) {
                    String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
                    boolean mostrarMinutos = (auxmo.equals("true"));
                    int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

                    for (Menu me : listMenus) {
                        if (me.getNivel() == 2 && (me.getCanal().equals(canal) || me.getCanal().equals(Constantes.VIA_TODOS))) {

                            if (me.getInformacion().equals(Constantes.INFO_SALDO_PAQUETIGO) || me.getInformacion().equals(Constantes.INFO_PAQUETIGOS_ACUMULADOS)) {

                                List<Billetera> listBilleterasMenu = me.getListBilletera();
                                if (listBilleterasMenu != null && !listBilleterasMenu.isEmpty()) {

                                    SaldoUnidad su = new SaldoUnidad();
                                    boolean mostrar = false;
                                    switch (me.getInformacion()) {
                                        case Constantes.INFO_SALDO_PAQUETIGO:
                                            su = obtenerSaldosDeBilleteras(listaBilleterasCMV, listBilleterasMenu, listOffer, listAcumulador, logPrefijo);
                                            if (su != null) {
                                                mostrar = UtilCcws2.mostrarMenu(me, su);
                                            }
                                            break;
                                        case Constantes.INFO_PAQUETIGOS_ACUMULADOS:
                                            su = obtenerSaldosDeBilleterasExpiradas(listaBilleterasCMV, listBilleterasMenu, listOffer, listAcumulador, logPrefijo);
                                            if (su != null) {
                                                mostrar = UtilCcws2.mostrarMenuCalculado(me, su, listaBilleterasCMV, listBilleterasMenu);
                                                su.setSaldoUnidad("");
                                            }
                                            break;
                                    }

                                    if (mostrar) {
                                        if (me.getDescripcion() != null && me.getDescripcion().contains(Propiedades.TEXTO_DESCRIPCION)) {
                                            if (menu.isEmpty()) {
                                                menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                            } else {
                                                menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                            }
                                        } else {
                                            if (su != null) {
                                                if (menu.isEmpty()) {
                                                    if (me.getTipoUnidad().equals(idSegundo) && mostrarMinutos) {
                                                        String aux = UtilCcws2.getMinutesSeconds(su.getSaldo());
                                                        su.setSaldoUnidad(aux);

                                                        menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre() + " " + su.getSaldoUnidad();
                                                    } else {
                                                        menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre() + " " + su.getSaldoUnidad();
                                                    }
                                                } else {
                                                    if (me.getTipoUnidad().equals(idSegundo) && mostrarMinutos) {
                                                        String aux = UtilCcws2.getMinutesSeconds(su.getSaldo());
                                                        su.setSaldoUnidad(aux);
                                                        menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre() + " " + su.getSaldoUnidad();
                                                    } else {
                                                        menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre() + " " + su.getSaldoUnidad();
                                                    }
                                                }
                                            }
                                        }
                                        //opcionesValidas.add(indice + SEPARADOR_DE_ITEM + me.getNombre() + SEPARADOR_DE_ITEM + me.getId());
                                        listOpciones.add(indice);
                                        listaOMP.add(new OpcionMenu(indice, me.getId()));
                                        indice++;
                                    }
                                } else {
                                    SaldoUnidad sa = new SaldoUnidad();
                                    sa.setSaldo(0.0);
                                    boolean mostrar = UtilCcws2.mostrarMenu(me, sa);

                                    if (mostrar) {
                                        if (menu.isEmpty()) {
                                            menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                        } else {
                                            menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                        }
                                        listOpciones.add(indice);
                                        listaOMP.add(new OpcionMenu(indice, me.getId()));
                                        indice++;
                                    }
                                    //opcionesValidas.add(indice + SEPARADOR_DE_ITEM + me.getNombre() + SEPARADOR_DE_ITEM + me.getId());
                                }
                            } else {
                                if (menu.isEmpty()) {
                                    menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                } else {
                                    menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                                }
                                //opcionesValidas.add(indice + SEPARADOR_DE_ITEM + me.getNombre() + CSUGeneral.SEPARADOR_DE_ITEM + me.getId());
                                listOpciones.add(indice);
                                indice++;
                            }
                        }
                    }
                }
                LOG.debug(logPrefijo + " menu: " + menu);
                resultado = menu;
                //informacion.setOpcionesValidas(opcionesValidas);
            }
            pantalla.setIdPantalla(idPantalla);
            pantalla.setOpciones(resultado);
            pantalla.setListaNroOpcion(listOpciones);
            pantalla.setOpcionPadre(opcionAnt);
            pantalla.setCabecera("");
            pantalla.setPantallaPadre(idPantFather);
            pantalla.setListaNavegacion(new ArrayList<>());
            LOG.debug("...:::" + logPrefijo + "TERMINO CONSTRUIR PANTALLA NIVEL-2 ID: " + idPantalla + " - " + (System.currentTimeMillis() - z) + " ms :::...");
        }

        return pantalla;
    }

    private SaldoUnidad obtenerSaldosDeBilleteras(List<WalletComverse> listaBilleteraCMV, List<Billetera> listBilleteras, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        if (listBilleteras != null) {

            String tipoUnidad = "";
            double total = 0;
            boolean whatsappIlimitado = false;
            Integer cantidadDecimales = 0;
            for (Billetera billetera : listBilleteras) {

                WalletComverse billeteraComverse = null;
                if (billetera.isAlco().equals("t")) {
                    Alco:
                    for (Offer oferta : listOffer) {
                        if (billetera.getNombreComverse().equals(oferta.getName())) {
                            billeteraComverse = new WalletComverse();
                            billeteraComverse.setNameWallet(oferta.getName());
                            billeteraComverse.setExpiration(oferta.getServiceEnd());
                            billeteraComverse.setUsada(true);
                            billeteraComverse.setAvailableBalance(1);

                            for (Acumulador acum : listAcumulador) {
                                LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                                if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                    double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                    LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                    if (totalAcumulador < 0) {
                                        billeteraComverse.setAvailableBalance(1);
                                    } else {
                                        billeteraComverse.setAvailableBalance(totalAcumulador);
                                    }
                                    break Alco;
                                }
                            }
                            oferta.setUsada(true);
                        }
                    }
                } else {
                    billeteraComverse = UtilCcws2.getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
                }

                if (billeteraComverse == null) {
                    //modificado el 15/02/2022
                    //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                    LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                } else {
                    //Habilitar para quitar monto monimo
                    // if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis()) {
                    if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA2_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                        double valueAplicadoOperador = UtilCcws2.getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance(), logPrefijo);

                        total = total + valueAplicadoOperador;
                        tipoUnidad = billetera.getPrefijoUnidad();
                        cantidadDecimales = billetera.getCantidadDecimales();
                        if (billeteraComverse.getAvailableBalance() >= billetera.getMontoMaximo()) {
                            whatsappIlimitado = true;
                        }
                    } else {
                        LOG.debug(logPrefijo + " Billetera expirada: " + billetera.getNombreComercial() + ", nameComverse: " + billetera.getNombreComverse());
                    }
                }
            }
            String valueStrBalance = UtilCcws2.formatValueWithDecimalFormat(total, cantidadDecimales, logPrefijo);
            if (!tipoUnidad.isEmpty()) {
                SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                su.setCantidadDecimales(cantidadDecimales);
                su.setSaldo(total);
                su.setWhatsppIlimitado(whatsappIlimitado);
                su.setSaldoTexto(valueStrBalance);
                su.setUnidad(tipoUnidad);
                su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);

                return su;
            }
        }
        return null;
    }

    private SaldoUnidad obtenerSaldoDeBilletera(List<WalletComverse> listaBilleteraCMV, Billetera billetera, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo, List<Billetera> billeteraReservada) {
        if (billetera != null) {
            String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
            boolean mostrarMinutos = (auxmo.equals("true"));
            int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

            WalletComverse billeteraComverse = null;
            if (billetera.isAlco().equals("t")) {
                Alco:
                for (Offer oferta : listOffer) {
                    if (billetera.getNombreComverse().equals(oferta.getName())) {
                        billeteraComverse = new WalletComverse();
                        billeteraComverse.setNameWallet(oferta.getName());
                        billeteraComverse.setExpiration(oferta.getServiceEnd());
                        billeteraComverse.setUsada(true);
                        //billeteraComverse.setAvailableBalance(0);
                        billeteraComverse.setAvailableBalance(1);

                        for (Acumulador acum : listAcumulador) {
                            LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                            if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                if (totalAcumulador < 0) {
                                    billeteraComverse.setAvailableBalance(1);
                                } else {
                                    billeteraComverse.setAvailableBalance(totalAcumulador);
                                }
                                break Alco;
                            }
                        }
                        oferta.setUsada(true);
                    }
                }
            } else {
                billeteraComverse = UtilCcws2.getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
            }

            if (billeteraComverse == null) {
                //modificado 16/02/2022
                //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
            } else {

                //Habilitar para quitar monto monimo
                // if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis()) {
                if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA2_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {

                    double valor_reserva = UtilCcws2.obtenerValorReserva(billetera, billeteraReservada, listaBilleteraCMV);
                    double valueAplicadoOperador = UtilCcws2.getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance() + valor_reserva, logPrefijo);
                    String tipoUnidad = billetera.getPrefijoUnidad();
                    String valueStrBalance = UtilCcws2.formatValueWithDecimalFormat(valueAplicadoOperador, billetera.getCantidadDecimales(), logPrefijo);

                    SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                    su.setCantidadDecimales(billetera.getCantidadDecimales());
                    su.setSaldo(valueAplicadoOperador);
                    su.setSaldoTexto(valueStrBalance);
                    su.setUnidad(tipoUnidad);
                    su.setFechaExpiracion(billeteraComverse.getExpiration());

                    if (billetera.isAlco().equals("f")) {
                        if (billetera.getUnitTypeId().equals(idSegundo) && mostrarMinutos) {
                            String aux = UtilCcws2.getMinutesSeconds(billeteraComverse.getAvailableBalance());
                            su.setSaldoUnidad(aux);
                        } else {
                            su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);
                        }
                        if (billetera.getMontoMaximo() <= valueAplicadoOperador) { // SI ES ILIMITADO
                            su.setSaldoUnidad(billetera.getTextoIlimitado());
                        }
                    } else {
                        su.setSaldoUnidad("");
                    }

                    return su;
                } else {
                    LOG.debug(logPrefijo + " Billetera expirada: " + billetera.getNombreComercial() + ", nameComverse: " + billetera.getNombreComverse());
                }
            }
        }
        return null;
    }

    private SaldoUnidad obtenerSaldosDeBilleterasExpiradas(List<WalletComverse> listaBilleteraCMV, List<Billetera> listBilleteras, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        if (listBilleteras != null) {

            String tipoUnidad = "";
            double total = 0;
            boolean whatsappIlimitado = false;
            Integer cantidadDecimales = 0;
            for (Billetera billetera : listBilleteras) {

                WalletComverse billeteraComverse = null;
                if (billetera.isAlco().equals("t")) {
                    Alco:
                    for (Offer oferta : listOffer) {
                        if (billetera.getNombreComverse().equals(oferta.getName())) {
                            billeteraComverse = new WalletComverse();
                            billeteraComverse.setNameWallet(oferta.getName());
                            billeteraComverse.setExpiration(oferta.getServiceEnd());
                            billeteraComverse.setUsada(true);
                            billeteraComverse.setAvailableBalance(1);

                            for (Acumulador acum : listAcumulador) {
                                LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                                if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                    double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                    LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                    if (totalAcumulador < 0) {
                                        //billeteraComverse.setAvailableBalance(0);
                                        billeteraComverse.setAvailableBalance(1);
                                    } else {
                                        billeteraComverse.setAvailableBalance(totalAcumulador);
                                    }
                                    break Alco;
                                }
                            }
                            oferta.setUsada(true);
                        }
                    }
                } else {
                    billeteraComverse = UtilCcws2.getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
                }

                if (billeteraComverse == null) {
                    //modificado el 15/02/2022
                    //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                    LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                } else {
                    //if (billeteraComverse.getExpiration().getTimeInMillis() < System.currentTimeMillis()) {
                    if (billeteraComverse.getExpiration().getTimeInMillis() < System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA2_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {

                        double valueAplicadoOperador = UtilCcws2.getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance(), logPrefijo);

                        total = total + valueAplicadoOperador;
                        tipoUnidad = billetera.getPrefijoUnidad();
                        cantidadDecimales = billetera.getCantidadDecimales();
                        if (billeteraComverse.getAvailableBalance() >= billetera.getMontoMaximo()) {
                            whatsappIlimitado = true;
                        }
                    } else {
                        LOG.debug(logPrefijo + " Billetera expirada: " + billetera.getNombreComercial() + ", nameComverse: " + billetera.getNombreComverse());
                    }
                }
            }
            String valueStrBalance = UtilCcws2.formatValueWithDecimalFormat(total, cantidadDecimales, logPrefijo);
            if (!tipoUnidad.isEmpty()) {
                SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                su.setCantidadDecimales(cantidadDecimales);
                su.setSaldo(total);
                su.setWhatsppIlimitado(whatsappIlimitado);
                su.setSaldoTexto(valueStrBalance);
                su.setUnidad("");
                su.setSaldoUnidad(valueStrBalance);

                return su;
            }
        }
        return null;
    }
}
