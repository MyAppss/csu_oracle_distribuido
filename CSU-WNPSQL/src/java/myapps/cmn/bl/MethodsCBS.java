/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultLifeCycleStatus;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader;
import com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.xml.rpc.ServiceException;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilDate;
import myapps.pool.cbs.ConexionCbs;
import myapps.pool.cbs.PoolCbs;
import org.apache.axis.AxisFault;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class MethodsCBS implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger(MethodsCBS.class);
    private List<String> codigosAceptados = null;
    private List<String> billeterasAfectadas = null;

    public MethodsCBS() {
        codigosAceptados = cadenaToList(Propiedades.SOAP_CBS_CODIGOS_ACEPTADOS);
        billeterasAfectadas = cadenaToList(Propiedades.VM_CBS_BILLETERA_MODIFICAR);
    }

    private QuerySubLifeCycleResultMsg obtenerPeticion(QuerySubLifeCycleRequestMsg request, String prefijo) throws AxisFault, RemoteException, Exception {
        QuerySubLifeCycleResultMsg response = null;
        ConexionCbs conexion = null;
        BcServicsBindingStub port = null;
        long tInicioConexion = 0;
        long tFinalConexion = 0;
        long tInicioRespuesta = 0;
        long tFinalRespuesta = 0;
        try {
            long contador = 0;
            long sleep = 500;
            tInicioConexion = System.currentTimeMillis();
            conexion = PoolCbs.poll();
            while (conexion == null && ((contador * sleep) < Propiedades.SOAP_CBS_TIMEOUTCONEXION)) {
                logger.debug(prefijo + "ConexionObtenidadDelPool resuelto nulo, contador: " + contador);
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    logger.error(prefijo + "Error en el sleep: ", e);
                    Thread.currentThread().interrupt();
                }
                conexion = PoolCbs.poll();
                contador++;
            }
            tFinalConexion = System.currentTimeMillis();
            if (conexion != null) {

                tInicioRespuesta = System.currentTimeMillis();
                port = conexion.getPort();
                port.setTimeout(Propiedades.SOAP_CBS_CONNECTIONTIMEOUT);
                response = port.querySubLifeCycle(request);
                tFinalRespuesta = System.currentTimeMillis();
            } else {
                throw new Exception("No se pudo obtener la conexion hacia CBS");
            }

        } catch (RemoteException r) {
            logger.error(prefijo + " RemoteException: " + r.getMessage(), r);
            throw r;
        } catch (Exception e) {
            logger.error(prefijo + " Exception: " + e.getMessage(), e);
            throw e;
        } finally {

            if (port != null) {
                PoolCbs.put(conexion);
                logger.debug(prefijo + "Conexion devuelta a la cola: " + conexion.getId());
                if (port._getCall() != null) {
                    String requestString = port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString();
                    logger.info(prefijo + " Request: " + requestString);
                    String responseString = port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString();
                    logger.info(prefijo + " Response: " + responseString);
                }
            }
            logger.info(prefijo + "Tiempo de Conexion hacia cbs: " + (tFinalConexion - tInicioConexion) + " ms.");
            logger.info(prefijo + "Tiempo de Consulta hacia cbs: " + (tFinalRespuesta - tInicioRespuesta) + " ms.");
        }

        return response;
    }

    private QuerySubLifeCycleRequestMsg armarRequest(String prefijo, String linea, String sessionId) {
        QuerySubLifeCycleRequestMsg request = new QuerySubLifeCycleRequestMsg();

        //1
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setVersion(Propiedades.SOAP_CBS_VERSION);
        requestHeader.setMessageSeq(sessionId);

        //1.1
        OwnershipInfo ownershipInfo = new OwnershipInfo();
        ownershipInfo.setBEID(Propiedades.SOAP_CBS_BEID);

        //1.2
        SecurityInfo securityInfo = new SecurityInfo();
        securityInfo.setLoginSystemCode(Propiedades.SOAP_CBS_USERNAME);
        securityInfo.setPassword(Propiedades.SOAP_CBS_PASSWORD);

        requestHeader.setOwnershipInfo(ownershipInfo);
        requestHeader.setAccessSecurity(securityInfo);
        request.setRequestHeader(requestHeader);

        //2
        QuerySubLifeCycleRequest querySubLifeCycleRequest = new QuerySubLifeCycleRequest();

        SubAccessCode subAccessCode = new SubAccessCode();
        subAccessCode.setPrimaryIdentity(Propiedades.SOAP_CBS_PREFIJO_MSISDN + linea);
        querySubLifeCycleRequest.setSubAccessCode(subAccessCode);
        request.setQuerySubLifeCycleRequest(querySubLifeCycleRequest);
        return request;
    }

    public Calendar obtenerFechaCbs(String prefijo, String linea, String sessionId) throws Exception {
        Calendar fechaFinal = null;
        String fecha = null;
        try {
            QuerySubLifeCycleRequestMsg request = armarRequest(prefijo, linea, sessionId);
            QuerySubLifeCycleResultMsg response = obtenerPeticion(request, prefijo);
            if (response != null && response.getResultHeader() != null) {
                String resultCode = response.getResultHeader().getResultCode();
                if (resultCode != null && codigosAceptados.contains(resultCode)) {
                    logger.debug(prefijo + " El ResultCode: " + resultCode + " es valido");
                    if (response.getQuerySubLifeCycleResult().getLifeCycleStatus() != null) {
                        for (QuerySubLifeCycleResultLifeCycleStatus cycle : response.getQuerySubLifeCycleResult().getLifeCycleStatus()) {
                            if (cycle != null && cycle.getStatusName().equals(Propiedades.VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO)) {
                                fecha = cycle.getStatusExpireTime();
                                break;
                            }
                        }
                    }
                } else {
                    logger.debug(prefijo + "El ResultCode: " + resultCode + " no es valido");
                }
            }

            if (fecha != null) {
                fechaFinal = UtilDate.stringToCalendar(fecha, Propiedades.VM_CBS_EXPIRE_TIME_FORMAT, prefijo);
                if (fechaFinal != null) {
                    logger.debug(prefijo + "La fecha sin conversion es: " + fecha);
                    logger.debug(prefijo + " La fecha convertida es: " + fechaFinal.toString());

                    fecha = UtilDate.dateToTimeString(fechaFinal, Propiedades.VM_CBS_EXPIRE_TIME_FORMAT_STRING, prefijo);
                    logger.debug(prefijo + " La fecha final a devolver es: " + fecha);
                }
            }

        } catch (RemoteException | ServiceException e) {
            logger.error(prefijo + " Exception: " + e.getMessage(), e);
            throw e;
        }

        return fechaFinal;
    }

    public static List<String> cadenaToList(String cadena) {
        List<String> lista = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split(";");
                lista = Arrays.asList(arrayOne);
            }
        } catch (Exception e) {
            logger.error("Error al cargar la lista de Resultados Aceptados: " + cadena, e);
        }
        return lista;
    }

    public List<String> getBilleterasAfectadas() {
        return billeterasAfectadas;
    }

}
