/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import myapps.cmn.util.Constantes;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilCcws3;
import myapps.cmn.util.UtilDate;
import myapps.cmn.util.UtilMontoMinimo;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.Pantalla;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.vo.WalletComverse;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ConsultarPantallaNro3 {

    public static final Logger LOG = Logger.getLogger(ConsultarPantallaNro3.class);
    private final GestorConfiguraciones gestorConfiguraciones;

    public ConsultarPantallaNro3() {
        this.gestorConfiguraciones = GestorConfiguraciones.getInstance();
    }

    public Pantalla consultarPantallaNro3(String msisdn, String canal,
            String corto, Integer opcionAnt, Integer menuAnteriorId, Integer idPantFather, Integer idPantalla,
            String logPrefijo, ResponseCCWS responseComverse, Config actualConfig, String sessionId) {

        long z = System.currentTimeMillis();
        Pantalla pantalla = new Pantalla();

        if (responseComverse != null && actualConfig != null) {
            logPrefijo = logPrefijo + "[cos: " + responseComverse.getNameCOS() + "]";
            List<WalletComverse> listaBilleterasCMV = responseComverse.getListWallet();
            List<Offer> listaOffer = responseComverse.getListOffer();
            List<Acumulador> listaAcumulador = responseComverse.getListAcumulador();

            String resultado = "";
            //List<String> opcionesValidas = new ArrayList<>();
            Menu menuAnterior = UtilCcws3.obtenerMenu(actualConfig, menuAnteriorId);
            if (menuAnterior != null && menuAnterior.getInformacion().equals(Constantes.INFO_SALDO_PAQUETIGO)) {
                List<Billetera> listBilleteras = menuAnterior.getListBilletera();

                if (listBilleteras != null) {
                    List<Billetera> listBilleterasAux = listBilleteras;
                    List<Billetera> listBilleterasReservadas = UtilCcws3.obtenerListaBilleterasReservadas(listBilleterasAux);
                    String separadorTextoBilleteraSaldo = gestorConfiguraciones.obtenerParametroBD("default_wallet_texto_separador", logPrefijo) + " ";

//                    for (Billetera billetera : listBilleterasReservadas) {
                    for (Billetera billetera : listBilleteras) {
                        //for (int i = 0; i < listBilleteras.size(); i++) {
                        //Billetera billetera = listBilleteras.get(i);
                        if (billetera.isReserva() == null) {
                            billetera.setReserva("f");
                        }
                        if (billetera.isReserva().equals("f")) {
                            SaldoUnidad su = obtenerSaldoDeBilletera(listaBilleterasCMV, billetera, listaOffer, listaAcumulador, logPrefijo, listBilleterasReservadas);
                            if (su != null) {
                                int seg_vigencia_dias_exp = Integer.parseInt(gestorConfiguraciones.obtenerParametroBD("seg_vigencia_dias_exp", logPrefijo));

                                String nameComercial = UtilCcws3.getNombreComercial(actualConfig.getListConfiguracionNombres(), billetera.getBilleteraId());
                                boolean mosFechaDos = UtilCcws3.getSegundaFechaExp(actualConfig.getListConfiguracionNombres(), billetera.getBilleteraId());

                                Calendar auxCalendar = Calendar.getInstance();
                                auxCalendar.setTime(su.getFechaExpiracion().getTime());
                                auxCalendar.add(Calendar.DATE, seg_vigencia_dias_exp);

                                String fechaExpiracion = UtilDate.dateToString(su.getFechaExpiracion().getTime(), Propiedades.TEXTO_SALDO_PAQUETIGOS_FORMATO_FECHA, logPrefijo);
                                String fechaExpiracionDos = Propiedades.textoVenceOtrosSaldosFechaExpiracion.replace("%FECHA%", UtilDate.dateToString(auxCalendar.getTime(), Propiedades.textoPaquetigosAcumuladosFormatoFecha, logPrefijo));

                                if (resultado.isEmpty()) {
                                    if (mosFechaDos) {
                                        if (menuAnterior.getInformacion().equals(Constantes.INFO_SALDO_PAQUETIGO)
                                                && Propiedades.textoVenceVigenteMostrarFechaExpiracion.equals("0")) {
                                            resultado = nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + Propiedades.TEXTO_SALDO_PAQUETIGOS_VIGENTE + " " + fechaExpiracion;
                                        } else {
                                            resultado = nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + Propiedades.TEXTO_SALDO_PAQUETIGOS_VIGENTE + " " + fechaExpiracion + " " + fechaExpiracionDos;
                                        }
                                    } else {
                                        resultado = nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + Propiedades.TEXTO_SALDO_PAQUETIGOS_VIGENTE + " " + fechaExpiracion;
                                    }
                                } else {
                                    String aux;
                                    if (mosFechaDos) {
                                        aux = resultado + "\n" + nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + Propiedades.TEXTO_SALDO_PAQUETIGOS_VIGENTE + " " + fechaExpiracion + " " + fechaExpiracionDos;
                                        resultado = aux;
                                    } else {
                                        aux = resultado + "\n" + nameComercial + separadorTextoBilleteraSaldo + su.getSaldoUnidad() + " " + Propiedades.TEXTO_SALDO_PAQUETIGOS_VIGENTE + " " + fechaExpiracion;
                                        resultado = aux;
                                    }
                                }

                            } else {
                                //modificado 16/02/2022
                                //LOG.warn(logPrefijo + " Billetera no encontrada: " + billetera.getNombreComverse());
                                LOG.debug(logPrefijo + " Billetera no encontrada: " + billetera.getNombreComverse());

                            }
                        }
                    }
                }
            } else if (menuAnterior != null && menuAnterior.getInformacion().equals(Constantes.INFO_PAQUETIGOS_ACUMULADOS)) {
                List<Billetera> listBilleteras = menuAnterior.getListBilletera();
                if (listBilleteras != null) {
                    for (Billetera billetera : listBilleteras) {
                        SaldoUnidad su = UtilCcws3.obtenerSaldoDeBilleteraSinVerificarExpiracion(listaBilleterasCMV, billetera, listaOffer, listaAcumulador, logPrefijo);
                        if (su != null) {

                            boolean mostrar = UtilCcws3.mostrarMenuCalculado(menuAnterior, su);
                            if (mostrar) {
                                if (su.getFechaExpiracion().getTimeInMillis() < System.currentTimeMillis()) {
                                    int seg_vigencia_dias_exp = Integer.parseInt(gestorConfiguraciones.obtenerParametroBD("seg_vigencia_dias_exp", logPrefijo));

                                    Calendar auxCalendar = Calendar.getInstance();
                                    auxCalendar.setTime(su.getFechaExpiracion().getTime());
                                    auxCalendar.add(Calendar.DATE, seg_vigencia_dias_exp);
                                    su.setFechaExpiracion(auxCalendar);

                                    String fechaExpiracion = "";
                                    if (Propiedades.TEXTO_PAQUETIGOS_ACUMULADOS_MOSTRAR_FECHA_EXPIRACION.equals("1")) {
                                        fechaExpiracion = Propiedades.TEXTO_PAQUETIGOS_ACUMULADOS_FECHA_EXPIRACION.replace("%FECHA%", UtilDate.dateToString(su.getFechaExpiracion().getTime(), Propiedades.textoPaquetigosAcumuladosFormatoFecha, logPrefijo));
                                    }

                                    String nameAcumulado = UtilCcws3.getNombreAcumulado(actualConfig.getListConfiguracionNombres(), billetera.getBilleteraId());
                                    String aux;
                                    if (resultado.isEmpty()) {
                                        aux = nameAcumulado + " " + Propiedades.TEXTO_PAQUETIGO_ACUMULADOS_NO_VIGENTE + " " + su.getSaldoUnidad() + " " + fechaExpiracion;
                                        resultado = aux;
                                    } else {
                                        aux = resultado + "\n" + nameAcumulado + " " + Propiedades.TEXTO_PAQUETIGO_ACUMULADOS_NO_VIGENTE + " " + su.getSaldoUnidad() + " " + fechaExpiracion;
                                        resultado = aux;
                                    }
                                }
                            }

                        }
                    }
                }
            } else {
                if (menuAnterior != null) {
                    LOG.warn(logPrefijo + " Informacion no valida: " + menuAnterior.getInformacion());
                }
            }

            pantalla.setIdPantalla(idPantalla);
            pantalla.setCabecera("");
            pantalla.setOpciones(resultado);
            pantalla.setPantallaPadre(idPantFather);
            pantalla.setOpcionPadre(opcionAnt);
            pantalla.setListaNroOpcion(new ArrayList<>());
            pantalla.setListaNavegacion(new ArrayList<>());
            LOG.debug("...:::" + logPrefijo + "TERMINO CONSTRUIR PANTALLA NIVEL-3 ID: " + idPantalla + " - " + (System.currentTimeMillis() - z) + " ms :::...");
        }

        return pantalla;
    }

    private SaldoUnidad obtenerSaldoDeBilletera(List<WalletComverse> listaBilleteraCMV, Billetera billetera, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo, List<Billetera> billeteraReservada) {
        if (billetera != null) {
            String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
            boolean mostrarMinutos = (auxmo.equals("true"));
            int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

            WalletComverse billeteraComverse = null;
            if (billetera.isAlco().equals("t")) {
                Alco:
                for (Offer oferta : listOffer) {
                    if (billetera.getNombreComverse().equals(oferta.getName())) {
                        billeteraComverse = new WalletComverse();
                        billeteraComverse.setNameWallet(oferta.getName());
                        billeteraComverse.setExpiration(oferta.getServiceEnd());
                        billeteraComverse.setUsada(true);
                        //billeteraComverse.setAvailableBalance(0);
                        billeteraComverse.setAvailableBalance(1);
                        
                        for (Acumulador acum : listAcumulador) {
                            LOG.debug(logPrefijo + " " + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                            if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                double totalAcumulador = billetera.getLimiteAcumulador() - acum.getAmount();
                                LOG.debug(logPrefijo + " total: " + totalAcumulador);
                                if (totalAcumulador < 0) {
                                    //billeteraComverse.setAvailableBalance(0);
                                    billeteraComverse.setAvailableBalance(1);
                                } else {
                                    billeteraComverse.setAvailableBalance(totalAcumulador);
                                }
                                break Alco;
                            }
                        }
                        oferta.setUsada(true);
                    }
                }
            } else {
                billeteraComverse = UtilCcws3.getWalletCmvAndMarcar(billetera.getNombreComverse(), listaBilleteraCMV);
            }

            if (billeteraComverse == null) {
                //  modificado 16/02/2022
                //LOG.warn(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
                LOG.debug(logPrefijo + " BILLETERA NO ENCONTRADA EN CCWS: " + billetera.getNombreComverse());
            } else {
                //Habilitar para quitar monto minimo
                // if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis()) {
                if (billeteraComverse.getExpiration().getTimeInMillis() >= System.currentTimeMillis() && UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA3_MONTO_MINIMO, billeteraComverse, billetera, logPrefijo)) {
                    //  listBilleterasReservadas = ListbilleteraAux(listBilleteras);
                    double valor_reserva = UtilCcws3.obtenerValorReserva(billetera, billeteraReservada, listaBilleteraCMV);

                    double valueAplicadoOperador = UtilCcws3.getValueAplicandoOperator(billetera, billeteraComverse.getAvailableBalance() + valor_reserva, logPrefijo);
                    String tipoUnidad = billetera.getPrefijoUnidad();
                    String valueStrBalance = UtilCcws3.formatValueWithDecimalFormat(valueAplicadoOperador, billetera.getCantidadDecimales(), logPrefijo);

                    SaldoUnidad su = new SaldoUnidad(0, "", "", "", 0, false);
                    su.setCantidadDecimales(billetera.getCantidadDecimales());
                    su.setSaldo(valueAplicadoOperador);
                    su.setSaldoTexto(valueStrBalance);
                    su.setUnidad(tipoUnidad);
                    su.setFechaExpiracion(billeteraComverse.getExpiration());

                    if (billetera.isAlco().equals("f")) {
                        if (billetera.getUnitTypeId().equals(idSegundo) && mostrarMinutos) {
                            String aux = UtilCcws3.getMinutesSeconds(billeteraComverse.getAvailableBalance());
                            su.setSaldoUnidad(aux);
                        } else {
                            su.setSaldoUnidad(valueStrBalance + " " + tipoUnidad);
                        }
                        if (billetera.getMontoMaximo() <= valueAplicadoOperador) { // SI ES ILIMITADO
                            su.setSaldoUnidad(billetera.getTextoIlimitado());
                        }
                    } else {
                        su.setSaldoUnidad("");
                    }

                    return su;
                } else {
                    LOG.debug(logPrefijo + " Billetera expirada: " + billetera.getNombreComercial() + ", nameComverse: " + billetera.getNombreComverse());
                }
            }
        }

        return null;
    }

}
