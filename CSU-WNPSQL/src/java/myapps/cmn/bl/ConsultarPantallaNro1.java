/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import myapps.cmn.vo.SaldoUnidad;
import myapps.cmn.util.Constantes;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilCcws1;
import myapps.cmn.util.UtilDate;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Cabecera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.OpcionMenu;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.Pantalla;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.WalletComverse;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ConsultarPantallaNro1 {

    private static final Logger LOG = Logger.getLogger(ConsultarPantallaNro1.class);
    private final GestorConfiguraciones gestorConfiguraciones;

    private List<OpcionMenu> listaOMP;

    public ConsultarPantallaNro1() {
        this.gestorConfiguraciones = GestorConfiguraciones.getInstance();
    }

    public List<OpcionMenu> getListaOMP() {
        return listaOMP;
    }

    public void setListaOMP(List<OpcionMenu> listaOMP) {
        this.listaOMP = listaOMP;
    }

    public Pantalla consultarPantallaNro1(String msisdn, String canal,
            String corto, Integer opcion, Integer idPantalla, String logPrefijo, ResponseCCWS responseComverse, Config actualConfig, String sessionId) {
        long z = System.currentTimeMillis();
        Pantalla pantalla = new Pantalla();
        this.listaOMP = new ArrayList<>();
        if (responseComverse != null && actualConfig != null) {
            List<WalletComverse> listaBilleterasCMV = responseComverse.getListWallet();
            List<Offer> listOffer = responseComverse.getListOffer();
            List<Acumulador> listAcumulador = responseComverse.getListAcumulador();

            double reserva = UtilCcws1.obtenerReservas(actualConfig, listaBilleterasCMV, logPrefijo);

            logPrefijo = logPrefijo + "[configId: " + actualConfig.getConfigId() + "]";
            LOG.debug(logPrefijo + " CAMPO_RESERVA: " + reserva);
            String saludo = actualConfig.getSaludoInicial();
            saludo = saludo.replace("%TELEFONO%", msisdn) + "\n";

            String cabecera;
            if (reserva == -1) {
                LOG.debug(logPrefijo + " ...::: NO TIENE RESERVAS :::..");
                cabecera = getOnTodos(actualConfig, listaBilleterasCMV, listOffer, listAcumulador, logPrefijo);
            } else {
                if (reserva <= 0.0) { // TIENE MB COMPRADOS
                    LOG.debug(logPrefijo + " ...::: TIENE MB COMPRADOS :::...");
                    cabecera = getConMegas(actualConfig, listaBilleterasCMV, listOffer, listAcumulador, logPrefijo);
                } else { // ON DEMAND
                    LOG.debug(logPrefijo + " ...::: ON DEMAND :::...");
                    cabecera = getOnDeman(actualConfig, listaBilleterasCMV, listOffer, listAcumulador, logPrefijo);
                }
            }
            LOG.debug(logPrefijo + " CABECERA: " + cabecera);

            String menu = "";
            int indice = 1;
            //List<String> opcionesValidas = new ArrayList<>();
            List<Integer> opciones = new ArrayList<>();
            List<Menu> listMenus = UtilCcws1.obtenerBilleterasNivel1(actualConfig.getListMenus());
            LOG.debug(logPrefijo + "Obtener list menus en " + (System.currentTimeMillis() - z) + " ms");
            if (listMenus != null) {
                for (Menu me : listMenus) {
                    //for (int i = 0; i < listMenus.size(); i++) {
                    //Menu me = listMenus.get(i);

                    if (me.getNivel() == 1 && (me.getCanal().equals(canal) || me.getCanal().equals(Constantes.VIA_TODOS))) {
                        boolean print = false;

                        SaldoUnidad su;
                        if (me.getTieneHijos().equals("t")) {
                            su = obtenerSaldoHijos(actualConfig.getListMenus(), me.getId(), listaBilleterasCMV, listOffer, listAcumulador, logPrefijo);
                            if (su != null) {
                                boolean mostra;
                                if (!me.getInformacion().equals(Constantes.INFO_PAQUETIGOS_ACUMULADOS)) {
                                    mostra = UtilCcws1.mostrarMenu(me, su);
                                } else {
                                    mostra = UtilCcws1.mostrarMenuCalculado(me, su);
                                }
                                if (mostra) {
                                    print = true;
                                }
                            }
                        } else {
                            su = UtilCcws1.obtenerSaldosDeBilleteras(listaBilleterasCMV, me.getListBilletera(), listOffer, listAcumulador, logPrefijo);
                            if (su != null) {
                                boolean mostrar = UtilCcws1.mostrarMenu(me, su);
                                if (mostrar) {
                                    print = true;
                                }
                            }
                        }

                        if (reserva <= 0 && me.getInformacion().equals(Constantes.INFO_INFO_DE_RESERVA)) {
                            print = false;
                        } else if (reserva > 0 && me.getInformacion().equals(Constantes.INFO_INFO_DE_RESERVA)) {
                            print = true;
                        }

                        if (print) {
                            if (menu.isEmpty()) {
                                menu = indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                            } else {
                                menu = menu + "\n" + indice + Constantes.SEPARADOR_DE_INDICE_MENU + me.getNombre();
                            }
                            opciones.add(indice);
                            listaOMP.add(new OpcionMenu(indice, me.getId()));
                            //opcionesValidas.add(indice + SEPARADOR_DE_ITEM + me.getNombre() + SEPARADOR_DE_ITEM + me.getId());
                            indice++;
                        }
                    }
                }
                LOG.debug(logPrefijo + " MENU: " + menu);
            }

            if (actualConfig.getLineas() != null && !actualConfig.getLineas().equals("null") && !actualConfig.getLineas().isEmpty()) {
                LOG.debug(logPrefijo + " actualConfig.getLineas(): " + actualConfig.getLineas());
                StringTokenizer st = new StringTokenizer(actualConfig.getLineas(), ";");
                String lineas = "";
                while (st.hasMoreElements()) {
                    String token = st.nextToken();
                    if (!token.trim().isEmpty()) {
                        if (lineas.isEmpty()) {
                            lineas = token;
                        } else {
                            lineas = lineas + "\n" + token;
                        }
                    }
                }
                if (cabecera.isEmpty()) {
                    cabecera = lineas;
                } else {
                    cabecera = cabecera + "\n" + lineas;
                }
            }

            pantalla.setCabecera(saludo + cabecera + "\n" + menu);
            pantalla.setOpciones(menu);
            pantalla.setIdPantalla(idPantalla);
            pantalla.setPantallaPadre(0);
            pantalla.setOpcionPadre(0);
            pantalla.setListaNroOpcion(opciones);
            pantalla.setListaNavegacion(new ArrayList<>());
            LOG.debug("...:::" + logPrefijo + "TERMINO CONSTRUIR PANTALLA NIVEL-1 ID: " + idPantalla + " - " + (System.currentTimeMillis() - z) + " ms :::...");
        }
        return pantalla;
    }

    private String getOnTodos(Config actualConfig, List<WalletComverse> listaBilleterasCMV,
            List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        String cabecera = "";

        List<Cabecera> listCabecera = actualConfig.getListCabeceras();

        if (listCabecera != null) {
            for (Cabecera cab : listCabecera) {

                if (cab.getTipoNavegacion().equals(Constantes.TN_CON_MB) || cab.getTipoNavegacion().equals(Constantes.TN_TODOS)) {
                    List<Billetera> listBilleterasCabecera = cab.getListBilleteras();

                    if (listBilleterasCabecera != null && !listBilleterasCabecera.isEmpty()) {

                        SaldoUnidad su = UtilCcws1.obtenerSaldosDeBilleteras(listaBilleterasCMV, listBilleterasCabecera, listOffer, listAcumulador, logPrefijo);

                        if (su != null) { // no mostrar si la billetera de la cabecera ha expirado
                            boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);
                            if (su.getSaldo() != -1) {
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    String sino = "NO";
                                    if (su.isWhatsppIlimitado()) {
                                        sino = "SI";
                                    }
                                    if (cabecera.isEmpty()) {
                                        if (mostrar) {
                                            cabecera = cab.getNombre() + " " + sino;
                                        }
                                    } else {
                                        if (mostrar) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + sino;
                                        }
                                    }
                                } else {
                                    String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
                                    boolean mostrarMinutos = (auxmo.equals("true"));
                                    int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

                                    if (cabecera.isEmpty()) {
                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }
                                            /*VIGENCIA MAXIMA
                                            cabecera = cab.getNombre() + " " + su.getSaldoUnidad();*/
                                            cabecera = cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONTODOS, logPrefijo);

                                        }
                                    } else {
                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }
                                            /*VIGENCIA MAXIMA
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad();*/
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONTODOS, logPrefijo);
                                        }
                                    }
                                }
                            } else { // si no encuentra en la lista de offer la billetera vigente whatsapp ilimitado
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    SaldoUnidad suuu = new SaldoUnidad();
                                    suuu.setSaldo(-1.0);
                                    boolean mostrarr = UtilCcws1.mostrarCabecera(cab, suuu);
                                    if (cabecera.isEmpty()) {
                                        if (mostrarr) {
                                            cabecera = cab.getNombre() + " NO";
                                        }
                                    } else {
                                        if (mostrarr) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                        }
                                    }
                                }
                            }
                        } else { // si no encuentra en las lista de offer la billetera whatsapp ilimitado
                            if (cab.getWhatsappIlimitado().equals("t")) {
                                SaldoUnidad suu = new SaldoUnidad();
                                suu.setSaldo(-1.0);
                                boolean mostrar = UtilCcws1.mostrarCabecera(cab, suu);
                                if (cabecera.isEmpty()) {
                                    if (mostrar) {
                                        cabecera = cab.getNombre() + " NO";
                                    }
                                } else {
                                    if (mostrar) {
                                        cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                    }
                                }
                            }
                        }
                    } else {
                        SaldoUnidad su = new SaldoUnidad();
                        su.setSaldo(-1.0);
                        boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);
                        if (cabecera.isEmpty()) {
                            if (mostrar) {
                                cabecera = cab.getNombre();
                            }
                        } else {
                            if (mostrar) {
                                cabecera = cabecera + "\n" + cab.getNombre();
                            }
                        }
                    }
                }
            }

        }

        return cabecera;
    }

    private String getConMegas(Config actualConfig, List<WalletComverse> listaBilleterasCMV,
            List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        String cabecera = "";

        List<Cabecera> listCabecera = actualConfig.getListCabeceras();

        if (listCabecera != null) {
            for (Cabecera cab : listCabecera) {

                if (cab.getTipoNavegacion().equals(Constantes.TN_CON_MB) || cab.getTipoNavegacion().equals(Constantes.TN_TODOS)) {
                    List<Billetera> listBilleterasCabecera = cab.getListBilleteras();

                    if (listBilleterasCabecera != null && !listBilleterasCabecera.isEmpty()) {

                        SaldoUnidad su = UtilCcws1.obtenerSaldosDeBilleteras(listaBilleterasCMV, listBilleterasCabecera, listOffer, listAcumulador, logPrefijo);

                        if (su != null) { // no mostrar si la billetera de la cabecera ha expirado
                            boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);

                            if (su.getSaldo() != -1) {
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    String sino = "NO";
                                    if (su.isWhatsppIlimitado()) {
                                        sino = "SI";
                                    }
                                    if (cabecera.isEmpty()) {
                                        if (mostrar) {
                                            cabecera = cab.getNombre() + " " + sino;
                                        }
                                    } else {
                                        if (mostrar) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + sino;
                                        }
                                    }
                                } else {

                                    String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
                                    boolean mostrarMinutos = (auxmo.equals("true"));
                                    int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

                                    if (cabecera.isEmpty()) {
                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }
                                            /*VIGENCI MAXIMA
                                            cabecera = cab.getNombre() + " " + su.getSaldoUnidad();*/
                                            cabecera = cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_CONMEGAS, logPrefijo);

                                        }
                                    } else {
                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }
                                            /*VIGENCI MAXIMA
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad();*/
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_CONMEGAS, logPrefijo);
                                        }
                                    }
                                }
                            } else { // si no encuentra en la lista de offer la billetera vigente whatsapp ilimitado
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    SaldoUnidad suuu = new SaldoUnidad();
                                    suuu.setSaldo(-1.0);
                                    boolean mostrarr = UtilCcws1.mostrarCabecera(cab, suuu);
                                    if (cabecera.isEmpty()) {
                                        if (mostrarr) {
                                            cabecera = cab.getNombre() + " NO";
                                        }
                                    } else {
                                        if (mostrarr) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                        }
                                    }
                                }
                            }
                        } else { // si no encuentra en las lista de offer la billetera whatsapp ilimitado
                            if (cab.getWhatsappIlimitado().equals("t")) {
                                SaldoUnidad suu = new SaldoUnidad();
                                suu.setSaldo(-1.0);
                                boolean mostrar = UtilCcws1.mostrarCabecera(cab, suu);
                                if (cabecera.isEmpty()) {
                                    if (mostrar) {
                                        cabecera = cab.getNombre() + " NO";
                                    }
                                } else {
                                    if (mostrar) {
                                        cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                    }
                                }
                            }
                        }
                    } else {
                        SaldoUnidad su = new SaldoUnidad();
                        su.setSaldo(-1.0);
                        boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);
                        if (cabecera.isEmpty()) {
                            if (mostrar) {
                                cabecera = cab.getNombre();
                            }
                        } else {
                            if (mostrar) {
                                cabecera = cabecera + "\n" + cab.getNombre();
                            }
                        }
                    }
                }
            }

        }

        return cabecera;
    }

    private String getOnDeman(Config actualConfig, List<WalletComverse> listaBilleterasCMV,
            List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        String cabecera = "";

        List<Cabecera> listCabecera = actualConfig.getListCabeceras();

        if (listCabecera != null) {
            for (Cabecera cab : listCabecera) {

                if (cab.getTipoNavegacion().equals(Constantes.TN_ON_DEMAND) || cab.getTipoNavegacion().equals(Constantes.TN_TODOS)) {
                    List<Billetera> listBilleterasCabecera = cab.getListBilleteras();

                    if (listBilleterasCabecera != null && !listBilleterasCabecera.isEmpty()) {

                        SaldoUnidad su = UtilCcws1.obtenerSaldosDeBilleteras(listaBilleterasCMV, listBilleterasCabecera, listOffer, listAcumulador, logPrefijo);

                        if (su != null) { // no mostrar si la billetera de la cabecera ha expirado
                            boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);
                            if (su.getSaldo() != -1) {
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    String sino = "NO";
                                    if (su.isWhatsppIlimitado()) {
                                        sino = "SI";
                                    }
                                    if (cabecera.isEmpty()) {
                                        if (mostrar) {
                                            cabecera = cab.getNombre() + " " + sino;
                                        }
                                    } else {
                                        if (mostrar) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + sino;
                                        }
                                    }
                                } else {
                                    String auxmo = gestorConfiguraciones.obtenerParametroBD("default_mostar_minutos", logPrefijo);
                                    boolean mostrarMinutos = (auxmo.equals("true"));
                                    int idSegundo = Integer.parseInt(Propiedades.TEXTO_SEGUNDO);

                                    if (cabecera.isEmpty()) {

                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }
                                            cabecera = cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONDEMAND, logPrefijo);
                                        }
                                    } else {
                                        if (mostrar) {
                                            if (cab.getUnitType().equals(idSegundo) && mostrarMinutos) {
                                                String aux = UtilCcws1.getMinutesSeconds(su.getSaldo());
                                                su.setSaldoUnidad(aux);
                                            }

                                            /* Cambio vigencia maxima
                                             cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad();*/
                                            cabecera = cabecera + "\n" + cab.getNombre() + " " + su.getSaldoUnidad() + UtilDate.fechaCabeceras(su.getFechaExpiracion(), Propiedades.VIGENCIA_MAXIMA_FECHA_CABECERAS_BANDERA_ONDEMAND, logPrefijo);
                                        }
                                    }
                                }
                            } else { // si no encuentra en la lista de offer la billetera vigente whatsapp ilimitado
                                if (cab.getWhatsappIlimitado().equals("t")) { // ES WHATSAPP ILIMITADO
                                    SaldoUnidad suuu = new SaldoUnidad();
                                    suuu.setSaldo(-1.0);
                                    boolean mostrarr = UtilCcws1.mostrarCabecera(cab, suuu);
                                    if (cabecera.isEmpty()) {
                                        if (mostrarr) {
                                            cabecera = cab.getNombre() + " NO";
                                        }
                                    } else {
                                        if (mostrarr) {
                                            cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                        }
                                    }
                                }
                            }
                        } else { // si no encuentra en las lista de offer la billetera whatsapp ilimitado
                            if (cab.getWhatsappIlimitado().equals("t")) {
                                SaldoUnidad suu = new SaldoUnidad();
                                suu.setSaldo(-1.0);
                                boolean mostrar = UtilCcws1.mostrarCabecera(cab, suu);
                                if (cabecera.isEmpty()) {
                                    if (mostrar) {
                                        cabecera = cab.getNombre() + " NO";
                                    }
                                } else {
                                    if (mostrar) {
                                        cabecera = cabecera + "\n" + cab.getNombre() + " NO";
                                    }
                                }
                            }
                        }
                    } else {
                        SaldoUnidad su = new SaldoUnidad();
                        su.setSaldo(-1.0);
                        boolean mostrar = UtilCcws1.mostrarCabecera(cab, su);
                        if (cabecera.isEmpty()) {
                            if (mostrar) {
                                cabecera = cab.getNombre();
                            }
                        } else {
                            if (mostrar) {
                                cabecera = cabecera + "\n" + cab.getNombre();
                            }
                        }
                    }
                }
            }

        }

        return cabecera;
    }

    private SaldoUnidad obtenerSaldoHijos(List<Menu> listaMenu, Integer menuId, List<WalletComverse> listaBilleteraCMV, List<Offer> listOffer, List<Acumulador> listAcumulador, String logPrefijo) {
        List<Menu> listaHijo = UtilCcws1.getListaMenuHijos(listaMenu, menuId);
        if (!listaHijo.isEmpty()) {
            for (Menu auxMenu : listaHijo) {
                //for (int i = 0; i < listaHijo.size(); i++) {
                //Menu auxMenu = listaHijo.get(i);
                SaldoUnidad su = UtilCcws1.obtenerSaldosDeBilleterasSinExpiracion(listaBilleteraCMV, auxMenu.getListBilletera(), listOffer, listAcumulador, logPrefijo);
                if (su != null) {
                    LOG.debug(logPrefijo + "Saldo Unidad " + su.toString());
                    boolean mostrar;
                    if (!auxMenu.getInformacion().equals(Constantes.INFO_PAQUETIGOS_ACUMULADOS)) {
                        mostrar = UtilCcws1.mostrarMenu(auxMenu, su);
                    } else {
                        mostrar = UtilCcws1.mostrarMenuCalculado(auxMenu, su);
                    }
                    if (mostrar) {
                        return su;
                    }
                }
            }
        }
        return null;
    }

}
