/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.Cabecera;
import org.apache.log4j.Logger;
import myapps.cmn.util.ConexionBD;

/**
 *
 * @author Jose Luis
 */
public class CabeceraDao {

    public static final Logger LOG = Logger.getLogger(CabeceraDao.class);

    public static List<Cabecera> getCabecerasByConfigId(int configId) throws Exception {
        LOG.debug("Obteniendo cabecera por Configuracion Id: " + configId);
        List<Cabecera> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM CABECERA WHERE CONFI_ID = ? AND ESTADO = 't' ORDER BY POSICION ASC";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Cabecera myCabecera = new Cabecera();
                        myCabecera.setCabeceraId(rs.getInt("cabecera_id"));
                        myCabecera.setNombre(rs.getString("nombre"));
                        myCabecera.setConfigId(rs.getInt("confi_id"));
                        myCabecera.setMostrarSiempre(rs.getString("visible"));
                        myCabecera.setPosicion(rs.getInt("posicion"));
                        myCabecera.setTipoNavegacion(rs.getString("tipo_navegacion"));
                        myCabecera.setMayorCero(rs.getString("saldo_calculado"));
                        myCabecera.setWhatsappIlimitado(rs.getString("whatsapp_ilimitado"));
                        myCabecera.setUnitType(rs.getInt("unit_type_id"));
                        result.add(myCabecera);
                    }
                }
            }
        } catch (SQLException ex) {
            LOG.warn("[getCabecerasByConfigId] Error al intentar cerrar el "
                    + "ResultSet despues de obtener la lista de cabeceras por "
                    + "configId " + configId + " SQLException:" + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getCabecerasByConfigId] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de cabeceras por configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getCabecerasByConfigId] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de cabeceras por configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getCabecerasByConfigId] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

}
