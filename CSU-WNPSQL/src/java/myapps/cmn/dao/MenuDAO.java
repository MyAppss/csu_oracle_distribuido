/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import myapps.cmn.vo.Menu;
import org.apache.log4j.Logger;
import myapps.cmn.util.ConexionBD;

/**
 *
 * @author Vehimar
 */
public class MenuDAO {

    public static final Logger LOG = Logger.getLogger(MenuDAO.class);

    public static List<Menu> getMenu(int nivel, int configId) {
        LOG.debug("Obtener menu por Nivel: " + nivel + " configuracion Id: " + configId);
        List<Menu> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT *  FROM MENU WHERE CONFI_ID= ?  AND NIVEL = ? AND VISIBLE = 't' AND ESTADO = 't' ORDER BY POSICION ASC";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                stmt.setInt(2, nivel);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Menu menu = new Menu();
                        menu.setId(rs.getInt("menu_id"));
                        menu.setNombre(rs.getString("nombre"));
                        menu.setPosicion(rs.getInt("posicion"));
                        menu.setMostrarSimpre(rs.getString("visible"));
                        menu.setMostrarMayorCero(rs.getString("mostarMayorCero"));
                        menu.setMostrarMayorCeroAcu(rs.getString("mostarMayorCeroAcumulado"));
                        menu.setTipoUnidad(rs.getInt("unit_type_id"));
                        menu.setTieneHijos(rs.getString("tienehijos"));
                        result.add(menu);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getCategoria] Error al intentar obtener la lista de "
                    + "categoria name id " + nivel + " "
                    + "SQLException:" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getCategoria] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de categoria por "
                            + "name" + nivel + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getCategoria] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de categoria por name " + nivel + " SQLException:" + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[obtenerPlan] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

    public static List<Menu> getMenuByNombre(String nombre) throws Exception {
        LOG.debug("Obteniendo lista de Menus por Nombre: " + nombre);
        List<Menu> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT *  FROM MENU WHERE NOMBRE = ?  AND CANAL  = ?  AND VISIBLE = 't'";

          
            conn = ConexionBD.openDBConnection();

            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, nombre);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Menu menu = new Menu();
                        menu.setId(rs.getInt("menu_id"));
                        menu.setNombre(rs.getString("nombre"));
                        menu.setPosicion(rs.getInt("posicion"));
                        menu.setMostrarSimpre(rs.getString("visible"));
                        menu.setPadre(rs.getInt("padre"));
                        menu.setDescripcion(rs.getString("descripcion"));
                        menu.setMostrarMayorCero(rs.getString("mostarMayorCero"));
                        menu.setMostrarMayorCeroAcu(rs.getString("mostarMayorCeroAcumulado"));
                        menu.setTipoUnidad(rs.getInt("unit_type_id"));
                        menu.setTieneHijos(rs.getString("tienehijos"));
                        result.add(menu);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getCategoria] Error al intentar obtener la lista de "
                    + "categoria name id " + nombre + " "
                    + "SQLException:" + e.getMessage());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOG.warn("[getCategoria] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de categoria por "
                            + "name" + nombre + " SQLException:" + e.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getCategoria] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de categoria por name " + nombre + " SQLException:" + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getCategoria] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

    public static List<Menu> getMenusByConfigId(int configId) throws Exception {
        LOG.debug("Obteniendo lista menu por ConfiguracionId: " + configId);
        List<Menu> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM MENU  WHERE CONFI_ID = ? AND ESTADO = 't' ORDER BY POSICION ASC";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Menu myMenu = new Menu();
                        myMenu.setId(rs.getInt("menu_id"));
                        myMenu.setNombre(rs.getString("nombre"));
                        myMenu.setPosicion(rs.getInt("posicion"));
                        myMenu.setMostrarSimpre(rs.getString("visible"));
                        myMenu.setNivel(rs.getInt("nivel"));
                        myMenu.setPadre(rs.getInt("padre"));
                        myMenu.setDescripcion(rs.getString("descripcion"));
                        myMenu.setMostrarMayorCero(rs.getString("mostarMayorCero"));
                        myMenu.setCanal(rs.getString("canal"));
                        myMenu.setConfigId(rs.getInt("confi_id"));
                        myMenu.setInformacion(rs.getString("informacion"));
                        myMenu.setMostrarMayorCeroAcu(rs.getString("mostarMayorCeroAcumulado"));
                        myMenu.setTipoUnidad(rs.getInt("unit_type_id"));
                        myMenu.setTieneHijos(rs.getString("tienehijos"));
                        result.add(myMenu);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getMenusByConfigId] Error al intentar obtener la lista de "
                    + "menus configId " + configId + " "
                    + "SQLException:" + e.getMessage());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de menus por "
                            + "configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de menus por configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

    public static List<Menu> getMenusByPadre(int idPadre) {
        LOG.debug("Obteniendo lsita de Menu por Menu Padre: " + idPadre);
        List<Menu> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM MENU WHERE PADRE = ?  ORDER BY POSICION ASC";

            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, idPadre);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Menu myMenu = new Menu();
                        myMenu.setId(rs.getInt("menu_id"));
                        myMenu.setNombre(rs.getString("nombre"));
                        myMenu.setPosicion(rs.getInt("posicion"));
                        myMenu.setMostrarSimpre(rs.getString("visible"));
                        myMenu.setNivel(rs.getInt("nivel"));
                        myMenu.setPadre(rs.getInt("padre"));
                        myMenu.setDescripcion(rs.getString("descripcion"));
                        myMenu.setMostrarMayorCero(rs.getString("mostarMayorCero"));
                        myMenu.setCanal(rs.getString("canal"));
                        myMenu.setConfigId(rs.getInt("confi_id"));
                        myMenu.setInformacion(rs.getString("informacion"));
                        myMenu.setMostrarMayorCeroAcu(rs.getString("mostarMayorCeroAcumulado"));
                        myMenu.setTipoUnidad(rs.getInt("unit_type_id"));
                        myMenu.setTieneHijos(rs.getString("tienehijos"));
                        result.add(myMenu);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getMenusByConfigId] Error al intentar obtener la lista de "
                    + "menus idPadre " + idPadre + " "
                    + "SQLException:" + e.getMessage());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de menus por "
                            + "idPadre " + idPadre + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de menus por idPadre " + idPadre + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getMenusByConfigId] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

}
