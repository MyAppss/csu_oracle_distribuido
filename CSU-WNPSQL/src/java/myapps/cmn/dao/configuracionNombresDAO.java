/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.ConfiguracionNombres;
import myapps.cmn.util.ConexionBD;
import org.apache.log4j.Logger;

/**
 *
 * @author Jose Luis
 */
public class configuracionNombresDAO {

    public static final Logger LOG = Logger.getLogger(configuracionNombresDAO.class);

    public static List<ConfiguracionNombres> getConfiguracionNombreByConfigId(int configId) throws Exception {
        LOG.debug("Obteniendo la lista de ConfiguracionNombre por confifId: " + configId);
        List<ConfiguracionNombres> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM CONFIG_ACUMULADO WHERE CONFIG_ID = ? ORDER BY BILLETERA_ID";

            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        ConfiguracionNombres configuracion = new ConfiguracionNombres();
                        configuracion.setConfiguracionNombresId(rs.getInt("config_acumulado_id"));
                        configuracion.setConfiguracionID(rs.getInt("config_id"));
                        configuracion.setBilleteraID(rs.getInt("billetera_id"));
                        configuracion.setNombreComercial(rs.getString("nombre_comercial"));
                        configuracion.setNombreAcumulado(rs.getString("nombre_acumulado"));
                        configuracion.setSegundaFechaExp(rs.getString("segunda_fecha_exp"));
                        result.add(configuracion);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getConfiguracionNombreByConfigId] Error al intentar obtener la lista de "
                    + "Configuracion Nombres configId " + configId + " "
                    + "SQLException:" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getConfiguracionNombreByConfigId] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de Configuracion Nombres por "
                            + "configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getConfiguracionNombreByConfigId] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de Configuracion Nombres por configId " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getConfiguracionNombreByConfigId] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

}
