package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.Parametro;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class ParametrosDAO {

    public static final Logger LOG = Logger.getLogger(ParametrosDAO.class);

    public static List<Parametro> getListParametros() {
        LOG.debug("Obteniendo la lista de la tabla: Parametros");
        List<Parametro> result = new LinkedList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "select  * from PARAMETRO";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Parametro myParametro = new Parametro();
                        myParametro.setNombre(rs.getString("nombre"));
                        myParametro.setValor(rs.getString("valor"));
                        myParametro.setDescripcion(rs.getString("descripcion"));
                        myParametro.setEstado(rs.getString("estado"));
                        result.add(myParametro);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getListParametros]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros] Error al intentar recuperar :" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros] Error al intentar cerrar Connection despues: SqlException: " + ex.getMessage());
                }
            }
        }
        return result;
    }

    public static Parametro obtenerByName(String name) throws Exception {
        LOG.debug("Obteniendo el Parametro: " + name + " de Tabla: Parametros");
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Parametro result = null;
        Connection conn = null;
        try {
            String sql = "select  * from PARAMETRO where NOMBRE =?";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        result = new Parametro();
                        result.setDescripcion(rs.getString("descripcion"));
                        result.setEstado(rs.getString("estado"));
                        result.setNombre(rs.getString("nombre"));
                        result.setValor(rs.getString("valor"));
                    }
                }
            }
        } catch (SQLException ex) {
            LOG.error("[obtenerByName] Error al intentar obtener "
                    + "parametro id=" + name
                    + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el ResultSet"
                            + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el Statement"
                            + "Param id=" + name
                            + "SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

    public static boolean UpdateParamResetConfig() {
        LOG.debug("Actualziando el parametro: sistema_consulta_saldo_sw_onchange=false");
        PreparedStatement st = null;
        Connection conn = null;
        boolean response = true;
        try {
            String sql = "update PARAMETRO SET VALOR = 'false' where NOMBRE='sistema_consulta_saldo_sw_onchange'";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.execute();
            }
        } catch (SQLException e) {
            LOG.error("[UpdateParamResetConfig] Error: " + e.getMessage());
            response = false;
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    response = false;
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    response = false;
                }
            }
        }
        return response;
    }

}
