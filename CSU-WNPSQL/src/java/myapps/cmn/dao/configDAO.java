/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import myapps.cmn.vo.Config;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class configDAO {

    public static final Logger LOG = Logger.getLogger(configDAO.class);

    public static Config obtenerByName(String nameCos) {
        LOG.debug("Obteniendo Configuracion por nombre: " + nameCos);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Config result = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM CONFIG WHERE NOMBRE= ? and  HABILITADO= 't' and ESTADO='t'";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, nameCos);
                rs = stmt.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        result = new Config();
                        result.setConfigId(rs.getInt("config_id"));
                        result.setDescripcion(rs.getString("descripcion"));
                        result.setEstado(rs.getString("estado"));
                        result.setHabilitado(rs.getString("habilitado"));
                        result.setMostraVigencia(rs.getString("mostrar_vigencia"));
                        result.setMostrarDpi(rs.getString("mostrar_dpi"));
                        result.setMostrarAcumuladosMegas(rs.getString("mostrar_acumulados_megas"));
                        result.setNombre(rs.getString("nombre"));
                        result.setSaludoInicial(rs.getString("saludo_inicial"));
                        result.setMostraBilleterasNoConfig(rs.getString("mostrar_billeteras_no_config"));
                        result.setEstado(rs.getString("estado"));
                    }
                }
            }
        } catch (SQLException ex) {
            LOG.error("[obtenerByName] Error al intentar obtener "
                    + "Cos id=" + nameCos
                    + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el ResultSet"
                            + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el Statement"
                            + "Cos id=" + nameCos
                            + "SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar la conexion"
                            + "Cos id=" + nameCos
                            + "SQLException:" + ex.getMessage());
                }
            }
        }
        return result;
    }

    public static Config obtenerById(int idConfiguracion) {
        LOG.debug("Obtener configuraicon por Id: " + idConfiguracion);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Config result = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM CONFIG WHERE CONFIG_ID= ? and HABILITADO= 't' and ESTADO='t'";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, idConfiguracion);
                rs = stmt.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        result = new Config();
                        result.setConfigId(rs.getInt("config_id"));
                        result.setDescripcion(rs.getString("descripcion"));
                        result.setEstado(rs.getString("estado"));
                        result.setHabilitado(rs.getString("habilitado"));
                        result.setMostraVigencia(rs.getString("mostrar_vigencia"));
                        result.setMostrarDpi(rs.getString("mostrar_dpi"));
                        result.setMostrarAcumuladosMegas(rs.getString("mostrar_acumulados_megas"));
                        result.setNombre(rs.getString("nombre"));
                        result.setSaludoInicial(rs.getString("saludo_inicial"));
                        result.setMostraBilleterasNoConfig(rs.getString("mostrar_billeteras_no_config"));
                        //result.setEstado(true);
                    }
                }
            }
        } catch (SQLException ex) {
            LOG.error("[obtenerByName] Error al intentar obtener "
                    + "Cos id=" + idConfiguracion
                    + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el ResultSet"
                            + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el Statement"
                            + "Cos id=" + idConfiguracion
                            + "SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar la conexion"
                            + "Cos id=" + idConfiguracion
                            + "SQLException:" + ex.getMessage());
                }
            }
        }
        return result;
    }

    public static List<Config> obtenerAllConfigs() {
        LOG.debug("Obteniendo la Lista de todas las configuraciones ");
        List<Config> result = new LinkedList<>();
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM CONFIG WHERE  ESTADO='t'";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        Config myConfig = new Config();
                        myConfig.setConfigId(rs.getInt("config_id"));
                        myConfig.setDescripcion(rs.getString("descripcion"));
                        myConfig.setEstado(rs.getString("estado"));
                        myConfig.setHabilitado(rs.getString("habilitado"));
                        myConfig.setMostraVigencia(rs.getString("mostrar_vigencia"));
                        myConfig.setMostrarDpi(rs.getString("mostrar_dpi"));
                        myConfig.setMostrarAcumuladosMegas(rs.getString("mostrar_acumulados_megas"));
                        myConfig.setNombre(rs.getString("nombre"));
                        myConfig.setSaludoInicial(rs.getString("saludo_inicial"));
                        myConfig.setMostraBilleterasNoConfig(rs.getString("mostrar_billeteras_no_config"));
                        result.add(myConfig);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[obtenerAllConfigs]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllConfigs] Error al intentar recuperar :" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllConfigs]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllConfigs] Error al intentar cerrar Connection despues: SqlException: " + ex.getMessage());
                }
            }
        }
        return result;
    }

}
