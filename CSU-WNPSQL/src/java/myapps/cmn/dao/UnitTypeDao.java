/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.UnitType;

/**
 *
 * @author Jose Luis
 */
public class UnitTypeDao {

    public static final Logger LOG = Logger.getLogger(UnitTypeDao.class);

    public static List<UnitType> getUniTypeforNombre(String nombre) throws Exception {
        LOG.debug("Obteniendo UnitType por nombre Id: " + nombre);
        List<UnitType> result = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "SELECT * FROM UNIT_TYPE WHERE NOMBRE = ? AND ESTADO = 't'";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, nombre);
                rs = stmt.executeQuery();
                if (rs != null) {
                    result = new ArrayList<>();
                    while (rs.next()) {
                        UnitType unitType = new UnitType();
                        unitType.setUnitTypeId(rs.getInt("UNIT_TYPE_ID"));
                        unitType.setNombre(rs.getString("NOMBRE"));
                        unitType.setDescripcion(rs.getString("DESCRIPCION"));
                        unitType.setEstado(rs.getString("ESTADO"));
                        result.add(unitType);
                    }
                }
            }
        } catch (SQLException ex) {
            LOG.warn("[getUniTypeforNombre] Error al intentar cerrar el "
                    + "ResultSet despues de obtener la lista de UnitType por "
                    + "nombre " + nombre + " SQLException:" + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUniTypeforNombre] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de UnitType por nombre " + nombre + " SQLException:" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUniTypeforNombre] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de UnitType por nombre " + nombre + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getUniTypeforNombre] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

}
