/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.Occ;
import org.apache.log4j.Logger;
import myapps.cmn.util.ConexionBD;

/**
 *
 * @author Vehimar
 */
public class OccDao {

    public static final Logger LOG = Logger.getLogger(OccDao.class);

    public static List<Occ> obtenerAllOcc() throws Exception {
        LOG.debug("Obteniendo todas la Lista de Occ");
        List<Occ> result = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "select\n"
                    + "  OCC.OCC_ID,OCC.CONFIG_ID,OCC.ORIGEN_ID,\n"
                    + "  ORIGEN.NOMBRE as nombre_origen, OCC.CORTO_ID, CORTO.NOMBRE as nombre_corto,\n"
                    + "  OCC.COS_ID, COS.NOMBRE as nombre_cos, OCC.POSICION\n"
                    + "from OCC, ORIGEN,CORTO,COS\n"
                    + "where OCC.OCC_ID<>0  and OCC.ESTADO='t' and OCC.ORIGEN_ID= ORIGEN.ORIGEN_ID\n"
                    + "and OCC.CORTO_ID= CORTO.CORTO_ID and OCC.COS_ID=COS.COS_ID and ORIGEN.ESTADO='t'\n"
                    + "and CORTO.ESTADO='t' and COS.ESTADO='t' order by CONFIG_ID, POSICION asc";
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        Occ occ = new Occ();
                        occ.setOccId(rs.getInt("occ_id"));
                        occ.setConfigId(rs.getInt("config_id"));
                        occ.setOrigenId(rs.getInt("origen_id"));
                        occ.setNombreOrigen(rs.getString("nombre_origen"));
                        occ.setCortoId(rs.getInt("corto_id"));
                        occ.setNombreCorto(rs.getString("nombre_corto"));
                        occ.setCosId(rs.getInt("cos_id"));
                        occ.setNombreCos(rs.getString("nombre_cos"));
                        result.add(occ);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[obtenerAllOcc]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllOcc] Error al intentar recuperar :" + ex.getMessage());
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllOcc]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[obtenerAllOcc] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
        }
        return result;
    }

}
