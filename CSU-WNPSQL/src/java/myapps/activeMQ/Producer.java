package myapps.activeMQ;

import java.io.Serializable;
import java.util.HashMap;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import myapps.cmn.util.Propiedades;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

public class Producer implements Serializable {

    private static final Logger log = Logger.getLogger(Producer.class);
    private static final String MQ_USER = Propiedades.MQ_USER;
    private static final String MQ_PASSWORD = Propiedades.MQ_PASSWORD;
    private static final String MQ_TIPO_CONEXION = Propiedades.MQ_TIPO_CONEXION;
    private static final String MQ_IP = Propiedades.MQ_IP;
    private static final int MQ_PUERTO = Propiedades.MQ_PUERTO;

    public boolean sendMessages(HashMap<String, Object> mensaje, String prefijo) {
        long tiempo = System.currentTimeMillis();
        boolean valor = false;
        Connection connection = null;

        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(MQ_USER, MQ_PASSWORD, MQ_TIPO_CONEXION + MQ_IP + ":" + MQ_PUERTO);
            connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue(Propiedades.MQ_COLA);

            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            sendMessage(mensaje, session, producer);
            session.commit();
            session.close();

            log.debug(prefijo + " Mensaje enviado correctamentea la Cola");
            valor = true;
        } catch (JMSException e) {
            log.error(prefijo + " Error al enviar al Mensaje a la Cola " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException ex) {
                    log.error("[sendMessages] " + prefijo + " Error al cerrar la COnexion hacia la Cola " + ex.getMessage());
                }
            }

        }
        log.debug("[sendMessages] " + prefijo + "  sendMessages se ejecuto en " + (System.currentTimeMillis() - tiempo) + " ms");
        return valor;
    }

    private void sendMessage(HashMap<String, Object> message, Session session, MessageProducer producer) throws JMSException {
        ObjectMessage mensaje = session.createObjectMessage(message);
        producer.send(mensaje);
    }

}
