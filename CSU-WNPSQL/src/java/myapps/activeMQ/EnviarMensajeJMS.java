/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.activeMQ;

import java.util.HashMap;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilUrl;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class EnviarMensajeJMS extends Thread {

    private static final Logger log = Logger.getLogger(EnviarMensajeJMS.class);
    private HashMap<String, Object> mensaje;
    private String logPrefijo;

    public EnviarMensajeJMS(HashMap<String, Object> mensaje, String logPrefijo) {
        this.mensaje = mensaje;
        this.logPrefijo = logPrefijo;
    }

    @Override
    public void run() {
        super.run();
        Integer n = 0;
        try {
            n = ColaThreadMax.poll();
            String mensajeString = UtilUrl.maptoString(mensaje);
            if (n != null) {
                Producer producer = new Producer();
                if (producer.sendMessages(mensaje, logPrefijo)) {
                    log.debug(logPrefijo + " El mensaje " + mensajeString + " se envio a la cola correctamente");
                } else {
                    log.debug(logPrefijo + " El mensaje " + mensajeString + " no se pudo  enviar a la cola: " + Propiedades.MQ_COLA);
                    ColaJMS.put(mensaje);
                    log.warn(logPrefijo + " El mensaje " + mensajeString + " sera enviado a la cola interna para su posterior reintento");
                }
            } else {
                ColaJMS.put(mensaje);
                log.warn(logPrefijo + " El mensaje " + mensajeString + " sera enviado a la cola interna para su posterior reintento");
            }

        } catch (Exception e) {
            log.error("Exception: " + e.getMessage(), e);
        } finally {
            if (n != null && n != 0) {
                ColaThreadMax.put(n);
            }
        }

    }
}
