/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.activeMQ;

import java.io.Serializable;
import javax.jms.Connection;
import javax.jms.JMSException;
import myapps.cmn.util.Propiedades;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ConexionJMS implements Serializable {

    public static final Logger LOG = Logger.getLogger(ConexionJMS.class);
    private static final long serialVersionUID = 1L;
    private static ConexionJMS _instance;
    private transient Connection connection = null;
    private ConexionJMS() {
        crearConexion();
    }

    public static synchronized ConexionJMS getInstance() {
        if (_instance == null) {
            _instance = new ConexionJMS();

        }
        return _instance;
    }

    public synchronized void reConexion(Connection connection) {
        try {
            if (connection == null) {
                LOG.info("Iniciando reconexion a ActiveMQ");
                ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Propiedades.MQ_USER, Propiedades.MQ_PASSWORD, Propiedades.MQ_TIPO_CONEXION + Propiedades.MQ_IP + ":" + Propiedades.MQ_PUERTO);
                connection = connectionFactory.createConnection();
                connection.start();
                LOG.info("Reconexion creada Satisfactoriamente ActiveMQ");
            }

        } catch (JMSException e) {
            LOG.error("[Producer] Error al crear la reconexion: " + e.getMessage(), e);
        }
    }

   private synchronized void crearConexion() {
        try {
            LOG.debug("Iniciando crear conexion a ActiveMQ");
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Propiedades.MQ_USER, Propiedades.MQ_PASSWORD, Propiedades.MQ_TIPO_CONEXION + Propiedades.MQ_IP + ":" + Propiedades.MQ_PUERTO);
            connection = connectionFactory.createConnection();
            connection.start();
            LOG.debug("Conexion creada Satisfactoriamente ActiveMQ");
        } catch (JMSException e) {
            connection = null;
            LOG.error("[Producer] Error al crear la conexion: " + e.getMessage(), e);
        }
    }

    public synchronized void cerrarConexion() {
        if (connection != null) {
            try {
                connection.close();
                LOG.debug("Conexion cerrada Satisfactoriamente ActiveMQ");
            } catch (JMSException ex) {
                LOG.error("[sendMessages]  Error al cerrar la Conexión hacia la Cola " + ex.getMessage(), ex);
            }
        }
    }

    public synchronized Connection getConnection() {
        return connection;
    }

}
