/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.gestorConfig;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import myapps.cmn.dao.CabeceraDao;
import myapps.cmn.dao.MenuDAO;
import myapps.cmn.dao.OccDao;
import myapps.cmn.dao.ParametrosDAO;
import myapps.cmn.dao.billeteraDAO;
import myapps.cmn.dao.configDAO;
import myapps.cmn.dao.configuracionNombresDAO;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilUrl;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.Cabecera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.ConfiguracionNombres;
import myapps.cmn.vo.Menu;
import myapps.cmn.vo.Occ;
import myapps.cmn.vo.Parametro;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class GestorConfiguraciones implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(GestorConfiguraciones.class);
    private static GestorConfiguraciones _instance;

    private List<Occ> listOcc;
    private List<Config> listConfiguraciones;
    private Config myConfigDefault;
    private Map<String, String> Parameters;

    private GestorConfiguraciones() {
        cargarParametros();
        cargarConfiguraciones();
        ParametrosDAO.UpdateParamResetConfig();
    }

    public static synchronized GestorConfiguraciones getInstance() {
        if (_instance == null) {
            if (UtilUrl.probarConexion(Propiedades.BD_SERVER, Propiedades.BD_PORT)) {
                _instance = new GestorConfiguraciones();
            } else {
                LOG.warn("NO CARGO LAS CONFIGURACIONES POR ERROR DE E/S A LA DB");
            }
        }
        return _instance;
    }

    private void cargarConfiguraciones() {
        LOG.debug("");
        LOG.debug("INICIANDO A CARGAR DATOS DE CONFIGURACIONES");
        try {
            //Cargar todos los nombres de configs
            listConfiguraciones = configDAO.obtenerAllConfigs();
            //Para cada config cargar sus Billeteras simples y Compuestas
            if (listConfiguraciones != null) {
                for (Config config : listConfiguraciones) {
                    List<Cabecera> listaCabeceras = obtenerListaDeCabeceras(config.getConfigId());
                    List<Menu> listaMenus = obtenerListaDeMenus(config.getConfigId());
                    List<ConfiguracionNombres> listaConfigNombres = configuracionNombresDAO.getConfiguracionNombreByConfigId(config.getConfigId());

                    config.setListCabeceras(listaCabeceras);
                    config.setListMenus(listaMenus);
                    config.setListConfiguracionNombres(listaConfigNombres);
                }
                LOG.debug("FINALIZO DE CARGAR DADOS DE CONFIGURACION SATISFACTORIO");
                LOG.debug("");
            } else {
                LOG.error("No existe Configuraciones definidas:La lista de configuraciones no tiene elementos");
            }
            this.myConfigDefault = loadConfigPorDefecto();
            this.listOcc = OccDao.obtenerAllOcc();
        } catch (Exception e) {
            LOG.error("[cargar] Exception: " + e.getMessage(), e);
        }
    }

    public synchronized void reset() {
        try {
            LOG.debug("INICIANDO A REALIZAR RESET DE LOS PARAMETROS DE LA BASE DE DATOS");
            cargarParametros();
            cargarConfiguraciones();
            ParametrosDAO.UpdateParamResetConfig();
            LOG.debug("FINALIZANDO DE REALIZAR RESET DE LOS PARAMETROS DE LA BASE DE DATOS");
        } catch (Exception e) {
            LOG.error("[reset] Exception: " + e.getMessage(), e);
        }
    }

    private void cargarParametros() {
        try {
            LOG.debug("INICIANDO A CARGAR PARAMETROS DE LA BASE DE DATOS");
            List<Parametro> listParam = ParametrosDAO.getListParametros();
            Parameters = new HashMap<>();
            for (Parametro parametro : listParam) {
                Parameters.put(parametro.getNombre(), parametro.getValor());
            }
            LOG.debug("FINALIZO DE CARGAR LOS PARAMetros DE LA BASE DE DATOS");
        } catch (Exception e) {
            LOG.error("[cargarParametros] Exception: "+e.getMessage(), e);
        }
    }

    public void setParameters(Map<String, String> Parameters) {
        this.Parameters = Parameters;
    }

    private List<Cabecera> obtenerListaDeCabeceras(int configId) throws Exception {
        List<Cabecera> listCabeceras = CabeceraDao.getCabecerasByConfigId(configId);
        if (listCabeceras != null) {
            LOG.debug("Configuracion Id: " + configId + ", Lista de cabeceras: " + listCabeceras.size());
            for (Cabecera cabecera : listCabeceras) {
                List<Billetera> listBilleteraByCabeceraId = billeteraDAO.getBilleterasByCabeceraId(cabecera.getCabeceraId());
                if (listBilleteraByCabeceraId != null) {
                    LOG.debug("listBilleteraByCabeceraId: " + listBilleteraByCabeceraId);
                    LOG.debug("_cabeceraId: " + cabecera.getCabeceraId() + ", nombre: " + cabecera.getNombre() + ", billetera.size: " + listBilleteraByCabeceraId.size());
                    cabecera.setListBilleteras(listBilleteraByCabeceraId);
                }
            }
        } else {
            LOG.debug("listCabeceras.size: " + listCabeceras);
        }
        LOG.debug("");
        return listCabeceras;
    }

    private List<Menu> obtenerListaDeMenus(int configId) throws Exception {
        List<Menu> listMenus = MenuDAO.getMenusByConfigId(configId);
        if (listMenus != null) {
            LOG.debug("Configuracion Id: " + configId + ", Lista de Menu: " + listMenus.size());
            for (Menu menu : listMenus) {
                List<Billetera> listBilleteraByMenuId = billeteraDAO.getBilleterasByMenuId(menu.getId());
                if (listBilleteraByMenuId != null) {
                    LOG.debug("listBilleteraByMenuId" + listBilleteraByMenuId);
                    LOG.debug("_menuId: " + menu.getId() + " Nombre: " + menu.getNombre() + ", billetera.size: " + listBilleteraByMenuId.size());
                    menu.setListBilletera(listBilleteraByMenuId);
                }
            }
        } else {
            LOG.debug("listMenus.size: " + listMenus);
        }
        LOG.debug("");
        return listMenus;
    }

    private Config loadConfigPorDefecto() {
        Config myConfig = null;
        LOG.debug("INICIANDO A CONSTRUIR CONFIGURACION POR DEFAULT");
        try {
            String StrIdConfigDefault = obtenerParametroBD("default_configuracion_id", null); //getParametro("default_configuracion_id");            
            LOG.debug("[loadConfigPorDefecto] default_configuracion_id: " + StrIdConfigDefault);
            if (!"".equals(StrIdConfigDefault)) {
                int idConfigDefault = Integer.parseInt(StrIdConfigDefault);

                if (idConfigDefault >= 0) {
                    myConfig = configDAO.obtenerById(idConfigDefault);
                    if (myConfig != null) {
                        //obtener billeteras simples
                        List<Billetera> listaBilleterasSimples = billeteraDAO.getBilleterasByConfigId(myConfig.getConfigId());
                        //obtener billeteras compuestas---------------------------------
                        //List<ComposicionBilletera> listaBilleterasComp = obtenerListaBilleterasComp(myConfig.getConfigId());
                        //-------------------------------------------------------------- 
                        if (listaBilleterasSimples == null) {
                            listaBilleterasSimples = new LinkedList<>();
                        }
                        //if (listaBilleterasComp == null) {
                        //    listaBilleterasComp = new LinkedList<ComposicionBilletera>();
                        //}
                        myConfig.setListBilleterasSimples(listaBilleterasSimples);
                        //myConfig.setListBilleterasCompuestas(listaBilleterasComp);

                        List<Cabecera> listaCabeceras = obtenerListaDeCabeceras(myConfig.getConfigId());
                        List<Menu> listaMenus = obtenerListaDeMenus(myConfig.getConfigId());
                        List<ConfiguracionNombres> listaConfigNombres = configuracionNombresDAO.getConfiguracionNombreByConfigId(myConfig.getConfigId());

                        myConfig.setListCabeceras(listaCabeceras);
                        myConfig.setListMenus(listaMenus);

                        myConfig.setListConfiguracionNombres(listaConfigNombres);

                    } else {
                        LOG.warn("[loadConfigPorDefecto]||ConfigId=" + StrIdConfigDefault + "|No se encontro Config OCC por defecto");
                    }
                } else {
                    LOG.warn("[loadConfigPorDefecto]||ConfigId=" + StrIdConfigDefault + "|No se Definio Config OCC por defecto");
                }
            } else {
                LOG.warn("[loadConfigPorDefecto]|| No se puedo obtener la ConfigId por Defecto de la BD");
            }
        } catch (Exception e) {
            LOG.error("[loadConfigPorDefecto] al intentar recuperar Config OCC, :" + e.getMessage(), e);
        }
        return myConfig;
    }

    public Config getMyConfigDefault() {
        return myConfigDefault;
    }

    public void setMyConfigDefault(Config myConfigDefault) {
        this.myConfigDefault = myConfigDefault;
    }

    private int getConfigId(String sessionId, String canal, String corto, String nameCos, String logPrefijo) {
        long z = System.currentTimeMillis();
        int resp = -1;
        try {
            if (listOcc != null) {
                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal)
                            && occ.getNombreCorto().equals(corto)
                            && occ.getNombreCos().equals(nameCos)) {
                        //WEB	123	FF
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: WEB	123 FF , DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal)
                            && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals("*")) {
                        //WEB	123	*
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: WEB	 123  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals(nameCos)) {
                        //WEB	*	FF
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: WEB	 * FF, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals(nameCos)) {
                        //*	123	FF
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: *	123  FF, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals("*")) {
                        //WEB	*	*
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: WEB	 *  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals("*")) {
                        //*	123	*
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: *	123  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals(nameCos)) {
                        //*	*	FF
                        resp = occ.getConfigId();
                        LOG.debug(logPrefijo + " Conbinacion encontrada: *	*  FF, DI:" + resp);
                        return resp;
                    }
                }
                LOG.debug(logPrefijo + " No se encontro Ninguna Conbinacion....");
            }

        } catch (Exception e) {
            LOG.error("[getConfigId]", e);
        }
        LOG.debug("...:::" + logPrefijo + "TERMINO DE OBTENER CONFIG ID: " + (System.currentTimeMillis() - z) + " ms :::...");

        return resp;
    }

    private Config obtenerConfigById(int idConfig) {
        Config resp = null;
        try {
            if (listConfiguraciones != null) {
                for (Config config : listConfiguraciones) {
                    if (config.getConfigId() == idConfig) {
                        return config;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("[obtenerConfigById]", e);
            return resp;
        }
        return resp;
    }

    public synchronized String obtenerParametroBD(String nameParameter, String logPrefijo) {
        String resp = "";
        try {
            if (Parameters != null && !Parameters.isEmpty()) {
                resp = Parameters.get(nameParameter);
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[getParametro]", e);
        }
        return resp;
    }

    public Map<String, String> getParameters() {
        return Parameters;
    }

    public Config obtenerConfigByOrigenCortoCos(String isdn, String nameCos, String canal, String corto, String logPrefijo, String sessionId) {
        long z = System.currentTimeMillis();
        LOG.debug(logPrefijo + "|ISDN=" + isdn + "|nameCanal=" + canal + "|nameCorto=" + corto + "|nameCOS=" + nameCos + " inicio metodo para obtener Configuracion COSNAME: " + nameCos);
        Config config = null;
        int idConfig = 0;
        try {
            if (!nameCos.equals("ERROR")) {
                idConfig = getConfigId(sessionId, canal, corto, nameCos, logPrefijo);
                LOG.debug(logPrefijo + " ID CONFIG: " + idConfig);
                if (idConfig > 0) {
                    Config aux = obtenerConfigById(idConfig);
                    if (aux != null) {
                        config = aux;
                        LOG.debug(logPrefijo + " ID CONFIG: " + idConfig + " Nombre: " + config.getNombre());
                    }
                }
            }
            if (config == null) {
                LOG.debug(logPrefijo + "|ISDN=" + isdn + "|NO se encontro Configuracion definida para |nameCanal= " + canal + "|nameCorto= " + corto + "|nameCos= " + nameCos + ", se cargara Configuracion por Defecto");
                config = myConfigDefault;
            }

        } catch (Exception e) {
            LOG.error(logPrefijo + "Error al obtener configuracion: ", e);
        }
        LOG.debug("...:::" + logPrefijo + " TERMINO DE OBTENER POR ORGIEN CORTO COS: " + (System.currentTimeMillis() - z) + " ms :::...");
        return config;
    }

    /*public Config obtenerConfiguracionForOrigenCortoCos(String isdn, String nameCos, String canal, String corto, String logPrefijo, String sessionId) {
        long z = System.currentTimeMillis();
        LOG.info("|ISDN=" + isdn + "|nameCanal=" + canal + "|nameCorto=" + corto + "|nameCOS=" + nameCos + " inicio metodo para obtener Configuracion COSNAME: " + nameCos);
        Config myConfig = null;
        try {
            String strNameCos = nameCos;
            if (!strNameCos.equals("ERROR")) {
                myConfig = obtenerConfigByOrigenCortoCos(isdn, nameCos, canal, corto, logPrefijo, sessionId);
            }
            if (myConfig == null) {
                LOG.info("|ISDN=" + isdn + "|NO se encontro Configuracion definida para |nameCanal= " + canal + "|nameCorto= " + corto + "|nameCos= " + strNameCos + ", se cargara Configuracion por Defecto");
                myConfig = myConfigDefault;
            }
        } catch (Exception e) {
            LOG.error("|ISDN=" + isdn + "| Al intentar obtener una configuracion OCC:" + e.getMessage());
        }
        LOG.info("...::: Session ID: " + sessionId + " TERMINO OBTENER CONF POR ORIG CORTO COS: " + (System.currentTimeMillis() - z) + " ms :::...");
        return myConfig;
    }*/
    public static boolean pingDB() {
        long z = System.currentTimeMillis();
        boolean sw = false;
        try {
            Pattern pattern = Pattern.compile("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b");
            //    Matcher matcher = pattern.matcher(Propiedades.DB_URL);
            Matcher matcher = pattern.matcher("SOLO FUNCIONA CON IP, no con DNS");
            String ipServidor;
            if (matcher.find()) {
                ipServidor = matcher.group();
                InetAddress in = InetAddress.getByName(ipServidor);
                if (in.isReachable(1000)) {
                    LOG.debug("Existe conexion y acceso de E/S a la base de datos: " + ipServidor);
                    sw = true;
                } else {
                    LOG.error("Servidor inalcanzable Error de E/S: " + ipServidor);
                    sw = false;
                }
            }

        } catch (IOException e) {
            LOG.error(e.getMessage());
            sw = false;
        }
        LOG.debug("...:: TERMINO DE REALIZAR PING: " + (System.currentTimeMillis() - z) + " :::...");
        return sw;
    }

}
