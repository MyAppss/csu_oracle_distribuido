/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.restfull;

import com.google.gson.Gson;
import java.util.HashMap;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import myapps.activeMQ.ColaJMS;
import myapps.activeMQ.ColaThreadMax;

import myapps.cmn.bl.WvnConsultar;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilJava;
import myapps.pool.cbs.PoolCbs;
import myapps.pool.cws.ColaDesconectado;
import myapps.pool.cws.Pool;
import org.apache.log4j.Logger;

/**
 * REST Web Service
 *
 * @author hp
 */
@Path("consulta")
public class ConsultaResource {

    @Context
    private UriInfo wsContext;
    private static final Logger LOG = Logger.getLogger(ConsultaResource.class);

    /**
     * Creates a new instance of ConsultaResource
     */
    public ConsultaResource() {

        /* COMENTAR PARA NO ESCRIBIR EN LOG DE CATALINA
        String archivoLog4j = "log4j.properties";
        URL urll = Thread.currentThread().getContextClassLoader().getResource(archivoLog4j);
        if (urll != null) {
            PropertyConfigurator.configure(urll);
        }*/
    }

    /**
     * Retrieves representation of an instance of
     * myapps.restfull.ConsultaResource
     *
     * @param httpHeaders
     * @param requestContext
     * @param sessionId
     * @param isdn
     * @param channel
     * @param corto
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("general")
    public Response getGeneral(@Context HttpHeaders httpHeaders,
            @Context HttpServletRequest requestContext,
            @QueryParam("sessionId") String sessionId,
            @QueryParam("isdn") String isdn,
            @Nullable @QueryParam("channel") String channel,
            @Nullable @QueryParam("short") String corto
    ) {

        LOG.info("...::: Session ID: " + sessionId + " CONSULTANDO: " + isdn + " :::...");

        String address = requestContext.getRemoteAddr();
        if (corto == null || corto.equals("")) {
            corto = Propiedades.SHORT;
        }
        if (channel == null || channel.equals("")) {
            channel = Propiedades.CHANNEL;
        }

        // String canal = "USSD";
        String logPrefijo = " [sessionId: " + sessionId + "][isdn: " + isdn + "][canal: " + channel + "][corto: " + corto + "][ip: " + address + "] ";

        String requisitos = UtilJava.requestValido(isdn, sessionId, channel, corto, logPrefijo);
        if (requisitos.isEmpty()) {
            WvnConsultar jsonConsultar = new WvnConsultar();

            long zz = System.currentTimeMillis();
            LOG.debug(logPrefijo);
            String json = jsonConsultar.consultarSaldo(sessionId, isdn, channel, corto, logPrefijo, address);
            LOG.info("...:::" + logPrefijo + " DEVOLVIENDO CONSULTA GENERADA: " + json + " :::...");
            LOG.info("...:::" + logPrefijo + " TERMINO REALIZAR CONSULTA CSU ISDN: " + isdn + " - " + (System.currentTimeMillis() - zz) + " ms :::...");
            LOG.info("");

            return Response.ok(json, MediaType.APPLICATION_JSON).build();
        } else {
            String requestInvalido = UtilJava.devolverRespuestaFallida(isdn, requisitos, logPrefijo);
            return Response.ok(requestInvalido, MediaType.APPLICATION_JSON).build();
        }

    }

    /**
     * Retrieves representation of an instance of myapps.csu.restfull.status
     *
     * @param httpHeaders
     * @param requestContext
     *
     * @return an instance of java.lang.String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("status")
    public Response status(@Context HttpHeaders httpHeaders,
            @Context HttpServletRequest requestContext) {
        HashMap map = new HashMap();
        map.put("cola_jms_size", ColaJMS.size());
        map.put("nro_hilos", Propiedades.MQ_HILOS);
        map.put("max_thread_disponiblbe", ColaThreadMax.size());
        map.put("pool_ccws", Pool.size());
        map.put("pool_cbs", PoolCbs.size());
        map.put("cola_desconectados", ColaDesconectado.size());
        Gson gson = new Gson();
        String representacionJSON = gson.toJson(map);

        return Response.ok(representacionJSON, MediaType.APPLICATION_JSON).build();
    }

}
