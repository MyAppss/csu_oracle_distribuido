/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.services.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import myapps.cmn.util.Propiedades;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import myapps.dao.MapeoOfertasSingleton;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
@WebServlet("/ServletParametro")
public class ServletParametro  extends HttpServlet {

    private final GestorConfiguraciones gestorConfiguraciones;
    private final MapeoOfertasSingleton mapeoOfertasSingleton;

    public static final Logger LOG = Logger.getLogger(ServletParametro.class);

    public ServletParametro() {
        super();
        this.gestorConfiguraciones = GestorConfiguraciones.getInstance();
        this.mapeoOfertasSingleton = MapeoOfertasSingleton.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        // response.getWriter().append("Served at: ").append(request.getContextPath());
        // Set response content type
        // response.setContentType("text/html");
        long z = System.currentTimeMillis();
        LOG.debug("INCIAR RESET DE CONFIGURACIONES");
        String respuesta = Actualizar();
        response.setHeader("Access-Control-Allow-Origin", "http://192.168.0.22");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET");

        response.setHeader("X-XSS-Protection", "1; mode=block");
        response.setHeader("X-CONTENT-TYPE-OPTIONS", "nosniff");
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        if (request.getScheme() != null && request.getScheme().toLowerCase().trim().equals("https")) {
            response.setHeader("Content-Security-Policy", "script-src 'unsafe-inline' " + "http://192.168.0.22:8084");
        }
        response.setHeader("Access-Control-Allow-Headers", "*");
        response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

        response.setContentType("text/plain");
        // String result=request.getRemoteAddr();
        try (PrintWriter out = response.getWriter()) {
            // String result=request.getRemoteAddr();
            String result = respuesta;
            // byte[] utf8 = result.getBytes("UTF8");
            // result = Base64.encodeToString(utf8, false);

            out.println(result);
        }
        // byte[] utf8 = result.getBytes("UTF8");
        // result = Base64.encodeToString(utf8, false);
        LOG.debug("FINALIZACION RESET DE CONFIGURACIONES EN " + (System.currentTimeMillis() - z) + " ms");
    }

    public synchronized String Actualizar() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            this.gestorConfiguraciones.reset();
            if (Propiedades.GQL_BANDERA) {
                this.mapeoOfertasSingleton.reset();
            }
            stringBuilder.append("Actualizacion de Configuaraciones Satisfactoria");
        } catch (Exception e) {
            LOG.error("Error al recargar parametro mediante el servlet: ", e);
            stringBuilder.append("Error al recargar parametro mediante el servlet: ").append(e.getMessage());
        }
        return stringBuilder.toString();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
