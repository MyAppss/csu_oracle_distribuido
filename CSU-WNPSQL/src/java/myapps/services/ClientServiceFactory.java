/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.services;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.callback.CallbackHandler;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.log4j.Logger;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;

/**
 *
 * @author MyApps
 */
public class ClientServiceFactory {

    private final QName service;
    private final Class serviceClass;
    public static final Logger LOG = Logger.getLogger(ClientServiceFactory.class);

    public ClientServiceFactory(QName service, Class serviceClass) {
        this.service = service;
        this.serviceClass = serviceClass;
    }

    public Object create(String url, long connectionTimeout, long receiveTimeout, String userName, String password) {
        Properties properties = System.getProperties();
        properties.put("org.apache.cxf.stax.allowInsecureParser", "1");
        System.setProperties(properties);

        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(serviceClass);
        jaxWsProxyFactoryBean.setServiceName(service);
        WSS4JOutInterceptor wsOut;
        if (userName != null && password != null && !userName.trim().isEmpty() && !password.trim().isEmpty()) {
            //AUTHORIZATION
            Map<String, Object> outProps = new HashMap<>();
            outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
            // Specify our username
            outProps.put(WSHandlerConstants.USER, userName);
            // Password type : plain text
            outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
            // Callback used to retrieve password for given user.
            outProps.put(WSHandlerConstants.PW_CALLBACK_REF, (CallbackHandler) callbacks -> {
                WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
                pc.setPassword(password);
            });
            wsOut = new WSS4JOutInterceptor(outProps);
            jaxWsProxyFactoryBean.getOutInterceptors().add(wsOut);

        }

        jaxWsProxyFactoryBean.getOutInterceptors().add(new RequestInterceptor());
        // Attache your ResponseInterceptor here
        jaxWsProxyFactoryBean.getInInterceptors().add(new ResponseInterceptor());

        Object clientService = jaxWsProxyFactoryBean.create();
        BindingProvider provider = (BindingProvider) clientService;
        provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);

        Client client = ClientProxy.getClient(clientService);
        HTTPConduit http = (HTTPConduit) client.getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        //TIME OUT
        httpClientPolicy.setConnectionTimeout(connectionTimeout);
        httpClientPolicy.setReceiveTimeout(receiveTimeout);
        httpClientPolicy.setAllowChunking(false);
        //SSL
        TLSClientParameters tlsParams = new TLSClientParameters();
        TrustManager[] trustAllCerts = new TrustManager[]{new TrustAllX509TrustManager()};
        tlsParams.setTrustManagers(trustAllCerts);
        tlsParams.setSSLSocketFactory(buildSocketFactory(trustAllCerts));
        tlsParams.setDisableCNCheck(true);

        http.setTlsClientParameters(tlsParams);
        http.setClient(httpClientPolicy);

        return clientService;

    }

    public static SSLSocketFactory buildSocketFactory(TrustManager[] trustAllCerts) {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, trustAllCerts, null);
            return ctx.getSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            return null;
        }

    }

    private static class TrustAllX509TrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    public static class RequestInterceptor extends LoggingOutInterceptor {

        public RequestInterceptor() {
            super(Phase.PRE_STREAM);
        }

        @Override
        public void handleMessage(Message message) throws Fault {
            OutputStream out = message.getContent(OutputStream.class);
            final CacheAndWriteOutputStream newOut = new CacheAndWriteOutputStream(out);
            message.setContent(OutputStream.class, newOut);
            newOut.registerCallback(new CachedOutputStreamCallback() {
                @Override
                public void onClose(CachedOutputStream cachedOutputStream) {
                    try {
                        StringBuilder builder = new StringBuilder();
                        cachedOutputStream.writeCacheTo(builder, limit);
                        String requestXml = builder.toString();
                        LOG.info("LOGGER REQUEST XML: " + requestXml);
                    } catch (IOException e) {
                        LOG.error("FAILD REQUEST XML: " + e.getMessage());
                    }
                }

                @Override
                public void onFlush(CachedOutputStream cachedOutputStream) {

                }
            });
        }
    }

    public static class ResponseInterceptor extends LoggingInInterceptor {

        public ResponseInterceptor() {
            super(Phase.RECEIVE);
        }

        @Override
        public void handleMessage(Message message) throws Fault {
            InputStream is = null;
            CachedOutputStream bos = null;
            try {
                is = message.getContent(InputStream.class);
                bos = new CachedOutputStream();
                if (is != null) {
                    IOUtils.copy(is, bos);
                }
                bos.flush();
                message.setContent(InputStream.class, bos.getInputStream());
                bos.registerCallback(new CachedOutputStreamCallback() {
                    @Override
                    public void onClose(CachedOutputStream co) {
                        try {
                            StringBuilder builder = new StringBuilder();
                            co.writeCacheTo(builder, limit);
                            String responseXml = builder.toString();
                            LOG.info("LOGGER RESPONSE XML: " + responseXml);
                        } catch (IOException e) {
                            LOG.info("FAILD RESPONSE XML: " + e.getMessage());
                        }
                    }

                    @Override
                    public void onFlush(CachedOutputStream cachedOutputStream) {

                    }
                });
            } catch (IOException e) {
                LOG.error("FAILD REQUEST XML: " + e.getMessage());
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        LOG.error("Error al cerrar InputStream " + e.getMessage());
                    }
                }
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        LOG.error("Error al cerrar CachedOutputStream " + e.getMessage());
                    }
                }
            }
        }
    }

}
