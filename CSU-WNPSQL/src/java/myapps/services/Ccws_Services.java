/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.services;

import com.tigo.ea.ccws.iingresotarjetavoucher.IIngresoTarjetaVoucher;
import javax.xml.namespace.QName;

/**
 *
 * @author hp
 */
public class Ccws_Services extends ClientServiceFactory {

    public final static QName SERVICE = new QName("http://IIngresoTarjetaVoucher.ccws.ea.tigo.com/", "IIngresoTarjetaVoucher");

   

    public Ccws_Services() {
        super(SERVICE, IIngresoTarjetaVoucher.class);
    }

    public IIngresoTarjetaVoucher getPort(String url, long connectionTimeout, long receiveTimeout, String userName, String password) {
        return (IIngresoTarjetaVoucher) create(url, connectionTimeout, receiveTimeout, userName, password);
    }

  

}
