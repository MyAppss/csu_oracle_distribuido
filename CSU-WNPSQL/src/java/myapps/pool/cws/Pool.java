/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class Pool implements Serializable {

    public static final Logger LOG = Logger.getLogger(Pool.class);

    private static final long serialVersionUID = 1L;
    private static final Queue<ConexionCcws> LIST_POOL;
    private static int cantidadRealConexiones;

    static {
        LIST_POOL = new LinkedList<>();
        cantidadRealConexiones = 0;
    }

    public synchronized static boolean put(ConexionCcws conexion) {
        return LIST_POOL.offer(conexion);
    }

    public synchronized static boolean contains(ConexionCcws conexion) {
        return LIST_POOL.contains(conexion);
    }

    public synchronized static ConexionCcws poll() {
        if (!LIST_POOL.isEmpty()) {
            return LIST_POOL.poll();
        }
        return null;
    }

    public synchronized static boolean removed(ConexionCcws conexion) {
        return LIST_POOL.remove(conexion);
    }

    public synchronized static long size() {
        return LIST_POOL.size();
    }

    public synchronized static void addCantidadRealConexiones() {
        cantidadRealConexiones++;
    }

    public synchronized static void quitCantidadRealConexiones() {
        cantidadRealConexiones--;
    }

    public synchronized static int getCantidadRealConexiones() {
        return cantidadRealConexiones;
    }
}
