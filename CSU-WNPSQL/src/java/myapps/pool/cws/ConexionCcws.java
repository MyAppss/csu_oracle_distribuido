/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import com.tigo.ea.ccws.iingresotarjetavoucher.IIngresoTarjetaVoucher;
import java.io.Serializable;

/**
 *
 * @author hp
 */
public class ConexionCcws implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private transient IIngresoTarjetaVoucher port;

    public ConexionCcws(Integer id, IIngresoTarjetaVoucher port) {
        super();
        this.id = id;
        this.port = port;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IIngresoTarjetaVoucher getPort() {
        return port;
    }

    public void setPort(IIngresoTarjetaVoucher port) {
        this.port = port;
    }
}
