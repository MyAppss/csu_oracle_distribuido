/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import com.tigo.ea.ccws.iingresotarjetavoucher.IIngresoTarjetaVoucher;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.logging.Level;
import myapps.cmn.util.Propiedades;
import myapps.services.Ccws_Services;
import myapps.cmn.util.UtilUrl;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ConexionServiceCcws implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final Logger LOG = Logger.getLogger(ConexionServiceCcws.class);
    private static String URL = Propiedades.CCWS_HOST;
    private static final int CONNECTION_TIMEOUT = Integer.parseInt(Propiedades.ccwsConnectionTimeout);
    private static final int RECEIVE_TIMEOUT = Integer.parseInt(Propiedades.ccwsReceiveTimeout);
    private static final String USER_NAME = Propiedades.CCWS_USER;
    private static final String USER_PASS = Propiedades.CCWS_PASS;

    public ConexionServiceCcws() {
    }

    public ConexionCcws crearConexionCcws(Integer id) throws Exception {
        if (Boolean.TRUE.equals(Propiedades.conexionPorIP)) {
            URL = UtilUrl.getIp(URL);
        }
        IIngresoTarjetaVoucher port = getService();
        ConexionCcws conexion = null;
        if (port != null) {
            conexion = new ConexionCcws(id, port);
        } else {
            LOG.error("Error al crear conexion Port NULL: " + id + "URL: " + URL);
        }
        return conexion;
    }

    private IIngresoTarjetaVoucher getService() throws Exception {
        IIngresoTarjetaVoucher port = null;
        try {
            URL ping = new URL(URL);
            HttpURLConnection con = (HttpURLConnection) ping.openConnection();
            con.setRequestMethod("POST");
            con.setConnectTimeout(CONNECTION_TIMEOUT);
            int responseCode = con.getResponseCode();
            LOG.debug("\nSending 'POST' request to URL : " + URL);
            LOG.debug("Response Code : " + responseCode);

            Ccws_Services services = new Ccws_Services();
            port = services.getPort(URL, CONNECTION_TIMEOUT, RECEIVE_TIMEOUT, USER_NAME, USER_PASS);
            return port;
        } catch (RuntimeException e) {
            LOG.error("Error al crear conexion RuntimeException: " + e.getMessage(), e);
        } catch (MalformedURLException ex) {
            LOG.error("Error al crear conexion MalformedURLException: " + ex.getMessage(), ex);
        } catch (SocketTimeoutException ex) {
            LOG.error("Error al crear conexion SocketTimeoutException: " + ex.getMessage(), ex);
        } catch (ProtocolException ex) {
            java.util.logging.Logger.getLogger(ConexionServiceCcws.class.getName()).log(Level.SEVERE, null, ex);
            LOG.error(URL+"Error al crear conexion PROTOCOLO: " + ex.getMessage(), ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ConexionServiceCcws.class.getName()).log(Level.SEVERE, null, ex);
            LOG.error( URL+"Error al crear conexion IO: " + ex.getMessage(), ex);
        }
        return port;
    }

    public void cerrarConexion(ConexionCcws con) {
        try {
            if (con != null) {
                ((java.io.Closeable) con.getPort()).close();
                LOG.debug("Conexion cerrada satisfactoriamente: " + con.getId());
            }
        } catch (IOException e) {
            LOG.error("Error al cerrar la conexion TSB: " + con.getId() + ": ", e);
        }
    }

}
