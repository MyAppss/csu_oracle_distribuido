/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author MyApps
 */
public class ThreadConsultar extends Thread {

    public static final Logger LOG = Logger.getLogger(ThreadConsultar.class);
    private final ConexionCcws conexionCcws;
    private static final String URL = Propiedades.CCWS_HOST;
    private static final int TIME_OUT = Integer.parseInt(Propiedades.ccwsConnectionTimeout);

    public ThreadConsultar(ConexionCcws conexion) {
        super();
        this.conexionCcws = conexion;
    }

    @Override
    public void run() {
        Integer id = null;
        try {
            id = conexionCcws.getId();
            URL ping = new URL(URL);
            HttpURLConnection con = (HttpURLConnection) ping.openConnection();
            con.setRequestMethod("GET");
            con.setConnectTimeout(TIME_OUT);
            int responseCode = con.getResponseCode();
            LOG.debug("\nSending 'GET' request to URL : " + URL);
            LOG.debug("Response Code : " + responseCode);
            LOG.debug("ValidacionSatisfactoriaConexionId : " + conexionCcws.getId());
            Pool.put(conexionCcws);

        } catch (IOException e) {
            LOG.error("ValidacionFallidaConexionId: " + conexionCcws.getId() + " " + e.getMessage(), e);
            // significa que tiene que crear una nueva conexion
            if (id != null) {
                try {
                    ConexionServiceCcws cc = new ConexionServiceCcws();
                    ConexionCcws conexionNueva = cc.crearConexionCcws(id);
                    if (conexionNueva != null) {
                        boolean put = Pool.put(conexionNueva);
                        if (put) {
                        } else {
                            LOG.debug("Fallido al encolar la conexion id: " + id);
                            Pool.quitCantidadRealConexiones();
                            cc.cerrarConexion(conexionCcws);
                        }
                    } else {
                        LOG.debug("No se pudo crear la conexion por validacion fallida: " + conexionCcws.getId());
                        Pool.quitCantidadRealConexiones();
                        ColaDesconectado.put(id);
                    }
                } catch (Exception ex) {
                    LOG.warn("Error al crear conexion Exception: ", ex);
                    LOG.error("Exception: " + ex.getMessage(), ex);
                }
            } else {
                Pool.quitCantidadRealConexiones();
            }
            LOG.debug("CantidadRealConexiones : " + Pool.getCantidadRealConexiones());
        }

    }
}
