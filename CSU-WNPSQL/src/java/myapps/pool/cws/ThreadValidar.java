/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import java.util.ArrayList;
import java.util.List;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ThreadValidar extends Thread {

    public static final Logger LOG = Logger.getLogger(ThreadValidar.class);
    private boolean live;

    public ThreadValidar() {
        super();
        this.live = true;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    @Override
    public void run() {
        while (this.live) {
            if (Propiedades.realizarValidacion) {
                try {
                    Thread.sleep(Propiedades.tareaValidarConexionMilisegundos);
                } catch (InterruptedException e) {
                    LOG.warn("[ThreadValidar] ERROR SPLEEP CCWSSERVICES : " + e.getMessage());
                    Thread.currentThread().interrupt();
                }
                LOG.debug("CANTIDAD CONEXIONES POOL : " + Pool.size());
                List<ConexionCcws> listConexion = new ArrayList<>();
                ConexionCcws con = Pool.poll();
                while (con != null) {
                    listConexion.add(con);
                    con = Pool.poll();
                }
                LOG.debug("CANTIDAD LISTA CONEXIONES VALIDAR :" + listConexion.size());
                for (ConexionCcws conexion : listConexion) {
                    ThreadConsultar tc = new ThreadConsultar(conexion);
                    tc.start();
                }
            }
        }
        LOG.debug("...::: FINALIZO HILO DE VALIDAR CONEXION CCWSSERVICES :::...");
    }

}
