/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class IniciarPool {
    
    private static final Logger LOG = Logger.getLogger(IniciarPool.class);

    public void crearConexiones() {
        try {
            if (Pool.size() == 0) {
                ConexionServiceCcws cc = new ConexionServiceCcws();
                int poolSize = Propiedades.cantidadConexionsPool;

                for (int i = 1; i <= poolSize; i++) {
                    ConexionCcws con = cc.crearConexionCcws(i);
                    if (con != null) {
                        //LOG.info("Conexion creada satisfactoriamente id CcwsServices: " + i);
                        boolean put = Pool.put(con);
                        if (put) {
                            Pool.addCantidadRealConexiones();
                        } else {
                            //LOG.info("Falla al encolar la conexion id CcwsServices: " + i);
                            cc.cerrarConexion(con);
                        }
                    } else {
                        //LOG.info("Conexion fallida id CcwsServices: " + i);
                        ColaDesconectado.put(i);
                    }
                }

                /* TODO DESCOMENTAR CUANDO SE PONGA EN EL SERVIDOR 
                 this.threadValidar = new ThreadValidar();
                 this.threadValidar.start();
                 threadReconexion = new ThreadReconexion();
                 threadReconexion.start();
                 */
                 
            }
        } catch (Exception e) {
            LOG.error("Error al iniciar contexto: ", e);
        }
    }

}
