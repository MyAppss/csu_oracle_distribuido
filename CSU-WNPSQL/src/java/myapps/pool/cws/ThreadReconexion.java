/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cws;

import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ThreadReconexion extends Thread {

    public static final Logger LOG = Logger.getLogger(ThreadReconexion.class);
    private boolean live;

    public ThreadReconexion() {
        super();
        this.live = true;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    @Override
    public void run() {
        while (live) {
            try {
                Thread.sleep(Propiedades.tareaValidarConexionMilisegundos);
            } catch (InterruptedException e) {
                LOG.warn("[ThreadReconexion] ERROR SPLEEP CCWSSERVICES : " + e.getMessage());
                Thread.currentThread().interrupt();
            }
            LOG.debug("TAMANHO DE COLA DESCONECTADOS CCWSSERVICES : " + ColaDesconectado.size());

            Integer id = ColaDesconectado.poll();
            boolean existeErrorConexion = false;

            while (id != null && !existeErrorConexion) {
                try {
                    ConexionServiceCcws cc = new ConexionServiceCcws();
                    ConexionCcws conexion = cc.crearConexionCcws(id);

                    if (conexion != null) {
                        LOG.debug("Reconexion satisfactoria CCWSSERVICES: " + id);
                        boolean put = Pool.put(conexion);
                        if (put) {
                            Pool.addCantidadRealConexiones();
                            id = ColaDesconectado.poll();
                        } else {
                            LOG.debug("Fallido al encolar la conexion CCWSSERVICES: " + id);
                            ColaDesconectado.put(id);
                            cc.cerrarConexion(conexion);
                        }
                    } else {
                        LOG.debug("Reconexion Fallida a CCWSSERVICES: " + id);
                        ColaDesconectado.put(id);
                        existeErrorConexion = true;
                    }
                } catch (Exception ex) {
                    LOG.error("[ThreadReconexion] ERROR: " + ex.getMessage() + " - " + ex.getCause());
                }
            }
        }
        LOG.debug("...::: FINALIZO HILO DE RECONEXION CONEXIONES CCWSSERVICES :::..");
    }

}
