/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ThreadConsultarCbs extends Thread {

    private static final Logger LOG = Logger.getLogger(ThreadConsultarCbs.class);
    private final ConexionCbs conexion;
    private static final String URL = Propiedades.SOAP_CBS_HOST;
    private static final int TIME_OUT = Propiedades.SOAP_CBS_CONNECTIONTIMEOUT;

    public ThreadConsultarCbs(ConexionCbs conexion) {
        super();
        this.conexion = conexion;
    }

    @Override
    public void run() {
        Integer id = null;
        try {
            id = conexion.getId();
            LOG.debug("Procesando id: " + id);
            if (Propiedades.SOAP_CBS_URL_TEST) {
                java.net.URL ping = new URL(URL);
                HttpURLConnection con = (HttpURLConnection) ping.openConnection();
                con.setRequestMethod("GET");
                con.setConnectTimeout(TIME_OUT);
                int responseCode = con.getResponseCode();
                LOG.debug("\nSending 'GET' request to URL : " + URL);
                LOG.debug("Response Code : " + responseCode);
                LOG.debug("ValidacionSatisfactoriaConexionId : " + conexion.getId());
            }
            PoolCbs.put(conexion);

        } catch (IOException e) {
            LOG.info("ValidacionFallidaConexionId: " + conexion.getId() + " " + e.getMessage());
            // significa que tiene que crear una nueva conexion
            if (id != null) {
                try {
                    GestorConexionCbs gestorConexion = new GestorConexionCbs();
                    ConexionCbs conexionNueva = gestorConexion.crearConexion(id);
                    if (conexionNueva != null) {
                        boolean put = PoolCbs.put(conexionNueva);
                        if (Boolean.TRUE.equals(put)) {
                            LOG.debug("Conexion añadida al pool");
                        } else {
                            LOG.debug("Fallido al encolar la conexion id: " + id);
                            PoolCbs.quitCantidadRealConexiones();
                            gestorConexion.cerrarConexion(conexion);
                        }
                    } else {
                        LOG.debug("No se pudo crear la conexion por validacion fallida: " + conexion.getId());
                        PoolCbs.quitCantidadRealConexiones();
                        ColaDesconectadoCbs.put(id);
                    }
                } catch (Exception ex) {
                    LOG.warn("Error al crear conexion Exception: ", ex);
                }
            } else {
                PoolCbs.quitCantidadRealConexiones();
            }
            LOG.debug("Cantidad Real de Conexiones : " + PoolCbs.getCantidadRealConexiones());
        }

    }
}
