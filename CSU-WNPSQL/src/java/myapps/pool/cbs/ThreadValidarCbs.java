/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import java.util.ArrayList;
import java.util.List;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ThreadValidarCbs extends Thread {

    private static final Logger LOG = Logger.getLogger(ThreadValidarCbs.class);
    private boolean live;

    public ThreadValidarCbs() {
        super();
        this.live = true;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    @Override
    public void run() {
        while (this.live) {
            if (Propiedades.SOAP_CBS_REALIZARVALIDACION) {
                try {
                    Thread.sleep(Propiedades.SOAP_CBS_TAREAVALIDARCONEXIONMILISEGUNDOS);
                } catch (InterruptedException e) {
                    LOG.warn("[ThreadValidar] ERROR SPLEEP CBS_SERVICES : " + e.getMessage(), e);
                    Thread.currentThread().interrupt();
                }
                LOG.debug("CANTIDAD CONEXIONES POOL : " + PoolCbs.size());
                List<ConexionCbs> listConexion = new ArrayList<>();
                ConexionCbs con = PoolCbs.poll();
                while (con != null) {
                    listConexion.add(con);
                    con = PoolCbs.poll();
                }
                LOG.debug("CANTIDAD LISTA CONEXIONES VALIDAR :" + listConexion.size());
                for (ConexionCbs conexion : listConexion) {
                    ThreadConsultarCbs tc = new ThreadConsultarCbs(conexion);
                    tc.start();
                }
            }
        }
        LOG.info("...::: FINALIZO HILO DE VALIDAR CONEXION CBS_SERVICES :::...");
    }

}
