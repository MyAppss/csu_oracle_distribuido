/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ThreadReconexionCbs extends Thread {

    private static final Logger LOG = Logger.getLogger(ThreadReconexionCbs.class);
    private boolean live;

    public ThreadReconexionCbs() {
        super();
        this.live = true;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    @Override
    public void run() {
        while (live) {
            try {
                Thread.sleep(Propiedades.SOAP_CBS_TAREAVALIDARCONEXIONMILISEGUNDOS);
            } catch (InterruptedException e) {
                LOG.warn("[ThreadReconexion] ERROR SPLEEP CBS_SERVICES : " + e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
            LOG.debug("TAMANHO DE COLA DESCONECTADOS CBS_SERVICES : " + ColaDesconectadoCbs.size());

            Integer id = ColaDesconectadoCbs.poll();
            boolean existeErrorConexion = false;

            while (id != null && !existeErrorConexion) {
                try {
                    GestorConexionCbs gestorConexion = new GestorConexionCbs();
                    ConexionCbs conexion = gestorConexion.crearConexion(id);

                    if (conexion != null) {
                        LOG.info("Reconexion satisfactoria CBS_SERVICES: " + id);
                        boolean put = PoolCbs.put(conexion);
                        if (put) {
                            PoolCbs.addCantidadRealConexiones();
                            id = ColaDesconectadoCbs.poll();
                        } else {
                            LOG.debug("Fallido al encolar la conexion CBS_SERVICES: " + id);
                            ColaDesconectadoCbs.put(id);
                            gestorConexion.cerrarConexion(conexion);
                        }
                    } else {
                        LOG.debug("Reconexion Fallida a CBS_SERVICES: " + id);
                        ColaDesconectadoCbs.put(id);
                        existeErrorConexion = true;
                    }
                } catch (Exception ex) {
                    LOG.error("[ThreadReconexion] ERROR: " + ex.getMessage(), ex);
                }
            }
        }
        LOG.info("...::: FINALIZO HILO DE RECONEXION CONEXIONES CBS_SERVICES :::..");
    }
}
