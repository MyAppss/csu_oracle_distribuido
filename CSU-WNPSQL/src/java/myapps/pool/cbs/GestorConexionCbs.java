/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import com.huawei.www.bme.cbsinterface.bcservices.BcServices_ServiceLocator;
import com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class GestorConexionCbs implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GestorConexionCbs.class);

    private static final String url = Propiedades.SOAP_CBS_HOST;
    private static final int timeOut = Propiedades.SOAP_CBS_CONNECTIONTIMEOUT;
    public static final String NOMBRE_SERVICIO = Propiedades.SOAP_CBS_NOMBRESERVICIO;

    public ConexionCbs crearConexion(Integer id) throws Exception {

        ConexionCbs conexion = null;
        BcServices_ServiceLocator service = getService();

        if (service != null) {
            BcServicsBindingStub port = (BcServicsBindingStub) service.getBcServicesPort();

            if (port != null) {

                conexion = new ConexionCbs(id, port);
                conexion.setService(service);
                log.debug("Conexion creada del servicio: " + NOMBRE_SERVICIO);
            } else {
                log.error("Error al crear conexion port null del servicio: " + NOMBRE_SERVICIO);
            }
        } else {
            log.error("Error al crear conexion service null del servicio: " + NOMBRE_SERVICIO);
        }
        return conexion;
    }

    private BcServices_ServiceLocator getService() {
        BcServices_ServiceLocator service = null;
        HttpURLConnection con = null;
        try {
            if (Propiedades.SOAP_CBS_URL_TEST) {
                log.debug("Validando Conexion por HttpURLConnection ");
                URL wsdlLocation = new URL(url);
                con = (HttpURLConnection) wsdlLocation.openConnection();
                con.setRequestMethod("GET");
                con.setConnectTimeout(timeOut);
                con.connect();
                int responseCode = con.getResponseCode();
                log.debug("Sending 'GET' request to URL : " + url);
                log.debug("Response Code del servicio " + NOMBRE_SERVICIO + ": " + responseCode);
            }

            service = new BcServices_ServiceLocator();
            service.setBcServicesPortEndpointAddress(url);

            log.debug("Url del servicio " + NOMBRE_SERVICIO + ": " + service.getBcServicesPortAddress());

            BcServicsBindingStub stub = (BcServicsBindingStub) service.getBcServicesPort();
            stub.setTimeout(5000);
            

            log.debug("Conexion creada del servicio: " + NOMBRE_SERVICIO);

        } catch (RuntimeException e) {
            log.error("Error al crear conexion RuntimeException del servicio " + url + ": ", e);

        } catch (MalformedURLException e) {
            log.error("Error al crear conexion MalformedURLException del servicio " + url + ": ", e);
            // } catch (SocketTimeoutException e) {
            // log.warn("Error al crear conexion SocketTimeoutException del servicio " + NOMBRE_SERVICIO + ": ", e);
        } catch (Exception e) {
            log.error("Error al crear conexion del servicio " + url + ": ", e);

        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

        return service;
    }

    public void cerrarConexion(ConexionCbs con) {
        try {
            if (con != null) {
                con.setPort(null);
                con.setService(null);
                log.info("Conexion cerrada satisfactoriamente: " + con.getId() + ", servicio: " + NOMBRE_SERVICIO);
            }
        } catch (Exception e) {
            log.error("Error al cerrar la conexion: " + con.getId() + ", servicio: " + NOMBRE_SERVICIO + " ", e);

        }
    }

}
