/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class PoolCbs implements Serializable {

    private static final Logger LOG = Logger.getLogger(PoolCbs.class);

    private static final long serialVersionUID = 1L;
    private static final Queue<ConexionCbs> LIST_POOL;
    private static int cantidadRealConexiones;

    static {
        LIST_POOL = new LinkedList<>();
        cantidadRealConexiones = 0;
    }

    public static synchronized  boolean put(ConexionCbs conexion) {
        return LIST_POOL.offer(conexion);
    }

    public static synchronized boolean contains(ConexionCbs conexion) {
        return LIST_POOL.contains(conexion);
    }

    public static synchronized ConexionCbs poll() {
        if (!LIST_POOL.isEmpty()) {
            return LIST_POOL.poll();
        }
        return null;
    }

    public static synchronized boolean removed(ConexionCbs conexion) {
        return LIST_POOL.remove(conexion);
    }

    public static synchronized long size() {
        return LIST_POOL.size();
    }

    public static synchronized void addCantidadRealConexiones() {
        cantidadRealConexiones++;
    }

    public static synchronized void quitCantidadRealConexiones() {
        cantidadRealConexiones--;
    }

    public static synchronized int getCantidadRealConexiones() {
        return cantidadRealConexiones;
    }
}
