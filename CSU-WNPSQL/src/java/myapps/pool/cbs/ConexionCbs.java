/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import com.huawei.www.bme.cbsinterface.bcservices.BcServices_ServiceLocator;
import com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub;
import java.io.Serializable;

/**
 *
 * @author hp
 */
public class ConexionCbs implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private transient BcServicsBindingStub port;
    private transient BcServices_ServiceLocator service;

    public ConexionCbs(Integer id, BcServicsBindingStub port) {
        super();
        this.id = id;
        this.port = port;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BcServicsBindingStub getPort() {
        return port;
    }

    public void setPort(BcServicsBindingStub port) {
        this.port = port;
    }

    public BcServices_ServiceLocator getService() {
        return service;
    }

    public void setService(BcServices_ServiceLocator service) {
        this.service = service;
    }

}
