/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.pool.cbs;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author hp
 */
public class ColaDesconectadoCbs implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Queue<Integer> COLA;

    static {
        COLA = new LinkedList<>();
    }

    public synchronized static boolean put(Integer id) {
        return COLA.offer(id);
    }

    public synchronized static Integer poll() {
        return COLA.poll();
    }

    public synchronized static int size() {
        return COLA.size();
    }

}
