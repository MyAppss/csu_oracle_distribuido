/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.context;

import java.util.UUID;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import myapps.activeMQ.ColaThreadMax;
import myapps.activeMQ.ConexionJMS;
import myapps.activeMQ.ThreadJms;
import myapps.cmn.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
@WebListener
public class InitContextListener implements ServletContextListener {
    
    public static final Logger LOG = Logger.getLogger(InitContextListener.class);
    private Boolean iniciado = false;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            LOG.info("=========The Application is starting up!============");
            iniciado = true;
            iniciarThread();
            LOG.info("========Finalizando Creación Pool de Conexiones============");
        } catch (Exception e) {
            LOG.error("Exception: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            LOG.info("========Iniciando Cerrado Pool de Conexiones============");
            iniciado = false;
            ConexionJMS.getInstance().cerrarConexion();
            LOG.info("========Finalizando Cerrado Pool de Conexiones============");
        } catch (Exception e) {
            LOG.error("Exception: " + e.getMessage(), e);
        }
    }
    
    public synchronized Boolean getIniciado() {
        return iniciado;
    }
    
    public synchronized void setIniciado(Boolean iniciado) {
        this.iniciado = iniciado;
    }
    
    public void iniciarThread() {
        for (int i = 1; i <= Propiedades.MQ_MAX_THREAD; i++) {
            ColaThreadMax.put(i);
        }
        LOG.info("Se  cartgaron " + Propiedades.MQ_MAX_THREAD + " elementos a la cola");
        
        LOG.info("Iniciando Hilos ThreadJMS: " + Propiedades.MQ_HILOS + " Nro Hilos");
        for (int i = 1; i <= Propiedades.MQ_HILOS; i++) {
            ThreadJms jms = new ThreadJms(i, UUID.randomUUID().toString(), this);
            jms.start();
        }
        
    }
    
}
