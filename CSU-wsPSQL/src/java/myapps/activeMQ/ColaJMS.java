/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.activeMQ;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author GENOSAURER
 */
public class ColaJMS implements Serializable{

    private static final long serialVersionUID = 1L;
    private static final Queue<HashMap<String, Object>> LIST_POOL;
    private static int cantidadConsultas;

    static {
        LIST_POOL = new LinkedList<>();
        cantidadConsultas = 0;
    }


    public static synchronized boolean put(HashMap<String, Object> consultaServicio) {
        return LIST_POOL.offer(consultaServicio);
    }

    public static synchronized boolean contains(HashMap<String, Object> consultaServicio) {
        return LIST_POOL.contains(consultaServicio);
    }

    public static synchronized HashMap<String, Object> poll() {
        if (!LIST_POOL.isEmpty()) {
            return LIST_POOL.poll();
        }
        return null;
    }


    public static synchronized boolean removed(HashMap<String, Object> consultaServicio) {
        return LIST_POOL.remove(consultaServicio);
    }

    public static synchronized long size() {
        return LIST_POOL.size();
    }

    public static synchronized void addCantidadConsultas() {
        cantidadConsultas++;
    }

    public static synchronized void quitCantidadConsultas() {
        cantidadConsultas--;
    }

    public static synchronized int getCantidadConsultas() {
        return cantidadConsultas;
    }
}

