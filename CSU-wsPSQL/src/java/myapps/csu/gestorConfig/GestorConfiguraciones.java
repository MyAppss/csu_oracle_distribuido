/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.gestorConfig;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import myapps.cmn.dao.OccDao;
import myapps.cmn.dao.ParametrosDAO;
import myapps.cmn.dao.billeteraDAO;
import myapps.cmn.dao.composicionBilleteraDAO;
import myapps.cmn.dao.configDAO;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.ComposicionBilletera;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Occ;
import myapps.cmn.vo.Parametro;

import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class GestorConfiguraciones implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(GestorConfiguraciones.class);
    private static GestorConfiguraciones _instance;
//   Banco BancoCurrent; 
//    private List<Cos> listCos;
    private List<Occ> listOcc;
    private List<Config> listConfiguraciones;
    private Config myConfigDefault;
//   List<Opcion> ListOpciones;
    Map<String, String> Parameters;

    private GestorConfiguraciones() {
        //Connection con = null;
        try {
            //con = ConexionBD.getConnection();
            cargarParametros(/*con*/);
            cargar(/*con*/);
            ParametrosDAO.UpdateParamResetConfig(/*con*/);
        } catch (Exception e) {
            LOG.error("[GestorConfiguraciones|Constructor]", e);
        }
        /*finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    LOG.warn("[Gestor configuraciones] Error al cerrar connection", ex);
                }
            }
        }
         */

    }
    //---------------------------------------------------------------------------- 

    private void cargar(/*Connection con*/) {
        LOG.debug("[cargar]:iniciando carga datos ");
        try {
            //Cargar todos los nombres de configs
            listConfiguraciones = configDAO.obtenerAllConfigs( /*con*/);
            //Para cada config cargar sus Billeteras simples y Compuestas
            if (listConfiguraciones != null) {
                for (Config config : listConfiguraciones) {
                    List<Billetera> listaBilleterasSimples = billeteraDAO.getBilleterasByConfigId(config.getConfigId() /*, con*/);

                    if (listaBilleterasSimples != null) {
                        LOG.debug("ID CONFIG: " + config.getConfigId() + ", size list billeteras simples: " + listaBilleterasSimples.size());
                        config.setListBilleterasSimples(listaBilleterasSimples);
                    } else {
                        LOG.debug("ID CONFIG: " + config.getConfigId() + ", size list billeteras: null");
                    }

                    List<ComposicionBilletera> listaBilleterasComp = obtenerListaBilleterasComp(config.getConfigId() /*, con*/);

                    if (listaBilleterasComp != null) {
                        LOG.debug("ID_CONFIG: " + config.getConfigId() + ", COMPOSICION_BILLETERAS: " + listaBilleterasComp.toString());
                        LOG.debug("ID_CONFIG: " + config.getConfigId() + ", size list billeteras compuestas: " + listaBilleterasComp.size());
                        config.setListBilleterasCompuestas(listaBilleterasComp);
                    } else {
                        LOG.debug("ID_CONFIG: " + config.getConfigId() + ", COMPOSICION_BILLETERAS: " + listaBilleterasComp);
                    }
                }
            } else {
                LOG.warn("[cargar]No existe Configuraciones definidas:La lista de configuraciones no tiene elementos");
            }
            //cargar Config por default            
            myConfigDefault = loadConfigPorDefecto(/*con*/);
            //cargar todos los COS que tengan definida una config                      
//            listCos = cosDAO.obtenerAllCos(con);
            //listOcc = OccDao.obtenerAllOcc(con);
            listOcc = OccDao.obtenerAllOcc();
            LOG.debug("[cargar]Finalizando correctamente");
        } catch (Exception e) {
            LOG.error("[cargar]:", e);
        }
    }

    public synchronized void reset() {
        //Connection con = null;
        try {
            // con = ConexionBD.getConnection();
            cargarParametros(/*con*/);
            cargar(/*con*/);
            ParametrosDAO.UpdateParamResetConfig(/*con*/);

        } catch (Exception e) {
            LOG.error("[reset]", e);
        }
        /*finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    LOG.warn("[Gestor Configuraciones] Error al cerrar connection", ex);
                }
            }
        }*/

    }

    public static synchronized GestorConfiguraciones getInstance() {
        if (_instance == null) {
            _instance = new GestorConfiguraciones();
        }
        return _instance;
    }

    //---------------parametros desde bd------------------------------------------  
    private void cargarParametros(/*Connection con*/) {
        try {
            LOG.debug("[cargarParametros] Iniciando carga de parametros..");
            List<Parametro> listParam = ParametrosDAO.getListParametros(/*con*/);
            Parameters = new HashMap<>();
            for (Parametro parametro : listParam) {
                Parameters.put(parametro.getNombre(), parametro.getValor());
            }
        } catch (Exception e) {
            LOG.error("[cargarParametros]", e);
        }

    }

    public synchronized String getParametro(String nameParameter) {
        return Parameters.get(nameParameter);
    }

    public Map<String, String> getParameters() {
        return Parameters;
    }

    public void setParameters(Map<String, String> Parameters) {
        this.Parameters = Parameters;
    }

    private List<ComposicionBilletera> obtenerListaBilleterasComp(int configId /*, Connection con*/) {
        //List<Billetera> list=new LinkedList<Billetera>();
        List<ComposicionBilletera> listComp = composicionBilleteraDAO.getComposicionBilleterasByConfigId(configId /*, con*/);

        if (listComp != null) {
            LOG.debug("configId: " + configId + ", listComp.size: " + listComp.size());

            for (ComposicionBilletera composicionBilletera : listComp) {
                List<Billetera> lisBilleteraByComp = billeteraDAO.getBilleterasByComposicionBilleteraId(composicionBilletera.getComposicionBilleteraId() /*, con*/);
                if (lisBilleteraByComp != null) {
                    LOG.debug("ID: " + composicionBilletera.getComposicionBilleteraId() + ", TRUST_listBilleteraByComp: " + lisBilleteraByComp);
                    composicionBilletera.setListBilleteras(lisBilleteraByComp);
                }
            }
        } else {
            LOG.debug("listComp.size: " + listComp);
        }

        return listComp;
    }
    //==========================================================================

    private Config loadConfigPorDefecto(/*Connection con*/) {

        Config myConfig = null;
        LOG.debug("[loadConfigPorDefecto]| Iniciando proceso de armar Configuracion X defecto");
        try {
            String StrIdConfigDefault = getParametro("default_configuracion_id");
            LOG.debug("[loadConfigPorDefecto] default_configuracion_id: " + StrIdConfigDefault);
            int idConfigDefault = Integer.parseInt(StrIdConfigDefault);

            if (idConfigDefault >= 0) {
                myConfig = configDAO.obtenerById(idConfigDefault /*, con*/);
                if (myConfig != null) {
                    //obtener billeteras simples
                    List<Billetera> listaBilleterasSimples = billeteraDAO.getBilleterasByConfigId(myConfig.getConfigId() /*, con*/);
                    //obtener billeteras compuestas---------------------------------
                    List<ComposicionBilletera> listaBilleterasComp = obtenerListaBilleterasComp(myConfig.getConfigId() /*, con*/);
                    //-------------------------------------------------------------- 

                    /*if (listaBilleterasSimples == null) {
                        listaBilleterasSimples = new LinkedList<Billetera>();
                    }
                    if (listaBilleterasComp == null) {
                        listaBilleterasComp = new LinkedList<ComposicionBilletera>();
                    }
                     */
                    myConfig.setListBilleterasSimples(listaBilleterasSimples);
                    myConfig.setListBilleterasCompuestas(listaBilleterasComp);
                } else {
                    LOG.warn("[loadConfigPorDefecto]||ConfigId=" + StrIdConfigDefault + "|No se encontro Config OCC por defecto");
                }
            } else {
                LOG.warn("[loadConfigPorDefecto]||ConfigId=" + StrIdConfigDefault + "|No se Definio Config OCC por defecto");
            }
        } catch (NumberFormatException e) {
            LOG.error("[loadConfigPorDefecto] al intentar recuperar Config OCC, :" + e.getMessage(), e);
        }
        return myConfig;
    }
    //==========================================================================

    public Config obtenerConfigByOrigenCortoCos(String nameCos, String canal, String corto) {
        Config config = null;
        try {
            LOG.debug("[obtenerConfigByOrigenCortoCos]Iniando proceso");
            int idConfig = getConfigId(canal, corto, nameCos);
            LOG.debug("[obtenerConfigByOrigenCortoCos] ID CONFIG: " + idConfig);
            if (idConfig > 0) {
                config = obtenerConfigById(idConfig);
            }
        } catch (Exception e) {
            LOG.error("[obtenerConfigByOrigenCortoCos]", e);
        }
        return config;
    }
    //==========================================================================

    public Config getMyConfigDefault() {
        return myConfigDefault;
    }

    public void setMyConfigDefault(Config myConfigDefault) {
        this.myConfigDefault = myConfigDefault;
    }

    private int getConfigId(String canal, String corto, String nameCos) {
        int resp = -1;
        LOG.debug("*** GetConfigId|Canal: " + canal + ", Corto:" + corto + ", nameCos:" + nameCos);
        try {
            if (listOcc != null) {
                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals(nameCos)) {
                        //WEB	123	FF
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: WEB	123 FF , DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals("*")) {
                        //WEB	123	*
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: WEB	 123  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals(nameCos)) {
                        //WEB	*	FF
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: WEB	 * FF, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals(nameCos)) {
                        //*	123	FF
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: *	123  FF, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals(canal) && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals("*")) {
                        //WEB	*	*
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: WEB	 *  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals(corto) && occ.getNombreCos().equals("*")) {
                        //*	123	*
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: *	123  *, DI:" + resp);
                        return resp;
                    }
                }

                for (Occ occ : listOcc) {
                    if (occ.getNombreOrigen().toUpperCase().equals("*") && occ.getNombreCorto().equals("*") && occ.getNombreCos().equals(nameCos)) {
                        //*	*	FF
                        resp = occ.getConfigId();
                        LOG.debug(" Conbinacion encontrada: *	*  FF, DI:" + resp);
                        return resp;
                    }
                }
                LOG.debug("No se encontro Ninguna Conbinacion....");
            }

        } catch (Exception e) {
            LOG.error("[getConfigId]", e);
        }

        return resp;
    }
    //---------------------------------------------

    private Config obtenerConfigById(int idConfig) {
        LOG.debug("[obtenerConfigById] Iniando proceso, idConfig: " + idConfig);

        Config resp = null;
        try {
            if (listConfiguraciones != null) {
                for (Config config : listConfiguraciones) {
                    if (config.getConfigId() == idConfig) {
                        return config;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("[obtenerConfigById]", e);
            return resp;
        }
        return resp;
    }
    //---------------------------------------------
}
