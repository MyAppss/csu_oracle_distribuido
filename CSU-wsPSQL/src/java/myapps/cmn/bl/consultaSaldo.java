/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Parametro;
import myapps.cmn.vo.ComposicionBilletera;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.BilleteraResponse;
import myapps.cmn.vo.Config;
import myapps.cmn.vo.Billetera;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.WalletComverse;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import myapps.cmn.dao.ParametrosDAO;
import myapps.cmn.dao.reporteConsultaDAO;
import myapps.cmn.util.AcumuladoMega;
import myapps.cmn.util.ErrorCodes;
import myapps.cmn.util.MethodsCBS;
import myapps.cmn.util.MethodsCCWS;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilDate;
import myapps.cmn.util.UtilMontoMinimo;
import myapps.cmn.util.UtilOperaciones;
import myapps.cmn.vo.ReporteConsulta;
import myapps.csu.gestorConfig.GestorConfiguraciones;
import myapps.dao.MapeoOfertasSingleton;
import myapps.main.ClienteSpring;
import myapps.utils.PropiedadesGQL;

import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class consultaSaldo {

    private static final Logger LOG = Logger.getLogger(consultaSaldo.class);
    /*
     * Método principal para recuperar saldo
     */
    private String ISDN;
    private Map<String, String> ParametersBD;
    private int ErrorCode;
    private String ErrorDescripcion;
    private GestorConfiguraciones GestorConfig;
    //==========================================================================    
    private String saludoInicial;
    private List<BilleteraResponse> listBilleteras;
    private String publicidad;

    //================= Unidades de Navegacion =======================
    private List<String> listWalletSimpleUnidNav;
    private List<String> listWalletCompUnidNav;
    private static PropiedadesGQL instance;

    public String getConsultaSaldo(String msisdn, int longitudMax, boolean swPublicidad, String canal, String corto, String session, String logPrefijo) {
        try {

            ISDN = msisdn;
            Calendar timeInicio = Calendar.getInstance();
            LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "|long=" + longitudMax + "|swPublicidad=" + swPublicidad + "|Iniciando proceso, canal: " + canal + ", corto: " + corto);
            GestorConfig = GestorConfiguraciones.getInstance();
            //verificar SI cambio, entonces actualizar datos del Gestor
            /* if (isChangeConfiguraciones()) {
                LOG.info("[getConsultaSaldo]ISDN=" + ISDN + "|Las configuraciones han cambiado, se Recargaran nuevamente");
                GestorConfig.reset();
            }*/

            cargarParametros();
            ErrorCode = 0;
            ErrorDescripcion = "correcto";
            listBilleteras = new ArrayList<BilleteraResponse>();

            String msgSaldo = "";
            String sw_sistema_enable = getParametro("sistema_consulta_saldo_sw_enable");
            //String str_sw_
            if (sw_sistema_enable.equals("true")) {//sistema esta ENABLE
                if (swPublicidad) {//devolver con publicidad
                    LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "|Iniciando proceso de consulta Definido CON publicidad");
                    msgSaldo = generarConsultaSaldo(msisdn, longitudMax, canal, corto, session, logPrefijo);
                    int longitudSaltoLinea = 1;
                    int longitudRestante = longitudMax - msgSaldo.length() - longitudSaltoLinea;
                    String Str_sw_publicidad_habilitada = getParametro("publicidad_sw_habilitado");

                    if (Str_sw_publicidad_habilitada.equals("true")) {
                        String publicidadMsg = generarMsgPublicidad(msisdn, longitudRestante);
                        publicidad = publicidadMsg;
                        if (publicidadMsg != null) {
                            if (!publicidadMsg.isEmpty()) {
                                msgSaldo = msgSaldo + "\n" + publicidadMsg;
                            }
                        }
                    } else {
                        LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "|Peticion Definida con Publicidad. Pero NO se agrego publicidad por q esta deshabilita en Sistema");
                    }
                    //genera publicidad
                } else {//sin publicidad
                    LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "|Iniciando proceso de consulta Definido SIN PUBLICIDAD");
                    msgSaldo = generarConsultaSaldo(msisdn, longitudMax, canal, corto, session, logPrefijo);
                }
            } else {
                String texto_fuera_servicio = getParametro("default_respuesta_fuera_servicio");
                texto_fuera_servicio = texto_fuera_servicio.replace("%TELEFONO%", msisdn);
                LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "| Sistema esta en estado DESHABILITADO. Devolviendo respuesta(por defecto):|" + texto_fuera_servicio + "|");
                msgSaldo = texto_fuera_servicio;
                saludoInicial = msgSaldo;
            }
            //-------calcular tiempo que tarda en generar la consulta-----------            
            Calendar timeFinal = Calendar.getInstance();
            // conseguir la representacion de la fecha en milisegundos
            long milis1 = timeInicio.getTimeInMillis();
            long milis2 = timeFinal.getTimeInMillis();
            // calcular la diferencia en milisengundos
            long diff = milis2 - milis1;
            // calcular la diferencia en segundos
            //diffSeconds = diff / 1000;
            LOG.debug(logPrefijo + "[getConsultaSaldo]ISDN=" + ISDN + "|Resp Generada en " + diff + " MiliSegundos|Retornando Consulta=" + msgSaldo + "|");

            //------------------------------------------------------------------
            return msgSaldo;

        } catch (Exception e) {
            LOG.error(logPrefijo + "[getConsultaSaldo] Al intentar generar consulta: " + e.getMessage(), e);
            return "";
        }

    }

    //==========================================================================
    /*
     * Método que devuelve un mensaje saldo sin publicidad
     */
    private String generarConsultaSaldo(String msisdn, int longitudMax, String canal, String corto, String session, String logPrefijo) {
        StringBuilder Mensaje = new StringBuilder();
        LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "| Iniciando consulta Sin publicidad. canal: " + canal + ", corto: " + corto);
        boolean swGeneroMsg = false;
        try {
            ResponseCCWS resCMV = ObtenerData(msisdn, session, logPrefijo);
            //log.debug("TSB OBT DATA: " + (System.currentTimeMillis() - z));

            LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "| resCMV: " + resCMV);
            if (resCMV != null && resCMV.getListWallet() != null && resCMV.getListOffer() != null && resCMV.getListAcumulador() != null) {
                // if (resCMV != null) {

                //-------------si subscriber not found saltar todo--------------
                if (resCMV.getErrorDescription().equals("Subscriber not found")) {
                    String MsgUsuarioNoComverse = getParametro("default_respuesta_usuario_no_comverse");
                    LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|SUBSCRIBER NOT FOUND| devolviento mensaje:|" + MsgUsuarioNoComverse + "|");
                    ErrorCode = ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE;
                    ErrorDescripcion = ErrorCodes.MSG_ERROR_USUARIO_NO_COMVERSE;
                    MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                    saludoInicial = MsgUsuarioNoComverse;
                    Mensaje.append(MsgUsuarioNoComverse);
                    return Mensaje.toString();
                }
                //--------------------------------------------------------------
                List<WalletComverse> listaBilleterasCMV = resCMV.getListWallet();
                List<Offer> listOferta = resCMV.getListOffer();
                List<Acumulador> listAcumulador = resCMV.getListAcumulador();
                Config currentConfig = obtenerConfiguracionForOrigenCortoCos(msisdn, resCMV.getNameCOS(), canal, corto);
                LOG.debug(logPrefijo + "oferList: " + listOferta.size() + ", acumList: " + listAcumulador.size());
                //-----------------------------------------------------------------
                if (currentConfig != null) {
                    if (resCMV.getErrorDescription().equals("Subscriber not found")) {
                        String MsgUsuarioNoComverse = getParametro("default_respuesta_usuario_no_comverse");
                        swGeneroMsg = true;
                        LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|SUBSCRIBER NOT FOUND| devolviento mensaje:|" + MsgUsuarioNoComverse + "|");
                        ErrorCode = ErrorCodes.CODE_ERROR_USUARIO_NO_COMVERSE;
                        ErrorDescripcion = ErrorCodes.MSG_ERROR_USUARIO_NO_COMVERSE;
                        MsgUsuarioNoComverse = MsgUsuarioNoComverse.replace("%TELEFONO%", msisdn);
                        saludoInicial = MsgUsuarioNoComverse;
                        Mensaje.append(MsgUsuarioNoComverse);
                    } else {
                        String saludoInic = generarSaludoInicial(currentConfig, msisdn);
                        saludoInicial = saludoInic;
                        Mensaje.append(saludoInic);

                        //================================================================================================
                        //*********************************** BILLETERAS SIMPLES *****************************************
                        //================================================================================================
                        /*List<WalletComverse> aaa = listaBilleterasCMV;
                        for (int i = 0; i < aaa.size(); i++) {
                            WalletComverse wal = aaa.get(i);
                            System.out.println("Billetera: " + wal.getNameWallet() + " Expiracion " + UtilDate.dateToTimeString(wal.getExpiration()));
                        }
                        System.out.println("=========================================");
                         */
                        String msgBilleterasSimples = obtenerMsgBilleterasSimples(currentConfig, listaBilleterasCMV, listOferta, listAcumulador, longitudMax, Mensaje.toString().length());
                        LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|Recuperada msg BilleterasSimples=" + msgBilleterasSimples + "|");
                        Mensaje.append(msgBilleterasSimples);

                        //Si no se ha sobrepasado la longitud max ,adicionar msg para Billeteras Compuestas 
                        String msgBilleteraComp = "";
                        if (Mensaje.length() < longitudMax) {
                            //================================================================================================
                            //*********************************** BILLETERAS COMPUESTAS *****************************************
                            //================================================================================================
                            /*List<WalletComverse> aa = listaBilleterasCMV;
                            for (int i = 0; i < aa.size(); i++) {
                                WalletComverse wal = aa.get(i);
                                System.out.println("Billetera: " + wal.getNameWallet() + " Expiracion " + UtilDate.dateToTimeString(wal.getExpiration()));
                            }
                             */

                            msgBilleteraComp = obtenerMsgBilleterasCompuestas(currentConfig, listaBilleterasCMV, listOferta, listAcumulador, longitudMax, Mensaje.toString().length());
                            LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|Recuperada msg BilleterasComp=" + msgBilleteraComp + "|");
                            Mensaje.append(msgBilleteraComp);
                        }
                        //==========================================================
                        //Si se configuro para Mostrar billeteras Nuevas con saldo >0
                        if (currentConfig.getMostraBilleterasNoConfig().equals("t")) {
                            LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|Configuracion definida Para mostrar Billeteras NUevas con saldo > 0");
                            String msgNuevasBilleteras = "";
                            if (Mensaje.length() < longitudMax) {
                                msgNuevasBilleteras = obtenerMsgBilleterasNuevas(currentConfig, listaBilleterasCMV, longitudMax, Mensaje.toString().length());
                                LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|se genero Mensaje Billeteras Nuevas(NO configuradas)=" + msgNuevasBilleteras + "|Se adiciona al texto respuesta");
                                Mensaje.append(msgNuevasBilleteras);

                            } else {
                                LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|No se adiciono mensaje Billeteras Nuevas, NO existen caracteres libres");
                            }
                        }
                        //==========================================================
                        if (!msgBilleterasSimples.isEmpty() || !msgBilleteraComp.isEmpty()) {
                            swGeneroMsg = true;
                        }
                    }
                } else {
                    LOG.debug(logPrefijo + "[generarConsultaSaldo]|ISDN=" + msisdn + "|No se encontro una configuracion valida para el COS de este usuario");
                    String MsgFueraServicio = getParametro("default_respuesta_fuera_servicio");
                    MsgFueraServicio = MsgFueraServicio.replace("%TELEFONO%", msisdn);
                    Mensaje.append(MsgFueraServicio);
                }
            } else {
                //swGeneroMsg = true;
                swGeneroMsg = false;
            }

        } catch (Exception e) {
            LOG.error(logPrefijo + "[getConsultaSaldoSinPublicidad]ISDN=" + msisdn + "| Error al intentar generar la consulta de saldo :" + e.getMessage(), e);
        }
        //veriricar si
        if (!swGeneroMsg) {
            //Aqui entra si el mensaje q se genero esta vacio
            //sino genero un mensaje de Billeteras defininir q se debe devolver;            
            String msgFueraServicio = getParametro("default_respuesta_fuera_servicio");
            msgFueraServicio = msgFueraServicio.replace("%TELEFONO%", msisdn);
            saludoInicial = msgFueraServicio;
            return msgFueraServicio;
        } else {
            return Mensaje.toString();
        }
    }
    //--------------------------------------------------------------------------

    private String generarMsgPublicidad(String isdn, int length) {
        String res = "";
        try {
            MessageConstructor msc = new MessageConstructor();
            res = msc.getMessagePublicidad(Integer.parseInt(isdn), length);
        } catch (Exception e) {
            LOG.error("[generarMsgPublicidad] error:" + e.getMessage(), e);
        }
        return res;
    }

    //--------------------------------------------------------------------------
    /*
     * Devuelve la configuracion al q pertenece el usuario si no existe el cos
     * configurado, devuelve una configuracion por defecto
     */
    private Config obtenerConfiguracionForOrigenCortoCos(String isdn, String nameCos, String canal, String corto) {
        LOG.debug("[obtenerConfiguracionForOrigenCortoCos]|ISDN=" + isdn + "|nameCanal=" + canal + "|nameCorto=" + corto + "|nameCOS=" + nameCos + " inicio metodo para obtener Configuracion COS");
        Config myConfig = null;
        try {
            String strNameCos = nameCos;
            LOG.info("[obtenerConfiguracionCOS]|ISDN=" + isdn + "| Recuperado Nombre COS de CMV =" + strNameCos);
            if (!strNameCos.equals("ERROR")) {
                myConfig = getConfigByOrigenCortoCos(nameCos, canal, corto);
            }
            if (myConfig == null) {
                LOG.debug("[obtenerConfiguracionForOrigenCortoCos]|ISDN=" + isdn + "|NO se encontro Configuracion definida para |nameCanal=" + canal + "|nameCorto=" + corto + "|nameCos=" + strNameCos + ", se cargara Configuracion por Defecto");
                myConfig = getConfigPorDefecto();
            }
        } catch (Exception e) {
            LOG.error("[obtenerConfiguracionForOrigenCortoCos]|ISDN=" + ISDN + "| Al intentar obtener una configuracion OCC:" + e.getMessage(), e);
        }
        return myConfig;
    }

    //--------------------------------------------------------------------------
    private Config getConfigByOrigenCortoCos(String nameCos, String canal, String corto) {
        try {
            LOG.debug("[getConfigByOrigenCortoCos]Iniciando proceso para obtener Config");
            return GestorConfig.obtenerConfigByOrigenCortoCos(nameCos, canal, corto);
        } catch (Exception e) {
            LOG.error("[getConfigByOrigenCortoCos]", e);

        }
        return null;
    }
    //--------------------------------------------------------------------------

    /*
     * Devuelve un cos por defecto cargado desde la bd con las opciones por
     * defecto : Saludo inicial configurado para COS q no existan en el sistem
     * se cargaran todas las billeteras base , con las opciones de ver vigencia,
     * cant decimales
     */
    private Config getConfigPorDefecto() {
        Config myConfig = null;
        LOG.debug("[getConfigPorDefecto]ISDN=" + ISDN + "| Iniciando proceso de armar Configuracion por DEFECTO");
        try {
            myConfig = GestorConfig.getMyConfigDefault();
            if (myConfig != null) {
                LOG.debug("[getConfigPorDefecto]ISDN=" + ISDN + "|Recuperada Config=" + myConfig.getNombre());
            }
        } catch (Exception e) {
            LOG.error("[getConfigPorDefecto]ISDN=" + ISDN + "| al intentar recuperar config OCC,  :" + e.getMessage(), e);
        }
        return myConfig;
    }

    private void cargarParametrosGQL() {
        if (instance == null) {
            instance = new PropiedadesGQL();
            LOG.debug("...::: CARGANDO PROPIEDADES PARA EL CONSUMO DE GQL :::...");
            PropiedadesGQL.setGqlTIMEOUT(Propiedades.GQL_TIMEOUT);
            PropiedadesGQL.setGqlDATEFORMAT(Propiedades.GQL_DATEFORMAT);
            PropiedadesGQL.setGqlENDPOINT(Propiedades.GQL_ENDPOINT);
            PropiedadesGQL.setGqlMESSAGE(Propiedades.GQL_MESSAGE);
            PropiedadesGQL.setGqlFECHAMINIMA(Propiedades.GQL_FECHAMINIMA);
            String url = "jdbc:oracle:thin:@" + Propiedades.BD_SERVER + ":" + Propiedades.BD_PORT + "/" + Propiedades.BD_NAME;

            PropiedadesGQL.setUrlBD(url);
            PropiedadesGQL.setUsuarioBD(Propiedades.DB_USER);
            PropiedadesGQL.setPasswordBD(Propiedades.DB_PASSWORD);
            PropiedadesGQL.setSetminevictableidletimemillisBD(Integer.parseInt(Propiedades.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
            PropiedadesGQL.setSetmaxactiveBD(Integer.parseInt(Propiedades.DB_MAX_ACTIVE));
            PropiedadesGQL.setSetminidleBD(Integer.parseInt(Propiedades.DB_MIN_IDLE));
            PropiedadesGQL.setSetMaxIdleBD(Integer.parseInt(Propiedades.DB_MAX_IDLE));
            PropiedadesGQL.setSetMaxWaitBD(Integer.parseInt(Propiedades.DB_MAX_WAIT));

            PropiedadesGQL.setGqlBOB(Propiedades.GQL_BOB);
            PropiedadesGQL.setGqlKB(Propiedades.GQL_KB);
            PropiedadesGQL.setPrecioKB(Propiedades.GQL_PrecioKB);
            PropiedadesGQL.setGqlItem(Propiedades.GQL_Item);
            PropiedadesGQL.setGqlDecimales(Propiedades.GQL_Decimales);
            PropiedadesGQL.setGqlNofound(Propiedades.GQL_NoFound);
            PropiedadesGQL.setGqlChilProdutActivo(Propiedades.GQL_CPACTIVO);
            PropiedadesGQL.setGqlChilProdutSuspendido(Propiedades.GQL_CPSUSPENDIDO);
            PropiedadesGQL.setGqlTYPE(Propiedades.GQL_TYPE);
            PropiedadesGQL.setImprimirBilleteras(Propiedades.GQL_IMPRIMIR_BILLETERAS);
            PropiedadesGQL.setNombreReseva(Propiedades.GQL_NOMBRE_RESERVA);
            PropiedadesGQL.setFechaImprimirBIlleteras(Propiedades.GQL_FORMATOFECHA_IB);
            MapeoOfertasSingleton.getInstance();
            LOG.debug("...::: FIN DE LA CARGA DE PROPIEDADES PARA EL CONSUMO DE GQL :::...");
        }
    }

    private ResponseCCWS ObtenerData(String isdn, String session, String logPrefijo) {
        ResponseCCWS responseCCws = null;
        try {
            cargarParametrosGQL();
            if (Propiedades.GQL_BANDERA) {
                LOG.debug(logPrefijo + "....::: INICIANDO OBTENER BILLETERAS DEL SERVICIO DE GQL  ISDN: " + isdn + " :::...");

                ClienteSpring clienteSpring = new ClienteSpring();
                responseCCws = (ResponseCCWS) clienteSpring.obtenerResponseCCWS("591" + isdn, "");

                long tInicio = System.currentTimeMillis();
                LOG.debug(logPrefijo + "TIEMPO DE OBTENCION DE BILLETERAS DEL SERVICIO GQL" + (System.currentTimeMillis() - tInicio));
            } else {
                responseCCws = getDataCOMVERSE(isdn, logPrefijo);
            }

            if (Boolean.TRUE.equals(Propiedades.VM_BANDERA) && responseCCws != null && responseCCws.getListWallet() != null) {
                long tInicio = System.currentTimeMillis();
                MethodsCBS cbs = new MethodsCBS();
                Calendar fecha = cbs.obtenerFechaCbs(logPrefijo, isdn, session);

                if (fecha != null) {
                    LOG.debug("Fecha de Expiracion Obtenida de CBS: " + fecha);
                    for (WalletComverse billetera : responseCCws.getListWallet()) {
                        if (cbs.getBilleterasAfectadas().contains(billetera.getNameWallet())) {
                            LOG.debug(logPrefijo + "Billetera: " + billetera.toString() + " afectada por fecha cbs: " + fecha.toString());
                            billetera.setExpiration(fecha);
                        }
                    }
                } else {
                    LOG.warn(logPrefijo + "ERROR AL OBTENER LA FECHA DE CBS null");
                }
                LOG.debug(logPrefijo + " Tiempo total cambios Vigencia Maxima :" + (System.currentTimeMillis() - tInicio) + " ms.");
            }
        } catch (Exception e) {
            LOG.error(logPrefijo + "[obtenerData] al intentar recuperar dat:" + e.getMessage(), e);

            String error = e.getMessage();
            if (error != null) {
                responseCCws = new ResponseCCWS();
                responseCCws.setNameCOS("ERROR");
                if (error.contains("Could not send Message")) {
                    responseCCws.setErrorDescription("Read timed out");
                    responseCCws.setListWallet(null);
                } else if (error.contains("Read timed out")) {
                    responseCCws.setErrorDescription("ConnectionErrorTimeOut");
                    responseCCws.setListWallet(null);
                } else if (error.contains("does not exist") || error.contains("Invalid QName in mapping: ns0:Client")) {
                    responseCCws.setErrorDescription("Subscriber not found");
                    responseCCws.setListWallet(null);
                } else {
                    responseCCws.setErrorDescription("ConnectionError");
                    responseCCws.setListWallet(null);
                }
            } else {
                if (responseCCws == null) {
                    responseCCws = new ResponseCCWS();
                } else {
                    responseCCws.setErrorDescription("ConnectionError");
                    responseCCws.setListWallet(null);
                }
            }
        }

        return responseCCws;

    }

    private ResponseCCWS getDataCOMVERSE(String isdn, String logPrefijo) {
        LOG.debug(logPrefijo + "[getDataCOMVERSE]Iniciando..");
        try {
            ResponseCCWS RCMV = null;
            if (Propiedades.CCWS_MODO_PRUEBA.equals("1")) {
                RCMV = MethodsCCWS.getListBalanceWalletSimular(isdn);
            } else {
                RCMV = MethodsCCWS.getListBalanceWallet(isdn, logPrefijo);
            }
            return RCMV;
        } catch (Exception e) {
            LOG.error(logPrefijo + "[getDataCOMVERSE] al intentar recuperar data de CMV:" + e.getMessage(), e);
        }
        return null;
    }

    private String generarSaludoInicial(Config myConfig, String isdn) {

        LOG.debug("[generarSaludoInicial]|ISDN=" + ISDN + "|COS=" + myConfig.getNombre() + "|Recuperando saludo Inicial..");
        String str = myConfig.getSaludoInicial();
        //se debe armar el saludos
        str = str.replace("%TELEFONO%", isdn);
        LOG.debug("[generarSaludoInicial]|ISDN=" + ISDN + "|COS=" + myConfig.getNombre() + "|Devolviendo saludoInicial=|" + str + "|");
        return str;
    }

    /*
     * Devuelve un mensaje para las billeteras simples
     */
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    private String obtenerMsgBilleterasSimples(Config myConfig, List<WalletComverse> listWalletCMV, List<Offer> listOferta, List<Acumulador> listAcumulador, int lengthMax, int longitudUsada) {
        StringBuilder msgBilleteras = new StringBuilder();
        LOG.debug("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Iniciando proceso para generar MsgBilleteras");
        //sacar mensaje para cada billetera configurada

        try {
            List<Billetera> listaConfigBilleteraSimple = myConfig.getListBilleterasSimples();
            if (listaConfigBilleteraSimple != null) {
                LOG.debug("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Lista de Billeteras simples Size=" + listaConfigBilleteraSimple.size());
                for (Billetera billetera : listaConfigBilleteraSimple) {

                    String msgBilleteraCurrent = getMensajeBilletera(billetera, listWalletCMV, listOferta, listAcumulador);
                    LOG.debug("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Billetera=" + billetera.getNombreComverse() + "| mensaje Generado=" + msgBilleteraCurrent + "|");
                    if (!msgBilleteraCurrent.isEmpty()) {
                        msgBilleteraCurrent = "\n" + msgBilleteraCurrent;
                        int longitudDisponible = lengthMax - longitudUsada - msgBilleteras.toString().length();
                        if (msgBilleteraCurrent.length() <= longitudDisponible) {
                            msgBilleteras.append(msgBilleteraCurrent);
                        }
                    }
                }
            } else {
                LOG.debug("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Config=" + myConfig.getNombre() + "|Esta Configuracion NO tiene billeteras simples,por tanto NO se genero msg billeteras simples.");
            }
            //Adicionar la Billetera DPI=======================================
            //SI esta configurada para mostrar
//            if (myConfig.getMostrarDpi().equals("t")) {
//                String msgBilleteraDPI = obtenerMsgDPI(ISDN);
//                LOG.info("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Billetera=DPI| mensaje Generado=" + msgBilleteraDPI + "|");
//                if (!msgBilleteraDPI.isEmpty()) {
//                    msgBilleteraDPI = "\n" + msgBilleteraDPI;
//                    int longitudDisponible = lengthMax - longitudUsada - msgBilleteras.toString().length();
//                    if (msgBilleteraDPI.length() <= longitudDisponible) {
//                        msgBilleteras.append(msgBilleteraDPI);
//                    }
//                } else {
//                    LOG.info("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Billetera=DPI| mensaje Generado=" + msgBilleteraDPI + "|Mensaje vacio, NO se adiciona ningun caracter");
//                }
//            }
//
//            if (myConfig.getMostrarAcumuladosMegas().equals("t")) {
//
//                String msgAcumuladosMegas = obtenerMsgAcumuladosMegas(ISDN, myConfig.getMostrarDpi(), myConfig.getMostrarAcumuladosMegas());
//                LOG.info("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Billetera=Acumulados Megas| mensaje Generado=" + msgAcumuladosMegas + "|");
//                if (!msgAcumuladosMegas.isEmpty()) {
//                    msgAcumuladosMegas = "\n" + msgAcumuladosMegas;
//                    int longitudDisponible = lengthMax - longitudUsada - msgBilleteras.toString().length();
//                    if (msgAcumuladosMegas.length() <= longitudDisponible) {
//                        msgBilleteras.append(msgAcumuladosMegas);
//                    }
//                } else {
//                    LOG.info("[obtenerMensajeBilleterasSimples]ISDN" + ISDN + "|Billetera=Acumulados Megas| mensaje Generado=" + msgAcumuladosMegas + "|Mensaje vacio, NO se adiciona ningun caracter");
//                }
//            }
            //==================================================================

        } catch (Exception e) {
            LOG.error("[obtenerMensajeBilleteraSimples]Error al intentar obtenerMensajeBilletera.", e);
        }

        return msgBilleteras.toString();
    }

    /*
     * Devuelve un mensaje para las billeteras Compuestas
     */
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    private String obtenerMsgBilleterasCompuestas(Config myCos, List<WalletComverse> listWalletCMV, List<Offer> listOferta, List<Acumulador> listAcumulador, int lengthMax, int longitudUsada) {
        StringBuilder msgBilleteras = new StringBuilder();
        LOG.debug("[obtenerMsgBilleterasCompuestas]ISDN" + ISDN + "|Iniciando proceso para generar MsgBilleteras");

        try {
            List<ComposicionBilletera> listaCompBilletera = myCos.getListBilleterasCompuestas();
            if (listaCompBilletera != null) {
                LOG.debug("[obtenerMensajeBilleterasCompuestas]ISDN" + ISDN + "|Lista de Billeteras Compuestas Size=" + listaCompBilletera.size());
                //sacar mensaje para cada billetera Compuesta configurada
                for (ComposicionBilletera composicionBilletera : listaCompBilletera) {
                    String msgCompBilleteraCurrent = getMensajeBilleteraComp(composicionBilletera, listWalletCMV, listOferta, listAcumulador);
                    LOG.debug("[obtenerMsgBilleterasCompuestas]ISDN" + ISDN + "|Billetera=" + composicionBilletera.getNombre() + "| mensaje Generado=" + msgCompBilleteraCurrent + "|");
                    if (!msgCompBilleteraCurrent.isEmpty()) {
                        msgCompBilleteraCurrent = "\n" + msgCompBilleteraCurrent;
                        int longitudDisponible = lengthMax - longitudUsada - msgBilleteras.toString().length();
                        if (msgCompBilleteraCurrent.length() <= longitudDisponible) {
                            msgBilleteras.append(msgCompBilleteraCurrent);
                        }
                    }
                }
            } else {
                LOG.debug("[obtenerMensajeBilleterasCompuestas]ISDN" + ISDN + "|La configuracion no contiene billeterasCompuestas.No se genero mensaje Para billeteras compuestas");
            }
        } catch (Exception e) {
            LOG.error("[obtenerMsgBilleterasCompuestas]" + e.getMessage(), e);
        }

        return msgBilleteras.toString();
    }
    //--------------------------------------------------------------------------

    private String obtenerMsgBilleterasNuevas(Config myConfig, List<WalletComverse> listWalletCMV, int lengthMax, int longitudUsada) {
        StringBuilder msgBilleteras = new StringBuilder();
        LOG.debug("[obtenerMensajeBilleterasNuevas]ISDN" + ISDN + "|Iniciando proceso para generar MsgBilleteras Nuevas");
        //sacar mensaje para cada billetera configurada        
        try {
            List<WalletComverse> listaConfigBilleteraNoUsadas = getListaWalletNoUsadas(listWalletCMV);

            LOG.debug("[obtenerMsgBilleterasNuevas]ISDN" + ISDN + "|Lista de Billeteras NUEVAS recuperadas Size=" + listaConfigBilleteraNoUsadas.size());

            for (WalletComverse wc : listaConfigBilleteraNoUsadas) {
                if (wc != null) {
                    String msgBilleteraCurrent = getMensajeBilleteraNueva(wc);
                    LOG.debug("[obtenerMsgBilleterasNuevas]ISDN" + ISDN + "|Billetera=" + wc.getNameWallet() + "| mensaje Generado=" + msgBilleteraCurrent + "|");
                    if (!msgBilleteraCurrent.isEmpty()) {
                        msgBilleteraCurrent = "\n" + msgBilleteraCurrent;
                        int longitudDisponible = lengthMax - longitudUsada - msgBilleteras.toString().length();
                        if (msgBilleteraCurrent.length() <= longitudDisponible) {
                            LOG.debug("[obtenerMsgBilleterasNuevas]ISDN" + ISDN + "|Longitud Disponible=" + longitudDisponible + "|longitud Mensaje Generado=" + msgBilleteraCurrent.length() + "|Adicionada Texto");
                            msgBilleteras.append(msgBilleteraCurrent);
                        } else {
                            LOG.debug("[obtenerMsgBilleterasNuevas]ISDN" + ISDN + "|Longitud Disponible=" + longitudDisponible + "|longitud Mensaje Generado=" + msgBilleteraCurrent.length() + "|Texto Descartado");
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("[obtenerMsgBilleterasNuevas]ISDN=" + ISDN + "|" + e.getMessage(), e);
        }
        return msgBilleteras.toString();
    }
    //------------------------------------------------------------------------

    private List<WalletComverse> getListaWalletNoUsadas(List<WalletComverse> list) {
        List<WalletComverse> listaNoUsadas = new LinkedList<WalletComverse>();
        try {
            if (list != null) {
                for (WalletComverse walletComverse : list) {
                    LOG.debug("_______________________ billetera: " + walletComverse.getNameWallet() + ", valor: " + walletComverse.getAvailableBalance() + ", usada: " + walletComverse.isUsada());
                    if (!walletComverse.isUsada()) {
                        listaNoUsadas.add(walletComverse);
                    }
                }
            }
            return listaNoUsadas;

        } catch (Exception e) {
            LOG.error("[getListaWalletNoUsadas]:", e);
        }
        return listaNoUsadas;
    }
    //-------------------------------------------------------------------------

    /* private String obtenerMsgDPI(String isdn) {
        String msgResponse = "";
        try {
            String nameComercial = getParametro("dpi_nombre_comercial");
            String dpi_operador = getParametro("dpi_operador");
            String dpi_prefijo_unidad = getParametro("dpi_prefijo_unidad");
            String dpi_sw_mostrar_vigencia = getParametro("dpi_sw_mostrar_vigencia");
            String dpi_sw_mostrar_si_saldo_mayor_cero = getParametro("dpi_sw_mostrar_si_saldo_mayor_cero");

            int dpi_cantDecimal = Integer.valueOf(getParametro("dpi_cantidad_decimal"));
            double dpi_value = Double.valueOf(getParametro("dpi_valor_operacion"));
            double dpi_monto_min = Double.valueOf(getParametro("dpi_monto_minimo"));
            double dpi_monto_max = Double.valueOf(getParametro("dpi_monto_maximo"));
            boolean swMostrarVigencia = Boolean.parseBoolean(dpi_sw_mostrar_vigencia);
            boolean swMostrarSoloSiMayorCero = Boolean.parseBoolean(dpi_sw_mostrar_si_saldo_mayor_cero);
            LOG.debug("[obtenerMsgDPI]ISDN+=" + isdn + "|Parametros cargados correctamente");
            //dpi_operadordpi_prefijo_unidad
            String msisdn = "591" + isdn;
            String urlCamiant = getParametro("dpi_direccion_ip");
            //Salgo recuperado en MEGAS
            double SaldoDPI_MB = ConsultingDPI.ConsultBalanceDPI(msisdn, urlCamiant);
            //  Mensaje = billetera.getConfigBilletera_nombreComercial() + ":" + valueStr;
            Billetera billetera = new Billetera();
            billetera.setValor(dpi_value);
            billetera.setOperador(dpi_operador);
            billetera.setMontoMinimo(dpi_monto_min);
            billetera.setMontoMaximo(dpi_monto_max);
            billetera.setCantidadDecimales(dpi_cantDecimal);
            billetera.setPrefijoUnidad(dpi_prefijo_unidad);
            billetera.setConfigBilletera_nombreComercial(nameComercial);
            //billetera.setConfigBilletera_mostrar_saldo_mayor_cero(swMostrarSaldoMayorCero);

            double valueAplicadoOperador = getValueAplicandoOperator(billetera, SaldoDPI_MB);

            //------------------------------------------------------------------
            if ((swMostrarSoloSiMayorCero && (valueAplicadoOperador > 0.0)) || (!swMostrarSoloSiMayorCero)) {

                String valueStr;
                BilleteraResponse br = new BilleteraResponse();
                br.setNombre(billetera.getConfigBilletera_nombreComercial());

                if ((billetera.getMontoMinimo() >= valueAplicadoOperador)) {//si es monto min
                    String strSwParam = getParametro("default_texto_saldo_min_sw_valor");
                    if (strSwParam.equals("true")) {
                        valueStr = formatearValor(0.0, billetera.getCantidadDecimales());
                        br.setValor(valueStr);
                        valueStr = valueStr + " " + billetera.getPrefijoUnidad();
                        br.setTipoUnidad(billetera.getPrefijoUnidad());
                    } else {
                        valueStr = getParametro("default_texto_saldo_min");
                        br.setValor(valueStr);
                    }
                } else if (billetera.getMontoMaximo() <= valueAplicadoOperador) {//si es monto max
                    valueStr = getParametro("default_texto_saldo_max");
                    br.setValor(valueStr);
                } else {
                    valueStr = formatearValor(valueAplicadoOperador, billetera.getCantidadDecimales());
                    br.setValor(valueStr);
                    valueStr = valueStr + " " + billetera.getPrefijoUnidad();
                    br.setTipoUnidad(billetera.getPrefijoUnidad());
                }
                if (swMostrarVigencia) {
                    Calendar dateExpiration = UtilDate.dateToCalendar(ConsultingDPI.getDateExpiration(msisdn, urlCamiant));
                    String strVigencia = getMensageVigencia(dateExpiration);
                    String StrSeparator = getParametro("default_wallet_texto_separador");
                    msgResponse = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStr + " " + strVigencia;

                    String strDate = formatDateWithSimpleDate(dateExpiration);
                    br.setExpiracion1(strDate);

                } else {
                    String StrSeparator = getParametro("default_wallet_texto_separador");
                    msgResponse = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStr;
                }
                listBilleteras.add(br);
            }

            return msgResponse;

        } catch (Exception e) {
            LOG.error("[obtenerMsgDPI] Al intentar generar MsgDPI", e);
        }
        return msgResponse;

    }*/
    private String obtenerMsgAcumuladosMegas(String isdn, String mostrarDpi, String mostrarAcumuladosMegas) {
        String msgResponse = "";
        try {
            String nameComercial = getParametro("ac_megas_nombre_comercial");
            String ac_megas_operador = getParametro("ac_megas_operador");
            String ac_megas_prefijo_unidad = getParametro("ac_megas_prefijo_unidad");
            String ac_megas_sw_mostrar_vigencia = getParametro("ac_megas_sw_mostrar_vigencia");
            String ac_megas_sw_mostrar_si_saldo_mayor_cero = getParametro("ac_megas_sw_mostrar_si_saldo_mayor_cero");

            int ac_megas_cantDecimal = Integer.valueOf(getParametro("ac_megas_cantidad_decimal"));
            double ac_megas_value = Double.valueOf(getParametro("ac_megas_valor_operacion"));
            double ac_megas_monto_min = Double.valueOf(getParametro("ac_megas_monto_minimo"));
            double ac_megas_monto_max = Double.valueOf(getParametro("ac_megas_monto_maximo"));
            boolean swMostrarVigencia = Boolean.parseBoolean(ac_megas_sw_mostrar_vigencia);
            boolean swMostrarSoloSiMayorCero = Boolean.parseBoolean(ac_megas_sw_mostrar_si_saldo_mayor_cero);

            String urlAcumuladosMegas = getParametro("ac_megas_direccion_ip");

            LOG.debug("[obtenerMsgAcumuladosMegas]ISDN+=" + isdn + "|Parametros cargados correctamente");

            AcumuladoMega ac = ConsultingAcumuladosMegas.getAcumuladosMegas(isdn, mostrarDpi, mostrarAcumuladosMegas, urlAcumuladosMegas);

            if (ac != null) {

                Billetera billetera = new Billetera();
                billetera.setValor(ac_megas_value);
                billetera.setOperador(ac_megas_operador);
                billetera.setMontoMinimo(ac_megas_monto_min);
                billetera.setMontoMaximo(ac_megas_monto_max);
                billetera.setCantidadDecimales(ac_megas_cantDecimal);
                billetera.setPrefijoUnidad(ac_megas_prefijo_unidad);
                billetera.setConfigBilletera_nombreComercial(nameComercial);
                //billetera.setConfigBilletera_mostrar_saldo_mayor_cero(swMostrarSaldoMayorCero);

                double valueAplicadoOperador = getValueAplicandoOperator(billetera, ac.getCuota());

                //------------------------------------------------------------------
                if ((swMostrarSoloSiMayorCero && (valueAplicadoOperador > 0.0)) || (!swMostrarSoloSiMayorCero)) {

                    String valueStr;
                    BilleteraResponse br = new BilleteraResponse();
                    br.setNombre(billetera.getConfigBilletera_nombreComercial());

                    if ((billetera.getMontoMinimo() >= valueAplicadoOperador)) {//si es monto min
                        String strSwParam = getParametro("default_texto_saldo_min_sw_valor");
                        if (strSwParam.equals("true")) {
                            valueStr = formatearValor(0.0, billetera.getCantidadDecimales());
                            br.setValor(valueStr);
                            valueStr = valueStr + " " + billetera.getPrefijoUnidad();
                            br.setTipoUnidad(billetera.getPrefijoUnidad());
                        } else {
                            valueStr = getParametro("default_texto_saldo_min");
                            br.setValor(valueStr);
                        }
                    } else if (billetera.getMontoMaximo() <= valueAplicadoOperador) {//si es monto max
                        valueStr = getParametro("default_texto_saldo_max");
                        br.setValor(valueStr);
                    } else {
                        valueStr = formatearValor(valueAplicadoOperador, billetera.getCantidadDecimales());
                        br.setValor(valueStr);
                        valueStr = valueStr + " " + billetera.getPrefijoUnidad();
                        br.setTipoUnidad(billetera.getPrefijoUnidad());
                    }
                    if (swMostrarVigencia) {

                        if (ac.getExpiracion() != null) {
                            Calendar dateExpiration = UtilDate.dateToCalendar(ac.getExpiracion());
                            String strVigencia = getMensageVigencia(dateExpiration);
                            String StrSeparator = getParametro("default_wallet_texto_separador");
                            msgResponse = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStr + " " + strVigencia;

                            String strDate = formatDateWithSimpleDate(dateExpiration);
                            br.setExpiracion1(strDate);
                        } else {
                            LOG.warn("[obtenerMsgAcumuladosMegas] isdn: " + isdn + ", fecha de acumulados megas: " + ac.getExpiracion());
                        }

                    } else {
                        String StrSeparator = getParametro("default_wallet_texto_separador");
                        msgResponse = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStr;
                    }
                    listBilleteras.add(br);
                }
            }

            return msgResponse;

        } catch (Exception e) {
            LOG.error("[obtenerMsgAcumuladosMegas] Al intentar generar Msg Acumulados Megas", e);
        }
        return msgResponse;

    }

    private String getMensajeBilletera(Billetera billetera,
            List<WalletComverse> listWalletCMV, List<Offer> listOferta,
            List<Acumulador> listAcumulador) {

        WalletComverse wcmv = null;
        double availableBalance = 0;
        LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|name billetera: " + billetera.getNombreComverse() + ", alco: " + billetera.isAlco());
        if (billetera.isAlco().equals("t")) {
            Alco:
            for (Offer oferta : listOferta) {
                if (billetera.getNombreComverse().equals(oferta.getName())) {
                    wcmv = new WalletComverse();
                    wcmv.setNameWallet(oferta.getName());
                    wcmv.setExpiration(oferta.getServiceEnd());
                    wcmv.setUsada(true);
                    wcmv.setAvailableBalance(1);

                    for (Acumulador acum : listAcumulador) {
                        LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|" + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                        if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                            double total = billetera.getLimiteAcumulador() - acum.getAmount();
                            LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|total: " + total);
                            if (total < 0) {
                                wcmv.setAvailableBalance(0);
                            } else {
                                wcmv.setAvailableBalance(total);
                            }
                            break Alco;
                        }
                    }
                    oferta.setUsada(true);
                }
            }
        } else {
            wcmv = getWalletCmvAndMarcar(billetera.getNombreComverse(), listWalletCMV);
        }

        String Mensaje = "";
        LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|iniciando..proceso para billetera=" + billetera.getNombreComverse());
        try {

            //Quitar if para desabilitar monto monimo
            if (UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA_BILLETERAS_SIMPLES_MONTO_MINIMO, wcmv, billetera, "")) {

                if (wcmv != null) {

                    availableBalance = wcmv.getAvailableBalance();
                    LOG.debug("STRING: " + wcmv.toString());
                    BilleteraResponse billeteraResponse = new BilleteraResponse();

                    //==========================================================                   
                    boolean swSaldoExpirado = false;
                    Calendar NowCal = Calendar.getInstance();
                    LOG.debug("[getMensajeBilletera]fechaActual=" + NowCal + "|FechaExpiration=" + wcmv.getExpiration());
                    LOG.debug("[getMensajeBilletera]FechaActual anio=" + NowCal.get(Calendar.YEAR) + "|mes=" + NowCal.get(Calendar.MONTH) + "|dia=" + NowCal.get(Calendar.DATE) + "|(" + NowCal.get(Calendar.HOUR_OF_DAY) + ":" + NowCal.get(Calendar.MINUTE) + ":" + NowCal.get(Calendar.SECOND) + ")");
                    Calendar Cal = wcmv.getExpiration();
                    LOG.debug("[getMensajeBilletera]FechaExpiracion anio=" + Cal.get(Calendar.YEAR) + "|mes=" + Cal.get(Calendar.MONTH) + "|dia=" + Cal.get(Calendar.DATE) + "|(" + Cal.get(Calendar.HOUR_OF_DAY) + ":" + Cal.get(Calendar.MINUTE) + ":" + Cal.get(Calendar.SECOND) + ")");
                    //=========validacion fecha indefinida========================
                    String StrFechaExpiration = UtilDate.dateToTimeString(Cal);

                    String vigencia_saldo_vigencia_ilimitada = getParametro("vigencia_saldo_vigencia_ilimitada");
                    LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|Param fecha para Ilimitadas=" + vigencia_saldo_vigencia_ilimitada + "|Fecha Expiration=" + StrFechaExpiration + "|");
                    boolean sw_is_fecha_ilimitada = false;
                    if (vigencia_saldo_vigencia_ilimitada != null
                            && StrFechaExpiration.trim().equals(vigencia_saldo_vigencia_ilimitada.trim())) {
                        LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|Fecha=" + vigencia_saldo_vigencia_ilimitada + "| Es fecha Ilimitada");
                        sw_is_fecha_ilimitada = true;
                    }
                    //=========validacion fecha indefinida========================      

                    if ((!sw_is_fecha_ilimitada) && wcmv.getExpiration().before(NowCal)) {
                        LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|billetera=" + billetera.getNombreComverse() + "|Billetera Expirada, El valor de la billetera se pondra  a cero (0)");
                        //
                        if (!billetera.isMostrarSaldoExpirado().equals("t")) {
                            wcmv.setAvailableBalance(0.0);
                            swSaldoExpirado = true;
                        } else {
                            LOG.debug("MOSTRAR SALDO EXPIRADO, swSaldoExpirado: " + swSaldoExpirado);
                        }

                        if (billetera.isConfigBilletera_no_mostrar_saldo_expirado().equals("t")) {
                            wcmv.setAvailableBalance(0.0);
                            swSaldoExpirado = true;
                        }

                    } else {
                        if (billetera.isMostrarSaldoExpirado().equals("t")) {
                            wcmv.setAvailableBalance(0.0);
                            swSaldoExpirado = true;
                        }
                    }
                    //==========================================================
                    if (billetera.isConfigBilletera_mostrar_siempre().equals("t")
                            || (billetera.isConfigBilletera_mostrar_saldo_mayor_cero().equals("t") && (wcmv.getAvailableBalance() > 0.0))
                            || (billetera.isConfigBilletera_mostrar_saldo_cero().equals("t") && (wcmv.getAvailableBalance() == 0.0))
                            || (billetera.isConfigBilletera_mostrar_saldo_menor_cero().equals("t") && (wcmv.getAvailableBalance() < 0.0))) {

                        String typeUnid = "";
                        boolean swIgnorarPorExpiracion = false;

                        if (billetera.isConfigBilletera_no_mostrar_saldo_expirado().equals("t") && swSaldoExpirado) {
                            LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|billetera=" + billetera.getNombreComverse() + "|Configurada para NO mostrar Saldos expirados|Billetera Expirada, no se mostrara");

                            if (!billetera.isMostrarSaldoExpirado().equals("t")) {
                                swIgnorarPorExpiracion = true;
                            } else {
                                LOG.debug("MOSTRAR SALDO EXPIRADO, swIgnorarPorExpiracion: " + swIgnorarPorExpiracion);
                            }

                        }
                        LOG.debug("swIgnorarPorExpiracion: " + swIgnorarPorExpiracion);
                        //---------------------------------------------------------------------
                        if (!swIgnorarPorExpiracion) {
                            double valueAplicadoOperador = getValueAplicandoOperator(billetera, wcmv.getAvailableBalance());

                            LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|billetera=" + billetera.getNombreComverse() + "|Valor Despues de Aplicar Operador=" + valueAplicadoOperador);
                            String valueStrBalance = "";
                            if ((billetera.isConfigBilletera_mostrar_saldo_menor_cero().equals("f"))
                                    && (billetera.getMontoMinimo() >= valueAplicadoOperador)) {//si es monto min
                                String strSwParam = getParametro("default_texto_saldo_min_sw_valor");
                                if (strSwParam.equals("true")) {
                                    valueStrBalance = formatearValor(0.0, billetera.getCantidadDecimales());
                                    billeteraResponse.setValor(valueStrBalance);
                                    valueStrBalance = valueStrBalance + " " + billetera.getPrefijoUnidad();
                                } else {
                                    valueStrBalance = getParametro("default_texto_saldo_min");
                                    billeteraResponse.setValor(valueStrBalance);
                                }
                            } else if (billetera.getMontoMaximo() <= valueAplicadoOperador) {//si es monto max
                                valueStrBalance = getParametro("default_texto_saldo_max");
                                billeteraResponse.setValor(valueStrBalance);
                                typeUnid = billetera.getPrefijoUnidad();
                            } else {

                                //=======================================================================================================================================================
                                //========================== AQUI esta implementado la logica para procesar la unidades de navegacion para Billeteras Simples ===========================
                                valueStrBalance = formatearValor(valueAplicadoOperador, billetera.getCantidadDecimales());

                                if (this.listWalletSimpleUnidNav == null
                                        || !this.listWalletSimpleUnidNav.contains(billetera.getBilleteraId() + "")) {

                                    billeteraResponse.setValor(valueStrBalance);
                                    valueStrBalance = valueStrBalance + " " + billetera.getPrefijoUnidad();
                                    typeUnid = billetera.getPrefijoUnidad();
                                } else {//inicalmente considerar el valor precalculado en MB
                                    LOG.debug("************* Aplicando Cambio de Conversion de Unidades Navegacion");
                                    int cambioUnidadConversion = Integer.parseInt(getParametro("valor_conversion_unidad_navegacion"));
                                    int cambioUnidadMonto = Integer.parseInt(getParametro("valor_cambio_unidad_navegacion"));
                                    String auxUnidades = getParametro("valor_unidades_navegacion");
                                    LOG.debug("Unindades de Navegacion para Conversion disponibles:" + auxUnidades);
                                    String[] unidades = auxUnidades.split("[,]");
                                    typeUnid = unidades[0];
                                    billeteraResponse.setValor(valueStrBalance);
                                    valueStrBalance = valueStrBalance + " " + typeUnid;
                                    for (int i = 1; i < unidades.length; i++) {
                                        if (valueAplicadoOperador >= cambioUnidadMonto) {
                                            typeUnid = unidades[i];
                                            valueAplicadoOperador = UtilOperaciones.dividir(valueAplicadoOperador, cambioUnidadConversion);
                                            LOG.debug("Aplicando Unidad Navegacion:" + typeUnid + "[" + valueAplicadoOperador + "]");
                                            valueStrBalance = formatearValor(valueAplicadoOperador, billetera.getCantidadDecimales());
                                            billeteraResponse.setValor(valueStrBalance);
                                            valueStrBalance = valueStrBalance + " " + typeUnid;
                                        } else {
                                            LOG.debug("El valor de navegacion:" + valueAplicadoOperador + ", no es  mayor o igual al valor de acmbio de Unidad:" + cambioUnidadMonto);
                                            break;
                                        }
                                    }

                                }
                                //==========================================================================================================================================
                            }
                            if (billetera.isConfigBilletera_mostrar_vigencia().equals("t")) {
                                String strVigencia = "";
                                if (sw_is_fecha_ilimitada) {
                                    strVigencia = getParametro("vigencia_saldo_txt_vigencia_ilimitada");
                                    billeteraResponse.setExpiracion1(strVigencia);
                                } else {
                                    strVigencia = getMensageVigencia(wcmv.getExpiration());
                                    billeteraResponse.setExpiracion1(formatDateWithSimpleDate(wcmv.getExpiration()));
                                }

                                String StrSeparator = getParametro("default_wallet_texto_separador");
                                //Mensaje = billetera.getNombreComercial() + StrSeparator + valueStrBalance + " " + strVigencia;
                                Mensaje = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStrBalance + " " + strVigencia;
                            } else {
                                String StrSeparator = getParametro("default_wallet_texto_separador");
                                //Mensaje = billetera.getNombreComercial() + StrSeparator + valueStrBalance;
                                Mensaje = billetera.getConfigBilletera_nombreComercial() + StrSeparator + valueStrBalance;
                            }

                            //============================================ Billieteras Simples Segunda Expiracion Start================
                            if (billetera.isConfigBilletera_mostrar_segunda_fecha_exp().equals("t")) {
                                String strSegVigencia = "";
                                //wcmv.getExpiration().getTime()
                                Date aux = wcmv.getExpiration().getTime();
                                Calendar calSegFechExo = Calendar.getInstance();
                                calSegFechExo.setTime(aux);
                                try {
                                    Integer intervaloDias = Integer.parseInt(getParametro("seg_vigencia_dias_exp"));
                                    calSegFechExo.add(Calendar.DAY_OF_MONTH, intervaloDias);
                                    boolean isVisibleHora = (billetera.isConfigBilletera_mostrar_hora_segunda_fecha_exp().equals("t"));
                                    boolean isAplicarFormatoHora = (billetera.isConfigBilletera_asumir_formato_hora_primera_fecha().equals("t"));

                                    strSegVigencia = getMensageSegVigencia(calSegFechExo, isVisibleHora, isAplicarFormatoHora);
                                    billeteraResponse.setExpiracion2(formatDateWithSimpleDateSegFecha(calSegFechExo, isVisibleHora, isAplicarFormatoHora));
                                    if (!Mensaje.isEmpty()) {
                                        Mensaje = Mensaje + " " + strSegVigencia;
                                    } else {
                                        Mensaje = strSegVigencia;
                                    }
                                } catch (NumberFormatException e) {
                                    LOG.error("[Billeteras Simples]Error al Procesar datos Segunda Vigencia, el parametro 'seg_vigencia_dias_exp' debe ser numerico: " + e.getMessage(), e);
                                }
                            }
                            //============================================ Segunda Expiracion End =================

                            billeteraResponse.setNombre(billetera.getConfigBilletera_nombreComercial());
//                        billeteraResponse.setTipoUnidad(billetera.getPrefijoUnidad());
                            billeteraResponse.setTipoUnidad(typeUnid);

                            listBilleteras.add(billeteraResponse);
                        }
                    }

                    wcmv.setAvailableBalance(availableBalance);
                } else {
                    LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "| No se encontro Billetera Comverse con nombre=" + billetera.getNombreComverse() + "| Devolviendo empty");
                }
            }
        } catch (Exception e) {
            LOG.error("[getMensajeBilletera]ISDN=" + ISDN + "| Error al intentar generar mensaje para billetera=" + billetera.getNombreComverse() + " :" + e.getMessage(), e);
        }
        return Mensaje;
    }
    //--------------------------------------------------------------------------

    private String getMensajeBilleteraNueva(WalletComverse wcmv) {
        LOG.debug("[getMensajeBilleteraNueva]ISDN=" + ISDN + "|iniciando..proceso");
        String Mensaje = "";

        try {

            if (wcmv != null) {
                LOG.debug("[getMensajeBilleteraNueva]ISDN=" + ISDN + "|iniciando..proceso para billetera=" + wcmv.getNameWallet() + "|value=" + wcmv.getAvailableBalance());
                BilleteraResponse billeteraResponse = new BilleteraResponse();

                //boolean expiro = false;
                //recuperar parametro de bd
                boolean swMostrarVigencia = Boolean.parseBoolean(getParametro("default_wallet_sw_mostrar_vigencia"));
                //boolean swSaldoExpirado = false;
                Calendar nowCal = Calendar.getInstance();
                Calendar Cal = wcmv.getExpiration();
                //=========validacion fecha indefinida========================
                String StrFechaExpiration = UtilDate.dateToTimeString(Cal);
                String vigencia_saldo_vigencia_ilimitada = getParametro("vigencia_saldo_vigencia_ilimitada");
                boolean sw_is_fecha_ilimitada = false;
                if (vigencia_saldo_vigencia_ilimitada != null && StrFechaExpiration.trim().equals(vigencia_saldo_vigencia_ilimitada.trim())) {
                    LOG.debug("[getMensajeBilleteraNueva]ISDN=" + ISDN + "|Fecha=" + vigencia_saldo_vigencia_ilimitada + "| Es fecha Ilimitada");
                    sw_is_fecha_ilimitada = true;
                }
                //=========validacion fecha indefinida========================                       
                if ((!sw_is_fecha_ilimitada) && wcmv.getExpiration().before(nowCal)) {
                    LOG.debug("[getMensajeBilleteraNueva]ISDN=" + ISDN + "|billetera=" + wcmv.getNameWallet() + "|Billetera Expirada, El valor de la billetera se pondra  a cero (0)");
                    wcmv.setAvailableBalance(0.0);
                }
                //==========================================================

                if ((wcmv.getAvailableBalance() > 0)) {
                    //log.debug("[getMensajeBilletera]ISDN=" + ISDN + "|billetera=" + wcmv.getNameWallet() + "|Valor Despues de Aplicar Operador=" + valueAplicadoOperador);
                    String valueStr = "";
                    String StrCantDec = getParametro("default_wallet_cantidad_decimal");
                    int cantDec = Integer.parseInt(StrCantDec);
                    valueStr = formatearValor(wcmv.getAvailableBalance(), cantDec);

                    if (swMostrarVigencia) {
                        String strVigencia = "";
                        if (sw_is_fecha_ilimitada) {
                            strVigencia = getParametro("vigencia_saldo_txt_vigencia_ilimitada");
                        } else {
                            strVigencia = getMensageVigencia(wcmv.getExpiration());
                        }
                        String StrSeparator = getParametro("default_wallet_texto_separador");
                        Mensaje = wcmv.getNameWallet() + StrSeparator + valueStr + " " + strVigencia;

                        billeteraResponse.setExpiracion1(strVigencia);
                    } else {
                        String StrSeparator = getParametro("default_wallet_texto_separador");
                        Mensaje = wcmv.getNameWallet() + StrSeparator + valueStr;

                    }

                    billeteraResponse.setNombre(wcmv.getNameWallet());
                    billeteraResponse.setValor(valueStr);

                    listBilleteras.add(billeteraResponse);
                }
            }
            return Mensaje;
        } catch (Exception e) {
            LOG.error("[getMensajeBilletera]ISDN=" + ISDN + "| Error al intentar generar mensaje para billetera=" + wcmv.getNameWallet() + " :" + e.getMessage(), e);
        }
        return Mensaje;
    }
    //--------------------------------------------------------------------------

    private String getMensajeBilleteraComp(ComposicionBilletera compBilletera, List<WalletComverse> listWalletCMV, List<Offer> listOferta, List<Acumulador> listAcumulador) {
        //WalletComverse wcmv = getWalletCmv(billetera.getNombreComverse(), listWalletCMV);

        String Mensaje = "";
        double total = 0.0;
        Calendar cal = null;
        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|iniciando..proceso para billeteraComp=" + compBilletera.getNombre());
        try {
            List<Billetera> listaBilleteras = compBilletera.getListBilleteras();
            BilleteraResponse billeteraResponse = new BilleteraResponse();
            if (listaBilleteras != null) {

                for (Billetera billetera : listaBilleteras) {//------------------

                    LOG.debug("---------------------> BILLETERA_COMPUESTA: " + compBilletera.getConfig_nombreComercial() + ", BILLETERA: " + billetera.getNombreComverse());

                    WalletComverse wc = null;

                    if (billetera.isAlco().equals("t")) {
                        Alco:
                        for (Offer oferta : listOferta) {
                            if (billetera.getNombreComverse().equals(oferta.getName())) {
                                wc = new WalletComverse();
                                wc.setNameWallet(oferta.getName());
                                wc.setExpiration(oferta.getServiceEnd());
                                wc.setUsada(true);
                                wc.setAvailableBalance(1);

                                for (Acumulador acum : listAcumulador) {
                                    LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|" + billetera.getNombreAcumulador() + " = " + acum.getAccumulatorName());
                                    if (billetera.getNombreAcumulador() != null && billetera.getNombreAcumulador().equals(acum.getAccumulatorName())) {
                                        double totalAlco = billetera.getLimiteAcumulador() - acum.getAmount();
                                        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|totalAlco: " + totalAlco);
                                        if (totalAlco < 0) {
                                            wc.setAvailableBalance(0);
                                        } else {
                                            wc.setAvailableBalance(totalAlco);
                                        }
                                        break Alco;
                                    }
                                }
                                oferta.setUsada(true);
                            }
                        }
                    } else {
                        // Valida billeteras simples para sumar a la billetera compuesta [Monto Minimo]
                        if (UtilMontoMinimo.validarMontoMinimo(Propiedades.BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO2, wc, billetera, "")) {
                            wc = getWalletCmv(billetera.getNombreComverse(), listWalletCMV);
                        }
                    }

                    if (wc != null) {
                        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|GrupoBilletera=" + compBilletera.getNombre() + "|Adicionando valores de BilleteraComverse=" + wc.getNameWallet() + "|Value=" + wc.getAvailableBalance() + "|Expiration=" + wc.getExpiration().toString() + "|");
                        LOG.debug("[getMensajeBilleteraComp] total: " + total + " + " + wc.getAvailableBalance());
                        double subTotal = getValueAplicandoOperator(billetera, wc.getAvailableBalance());
                        total = total + subTotal;
//                        total = total + wc.getAvailableBalance();
                        LOG.debug("[getMensajeBilleteraComp] total: " + total);

                        if (cal == null) {
                            cal = wc.getExpiration();
                        }
                        //Calendar nowCal = Calendar.getInstance();
                        Calendar Cal = wc.getExpiration();
                        //=========validacion fecha indefinida========================
                        String StrFechaExpiration = UtilDate.dateToTimeString(Cal);
                        String vigencia_saldo_vigencia_ilimitada = getParametro("vigencia_saldo_vigencia_ilimitada");
                        boolean sw_is_fecha_ilimitada = false;
                        if (vigencia_saldo_vigencia_ilimitada != null && StrFechaExpiration.trim().equals(vigencia_saldo_vigencia_ilimitada.trim())) {
                            //log.info("[getMensajeBilleteraComp]ISDN=" + ISDN + "|Fecha=" + vigencia_saldo_vigencia_ilimitada + "| Es fecha Ilimitada");
                            sw_is_fecha_ilimitada = true;
                        }

                        //La fecha de expiracion debe ser la mayor de todas
                        //si se encuentra una fecha ilimitada se toma esa
                        if (wc.getExpiration().after(cal) || sw_is_fecha_ilimitada) {
                            cal = wc.getExpiration();
                        }

                    }
                }//fin del for-------------------------------------------------------------

                // Quitar if para deshabilitar monto minimo [Monto Minimo]
                if (!UtilMontoMinimo.validarMontoMinimoCompuesta(Propiedades.BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO1, total, compBilletera, "")) {
                    Mensaje = "";
                    cal = null;
                }
                if (cal != null) {
                    //==========================================================
                    LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|sumatoria de valores calculados:total=" + total + "|" + "fechaMax=" + cal.getTime().toString());
                    boolean swSaldoExpirado = false;
                    Calendar Now = Calendar.getInstance();
                    //=========validacion fecha indefinida========================
                    String StrFechaExpiration = UtilDate.dateToTimeString(cal);
                    String vigencia_saldo_vigencia_ilimitada = getParametro("vigencia_saldo_vigencia_ilimitada");
                    boolean sw_is_fecha_ilimitada = false;
                    if (vigencia_saldo_vigencia_ilimitada != null && StrFechaExpiration.equals(vigencia_saldo_vigencia_ilimitada)) {
                        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|Fecha=" + vigencia_saldo_vigencia_ilimitada + "| Es fecha Ilimitada");
                        sw_is_fecha_ilimitada = true;
                    }
                    if ((!sw_is_fecha_ilimitada) && cal.before(Now)) {
                        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|GrupoBilletera=" + compBilletera.getNombre() + "|Billetera Expirada, El valor de la billetera se pondra  a cero (0)");

                        if (compBilletera.isMostrar_saldo_expirado().equals("f")) {
                            total = 0.0;
                            swSaldoExpirado = true;
                        } else {
                            LOG.debug("Se va ha mostrar el saldo expirado swSaldoExpirado: " + swSaldoExpirado);
                        }

                        LOG.debug("No mostrar si expiro: " + compBilletera.isConfig_no_mostrar_saldo_expirado());
                        if (compBilletera.isConfig_no_mostrar_saldo_expirado().equals("t")) {
                            total = 0.0;
                            swSaldoExpirado = true;
                        } else {
                        }

                    } else {
                        if (compBilletera.isMostrar_saldo_expirado().equals("t")) {
                            total = 0.0;
                            swSaldoExpirado = true;
                        }
                    }
                    //==========================================================
                    if (compBilletera.isConfig_mostrar_siempre().equals("t")
                            || (compBilletera.isConfig_mostrar_saldo_mayor_cero().equals("t") && (total > 0.0))
                            || (compBilletera.isConfig_mostrar_saldo_cero().equals("t") && (total == 0.0))
                            || (compBilletera.isConfig_mostrar_saldo_menor_cero().equals("t") && (total < 0.0))) {
                        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|GrupoBilletera=" + compBilletera.getNombre() + "|Cumplio alguna condicion, se procede a generar mensaje");

                        String typeUnid = "";
                        boolean swIgnorarPorExpiracion = false;
                        if (compBilletera.isConfig_no_mostrar_saldo_expirado().equals("t") && swSaldoExpirado) {
                            LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|GrupoBilletera=" + compBilletera.getNombre() + "|Configurada para NO mostrar Saldos expirados|Billetera Expirada, no se mostrara");

                            if (compBilletera.isMostrar_saldo_expirado().equals("f")) {
                                swIgnorarPorExpiracion = true;
                            } else {
                                LOG.debug("Se va ha mostrar el saldo expirado swIgnorarPorExpiracion: " + swIgnorarPorExpiracion);
                            }

                        }
                        if (!swIgnorarPorExpiracion) {
                            double valueAplicadoOperador = getValueAplicandoOperator(compBilletera, total);
                            LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|Grupobilletera=" + compBilletera.getNombre() + "|Valor Despues de Aplicar Operador=" + valueAplicadoOperador);
                            String valueStr = "";
                            //SI esta configurado mostrar negativos, entonces saltar la validacion monto min
                            if ((compBilletera.isConfig_mostrar_saldo_menor_cero().equals("f"))
                                    && (compBilletera.getMontoMinimo() >= valueAplicadoOperador)) {//si es monto min
                                String strSwParam = getParametro("default_texto_saldo_min_sw_valor");
                                if (strSwParam.equals("true")) {
                                    valueStr = formatearValor(0.0, compBilletera.getCantidadDecimales());

                                    billeteraResponse.setValor(valueStr);
                                    billeteraResponse.setTipoUnidad(compBilletera.getPrefijoUnidad());

                                    valueStr = valueStr + " " + compBilletera.getPrefijoUnidad();
                                } else {
                                    valueStr = getParametro("default_texto_saldo_min");
                                    billeteraResponse.setValor(valueStr);
                                }
                            } else if (compBilletera.getMontoMaximo() <= valueAplicadoOperador) {//si es monto max
                                valueStr = getParametro("default_texto_saldo_max");
                                billeteraResponse.setValor(valueStr);
                                LOG.debug("[getMensajeBilletera]ISDN=" + ISDN + "|Grupobilletera=" + compBilletera.getNombre() + "|valor=" + valueAplicadoOperador + "|MAXIMO=" + compBilletera.getMontoMaximo() + "|El valor es igual o mayor al MAXIMO, se reemplazar con texto=" + valueStr + "|");

                            } else {

                                //=======================================================================================================================================================
                                //========================== AQUI esta implementado la logica para procesar la unidades de navegacion para Billeteras Compuestas ===========================
                                valueStr = formatearValor(valueAplicadoOperador, compBilletera.getCantidadDecimales());
                                if (this.listWalletCompUnidNav == null || !this.listWalletCompUnidNav.contains(compBilletera.getComposicionBilleteraId() + "")) {
                                    billeteraResponse.setValor(valueStr);
                                    valueStr = valueStr + " " + compBilletera.getPrefijoUnidad();
                                    typeUnid = compBilletera.getPrefijoUnidad();
                                } else {//inicalmente considerar el valor precalculado en MB
                                    LOG.debug("************* Aplicando Cambio de Conversion de Unidades Navegacion para Billeteras Compuestas");
                                    int cambioUnidadConversion = Integer.parseInt(getParametro("valor_conversion_unidad_navegacion"));
                                    int cambioUnidadMonto = Integer.parseInt(getParametro("valor_cambio_unidad_navegacion"));
                                    String auxUnidades = getParametro("valor_unidades_navegacion");
                                    LOG.debug("Unindades de Navegacion para Conversion disponibles:" + auxUnidades);
                                    String[] unidades = auxUnidades.split("[,]");
                                    typeUnid = unidades[0];
                                    billeteraResponse.setValor(valueStr);
                                    valueStr = valueStr + " " + typeUnid;
                                    for (int i = 1; i < unidades.length; i++) {
                                        if (valueAplicadoOperador >= cambioUnidadMonto) {
                                            typeUnid = unidades[i];
                                            valueAplicadoOperador = UtilOperaciones.dividir(valueAplicadoOperador, cambioUnidadConversion);
                                            LOG.debug("Aplicando Unidad Navegacion:" + typeUnid + "[" + valueAplicadoOperador + "]");
                                            valueStr = formatearValor(valueAplicadoOperador, compBilletera.getCantidadDecimales());
                                            billeteraResponse.setValor(valueStr);
                                            valueStr = valueStr + " " + typeUnid;
                                        } else {
                                            LOG.debug("El valor de navegacion:" + valueAplicadoOperador + ", no es  mayor o igual al valor de cambio de Unidad:" + cambioUnidadMonto);
                                            break;
                                        }
                                    }
                                }
                                //==========================================================================================================================================
                            }
                            if (compBilletera.isConfig_mostrar_vigencia().equals("t")) {
                                String strVigencia = "";
                                if (sw_is_fecha_ilimitada) {
                                    strVigencia = getParametro("vigencia_saldo_txt_vigencia_ilimitada");
                                } else {
                                    strVigencia = getMensageVigencia(cal);
                                }

                                String StrSeparator = getParametro("default_wallet_texto_separador");
                                Mensaje = compBilletera.getConfig_nombreComercial() + StrSeparator + valueStr + " " + strVigencia;

                                billeteraResponse.setExpiracion1(formatDateWithSimpleDate(cal));
                            } else {
                                String StrSeparator = getParametro("default_wallet_texto_separador");
                                Mensaje = compBilletera.getConfig_nombreComercial() + StrSeparator + valueStr;
                            }
                            //============================================ Billieteras Compuestas Segunda Expiracion Start ================
                            if (compBilletera.isConfigBilletera_mostrar_segunda_fecha_exp().equals("t")) {
                                String strSegVigencia = "";
                                //Calendar calSegFechExo = cal;                               
                                Calendar calSegFechExo = Calendar.getInstance();
                                calSegFechExo.setTime(cal.getTime());

                                try {
                                    Integer intervaloDias = Integer.parseInt(getParametro("seg_vigencia_dias_exp"));
                                    calSegFechExo.add(Calendar.DAY_OF_MONTH, intervaloDias);
                                    boolean isVisibleHora = compBilletera.isConfigBilletera_mostrar_hora_segunda_fecha_exp().equals("t");
                                    boolean isAplicarFormatoHora = compBilletera.isConfigBilletera_asumir_formato_hora_primera_fecha().equals("t");
                                    strSegVigencia = getMensageSegVigencia(calSegFechExo, isVisibleHora, isAplicarFormatoHora);
                                    billeteraResponse.setExpiracion2(formatDateWithSimpleDateSegFecha(calSegFechExo, isVisibleHora, isAplicarFormatoHora));
                                    if (!Mensaje.isEmpty()) {
                                        Mensaje = Mensaje + " " + strSegVigencia;
                                    } else {
                                        Mensaje = strSegVigencia;
                                    }
                                } catch (NumberFormatException e) {
                                    LOG.error("[Billeteras Compuetas] Error al Procesar datos Segunda Vigencia, el parametro 'seg_vigencia_dias_exp' debe ser numerico: " + e.getMessage(), e);
                                }

                            }
                            //============================================ Segunda Expiracion END ================

                            billeteraResponse.setNombre(compBilletera.getConfig_nombreComercial());
                            billeteraResponse.setTipoUnidad(typeUnid);
                            listBilleteras.add(billeteraResponse);
                        }
                    }
                }

            } else {//lista de billeteras del Grupo Billetera EMPTY
                //Mensaje = compBilletera.getConfig_nombreComercial() + StrSeparator + valueStr + " " + strVigencia;
                String StrSeparator = getParametro("default_wallet_texto_separador");
                String valueStr = formatearValor(0, compBilletera.getCantidadDecimales());
                billeteraResponse.setNombre(compBilletera.getConfig_nombreComercial());
                billeteraResponse.setValor(valueStr);
                billeteraResponse.setTipoUnidad(compBilletera.getPrefijoUnidad());
                listBilleteras.add(billeteraResponse);

                valueStr = valueStr + " " + compBilletera.getPrefijoUnidad();
                Mensaje = compBilletera.getConfig_nombreComercial() + StrSeparator + valueStr;
            }

        } catch (Exception e) {
            LOG.error("[getMensajeBilleteraComp]ISDN=" + ISDN + "| Error al intentar generar mensaje para billetera=" + compBilletera.getNombre() + " :" + e.getMessage(), e);

        }
        LOG.debug("[getMensajeBilleteraComp]ISDN=" + ISDN + "|billeteraAgrupada=" + compBilletera.getNombre() + " devolviendo |mensaje generado=" + Mensaje + "|");
        return Mensaje;
    }

    private WalletComverse getWalletCmv(String nameWallet, List<WalletComverse> listWalletCMV) {
        LOG.debug("[getWalletCmv]ISDN=" + ISDN + "| iniciando proceso para obtener billetera COMVERSE desde lista.");
        try {
            for (WalletComverse walletComverse : listWalletCMV) {
                LOG.debug("[getWalletCmv]iniciando comparacion.....walleCMV=" + walletComverse.getNameWallet() + "| billetera Entrante=" + nameWallet);
                if (walletComverse.getNameWallet().equals(nameWallet)) {
                    //walletComverse.setUsada(true);
                    LOG.debug("[getWalletCmv]ISDN=" + ISDN + "| WalleCMV=" + walletComverse.getNameWallet() + " billetera=" + nameWallet + "|Encontrado Valor=" + walletComverse.getAvailableBalance());
                    return walletComverse;
                }
            }
        } catch (Exception e) {
            LOG.error("[getWalletCmv]ISDN=" + ISDN + "| al intentar obtener walletCMV:" + e.getMessage(), e);
            return null;
        }
        return null;
    }
    //-------------------------------------------------------------------------

    private WalletComverse getWalletCmvAndMarcar(String nameWallet, List<WalletComverse> listWalletCMV) {
        LOG.debug("[getWalletCmvAndMarcar]ISDN=" + ISDN + "| iniciando proceso para obtener billetera COMVERSE desde lista.");
        try {
            if (listWalletCMV != null) {
                for (WalletComverse walletComverse : listWalletCMV) {
                    LOG.debug("[getWalletCmvAndMarcar]iniciando comparacion.....walleCMV=" + walletComverse.getNameWallet() + "| billetera Entrante=" + nameWallet);
                    if (walletComverse.getNameWallet().equals(nameWallet)) {
                        walletComverse.setUsada(true);
                        LOG.debug("[getWalletCmvAndMarcar]ISDN=" + ISDN + "| WalleCMV=" + walletComverse.getNameWallet() + " billetera=" + nameWallet + "|Encontrado Valor=" + walletComverse.getAvailableBalance());
                        return walletComverse;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("[getWalletCmvAndMarcar]ISDN=" + ISDN + "| al intentar obtener walletCMV:" + e.getMessage(), e);
            return null;
        }
        LOG.debug("[getWalletCmvAndMarcar]ISDN=" + ISDN + "| billetera nombre=" + nameWallet + "|NO ENCONTRADA EN COMVERSE|Verifique que este escrita correctamente");
        return null;
    }

    private String getMensageVigencia(Calendar Expiration) {
        //recuperar formato de bd y aplicar
        String textoVigencia = "";
        try {
            textoVigencia = getParametro("vigencia_saldo_texto");
            String strDate = formatDateWithSimpleDate(Expiration);
            textoVigencia = textoVigencia.replace("%FECHA_VIGENCIA%", strDate);
        } catch (Exception e) {
            LOG.error("[getMensajeVigencia] al intentar generar texto Vigencia: " + e.getMessage(), e);
        }
        return textoVigencia;
    }

    private String getMensageSegVigencia(Calendar Expiration, boolean isVisibleHora, boolean isAplicarFormatoHora) {
        //recuperar formato de bd y aplicar
        String textoVigencia = "";
        try {
            textoVigencia = getParametro("seg_vigencia_text_saldo");
            String strDate = formatDateWithSimpleDateSegFecha(Expiration, isVisibleHora, isAplicarFormatoHora);
            textoVigencia = textoVigencia.replace("%FECHA_VIGENCIA%", strDate);
        } catch (Exception e) {
            LOG.error("[getMensageSegVigencia] al intentar generar texto Vigencia: " + e.getMessage(), e);
        }
        return textoVigencia;
    }

    private String formatearValor(double value, int cantDec) {
        LOG.debug("FORMATEAR_VALOR value=" + value + " ; cantDecimales=" + cantDec);
        return formatValueWithDecimalFormat(value, cantDec);
    }

    //**************************************************************************
    //devuelve el valor calculado para una billetera
    //tomandod en cuenta el OPerador
    public double getValueAplicandoOperator(Billetera w, double valueComverse) {
        try {
            LOG.debug("[getValueAplicandoOperator]ISDN=" + ISDN + "|Billetera=" + w.getNombreComverse() + "|value in CMV=" + valueComverse + "|");
            double value = 0.0;
            double valueOperation = Truncar(w.getValor(), 2);
            // log.debug("[getValueAplicandoOperator]:Obteniendo Valor calculando con Operador="
//                + w.getOperador() + " con value comverse=" + valueComverse);
            //if (valueComverse > 0) {
            if (w.getOperador().equals("SUMA")) {
                value = valueComverse + valueOperation;
            } else if (w.getOperador().equals("RESTA")) {
                value = valueComverse - valueOperation;
            } else if (w.getOperador().equals("DIVISION")) {
                if (valueOperation != 0) {
                    // value = valueComverse / valueOperation;
                    value = (valueComverse * 10) / (valueOperation * 10);
                } else {
                    value = 0;
                }
            } else if (w.getOperador().equals("MULTIPLICACION")) {
                value = valueComverse * valueOperation;
            } else if (w.getOperador().equals("NINGUNO")) {
                value = valueComverse;
            }
            //}
            // log.debug("VALOR (antes de truncar o reducir) aplicando " + w.getOperador()
            // + " " + valueComverse + "," + valueOperation + "=" + value);
            if (w.getCantidadDecimales() == 0) {
                return Truncar(value, w.getCantidadDecimales());
            } else {
                return redondear(value, w.getCantidadDecimales());
            }

        } catch (Exception e) {
            LOG.error("[getValueAplicandoOperator]NO se pudo aplicar operador-valor:", e);
            return 0;
        }

    }
    //**************************************************************************
    //devuelve el valor calculado para una billetera
    //tomandod en cuenta el OPerador

    public double getValueAplicandoOperator(ComposicionBilletera w, double valueComverse) {
        try {
            LOG.debug("[getValueAplicandoOperator]Billetera Compuesta=" + w.getNombre() + "|valor=" + valueComverse);
            double value = 0.0;
            double valueOperation = Truncar(w.getValor(), 2);
            LOG.debug("[getValueAplicandoOperator]:Obteniendo Valor calculando con Operador="
                    + w.getOperador() + " con value comverse=" + valueComverse);

            //if (valueComverse > 0) {
            if (w.getOperador().equals("SUMA")) {
                value = valueComverse + valueOperation;
            } else if (w.getOperador().equals("RESTA")) {
                value = valueComverse - valueOperation;
            } else if (w.getOperador().equals("DIVISION")) {
                if (valueOperation != 0) {
                    // value = valueComverse / valueOperation;
                    value = (valueComverse * 10) / (valueOperation * 10);
                } else {
                    value = 0;
                }
            } else if (w.getOperador().equals("MULTIPLICACION")) {
                value = valueComverse * valueOperation;
            } else if (w.getOperador().equals("NINGUNO")) {
                value = valueComverse;
            }
            //}
            LOG.debug("VALOR (antes de truncar o reducir) aplicando " + w.getOperador()
                    + " " + valueComverse + "," + valueOperation + "=" + value);
            if (w.getCantidadDecimales() == 0) {
                return Truncar(value, w.getCantidadDecimales());
            } else {
                return redondear(value, w.getCantidadDecimales());
            }

        } catch (Exception e) {
            LOG.error("[getValueAplicandoOperator]:", e);
            return 0;
        }

    }
    //------------------------utilidades---------------------------------------

    public static String formatValueWithDecimalFormat(double valor, int cantdec) {
        String res = "";
        try {
            DecimalFormat Formateador = new DecimalFormat("0.0000");
            //NumberFormat Formateador= NumberFormat.getInstance();
            // log.debug("[FormatValue]: Iniciado formateando salida de valor " + valor);
            if ((valor % 1) == 0) {
                Formateador.setMaximumFractionDigits(0);
            } else {
                Formateador.setMaximumFractionDigits(cantdec);
            }
            Formateador.setRoundingMode(RoundingMode.DOWN);
            res = Formateador.format(valor);
            //log.debug("[FormatValue]: Despues de formateo " + res);
        } catch (Exception e) {
            LOG.error("[formatValueWithDecimal]error:" + e.getMessage(), e);
        }
        return res;
    }

    public String formatDateWithSimpleDate(Calendar cal) {
        String s = "";
        try {
            String formatDateExpiration = getParametro("vigencia_saldo_formato_date");
            SimpleDateFormat formateador = new SimpleDateFormat(formatDateExpiration);
            s = formateador.format(cal.getTime());
        } catch (Exception e) {
            LOG.error("[formatDateWithSimpleDate] Al intentar dar formato fecha:" + e.getMessage(), e);
        }
        return s;
    }

    public String formatDateWithSimpleDateSegFecha(Calendar cal, boolean isVisibleHora, boolean isAplicarFormatoHora) {
        String s = "";
        try {
            String patternSegFech = getParametro("seg_vigencia_formato_fecha");
            if (isVisibleHora && isAplicarFormatoHora) {
                patternSegFech = patternSegFech + " " + getParametro("seg_vigencia_formato_hora");
            }
            SimpleDateFormat formateador = new SimpleDateFormat(patternSegFech);
            s = formateador.format(cal.getTime());
            if (isVisibleHora && !isAplicarFormatoHora) {
                s = s + " 23:59:59";
            }

        } catch (Exception e) {
            LOG.error("[formatDateWithSimpleDateSegFecha] Al intentar dar formato fecha:" + e.getMessage(), e);
        }
        return s;
    }

    public double Truncar(double nD, int nDec) {
        if (nD > 0) {
            nD = Math.floor(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        } else {
            nD = Math.ceil(nD * Math.pow(10, nDec)) / Math.pow(10, nDec);
        }
        return nD;
    }

    public double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        double resultado = (Math.round(numero * mult)) / (double) mult;
        return resultado;
    }
    //--------------------------------------------------------------------------

    public void cargarParametros() {
        //canal 1 ussd
//        log.info("[cargarParametros] Iniciando carga de parametros..");
//        List<Parametro> listParam = ParametrosDAO.getListParametros();
//        ParametersBD = new HashMap<String, String>();
//        for (Parametro parametro : listParam) {
//            ParametersBD.put(parametro.getNombre(), parametro.getValor());
//        }
        ParametersBD = GestorConfig.getParameters();
        fillWallestUnidNav();
    }

    public String getParametro(String nameParameter) {
        try {
            if (ParametersBD != null) {
                return ParametersBD.get(nameParameter);
            } else {
                return "";
            }

        } catch (Exception e) {
            LOG.error("[getParametro] Exception: " + e.getMessage(), e);
        }
        return "";
    }

    private void fillWallestUnidNav() {
        String aux = getParametro("valor_billeteras_simple_unidad_navegacion");
        if (aux != null) {
            String[] billeSimp = aux.split("[,]");
            this.listWalletSimpleUnidNav = Arrays.asList(billeSimp);
            LOG.debug("Lista de Billeteras IDs Simples para conversion de Unidad de Navegacion:" + this.listWalletSimpleUnidNav.toString());
        }

        String aux1 = getParametro("valor_billeteras_agrupadas_unidad_navegacion");
        if (aux1 != null) {
            String[] billeComp = aux1.split("[,]");
            this.listWalletCompUnidNav = Arrays.asList(billeComp);
            LOG.debug("Lista de Billeteras IDs Compouestas para conversion de Unidad de Navegacion:" + this.listWalletCompUnidNav.toString());
        }
    }

    private boolean isChangeConfiguraciones() {
        try {
            //Connection con = ConexionBD.getConnection();
            Parametro param = ParametrosDAO.obtenerByName("sistema_consulta_saldo_sw_onchange" /*, con*/);
            //con.close();
            if (param != null && param.getValor().equals("true")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            LOG.error("[IsChangeConfiguraciones]", e);
            return false;
        }

    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    public String getErrorDescripcion() {
        return ErrorDescripcion;
    }

    public void setErrorDescripcion(String ErrorDescripcion) {
        this.ErrorDescripcion = ErrorDescripcion;
    }

    public String getSaludoInicial() {
        return saludoInicial;
    }

    public void setSaludoInicial(String saludoInicial) {
        this.saludoInicial = saludoInicial;
    }

    public List<BilleteraResponse> getListBilleteras() {
        return listBilleteras;
    }

    public void setListBilleteras(List<BilleteraResponse> listBilleteras) {
        this.listBilleteras = listBilleteras;
    }

    public String getPublicidad() {
        return publicidad;
    }

    public void setPublicidad(String publicidad) {
        this.publicidad = publicidad;
    }

    public void saveConsulta(ReporteConsulta RC) {
        reporteConsultaDAO.saveReporteConsulta(RC);
    }

    public static void main(String[] args) throws InterruptedException {

        // String aa = a.getConsultaSaldo("77800221", 500, false, "USSD", "*190#");
        //String aa = a.getConsultaSaldo("77309639", 500, false, "USSD", "*190#");
        //String aa = a.getConsultaSaldo("78793575", 10000, false, "USSD", "*190#");
        //String aa = a.getConsultaSaldo("75585277", 10000, false, "USSD", "*190#");        
        /*for (int i = 0; i < 30; i++) {
            Thread.sleep(500);
            long z = System.currentTimeMillis();
            consultaSaldo cs = new consultaSaldo();
            String aa = cs.getConsultaSaldo("77800221", 10000, false, "*", "*");            
            System.out.println(aa);
            List<BilleteraResponse> list = cs.getListBilleteras();                       
            System.out.println("TIGO SHOP : " + list.size());
            
            System.out.println("CONSULTA " + (System.currentTimeMillis() - z));
     
        }
         */
        consultaSaldo cs = new consultaSaldo();
        String aa = cs.getConsultaSaldo("74161584", 100, false, "*", "*", "", null);
        System.out.println(aa);

    }

}
