/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import myapps.cmn.dao.guideDAO;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.vo.guide;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class MessageConstructor {

    public static final int CODE_ERROR_LOGIN_WS = 1;
    public static final String MSG_ERROR_LOGIN_WS = "Error : Usuario y/o password No válidos";
    public static final int CODE_ERROR_LOAD_PROPER = 2;
    public static final String MSG_ERROR_LOAD_PROPER = "Error al cargar parametros";
    public static final int CODE_ERROR_MSG_ERROR = 3;
    public static final String MSG_ERROR_MSG_ERROR = "Error al intentar obtener mensaje";
    public static final int CODE_ERROR_CONEX_BD = 10;
    public static final String MSG_ERROR_CONEX_BD = "Error al intentar conectar a BD";
    public static final int CODE_ERROR_BD_GUIA = 11;
    public static final String MSG_ERROR_BD_GUIA = "Error al consultar tabla Guia de BD";
    public static final int CODE_ERROR_BD_BILLETERA = 12;
    public static final String MSG_ERROR_BD_BILLETERA = "Error al consultar tabla Billetera de BD";
    public static final int CODE_ERROR_BD_QUOTA = 13;
    public static final String MSG_ERROR_BD_QUOTA = "Error al consultar tabla QUOTA de BD";
    public static final int CODE_ERROR_COMVERSE_CONEX = 20;
    public static final String MSG_ERROR_CONVERSE_CONEX = "Error al conectar con Comverse";
    public static final int CODE_ERROR_COMVERSE_RESPONSE = 21;
    public static final String MSG_ERROR_CONVERSE_RESPONSE = "Error: Comverse no responde ";
    public static final int CODE_ERROR_COMVERSE_VERIFICAR = 22;
    public static final String MSG_ERROR_CONVERSE_VERIFICAR = "Error: no se puede "
            + "verificar estado Comverse. Error al leer tabla Configuracion";
    private static final Logger LOG = Logger.getLogger(MessageConstructor.class);
//    public int CodeError = 0;
//    public String MessageError = "";
    //private double ValorCoreBalanceCurrent = 0.0;
    List<guide> listGuides;

    //para el valor del GPRS_SUSCRIPTION q se calculara con del valor en mb del DPI
    //private double ValorGPRSCurrent=0.0; 
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    public String getMessagePublicidad(int NumberClient, int lengthMax) {
        //String Message=null;  
        try {
            LOG.debug("[getMessagePublicidad]|ISDN=" + NumberClient + "|: longitud max esperada= " + lengthMax + " Iniciando proceso para obtener mensaje");
            String nametabGuide = GetNameGuidesWallet();
            //Obtener lista de guias    
            listGuides = new ArrayList<guide>();
            String sqlGuides = guideDAO.getGenerarSQLGuides(NumberClient, nametabGuide);
            listGuides = guideDAO.getGuides(NumberClient, this, sqlGuides);
            LOG.debug("[getMessagePublicidad]|ISDN=" + NumberClient
                    + "|Lista de guias recuperada con " + listGuides.size());
            int k = 0;
            guide myguide = null;
            //------------------------------------------------------------------
            while (k < listGuides.size()) {
                myguide = listGuides.get(k);
                String msgResp = myguide.getMessageCommercial();
                if ((msgResp.length()) <= lengthMax) {
                    String msgFinal = myguide.getMessageCommercial();
                    LOG.debug("[getMessagePublicidad]|ISDN=" + NumberClient + "|: message "
                            + "a RETORNAR (longitud=" + msgFinal.length() + " )='" + msgFinal + "'");
//                    CodeError = 0;
//                    MessageError = "";
                    return (msgFinal);
                }
                k++;
            }
            //------------------------------------------------------------------
        } catch (Exception e) {
            LOG.error("[getMessagePublicidad] Error al intentar obtener "
                    + "Mensaje publicidad"
                    + "con el Numero de Cliente " + NumberClient + " "
                    + "Exception:" + e.getMessage());
            //CodeError = 0;
            return "";
        }
        return "";
    }

    public String GetNameGuidesWallet() {
        return guideDAO.getMaxFormatGuidesWallet(this);

    }

    /*public static void main(String arg[]) {
        //String s=formatearValor(123.935,4);
        //String s2=formatValueWithDecimalFormat(CODE_ERROR_BD_GUIA, CODE_ERROR_BD_GUIA)ecimalFormat(123.935,4);
    }*/
}
