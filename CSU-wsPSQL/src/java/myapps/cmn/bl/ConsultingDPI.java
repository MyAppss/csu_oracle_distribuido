/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;
/*
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import myapps.cmn.dao.quotaDAO;
import myapps.cmn.util.Propiedades;
import myapps.cmn.vo.QuotaDPI;
import org.apache.log4j.Logger;
*/
/**
 *
 * @author Vehimar
 */
public class ConsultingDPI {
//
//    private static final Logger LOG = Logger.getLogger(ConsultingDPI.class);
//    //==========================================================================
//
//    private static List<QuotaDPI> getListQuotas(String msisdn, String urlCamiant) {
//        try {
//            List<QuotaDPI> myList = new LinkedList<QuotaDPI>();
//
//            //recuperar xml con consulta http           
//            String Consulta = urlCamiant + "/rs/msr/sub/msisdn/" + msisdn + "/data/quota";
//            //log.info("[ConsultingDPI]:query: "+Consulta);
//            String XML_DPI = consultaHTTP(Consulta);
//
//            /*
//             * String XML_DPI="<?xml version=1.0 encoding=UTF-8
//             * standalone=yes?><subscriber>" + "<data name=quota><?xml
//             * version=1.0 encoding=UTF-8?><usage>" + "
//             * <version>1</version><quota name=\"1 GB Topup Quota Postpaid\">" +
//             * "<cid>5764888998014545849</cid><nextResetTime>2012-03-01T00:00:00-04:00</nextResetTime>"
//             * + "<totalVolume>1048589575</totalVolume> </quota><quota name=\"1
//             * GB Quota Postpaid\">" +
//             * "<cid>5764888998014545836</cid><nextResetTime>2012-03-01T00:00:00-04:00</nextResetTime>"
//             * +
//             * "<totalVolume>1073806369</totalVolume></quota></usage></data></subscriber>";
//             */
//            XML_DPI = XML_DPI.replace("&lt;", "<");
//            XML_DPI = XML_DPI.replace("&quot;", "\"");
//            XML_DPI = XML_DPI.replace("&gt;", ">");
//
//            //======================================================
//            int Index = 0;
//            boolean HayQuotas = true;
//
//            while (HayQuotas) {
//
//                int indexIni = XML_DPI.indexOf("<quota name", Index);
//                int indexFin = XML_DPI.indexOf("</quota>", Index);
//
//                if ((indexIni > -1) && (indexFin > -1)) {
//
//                    if (indexFin > -1) {
//                        indexFin = indexFin + 8;
//                    }
//
//                    if ((indexIni >= 0) && (indexFin >= 0)) {
//                        String StrSectionQuota = XML_DPI.substring(indexIni, indexFin);
//
//                        int indexIniNameQuota = StrSectionQuota.indexOf("name=\"");
//                        int indexFinNameQuota = StrSectionQuota.indexOf(">");
//                        String StrNameQuota = StrSectionQuota.substring(indexIniNameQuota + 6, indexFinNameQuota - 1);
//
//                        int indexIniVolumenQuota = StrSectionQuota.indexOf("<totalVolume>");
//                        int indexFinVolumenQuota = StrSectionQuota.indexOf("</totalVolume>");
//                        String StrVolumenQuota = StrSectionQuota.substring(indexIniVolumenQuota + 13, indexFinVolumenQuota);
//
//                        QuotaDPI quota = new QuotaDPI();
//                        quota.setPaquetePlan(StrNameQuota);
//                        quota.setVolumenConsumido(Long.parseLong(StrVolumenQuota));
//                        myList.add(quota);
//                    }
//                    Index = indexFin;
//                } else {
//                    HayQuotas = false;
//                }
//
//            }
//            return myList;
//        } catch (Exception e) {
//            LOG.error("[getListQuotas]Error al intentar Obtener lista de QUOTAS:", e);
//        }
//        return new LinkedList<QuotaDPI>();
//    }
//
//    //==========================================================================
//    public static HashMap<String, String> getMapState(String msisdn, String urlCamiant) {
//        HashMap<String, String> myMap = new HashMap();
//        //recuperar xml con consulta http        
//        String Consulta = urlCamiant + "/rs/msr/sub/msisdn/" + msisdn + "/data/state";
//        String XML_DPI = consultaHTTP(Consulta);
//
//        XML_DPI = XML_DPI.replace("&lt;", "<");
//        XML_DPI = XML_DPI.replace("&quot;", "\"");
//        XML_DPI = XML_DPI.replace("&gt;", ">");
//
//        //======================================================
//        int Index = 0;
//        boolean HayQuotas = true;
//
//        while (HayQuotas) {
//
//            int indexIni = XML_DPI.indexOf("<property", Index);
//            int indexFin = XML_DPI.indexOf("</property>", Index);
//
//            if ((indexIni > -1) && (indexFin > -1)) {
//
//                if (indexFin > -1) {
//                    indexFin = indexFin + 11;
//                }
//
//                if ((indexIni >= 0) && (indexFin >= 0)) {
//                    String StrSectionQuota = XML_DPI.substring(indexIni, indexFin);
//
//                    int indexIniNameQuota = StrSectionQuota.indexOf("<name>");
//                    int indexFinNameQuota = StrSectionQuota.indexOf("</name>");
//                    String StrNameProperty = StrSectionQuota.substring(indexIniNameQuota + 6, indexFinNameQuota);
//
//                    int indexIniVolumenQuota = StrSectionQuota.indexOf("<value>");
//                    int indexFinVolumenQuota = StrSectionQuota.indexOf("</value>");
//                    String StrValueProperty = StrSectionQuota.substring(indexIniVolumenQuota + 7, indexFinVolumenQuota);
//
//                    myMap.put(StrNameProperty, StrValueProperty);
//
//                }
//                Index = indexFin;
//            } else {
//                HayQuotas = false;
//            }
//
//        }
//
//        return myMap;
//    }
//
//    public static List<String> getListProfile(String msisdn, String urlCamiant) {
//        List<String> myList = new LinkedList<String>();
//        try {
//            String StrDateExpiration = "";
//
//            String Consulta = urlCamiant + "/rs/msr/sub/msisdn/" + msisdn + "";
//
//            String XML_DPI = consultaHTTP(Consulta);
//
//            XML_DPI = XML_DPI.replace("&lt;", "<");
//            XML_DPI = XML_DPI.replace("&quot;", "\"");
//            XML_DPI = XML_DPI.replace("&gt;", ">");
//
//            //sacar fecha de expiracion
//            int index1 = XML_DPI.indexOf("<field name=\"Custom2\">", 0);
//            int index2 = XML_DPI.indexOf("</field>", index1);
//
//            if ((index1 >= 0) && (index2 >= 0)) {
//                //XML_DPI.sub
//                //String StrSectionDate=XML_DPI.substring(index1+23, index2);
//                StrDateExpiration = XML_DPI.substring(index1 + 23, index2);
//            }
//
//            int Index = 0;
//            boolean HayQuotas = true;
//
//            while (HayQuotas) {
//
//                int indexIni = XML_DPI.indexOf("<field name=\"Entitlement\">", Index);
//                int indexFin = XML_DPI.indexOf("</field>", indexIni);
//
//                if ((indexIni > -1) && (indexFin > -1)) {
//
//                    if ((indexIni >= 0) && (indexFin >= 0)) {
//                        //XML_DPI.sub
//                        String StrSectionEntitlement = XML_DPI.substring(indexIni + 26, indexFin);
//                        myList.add(StrSectionEntitlement);
//
//                    }
//                    Index = indexFin;
//                } else {
//                    HayQuotas = false;
//                }
//            }
//
//        } catch (Exception e) {
//            LOG.error("[getListProfile]Al intentar obtener perfiles.", e);
//        }
//
//        return myList;
//    }
//
//    public static boolean IsDateExpired(String msisdn, String urlCamiant) {
//
//        String StrDateExpiration = "";
//        boolean Expired = false;
//
//        String Consulta = urlCamiant + "/rs/msr/sub/msisdn/" + msisdn + "";
//
//        String XML_DPI = consultaHTTP(Consulta);
//
//        XML_DPI = XML_DPI.replace("&lt;", "<");
//        XML_DPI = XML_DPI.replace("&quot;", "\"");
//        XML_DPI = XML_DPI.replace("&gt;", ">");
//
//        int index1 = XML_DPI.indexOf("<field name=\"Custom2\">", 0);
//        int index2 = XML_DPI.indexOf("</field>", index1);
//
//        if ((index1 >= 0) && (index2 >= 0)) {
//
//            StrDateExpiration = XML_DPI.substring(index1 + 22, index2);
//        }
//        if (!StrDateExpiration.equals("")) {
//            String[] ArrDateAndHours = StrDateExpiration.split("T");
//            String[] ArraOnlyDate = ArrDateAndHours[0].split("-");
//
//            String[] ArraOnlyHours = ArrDateAndHours[1].split(":");
//
//            Date DateExpiration = new Date(Integer.parseInt(ArraOnlyDate[0]) - 1900, Integer.parseInt(ArraOnlyDate[1]) - 1, Integer.parseInt(ArraOnlyDate[2]), Integer.parseInt(ArraOnlyHours[0]), Integer.parseInt(ArraOnlyHours[1]), Integer.parseInt(ArraOnlyHours[2]));
//
//            Date now = new Date();
//            if (now.after(DateExpiration)) {
//                Expired = true;
//            }
//        }
//
//        return Expired;
//    }
//    //=========================================================================
//    //=========================================================================
//
//    public static double ConsultBalanceDPI(String msisdn, String urlCamiant) {
//        // double Res=0.0;  
//        try {
//            List<QuotaDPI> ListQuotas = getListQuotas(msisdn, urlCamiant);
//            //open connection
//            //Connection con = ConexionBD.getConnection();
//            //==================================================================
//            if (ListQuotas.size() > 0) {
//                //System.out.print("ListaQuotas>0 Iniciando resta");
//                double sumQuotasBytes = 0.0;
//                Map<String, Long> mapResult = quotaDAO.getValueQuotas(/*con*/);
//
//                //calculo de quotas y volumen consumido-------------------------
//                for (int i = 0; i < ListQuotas.size(); i++) {
//                    QuotaDPI mytop = ListQuotas.get(i);
//                    String currentPlan = mytop.getPaquetePlan();
//                    double currentVolConsumido = mytop.getVolumenConsumido();
//
//                    //recuperar Volumen del plan
//                    double currentVolPlan = mapResult.get(currentPlan);
//                    double currentSaldo = currentVolPlan - currentVolConsumido;
//                    if (currentSaldo < 0) {
//                        currentSaldo = 0;
//                    }
//                    sumQuotasBytes = sumQuotasBytes + currentSaldo;
//                }
//                //-------------------------------------------------------------
//                if (sumQuotasBytes > 0) {
//                    return BytesToMbs(sumQuotasBytes);
//                } else {
//
//                    Map<String, String> MyMapStateDPI = getMapState(msisdn, urlCamiant);
//                    //MyListQuotasDPI.
//                    if (MyMapStateDPI.containsKey(new String("TopUp"))) {
//                        //++++++++++++++++++++++++++++++++++++++++++++++++
//                        String valueQuotaPlanTopUp = "INDEFINIDO";
//                        if (MyMapStateDPI.containsKey("quota_plan_topup")) {
//                            valueQuotaPlanTopUp = MyMapStateDPI.get("quota_plan_topup");
//                        }
//                        //++++++++++++++++++++++++++++++++++++++++++++++++
//
//                        if (valueQuotaPlanTopUp.equals("cancelled")) {
//                            //retornar saldo 0
//                            return BytesToMbs(0.0);
//                        } else {
//                            //sacar el valor de bd para un determinado estado de TopUp
//                            Map<String, Long> MyMapStateValue = quotaDAO.getValueState(/*con*/);
//                            //sacar el estado de TopUp
//                            String StateTopUp = MyMapStateDPI.get("TopUp");
//                            //sacar valor en bytes para el estado del topUp
//                            double BytesState = MyMapStateValue.get(StateTopUp);
//                            return BytesToMbs(BytesState);
//                        }
//                    } else {
//                        return BytesToMbs(0.0);
//                    }
//                }
//
//            }//====================================================================    
//            else { //puede que no haya empezado a consumir
//                //veri
//                List<String> ListaProfiles = getListProfile(msisdn, urlCamiant);
//                //if(ListaProfiles)
//                if (ListaProfiles.size() > 0) {
//                    String sql = quotaDAO.getSqlQuota(ListaProfiles);
//                    long bytes = quotaDAO.getBytesProfile(sql /*, con*/);
//
//                    if (IsDateExpired(msisdn, urlCamiant)) {
//                        return BytesToMbs(0.0);
//                    } else {
//                        return BytesToMbs(bytes);
//                    }
//                } else {
//                    return BytesToMbs(0.0);
//                }
//
//            }
//
//        } catch (Exception e) {
//            LOG.error("[ConsultaBalanceDPI]Al intentar recuperar saldo:", e);
//        }
//        return 0;
//        //return Res;
//    }
//
//    //=========================================================================
//    public static double BytesToMbs(double bytes) {
//        double divisor = 1024 * 1024;
//        return bytes / (divisor);
//    }
//    //==========================================================================
//
//    public static Date getDateExpiration(String msisdn, String urlCamiant) {
//        Date DateExpiration = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MONTH, 1);
//        cal.set(Calendar.DAY_OF_MONTH, 1);
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        DateExpiration.setTime(cal.getTimeInMillis());
//        try {
//            String StrDateExpiration = "";
//            String Consulta = urlCamiant + "/rs/msr/sub/msisdn/" + msisdn + "";
//            //log.info("[ConsultingDPI]:query: "+Consulta);
////            String XML_DPI = HttpUtils.conexionGET(Consulta, "HTTP");
//            String XML_DPI = consultaHTTP(Consulta);
//            XML_DPI = XML_DPI.replace("&lt;", "<");
//            XML_DPI = XML_DPI.replace("&quot;", "\"");
//            XML_DPI = XML_DPI.replace("&gt;", ">");
//
//            //======================================================================
//            //sacar fecha de expiracion
//            int index1 = XML_DPI.indexOf("<field name=\"Custom2\">", 0);
//            int index2 = XML_DPI.indexOf("</field>", index1);
//
//            if ((index1 >= 0) && (index2 >= 0)) {
//                //XML_DPI.sub
//                //String StrSectionDate=XML_DPI.substring(index1+23, index2);
//                StrDateExpiration = XML_DPI.substring(index1 + 22, index2);
//            }
//            if (!StrDateExpiration.equals("")) {//si fecha de expiracion dif de null
//                String[] ArrDateAndHours = StrDateExpiration.split("T");
//                String[] ArraOnlyDate = ArrDateAndHours[0].split("-");
//                String[] ArraOnlyHours = ArrDateAndHours[1].split(":");
//                DateExpiration = new Date(Integer.parseInt(ArraOnlyDate[0]) - 1900, Integer.parseInt(ArraOnlyDate[1]) - 1, Integer.parseInt(ArraOnlyDate[2]), Integer.parseInt(ArraOnlyHours[0]), Integer.parseInt(ArraOnlyHours[1]), Integer.parseInt(ArraOnlyHours[2]));
//
//            }
//            //return DateExpiration;
//        } catch (Exception e) {
//            LOG.error("[getDateExpiration]", e);
//        }
//        return DateExpiration;
//
//        //return DateExpiration;
//    }
//    //==========================================================================
//
//    private static String consultaHTTP(String url) {
//        String cadena_salida = "";
//
//        String inputLine = "";
////        String inLine = "";
//        StringBuilder sb = new StringBuilder();
//        HttpURLConnection connection = null;
//        //OutputStream out = null;
//        BufferedReader in = null;
//        //Writer wout = null;
//
//        try {
//            URL u = new URL(url);
//            URLConnection uc = u.openConnection();
//            connection = (HttpURLConnection) uc;
//
//            connection.setDoOutput(true);
//            connection.setDoInput(true);
//            connection.setRequestMethod("GET");
//            connection.setRequestProperty("Content-type", "application/camiant-msr-v1+xml");
//            connection.setRequestProperty("Accept", "");
//            connection.setConnectTimeout(Integer.parseInt(Propiedades.DPI_CONNET_TIME_OUT));
//            connection.setReadTimeout(Integer.parseInt(Propiedades.DPI_READ_TIMEOUT));
//
////            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
//
//            while ((inputLine = in.readLine()) != null) {
////                inLine = inLine + inputLine;
//                sb.append(inputLine);
//            }
//            String inLine = sb.toString();
//            cadena_salida = inLine != null && !inLine.isEmpty() ? inLine : cadena_salida;
//        } catch (IOException | NumberFormatException e) {
//            LOG.warn("Error consulta Url: " + url, e);
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//
//            /* if (out != null) {
//                try {
//                    out.close();
//                } catch (IOException ex) {
//                    LOG.error("No se puede cerrar el outbuffer", ex);
//                }
//            }*/
//            if (in != null) {
//                try {
//                    in.close();
//                } catch (IOException ex) {
//                    LOG.error("No se puede cerrar el BufferedReader", ex);
//                }
//            }
//
//            /*if (wout != null) {
//                try {
//                    wout.close();
//                } catch (IOException ex) {
//                    LOG.error("No se puede cerrar el BufferedReader", ex);
//                }
//            }*/
//        }
//        return cadena_salida;
//    }
//
}
