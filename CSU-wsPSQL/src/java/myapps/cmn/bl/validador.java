/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.bl;

import java.util.List;
import java.util.Map;
import javax.xml.ws.handler.MessageContext;
import myapps.cmn.util.Propiedades;
import myapps.security.Credencial;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class validador {

    private static final Logger LOG = Logger.getLogger(validador.class);

    public static Credencial getCredenciales(MessageContext mctx) {
        LOG.debug("Obteniendo credenciales");
        Credencial credencial = new Credencial();
        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
        List userList = (List) http_headers.get("Username");
        List passList = (List) http_headers.get("Password");
        String user = (String) ((userList != null && userList.size() > 0) ? userList.get(0) : "");
        String pass = (String) ((passList != null && passList.size() > 0) ? passList.get(0) : "");
        credencial.setUser(user);
        credencial.setPass(pass);
        return credencial;
    }
//

    public static boolean verificarCredencial(Credencial credencial) {
        String config_user = Propiedades.USER_WS.trim();
        String config_pass = Propiedades.PASS_WS.trim();
        boolean b = (config_user.equals(credencial.getUser()) && config_pass.equals(credencial.getPass()));
        if (b) {
            LOG.debug("Usuario de Webservice autenticado correctamente");
        } else {
            LOG.debug("El Usuario y/o Pass para autenticar en WebService No son Válidos");
        }
        return b;
    }
}
