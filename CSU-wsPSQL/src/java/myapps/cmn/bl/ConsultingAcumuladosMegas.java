package myapps.cmn.bl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import myapps.cmn.util.AcumuladoMega;
import myapps.cmn.util.Propiedades;
import micrium.consultaacumuladomegas.ws.acumulado.Acumulado;
import micrium.consultaacumuladomegas.ws.consultaacumuladosmegas.ConsultaAcumuladosMegas_Service;
import micrium.consultaacumuladomegas.ws.obteneracumuladosrequest.ObtenerAcumuladosRequest;
import micrium.consultaacumuladomegas.ws.obteneracumuladosresponse.ObtenerAcumuladosResponse;
import micrium.consultaacumuladomegas.ws.plataforma.Plataforma;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class ConsultingAcumuladosMegas {

    private static final Logger LOG = Logger.getLogger(ConsultingAcumuladosMegas.class);

    public static AcumuladoMega getAcumuladosMegas(String isdn, String mostrarDpi, String mostrarAcumuladosMegas, String urlAcumuladosMegas) {
        AcumuladoMega mega = new AcumuladoMega(0, null);
        try {
            URL wsdl = new URL(urlAcumuladosMegas);
            ConsultaAcumuladosMegas_Service service = new ConsultaAcumuladosMegas_Service(wsdl);

            LOG.debug("[getAcumuladosMegas] wsdl del servicio acumulados megas: " + service.getWSDLDocumentLocation());

            ObtenerAcumuladosRequest request = new ObtenerAcumuladosRequest();
            request.setCuenta("591" + isdn);
            if (mostrarDpi.equals("f") && mostrarAcumuladosMegas.equals("t")) {
                request.setPlataforma(Plataforma.COMVERSE);
            } else {
                request.setPlataforma(Plataforma.DPI);
            }

            request.setBilletera(Propiedades.BILLETERA);
            ObtenerAcumuladosResponse response = service.getConsultaAcumuladosMegasSOAP().obtenerAcumulados(request);
            Date fecha = null;

            if (response != null && response.getCodigo() == 0) {
                LOG.debug("[getAcumuladosMegas] isdn: " + isdn + ", codigo: " + response.getCodigo() + ", descripcion: " + response.getDescripcion() + ", cuota: " + response.getCuotaEquivalente() + ", idPaquete: " + response.getIdPaqueteEquivalente());

                double cuota = response.getCuotaEquivalente();
                if (cuota > 0) {
                    mega.setCuota(cuota);

                    List<Acumulado> acumulados = response.getAcumulados();
                    if (acumulados != null) {
                        for (Acumulado ac : acumulados) {
                            fecha = ac.getFechaExpiracion().toGregorianCalendar().getTime();
                        }
                        mega.setExpiracion(fecha);
                    }

                } else {
                    if (mostrarDpi.equals("t") && mostrarAcumuladosMegas.equals("t")) { // SI TENGO Q CONSULTAR TB POR COMVERSE
                        request.setPlataforma(Plataforma.COMVERSE);
                        response = service.getConsultaAcumuladosMegasSOAP().obtenerAcumulados(request);
                        if (response != null && response.getCodigo() == 0) {
                            LOG.debug("[getAcumuladosMegas] plataforma: COMVERSE isdn: " + isdn + ", codigo: " + response.getCodigo() + ", descripcion: " + response.getDescripcion() + ", cuota: " + response.getCuotaEquivalente() + ", idPaquete: " + response.getIdPaqueteEquivalente());
                            cuota = response.getCuotaEquivalente();
                            mega.setCuota(cuota);

                            List<Acumulado> acumulados = response.getAcumulados();
                            if (acumulados != null) {
                                for (Acumulado ac : acumulados) {
                                    fecha = ac.getFechaExpiracion().toGregorianCalendar().getTime();
                                }
                                mega.setExpiracion(fecha);
                            }
                        } else {
                            String error = "";
                            if (response != null) {
                                error = "code: " + response.getCodigo() + ", descripcion: " + response.getDescripcion();
                            }
                            LOG.warn("[getAcumuladosMegas] isdn: " + isdn + ", " + error);
                        }
                    } else {
                        List<Acumulado> acumulados = response.getAcumulados();
                        if (acumulados != null) {
                            for (Acumulado ac : acumulados) {
                                fecha = ac.getFechaExpiracion().toGregorianCalendar().getTime();
                            }
                            mega.setExpiracion(fecha);
                        }
                    }
                }
            } else {
                String error = "";
                if (response != null) {
                    error = "code: " + response.getCodigo() + ", descripcion: " + response.getDescripcion();
                }
                LOG.warn("[getAcumuladosMegas] isdn: " + isdn + ", " + error);
            }

        } catch (MalformedURLException e) {
            LOG.error("[getAcumuladosMegas] Error de Conexion al Servicio Consulta de Acumulados Megas isdn: " + isdn + ", dpi: " + mostrarDpi + ", ac: " + mostrarAcumuladosMegas, e);
            mega = null;
        } catch (Exception e) {
            LOG.error("[getAcumuladosMegas] Error en el Sistema isdn: " + isdn + ", dpi: " + mostrarDpi + ", ac: " + mostrarAcumuladosMegas, e);
            mega = null;
        }
        return mega;
    }
}
