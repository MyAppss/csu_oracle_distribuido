package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class guide implements Serializable{

    private static final long serialVersionUID = 1L;

    private int clientNumber;
    private int campaingId;
    private int messageId;
    private int campaingPriority;
    private int messagePriority;
    private int messagelength;
    private String messageInitial;
    private String messageCommercial;

    /**
     * @return the clientNumber
     */
    public int getClientNumber() {
        return clientNumber;
    }

    /**
     * @param clientNumber the clientNumber to set
     */
    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    /**
     * @return the campaingId
     */
    public int getCampaingId() {
        return campaingId;
    }

    /**
     * @param campaingId the campaingId to set
     */
    public void setCampaingId(int campaingId) {
        this.campaingId = campaingId;
    }

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the campaingPriority
     */
    public int getCampaingPriority() {
        return campaingPriority;
    }

    /**
     * @param campaingPriority the campaingPriority to set
     */
    public void setCampaingPriority(int campaingPriority) {
        this.campaingPriority = campaingPriority;
    }

    /**
     * @return the messagePriority
     */
    public int getMessagePriority() {
        return messagePriority;
    }

    /**
     * @param messagePriority the messagePriority to set
     */
    public void setMessagePriority(int messagePriority) {
        this.messagePriority = messagePriority;
    }

    /**
     * @return the messagelength
     */
    public int getMessagelength() {
        return messagelength;
    }

    /**
     * @param messagelength the messagelength to set
     */
    public void setMessagelength(int messagelength) {
        this.messagelength = messagelength;
    }

    /**
     * @return the messageInitial
     */
    public String getMessageInitial() {
        return messageInitial;
    }

    /**
     * @param messageInitial the messageInitial to set
     */
    public void setMessageInitial(String messageInitial) {
        this.messageInitial = messageInitial;
    }

    /**
     * @return the messageCommercial
     */
    public String getMessageCommercial() {
        return messageCommercial;
    }

    /**
     * @param messageCommercial the messageCommercial to set
     */
    public void setMessageCommercial(String messageCommercial) {
        this.messageCommercial = messageCommercial;
    }

}
