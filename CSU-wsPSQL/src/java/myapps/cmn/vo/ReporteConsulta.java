/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author Vehimar
 */
public class ReporteConsulta implements Serializable {

    private static final long serialVersionUID = 1234L;

    private Timestamp fecha;
    private String msisdn;
    private String identificador_servicio;
    private String nombre_canal;
    private String publicidad_solicitada;
    private int longitud_max_solicitada;
    private String ip_cliente;
    private String texto_generado;
    private String sessionId;
    private String opcion;

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getIdentificador_servicio() {
        return identificador_servicio;
    }

    public void setIdentificador_servicio(String identificador_servicio) {
        this.identificador_servicio = identificador_servicio;
    }

    public String getIp_cliente() {
        return ip_cliente;
    }

    public void setIp_cliente(String ip_cliente) {
        this.ip_cliente = ip_cliente;
    }

    public int getLongitud_max_solicitada() {
        return longitud_max_solicitada;
    }

    public void setLongitud_max_solicitada(int longitud_max_solicitada) {
        this.longitud_max_solicitada = longitud_max_solicitada;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getNombre_canal() {
        return nombre_canal;
    }

    public void setNombre_canal(String nombre_canal) {
        this.nombre_canal = nombre_canal;
    }

    public String isPublicidad_solicitada() {
        return publicidad_solicitada;
    }

    public void setPublicidad_solicitada(String publicidad_solicitada) {
        this.publicidad_solicitada = publicidad_solicitada;
    }

    public String getTexto_generado() {
        return texto_generado;
    }

    public void setTexto_generado(String texto_generado) {
        this.texto_generado = texto_generado;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    @Override
    public String toString() {
        return "ReporteConsulta{" + "fecha=" + fecha + ", msisdn=" + msisdn + ", identificador_servicio=" + identificador_servicio + ", nombre_canal=" + nombre_canal + ", publicidad_solicitada=" + publicidad_solicitada + ", longitud_max_solicitada=" + longitud_max_solicitada + ", ip_cliente=" + ip_cliente + ", texto_generado=" + texto_generado + ", sessionId=" + sessionId + ", opcion=" + opcion + '}';
    }
    
    
    
}
