package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class Billetera implements Serializable {

    private static final long serialVersionUID = 1234L;

    private Integer billeteraId;
    private Integer unitTypeId;
    private String nombreComverse;
    private String nombreComercial;
    private String prefijoUnidad;
    private String operador;
    private double valor;
    private Integer cantidadDecimales;
    private double montoMinimo;
    private double montoMaximo;
    private String estado;
    private String alco;
    private String nombreAcumulador;
    private Integer limiteAcumulador;
    //-------------------------------------------------------------------------
    private String ConfigBilletera_nombreComercial;
    private String ConfigBilletera_mostrar_siempre;
    private String ConfigBilletera_mostrar_saldo_mayor_cero;
    private String ConfigBilletera_mostrar_saldo_menor_cero;
    private String ConfigBilletera_mostrar_saldo_cero;
    private String ConfigBilletera_no_mostrar_saldo_expirado;
    private String ConfigBilletera_mostrar_vigencia;
    private String mostrarSaldoExpirado;

    //////////////////////////////////////////////////////////
    private String ConfigBilletera_mostrar_segunda_fecha_exp;
    private String ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    private String ConfigBilletera_asumir_formato_hora_primera_fecha;
    ////////////////////////////////////////////////////////

    private Integer categoriaId;

    public Integer getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(Integer dato) {
        this.billeteraId = dato;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(Integer dato) {
        this.unitTypeId = dato;
    }

    public String getNombreComverse() {
        return nombreComverse;
    }

    public void setNombreComverse(String dato) {
        this.nombreComverse = dato;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String dato) {
        this.nombreComercial = dato;
    }

    public String getPrefijoUnidad() {
        return prefijoUnidad;
    }

    public void setPrefijoUnidad(String dato) {
        this.prefijoUnidad = dato;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String dato) {
        this.operador = dato;
    }

    public Integer getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(Integer dato) {
        this.cantidadDecimales = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public double getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(double montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String isConfigBilletera_mostrar_saldo_cero() {
        return ConfigBilletera_mostrar_saldo_cero;
    }

    public void setConfigBilletera_mostrar_saldo_cero(String ConfigBilletera_mostrar_saldo_cero) {
        this.ConfigBilletera_mostrar_saldo_cero = ConfigBilletera_mostrar_saldo_cero;
    }

    public String isConfigBilletera_no_mostrar_saldo_expirado() {
        return ConfigBilletera_no_mostrar_saldo_expirado;
    }

    public void setConfigBilletera_no_mostrar_saldo_expirado(String ConfigBilletera_no_mostrar_saldo_expirado) {
        this.ConfigBilletera_no_mostrar_saldo_expirado = ConfigBilletera_no_mostrar_saldo_expirado;
    }

    public String isConfigBilletera_mostrar_saldo_mayor_cero() {
        return ConfigBilletera_mostrar_saldo_mayor_cero;
    }

    public void setConfigBilletera_mostrar_saldo_mayor_cero(String ConfigBilletera_mostrar_saldo_mayor_cero) {
        this.ConfigBilletera_mostrar_saldo_mayor_cero = ConfigBilletera_mostrar_saldo_mayor_cero;
    }

    public String isConfigBilletera_mostrar_saldo_menor_cero() {
        return ConfigBilletera_mostrar_saldo_menor_cero;
    }

    public void setConfigBilletera_mostrar_saldo_menor_cero(String ConfigBilletera_mostrar_saldo_menor_cero) {
        this.ConfigBilletera_mostrar_saldo_menor_cero = ConfigBilletera_mostrar_saldo_menor_cero;
    }

    public String isConfigBilletera_mostrar_siempre() {
        return ConfigBilletera_mostrar_siempre;
    }

    public void setConfigBilletera_mostrar_siempre(String ConfigBilletera_mostrar_siempre) {
        this.ConfigBilletera_mostrar_siempre = ConfigBilletera_mostrar_siempre;
    }

    public String getConfigBilletera_nombreComercial() {
        return ConfigBilletera_nombreComercial;
    }

    public void setConfigBilletera_nombreComercial(String ConfigBilletera_nombreComercial) {
        this.ConfigBilletera_nombreComercial = ConfigBilletera_nombreComercial;
    }

    public String isConfigBilletera_mostrar_vigencia() {
        return ConfigBilletera_mostrar_vigencia;
    }

    public void setConfigBilletera_mostrar_vigencia(String ConfigBilletera_mostrar_vigencia) {
        this.ConfigBilletera_mostrar_vigencia = ConfigBilletera_mostrar_vigencia;
    }

    public String isAlco() {
        return alco;
    }

    public void setAlco(String alco) {
        this.alco = alco;
    }

    public String getNombreAcumulador() {
        return nombreAcumulador;
    }

    public void setNombreAcumulador(String nombreAcumulador) {
        this.nombreAcumulador = nombreAcumulador;
    }

    public Integer getLimiteAcumulador() {
        return limiteAcumulador;
    }

    public void setLimiteAcumulador(Integer limiteAcumulador) {
        this.limiteAcumulador = limiteAcumulador;
    }

    public String isMostrarSaldoExpirado() {
        return mostrarSaldoExpirado;
    }

    public void setMostrarSaldoExpirado(String mostrarSaldoExpirado) {
        this.mostrarSaldoExpirado = mostrarSaldoExpirado;
    }

    public String isConfigBilletera_mostrar_segunda_fecha_exp() {
        return ConfigBilletera_mostrar_segunda_fecha_exp;
    }

    public void setConfigBilletera_mostrar_segunda_fecha_exp(String ConfigBilletera_mostrar_segunda_fecha_exp) {
        this.ConfigBilletera_mostrar_segunda_fecha_exp = ConfigBilletera_mostrar_segunda_fecha_exp;
    }

    public String isConfigBilletera_mostrar_hora_segunda_fecha_exp() {
        return ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    }

    public void setConfigBilletera_mostrar_hora_segunda_fecha_exp(String ConfigBilletera_mostrar_hora_segunda_fecha_exp) {
        this.ConfigBilletera_mostrar_hora_segunda_fecha_exp = ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    }

    public String isConfigBilletera_asumir_formato_hora_primera_fecha() {
        return ConfigBilletera_asumir_formato_hora_primera_fecha;
    }

    public void setConfigBilletera_asumir_formato_hora_primera_fecha(String ConfigBilletera_asumir_formato_hora_primera_fecha) {
        this.ConfigBilletera_asumir_formato_hora_primera_fecha = ConfigBilletera_asumir_formato_hora_primera_fecha;
    }

    public Integer getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(Integer categoriaId) {
        this.categoriaId = categoriaId;
    }

    @Override
    public String toString() {
        return "Billetera{" + "billeteraId=" + billeteraId + ", unitTypeId=" + unitTypeId + ", nombreComverse=" + nombreComverse + ", nombreComercial=" + nombreComercial + ", prefijoUnidad=" + prefijoUnidad + ", operador=" + operador + ", valor=" + valor + ", cantidadDecimales=" + cantidadDecimales + ", montoMinimo=" + montoMinimo + ", montoMaximo=" + montoMaximo + ", estado=" + estado + ", alco=" + alco + ", nombreAcumulador=" + nombreAcumulador + ", limiteAcumulador=" + limiteAcumulador + ", ConfigBilletera_nombreComercial=" + ConfigBilletera_nombreComercial + ", ConfigBilletera_mostrar_siempre=" + ConfigBilletera_mostrar_siempre + ", ConfigBilletera_mostrar_saldo_mayor_cero=" + ConfigBilletera_mostrar_saldo_mayor_cero + ", ConfigBilletera_mostrar_saldo_menor_cero=" + ConfigBilletera_mostrar_saldo_menor_cero + ", ConfigBilletera_mostrar_saldo_cero=" + ConfigBilletera_mostrar_saldo_cero + ", ConfigBilletera_no_mostrar_saldo_expirado=" + ConfigBilletera_no_mostrar_saldo_expirado + ", ConfigBilletera_mostrar_vigencia=" + ConfigBilletera_mostrar_vigencia + ", mostrarSaldoExpirado=" + mostrarSaldoExpirado + '}';
    }
}
