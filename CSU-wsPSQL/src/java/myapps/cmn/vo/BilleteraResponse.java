/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class BilleteraResponse implements Serializable {

    private static final long serialVersionUID = 1234L;

    private String nombre;
    private String valor;
    private String tipoUnidad;
    private String expiracion1;
    private String expiracion2;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipoUnidad() {
        return tipoUnidad;
    }

    public void setTipoUnidad(String tipoUnidad) {
        this.tipoUnidad = tipoUnidad;
    }

    public String getExpiracion1() {
        return expiracion1;
    }

    public void setExpiracion1(String expiracion1) {
        this.expiracion1 = expiracion1;
    }

    public String getExpiracion2() {
        return expiracion2;
    }

    public void setExpiracion2(String expiracion2) {
        this.expiracion2 = expiracion2;
    }

    @Override
    public String toString() {
        return "BilleteraResponse{" + "nombre=" + nombre + ", valor=" + valor + ", tipoUnidad=" + tipoUnidad + ", expiracion1=" + expiracion1 + ", expiracion2=" + expiracion2 + '}';
    }
    
    

}
