/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class QuotaDPI implements Serializable{

    private static final long serialVersionUID = 1L;

    private String paquetePlan;
    private long volumenConsumido;

    public String getPaquetePlan() {
        return paquetePlan;
    }

    public void setPaquetePlan(String paquetePlan) {
        this.paquetePlan = paquetePlan;
    }

    public long getVolumenConsumido() {
        return volumenConsumido;
    }

    public void setVolumenConsumido(long volumenConsumido) {
        this.volumenConsumido = volumenConsumido;
    }

}
