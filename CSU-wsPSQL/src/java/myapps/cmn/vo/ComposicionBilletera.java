package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

public class ComposicionBilletera implements Serializable {

    private static final long serialVersionUID = 1234L;

    private Integer composicionBilleteraId;
    private String nombre;
    private String nombreComercial;
    private String prefijoUnidad;
    private String operador;
    private Double valor;
    private Integer cantidadDecimales;
    private String estado;
    private Integer unitTypeId;
    private double montoMinimo;
    private double montoMaximo;
    //-------------------------------------------------------------------------
    private String Config_nombreComercial;
    private String Config_mostrar_siempre;
    private String Config_mostrar_saldo_mayor_cero;
    private String Config_mostrar_saldo_menor_cero;
    private String Config_mostrar_saldo_cero;
    private String Config_no_mostrar_saldo_expirado;
    private String Config_mostrar_vigencia;
    private String mostrar_saldo_expirado;

    //////////////////////////////////////////////////////////
    private String ConfigBilletera_mostrar_segunda_fecha_exp;
    private String ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    private String ConfigBilletera_asumir_formato_hora_primera_fecha;
    ////////////////////////////////////////////////////////

    private List<Billetera> listBilleteras;
//     private boolean newForConfig;

    public Integer getComposicionBilleteraId() {
        return composicionBilleteraId;
    }

    public void setComposicionBilleteraId(Integer dato) {
        this.composicionBilleteraId = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String dato) {
        this.nombreComercial = dato;
    }

    public String getPrefijoUnidad() {
        return prefijoUnidad;
    }

    public void setPrefijoUnidad(String dato) {
        this.prefijoUnidad = dato;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String dato) {
        this.operador = dato;
    }

    public Integer getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(Integer dato) {
        this.cantidadDecimales = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public String isConfig_mostrar_saldo_cero() {
        return Config_mostrar_saldo_cero;
    }

    public void setConfig_mostrar_saldo_cero(String Config_mostrar_saldo_cero) {
        this.Config_mostrar_saldo_cero = Config_mostrar_saldo_cero;
    }

    public String isConfig_no_mostrar_saldo_expirado() {
        return Config_no_mostrar_saldo_expirado;
    }

    public void setConfig_no_mostrar_saldo_expirado(String Config_no_mostrar_saldo_expirado) {
        this.Config_no_mostrar_saldo_expirado = Config_no_mostrar_saldo_expirado;
    }

    public String isConfig_mostrar_saldo_mayor_cero() {
        return Config_mostrar_saldo_mayor_cero;
    }

    public void setConfig_mostrar_saldo_mayor_cero(String Config_mostrar_saldo_mayor_cero) {
        this.Config_mostrar_saldo_mayor_cero = Config_mostrar_saldo_mayor_cero;
    }

    public String isConfig_mostrar_saldo_menor_cero() {
        return Config_mostrar_saldo_menor_cero;
    }

    public void setConfig_mostrar_saldo_menor_cero(String Config_mostrar_saldo_menor_cero) {
        this.Config_mostrar_saldo_menor_cero = Config_mostrar_saldo_menor_cero;
    }

    public String isConfig_mostrar_siempre() {
        return Config_mostrar_siempre;
    }

    public void setConfig_mostrar_siempre(String Config_mostrar_siempre) {
        this.Config_mostrar_siempre = Config_mostrar_siempre;
    }

    public String getConfig_nombreComercial() {
        return Config_nombreComercial;
    }

    public void setConfig_nombreComercial(String Config_nombreComercial) {
        this.Config_nombreComercial = Config_nombreComercial;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Billetera> getListBilleteras() {
        return listBilleteras;
    }

    public void setListBilleteras(List<Billetera> listBilleteras) {
        this.listBilleteras = listBilleteras;
    }

    public double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public double getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(double montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public String isConfig_mostrar_vigencia() {
        return Config_mostrar_vigencia;
    }

    public void setConfig_mostrar_vigencia(String Config_mostrar_vigencia) {
        this.Config_mostrar_vigencia = Config_mostrar_vigencia;
    }

    public String isMostrar_saldo_expirado() {
        return mostrar_saldo_expirado;
    }

    public void setMostrar_saldo_expirado(String mostrar_saldo_expirado) {
        this.mostrar_saldo_expirado = mostrar_saldo_expirado;
    }

    public String isConfigBilletera_mostrar_segunda_fecha_exp() {
        return ConfigBilletera_mostrar_segunda_fecha_exp;
    }

    public void setConfigBilletera_mostrar_segunda_fecha_exp(String ConfigBilletera_mostrar_segunda_fecha_exp) {
        this.ConfigBilletera_mostrar_segunda_fecha_exp = ConfigBilletera_mostrar_segunda_fecha_exp;
    }

    public String isConfigBilletera_mostrar_hora_segunda_fecha_exp() {
        return ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    }

    public void setConfigBilletera_mostrar_hora_segunda_fecha_exp(String ConfigBilletera_mostrar_hora_segunda_fecha_exp) {
        this.ConfigBilletera_mostrar_hora_segunda_fecha_exp = ConfigBilletera_mostrar_hora_segunda_fecha_exp;
    }

    public String isConfigBilletera_asumir_formato_hora_primera_fecha() {
        return ConfigBilletera_asumir_formato_hora_primera_fecha;
    }

    public void setConfigBilletera_asumir_formato_hora_primera_fecha(String ConfigBilletera_asumir_formato_hora_primera_fecha) {
        this.ConfigBilletera_asumir_formato_hora_primera_fecha = ConfigBilletera_asumir_formato_hora_primera_fecha;
    }

    @Override
    public String toString() {
        return "ComposicionBilletera{" + "composicionBilleteraId=" + composicionBilleteraId + ", nombre=" + nombre + ", nombreComercial=" + nombreComercial + ", prefijoUnidad=" + prefijoUnidad + ", operador=" + operador + ", valor=" + valor + ", cantidadDecimales=" + cantidadDecimales + ", estado=" + estado + ", unitTypeId=" + unitTypeId + ", montoMinimo=" + montoMinimo + ", montoMaximo=" + montoMaximo + ", Config_nombreComercial=" + Config_nombreComercial + ", Config_mostrar_siempre=" + Config_mostrar_siempre + ", Config_mostrar_saldo_mayor_cero=" + Config_mostrar_saldo_mayor_cero + ", Config_mostrar_saldo_menor_cero=" + Config_mostrar_saldo_menor_cero + ", Config_mostrar_saldo_cero=" + Config_mostrar_saldo_cero + ", Config_no_mostrar_saldo_expirado=" + Config_no_mostrar_saldo_expirado + ", Config_mostrar_vigencia=" + Config_mostrar_vigencia + ", mostrar_saldo_expirado=" + mostrar_saldo_expirado + ", listBilleteras=" + listBilleteras + '}';
    }

}
