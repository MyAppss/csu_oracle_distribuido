/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class Occ implements Serializable{

    private static final long serialVersionUID = 1L;

    private Integer occId;
    private Integer configId;
    private Integer origenId;
    private String nombreOrigen;
    private Integer cortoId;
    private String nombreCorto;
    private Integer cosId;
    private String nombreCos;
    private Integer posicion;

    public Integer getOccId() {
        return occId;
    }

    public void setOccId(Integer occId) {
        this.occId = occId;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public Integer getOrigenId() {
        return origenId;
    }

    public void setOrigenId(Integer origenId) {
        this.origenId = origenId;
    }

    public String getNombreOrigen() {
        return nombreOrigen;
    }

    public void setNombreOrigen(String nombreOrigen) {
        this.nombreOrigen = nombreOrigen;
    }

    public Integer getCortoId() {
        return cortoId;
    }

    public void setCortoId(Integer cortoId) {
        this.cortoId = cortoId;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public Integer getCosId() {
        return cosId;
    }

    public void setCosId(Integer cosId) {
        this.cosId = cosId;
    }

    public String getNombreCos() {
        return nombreCos;
    }

    public void setNombreCos(String nombreCos) {
        this.nombreCos = nombreCos;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }
}
