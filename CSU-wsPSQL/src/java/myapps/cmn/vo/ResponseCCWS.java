/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vehimar
 */
public class ResponseCCWS implements Serializable{

    private static final long serialVersionUID = 1L;

    String errorDescription;
    List<WalletComverse> listWallet;
    String nameCOS;
    List<Offer> listOffer;
    List<Acumulador> listAcumulador;

    public List<Acumulador> getListAcumulador() {
        return listAcumulador;
    }

    public void setListAcumulador(List<Acumulador> listAcumulador) {
        this.listAcumulador = listAcumulador;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public List<WalletComverse> getListWallet() {
        return listWallet;
    }

    public void setListWallet(List<WalletComverse> listWallet) {
        this.listWallet = listWallet;
    }

    public String getNameCOS() {
        return nameCOS;
    }

    public void setNameCOS(String nameCOS) {
        this.nameCOS = nameCOS;
    }

    public List<Offer> getListOffer() {
        return listOffer;
    }

    public void setListOffer(List<Offer> listOffer) {
        this.listOffer = listOffer;
    }

    @Override
    public String toString() {
        return "ResponseCCWS{" + "errorDescription=" + errorDescription + ", listWallet=" + listWallet + ", nameCOS=" + nameCOS + ", listOffer=" + listOffer + ", listAcumulador=" + listAcumulador + '}';
    }

}
