package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class Campaing implements Serializable {

    private static final long serialVersionUID = 1234L;

    private int campaingId;
    private String description;
    private String initMessage;
    private int priority;
    private String status;

    /**
     * @return the campaingId
     */
    public int getCampaingId() {
        return campaingId;
    }

    /**
     * @param campaingId the campaingId to set
     */
    public void setCampaingId(int campaingId) {
        this.campaingId = campaingId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the initMessage
     */
    public String getInitMessage() {
        return initMessage;
    }

    /**
     * @param initMessage the initMessage to set
     */
    public void setInitMessage(String initMessage) {
        this.initMessage = initMessage;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
