/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;

/**
 *
 * @author Vehimar REVISAR ESTA CLASE, se debe crear get y set methods para
 * evitar observacion en Coverity Actualizar en cliente
 */
public class MessageResponse implements Serializable{

    private static final long serialVersionUID = 1L;

    private int codeError;
    private String descriptionError;
    private String message;

    public int getCodeError() {
        return codeError;
    }

    public void setCodeError(int codeError) {
        this.codeError = codeError;
    }

    public String getDescriptionError() {
        return descriptionError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageResponse{" + "codeError=" + codeError + ", descriptionError=" + descriptionError + ", message=" + message + '}';
    }
    
    

}
