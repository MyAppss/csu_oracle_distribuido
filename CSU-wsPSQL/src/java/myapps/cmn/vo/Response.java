/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vehimar
 */
public class Response implements Serializable{

    private static final long serialVersionUID = 1L;

    private int codeError;
    private String descripcion;
    private String saludoInicial;
    private String publicidad;
    private List<BilleteraResponse> billeteras = new ArrayList<>();

    public int getCodeError() {
        return codeError;
    }

    public void setCodeError(int codeError) {
        this.codeError = codeError;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSaludoInicial() {
        return saludoInicial;
    }

    public void setSaludoInicial(String saludoInicial) {
        this.saludoInicial = saludoInicial;
    }

    public List<BilleteraResponse> getBilleteras() {
        return billeteras;
    }

    public void setBilleteras(List<BilleteraResponse> billeteras) {
        this.billeteras = billeteras;
    }

    public String getPublicidad() {
        return publicidad;
    }

    public void setPublicidad(String publicidad) {
        this.publicidad = publicidad;
    }

    @Override
    public String toString() {
        return "Response{" + "codeError=" + codeError + ", descripcion=" + descripcion + ", saludoInicial=" + saludoInicial + ", publicidad=" + publicidad + ", billeteras=" + billeteras.toString() + '}';
    }
    
    
}
