/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import myapps.cmn.vo.MessageResponse;
import myapps.cmn.vo.ReporteConsulta;
import myapps.cmn.vo.Response;
import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;

import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class UtilJava {

    public static final Logger LOG = Logger.getLogger(ConexionBD.class);

    public static boolean probarConexion(String url, int puerto) {
        boolean valor = false;
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(url, puerto), Propiedades.MQ_TIMEOUT);
            valor = socket.isConnected();
            socket.close();
        } catch (IOException e) {
            LOG.error("[probarConexion] No se pudo establecer conexion hacia " + url + ":" + puerto);
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                LOG.error("[probarConexion] Error al cerrar sSocket");
            }
        }

        return valor;
    }

    public static String maptoString(HashMap<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, Object> mapa;
        try {
            if (map != null) {
                if (map.containsKey("DETALLE_CONSULTA")) {
                    ReporteConsulta rc = (ReporteConsulta) map.get("DETALLE_CONSULTA");
                    stringBuilder.append(rc.toString());
                } else {
                    mapa = map;
                    mapa.entrySet().forEach((entry) -> {
                        stringBuilder.append(entry.getKey()).append(" = ").append(entry.getValue()).append("; ");
                    });
                }

            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        return stringBuilder.toString();
    }

    /*public static void main(String[] args) {
        long tiempo = System.currentTimeMillis();
        System.out.println(probarConexion("172.28.10.96", 61616));
        System.out.println("tardo " + (System.currentTimeMillis() - tiempo) + " ms");
    }*/
    public static String requestValido(String telefono, String canal, String corto, String logPrefijo) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!validarCadena(telefono, Propiedades.PATTERN_TELEFONO)) {
            stringBuilder.append(" -Formato Telefono no Valido ").append(Propiedades.PATTERN_TELEFONO);
            LOG.debug(logPrefijo + "Formato Telefono no Valido, " + telefono);
        }

        if (!validarCadena(canal, Propiedades.PATTERN_CHANNEL)) {
            stringBuilder.append(" -Formato Canal no Valido").append(Propiedades.PATTERN_CHANNEL);
            LOG.debug(logPrefijo + "Formato Canal no Valido, " + canal);
        }

        if (!validarCadena(corto, Propiedades.PATTERN_SHORT)) {
            stringBuilder.append(" -Formato Corto no Valido").append(Propiedades.PATTERN_SHORT);
            LOG.debug(logPrefijo + "Formato Corto no Valido, " + corto);
        }

        return stringBuilder.toString();
    }

    public static boolean validarCadena(String cadena, String patter) {
        boolean valor = false;
        if (cadena != null && patter != null) {
            Pattern pat = Pattern.compile(patter);
            Matcher mat = pat.matcher(cadena);
            valor = mat.matches();
        }
        return valor;
    }

    public static Response devolverRespuestaFallidaList(String isdn, String requisitos, String logPrefijo) {
        Response me = new Response();
        me.setCodeError(300);
        me.setDescripcion(requisitos);
        me.setSaludoInicial("PARAMETROS INVALIDOS");
        return me;
    }

    public static MessageResponse devolverRespuestaFallida(String isdn, String requisitos, String logPrefijo) {
        MessageResponse me = new MessageResponse();
        me.setCodeError(300);
        me.setDescriptionError(requisitos);
        me.setMessage("PARAMETROS INVALIDOS");
        return me;
    }

    public static void imprimirXml(String nombreServicio, Call call, String logPrefijo) {
        try {
            if (call != null && call.getMessageContext() != null) {
                LOG.info(logPrefijo + " REQUEST enviado al servicio " + nombreServicio + ": " + call.getMessageContext().getRequestMessage().getSOAPPartAsString());
                LOG.info(logPrefijo + " RESPONSE obtenido del servicio " + nombreServicio + ": " + call.getMessageContext().getResponseMessage().getSOAPPartAsString());
            } else {
                LOG.warn(logPrefijo + " No se pudo imprimir REQUEST y RESPONSE del servicio " + nombreServicio);
            }
        } catch (AxisFault f) {
            LOG.error(logPrefijo + "AxisFault Error al imprimir XML: " + f.getMessage(), f);

        } catch (Exception e) {
            LOG.warn(logPrefijo + "Exception Error al imprimir XML: " + e.getMessage());

        }
    }

    public static void imprimirXmlError(String nombreServicio, Call call, String logPrefijo) {
        try {
            if (call != null && call.getMessageContext() != null) {
                LOG.error(logPrefijo + " REQUEST enviado al servicio " + nombreServicio + ": " + call.getMessageContext().getRequestMessage().getSOAPPartAsString());
                LOG.error(logPrefijo + " RESPONSE obtenido del servicio " + nombreServicio + ": " + call.getMessageContext().getResponseMessage().getSOAPPartAsString());
            } else {
                LOG.warn(logPrefijo + " No se pudo imprimir REQUEST y RESPONSE del servicio " + nombreServicio);
            }
        } catch (AxisFault f) {
            LOG.error(logPrefijo + "AxisFault Error al imprimir XML: " + f.getMessage(), f);

        } catch (Exception e) {
            LOG.error(logPrefijo + "Exception Error al imprimir XML: " + e.getMessage());

        }
    }
}
