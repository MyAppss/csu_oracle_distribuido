/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.www.bme.cbsinterface.bcservices.BcServices_ServiceLocator;
import com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultLifeCycleStatus;
import com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg;
import com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo;
import com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader;
import com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.xml.rpc.ServiceException;
import org.apache.axis.AxisFault;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class MethodsCBS implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger(MethodsCBS.class);
    private List<String> codigosAceptados = null;
    private List<String> billeterasAfectadas = null;

    public MethodsCBS() {
        codigosAceptados = cadenaToList(Propiedades.VM_CBS_RESPONSE_CODIGOS_ACEPTADOS);
        billeterasAfectadas = cadenaToList(Propiedades.VM_CBS_BILLETERA_MODIFICAR);
    }

    private QuerySubLifeCycleResultMsg obtenerPeticion(QuerySubLifeCycleRequestMsg request, String logPrefijo) throws AxisFault, RemoteException, ServiceException {
        QuerySubLifeCycleResultMsg response = null;
        BcServicsBindingStub port = null;
        long tInicioConexion = 0;
        long tFinalConexion = 0;
        long tInicioRespuesta = 0;
        long tFinalRespuesta = 0;
        try {
            tInicioConexion = System.currentTimeMillis();
            BcServices_ServiceLocator locator = new BcServices_ServiceLocator();
            locator.setBcServicesPortEndpointAddress(Propiedades.VM_CBS_SOAP_SERVIDOR);
            port = (BcServicsBindingStub) locator.getBcServicesPort();
            port.setTimeout(Propiedades.VM_CBS_SOAP_TIMEOUT);
            tFinalConexion = System.currentTimeMillis();

            tInicioRespuesta = System.currentTimeMillis();
            response = port.querySubLifeCycle(request);
            tFinalRespuesta = System.currentTimeMillis();
        } catch (RemoteException | ServiceException e) {
            logger.error(logPrefijo + " Exception: " + e.getMessage(), e);
           /* if (port != null) {
                UtilJava.imprimirXmlError("CBS", port._getCall(), logPrefijo);
            }*/
            throw e;
        } finally {
            if ( port != null) {
                UtilJava.imprimirXml("CBS", port._getCall(), logPrefijo);
            }
            logger.info(logPrefijo + "Tiempo de Respuesta hacia cbs: " + (tFinalRespuesta - tInicioRespuesta) + " ms.");
            logger.info(logPrefijo + "Tiempo de Conexion hacia cbs: " + (tFinalConexion - tInicioConexion) + " ms.");
        }

        return response;
    }

    private QuerySubLifeCycleRequestMsg armarRequest(String prefijo, String linea, String sessionId) {
        QuerySubLifeCycleRequestMsg request = new QuerySubLifeCycleRequestMsg();

        //1
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setVersion(Propiedades.VM_CBS_REQUEST_VERSION);
        requestHeader.setMessageSeq(sessionId);

        //1.1
        OwnershipInfo ownershipInfo = new OwnershipInfo();
        ownershipInfo.setBEID(Propiedades.VM_CBS_REQUEST_BEID);

        //1.2
        SecurityInfo securityInfo = new SecurityInfo();
        securityInfo.setLoginSystemCode(Propiedades.VM_CBS_REQUEST_USUARIO);
        securityInfo.setPassword(Propiedades.VM_CBS_REQUEST_CLAVE);

        requestHeader.setOwnershipInfo(ownershipInfo);
        requestHeader.setAccessSecurity(securityInfo);
        request.setRequestHeader(requestHeader);

        //2
        QuerySubLifeCycleRequest querySubLifeCycleRequest = new QuerySubLifeCycleRequest();

        SubAccessCode subAccessCode = new SubAccessCode();
        subAccessCode.setPrimaryIdentity(Propiedades.VM_CBS_REQUEST_PREFIJO_MSISDN + linea);
        querySubLifeCycleRequest.setSubAccessCode(subAccessCode);
        request.setQuerySubLifeCycleRequest(querySubLifeCycleRequest);
        return request;
    }

    public Calendar obtenerFechaCbs(String prefijo, String linea, String sessionId) throws Exception {
        Calendar fechaFinal = null;
        String fecha = null;
        try {
            QuerySubLifeCycleRequestMsg request = armarRequest(prefijo, linea, sessionId);
            QuerySubLifeCycleResultMsg response = obtenerPeticion(request, prefijo);
            if (response != null && response.getResultHeader() != null) {
                String resultCode = response.getResultHeader().getResultCode();
                if (resultCode != null && codigosAceptados.contains(resultCode)) {
                    logger.debug(prefijo + " El ResultCode: " + resultCode + " es valido");
                    if (response.getQuerySubLifeCycleResult().getLifeCycleStatus() != null) {
                        for (QuerySubLifeCycleResultLifeCycleStatus cycle : response.getQuerySubLifeCycleResult().getLifeCycleStatus()) {
                            if (cycle != null && cycle.getStatusName().equals(Propiedades.VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO)) {
                                fecha = cycle.getStatusExpireTime();
                                break;
                            }
                        }
                    }
                } else {
                    logger.debug(prefijo + "El ResultCode: " + resultCode + " no es valido");
                }
            }

            if (fecha != null) {
                fechaFinal = UtilDate.stringToCalendar(fecha, Propiedades.VM_CBS_EXPIRE_TIME_FORMAT, prefijo);
                if (fechaFinal != null) {
                    logger.debug(prefijo + "La fecha sin conversion es: " + fecha);
                    logger.debug(prefijo + " La fecha convertida es: " + fechaFinal.toString());

                    fecha = UtilDate.dateToTimeString(fechaFinal, Propiedades.VM_CBS_EXPIRE_TIME_FORMAT_STRING, prefijo);
                    logger.debug(prefijo + " La fecha final a devolver es: " + fecha);
                }
            }

        } catch (RemoteException | ServiceException e) {
            logger.error(prefijo + " Exception: " + e.getMessage(), e);
            throw e;
        }

        return fechaFinal;
    }

    public static List<String> cadenaToList(String cadena) {
        List<String> lista = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split(";");
                lista = Arrays.asList(arrayOne);
            }
        } catch (Exception e) {
            logger.error("Error al cargar la lista de Resultados Aceptados: " + cadena, e);
        }
        return lista;
    }

    public List<String> getBilleterasAfectadas() {
        return billeterasAfectadas;
    }

}
