/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.util.Date;

/**
 *
 * @author Vehimar
 */
public class AcumuladoMega {

    private double cuota;
    private Date expiracion;

    public AcumuladoMega() {
    }

    public AcumuladoMega(double cuota, Date expiracion) {
        this.cuota = cuota;
        this.expiracion = expiracion;
    }

    public double getCuota() {
        return cuota;
    }

    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    public Date getExpiracion() {
        return expiracion;
    }

    public void setExpiracion(Date expiracion) {
        this.expiracion = expiracion;
    }
}
