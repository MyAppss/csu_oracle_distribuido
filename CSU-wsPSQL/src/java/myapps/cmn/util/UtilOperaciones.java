/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

import java.util.HashMap;
import myapps.cmn.vo.ReporteConsulta;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class UtilOperaciones {

    private static final Logger LOG = Logger.getLogger(UtilOperaciones.class);

    public static double dividir(double a, int b) {
        double valor = 0;
        try {
            if (b != 0) {
                valor = a / b;
            }
        } catch (Exception e) {
            LOG.error("Error al dividir " + e.getMessage(), e);
        }
        return valor;
    }

    public static String maptoString(HashMap<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, Object> mapa;
        try {
            if (map != null) {
                if (map.containsKey("CONSULTA")) {
                    ReporteConsulta rc = (ReporteConsulta) map.get("CONSULTA");
                    stringBuilder.append(rc.toString());
                } else {
                    mapa = map;
                    mapa.entrySet().forEach((entry) -> {
                        stringBuilder.append(entry.getKey()).append(" = ").append(entry.getValue()).append("; ");
                    });
                }

            }
        } catch (Exception e) {
            LOG.error("Exception: " + e.getMessage(), e);
        }

        return stringBuilder.toString();
    }

}
