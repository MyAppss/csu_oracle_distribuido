package myapps.cmn.util;

import java.io.Serializable;
import java.rmi.Remote;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.apache.ws.security.message.token.UsernameToken;

import com.comverse_in.prepaid.ccws.ServiceLocator;
import com.comverse_in.prepaid.ccws.ServiceSoap_BindingStub;
import com.comverse_in.prepaid.ccws.ServiceSoap_PortType;

/**
 * Clase que se conectara al Servicio Web del Comverse para obtener los datos.
 *
 * @author Vehimar
 */
public class Comverse implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger("Comverse.class");
    private static Map<String, ServiceSoap_BindingStub> ports = new HashMap<String, ServiceSoap_BindingStub>();

    //private static int timeOut = Parametros.comverseWsTimeOut;
    // private static int reintentos = Parametros.comverseWsConexionIntentos;
    public static ServiceSoap_BindingStub getPort(String url, String username, String password) throws Exception {
        // public static synchronized ServiceSoap_BindingStub getPort(String url, String username, String password) throws Exception {
        ServiceSoap_BindingStub port = ports.get(url + username);
        if (port == null) {
            port = initPort(url, username, password);
            //port = getService(url, username, password);
        }
        return port;
    }

    private static ServiceSoap_BindingStub initPort(String url, String username, String password) throws Exception {
        //private static synchronized ServiceSoap_BindingStub initPort(String url, String username, String password) throws Exception {

        ServiceSoap_BindingStub port = ports.get(url + username);

        if (port == null) {
            port = getService(url, username, password);
        }
        return port;
    }

    public static ServiceSoap_BindingStub getService(String url, String username, String password) throws Exception {
        //  public static synchronized ServiceSoap_BindingStub getService(String url, String username, String password) throws Exception {

        LOG.debug("Ingresando a obtener servicio url: " + url + ", user: " + username + ", password: " + password);
        ServiceSoap_BindingStub port = ports.get(url + username);

        if (port == null) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            sb.append("<deployment xmlns=\"http://xml.apache.org/axis/wsdd/\" xmlns:java=\"http://xml.apache.org/axis/wsdd/providers/java\"> \r\n");
            sb.append("<transport name=\"http\" pivot=\"java:org.apache.axis.transport.http.HTTPSender\"/> \r\n");
            sb.append("<globalConfiguration > \r\n");
            sb.append("<requestFlow >\r\n");
            sb.append("<handler type=\"java:org.apache.ws.axis.security.WSDoAllSender\" >\r\n");
            sb.append("</handler>\r\n");
            sb.append("</requestFlow >\r\n");
            sb.append("</globalConfiguration >\r\n");
            sb.append("</deployment>\r\n");
            org.apache.axis.configuration.XMLStringProvider config = new org.apache.axis.configuration.XMLStringProvider(sb.toString());
            org.apache.axis.EngineConfiguration axisconfig = config;

            ServiceLocator locator = new ServiceLocator(axisconfig);
            locator.setServiceSoapEndpointAddress(url);
            Remote remote = locator.getPort(ServiceSoap_PortType.class);
            port = (ServiceSoap_BindingStub) remote;

            port._setProperty(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
            port._setProperty(UsernameToken.PASSWORD_TYPE, WSConstants.PW_DIGEST);
            port.setUsername(username);
            port.setPassword(password);
            port.setTimeout(Integer.parseInt(Propiedades.CCWS_TIME_OUT));
            //port.setTimeout(1000000000);

            port.getVersionId();
            ports.put(url + username, port);

        }
        return port;
    }

    /*public static void main(String[] args) {
        try {
            //DOMConfigurator.configure("etc/log4j.xml");
            //ServiceSoap_BindingStub port = Comverse.getPort("http://172.28.8.9:8084/CCWSV2/ccws.asmx?wsdl", "BSSAPIQA", "Tigo1234*");
            ServiceSoap_BindingStub port = Comverse.getPort("http://127.0.0.1:8084/CCWSV2/ccws.asmx?wsdl", "VSFF", "Tigo1234*");

             //port.retrieveSubscriberWithIdentityNoHistory("77800190" ,"Personal", 1);
        } catch (Exception e) {
        }
    }*/
}
