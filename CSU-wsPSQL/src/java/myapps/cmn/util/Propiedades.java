package myapps.cmn.util;

import java.util.ResourceBundle;
import org.apache.log4j.Logger;

public class Propiedades {

    private static final Logger LOG = Logger.getLogger(Propiedades.class);
    protected final static String CONFIG_FILE = "myapps.properties.CSU";

    public static String BD_SERVER;
    public static String BD_NAME;
    public static int BD_PORT;
    public static final String DB_USER;
    public static final String DB_PASSWORD;
    public static final String USER_WS;
    public static final String PASS_WS;
    public static final String CCWS_TIME_OUT;

    public static final String CCWS_HOST;
    public static final String CCWS_USER;
    public static final String CCWS_PASS;
    public static final String CCWS_MODO_PRUEBA;
    //public static final List<String> ListaCodesErrorCMV;
    public static final String DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS;
    public static final String DB_MAX_ACTIVE;
    public static final String DB_MIN_IDLE;
    public static final String DB_MAX_IDLE;
    public static final String DB_MAX_WAIT;
    public static final String BILLETERA;

    public static final String DPI_CONNET_TIME_OUT;
    public static final String DPI_READ_TIMEOUT;

    public static final String DEFAULT_SERVICIO;
    public static final String DEFAULT_CANAL;

    public static final String STRING_CODE_ERRORS;

    public static final String GQL_ENDPOINT;
    public static final String GQL_MESSAGE;
    public static final String GQL_DATEFORMAT;
    public static final Integer GQL_TIMEOUT;
    public static final boolean GQL_BANDERA;
    public static final String GQL_FECHAMINIMA;
    public static final Double GQL_BOB;
    public static final Double GQL_KB;
    public static final Double GQL_PrecioKB;
    public static final Double GQL_Item;
    public static final Integer GQL_Decimales;
    public static final String GQL_NoFound;
    public static final String GQL_TYPE;
    public static final String GQL_CPACTIVO;
    public static final String GQL_CPSUSPENDIDO;
    public static final boolean GQL_IMPRIMIR_BILLETERAS;
    public static final String GQL_NOMBRE_RESERVA;
    public static final String GQL_FORMATOFECHA_IB;

    public static String MQ_USER;
    public static String MQ_PASSWORD;
    public static String MQ_COLA;
    public static String MQ_IP;
    public static int MQ_PUERTO;
    public static String MQ_TIPO_CONEXION;
    public static int MQ_TIMEOUT;
    public static int MQ_HILOS;
    public static long MQ_HILOS_SLEEP;
    public static int MQ_MAX_THREAD;

    public static boolean BANDERA_BILLETERAS_SIMPLES_MONTO_MINIMO;
    public static boolean BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO1;
    public static boolean BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO2;
    public static String UNIT_TYPE_MONTO_MINIMO;

    public static String PATTERN_TELEFONO;
    public static String PATTERN_CHANNEL;
    public static String PATTERN_SHORT;

    public static boolean BANDERA_MOSTRAR_INFORMACION;

    public static String VM_CBS_SOAP_SERVIDOR;
    public static int VM_CBS_SOAP_TIMEOUT;
    public static String VM_CBS_REQUEST_VERSION;
    public static String VM_CBS_REQUEST_BEID;
    public static String VM_CBS_REQUEST_USUARIO;
    public static String VM_CBS_REQUEST_CLAVE;
    public static String VM_CBS_REQUEST_PREFIJO_MSISDN;

    public static boolean VM_BANDERA;
    public static String VM_CBS_RESPONSE_CODIGOS_ACEPTADOS;
    public static String VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO;
    public static String VM_CBS_EXPIRE_TIME_FORMAT;
    public static String VM_CBS_EXPIRE_TIME_FORMAT_STRING;

    public static String VM_CBS_BILLETERA_MODIFICAR;

    static {
        ResourceBundle rb = ResourceBundle.getBundle(CONFIG_FILE);
        BD_SERVER = rb.getString("bd.servidor");
        BD_NAME = rb.getString("bd.nombre");
        BD_PORT = Integer.parseInt(rb.getString("bd.puerto"));
        DB_USER = rb.getString("bd.usuario");
        DB_PASSWORD = rb.getString("bd.contrasenna");

        USER_WS = rb.getString("WS.usuario");
        PASS_WS = rb.getString("WS.contrasenia");

        CCWS_HOST = rb.getString("ccws.servidor");
        CCWS_USER = rb.getString("ccws.usuario");
        CCWS_PASS = rb.getString("ccws.contrasenna");
        CCWS_MODO_PRUEBA = rb.getString("ccws.modoprueba");
        CCWS_TIME_OUT = rb.getString("ccws.timeout");

        BILLETERA = rb.getString("ws.billetera");

        DPI_CONNET_TIME_OUT = rb.getString("dpi.connect.timeout");
        DPI_READ_TIMEOUT = rb.getString("dpi.read.timeout");

        STRING_CODE_ERRORS = "4000,4004,4005";
        /*CODIGOS DE ERROR Q GENERA COMVERSE (Aqui NO SE INCLUYE EL ERROR 4006 QUE SALE..
        #..CUANDO EL USUARIO ES POSTPAGO, YA Q ESTE CODE YA ESTA DEFINIDO 
        codes.error.comverse = 4000,4004,4005*/

        DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = rb.getString("bd.setminevictableidletimemillis");
        DB_MAX_ACTIVE = rb.getString("bd.setmaxactive");
        DB_MIN_IDLE = rb.getString("bd.setminidle");
        DB_MAX_IDLE = rb.getString("bd.setMaxIdle");
        DB_MAX_WAIT = rb.getString("bd.setMaxWait");

        /* ListaCodesErrorCMV = new ArrayList<>();
        for (String code : STRING_CODE_ERRORS.split(",")) {
            ListaCodesErrorCMV.add(code);
        }*/
        DEFAULT_SERVICIO = rb.getString("ws.Default.IdServicio");
        DEFAULT_CANAL = rb.getString("ws.Default.canal");

        GQL_ENDPOINT = rb.getString("GQL.ENDPOINT");
        GQL_MESSAGE = rb.getString("GQL.MESSAGE");
        GQL_DATEFORMAT = rb.getString("GQL.DATEFORMAT");
        GQL_TIMEOUT = Integer.parseInt(rb.getString("GQL.TIMEOUT"));
        GQL_BANDERA = Boolean.parseBoolean(rb.getString("GQL.BANDERA"));
        GQL_FECHAMINIMA = rb.getString("GQL.FECHAMINIMA");
        GQL_BOB = Double.parseDouble(rb.getString("GQL.BOB"));
        GQL_KB = Double.parseDouble(rb.getString("GQL.KB"));
        GQL_PrecioKB = Double.parseDouble(rb.getString("GQL.PrecioKB"));
        GQL_Item = Double.parseDouble(rb.getString("GQL.Item"));
        GQL_Decimales = Integer.parseInt(rb.getString("GQL.Decimales"));
        GQL_NoFound = rb.getString("GQL.NOFOUND");
        GQL_TYPE = rb.getString("GQL.TYPE");
        GQL_CPACTIVO = rb.getString("GQL.ChildProductACTIVO");
        GQL_CPSUSPENDIDO = rb.getString("GQL.ChildProductSUSPENDIDO");
        GQL_IMPRIMIR_BILLETERAS = Boolean.parseBoolean(rb.getString("GQL.imprimirBilleteras"));
        GQL_NOMBRE_RESERVA = rb.getString("GQL.RESERVA");
        GQL_FORMATOFECHA_IB = rb.getString("GQL.FechaimprimirBilleteras");

        MQ_USER = rb.getString("MQ.user");
        MQ_PASSWORD = rb.getString("MQ.password");
        MQ_COLA = rb.getString("MQ.nombreCola");
        MQ_IP = rb.getString("MQ.servidorIP");
        MQ_PUERTO = Integer.parseInt(rb.getString("MQ.servidorPuerto"));
        MQ_TIPO_CONEXION = rb.getString("MQ.tipoConexion");
        MQ_TIMEOUT = Integer.parseInt(rb.getString("MQ.timeout"));
        MQ_HILOS = Integer.parseInt(rb.getString("MQ.hilos"));
        MQ_HILOS_SLEEP = Long.parseLong(rb.getString("MQ.hilos.sleep"));
        MQ_MAX_THREAD = Integer.parseInt(rb.getString("MQ.maxThread"));

        BANDERA_BILLETERAS_SIMPLES_MONTO_MINIMO = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.billeteras.simples"));
        BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO1 = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.billeteras.compuestas1"));
        BANDERA_BILLETERAS_COMPUESTAS_MONTO_MINIMO2 = Boolean.parseBoolean(rb.getString("bandera.montoMinimo.billeteras.compuestas2"));
        UNIT_TYPE_MONTO_MINIMO = rb.getString("unitType.montoMinimo");

        PATTERN_TELEFONO = rb.getString("PATTERN.TELEFONO");
        PATTERN_CHANNEL = rb.getString("PATTERN.CHANNEL");
        PATTERN_SHORT = rb.getString("PATTERN.SHORT");

        BANDERA_MOSTRAR_INFORMACION = Boolean.parseBoolean(rb.getString("Bandera.Mostrar.Informacion"));

        VM_BANDERA = Boolean.parseBoolean(rb.getString("VM.CBS.BANDERA"));
        if (VM_BANDERA) {
            VM_CBS_SOAP_SERVIDOR = rb.getString("VM.SOAP.CBS.SERVIDOR");
            VM_CBS_SOAP_TIMEOUT = Integer.parseInt(rb.getString("VM.SOAP.CBS.TIMEOUT"));
            VM_CBS_REQUEST_VERSION = rb.getString("VM.SOAP.CBS.REQUEST.VERSION");
            VM_CBS_REQUEST_BEID = rb.getString("VM.SOAP.CBS.REQUEST.BEID");
            VM_CBS_REQUEST_USUARIO = rb.getString("VM.SOAP.CBS.REQUEST.LOGIN");
            VM_CBS_REQUEST_CLAVE = rb.getString("VM.SOAP.CBS.REQUEST.PASSWORD");
            VM_CBS_REQUEST_PREFIJO_MSISDN = rb.getString("VM.SOAP.CBS.REQUEST.PREFIJO.MSISDN");

            VM_CBS_RESPONSE_CODIGOS_ACEPTADOS = rb.getString("VM.SOAP.CBS.CODIGOS.ACEPTADOS");
            VM_CBS_LIFE_CYCLE_STATUS_SELECCIONADO = rb.getString("VM.CBS.LIFE.CYCLE.STATUS.NAME");
            VM_CBS_EXPIRE_TIME_FORMAT = rb.getString("VM.CBS.EXPIRE.TIME.FORMAT");
            VM_CBS_EXPIRE_TIME_FORMAT_STRING = rb.getString("VM.CBS.EXPIRE.TIME.FORMAT.STRING");
            VM_CBS_BILLETERA_MODIFICAR = rb.getString("VM.CBS.BILLETERA.MODIFICAR");
        }

    }

}
