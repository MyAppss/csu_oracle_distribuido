package myapps.cmn.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.sql.Time;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class UtilDate {

    private static final Logger LOG = Logger.getLogger(UtilDate.class);

    public static Timestamp dateToTimestamp(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = formatter.format(fecha);
        return stringToTimestamp(str);
    }

    public static Calendar stringToCalendar(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error("stringToCalendar " + e.getMessage(), e);
        }
        return cal;
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = null;
        try {
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (Exception e) {
            LOG.error("dateToCalendar " + e.getMessage(), e);
        }
        return cal;
    }

    public static Calendar stringToCalendarPM_AM(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error("stringToCalendarPM_AM " + e.getMessage(), e);
        }
        return cal;
    }

    public static Timestamp stringToTimestamp(String fecha) {
        Calendar date = stringToCalendar(fecha);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Timestamp stringToTimestampPM_AM(String fecha) {
        Calendar date = stringToCalendarPM_AM(fecha);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Date stringToDate(String fecha) {
        fecha = fecha + " 00:00:00";
        Calendar date = stringToCalendar(fecha);
        Date dat = new Date();
        if (date != null) {
            dat = new Date(date.getTimeInMillis());
        }
        return dat;
    }

    public static Time stringToTime(String fecha) {
        fecha = fecha + ":00";
        Time t = Time.valueOf(fecha);
        return t;
    }

    public static String dateToString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Timestamp dateToTimetampSW_IF(Date fecha, boolean swIni) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String str = formatter.format(fecha);
        if (swIni) {
            str = str + " 00:00:00";
        } else {
            str = str + " 23:59:59";
        }

        return stringToTimestamp(str);
    }

    public static Timestamp getTimetampActual() {
        /*Date dd = new Date();
        String str = dd.getTime() + "";
        str = str.substring(0, 10);
        str = str + "000";
        long tt = Long.parseLong(str);
        Timestamp t = new Timestamp(tt);
         */
        long tt = Calendar.getInstance().getTimeInMillis();
        Timestamp t = new Timestamp(tt);
        return t;
    }

    public static String timetampToString(Timestamp fecha) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static String dateToTimeString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("HH:mm");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static String dateToTimeString(Calendar fecha) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");//("yyyy-MM-dd");
            // 01/01/0001 00:00:00
            return formatter.format(fecha.getTime());
        } catch (Exception e) {
            LOG.error("dateToTimeString " + e.getMessage(), e);
            return null;
        }
    }

    public static Date unirTwoDate(Date day, Date time) {

        String str1 = UtilDate.dateToString(day);

        String str = UtilDate.dateToTimeString(time);
        str = str1 + " " + str;
        return UtilDate.stringToTimestamp(str);
    }

    public static boolean intervalo(Date mI, Date mF, Date fecha) {
        if (fecha.getTime() == mI.getTime()) {
            return true;
        }

        if (fecha.getTime() == mF.getTime()) {
            return true;
        }

        if (fecha.after(mI) && (fecha.before(mF))) {
            return true;
        } else {
            return false;
        }
    }

    public static String dateToTimeString(Calendar fecha, String format, String prefijo) {
        try {
            DateFormat formatter = new SimpleDateFormat(format);
            return formatter.format(fecha.getTime());
        } catch (Exception e) {
            LOG.error(prefijo + "dateToTimeString " + e.getMessage(), e);
            return null;
        }
    }
    
        public static Calendar stringToCalendar(String fecha, String format, String prefijo) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat(format);
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            LOG.error(prefijo + " stringToCalendar " + e.getMessage(), e);
        }
        return cal;
    }
}
