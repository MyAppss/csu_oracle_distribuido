package myapps.cmn.util;

import com.comverse_in.prepaid.ccws.AccumulatorEntity;
import com.comverse_in.prepaid.ccws.BalanceEntity;
import com.comverse_in.prepaid.ccws.ServiceSoap_BindingStub;
import com.comverse_in.prepaid.ccws.SubscribedOffer;
import com.comverse_in.prepaid.ccws.SubscriberRetrieve;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.vo.Acumulador;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.ResponseCCWS;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class MethodsCCWS {

    private static final Logger LOG = Logger.getLogger(MethodsCCWS.class);

    /*    
     * ResponseCCWS Atributos String errorDescription
     * List<WalletComverse> listWallet   
     */
    //-------------------------------------------------------------------------- 
    public static ResponseCCWS getListBalanceWallet(String clientNumber, String logPrefijo) {

        LOG.debug(logPrefijo + "[getListBalanceWallet]|ISDN=" + clientNumber + "|COMVERSE: Inicio de consulta para obtener billeteras");
        ResponseCCWS response = null;
        List<WalletComverse> list = null;
        List<Offer> listOffer = null;
        List<Acumulador> listAcumulador = null;
        ServiceSoap_BindingStub axisPort = null;
        try {
            LOG.debug(logPrefijo + "Iniciando metodo getlistBalanceWallets de COMVERSE para NroCliente=" + clientNumber);
            long tiempoInicial = System.currentTimeMillis();
            axisPort = Comverse.getPort(Propiedades.CCWS_HOST, Propiedades.CCWS_USER, Propiedades.CCWS_PASS);
            axisPort.setTimeout(Integer.parseInt(Propiedades.CCWS_TIME_OUT));
            SubscriberRetrieve sub = axisPort.retrieveSubscriberWithIdentityNoHistory(clientNumber, null, 1);
            LOG.info("...:::" + logPrefijo + "RESPONSE GENERAL OBTENIDO EN: " + (System.currentTimeMillis() - tiempoInicial) + " milisegundos :::...");

            //validar sub || getBalances
            if (sub != null && sub.getSubscriberData() != null && sub.getSubscriberData().getBalances() != null) {

                BalanceEntity[] balances = sub.getSubscriberData().getBalances();
                WalletComverse wallet = null;
                response = new ResponseCCWS();
                list = new ArrayList<WalletComverse>();
                for (int i = 0; i < balances.length; i++) {
                    wallet = new WalletComverse();
                    wallet.setNameWallet(balances[i].getBalanceName());
                    wallet.setAvailableBalance(balances[i].getAvailableBalance());
                    wallet.setExpiration(balances[i].getAccountExpiration());
                    //balances[i].get
                    wallet.setUsada(false);
                    //balances[i].getA
                    LOG.debug(logPrefijo + "Recuperado> wallet de COMVERSE para Nro " + clientNumber + ": " + wallet.getNameWallet() + " valor=" + wallet.getAvailableBalance());

                    list.add(wallet);
                    wallet = null;
                }

                SubscribedOffer[] listOffert = sub.getSubscriberData().getOffer();
                listOffer = new ArrayList<Offer>();

                if (listOffert != null) {

                    for (int i = 0; i < listOffert.length; i++) {

                        if (listOffert[i].getState() != null && listOffert[i].getState().getValue().equalsIgnoreCase("ENABLED")) {
                            Offer offer = new Offer();
                            offer.setName(listOffert[i].getName());
                            offer.setServiceStart(listOffert[i].getServiceStart());
                            offer.setServiceEnd(listOffert[i].getServiceEnd());
                            offer.setState(listOffert[i].getState().getValue());
                            offer.setInstantiationTime(listOffert[i].getInstantiationTime());
                            offer.setUsada(false);

                            LOG.debug(logPrefijo + "Recuperado> offert de COMVERSE para Nro " + clientNumber + ": " + offer.getName() + " valor=" + offer.getState());
                            listOffer.add(offer);
                        }
                    }
                }

                AccumulatorEntity[] listAccumulator = sub.getSubscriberData().getAccumulator();
                listAcumulador = new ArrayList<Acumulador>();

                if (listAccumulator != null) {

                    for (int i = 0; i < listAccumulator.length; i++) {
                        Acumulador acumulador = new Acumulador();
                        acumulador.setAccumulatorName(listAccumulator[i].getAccumulatorName());
                        acumulador.setAmount(Double.valueOf(listAccumulator[i].getAmount()));

                        LOG.debug(logPrefijo + "Recuperado> accumulator de COMVERSE para Nro " + clientNumber + ": " + acumulador.getAccumulatorName() + " valor=" + acumulador.getAmount());
                        listAcumulador.add(acumulador);
                    }
                }

                response.setErrorDescription("TransactionCorrect");
                response.setListWallet(list);
                response.setListOffer(listOffer);
                response.setListAcumulador(listAcumulador);
                response.setNameCOS(sub.getSubscriberData().getCOSName());
                LOG.debug(logPrefijo + "[getListBalanceWallet]|ISDN=" + clientNumber + "|COS: " + response.getNameCOS() + "|COMVERSE: Se recupera listaBilleteras con Size=" + list.size());

            } else {
                LOG.warn(logPrefijo+ "La respuesta del servicio no obtuvo balances");
                throw new Exception(logPrefijo + "La Consulta de billeteras retornno un valor Nulo...");
            }
            
            
        } catch (RuntimeException e) {
            String error = e.getMessage();
            if (error != null) {
                response = new ResponseCCWS();
                response.setNameCOS("ERROR");
                if (error.contains("Subscriber not found")) {
                    response.setErrorDescription("Subscriber not found");
                    response.setListWallet(null);
                } else {
                    //comprobamos si en la descripcion del error esta alguno de los 
                    //codigo de comverse definidos desde properties.
                    if (ContainCodeError(response.getErrorDescription())) {
                        response.setErrorDescription("Error In ListErrors");
                        response.setListWallet(null);
                    } else {
                        response.setErrorDescription("ConnectionError");
                        response.setListWallet(null);
                    }
                }
            } else {
                if (response == null) {
                    response = new ResponseCCWS();
                }
                if (response != null) {
                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);
                }
            }
            LOG.error("[getListBalanceWallet] Error: " + error, e);
            /*  if (axisPort != null) {
                UtilJava.imprimirXmlError("CCWS", axisPort._getCall(), logPrefijo);
            }*/
        } catch (Exception e) {
            String error = e.getMessage();
            if (error != null) {
                response = new ResponseCCWS();
                response.setNameCOS("ERROR");
                if (error.contains("Subscriber not found")) {
                    response.setErrorDescription("Subscriber not found");
                    response.setListWallet(null);
                } else {
                    //comprobamos si en la descripcion del error esta alguno de los 
                    //codigo de comverse definidos desde properties.
                    if (ContainCodeError(response.getErrorDescription())) {
                        response.setErrorDescription("Error In ListErrors");
                        response.setListWallet(null);
                    } else {
                        response.setErrorDescription("ConnectionError");
                        response.setListWallet(null);
                    }
                }
            } else {
                if (response == null) {
                    response = new ResponseCCWS();
                }
                if (response != null) {
                    response.setErrorDescription("ConnectionError");
                    response.setListWallet(null);
                }

            }
            LOG.error(logPrefijo + "Error al consultar billeteras: " + e.getMessage(), e);
            /* if (axisPort != null) {
                UtilJava.imprimirXmlError("CCWS", axisPort._getCall(), logPrefijo);
            }*/
        } finally {
            if (axisPort != null) {
                UtilJava.imprimirXml("CCWS", axisPort._getCall(), logPrefijo);
            }
        }

        return response;
    }
    //-------------------------------------------------------------------------- 

    public static ResponseCCWS getListBalanceWalletSimular(String clientNumber) {
        LOG.debug("[getListBalanceWalletSimular] Iniciando..");
        ResponseCCWS RCWS = new ResponseCCWS();
        //String message="Subscriber not found";
        String message = "TransactionCorrect";
        //String message="Error In ListErrors";
        //String message="Error sadfasdfasdfasdf";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 1);

        List<WalletComverse> response = new LinkedList<WalletComverse>();
        WalletComverse w1 = new WalletComverse();
        w1.setNameWallet("CORE BALANCE");
        w1.setUsada(false);
        //w1.setAvailableBalance(0.4905901234);
        w1.setAvailableBalance(12);
        w1.setExpiration(c);
        response.add(w1);

        WalletComverse w2 = new WalletComverse();
        w2.setNameWallet("CORE_BALANCE_2");
        w2.setAvailableBalance(23);
        w2.setExpiration(c);
        w2.setUsada(false);
        response.add(w2);

        WalletComverse w3 = new WalletComverse();
        w3.setNameWallet("SMS_ICX");
        w3.setAvailableBalance(20);
        w3.setExpiration(c);
        w3.setUsada(false);
        response.add(w3);

        WalletComverse w4 = new WalletComverse();
        w4.setNameWallet("MMS");
        w4.setAvailableBalance(200);
        w4.setExpiration(c);
        w4.setUsada(false);
        response.add(w4);

        WalletComverse w5 = new WalletComverse();
        w5.setNameWallet("Broadband_Currency");
        w5.setAvailableBalance(34);
        w5.setExpiration(c);
        w5.setUsada(false);
        response.add(w5);

        WalletComverse w6 = new WalletComverse();
        w6.setNameWallet("Blackberry");
        w6.setAvailableBalance(8.5);
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, 2);
        w6.setExpiration(c1);
        w6.setUsada(false);
        response.add(w6);

        WalletComverse w7 = new WalletComverse();
        w7.setNameWallet("SMS_PROMO_CARGA");
        w7.setAvailableBalance(150);
        w7.setExpiration(c);
        w7.setUsada(false);
        response.add(w7);

        WalletComverse w8 = new WalletComverse();
        w8.setNameWallet("SMS_TEMPORAL");
        w8.setAvailableBalance(20);
        w8.setExpiration(c);
        w8.setUsada(false);
        response.add(w8);

        WalletComverse w9 = new WalletComverse();
        w9.setNameWallet("PROMO_SMS_TEMP");
        w9.setAvailableBalance(3);
        w9.setExpiration(c);
        w9.setUsada(false);
        response.add(w9);

        WalletComverse w10 = new WalletComverse();
        w10.setNameWallet("Productos_VAS");
        w10.setExpiration(c);
        w10.setUsada(false);
        response.add(w10);

        WalletComverse w11 = new WalletComverse();
        w11.setNameWallet("SALDO_PROMO_CARGA");
        w11.setExpiration(c);
        w11.setUsada(false);
        response.add(w11);

        WalletComverse w12 = new WalletComverse();
        w12.setNameWallet("VOZ_SMS");
        w12.setExpiration(c);
        w12.setUsada(false);
        response.add(w12);

        WalletComverse w13 = new WalletComverse();
        w13.setNameWallet("LLAM_MOV_LOC");
        w13.setExpiration(c);
        w13.setAvailableBalance(20);
        w13.setUsada(false);
        response.add(w13);

        WalletComverse w14 = new WalletComverse();
        w14.setNameWallet("PROMO_SMS_ICX");
        w14.setExpiration(c);
        w14.setUsada(false);
        response.add(w14);

        WalletComverse w15 = new WalletComverse();
        w15.setNameWallet("GPRS_PROMO SALDO");
        w15.setExpiration(c);
        w15.setUsada(false);
        response.add(w15);

        WalletComverse w16 = new WalletComverse();
        w16.setNameWallet("PROMO_SMSP2P_XTREMO");
        w16.setExpiration(c);
        w16.setUsada(false);
        response.add(w16);

        WalletComverse w17 = new WalletComverse();
        w17.setNameWallet("PROMO_ANTICHURN");
        w17.setExpiration(c);
        w17.setUsada(false);
        response.add(w17);

        WalletComverse w18 = new WalletComverse();
        w18.setNameWallet("PROMO_LDI");
        w18.setExpiration(c);
        w18.setUsada(false);
        response.add(w18);

        WalletComverse w19 = new WalletComverse();
        w19.setNameWallet("GPRS_SUBSCRIPTIONS");
        w19.setAvailableBalance(50);
        w19.setExpiration(c);
        w19.setUsada(false);
        response.add(w19);

        WalletComverse w20 = new WalletComverse();
        w20.setNameWallet("LLAMADA_SEGUNDOS");
        w20.setAvailableBalance(130);
        w20.setExpiration(c);
        w20.setUsada(false);
        response.add(w20);

        WalletComverse w21 = new WalletComverse();
        w21.setNameWallet("LLAMADA_MINUTOS");
        w21.setAvailableBalance(20);
        w21.setExpiration(c);
        w21.setUsada(false);
        response.add(w21);

        RCWS.setErrorDescription(message);
        RCWS.setListWallet(response);
//        RCWS.setNameCOS("PRE_PAGO");
        RCWS.setNameCOS("PREPAGO");
//        RCWS.setNameCOS("FACTURA_FIJA");
        LOG.debug("[getListBalanceWalletSimular] Finalizando..");

        List<Offer> listOferta = new ArrayList<Offer>();
        List<Acumulador> listAcumulador = new ArrayList<Acumulador>();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        Offer offert = new Offer();
        offert.setName("MINUTOS_LIBRES");
        offert.setState("ENABLED");
        offert.setServiceEnd(cal);
        offert.setUsada(false);

        Offer offert1 = new Offer();
        offert1.setName("INTERNET_TOTAL");
        offert1.setState("ENABLED");
        offert1.setServiceEnd(cal);
        offert1.setUsada(false);

        listOferta.add(offert);
        listOferta.add(offert1);

        Acumulador acum = new Acumulador();
        acum.setAccumulatorName("ACUM_MINUTOS_LIBRES");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_A");
        acum.setAmount(10);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUM_INTERNET_TOTAL");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_B");
        acum.setAmount(20);
        listAcumulador.add(acum);

        acum = new Acumulador();
        acum.setAccumulatorName("ACUMULADOR_C");
        acum.setAmount(30);

        listAcumulador.add(acum);

        RCWS.setListOffer(listOferta);
        RCWS.setListAcumulador(listAcumulador);

        return RCWS;
        //return null;
    }
    //*************************************************************************
//     public static String getCOS_simular(String numberClient) {
//         return "ERROR";
//     }       

    /* public static boolean ContainCodeError(String DescriptionError) {
        int i = 0;
        try {
            if (DescriptionError != null) {
                List<String> listErrors = Propiedades.ListaCodesErrorCMV;
                while (i < listErrors.size()) {
                    String AuxError = listErrors.get(i);
                    if (DescriptionError.contains(AuxError)) {
                        return true;
                    }
                    i++;
                }
            }
        } catch (Exception e) {
            LOG.error("[ContainCodeError] verificando si contiene error Code :" + e.getMessage(), e);
        }
        return false;
    }*/
    public static boolean ContainCodeError(String DescriptionError) {
        int i = 0;
        try {
            if (DescriptionError != null) {
                String[] listErrors = Propiedades.STRING_CODE_ERRORS.split(",");
                while (i < listErrors.length) {
                    String AuxError = listErrors[i];
                    if (DescriptionError.contains(AuxError)) {
                        return true;
                    }
                    i++;
                }
            }
        } catch (Exception e) {
            LOG.error("[ContainCodeError] verificando si contiene error Code :" + e.getMessage(), e);
        }
        return false;
    }

}
