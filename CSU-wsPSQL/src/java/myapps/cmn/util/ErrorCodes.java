/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.util;

/**
 *
 * @author Vehimar
 */
public class ErrorCodes {

    public static final int CODE_ERROR_LOGIN_WS = 1;
    public static final String MSG_ERROR_LOGIN_WS = "Error : Usuario y/o password No válidos";
    public static final int CODE_ERROR_LOAD_PROPER = 2;
    public static final String MSG_ERROR_LOAD_PROPER = "Error al cargar parametros";
    public static final int CODE_ERROR_MSG_ERROR = 3;
    public static final String MSG_ERROR_MSG_ERROR = "Error al intentar obtener mensaje";
    public static final int CODE_ERROR_CONEX_BD = 10;
    public static final String MSG_ERROR_CONEX_BD = "Error al intentar conectar a BD";
    public static final int CODE_ERROR_BD_GUIA = 11;
    public static final String MSG_ERROR_BD_GUIA = "Error al consultar tabla Guia de BD";
    public static final int CODE_ERROR_BD_BILLETERA = 12;
    public static final String MSG_ERROR_BD_BILLETERA = "Error al consultar tabla Billetera de BD";
    public static final int CODE_ERROR_BD_quota = 13;
    public static final String MSG_ERROR_BD_quota = "Error al consultar tabla QUOTA de BD";
    public static final int CODE_ERROR_COMVERSE_CONEX = 20;
    public static final String MSG_ERROR_CONVERSE_CONEX = "Error al conectar con Comverse";
    public static final int CODE_ERROR_COMVERSE_RESPONSE = 21;
    public static final String MSG_ERROR_CONVERSE_RESPONSE = "Error: Comverse no responde ";
    public static final int CODE_ERROR_COMVERSE_VERIFICAR = 22;
    public static final String MSG_ERROR_CONVERSE_VERIFICAR = "Error: no se puede "
            + "verificar estado Comverse. Error al leer tabla Configuracion";
    //--------------------------------------------------------------------------
    public static final int CODE_ERROR_USUARIO_NO_COMVERSE = 30;
    public static final String MSG_ERROR_USUARIO_NO_COMVERSE = "Usuario no encontrado en CCWS(Comverse)";

}
