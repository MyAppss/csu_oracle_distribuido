/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.Billetera;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class billeteraDAO {

    private static final Logger LOG = Logger.getLogger(billeteraDAO.class);

    public static List<Billetera> getBilleterasByConfigId(int configId /*, Connection conn*/) {

        List<Billetera> result = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "select\n"
                    + "DISTINCT B.BILLETERA_ID as billetera_id,\n"
                    + "B.CANTIDAD_DECIMALES as cantidad_decimales,\n"
                    + "B.ESTADO as estado,\n"
                    + "B.MONTO_MAXIMO as monto_maximo,\n"
                    + "B.MONTO_MINIMO as monto_minimo,\n"
                    + "B.NOMBRE_COMERCIAL as nombre_comercial,\n"
                    + "B.NOMBRE_COMVERSE as nombre_comverse,\n"
                    + "B.OPERADOR as operador,\n"
                    + "B.PREFIJO_UNIDAD as prefijo_unidad,\n"
                    + "B.UNIT_TYPE_ID as unit_type_id,\n"
                    + "B.VALOR as valor,\n"
                    + "B.ALCO as alco,\n"
                    + "B.ACUMULADOR_ID as acumulador_id,\n"
                    + "A.NOMBRE as nombre_acumulador,\n"
                    + "A.LIMITE as limite_acumulador,\n"
                    + "Cb.NOMBRE_COMERCIAL as config_nombre_comercial,\n"
                    + "CB.MOSTRAR_SALDO_CERO as config_mostrar_saldo_cero,\n"
                    + "CB.NO_MOSTRAR_SALDO_EXPIRADO as no_mostrar_saldo_expirado,\n"
                    + "CB.MOSTRAR_SALDO_MAYOR_CERO as mostrar_saldo_mayor_cero,\n"
                    + "CB.MOSTRAR_SALDO_MENOR_CERO as mostrar_saldo_menor_cero,\n"
                    + "CB.MOSTRAR_SIEMPRE as mostrar_siempre,\n"
                    + "CB.MOSTRAR_VIGENCIA as mostrar_vigencia,\n"
                    + "CB.MOSTRAR_SALDO_EXPIRADO as mostrar_saldo_expirado,\n"
                    + "CB.POSICION as posicion,\n"
                    + "CB.MOSTRAR_SEGUNDA_FECHA_EXP as mostrar_segunda_fecha_exp,\n"
                    + "CB.MOSTRAR_HORA_SEGUNDA_FECHA_EXP as mostrar_hr_segunda_fecha_exp,\n"
                    + "CB.ASUMIR_FORMAT_HR_PRIMERA_FECHA as asumir_formt_hr_primera_fecha\n"
                    + "from BILLETERA B inner join CONFIG_BILLETERA CB on B.BILLETERA_ID = CB.BILLETERA_ID\n"
                    + "and B.ESTADO='t' and CB.ESTADO='t' and CB.CONFIG_ID=? left join ACUMULADOR A on B.ACUMULADOR_ID = A.ACUMULADOR_ID\n"
                    + "and B.ESTADO='t' and CB.ESTADO='t' and A.ESTADO='t' ORDER BY POSICION ASC";
            conn = ConexionBD.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        Billetera myBilletera = new Billetera();
                        myBilletera.setBilleteraId(rs.getInt("billetera_id"));
                        myBilletera.setEstado(rs.getString("estado"));
                        myBilletera.setMontoMaximo(rs.getDouble("monto_maximo"));
                        myBilletera.setMontoMinimo(rs.getDouble("monto_minimo"));
                        myBilletera.setNombreComercial(rs.getString("nombre_comercial"));
                        myBilletera.setNombreComverse(rs.getString("nombre_comverse"));
                        myBilletera.setOperador(rs.getString("operador"));
                        myBilletera.setPrefijoUnidad(rs.getString("prefijo_unidad"));
                        myBilletera.setUnitTypeId(rs.getInt("unit_type_id"));
                        myBilletera.setValor(rs.getDouble("valor"));

                        myBilletera.setAlco(rs.getString("alco"));
                        myBilletera.setNombreAcumulador(rs.getString("nombre_acumulador"));
                        myBilletera.setLimiteAcumulador(rs.getInt("limite_acumulador"));

                        myBilletera.setCantidadDecimales(rs.getInt("cantidad_decimales"));
                        myBilletera.setConfigBilletera_nombreComercial(rs.getString("config_nombre_comercial"));
                        myBilletera.setConfigBilletera_mostrar_saldo_cero(rs.getString("config_mostrar_saldo_cero"));
                        myBilletera.setConfigBilletera_no_mostrar_saldo_expirado(rs.getString("no_mostrar_saldo_expirado"));
                        myBilletera.setConfigBilletera_mostrar_saldo_mayor_cero(rs.getString("mostrar_saldo_mayor_cero"));
                        myBilletera.setConfigBilletera_mostrar_saldo_menor_cero(rs.getString("mostrar_saldo_menor_cero"));
                        myBilletera.setConfigBilletera_mostrar_siempre(rs.getString("mostrar_siempre"));
                        myBilletera.setConfigBilletera_mostrar_vigencia(rs.getString("mostrar_vigencia"));
                        myBilletera.setMostrarSaldoExpirado(rs.getString("mostrar_saldo_expirado"));
                        /////////////////////////////////////////////////////////////////////////////
                        myBilletera.setConfigBilletera_mostrar_segunda_fecha_exp(rs.getString("mostrar_segunda_fecha_exp"));
                        myBilletera.setConfigBilletera_mostrar_hora_segunda_fecha_exp(rs.getString("mostrar_hr_segunda_fecha_exp"));
                        myBilletera.setConfigBilletera_asumir_formato_hora_primera_fecha(rs.getString("asumir_formt_hr_primera_fecha"));
                        /////////////////////////////////////////////////////////////////////////////
                        result.add(myBilletera);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[getBilleteras] Error al intentar obtener la lista de "
                    + "billeteras Cos id " + configId + " "
                    + "SQLException:" + e.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de billeteras por Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de billeteras por "
                            + "Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de billeteras por "
                            + "Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }

        }
        return result;
    }

    public static List<Billetera> getBilleterasByComposicionBilleteraId(int composicionBilleteraId /*, Connection conn*/) {
        List<Billetera> result = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            String sql = "select\n"
                    + "  DISTINCT\n"
                    + "  B.BILLETERA_ID       as billetera_id,\n"
                    + "  B.CANTIDAD_DECIMALES as cantidad_decimales,\n"
                    + "  B.ALCO               as alco,\n"
                    + "  B.ACUMULADOR_ID      as acumulador_id,\n"
                    + "  A.NOMBRE             as nombre_acumulador,\n"
                    + "  A.LIMITE             as limite_acumulador,\n"
                    + "  B.ESTADO             as estado,\n"
                    + "  B.MONTO_MAXIMO       as monto_maximo,\n"
                    + "  B.MONTO_MINIMO       as monto_minimo,\n"
                    + "  B.NOMBRE_COMERCIAL   as nombre_comercial,\n"
                    + "  B.NOMBRE_COMVERSE    as nombre_comverse,\n"
                    + "  B.OPERADOR           as operador,\n"
                    + "  B.PREFIJO_UNIDAD     as prefijo_unidad,\n"
                    + "  B.UNIT_TYPE_ID       as unit_type_id,\n"
                    + "  B.VALOR              as valor\n"
                    + "from BILLETERA B inner join GRUPO_BILLETERA GB on B.BILLETERA_ID = GB.BILLETERA_ID\n"
                    + " and B.ESTADO = 't' and GB.ESTADO = 't' and GB.COMPOSICION_BILLETERA_ID = ?\n"
                    + "  LEFT JOIN ACUMULADOR A on B.ACUMULADOR_ID = A.ACUMULADOR_ID and GB.COMPOSICION_BILLETERA_ID = ?\n"
                    + "ORDER BY B.BILLETERA_ID asc";

            LOG.debug("[getBilleterasByComposicionBilleteraId]Billetera sql: " + sql);
            conn = ConexionBD.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, composicionBilleteraId);
                stmt.setInt(2, composicionBilleteraId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        Billetera myBilletera = new Billetera();
                        myBilletera.setBilleteraId(rs.getInt("billetera_id"));
                        myBilletera.setEstado(rs.getString("estado"));
                        myBilletera.setMontoMaximo(rs.getDouble("monto_maximo"));
                        myBilletera.setMontoMinimo(rs.getDouble("monto_minimo"));
                        myBilletera.setNombreComercial(rs.getString("nombre_comercial"));
                        myBilletera.setNombreComverse(rs.getString("nombre_comverse"));
                        myBilletera.setOperador(rs.getString("operador"));
                        myBilletera.setPrefijoUnidad(rs.getString("prefijo_unidad"));
                        myBilletera.setUnitTypeId(rs.getInt("unit_type_id"));
                        myBilletera.setValor(rs.getDouble("valor"));
                        myBilletera.setCantidadDecimales(rs.getInt("cantidad_decimales"));
                        myBilletera.setAlco(rs.getString("alco"));
                        myBilletera.setNombreAcumulador(rs.getString("nombre_acumulador"));
                        myBilletera.setLimiteAcumulador(rs.getInt("limite_acumulador"));
                        result.add(myBilletera);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[getBilleteras] Error al intentar obtener la lista de "
                    + "billeteras Cos id " + composicionBilleteraId + " "
                    + "SQLException:" + e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de billeteras por Cos id " + composicionBilleteraId + " SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de billeteras por "
                            + "Cos id " + composicionBilleteraId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de billeteras por "
                            + "Cos id " + composicionBilleteraId + " SQLException:" + ex.getMessage());
                }
            }
        }
        return result;
    }

}
