/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.Occ;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class OccDao {

    private static final Logger LOG = Logger.getLogger(OccDao.class);

    public static List<Occ> obtenerAllOcc(/*Connection conn*/) {
        LOG.debug("ingresando a recuperar Lista de Occ");

        List<Occ> result = new LinkedList<>();
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT\n"
                        + "  OCC.OCC_ID,\n"
                        + "  OCC.CONFIG_ID,\n"
                        + "  OCC.ORIGEN_ID,\n"
                        + "  ORIGEN.NOMBRE as nombre_origen,\n"
                        + "  OCC.CORTO_ID,\n"
                        + "  CORTO.NOMBRE  as nombre_corto,\n"
                        + "  OCC.COS_ID,\n"
                        + "  COS.NOMBRE    as nombre_cos,\n"
                        + "  OCC.POSICION\n"
                        + "from OCC, ORIGEN, CORTO, COS\n"
                        + "WHERE OCC.OCC_ID <> 0\n"
                        + "AND OCC.ESTADO = 't'\n"
                        + "AND OCC.ORIGEN_ID = ORIGEN.ORIGEN_ID\n"
                        + "AND OCC.CORTO_ID = CORTO.CORTO_ID\n"
                        + "AND OCC.COS_ID = COS.COS_ID\n"
                        + "AND ORIGEN.ESTADO = 't'\n"
                        + "AND CORTO.ESTADO = 't'\n"
                        + "AND COS.ESTADO = 't'\n"
                        + "ORDER BY OCC.CONFIG_ID, OCC.POSICION ASC";
                LOG.debug("[obtenerAllOcc SQL]:" + sql);
                stmt = conn.prepareStatement(sql);
//                stmt.setInt(1, idCanal);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        Occ occ = new Occ();
                        occ.setOccId(rs.getInt("occ_id"));
                        occ.setConfigId(rs.getInt("config_id"));
                        occ.setOrigenId(rs.getInt("origen_id"));
                        occ.setNombreOrigen(rs.getString("nombre_origen"));
                        occ.setCortoId(rs.getInt("corto_id"));
                        occ.setNombreCorto(rs.getString("nombre_corto"));
                        occ.setCosId(rs.getInt("cos_id"));
                        occ.setNombreCos(rs.getString("nombre_cos"));
                        result.add(occ);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[obtenerAllOcc]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllOcc] Error al intentar recuperar :" + ex.getMessage());
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllOcc]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerAllConfigs] Error al intentar cerrar Connection despues: SqlException: " + ex.getMessage());
                }
            }
        }
        return result;
    }

}
