/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import myapps.cmn.util.ConexionBD;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class quotaDAO {

    private static final Logger LOG = Logger.getLogger(quotaDAO.class);

    public static Map<String, Long> getValueQuotas(/*Connection conn*/) {
        LOG.debug("ingresando a recuperar precios de quota de bd");
        // String nroClie=String.valueOf(numberClient);
        Map<String, Long> mapResult = new HashMap<>();
        Statement stmt = null;
        Connection conn = null;

        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "select  * from QUOTA";
                LOG.debug("[quotas] " + sql);
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        //myquota = new quota();
                        //myquota.setClientNumber(rs.getInt("nro_cliente"));
                        String nameQuota = rs.getString("nombre");
                        long bytesQuota = rs.getLong("bytes");
                        mapResult.put(nameQuota, bytesQuota);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[getquota] Error al intentar obtener la lista de "
                    + "quotas SQLException:" + e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaByCycle] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquota] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista  SQLException:" + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaas] Error al intentar cerrar Connection despues "
                            + "de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }
        }

        return mapResult;
    }

    //=============================================================================
    public static Map<String, Long> getValueState(/*Connection conn*/) {
        LOG.debug("ingresando a recuperar Valor de Estado bd");
        // String nroClie=String.valueOf(numberClient);
        Map<String, Long> mapResult = new HashMap<>();
        Statement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT ESTADO, BYTES\n"
                        + "from QUOTA where ESTADO <> '' or ESTADO is not null";
                LOG.debug("[quotas] " + sql);
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        String nameState = rs.getString("estado");
                        long bytesState = rs.getLong("bytes");
                        mapResult.put(nameState, bytesState);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getquota] Error al intentar obtener la lista de "
                    + "quotas SQLException:" + e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaByCycle] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquota] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista  SQLException:" + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaas] Error al intentar cerrar Connection despues "
                            + "de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }
        }

        return mapResult;
    }

    //=============================================================================
    /*NO USADA
    public static Map<String, Long> getValuePerfil() {
        LOG.info("ingresando a recuperar precios de quota de bd");
        // String nroClie=String.valueOf(numberClient);
        Map<String, Long> mapResult = new HashMap<String, Long>();
        Statement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT \"quota\".\"estado\", \"quota\".\"bytes\""
                        + "from \"quota\" WHERE \"quota\".\"estado\" <> ''";
                LOG.debug("[quotas] " + sql);
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);

                if (rs != null) {
                    while (rs.next()) {
                        //myquota = new quota();
                        //myquota.setClientNumber(rs.getInt("nro_cliente"));

                        String nameState = rs.getString("estado");
                        long bytesState = rs.getLong("bytes");

                        mapResult.put(nameState, bytesState);
                    }
                } else {
                    //    refMsgConstructor.CodeError=MessageConstructor.CODE_ERROR_CONEX_BD;
                    //    refMsgConstructor.MessageError=MessageConstructor.MSG_ERROR_CONEX_BD;

                }
            }

        } catch (SQLException e) {
            LOG.error("[getquota] Error al intentar obtener la lista de "
                    + "quotas SQLException:" + e.getMessage());
            // refMsgConstructor.CodeError=MessageConstructor.CODE_ERROR_BD_quota;
            // refMsgConstructor.MessageError=MessageConstructor.MSG_ERROR_BD_quota;

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaByCycle] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquota] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista  SQLException:" + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getquotaas] Error al intentar cerrar Connection despues "
                            + "de obtener la lista de quotas  SQLException:" + ex.getMessage());
                }
            }
        }

        return mapResult;
    }
     */
    //===========================================================================
    public static long getBytesProfile(String sql /*, Connection conn*/) {
        LOG.debug("ingresando a recuperar Bytes para Perfil de bd");

        long resp = 0;
        PreparedStatement stmt = null;
        Connection conn = null;

        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                LOG.debug("[Profiles] Sql para obtener Valor para Entitlements:" + sql);
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs != null) {
                    rs.next();
                    resp = rs.getLong("bytes");
                    //resp="_"+resp;
                    //log.info("[Profiles] Generado :"+resp);                   
                }

            }

        } catch (SQLException e) {
            LOG.error("[getProfileValue]"
                    + "SQLException:" + e.getMessage(), e);

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getProfileValue] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la Ultima guia SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getProfilesValue] Error al intentar cerrar el "
                            + "ResultSet " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getProfileValue] Error al intentar cerrar Connection despues "
                            + "de obtener Ultima guia por cliente id  SQLException:" + ex.getMessage());
                }
            }
        }

        return resp;
    }

    public static String getSqlQuota(List<String> ListEntitlements) {
        try {
            StringBuilder cadWhere = new StringBuilder();
            for (String string : ListEntitlements) {
                if (cadWhere.toString().equals("")) {
                    cadWhere.append(" PERFIL='");
                    cadWhere.append(string);
                    cadWhere.append("' ");
                } else {
                    cadWhere.append("OR PERFIL='");
                    cadWhere.append(string);
                    cadWhere.append("' ");
                }
            }
            String sql = "select * from QUOTA WHERE " + cadWhere;

            return sql;

        } catch (Exception e) {
            LOG.error("[getSqlQuota]error:" + e.getMessage());
            return "";
        }
    }

}
