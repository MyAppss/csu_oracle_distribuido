/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import myapps.cmn.vo.guide;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.bl.MessageConstructor;
import myapps.cmn.util.ConexionBD;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class guideDAO {

    private static final Logger LOG = Logger.getLogger(guideDAO.class);

    public static List<guide> getGuides(int numberClient, MessageConstructor refMsgConstructor, String sql) {
        LOG.debug("ingresando a recuperar Guias de bd");
        List<guide> result = new LinkedList<>();
        PreparedStatement stmt = null;
        Connection conn = null;

        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                LOG.debug("[Guides] " + sql);
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, numberClient);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        guide myguide = new guide();
                        myguide.setClientNumber(rs.getInt("nro_cliente"));
                        myguide.setCampaingId(rs.getInt("campanna_id"));
                        myguide.setMessageId(rs.getInt("mensaje_id"));
                        myguide.setCampaingPriority(rs.getInt("campanna_prioridad"));
                        myguide.setMessagePriority(rs.getInt("mensaje_prioridad"));
                        myguide.setMessagelength(rs.getInt("mensaje_longitud"));
                        myguide.setMessageInitial(rs.getString("mensaje_inicial"));
                        myguide.setMessageCommercial(rs.getString("mensaje_promocional"));
                        result.add(myguide);
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error("[getGuide] Error al intentar obtener la lista de "
                    + "guias del cliente con id " + numberClient + " "
                    + "SQLException:" + e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getguiaByCycle] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de guias por cliente id "
                            + numberClient + " SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGuide] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de guias por "
                            + "cliente id " + numberClient + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getguideas] Error al intentar cerrar Connection despues "
                            + "de obtener la lista de guias por cliente id "
                            + numberClient + " SQLException:" + ex.getMessage());
                }
            }
        }

        return result;
    }
    public static String getMaxFormatGuidesWallet(MessageConstructor refMsgConstructor) {
        LOG.debug("ingresando a recuperar Guias de bd");
        String resp = null;
        Statement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT * FROM LISTA_GUIAS WHERE ESTADO = 'CORRECTO' ORDER BY FECHA DESC";

                LOG.debug("[Guides] Sql para obtener Ultima Guia:" + sql);
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
                if (rs != null) {
                    rs.next();
                    resp = rs.getString("formato");
                    resp = "GUIA_" + resp;
                    LOG.debug("[Guides] Generado Nombre Ultima Guia:" + resp);
                }
            }

        } catch (SQLException e) {
            LOG.error("[getGuide] Error al intentar obtener ultima guia"
                    + "SQLException:" + e.getMessage());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getguia] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la Ultima guia SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGuide] Error al intentar cerrar el "
                            + "ResultSet despues de obtener Ultima Guia" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getguideas] Error al intentar cerrar Connection despues "
                            + "de obtener Ultima guia por cliente id  SQLException:" + ex.getMessage());
                }
            }
        }

        return resp;
    }

    public static String getGenerarSQLGuides(int numberClient, String namGuide) {
        try {
            String nroClie = String.valueOf(numberClient);
            String sufix = nroClie.substring(0, 2);
            if (sufix.equals("75") || sufix.equals("76") || sufix.equals("77") || sufix.equals("78")) {
            } else {
                sufix = "otros";
            }
            String sql = "SELECT * FROM " + namGuide + "_" + sufix + " where nro_cliente=? "
                    + "ORDER BY campanna_prioridad ASC, "
                    + "mensaje_prioridad ASC;";
            return sql;
        } catch (Exception e) {
            LOG.error("[getSQLGuides]:" + e.getMessage());
            return "";
        }
    }

}
