package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.Parametro;
import org.apache.log4j.Logger;
//import micrium.optin.model.Invitado;
//import micrium.optin.model.Parametro;
//import micrium.optin.utils.ServiceProvider;

/**
 *
 * @author Vehimar
 */
public class ParametrosDAO {

    private static final Logger LOG = Logger.getLogger(ParametrosDAO.class);
//----------------------------------------------------------------------------
//------obtener lista de invitados para determinado OPtin/Encuesta

    /*   public static List<Parametro> getListParametros() {
        log.debug("ingresando a recuperar Lista de Parametros ");

        List<Parametro> result = new LinkedList<Parametro>();
        PreparedStatement stmt = null;
        Connection conn = null;

        ResultSet rs = null;
        try {
            //conn = ServiceProvider.openDBConnection();
              conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT * FROM \"parametro\"";
                log.debug("[getListParametros SQL]:" + sql);
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                Parametro myParametro = null;
                if (rs != null) {
                    while (rs.next()) {
                        myParametro = new Parametro();
                        myParametro.setNombre(rs.getString("nombre"));
                        myParametro.setValor(rs.getString("valor"));
                        myParametro.setDescripcion(rs.getString("descripcion"));
                        myParametro.setEstado(rs.getString("estado"));
                        result.add(myParametro);
                    }
                }
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                log.warn("[getListParametros] Error al intentar recuperar :" + ex.getMessage());

            }
        } catch (Exception e) {
            log.error("[getListParametros]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    log.warn("[getListParametros]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    log.warn("[getListParametros] Error al intentar cerrar Connection despues: SqlException: " + ex.getMessage());

                }
            }
        }

        return result;
    }
     */
    //==========================================================================
    public static List<Parametro> getListParametros(/*Connection conn*/) {
        LOG.debug("ingresando a recuperar Lista de Parametros ");
        List<Parametro> result = new LinkedList<>();
        PreparedStatement stmt = null;
        Connection conn = null;

        ResultSet rs = null;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "SELECT * FROM PARAMETRO";
                LOG.debug("[getListParametros SQL]:" + sql);
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        Parametro myParametro = new Parametro();
                        myParametro.setNombre(rs.getString("nombre"));
                        myParametro.setValor(rs.getString("valor"));
                        myParametro.setDescripcion(rs.getString("descripcion"));
                        myParametro.setEstado(rs.getString("estado"));
                        result.add(myParametro);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[getListParametros]Error al intentar obtener la lista de parametros: SqlException: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros] Error al intentar recuperar :" + ex.getMessage());

                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros]Error al intentar cerrar PreparedStatement: SqlException: " + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getListParametros] Error al intentar cerrar Connection despues: SqlException: " + ex.getMessage());

                }
            }
        }

        return result;
    }

    //==========================================================================
    public static Parametro obtenerByName(String name /*, Connection conn*/) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Parametro result = null;
        Connection conn = null;
        try {
            String sql = "SELECT * from PARAMETRO WHERE NOMBRE =?";

            LOG.debug("[obtenerById] SQL: " + sql);
            conn = ConexionBD.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        result = new Parametro();
                        result.setDescripcion(rs.getString("descripcion"));
                        result.setEstado(rs.getString("estado"));
                        result.setNombre(rs.getString("nombre"));
                        result.setValor(rs.getString("valor"));
                    }
                }

            }
        } catch (SQLException ex) {
            LOG.error("[obtenerByName] Error al intentar obtener "
                    + "parametro id=" + name
                    + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el ResultSet"
                            + ex.getMessage());
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar el Statement"
                            + "Param id=" + name
                            + "SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[obtenerByName] Error al intentar cerrar la conexion"
                            + "Param id=" + name
                            + "SQLException:" + ex.getMessage());
                }
            }
        }
        return result;
    }

    //=========================================================================
/*    public static boolean UpdateParamResetConfig() {
        PreparedStatement st = null;
        Connection conn = null;
        boolean response = true;
        try {
            //conn = ServiceProvider.openDBConnection();
            conn = ConexionBD.getConnection();
            String sql = "UPDATE \"parametro\"\n"
                    + "SET \"parametro\".\"valor\" ='false'\n"
                    + "WHERE \"parametro\".\"nombre\" = 'sistema_consulta_saldo_sw_onchange'";

            LOG.info("[UpdateParamResetConfig]sql: " + sql);
            st = conn.prepareStatement(sql);

            st.execute();
            conn.close();
        } catch (Exception e) {
            LOG.error("[UpdateParamResetConfig] Error: " + e.getMessage());
            response = false;
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    response = false;
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    response = false;
                }
            }
        }
        return response;
    }
     */
    public static boolean UpdateParamResetConfig(/*Connection conn*/) {
        PreparedStatement st = null;
        Connection conn = null;
        boolean sw = true;
        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                String sql = "update PARAMETRO SET VALOR = 'false' where NOMBRE='sistema_consulta_saldo_sw_onchange'";
                LOG.debug("[UpdateParamResetConfig]sql: " + sql);
                st = conn.prepareStatement(sql);
                st.execute();
            }
        } catch (SQLException e) {
            LOG.error("[UpdateParamResetConfig] Error: " + e.getMessage());
            sw = false;
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    sw = false;
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.error("[UpdateParamResetConfig] Error: " + ex.getMessage());
                    sw = false;
                }
            }
        }
        return sw;
    }

}
