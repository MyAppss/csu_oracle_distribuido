/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.ComposicionBilletera;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class composicionBilleteraDAO {

    private static final Logger LOG = Logger.getLogger(composicionBilleteraDAO.class);

    public static List<ComposicionBilletera> getComposicionBilleterasByConfigId(int configId /*, Connection conn*/) {

        List<ComposicionBilletera> result = new ArrayList<>();
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "select\n"
                    + "  DISTINCT\n"
                    + "  CB.COMPOSICION_BILLETERA_ID        as composicion_billetera_id,\n"
                    + "  CB.CANTIDAD_DECIMALES              as cantidad_decimales,\n"
                    + "  CB.ESTADO                          as estado,\n"
                    + "  CB.MONTO_MAXIMO                    as monto_maximo,\n"
                    + "  CB.MONTO_MINIMO                    as monto_minimo,\n"
                    + "  CB.NOMBRE_COMERCIAL                as nombre_comercial,\n"
                    + "  CB.NOMBRE                          as nombre,\n"
                    + "  CB.OPERADOR                        as operador,\n"
                    + "  CB.PREFIJO_UNIDAD                  as prefijo_unidad,\n"
                    + "  CB.UNIT_TYPE_ID                    as unit_type_id,\n"
                    + "  CB.VALOR                           as valor,\n"
                    + "  CCB.NOMBRE_COMERCIAL               as config_nombre_comercial,\n"
                    + "  CCB.MOSTRAR_SALDO_CERO             as config_mostrar_saldo_cero,\n"
                    + "  CCB.NO_MOSTRAR_SALDO_EXPIRADO      as no_mostrar_saldo_expirado,\n"
                    + "  CCB.MOSTRAR_SALDO_MAYOR_CERO       as mostrar_saldo_mayor_cero,\n"
                    + "  CCB.MOSTRAR_SALDO_MENOR_CERO       as mostrar_saldo_menor_cero,\n"
                    + "  CCB.MOSTRAR_SIEMPRE                as mostrar_siempre,\n"
                    + "  CCB.MOSTRAR_VIGENCIA               as mostrar_vigencia,\n"
                    + "  CCB.MOSTRAR_SALDO_EXPIRADO         as mostrar_saldo_expirado,\n"
                    + "  CCB.POSICION                       as posicion,\n"
                    + "  CCB.MOSTRAR_SEGUNDA_FECHA_EXP      as mostrar_segunda_fecha_exp,\n"
                    + "  CCB.MOSTRAR_HORA_SEGUNDA_FECHA_EXP as mostrar_hora_segunda_fecha_exp,\n"
                    + "  CCB.ASUMIR_FORMAT_HR_PRIMERA_FECHA as asumir_format_hr_primera_fecha\n"
                    + "from COMPOSICION_BILLETERA CB inner join CONFIG_COMPOSICION_BILLETERA CCB\n"
                    + "    on CB.COMPOSICION_BILLETERA_ID = CCB.COMPOSICION_BILLETERA_ID\n"
                    + "       and CB.ESTADO = 't' and CCB.ESTADO = 't' and CCB.CONFIG_ID = ?\n"
                    + "ORDER BY POSICION ASC";
            LOG.debug("[getComposicionBilleterasByConfigId]Billetera sql: " + sql);
            conn = ConexionBD.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, configId);
                rs = stmt.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        ComposicionBilletera myBilletera = new ComposicionBilletera();
                        myBilletera.setComposicionBilleteraId(rs.getInt("composicion_billetera_id"));
                        myBilletera.setEstado(rs.getString("estado"));
                        myBilletera.setNombreComercial(rs.getString("nombre_comercial"));
                        myBilletera.setNombre(rs.getString("nombre"));
                        myBilletera.setOperador(rs.getString("operador"));
                        myBilletera.setPrefijoUnidad(rs.getString("prefijo_unidad"));
                        myBilletera.setUnitTypeId(rs.getInt("unit_type_id"));
                        myBilletera.setValor(rs.getDouble("valor"));
                        myBilletera.setMontoMinimo(rs.getDouble("monto_minimo"));
                        myBilletera.setMontoMaximo(rs.getDouble("monto_maximo"));
                        myBilletera.setCantidadDecimales(rs.getInt("cantidad_decimales"));
                        myBilletera.setConfig_nombreComercial(rs.getString("config_nombre_comercial"));
                        myBilletera.setConfig_mostrar_saldo_cero(rs.getString("config_mostrar_saldo_cero"));
                        myBilletera.setConfig_no_mostrar_saldo_expirado(rs.getString("no_mostrar_saldo_expirado"));
                        myBilletera.setConfig_mostrar_saldo_mayor_cero(rs.getString("mostrar_saldo_mayor_cero"));
                        myBilletera.setConfig_mostrar_saldo_menor_cero(rs.getString("mostrar_saldo_menor_cero"));
                        myBilletera.setConfig_mostrar_siempre(rs.getString("mostrar_siempre"));
                        myBilletera.setConfig_mostrar_vigencia(rs.getString("mostrar_vigencia"));
                        myBilletera.setMostrar_saldo_expirado(rs.getString("mostrar_saldo_expirado"));
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        myBilletera.setConfigBilletera_mostrar_segunda_fecha_exp(rs.getString("mostrar_segunda_fecha_exp"));
                        myBilletera.setConfigBilletera_mostrar_hora_segunda_fecha_exp(rs.getString("mostrar_hora_segunda_fecha_exp"));
                        myBilletera.setConfigBilletera_asumir_formato_hora_primera_fecha(rs.getString("asumir_format_hr_primera_fecha"));
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        result.add(myBilletera);
                    }
                }
            }

        } catch (SQLException e) {
            LOG.error("[getBilleteras] Error al intentar obtener la lista de "
                    + "billeteras Cos id " + configId + " "
                    + "SQLException:" + e.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar PreparedStatement "
                            + "despues de obtener la lista de billeteras por Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar el "
                            + "ResultSet despues de obtener la lista de billeteras por "
                            + "Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getBilleteras] Error al intentar cerrar Connection despues "
                            + "de obtener la lista de billeteras por Cos id " + configId + " SQLException:" + ex.getMessage());
                }
            }
        }
        return result;
    }

}
