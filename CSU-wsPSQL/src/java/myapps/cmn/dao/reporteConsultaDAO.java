/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.dao;

import java.sql.*;
import myapps.cmn.util.ConexionBD;
import myapps.cmn.vo.ReporteConsulta;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class reporteConsultaDAO {

    private static final Logger LOG = Logger.getLogger(reporteConsultaDAO.class);
    //o SAC
    //1 CONTACT CENTER
    //============================================================================   

    public static void saveReporteConsulta(ReporteConsulta rep) {
        PreparedStatement stmt = null;
        Connection conn = null;

        try {
            conn = ConexionBD.getConnection();
            if (conn != null) {
                // String sql = "insert into \"reporte_consulta\"(\"fecha\",\"msisdn\",\"identificador_servicio\",\"nombre_canal\",\"publicidad_solicitada\",\"longitud_max_solicitada\",\"ip_cliente\",\"texto_generado\")\n" +
                //               "values(?,?,?,?,?,?,?,?)";
                String sql = "insert  into REPORTE_CONSULTA (FECHA,MSISDN,IDENTIFICADOR_SERVICIO,NOMBRE_CANAL,PUBLICIDAD_SOLICITADA,LONGITUD_MAX_SOLICITADA,IP_CLIENTE,TEXTO_GENERADO,SESSIONID,OPCION) values (?,?,?,?,?,?,?,?,?,?)";

                LOG.debug("[saveReporteConsulta]:sql " + sql);
                stmt = conn.prepareStatement(sql);
                stmt.setTimestamp(1, rep.getFecha());
                stmt.setString(2, rep.getMsisdn());
                stmt.setString(3, rep.getIdentificador_servicio());
                stmt.setString(4, rep.getNombre_canal());
                stmt.setString(5, rep.isPublicidad_solicitada());
                stmt.setInt(6, rep.getLongitud_max_solicitada());
                stmt.setString(7, rep.getIp_cliente());
                stmt.setString(8, rep.getTexto_generado());
                stmt.setString(9, rep.getSessionId());
                stmt.setString(10, rep.getOpcion());
                stmt.execute();
                LOG.debug("[saveReporteConsulta]:nuevo registro guardado correctamente");
            }
        } catch (SQLException e) {
            LOG.error("[saveReporteConsulta]:error al intentar guardar:" + e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[saveReporteConsulta] Error al cerrar PreparedStatement", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[saveReporteConsulta] Error al cerrar connection", ex);
                }
            }
        }

    }

    
 
}
