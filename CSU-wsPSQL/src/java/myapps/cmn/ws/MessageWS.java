/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.ws;

import java.util.HashMap;
import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import myapps.activeMQ.ColaJMS;
import myapps.activeMQ.ColaThreadMax;
import myapps.activeMQ.EnviarMensajeJMS;
import myapps.cmn.bl.consultaSaldo;
import myapps.cmn.bl.validador;
import myapps.cmn.util.ErrorCodes;
import myapps.cmn.util.Propiedades;
import myapps.cmn.util.UtilDate;
import myapps.cmn.util.UtilJava;
import myapps.cmn.util.UtilMontoMinimo;
import myapps.cmn.vo.MessageResponse;
import myapps.cmn.vo.ReporteConsulta;
import myapps.cmn.vo.Response;
import myapps.security.Credencial;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
@WebService(serviceName = "MessageWS", targetNamespace = "http://ws.cmn.micrium/")
public class MessageWS {

    private static final Logger LOG = Logger.getLogger(MessageWS.class);
    @Resource
    private WebServiceContext wsContext;

    /**
     * Web service operation P_isdn: Nro de cel, sin prefijo LengthMax: Longitud
     * max q debe tener el mensaje a generar IdServicio: Nombre o identificador
     * del servio(cliente) q consume al ws. ej: *611# Canal: nombre del canal
     * por el q se muestrara la consulta: Ej: Ussd, Sms SwPublicidad: Boolean q
     * indica si se debe o no incluir publicidad en el msg
     *
     * @param P_isdn
     * @param LengthMax
     * @param IdServicio
     * @param Canal
     * @param SwPublicidad
     * @return
     */
    //@WebParam(name = "P_isdn") String P_isdn,@WebParam(name = "LengthMax") int LengthMax
    public MessageResponse getConsultaSaldo(
            @WebParam(name = "P_isdn") String P_isdn,
            @WebParam(name = "LengthMax") int LengthMax,
            @WebParam(name = "IdServicio") String IdServicio,
            @WebParam(name = "Canal") String Canal,
            @WebParam(name = "SwPublicidad") boolean SwPublicidad) {
        long tiempoInicio = System.currentTimeMillis();
        String session = UtilMontoMinimo.generarKey();
        String logPrefijo = " P_isdnisdn: " + P_isdn + "][Session: " + session + "][LengthMax: " + LengthMax + "][Canal: " + Canal + "][IdServicio: " + IdServicio + "][SwPublicidad: " + SwPublicidad + "] ";
        try {

            LOG.info("Iniciando consulta de saldo isdn: " + P_isdn + ", canal: " + Canal + ", corto: " + IdServicio);

            Credencial credencial = validador.getCredenciales(wsContext.getMessageContext());

            MessageResponse me = new MessageResponse();
            if (validador.verificarCredencial(credencial)) {
                if (IdServicio == null || IdServicio.equals("")) {
                    IdServicio = Propiedades.DEFAULT_SERVICIO;
                }
                if (Canal == null || Canal.equals("")) {
                    Canal = Propiedades.DEFAULT_CANAL;
                }
                String requisitos = UtilJava.requestValido(P_isdn, Canal, IdServicio, logPrefijo);
                if (requisitos.isEmpty()) {
                    LOG.debug(logPrefijo + "Credenciales correctar : " + P_isdn + ", canal: " + Canal + ", corto: " + IdServicio);
                    consultaSaldo cs = new consultaSaldo();
                    String mensaje = cs.getConsultaSaldo(P_isdn, LengthMax, SwPublicidad, Canal.toUpperCase(), IdServicio, session, logPrefijo);
                    me.setMessage(mensaje);
                    me.setCodeError(cs.getErrorCode());
                    me.setDescriptionError(cs.getErrorDescripcion());

                    //registrar en reporte de Consultas de saldo-----------------------
                    MessageContext mc = wsContext.getMessageContext();
                    HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                    ReporteConsulta RC = new ReporteConsulta();
                    RC.setFecha(UtilDate.getTimetampActual());
                    RC.setIdentificador_servicio(IdServicio);
                    RC.setIp_cliente(req.getRemoteAddr());
                    RC.setLongitud_max_solicitada(LengthMax);
                    RC.setMsisdn(P_isdn);
                    RC.setNombre_canal(Canal);
                    if (SwPublicidad == true) {
                        RC.setPublicidad_solicitada("t");
                    } else {
                        RC.setPublicidad_solicitada("f");
                    }
                    RC.setTexto_generado(me.getMessage());
                    // session = Calendar.getInstance().getTimeInMillis() + "_" + P_isdn;

                    RC.setSessionId(session);
                    RC.setOpcion("");
                    //Connection con = ConexionBD.getConnection();
                    // reporteConsultaDAO.saveReporteConsulta(RC);
                    //con.close();
                    //-----------------------------------------------------------------       

                    HashMap<String, Object> mapa = new HashMap<>();
                    mapa.put("CONSULTA", RC);

                    EnviarMensajeJMS jms = new EnviarMensajeJMS(mapa, "[JMS] [isdn: " + P_isdn + " session: " + session + ", canal: " + Canal + ", corto: " + IdServicio + "]");
                    jms.start();
                } else {
                    me = UtilJava.devolverRespuestaFallida(P_isdn, requisitos, logPrefijo);
                }

            } else {   //MessageConstructor ms=new MessageConstructor();
                me.setCodeError(ErrorCodes.CODE_ERROR_LOGIN_WS);
                me.setDescriptionError(ErrorCodes.MSG_ERROR_LOGIN_WS);
                me.setMessage("ERROR");
            }
            LOG.info(logPrefijo + "Consulta de saldo Satisfactoria isdn: " + P_isdn + " session: " + session + ", canal: " + Canal + ", corto: " + IdServicio + " Respuesta Satisfactoria: " + me.toString() + " tiempo Total del Servicio: " + (System.currentTimeMillis() - tiempoInicio) + " ms");
            return me;

        } catch (Exception e) {
            LOG.error("[getConsultaSaldo]", e);
            MessageResponse me = new MessageResponse();
            me.setCodeError(30);
            me.setDescriptionError("Error:Se produjo una excepcion");
            me.setMessage("");
            LOG.warn(logPrefijo + "Consulta de saldo Fallida : " + me.toString() + " tiempo Total del Servicio: " + (System.currentTimeMillis() - tiempoInicio) + " ms");
            return me;
        }
    }

    public Response getConsultaSaldoList(
            @WebParam(name = "P_isdn") String P_isdn,
            @WebParam(name = "IdServicio") String IdServicio,
            @WebParam(name = "Canal") String Canal,
            @WebParam(name = "SwPublicidad") boolean SwPublicidad) {
        //TODO write your implementation code here:
        //return null
        long tiempoInicio = System.currentTimeMillis();
        String session = UtilMontoMinimo.generarKey();
        String logPrefijo = " [isdn: " + P_isdn + "][Session: " + session + "][canal: " + Canal + "][corto: " + IdServicio + "] ";

        try {

            if (IdServicio == null || IdServicio.equals("")) {
                IdServicio = Propiedades.DEFAULT_SERVICIO;
            }
            if (Canal == null || Canal.equals("")) {
                Canal = Propiedades.DEFAULT_CANAL;
            }
            LOG.info(logPrefijo + "Iniciando consulta de saldo isdn: " + P_isdn + ", canal: " + Canal + ", corto: " + IdServicio);

            Credencial credencial = validador.getCredenciales(wsContext.getMessageContext());
            Response me = new Response();
            if (validador.verificarCredencial(credencial)) {

                String requisitos = UtilJava.requestValido(P_isdn, Canal, IdServicio, logPrefijo);
                if (requisitos.isEmpty()) {

                    LOG.debug(logPrefijo + "Credenciales correctar : " + P_isdn + ", canal: " + Canal + ", corto: " + IdServicio);
                    consultaSaldo cs = new consultaSaldo();
                    String saldoString = cs.getConsultaSaldo(P_isdn, 1000, SwPublicidad, Canal.toUpperCase(), IdServicio, session, logPrefijo);
                    me.setCodeError(cs.getErrorCode());
                    me.setDescripcion(cs.getErrorDescripcion());
                    me.setSaludoInicial(cs.getSaludoInicial());
                    me.setBilleteras(cs.getListBilleteras());
                    me.setPublicidad(cs.getPublicidad());

                    //registrar en reporte de Consultas de saldo-----------------------
                    MessageContext mc = wsContext.getMessageContext();
                    HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                    ReporteConsulta RC = new ReporteConsulta();
                    RC.setFecha(UtilDate.getTimetampActual());
                    RC.setIdentificador_servicio(IdServicio);
                    RC.setIp_cliente(req.getRemoteAddr());
                    RC.setLongitud_max_solicitada(1000);
                    RC.setMsisdn(P_isdn);
                    RC.setNombre_canal(Canal);
                    if (SwPublicidad == true) {
                        RC.setPublicidad_solicitada("t");
                    } else {
                        RC.setPublicidad_solicitada("f");
                    }
                    RC.setTexto_generado(saldoString);
                    //session = Calendar.getInstance().getTimeInMillis() + "_" + P_isdn;
                    RC.setSessionId(session);
                    RC.setOpcion("");
                    //Connection con = ConexionBD.getConnection();
                    //reporteConsultaDAO.saveReporteConsulta(RC);
                    //con.close();             
                    //cs.saveConsulta(RC);
                    HashMap<String, Object> mapa = new HashMap<>();
                    mapa.put("CONSULTA", RC);
                    EnviarMensajeJMS jms = new EnviarMensajeJMS(mapa, "[JMS] isdn: " + P_isdn + " session: " + session + ", canal: " + Canal + ", corto: " + IdServicio);
                    jms.start();
                    //-----------------------------------------------------------------   
                } else {
                    me = UtilJava.devolverRespuestaFallidaList(P_isdn, requisitos, logPrefijo);
                }
            } else {   //MessageConstructor ms=new MessageConstructor();
                me.setCodeError(ErrorCodes.CODE_ERROR_LOGIN_WS);
                me.setDescripcion(ErrorCodes.MSG_ERROR_LOGIN_WS);
                me.setSaludoInicial("ERROR");
            }
            LOG.info(logPrefijo + "Consulta de saldo Satisfactoria isdn: " + P_isdn + " session: " + session + ", canal: " + Canal + ", corto: " + IdServicio + " Respuesta Satisfactoria: " + me.toString() + " tiempo Total del Servicio: " + (System.currentTimeMillis() - tiempoInicio) + " ms");

            return me;

        } catch (Exception e) {
            LOG.error("[getConsultaSaldo]", e);
            Response me = new Response();
            me.setCodeError(30);
            me.setDescripcion("Error:Se produjo una excepcion");
            me.setSaludoInicial("");
            LOG.warn(logPrefijo + "Consulta de saldo Fallida : " + me.toString() + " tiempo Total del Servicio: " + (System.currentTimeMillis() - tiempoInicio) + " ms");
            return me;
        }
    }

    public String getVersion() {
        if (Boolean.TRUE.equals(Propiedades.BANDERA_MOSTRAR_INFORMACION)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("cola_size", ColaJMS.size());
            map.put("nro_hilos", Propiedades.MQ_HILOS);
            map.put("max_thread_disponiblbe", ColaThreadMax.size());
            map.put("version", "1.0");
            return map.toString();
        } else {

            return "1.0";
        }

    }

}
