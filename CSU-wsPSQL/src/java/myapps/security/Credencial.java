/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.security;

/**
 *
 * @author Vehimar
 */
public class Credencial {

    private String user;
    private String pass;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
}
