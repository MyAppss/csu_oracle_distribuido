CREATE OR REPLACE PROCEDURE CSUORACLE.CREATE_PARTITION
IS
  TYPE TIPO_RECORD IS RECORD (TABLA              VARCHAR2(30) ,
                                                  FECHA              DATE,
                                                  PARTICION       VARCHAR2(20)); 
  TYPE TIPO_ARRAY IS TABLE OF TIPO_RECORD;
  v_query                   TIPO_ARRAY;
  vl_date                   DATE;
  vl_fecaux                DATE;
  vl_fecha                  VARCHAR2(10); 
BEGIN
  vl_date    := TRUNC(SYSDATE + 15);
  vl_fecha  := CONCAT('P' , TO_CHAR(vl_date,'YYYYMMDD'));
  SELECT TABLE_NAME , TO_DATE(SUBSTR(MAX(PARTITION_NAME),2,9),'YYYY/MM/DD'), MAX(PARTITION_NAME)
      BULK COLLECT INTO v_query
     FROM USER_TAB_PARTITIONS M
WHERE NOT EXISTS ( SELECT * FROM USER_TAB_PARTITIONS N WHERE M.TABLE_NAME  = N.TABLE_NAME and N.PARTITION_NAME = VL_FECHA)
    AND TABLE_NAME NOT LIKE 'CTRL%'
 GROUP BY TABLE_NAME;
  ----
  FOR i IN 1 .. v_query.COUNT  LOOP
        vl_fecaux := v_query(i).fecha + 1;
        WHILE vl_fecaux <= vl_date LOOP
             vl_fecha  := CONCAT('P' , TO_CHAR(vl_fecaux,'YYYYMMDD'));
             vl_fecaux := vl_fecaux + 1 ;
            EXECUTE IMMEDIATE 'ALTER TABLE CSUORACLE.' || v_query(i).TABLA || ' ADD PARTITION ' ||  vl_fecha || 
                                                   ' VALUES LESS THAN (TO_DATE (' || CHR(39)||TO_CHAR(vl_fecaux,'DD/MM/YYYY') || CHR(39) || ',' || CHR(39) || 'DD/MM/YYYY' || CHR(39) ||'))';
         END LOOP;       
  END LOOP;    
  --
END;
/
