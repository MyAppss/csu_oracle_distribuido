
--Acceder al esquema
ALTER SESSION SET CURRENT_SCHEMA = CSUORACLE;


---------------------------------------------------------------------------------------------------------------------------------------------------
--Migracion con fecha parametrizada (Opcion1)
---------------------------------------------------------------------------------------------------------------------------------------------------

-- renombrar tabla anterior--
ALTER TABLE DETALLE_REPORTE_CONSULTA RENAME TO DETALLE_REPORTE_CONSULTA_BACKUP;

--create table
CREATE TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA" (
  "MSISDN" VARCHAR2(50 BYTE) VISIBLE ,
  "SESSIONID" CHAR(50 BYTE) VISIBLE ,
  "OPCION" CHAR(100 BYTE) VISIBLE ,
  "FECHA" TIMESTAMP(6) VISIBLE 
)
NOLOGGING
NOCOMPRESS

STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT



PARTITION BY RANGE (FECHA)
 (
PARTITION P20200101 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
LOGGING
NOCOMPRESS)
;


--crear indices
	 CREATE UNIQUE INDEX "CSUORACLE"."PK_DETALLE_REPORTE_CONSULTA21" ON "CSUORACLE"."DETALLE_REPORTE_CONSULTA"
(FECHA,MSISDN,SESSIONID)
STORAGE (
BUFFER_POOL DEFAULT
FLASH_CACHE DEFAULT
CELL_FLASH_CACHE DEFAULT
)
LOGGING
LOCAL (
PARTITION P20200101
NOCOMPRESS
)
NOPARALLEL;


-- crear llaves primarias--
ALTER TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA"
ADD CONSTRAINT PK_DETALLE_REPORTE_CONSULTA2021
PRIMARY KEY (FECHA,MSISDN,SESSIONID);


--funtion para crear particiones en la nueva tabla--
	DECLARE
	fechaMinima TIMESTAMP;
	fechaString VARCHAR(50);
	fechaMaxima TIMESTAMP;
	consulta VARCHAR(1500);
	vl_fecha  VARCHAR2(20); 
	final VARCHAR(1500);
	BEGIN
	--parametrizar su fecha minima
	SELECT TO_DATE('01-01-2021','DD-MM-YYYY') into fechaMinima from dual;
	SELECT MAX(FECHA) into fechaMaxima  from DETALLE_REPORTE_CONSULTA_BACKUP;
	
		DBMS_OUTPUT.PUT_LINE ('fecha minima es: '||fechaMinima);
		DBMS_OUTPUT.PUT_LINE ('fecha maxima es: '||fechaMaxima);

	select 'ALTER TABLE CSUORACLE.DETALLE_REPORTE_CONSULTA ' into consulta from dual;
	DBMS_OUTPUT.PUT_LINE ('consulta: '||consulta);
		WHILE fechaMinima<=fechaMaxima+1
		LOOP
			  SELECT TO_CHAR(fechaMinima,'dd-MM-yyyy') into fechaString  from dual;
				  vl_fecha  := CONCAT('P' , TO_CHAR(fechaMinima,'YYYYMMDD'));
				final:=consulta||' ADD PARTITION '||vl_fecha||' VALUES LESS THAN (TO_DATE('||CHR(39)||fechaString||CHR(39)||','||CHR(39)||'dd-MM-yyyy'||CHR(39)||'))';
			execute immediate final;
				DBMS_OUTPUT.PUT_LINE ('------------------');
				fechaMinima:=fechaMinima+1;
END LOOP;

	END;
/


---traspasar datos de la antigua tabala a la nueva ponienda fecha minima
	 	INSERT INTO CSUORACLE.DETALLE_REPORTE_CONSULTA  SELECT * FROM  CSUORACLE.DETALLE_REPORTE_CONSULTA_BACKUP  WHERE FECHA>= TO_DATE('01-01-2021','DD-MM-YYYY');



--Elimnar los datos salvados de la tabla backup  se recomienda moverlos datos de la tabla backup a la bd historica
	DELETE FROM  CSUORACLE.DETALLE_REPORTE_CONSULTA_BACKUP WHERE FECHA>= TO_DATE('01-01-2021','DD-MM-YYYY');
	
--opcional ejcutar proceso de crear Particion
	BEGIN
	  CSUORACLE.CREATE_PARTITION();
	    COMMIT;
	END;
	
--Limpiar tabla backup
	 DROP TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA_BACKUP" purge;
	 
	 
---------------------------------------------------------------------------------------------------------------------------------------------------
--Migracion total de la bd (Opcion2)
---------------------------------------------------------------------------------------------------------------------------------------------------
	-- renombrar tabla anterior--
ALTER TABLE DETALLE_REPORTE_CONSULTA RENAME TO DETALLE_REPORTE_CONSULTA_BACKUP;

--create table
CREATE TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA" (
  "MSISDN" VARCHAR2(50 BYTE) VISIBLE ,
  "SESSIONID" CHAR(50 BYTE) VISIBLE ,
  "OPCION" CHAR(100 BYTE) VISIBLE ,
  "FECHA" TIMESTAMP(6) VISIBLE 
)
NOLOGGING
NOCOMPRESS

STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT



PARTITION BY RANGE (FECHA)
 (
PARTITION P20200101 VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
LOGGING
NOCOMPRESS)
;


--crear indices
	 CREATE UNIQUE INDEX "CSUORACLE"."PK_DETALLE_REPORTE_CONSULTA21" ON "CSUORACLE"."DETALLE_REPORTE_CONSULTA"
(FECHA,MSISDN,SESSIONID)
STORAGE (
BUFFER_POOL DEFAULT
FLASH_CACHE DEFAULT
CELL_FLASH_CACHE DEFAULT
)
LOGGING
LOCAL (
PARTITION P20200101
NOCOMPRESS
)
NOPARALLEL;


-- crear llaves primarias--
ALTER TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA"
ADD CONSTRAINT PK_DETALLE_REPORTE_CONSULTA2021
PRIMARY KEY (FECHA,MSISDN,SESSIONID);


--funtion para crear particiones en la nueva tabla--
	DECLARE
	fechaMinima TIMESTAMP;
	fechaString VARCHAR(50);
	fechaMaxima TIMESTAMP;
	consulta VARCHAR(1500);
	vl_fecha  VARCHAR2(20); 
	final VARCHAR(1500);
	BEGIN
	SELECT MIN(FECHA) into fechaMinima  from DETALLE_REPORTE_CONSULTA_BACKUP;
	SELECT MAX(FECHA) into fechaMaxima  from DETALLE_REPORTE_CONSULTA_BACKUP;
	
		DBMS_OUTPUT.PUT_LINE ('fecha minima es: '||fechaMinima);
		DBMS_OUTPUT.PUT_LINE ('fecha maxima es: '||fechaMaxima);

	select 'ALTER TABLE CSUORACLE.DETALLE_REPORTE_CONSULTA ' into consulta from dual;
	DBMS_OUTPUT.PUT_LINE ('consulta: '||consulta);
		WHILE fechaMinima<=fechaMaxima+1
		LOOP
			  SELECT TO_CHAR(fechaMinima,'dd-MM-yyyy') into fechaString  from dual;
				  vl_fecha  := CONCAT('P' , TO_CHAR(fechaMinima,'YYYYMMDD'));
				final:=consulta||' ADD PARTITION '||vl_fecha||' VALUES LESS THAN (TO_DATE('||CHR(39)||fechaString||CHR(39)||','||CHR(39)||'dd-MM-yyyy'||CHR(39)||'))';
			execute immediate final;
				DBMS_OUTPUT.PUT_LINE ('------------------');
				fechaMinima:=fechaMinima+1;
END LOOP;

	END;
/
	
--traspasar la data de la antigua tabla ala nueva--
	INSERT INTO CSUORACLE.DETALLE_REPORTE_CONSULTA  
	 SELECT * FROM  CSUORACLE.DETALLE_REPORTE_CONSULTA_BACKUP ;
	 
	 
	 --opcional ejcutar proceso de crear Particion (ejeutar al final)
	BEGIN
	  CSUORACLE.CREATE_PARTITION();
	    COMMIT;
	END;
	
--Limpiar tabla backup
	 DROP TABLE "CSUORACLE"."DETALLE_REPORTE_CONSULTA_BACKUP" purge;
	 

		

	 

	 
