/*
 Navicat Premium Data Transfer

 Source Server         : ORACLE
 Source Server Type    : Oracle
 Source Server Version : 120200
 Source Host           : localhost:1521
 Source Schema         : CSUORACLE

 Target Server Type    : Oracle
 Target Server Version : 120200
 File Encoding         : 65001

 Date: 28/07/2020 09:54:07
*/


-- ----------------------------
-- Table structure for USUARIO
-- ----------------------------
DROP TABLE "CSUORACLE"."USUARIO";
CREATE TABLE "CSUORACLE"."USUARIO" (
  "USUARIO_ID" NUMBER(11) VISIBLE NOT NULL ,
  "ROL_ID" NUMBER(11) VISIBLE ,
  "LOGIN" NVARCHAR2(40) VISIBLE ,
  "NOMBRE" NVARCHAR2(50) VISIBLE ,
  "ESTADO" VARCHAR2(5 CHAR) VISIBLE 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of USUARIO
-- ----------------------------
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('295', '4', 'padillaa', 'Alberto Padilla Rojas', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('296', '4', 'paradal', 'Luis Fernando Parada Guerrero', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('285', '4', 'ayalah', 'Henrry Ayala', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('286', '4', 'uzquianoca', 'Carlos Favio Uzquiano Velasco', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('287', '4', 'cisnerosg', 'Gustavo Cisneros Leon', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('288', '4', 'guzmany', 'Ysaac Guzman', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('289', '4', 'montanola', NULL, 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('290', '4', 'montanola', 'Laura Yubinka Montaño Barba', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('291', '4', 'picolominia', 'Avigail Picolomini Mendoza', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('292', '4', 'paradav', 'Victor Hugo Parada Paz', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('293', '4', 'chavezj', 'Josué Chavez Gonzales', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('294', '4', 'paniaguak', 'Klever Edson Paniagua', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('284', '1', 'villarroelj', 'Julio Cesar Villarroel Zabala', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('283', '4', 'crespoy', 'Yim Deivid Crespo Marquez', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('282', '5', 'uzquianoca', 'Carlos Favio Uzquiano Velasco', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('281', '5', 'bernalp', 'Pablo Bernal', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('275', '1', 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('1', '1', 'admin', 'admin', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('3', '1', 'riveroju', 'Julio Cesar Rivero Oblitas', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('4', '1', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('5', '4', 'osinagac', 'Carlos Rubirh Osinaga Chavez', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('6', '5', 'vacad', 'Daniel Vaca Rua', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('7', '5', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('8', '1', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('9', '1', 'penama', 'Marco Augusto Peña  Alcocer', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('10', '1', 'guzmany', 'Ysaac Guzman', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('11', '1', 'vargasm', 'Marilin Vargas Perez', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('12', '4', 'eguezc', 'Carlos Eduardo Eguez Rojas', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('13', '4', 'villarroellu', 'Luis Daniel Villarroel', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('14', '4', 'bordaj', 'Jimmy Alejandro Borda Villa', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('15', '4', 'monasteriok', 'Kevin Monasterio Mejia', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('16', '1', 'salazari', 'Iver Salazar Zorilla', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('17', '4', 'montecinosd', 'Denis Maribel Montecinos Coronado', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('18', '1', 'pedrozow', 'Wilfredo Pedrozo Guzman', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('19', '4', 'iralaj', 'Jose Luis Irala', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('20', '4', 'palomog', 'Giovana Palomo Sainz', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('280', '4', 'uzquianoca', 'Carlos Favio Uzquiano Velasco', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('274', '1', 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('278', '5', 'ramose', 'Erika Johana Ramos Peña', 't');
INSERT INTO "CSUORACLE"."USUARIO" VALUES ('276', '1', 'vargasm', 'Marilin Vargas Perez', 't');

-- ----------------------------
-- Primary Key structure for table USUARIO
-- ----------------------------
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004231" PRIMARY KEY ("USUARIO_ID");

-- ----------------------------
-- Checks structure for table USUARIO
-- ----------------------------
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C0012490" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C0029197" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C0039415" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004048" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004228" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004229" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004230" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C007941" CHECK ("USUARIO_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "USUARIO_USUARIO_ID_CHECK" CHECK (("USUARIO_ID" IS NOT NULL)) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table USUARIO
-- ----------------------------
ALTER TABLE "CSUORACLE"."USUARIO" ADD CONSTRAINT "SYS_C004258" FOREIGN KEY ("ROL_ID") REFERENCES "CSUORACLE"."ROL" ("ROL_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
