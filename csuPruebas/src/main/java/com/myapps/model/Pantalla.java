package com.myapps.model;

import java.util.List;

/**
 *
 * @author Usuario
 */
public class Pantalla {

    private int idPantalla;
    private String cabecera;
    private String opciones;
    private List<Integer> ListaNroOpcion;
    private List<Navega> listaNavegacion;
    private Integer pantallaPadre;
    private Integer opcionPadre;

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    public String getOpciones() {
        return opciones;
    }

    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public Integer getPantallaPadre() {
        return pantallaPadre;
    }

    public void setPantallaPadre(Integer pantallaPadre) {
        this.pantallaPadre = pantallaPadre;
    }

    public Integer getOpcionPadre() {
        return opcionPadre;
    }

    public void setOpcionPadre(Integer opcionPadre) {
        this.opcionPadre = opcionPadre;
    }

    public int getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(int idPantalla) {
        this.idPantalla = idPantalla;
    }

    public List<Navega> getListaNavegacion() {
        return listaNavegacion;
    }

    public void setListaNavegacion(List<Navega> listaNavegacion) {
        this.listaNavegacion = listaNavegacion;
    }

    public List<Integer> getListaNroOpcion() {
        return ListaNroOpcion;
    }

    public void setListaNroOpcion(List<Integer> ListaNroOpcion) {
        this.ListaNroOpcion = ListaNroOpcion;
    }

}
