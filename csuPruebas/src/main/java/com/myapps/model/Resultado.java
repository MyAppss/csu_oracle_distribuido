package com.myapps.model;

import java.util.List;

/**
 *
 * @author Usuario
 */
public class Resultado {

    private String Linea;
    private String codigo;
    private String mensaje;
    private List<Pantalla> pantallas;

    /**
     * @return the Linea
     */
    public String getLinea() {
        return Linea;
    }

    /**
     * @param Linea the Linea to set
     */
    public void setLinea(String Linea) {
        this.Linea = Linea;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the pantallas
     */
    public List<Pantalla> getPantallas() {
        return pantallas;
    }

    /**
     * @param pantallas the pantallas to set
     */
    public void setPantallas(List<Pantalla> pantallas) {
        this.pantallas = pantallas;
    }

}
