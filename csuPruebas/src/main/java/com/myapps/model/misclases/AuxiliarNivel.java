package com.myapps.model.misclases;

import java.util.ArrayList;
import java.util.List;

public class AuxiliarNivel {
    private int nivel;
    private int numero;
    private String cabecera;
    private String nombre;
    private List<AuxiliarNivel> hijos = new ArrayList<>();

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<AuxiliarNivel> getHijos() {
        return hijos;
    }

    public void setHijos(List<AuxiliarNivel> hijos) {
        this.hijos = hijos;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }
}
