package com.myapps.model;

public class Navega {

    private int opcion;
    private int pantalla;

    public Navega(int opcion, int pantalla) {
        this.opcion = opcion;
        this.pantalla = pantalla;
    }

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    public int getPantalla() {
        return pantalla;
    }

    public void setPantalla(int pantalla) {
        this.pantalla = pantalla;
    }

}

