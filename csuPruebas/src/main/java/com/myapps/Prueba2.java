package com.myapps;

import com.myapps.model.Navega;
import com.myapps.model.Pantalla;
import com.myapps.model.Resultado;
import com.myapps.model.misclases.AuxiliarNivel;

import java.util.ArrayList;
import java.util.List;

public class Prueba2 {

    public static Resultado myPrueba(Resultado resultado, List<AuxiliarNivel> lista) {
        Resultado valor = new Resultado();
        valor.setLinea(resultado.getLinea());
        valor.setCodigo(resultado.getCodigo());
        valor.setMensaje(resultado.getMensaje());
        List<Pantalla> pantallaList = new ArrayList<>();
        if (lista != null) {

            for (Pantalla myPantalla : resultado.getPantallas()) {
                Pantalla pantallaAuxiliar = procesarPantalla(myPantalla, lista, myPantalla.getIdPantalla(), resultado.getPantallas());
                if (pantallaAuxiliar != null) {
                    pantallaList.add(pantallaAuxiliar);
                }
            }
        }
        valor.setPantallas(pantallaList);

        return valor;
    }

    private static Pantalla procesarPantalla(Pantalla pantalla, List<AuxiliarNivel> lista, int numeroPantalla, List<Pantalla> pantallaList) {
        Pantalla valor = null;

        if (pantalla.getPantallaPadre() == 0) {
            String opciones = obtenerOpciones(lista, 1);
            pantalla.setCabecera(Main.obtenerCabecera(pantalla.getCabecera(), opciones));
            pantalla.setOpciones(opciones);
            pantalla.setListaNroOpcion(obtenerListaOpcion(lista, 1, pantalla.getListaNroOpcion()));
            pantalla.setListaNavegacion(obtenerListaNavega(lista, 1, pantalla.getListaNavegacion()));
            valor = pantalla;
        } else if (pantalla.getPantallaPadre() == 1) {
            String opciones = obtenerOpciones(lista, 2);
            if (!opciones.isEmpty()) {
                pantalla.setCabecera(Main.obtenerCabecera(pantalla.getCabecera(), opciones));
                pantalla.setOpciones(opciones);
                pantalla.setListaNroOpcion(obtenerListaOpcion(lista, 2, pantalla.getListaNroOpcion()));
                pantalla.setListaNavegacion(obtenerListaNavega(lista, 2, pantalla.getListaNavegacion()));
                valor = pantalla;
            } else {
                int contador = numeroPantalla++;
                if (contador <= pantallaList.size()-1) {
                    pantalla.setOpciones(pantallaList.get(contador).getOpciones());
                    List<AuxiliarNivel> listahijos = obtenerListaHijo(pantalla.getOpciones(), lista);
                    pantalla.setCabecera(Main.obtenerCabecera(pantalla.getCabecera(), opciones));
                    pantalla.setListaNroOpcion(obtenerListaOpcion(listahijos, 2, pantalla.getListaNroOpcion()));
                    pantalla.setListaNavegacion(obtenerListaNavega(listahijos, 2, pantalla.getListaNavegacion()));
                    valor = pantalla;
                }
            }
        } else {
        }


        return valor;
    }

    private static String obtenerOpciones(List<AuxiliarNivel> lista, int nivel) {
        StringBuilder valor = new StringBuilder();
        switch (nivel) {
            case 1:
                if (lista != null) {
                    int numero = 1;
                    for (AuxiliarNivel aux : lista) {
                        if (aux.getNivel() == nivel) {
                            if (aux.getHijos().isEmpty()) {
                                valor.append(aux.getNombre());
                            } else {
                                if (numero != 1) {
                                    valor.append("\n");
                                }
                                valor.append(numero + ".-" + aux.getNombre());
                                numero++;
                            }

                        }
                    }
                }
                break;
            case 2:
                break;
            case 3:
                break;
            default:
                break;
        }
        return valor.toString();
    }


    private static List<Integer> obtenerListaOpcion(List<AuxiliarNivel> lista, int nivel, List<Integer> listaOpcion) {
        List<Integer> valor = new ArrayList<>();
        if (lista != null && listaOpcion != null) {
            for (int i = 0; i <= lista.size() - 1; i++) {
                valor.add(listaOpcion.get(i));
            }
        }
        return valor;
    }


    private static List<Navega> obtenerListaNavega(List<AuxiliarNivel> lista, int nivel, List<Navega> listaNavega) {
        List<Navega> valor = new ArrayList<>();
        if (lista != null && listaNavega != null) {
            for (int i = 0; i <= lista.size() - 1; i++) {
                valor.add(listaNavega.get(i));
            }
        }
        return valor;
    }

    private static List<AuxiliarNivel> obtenerListaHijo(String nombre, List<AuxiliarNivel> lista) {
        List<AuxiliarNivel> valor = new ArrayList<>();
        for (AuxiliarNivel a1 : lista) {
            if (nombre.equals(a1.getNombre())) {
                valor = a1.getHijos();
                break;
            }
            for (AuxiliarNivel a2 : a1.getHijos()) {
                if (nombre.equals(a2.getNombre())) {
                    valor = a2.getHijos();
                    break;
                }
            }
        }
        return valor;
    }
}
