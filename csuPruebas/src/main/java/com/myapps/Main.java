package com.myapps;

import com.google.gson.Gson;
import com.myapps.model.Navega;
import com.myapps.model.Pantalla;
import com.myapps.model.Resultado;
import com.myapps.model.misclases.AuxiliarNivel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            String response = "{\"Linea\":\"75690962\",\"codigo\":\"200\",\"mensaje\":\"Consulta Satisfactoria\",\"pantallas\":[{\"idPantalla\":1,\"cabecera\":\"Linea:75690962\\nSaldo Total_D: 12 Bs\\nWhatsapp ilimitado: SI\\n1.-Saldo Paquetigos\\n2.-Otros Saldos\\n3.-myMenu\",\"opciones\":\"1.-Saldo Paquetigos\\n2.-Otros Saldos\\n3.-myMenu\",\"ListaNroOpcion\":[1,2,3],\"listaNavegacion\":[{\"opcion\":1,\"pantalla\":2},{\"opcion\":2,\"pantalla\":3},{\"opcion\":3,\"pantalla\":4}],\"pantallaPadre\":0,\"opcionPadre\":0},{\"idPantalla\":2,\"cabecera\":\"\",\"opciones\":\".\",\"ListaNroOpcion\":[],\"listaNavegacion\":[],\"pantallaPadre\":1,\"opcionPadre\":1},{\"idPantalla\":3,\"cabecera\":\"\",\"opciones\":\"Divertido: 1 Bs activalos hasta el 28/09/2022 23:59:59\\nSMS: 200 sms activalos hasta el 28/09/2022 23:59:59\",\"ListaNroOpcion\":[],\"listaNavegacion\":[],\"pantallaPadre\":1,\"opcionPadre\":2},{\"idPantalla\":4,\"cabecera\":\"\",\"opciones\":\"La locura Automatica\",\"ListaNroOpcion\":[],\"listaNavegacion\":[],\"pantallaPadre\":1,\"opcionPadre\":3}]}";

            Gson gson = new Gson();
            Resultado resultado = gson.fromJson(response, Resultado.class);

            boolean resultado1 = consultaValida(resultado);
            if (Boolean.TRUE.equals(resultado1)) {
                System.out.println("resultado valido");
            } else {
                System.out.println("resultado necesita moificar");
                modificarResultado(resultado);
            }


        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }

    }


    private static boolean consultaValida(Resultado resultado) {
        boolean valor = true;
        if (resultado != null) {
            for (Pantalla pantalla : resultado.getPantallas()) {
                if (pantalla.getOpciones().equals("") || pantalla.getOpciones().equals(".")) {
                    valor = false;
                }
            }
        }
        return valor;
    }

    private static Resultado modificarResultado(Resultado resultado) {
        Resultado valor = null;
        List<AuxiliarNivel> padres = new ArrayList<>();
        if (resultado != null && resultado.getPantallas() != null) {

            for (Pantalla pantalla : resultado.getPantallas()) {
                if (pantalla.getIdPantalla() == 1) {
                    String[] array1 = obtenerArray(pantalla.getOpciones());
                    if (array1 != null) {
                        int i = 1;
                        for (String name : array1) {
                            AuxiliarNivel padre = new AuxiliarNivel();
                            padre.setNivel(1);
                            padre.setNumero(i);
                            padre.setCabecera(pantalla.getCabecera());
                            padre.setNombre(name.split(".-")[1]);
                            padres.add(padre);
                            i++;
                        }
                    }

                } else if (pantalla.getPantallaPadre() == 1) {
                    String[] array2 = obtenerArray(pantalla.getOpciones());
                    int i = 1;
                    for (AuxiliarNivel nivel : padres) {
                        AuxiliarNivel nivel2 = obtenerHijos(array2, nivel.getNumero(), pantalla.getOpcionPadre());
                        if (nivel2 != null) {
                            nivel2.setNivel(2);
                            nivel2.setNumero(i);
                            nivel2.setCabecera(pantalla.getCabecera());

                            nivel.getHijos().add(nivel2);
                            i++;
                        }
                    }

                } else {
                    String[] array3 = obtenerArray(pantalla.getOpciones());
                    int i = 1;
                    for (AuxiliarNivel nivel : padres) {
                        for (AuxiliarNivel nive2 : nivel.getHijos()) {
                            AuxiliarNivel nivel3 = obtenerHijos(array3, nivel.getNumero(), pantalla.getOpcionPadre());
                            if (nivel3 != null) {
                                nivel3.setNivel(3);
                                nivel3.setNumero(i);
                                nivel3.setCabecera(pantalla.getCabecera());
                                nive2.getHijos().add(nivel3);
                                i++;
                            }
                        }
                    }

                }
            }
        }
        System.out.println(padres.size());
        padres = limpiarLista(padres);
        System.out.println(padres.size());
        valor = Prueba2.myPrueba(resultado, padres);

        // valor = crearResultado(padres);
        return valor;
    }

    private static String[] obtenerArray(String opciones) {
        String[] valor = new String[1];
        if (opciones.contains("\\.-")) {
            valor = opciones.split("\n");
        } else {
            valor[0] = opciones;
        }
        return valor;
    }

    private static AuxiliarNivel obtenerHijos(String[] array, int padre, int opcionPadre) {
        AuxiliarNivel valor = null;
        if (array != null) {
            for (String name : array) {
                if (padre == opcionPadre) {
                    valor = new AuxiliarNivel();
                    valor.setNombre(name);
                }
            }
        }

        return valor;
    }

    private static List<AuxiliarNivel> limpiarLista(List<AuxiliarNivel> myLista) {
        List<AuxiliarNivel> valor = new ArrayList<>();
        if (myLista != null) {
            Iterator<AuxiliarNivel> iterator1 = myLista.iterator();
            while (iterator1.hasNext()) {
                AuxiliarNivel nivel1 = iterator1.next();
                nivel1.setHijos(limpiarLista(nivel1.getHijos()));

                    if (nivel1.getNivel() != 1 && nivel1.getNombre().equals(".") || nivel1.getNivel() == 1 && nivel1.getHijos().isEmpty()) {
                        iterator1.remove();

                }
            }
            valor = myLista;
        }
        return valor;
    }


    private static Resultado crearResultado(List<AuxiliarNivel> myLista) {
        Resultado resultado = new Resultado();
        resultado.setLinea("76075808");
        resultado.setCodigo("200");
        resultado.setMensaje("Consulta Satisfactoria");
        List<Pantalla> pantallaList = new ArrayList<>();
        int numeroPantalla = 0;
        //pantalla1
        if (myLista != null) {

            AuxiliarNivel aux = new AuxiliarNivel();
            aux.setNivel(0);
            aux.setNumero(0);
            aux.setHijos(myLista);
            for (AuxiliarNivel nivel : aux.getHijos()) {
                pantallaList = pantalla(nivel, numeroPantalla++, nivel.getNivel(), nivel.getNumero());
            }


        }
        resultado.setPantallas(pantallaList);

        return resultado;
    }


    private static List<Pantalla> pantalla(AuxiliarNivel aux, int numeroPantalla, int pantallaPadre, int opcionPadre) {
        List<Pantalla> lista = new ArrayList<>();
        lista.addAll(armarPantallas(aux, numeroPantalla, pantallaPadre, opcionPadre));
        return lista;
    }

    private static List<Pantalla> armarPantallas(AuxiliarNivel auxiliarList, int numeroPantalla, int pantallaPadre, int opcionPadre) {
        List<Pantalla> pantallaList = new ArrayList<>();
        if (auxiliarList != null) {
            Pantalla pantalla1 = new Pantalla();

            int i = 1;
            String opciones = obtenerOpciones(auxiliarList, i);
            numeroPantalla++;

            pantalla1.setIdPantalla(numeroPantalla);
            pantalla1.setOpciones(opciones);
            pantalla1.setCabecera(obtenerCabecera(auxiliarList.getCabecera(), opciones));

            pantalla1.setListaNroOpcion(obtenerListaOpcion(auxiliarList.getHijos()));
            pantalla1.setListaNavegacion(obtenerListaNavegacion(pantalla1.getListaNroOpcion(), i));

            if (auxiliarList.getNivel() == 1) {
                pantalla1.setPantallaPadre(0);
                pantalla1.setOpcionPadre(0);
            } else {
                pantalla1.setPantallaPadre(pantallaPadre);
                pantalla1.setOpcionPadre(opcionPadre - 1);
            }
            pantallaList.add(pantalla1);
            if (!auxiliarList.getHijos().isEmpty()) {
                pantallaList.addAll(armarPantallas(auxiliarList.getHijos().get(numeroPantalla - 1), numeroPantalla++, auxiliarList.getNivel(), auxiliarList.getNumero()));

            }


        }
        return pantallaList;
    }


    private static String obtenerOpciones(AuxiliarNivel auxiliarList, int numero) {
        String valor = "";
        if (auxiliarList != null) {
            StringBuilder newOpciones = new StringBuilder();
            if (auxiliarList.getHijos().isEmpty()) {
                newOpciones.append(auxiliarList.getNombre());
            } else {
                newOpciones.append(numero + ".-" + auxiliarList.getNombre());
            }
            valor = newOpciones.toString();
        }
        return valor;
    }

    @SuppressWarnings({"unchecked", "deprecated"})
    public static String obtenerCabecera(String myCabecera, String opciones) {
        String valor = "";
        if (myCabecera != null && myCabecera.isEmpty() || opciones != null && opciones.isEmpty()) {
            System.out.println("sin cambios");
        } else {
            String[] arrayOne = myCabecera.split("\n1.-");
            if (arrayOne != null && arrayOne.length == 2) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(arrayOne[0]);
                stringBuilder.append("\n");
                stringBuilder.append(opciones);
                valor = stringBuilder.toString();
            }
        }
        return valor;
    }

    private static List<Integer> obtenerListaOpcion(List<AuxiliarNivel> hijos) {
        List<Integer> myListaOpcion = new ArrayList<>();
        if (hijos != null) {
            for (int i = 1; i <= hijos.size(); i++) {
                myListaOpcion.add(i);
            }
        }
        return myListaOpcion;
    }


    private static List<Navega> obtenerListaNavegacion(List<Integer> myListaOpcion, int numeroPantalla) {
        List<Navega> myListaNavegacion = new ArrayList<>();
        if (myListaOpcion != null) {
            for (Integer entero : myListaOpcion) {
                numeroPantalla = numeroPantalla + 1;
                myListaNavegacion.add(new Navega(entero, numeroPantalla));
            }
        }
        return myListaNavegacion;
    }




}
