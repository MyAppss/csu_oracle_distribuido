package myapps.dao;

import myapps.conexion.DatasourceConexion;
import myapps.model.ReporteConsultaEntity;
import myapps.util.UtilJava;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;


public class ReporteConsultaDAOImplement implements ReporteConsultaDAO {
    private static final Logger logger = LogManager.getLogger(ReporteConsultaDAOImplement.class);


    public void guardarConsulta(ReporteConsultaEntity reporteConsultaEntity, String hilo) {
        String identificador = "Identificador: " + hilo;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            String sql = "INSERT INTO REPORTE_CONSULTA (FECHA,MSISDN,IDENTIFICADOR_SERVICIO,NOMBRE_CANAL,PUBLICIDAD_SOLICITADA,LONGITUD_MAX_SOLICITADA,IP_CLIENTE,TEXTO_GENERADO,SESSIONID) " +
                    "VALUES ( ?,?,?,?,?,?,?,?,?)";
            logger.debug(identificador + " [guardarConsulta] SQL: " + sql);
            conn = DatasourceConexion.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setTimestamp(1, reporteConsultaEntity.getFecha());
                pstmt.setString(2, reporteConsultaEntity.getMsisdn().trim());
                pstmt.setString(3, reporteConsultaEntity.getIdentificadorServicio());
                pstmt.setString(4, reporteConsultaEntity.getNombreCanal());
                pstmt.setString(5, reporteConsultaEntity.getPublicidadSolicitada());
                pstmt.setLong(6, reporteConsultaEntity.getLongitudMaxSolicitada());
                pstmt.setString(7, reporteConsultaEntity.getIpCliente());
                pstmt.setString(8, reporteConsultaEntity.getTextoGenerado());
                pstmt.setString(9, reporteConsultaEntity.getSessionid().trim());
                /* pstmt.setString(10, reporteConsultaEntity.getOpcion());*/
                pstmt.execute();
                logger.debug(identificador + " [guardarConsulta] Guardado el Objeto " + reporteConsultaEntity.toString());
            }
        } catch (SQLIntegrityConstraintViolationException sql) {
            logger.info(identificador + " [guardarConsulta] No se pudo guardar el Objeto " + reporteConsultaEntity.toString());
            logger.warn(identificador + " [guardarConsulta] Error al insertar en la DB SQLException:" + sql.getMessage() + "-" + reporteConsultaEntity.toString(), sql);
            logger.info("SE PROCEDERA A AUMENTAR UN SUFIJO AL ISDN");
            if (reporteConsultaEntity != null && reporteConsultaEntity.getSessionid() != null) {
                String sufijo = "_R" + UtilJava.obtenerUUID();
                logger.info("SE PROCEDERA A AUMENTAR EL SUFIJO: " + sufijo + " A LA PETICION: " + reporteConsultaEntity.getSessionid());
                reporteConsultaEntity.setSessionid(reporteConsultaEntity.getSessionid() + sufijo);

            }

        } catch (SQLException e) {
            logger.info(identificador + " [guardarConsulta] No se pudo guardar el Objeto " + reporteConsultaEntity.toString());
            logger.error(identificador + " [guardarConsulta] Error al insertar en la DB SQLException:" + e.getMessage() + "-" + reporteConsultaEntity.toString(), e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    logger.warn(identificador + " [guardarConsulta] Error al intentar cerrar ", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.warn(identificador + " [guardarConsulta] Error al intentar cerrar:", ex);
                }
            }
        }

    }


}
