package myapps.dao;

import myapps.model.ReporteConsultaEntity;

public interface ReporteConsultaDAO {
    void guardarConsulta(ReporteConsultaEntity reporteConsultaEntity, String hilo);
}