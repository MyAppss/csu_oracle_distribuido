package myapps.conexion;

import myapps.util.Parametros;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.sql.Connection;
import java.sql.SQLException;

public class DatasourceConexion {
    private static final Logger LOG = Logger.getLogger(DatasourceConexion.class);

    private static final String HOST = Parametros.BD_SERVER;
    private static final String DB = Parametros.BD_NAME;
    private static final int PORT = Parametros.BD_PORT;
    private static final String USER = Parametros.BD_USER;
    private static final String PASSWORD = Parametros.BD_PASS;
    public static DataSource datasource;


    private DatasourceConexion() {
    }

    public static synchronized Connection getConnection() throws SQLException {
        try {
            if (datasource == null) {
                datasource = setupDataSource();
            }
        } catch (Exception e) {
            LOG.error("Error al conectar a la bd " + Parametros.BD_SERVER);
        }

        return datasource.getConnection();
    }


    private static DataSource setupDataSource() {

        PoolProperties p = new PoolProperties();
        String url = "jdbc:oracle:thin:@" + HOST + ":" + PORT + "/" + DB;
        p.setUrl(url);
        p.setDriverClassName("oracle.jdbc.OracleDriver");
        p.setUsername(USER);
        p.setPassword(PASSWORD);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setTestOnReturn(false);
        p.setValidationQuery("SELECT 1 FROM DUAL");
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMinEvictableIdleTimeMillis(Parametros.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS);
        p.setMaxActive(Parametros.DB_MAX_ACTIVE);
        p.setMinIdle(Parametros.DB_MIN_IDLE);
        p.setMaxIdle(Parametros.DB_MAX_IDLE);
        p.setMaxWait(Parametros.DB_MAX_WAIT);
        p.setInitialSize(2);
        p.setRemoveAbandonedTimeout(60);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(false);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource dataSource = new DataSource();
        dataSource.setPoolProperties(p);


        return dataSource;
    }

}
