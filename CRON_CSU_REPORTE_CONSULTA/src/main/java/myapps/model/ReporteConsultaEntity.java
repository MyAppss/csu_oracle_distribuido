package myapps.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class ReporteConsultaEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private long idRc;
    private Timestamp fecha;
    private String msisdn;
    private String identificadorServicio;
    private String nombreCanal;
    private String publicidadSolicitada;
    private Long longitudMaxSolicitada;
    private String ipCliente;
    private String textoGenerado;
    private String sessionid;
    private String opcion;


    public long getIdRc() {
        return idRc;
    }

    public void setIdRc(long idRc) {
        this.idRc = idRc;
    }


    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }


    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }


    public String getIdentificadorServicio() {
        return identificadorServicio;
    }

    public void setIdentificadorServicio(String identificadorServicio) {
        this.identificadorServicio = identificadorServicio;
    }


    public String getNombreCanal() {
        return nombreCanal;
    }

    public void setNombreCanal(String nombreCanal) {
        this.nombreCanal = nombreCanal;
    }


    public String getPublicidadSolicitada() {
        return publicidadSolicitada;
    }

    public void setPublicidadSolicitada(String publicidadSolicitada) {
        this.publicidadSolicitada = publicidadSolicitada;
    }


    public Long getLongitudMaxSolicitada() {
        return longitudMaxSolicitada;
    }

    public void setLongitudMaxSolicitada(Long longitudMaxSolicitada) {
        this.longitudMaxSolicitada = longitudMaxSolicitada;
    }


    public String getIpCliente() {
        return ipCliente;
    }

    public void setIpCliente(String ipCliente) {
        this.ipCliente = ipCliente;
    }


    public String getTextoGenerado() {
        return textoGenerado;
    }

    public void setTextoGenerado(String textoGenerado) {
        this.textoGenerado = textoGenerado;
    }


    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }


    @Override
    public String toString() {
        return "ReporteConsultaEntity{" +
                "idRc=" + idRc +
                ", fecha=" + fecha +
                ", msisdn='" + msisdn + '\'' +
                ", identificadorServicio='" + identificadorServicio + '\'' +
                ", nombreCanal='" + nombreCanal + '\'' +
                ", publicidadSolicitada='" + publicidadSolicitada + '\'' +
                ", longitudMaxSolicitada=" + longitudMaxSolicitada +
                ", ipCliente='" + ipCliente + '\'' +
                ", textoGenerado='" + textoGenerado + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", opcion='" + opcion + '\'' +
                '}';
    }
}
