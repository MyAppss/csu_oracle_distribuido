package myapps.model;

import java.io.Serializable;
import java.sql.Timestamp;


public class DetalleReporteConsultaEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private long idDetalle;
    private String msisdn;
    private String sessionid;
    private String opcion;
    private Timestamp fecha;


    public long getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(long idDetalle) {
        this.idDetalle = idDetalle;
    }


    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }


    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }


    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }


    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }


}
