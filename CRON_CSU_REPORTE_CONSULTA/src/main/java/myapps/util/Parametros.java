package myapps.util;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Parametros {
    private static final Logger log = Logger.getLogger(Parametros.class);

    public static String BD_SERVER;
    public static String BD_NAME;
    public static int BD_PORT;
    public static String BD_USER;
    public static String BD_PASS;
    public static int DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS;
    public static int DB_MAX_ACTIVE;
    public static int DB_MIN_IDLE;
    public static int DB_MAX_IDLE;
    public static int DB_MAX_WAIT;

    public static String MQ_USER;
    public static String MQ_PASSWORD;
    public static String MQ_COLA;
    public static String MQ_IP;
    public static int MQ_PUERTO;
    public static String MQ_TIPO_CONEXION;
    public static int MQ_TIMEOUT;


    public static int TIME = 0;
    public static int HILOS = 0;
    private static boolean parameters_is_loaders = false;

    static {
        init();
    }


    public static synchronized void init() {


        if (parameters_is_loaders) {
            return;
        }
        try (InputStream inpustream = Parametros.class.getClassLoader().getResourceAsStream("cronReporteConsulta.properties");) {
            log.info("Iniciando Cargado de Parametros");
            Properties prop = new Properties();
            prop.load(inpustream);


            Parametros.BD_SERVER = prop.getProperty("bd.servidor");
            Parametros.BD_NAME = prop.getProperty("bd.nombre");
            Parametros.BD_PORT = Integer.parseInt(prop.getProperty("bd.puerto"));
            Parametros.BD_USER = prop.getProperty("bd.usuario");
            Parametros.BD_PASS = prop.getProperty("bd.password");
            Parametros.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = Integer.parseInt(prop.getProperty("bd.setminevictableidletimemillis"));
            Parametros.DB_MAX_ACTIVE = Integer.parseInt(prop.getProperty("bd.setmaxactive"));
            Parametros.DB_MIN_IDLE = Integer.parseInt(prop.getProperty("bd.setminidle"));
            Parametros.DB_MAX_IDLE = Integer.parseInt(prop.getProperty("bd.setMaxIdle"));
            Parametros.DB_MAX_WAIT = Integer.parseInt(prop.getProperty("bd.setMaxWait"));

            Parametros.MQ_USER = prop.getProperty("MQ.user");
            Parametros.MQ_PASSWORD = prop.getProperty("MQ.password");
            Parametros.MQ_COLA = prop.getProperty("MQ.nombreCola");
            Parametros.MQ_IP = prop.getProperty("MQ.servidorIP");
            Parametros.MQ_PUERTO = Integer.parseInt(prop.getProperty("MQ.servidorPuerto"));
            Parametros.MQ_TIPO_CONEXION = prop.getProperty("MQ.tipoCOnexion");
            Parametros.MQ_TIMEOUT = Integer.parseInt(prop.getProperty("MQ.timeout"));

            Parametros.TIME = Integer.parseInt("100");
            Parametros.HILOS = Integer.parseInt(prop.getProperty("MQ.hilosConsumidores"));
            parameters_is_loaders = true;
            log.info("Finalizado Cargado de Parametros");

        } catch (IOException e) {
            log.error("Lastimosamente a ocurrido un error al leer el archivo config.properties " + e.getMessage());
        }

    }


}
