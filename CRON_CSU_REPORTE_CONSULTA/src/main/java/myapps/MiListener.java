/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps;

import myapps.dao.ReporteConsultaDAO;
import myapps.dao.ReporteConsultaDAOImplement;
import myapps.util.Parametros;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.UUID;


@WebListener
public class MiListener implements ServletContextListener {
    private Logger log = Logger.getLogger(MiListener.class);
    ReporteConsultaDAO dao = new ReporteConsultaDAOImplement();
    private ThreadGroup workers;


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.log(Level.INFO, "========App " + MiListener.class.getSimpleName() + " is starting...!============");
        Controlador.getInstance().setValor(true);
        workers = new ThreadGroup(MiListener.class.getSimpleName() + "-" + System.currentTimeMillis());
        startUpMessage();
        log.log(Level.INFO, "========App " + MiListener.class.getSimpleName() + " is start up!============");

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.log(Level.INFO, "========ShutDown============");
        Controlador.getInstance().setValor(false);
        if (workers != null) workers.interrupt();
        log.log(Level.INFO, "========Stopped============");
    }

    private void startUpMessage() {
        try {
            int pool = Parametros.HILOS;
            for (int i = 1; i <= pool; i++) {
                String identificador = UUID.randomUUID().toString();
                ConsultasEventMessageListener consultasEventMessageListener = new ConsultasEventMessageListener(workers,identificador, dao);
                consultasEventMessageListener.iniciar();
                log.log(Level.INFO, "Iniciando hilo: " + i + " con identificador: " + identificador);
            }
            log.log(Level.INFO, "Complete");
        } catch (Exception e) {
            log.log(Level.ERROR, "Exception: " + e.getMessage(), e);
        }
    }


}
