/*
 Navicat Premium Data Transfer

 Source Server         : CSU DISTRIBUIDO ORACLE 19C
 Source Server Type    : Oracle
 Source Server Version : 190000
 Source Host           : 172.30.32.15:1521
 Source Schema         : CSU_DISTRIBUIDO

 Target Server Type    : Oracle
 Target Server Version : 190000
 File Encoding         : 65001

 Date: 13/03/2020 16:19:43
*/


-- ----------------------------
-- Table structure for DETALLE_REPORTE_CONSULTA
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."DETALLE_REPORTE_CONSULTA";
CREATE TABLE "CSU_DISTRIBUIDO"."DETALLE_REPORTE_CONSULTA" (
  "ID_DETALLE" NUMBER(8,2) VISIBLE NOT NULL ,
  "MSISDN" VARCHAR2(50 BYTE) VISIBLE ,
  "SESSIONID" CHAR(50 BYTE) VISIBLE ,
  "OPCION" CHAR(100 BYTE) VISIBLE ,
  "FECHA" TIMESTAMP(6) VISIBLE 
)
NOLOGGING
NOCOMPRESS

STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of DETALLE_REPORTE_CONSULTA
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."DETALLE_REPORTE_CONSULTA" VALUES ('0', '7675808', '123                                               ', 'hhh                                                                                                 ', TO_TIMESTAMP('2020-03-12 13:56:05.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Primary Key structure for table DETALLE_REPORTE_CONSULTA
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."DETALLE_REPORTE_CONSULTA" ADD CONSTRAINT "SYS_C0012682" PRIMARY KEY ("ID_DETALLE");

-- ----------------------------
-- Checks structure for table DETALLE_REPORTE_CONSULTA
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."DETALLE_REPORTE_CONSULTA" ADD CONSTRAINT "SYS_C0012669" CHECK ("ID_DETALLE" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
