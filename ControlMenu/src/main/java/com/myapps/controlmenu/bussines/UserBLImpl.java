/*    */
package com.myapps.controlmenu.bussines;
/*    */
/*    */

import com.google.inject.Inject;
/*    */ import com.myapps.controlmenu.dao.GroupDAO;
/*    */ import com.myapps.controlmenu.dao.RolFormularioDAO;
/*    */ import com.myapps.controlmenu.dao.UserDAO;
/*    */ import com.myapps.controlmenu.model.GrupoAd;
/*    */ import com.myapps.controlmenu.model.Usuario;
/*    */ import java.util.List;

/*    */
/*    */ public class UserBLImpl
        /*    */ implements UserBL
        /*    */ {
    /*    */
    @Inject
    /*    */ private UserDAO dao;
    /*    */
    @Inject
    /*    */ private GroupDAO daoGr;
    /*    */
    @Inject
    /*    */ private RolFormularioDAO daoRF;

    /*    */
    /*    */
    public Usuario getUser(int user_id) {
        /* 22 */
        return this.dao.getUserId(user_id);
        /*    */
    }

    /*    */
    /*    */
    /*    */
    public List<Usuario> getUsers() {
        /* 27 */
        return this.dao.getUsers();
        /*    */
    }

    /*    */
    /*    */
    /*    */
    public void updateUser(Usuario user) {
        /* 32 */
        this.dao.updateUser(user);
        /*    */
    }

    /*    */
    /*    */
    /*    */
    public void insertUser(Usuario user) {
        /* 37 */
        user.setEstado("t");
        /* 38 */
        this.dao.insertUser(user);
        /*    */
    }

    /*    */
    /*    */
    /*    */
    public void deleteUser(int user_id) {
        /* 43 */
        this.dao.deleteUser(user_id);
        /*    */
    }

    /*    */
    /*    */
    /*    */
    public String validate(Usuario user, String idStr) {
        /* 48 */
        if (idStr != null && !idStr.isEmpty() && Integer.parseInt(idStr) == 1) {
            /* 49 */
            return "El Usuario no se puede modificar es de Administracion";
            /*    */
        }
        /* 51 */
        if (user.getLogin().isEmpty()) {
            /* 52 */
            return "El campo Login esta Vacio";
            /*    */
        }
        /* 54 */
        Usuario usAux = this.dao.getUserName(user.getLogin());
        /* 55 */
        if (usAux == null) {
            /* 56 */
            return "";
            /*    */
        }
        /* 58 */
        if (idStr != null && !idStr.isEmpty()) {
            /* 59 */
            int id = Integer.parseInt(idStr);
            /* 60 */
            if (id == 1) {
                /* 61 */
                return "El Usuario no se puede modificar es de Administracion";
                /*    */
            }
            /* 63 */
            if (id == usAux.getUsuarioId().intValue() && user.getLogin().equals(usAux.getLogin())) {
                /* 64 */
                return "";
                /*    */
            }
            /*    */
        }
        /* 67 */
        return "este login existe";
        /*    */
    }


    public int getIdRole(String login, List userGroups) {
        Usuario user = this.dao.getUserName(login);
        if (user != null) {
            return user.getRolId().intValue();
        }
        for (Object object : userGroups) {
            GrupoAd gr = this.daoGr.getGroupName(object.toString());
            if (gr != null) {
                return gr.getRolId().intValue();
            }
        }
        return -1;
    }
}


