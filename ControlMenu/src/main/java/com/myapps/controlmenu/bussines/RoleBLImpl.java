package com.myapps.controlmenu.bussines;

import com.google.inject.Inject;
import com.myapps.controlmenu.dao.*;

import com.myapps.controlmenu.model.Formulario;
import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.model.RolFormulario;

import java.util.ArrayList;
import java.util.List;

public class RoleBLImpl implements RoleBL {

    @Inject
    private RoleDAO dao;

    @Inject
    private FormularioBL daoFor;

    @Inject
    private GroupDAO daoGroup;

    @Inject
    private RolFormularioDAO daoRolFor;

    @Inject
    private UserDAO daoUser;


    public String validate(Rol role, String idStr) {

        if (idStr != null && !idStr.isEmpty() && Integer.parseInt(idStr) == 1) {
            return "Este ROL no se puede modificar es de Administracion";
        }

        if (role.getNombre().isEmpty()) {
            return "El campo Nombre esta Vacio";
        }

        Rol rolAux = this.dao.getRoleName(role.getNombre());
        if (rolAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {

            int id = Integer.parseInt(idStr);
            if (id == 1) {
                return "Este ROL no se puede modificar es de Administracion";
            }
            if (id == rolAux.getRolId().intValue() && role.getNombre().equals(rolAux.getNombre())) {
                return "";
            }
        }
        return "este nombre existe";
    }

    public String validateDelete(int roleId) {
        if (roleId == 1) {
            return "Este ROL no se puede eliminar es de Administracion";
        }
        if (!this.daoUser.getUsersDeleteRole(roleId).isEmpty()) {
            return "Existen Usuarios relacionados a este ROL.";
        }
        if (this.daoGroup.getGroupsDeleteRole(roleId).isEmpty()) {
            return "";
        }
        return "Existen Grupos relacionados a este ROL.";
    }

    public Rol getRole(int roleId) {
        return this.dao.getRoleId(roleId);
    }

    public List<Rol> getRoles() {
        return this.dao.getRoles();
    }

    public void insertRole(Rol role) {
        role.setEstado("t");
        Integer lastId = this.dao.getLastId();
        role.setRolId(lastId);
        this.dao.insertRole(role);
        for (Formulario formulario : this.daoFor.getFormularios()) {
            RolFormulario rolFor = new RolFormulario();
            rolFor.setRolId(role.getRolId());
            rolFor.setFormularioId(formulario.getFormularioId());
            rolFor.setEstado("t");
            this.daoRolFor.insert(rolFor);
        }
    }


    public void updateRole(Rol role) {
        this.dao.updateRole(role);
    }

    public void deleteRole(int roleId) {
        this.dao.deleteRole(roleId);
    }


    public List<RolFormulario> getListRolFormulario(int idRol) {
        return this.daoRolFor.getList(idRol);
    }


    public void updateRoleFormulario(RolFormulario roleFor) {
        this.daoRolFor.update(roleFor);
    }


    public void updateRoleFormularioList(List<String> listaAvil, int idRol) {
        List<RolFormulario> listRolFor = getFormularios();
        List<RolFormulario> listaValida = new ArrayList<>();

        for (String grupo : listaAvil) {
            for (RolFormulario rolFormulario : listRolFor) {
                rolFormulario.setEstado("f");
                rolFormulario.setRolId(idRol);
                daoRolFor.update(rolFormulario);
                if (grupo.equals(rolFormulario.getName())) {
                    listaValida.add(rolFormulario);
                }
            }

        }


        for (RolFormulario rolFormularioValido : listaValida) {
            rolFormularioValido.setEstado("t");
            daoRolFor.update(rolFormularioValido);
            for (RolFormulario rolFormulario : listRolFor) {
                if (rolFormulario.getDepende() != null && rolFormulario.getDepende().equals(rolFormularioValido.getFormularioId())) {
                    rolFormulario.setEstado("t");
                    daoRolFor.update(rolFormulario);
                }

                if (rolFormularioValido.getNivel() != null && rolFormularioValido.getNivel().contains(".")) {
                    String idNivel = String.valueOf(rolFormularioValido.getNivel().charAt(0));
                    int id = Integer.parseInt(idNivel);
                    if (id == rolFormulario.getFormularioId()) {
                        rolFormulario.setEstado("t");
                        daoRolFor.update(rolFormulario);
                    }
                }

            }
        }




        /*
        List<RolFormulario> listRolFor = getListRolFormulario(idRol);


        Map<Integer, RolFormulario> mapRolForIdForm = new HashMap<>();
        Map<String, RolFormulario> mapRolForName = new HashMap<>();
        Map<String, RolFormulario> mapRolForNivel = new HashMap<>();
        List<RolFormulario> listRFAux = new ArrayList<>();
        List<RolFormulario> listRFDepende = new ArrayList<>();
        for (RolFormulario rolFor : listRolFor) {
             if (rolFor.getNivel() != null && rolFor.getNivel().isEmpty()) {
          //  if (rolFor.getNivel() == null || rolFor.getNivel().isEmpty()) {
                listRFDepende.add(rolFor);
            } else {
                mapRolForName.put(rolFor.getName(), rolFor);
                mapRolForNivel.put(rolFor.getNivel(), rolFor);
                listRFAux.add(0, rolFor);
            }
            mapRolForIdForm.put(rolFor.getFormularioId(), rolFor);
        }
        for (RolFormulario rolFor2 : listRFAux) {
            String name = rolFor2.getName();
            if (listaAvil.indexOf(name) != -1) {
                String str = (mapRolForName.get(name)).getNivel();
                int k = str.lastIndexOf(".");
                if (k != -1) {
                    name = (mapRolForNivel.get(str.substring(0, k))).getName();
                    if (listaAvil.indexOf(name) == -1) {
                        listaAvil.add(name);
                    }
                }
            }
        }
        for (RolFormulario rolFor22 : listRFAux) {
            if (listaAvil.indexOf(rolFor22.getName()) != -1) {
                rolFor22.setEstado("t");
            } else {
                rolFor22.setEstado("f");
            }
            this.daoRolFor.update(rolFor22);
        }

        for (RolFormulario rolFor222 : listRFDepende) {
            //  while (true) {
            RolFormulario rolForAux = mapRolForIdForm.get(rolFor222.getDepende());
            if (rolForAux.getDepende().intValue() == 0) {
                rolFor222.setEstado(rolForAux.getEstado());
                this.daoRolFor.update(rolFor222);
            }
            // }
        }*/
    }

    public List<RolFormulario> getFormularios() {
        return this.daoRolFor.getFormularios();
    }

}


