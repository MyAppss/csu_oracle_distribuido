package com.myapps.controlmenu.bussines;

import com.myapps.controlmenu.model.Usuario;

import java.util.List;

public interface UserBL {
  void deleteUser(int paramInt);
  
  int getIdRole(String paramString, List paramList);
  
  Usuario getUser(int paramInt);
  
  List<Usuario> getUsers();
  
  void insertUser(Usuario paramUsuario);
  
  void updateUser(Usuario paramUsuario);
  
  String validate(Usuario paramUsuario, String paramString);
}

