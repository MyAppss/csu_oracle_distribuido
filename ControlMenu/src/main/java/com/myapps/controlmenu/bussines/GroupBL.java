package com.myapps.controlmenu.bussines;


import com.myapps.controlmenu.model.GrupoAd;

import java.util.List;

public interface GroupBL {
    void deleteGroup(int paramInt);

    GrupoAd getGroup(int paramInt);

    List<GrupoAd> getGroups();

    void insertGroup(GrupoAd paramGrupoAd);

    void updateGroup(GrupoAd paramGrupoAd);

    String validate(GrupoAd paramGrupoAd, String paramString);
}


