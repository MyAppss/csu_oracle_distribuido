/*    */ package com.myapps.controlmenu.bussines;
/*    */ 
/*    */ import com.google.inject.Inject;
import com.myapps.controlmenu.dao.GroupDAO;
import com.myapps.controlmenu.model.GrupoAd;

/*    */ import java.util.List;
/*    */ 
/*    */ public class GroupBLImpl
/*    */   implements GroupBL
/*    */ {
/*    */   @Inject
/*    */   private GroupDAO dao;
/*    */   
/*    */   public String validate(GrupoAd user, String idStr) {
/* 15 */     if (user.getNombre().isEmpty()) {
/* 16 */       return "El campo Nombre esta Vacio";
/*    */     }
/* 18 */     GrupoAd usAux = this.dao.getGroupName(user.getNombre());
/* 19 */     if (usAux == null) {
/* 20 */       return "";
/*    */     }
/* 22 */     if (idStr == null || idStr.isEmpty() || Integer.parseInt(idStr) != usAux.getGrupoId().intValue() || !user.getNombre().equals(usAux.getNombre())) {
/* 23 */       return "este nombre de grupo ya existe";
/*    */     }
/* 25 */     return "";
/*    */   }
/*    */ 
/*    */   
/*    */   public GrupoAd getGroup(int Group_id) {
/* 30 */     return this.dao.getGroupId(Group_id);
/*    */   }
/*    */ 
/*    */   
/*    */   public List<GrupoAd> getGroups() {
/* 35 */     return this.dao.getGroups();
/*    */   }
/*    */ 
/*    */   
/*    */   public void updateGroup(GrupoAd group) {
/* 40 */     this.dao.updateGroup(group);
/*    */   }
/*    */ 
/*    */   
/*    */   public void insertGroup(GrupoAd group) {
/* 45 */     group.setEstado("t");
/* 46 */     this.dao.insertGroup(group);
/*    */   }
/*    */ 
/*    */   
/*    */   public void deleteGroup(int fiel_id) {
/* 51 */     this.dao.deleteGroup(fiel_id);
/*    */   }
/*    */ }


/* Location:              D:\myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\bussines\GroupBLImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */