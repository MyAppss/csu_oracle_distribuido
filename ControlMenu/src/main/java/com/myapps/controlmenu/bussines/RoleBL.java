package com.myapps.controlmenu.bussines;


import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.model.RolFormulario;

import java.util.List;

public interface RoleBL {
    void deleteRole(int paramInt);

    List<RolFormulario> getListRolFormulario(int paramInt);

    Rol getRole(int paramInt);

    List<Rol> getRoles();

    void insertRole(Rol paramRol);

    void updateRole(Rol paramRol);

    void updateRoleFormulario(RolFormulario paramRolFormulario);

    void updateRoleFormularioList(List<String> paramList, int paramInt);

    String validate(Rol paramRol, String paramString);

    String validateDelete(int paramInt);

    List<RolFormulario> getFormularios();
}


