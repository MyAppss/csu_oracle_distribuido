package com.myapps.controlmenu.bussines;


import com.myapps.controlmenu.model.Bitacora;

import java.util.List;

public interface BitacoraBL {
  void accionDelete(String paramString1, String paramString2, Enum paramEnum, String paramString3, String paramString4);
  
  void accionInsert(String paramString1, String paramString2, Enum paramEnum, String paramString3, String paramString4);
  
  void accionUpdate(String paramString1, String paramString2, Enum paramEnum, String paramString3, String paramString4);
  
  List<Bitacora> getLogWebs();
  
  void insertLogWeb(Bitacora paramBitacora);
  
  void insertLogWebTimeOut(Bitacora paramBitacora);
}


