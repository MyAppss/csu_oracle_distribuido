package com.myapps.controlmenu.bussines;

import com.google.inject.Inject;
import com.myapps.controlmenu.dao.BitacoraDAO;
import com.myapps.controlmenu.model.Bitacora;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public class BitacoraBLImpl implements BitacoraBL {

    @Inject
    private BitacoraDAO dao;


    public List<Bitacora> getLogWebs() {

        return this.dao.getLogWebs();

    }

    public void insertLogWeb(Bitacora logWeb) {

        logWeb.setStartDate(new Timestamp((new Date()).getTime()));

        this.dao.insertLogWeb(logWeb);

    }


    public void insertLogWebTimeOut(Bitacora logWeb) {

        this.dao.insertLogWeb(logWeb);

    }


    public void accionInsert(String user, String ip, Enum dato, String id, String name) {

        String ele = dato.name();

        String formulario = dato.toString();

        this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se adiciono:" + ele + " con Id:" + id + "; Nombre:" + name, ip);

    }


    public void accionDelete(String user, String ip, Enum dato, String id, String name) {

        String ele = dato.name();

        String formulario = dato.toString();

        this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se elimino:" + ele + " con Id:" + id + "; Nombre:" + name, ip);

    }


    public void accionUpdate(String user, String ip, Enum dato, String id, String name) {

        String ele = dato.name();

        String formulario = dato.toString();

        this.dao.insertLogWebBase(new Timestamp((new Date()).getTime()), user, formulario, "Se modifico:" + ele + " con Id:" + id + "; Nombre:" + name, ip);

    }

}


