package com.myapps.controlmenu.bussines;

import com.google.inject.Inject;
import com.myapps.controlmenu.dao.FormularioDAO;
import com.myapps.controlmenu.model.Formulario;

import java.util.List;


public class FormularioBLImpl implements FormularioBL {
    @Inject
    private FormularioDAO dao;

    public List<Formulario> getFormularios() {
        return this.dao.getFormularios();
    }
}
