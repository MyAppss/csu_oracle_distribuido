package com.myapps.controlmenu.bussines;

import com.myapps.controlmenu.model.Formulario;

import java.util.List;

public interface FormularioBL {
    List<Formulario> getFormularios();
}
