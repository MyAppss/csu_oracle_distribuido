/*    */ package com.myapps.controlmenu.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class Formulario
/*    */   implements Serializable {
/*    */   private Boolean estado;
/*    */   private Integer formularioId;
/*    */   private String nombre;
/*    */   private Integer raiz;
/*    */   private String url;
/*    */   
/*    */   public Integer getFormularioId() {
/* 14 */     return this.formularioId;
/*    */   }
/*    */   
/*    */   public void setFormularioId(Integer formularioId) {
/* 18 */     this.formularioId = formularioId;
/*    */   }
/*    */   
/*    */   public Boolean getEstado() {
/* 22 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(Boolean estado) {
/* 26 */     this.estado = estado;
/*    */   }
/*    */   
/*    */   public String getNombre() {
/* 30 */     return this.nombre;
/*    */   }
/*    */   
/*    */   public void setNombre(String nombre) {
/* 34 */     this.nombre = nombre;
/*    */   }
/*    */   
/*    */   public String getUrl() {
/* 38 */     return this.url;
/*    */   }
/*    */   
/*    */   public void setUrl(String url) {
/* 42 */     this.url = url;
/*    */   }
/*    */   
/*    */   public Integer getRaiz() {
/* 46 */     return this.raiz;
/*    */   }
/*    */   
/*    */   public void setRaiz(Integer raiz) {
/* 50 */     this.raiz = raiz;
/*    */   }
/*    */ }


/* Location:              D:\myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\Formulario.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */