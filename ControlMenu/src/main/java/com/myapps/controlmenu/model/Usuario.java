package com.myapps.controlmenu.model;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String estado;
    private String login;
    private String nameRole;
    private String nombre;
    private Integer rolId;
    private Integer usuarioId;

    public Integer getUsuarioId() {
        return this.usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getRolId() {
        return this.rolId;
    }

    public void setRolId(Integer rolId) {
        this.rolId = rolId;
    }

    public String getNameRole() {
        return this.nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    public Usuario(String estado, String login, String nameRole, String nombre, Integer rolId, Integer usuarioId) {
        this.estado = estado;
        this.login = login;
        this.nameRole = nameRole;
        this.nombre = nombre;
        this.rolId = rolId;
        this.usuarioId = usuarioId;
    }

    public Usuario() {
    }
}



