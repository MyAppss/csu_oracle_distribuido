/*    */ package com.myapps.controlmenu.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class GrupoAd
/*    */   implements Serializable
/*    */ {
/*    */   private String detalle;
/*    */   private String estado;
/*    */   private Integer grupoId;
/*    */   private String nameRole;
/*    */   private String nombre;
/*    */   private Integer rolId;
/*    */   
/*    */   public GrupoAd() {}
/*    */   
/*    */   public GrupoAd(String detalle, String estado, Integer grupoId, String nameRole, String nombre, Integer rolId) {
/* 18 */     this.detalle = detalle;
/* 19 */     this.estado = estado;
/* 20 */     this.grupoId = grupoId;
/* 21 */     this.nameRole = nameRole;
/* 22 */     this.nombre = nombre;
/* 23 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public Integer getGrupoId() {
/* 27 */     return this.grupoId;
/*    */   }
/*    */   
/*    */   public void setGrupoId(Integer grupoId) {
/* 31 */     this.grupoId = grupoId;
/*    */   }
/*    */   
/*    */   public String getDetalle() {
/* 35 */     return this.detalle;
/*    */   }
/*    */   
/*    */   public void setDetalle(String detalle) {
/* 39 */     this.detalle = detalle;
/*    */   }
/*    */   
/*    */   public String getEstado() {
/* 43 */     return this.estado;
/*    */   }
/*    */   
/*    */   public void setEstado(String estado) {
/* 47 */     this.estado = estado;
/*    */   }
/*    */   
/*    */   public String getNombre() {
/* 51 */     return this.nombre;
/*    */   }
/*    */   
/*    */   public void setNombre(String nombre) {
/* 55 */     this.nombre = nombre;
/*    */   }
/*    */   
/*    */   public Integer getRolId() {
/* 59 */     return this.rolId;
/*    */   }
/*    */   
/*    */   public void setRolId(Integer rolId) {
/* 63 */     this.rolId = rolId;
/*    */   }
/*    */   
/*    */   public String getNameRole() {
/* 67 */     return this.nameRole;
/*    */   }
/*    */   
/*    */   public void setNameRole(String nameRole) {
/* 71 */     this.nameRole = nameRole;
/*    */   }
/*    */ }


/* Location:              D:\myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\model\GrupoAd.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */