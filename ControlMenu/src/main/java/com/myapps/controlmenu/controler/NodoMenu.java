/*    */
package com.myapps.controlmenu.controler;

/*    */
/*    */ public class NodoMenu
        /*    */ {
    /*    */   private String name;
    /*    */   private String pather;
    /*    */   private boolean swRendered;
    /*    */   private int tipo;
    /*    */   private String url;

    /*    */
    /*    */
    public String getName() {
        /* 12 */
        return this.name;
        /*    */
    }

    /*    */
    /*    */
    public void setName(String name) {
        /* 16 */
        this.name = name;
        /*    */
    }

    /*    */
    /*    */
    public String getUrl() {
        /* 20 */
        return this.url;
        /*    */
    }

    /*    */
    /*    */
    public void setUrl(String url) {
        /* 24 */
        this.url = url;
        /*    */
    }

    /*    */
    /*    */
    public int getTipo() {
        /* 28 */
        return this.tipo;
        /*    */
    }

    /*    */
    /*    */
    public void setTipo(int tipo) {
        /* 32 */
        this.tipo = tipo;
        /*    */
    }

    /*    */
    /*    */
    public boolean isSwRendered() {
        /* 36 */
        return this.swRendered;
        /*    */
    }

    /*    */
    /*    */
    public void setSwRendered(boolean swRendered) {
        /* 40 */
        this.swRendered = swRendered;
        /*    */
    }

    /*    */
    /*    */
    public String getPather() {
        /* 44 */
        return this.pather;
        /*    */
    }

    /*    */
    /*    */
    public void setPather(String pather) {
        /* 48 */
        this.pather = pather;
        /*    */
    }
    /*    */
}


