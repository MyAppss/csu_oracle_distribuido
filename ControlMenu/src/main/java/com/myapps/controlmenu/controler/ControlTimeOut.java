package com.myapps.controlmenu.controler;

public interface ControlTimeOut {
  void addNodoClient(NodoClient paramNodoClient);
  
  void addRutaBase(String paramString);
  
  void clearUserCliente();
  
  int countIntentoUserIngreso(String paramString);
  
  void createNewClient(String paramString1, String paramString2);
  
  void deleteUserIngreso(String paramString);
  
  boolean exisUserIngreso(String paramString);
  
  String getAddressIP(String paramString);
  
  NodoClient getNodoClient(String paramString);
  
  NodoIngresoUser getNodoIngresoUser(String paramString);
  
  void ingrementIntentoUser(String paramString);
  
  void insertUserIngreso(String paramString);
  
  boolean isPaginaUsuario(String paramString1, String paramString2);
  
  void registerOutTime(long paramLong);
  
  void remove(String paramString);
  
  void setDatos(String paramString, long paramLong);
}


/* Location:              D:\myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\controler\ControlTimeOut.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */