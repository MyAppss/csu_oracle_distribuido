/*     */
package com.myapps.controlmenu.controler;
/*     */
/*     */

import com.myapps.controlmenu.bussines.BitacoraBL;
/*     */ import com.myapps.controlmenu.bussines.RoleBL;
/*     */ import com.myapps.controlmenu.bussines.UserBL;
/*     */ import com.myapps.controlmenu.model.Bitacora;
/*     */ import com.myapps.controlmenu.model.RolFormulario;
/*     */ import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
/*     */ import com.myapps.controlmenu.util.ParametersCM;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import org.apache.log4j.Logger;


public class ControlLoginImpl implements ControlLogin {
   private static ControlTimeOut controlTA;
   private static Logger log;
  private static BitacoraBL logBL;
     private static int nroOption = ParametersCM.NRO_INTENTOS;
    private static RoleBL roleBL;
    /*  30 */   private static int timeOut = ParametersCM.MINUTOS_FUERA;

    static {
        logBL = (BitacoraBL) GuiceInjectorSingletonCM.getInstance(BitacoraBL.class);
        userBL = (UserBL) GuiceInjectorSingletonCM.getInstance(UserBL.class);
        roleBL = (RoleBL) GuiceInjectorSingletonCM.getInstance(RoleBL.class);
        controlTA = (ControlTimeOut) GuiceInjectorSingletonCM.getInstance(ControlTimeOut.class);
        log = Logger.getLogger(ControlLoginImpl.class);
    }

  private static UserBL userBL;


    public String validateIngreso(String login, String passWord) {

        if (login == null) {

            return "Usuario invalido !";

        }

        if (login.isEmpty()) {
            return "Usuario invalido !";
        }
        if (passWord == null) {
            return "Contraseña invalido !";
        }
        if (passWord.isEmpty()) {
            return "Contraseña invalido !";
        }
        if (controlTA.exisUserIngreso(login) && nroOption == controlTA.countIntentoUserIngreso(login)) {
            Date dateAct = new Date();
            Calendar cal = Calendar.getInstance();
            NodoIngresoUser user = controlTA.getNodoIngresoUser(login);
            cal.setTime(user.getDate());
            cal.add(12, timeOut);
            if (dateAct.before(cal.getTime())) {
                return "EL USUARIO \"" + login + '"' + " HA SIDO BLOQUEADO.VUELVA A INTENTAR EN " + timeOut + " MINUTOS.";
                /*     */
            }
            user.setCount(0);
            user.setDate(new Date());
        }
        return "";
    }


    public String controlError(String login) {
        String str = "";
        if (controlTA.exisUserIngreso(login)) {
            controlTA.ingrementIntentoUser(login);
            if (nroOption - controlTA.getNodoIngresoUser(login).getCount() > 0) {
                return "EL USUARIO O CONTRASEÑA INGRESADOS SON INCORRECTOS";
            }
            return "EL USUARIO \"" + login + '"' + " HA SIDO BLOQUEADO.VUELVA A INTENTAR EN " + timeOut + " MINUTOS.";
        }
        controlTA.insertUserIngreso(login);
        return "EL USUARIO O CONTRASEÑA INGRESADOS SON INCORRECTOS";
    }


    public int getIdRole(String userID, List userGroups) {
        return userBL.getIdRole(userID, userGroups);
    }


    public int existUser(String userId, String password) {
        if (userId.indexOf(ParametersCM.LOGIN) != -1 && password.equals(ParametersCM.PASSWORD_USER)) {
            return 1;
        }
        return 2;
    }


    public void addBitacoraLogin(String strIdUs, String address, int idRol) {
        /*  96 */
        Bitacora logg = new Bitacora();
        /*  97 */
        logg.setForm("INICIO");
        /*  98 */
        logg.setAction("Ingreso al Sistema");
        /*  99 */
        logg.setUser(strIdUs);
        /* 100 */
        logg.setAddress(address);
        /* 101 */
        logBL.insertLogWeb(logg);
        /* 102 */
        List<RolFormulario> lista = roleBL.getListRolFormulario(idRol);
        /* 103 */
        NodoClient nd = new NodoClient();
        /* 104 */
        nd.setUser(strIdUs);
        /* 105 */
        nd.setAddressIp(address);
        /* 106 */
        nd.setTime((new Date()).getTime());
        /* 107 */
        for (RolFormulario rolFormulario : lista) {
            /* 108 */
            log.info("[addBitacoraLogin] URL:" + rolFormulario.getUrl() + " estado:" + rolFormulario.getEstado());
            /* 109 */
            nd.addUrl(rolFormulario.getUrl(), StringToBoolean(rolFormulario.getEstado()).booleanValue());
            /*     */
        }
        /*     */
        /* 112 */
        nd.addUrl("Menu.xhtml", true);
        /* 113 */
        controlTA.addNodoClient(nd);
        /*     */
    }

    /*     */
    /*     */
    private Boolean StringToBoolean(String estado) {
        /* 117 */
        return Boolean.valueOf((estado.equals("true") || estado.equals("t")));
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public void salirBitacora(String strIdUs, String address) {
        /* 122 */
        Bitacora logg = new Bitacora();
        /* 123 */
        logg.setForm("");
        /* 124 */
        logg.setAction("Salio del Sistema");
        /* 125 */
        logg.setUser(strIdUs);
        /* 126 */
        String addr = controlTA.getAddressIP(strIdUs);
        /* 127 */
        logg.setAddress(address);
        /* 128 */
        logBL.insertLogWeb(logg);
        /* 129 */
        if (addr.equals(address)) {
            /* 130 */
            controlTA.remove(strIdUs);
            /*     */
        }
        /*     */
    }

    /*     */
    /*     */
    /*     */
    public List getListaMenu(int idRole) {
        /* 136 */
        List<NodoMenu> listaNM = new ArrayList<>();
        /* 137 */
        Map<String, NodoMenu> mapTreeNode = new HashMap<>();
        /* 138 */
        for (RolFormulario rolFor : roleBL.getListRolFormulario(idRole)) {
            /* 139 */
            String str = rolFor.getNivel();
            /* 140 */
            if (str != null && !str.isEmpty()) {
                /* 141 */
                int k = str.lastIndexOf(".");
                /*     */
                /* 143 */
                if (k == -1) {
                    /* 144 */
                    NodoMenu ndMenu = new NodoMenu();
                    /* 145 */
                    ndMenu.setName(rolFor.getName());
                    /* 146 */
                    ndMenu.setSwRendered(StringToBoolean(rolFor.getEstado()).booleanValue());
                    /*     */
                    /* 148 */
                    ndMenu.setTipo(1);
                    /* 149 */
                    mapTreeNode.put(str, ndMenu);
                    /* 150 */
                    listaNM.add(ndMenu);
                    continue;
                    /*     */
                }
                /* 152 */
                NodoMenu ndMenuF = mapTreeNode.get(str.substring(0, k));
                /* 153 */
                if (rolFor.getUrl().isEmpty()) {
                    /* 154 */
                    NodoMenu ndMenu = new NodoMenu();
                    /* 155 */
                    ndMenu.setName(rolFor.getName());
                    /* 156 */
                    ndMenu.setSwRendered(StringToBoolean(rolFor.getEstado()).booleanValue());
                    /* 157 */
                    ndMenu.setPather(ndMenuF.getName());
                    /* 158 */
                    ndMenu.setTipo(2);
                    /* 159 */
                    mapTreeNode.put(str, ndMenu);
                    /* 160 */
                    listaNM.add(ndMenu);
                    continue;
                    /*     */
                }
                /* 162 */
                NodoMenu item = new NodoMenu();
                /* 163 */
                item.setName(rolFor.getName());
                /* 164 */
                item.setUrl("/view/" + rolFor.getUrl());
                /* 165 */
                item.setSwRendered(StringToBoolean(rolFor.getEstado()).booleanValue());
                /* 166 */
                item.setPather(ndMenuF.getName());
                /* 167 */
                item.setTipo(3);
                /* 168 */
                listaNM.add(item);
                /*     */
            }
            /*     */
        }
        /*     */
        /*     */
        /* 173 */
        return listaNM;
        /*     */
    }


    public List getListaGrupo() {
        List<String> groups = new ArrayList<>();
        groups.add("Administradores");
        return groups;
    }

}


/* Location:              D:\myapps2019\CSU CHANGE REQUEST ORACLE\ControlMenu.jar!\controlmenu\controler\ControlLoginImpl.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */