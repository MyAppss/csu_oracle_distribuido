/*    */ package com.myapps.controlmenu.controler;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class NodoClient
/*    */ {
/*    */   private String addressIp;
/* 14 */   private Map<String, Boolean> mapListaUrl = new HashMap<>();
/*    */   private long time;
/*    */   
/*    */   public void addUrl(String url, boolean sw) {
/* 18 */     this.mapListaUrl.put(url, Boolean.valueOf(sw));
/*    */   }
/*    */   private String user;
/*    */   public boolean existeUrl(String url) {
/* 22 */     if (this.mapListaUrl.containsKey(url)) {
/* 23 */       return ((Boolean)this.mapListaUrl.get(url)).booleanValue();
/*    */     }
/* 25 */     return false;
/*    */   }
/*    */   
/*    */   public String getUser() {
/* 29 */     return this.user;
/*    */   }
/*    */   
/*    */   public void setUser(String user) {
/* 33 */     this.user = user;
/*    */   }
/*    */   
/*    */   public String getAddressIp() {
/* 37 */     return this.addressIp;
/*    */   }
/*    */   
/*    */   public void setAddressIp(String addressIp) {
/* 41 */     this.addressIp = addressIp;
/*    */   }
/*    */   
/*    */   public long getTime() {
/* 45 */     return this.time;
/*    */   }
/*    */   
/*    */   public void setTime(long time) {
/* 49 */     this.time = time;
/*    */   }
/*    */ }


