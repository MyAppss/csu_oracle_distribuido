package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public class UserDAOImpl implements UserDAO {
    private static final Logger LOG = Logger.getLogger(UserDAOImpl.class);

    public static final String USUARIO_ID = "usuario_id";
    public static final String ROL_ID = "rol_id";
    public static final String LOGIN = "login";
    public static final String NOMBRE = "nombre";
    public static final String ESTADO = "estado";
    public static final String SQL_EXCEPTION = " SQLException: ";


    public Usuario getUserId(int userId) {
        PreparedStatement stmt = null;
        Usuario usuario = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM USUARIO WHERE USUARIO_ID=(?)";
            LOG.debug("[getUserId] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, userId);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    Usuario result = new Usuario();
                    result.setUsuarioId(Integer.valueOf(rs.getInt(USUARIO_ID)));
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setLogin(rs.getString(LOGIN));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setEstado(rs.getString(ESTADO));
                    usuario = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getUserId] Error al intentar obteneral usuario con id " + userId + SQL_EXCEPTION, e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getUserId] Error al intentar cerrar el ResultSet usuario con Id: " + userId + SQL_EXCEPTION, ex22);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex222) {
                    LOG.warn("[getUserId] Error al intentar cerrar Statemental obtener al usuario con id:" + userId + SQL_EXCEPTION, ex222);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUserId] Error al intentar cerrar Connection despues de obtener userId SQLException:", ex);
                }
            }
        }
        return usuario;

    }


    public Usuario getUserName(String name) {
        Usuario usuario = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM USUARIO WHERE LOGIN=(?) AND (ESTADO='t' OR ESTADO='true')";
            LOG.debug("[getUserName] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    Usuario result = new Usuario();
                    result.setUsuarioId(Integer.valueOf(rs.getInt(USUARIO_ID)));
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setLogin(rs.getString(LOGIN));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setEstado(rs.getString(ESTADO));
                    usuario = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getUserName] Error al intentar obteneral usuario con login: " + name + SQL_EXCEPTION, e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getUserName] Error al intentar cerrar el ResultSet usuario con login: " + name + SQL_EXCEPTION, ex22);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex222) {
                    LOG.warn("[getUserName] Error al intentar cerrar Statemental obtener al usuario con login: " + name + SQL_EXCEPTION, ex222);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUserName] Error al intentar cerrar Connection despues de obtener userName SQLException:", ex);
                }
            }
        }
        return usuario;
    }


    public List<Usuario> getUsers() {
        List<Usuario> list = null;
        String sql = "SELECT U.USUARIO_ID,U.ROL_ID as ROL_ID,U.LOGIN, U.NOMBRE as NOMBRE, U.ESTADO as ESTADO_USER,R.NOMBRE as NOMBRE_ROL FROM USUARIO U INNER JOIN ROL R ON U.ROL_ID = R.ROL_ID WHERE U.ESTADO = 'true' OR U.ESTADO = 't' ORDER BY USUARIO_ID DESC";
        try (
                Connection conn = FactoryNative.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            LOG.debug("[getUsers] SQL: " + sql);

            if (rs != null) {
                List<Usuario> result = new ArrayList<>();
                while (rs.next()) {
                    Usuario usuario = new Usuario();
                    usuario.setUsuarioId(Integer.valueOf(rs.getInt(USUARIO_ID)));
                    usuario.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    usuario.setLogin(rs.getString(LOGIN));
                    usuario.setNombre(rs.getString(NOMBRE));
                    usuario.setEstado(rs.getString("estado_user"));
                    usuario.setNameRole(rs.getString("nombre_rol"));
                    result.add(usuario);
                }
                list = result;

            }
        } catch (SQLException e3) {
            LOG.error("[getUsers] Error al intentar obtenerSQLException:", e3);
        }

        return list;

    }

    public void insertUser(Usuario user) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String sql = "INSERT INTO USUARIO (USUARIO_ID,ROL_ID, LOGIN, NOMBRE,ESTADO) VALUES (USUARIO_USUARIO_ID_SEQ.nextval,?,?,?,?)";
            LOG.debug("[insertUser] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, user.getRolId().intValue());
                pstmt.setString(2, user.getLogin());
                pstmt.setString(3, user.getNombre());
                pstmt.setString(4, user.getEstado());
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insertUser] Error al intentar adicionaral usuario con id:" + user.getUsuarioId() + SQL_EXCEPTION, e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertUser] Error al intentar cerrar al usuario con id:" + user.getUsuarioId() + SQL_EXCEPTION, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertUser] Error al intentar cerrar Connection despues de insertar user SQLException:", ex);
                }
            }
        }
    }


    public void updateUser(Usuario user) {
        Connection conn = null;
        PreparedStatement st = null;
        if (user.getNombre() == null) {
            user.setNombre("");
        }
        try {
            String sql = "UPDATE USUARIO SET ROL_ID=(?), LOGIN=(?), NOMBRE =(?) WHERE USUARIO_ID=(?)";
            LOG.debug("[updateUser] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setInt(1, user.getRolId());
                st.setString(2, user.getLogin());
                st.setString(3, user.getNombre());
                st.setInt(4, user.getUsuarioId());
                st.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.warn("[updateUser] Error al intentar actualizar al usuario con id" + user.getUsuarioId() + SQL_EXCEPTION, e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateUser] Error al intentar cerrar Statement despues de actualizarusuario con id:" + user.getUsuarioId() + SQL_EXCEPTION, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateUser] Error al intentar cerrar Connection despues de actualizar user SQLException:", ex);
                }
            }
        }
    }


    public void deleteUser(int userId) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            String sql = "UPDATE USUARIO SET ESTADO = 'f' WHERE USUARIO_ID=(?)";
            LOG.debug("[deleteUser] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setInt(1, userId);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.warn("[deleteUser] Error al intentar actualizar al usuario con id" + userId + SQL_EXCEPTION, e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteUser] Error al intentar cerrar Statement despues de actualizarusuario con id:" + userId + SQL_EXCEPTION, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteUser] Error al intentar cerrar Connection despues de eliminar user SQLException:", ex);
                }
            }
        }
    }

    public List<Usuario> getUsersDeleteRole(int idRole) {
        List<Usuario> list = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM USUARIO WHERE (ESTADO='true' OR ESTADO='t') and ROL_ID=(?)";
            LOG.debug("[getUsersDeleteRole] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, idRole);
                rs = stmt.executeQuery();
                if (rs != null) {
                    List<Usuario> result = new ArrayList<>();
                    while (rs.next()) {
                        Usuario usuario = new Usuario();
                        usuario.setUsuarioId(Integer.valueOf(rs.getInt(USUARIO_ID)));
                        usuario.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                        usuario.setLogin(rs.getString(LOGIN));
                        usuario.setNombre(rs.getString(NOMBRE));
                        usuario.setEstado("t");
                        result.add(usuario);
                    }
                    list = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getUsersDeleteRole] Error al intentar obtenerSQLException:", e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUsersDeleteRole] Error al intentar cerrar el ResultSet SQLException:", ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex2) {
                    LOG.warn("[getUsersDeleteRole] Error al intentar cerrar StatementSQLException:", ex2);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getUsersDeleteRole] Error al intentar cerrar Connection despues de obtener la lista user role SQLException:", ex);
                }
            }
        }
        return list;
    }
}


