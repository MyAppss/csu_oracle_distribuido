
package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Bitacora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public class BitacoraDAOImpl implements BitacoraDAO {
    private static final Logger LOG = Logger.getLogger(BitacoraDAOImpl.class);


    public List<Bitacora> getLogWebs() {
        List<Bitacora> list = null;
        String sql = "SELECT * FROM (SELECT * FROM BITACORA ORDER BY  FECHA DESC) WHERE ROWNUM <= 1000";

        try (Connection conn = FactoryNative.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql);) {
            LOG.debug("[getUsers] SQL: " + sql);
            if (rs != null) {
                List<Bitacora> result = new ArrayList<>();
                while (rs.next()) {
                    Bitacora bitacora = new Bitacora();
                    bitacora.setStartDate(rs.getTimestamp("fecha"));
                    bitacora.setUser(rs.getString("usuario"));
                    bitacora.setForm(rs.getString("formulario"));
                    bitacora.setAction(rs.getString("accion"));
                    bitacora.setAddress(rs.getString("direccion_ip"));
                    result.add(bitacora);
                }
                list = result;
            }

        } catch (SQLException e3) {
            LOG.error("[getFormularios] Error al intentar obtener el plan de consumo SQLException:" + e3.getMessage());
        }

        return list;

    }


    public void insertLogWeb(Bitacora logWeb) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            String sql = "INSERT INTO BITACORA (FECHA, USUARIO, FORMULARIO, ACCION,DIRECCION_IP) VALUES (?,?,?,?,?)";
            LOG.debug("[insertLogWeb] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setTimestamp(1, logWeb.getStartDate());
                pstmt.setString(2, logWeb.getUser());
                pstmt.setString(3, logWeb.getForm());
                pstmt.setString(4, logWeb.getAction());
                pstmt.setString(5, logWeb.getAddress());
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insertLogWeb] Error al insertar en la DB SQLException:" + e.getMessage());
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertLogWeb] Error al intentar cerrar ", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertLogWeb] Error al intentar cerrar:", ex);
                }
            }
        }

    }


    public void insertLogWebBase(Timestamp startDate, String user, String form, String action, String address) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            String sql = "INSERT INTO BITACORA(FECHA, USUARIO, FORMULARIO, ACCION, DIRECCION_IP) VALUES (?,?,?,?,?)";
            LOG.debug("[insertLogWebBase] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setTimestamp(1, startDate);
                pstmt.setString(2, user);
                pstmt.setString(3, form);
                pstmt.setString(4, action);
                pstmt.setString(5, address);
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insertLogWebBase] Error al intentar adicionar a la SQLException:", e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex22) {
                    LOG.warn("[insertLogWebBase] Error al intentar cerrar ", ex22);
                }
            }
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertLogWeb] Error al intentar cerrar:", ex);
                }
        }
    }



}


