package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Usuario;
import java.util.List;

public interface UserDAO {
  void deleteUser(int paramInt);
  
  Usuario getUserId(int paramInt);
  
  Usuario getUserName(String paramString);
  
  List<Usuario> getUsers();
  
  List<Usuario> getUsersDeleteRole(int paramInt);
  
  void insertUser(Usuario paramUsuario);
  
  void updateUser(Usuario paramUsuario);
}

