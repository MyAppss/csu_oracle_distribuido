package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Formulario;

import java.util.List;

public interface FormularioDAO {
    List<Formulario> getFormularios();
}


