package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.RolFormulario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class RolFormularioDAOImpl implements RolFormularioDAO {
    private static final Logger LOG = Logger.getLogger(RolFormularioDAOImpl.class);


    public List<RolFormulario> getList(int idRol) {
        List<RolFormulario> list = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT RF.ROL_ID,\n" +
                    "RF.FORMULARIO_ID AS FORMULARIO,\n" +
                    "RF.ESTADO,\n" +
                    "F.URL,\n" +
                    "F.NOMBRE,\n" +
                    "F.DEPENDE,\n" +
                    "F.NIVEL\n" +
                    "FROM ROL_FORMULARIO RF INNER JOIN FORMULARIO F ON RF.FORMULARIO_ID=F.FORMULARIO_ID\n" +
                    "where RF.ROL_ID=(?)  ORDER BY F.NIVEL";
            //String sql = "SELECT \"rol_formulario\".\"rol_id\", \"rol_formulario\".\"formulario_id\" as formulario,\"rol_formulario\".\"estado\" as estado,\"formulario\".\"url\", \"formulario\".\"nombre\" , \"formulario\".\"depende\",\"formulario\".\"nivel\"  from \"rol_formulario\" INNER JOIN \"formulario\" ON \"rol_formulario\".\"formulario_id\"=\"formulario\".\"formulario_id\" where \"rol_formulario\".\"rol_id\"=(?)  ORDER BY \"formulario\".\"nivel\"";
            LOG.debug("[getList] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, idRol);
                rs = stmt.executeQuery();
                if (rs != null) {
                    List<RolFormulario> result = new ArrayList<>();
                    while (rs.next()) {
                        RolFormulario grupo = new RolFormulario();
                        grupo.setRolId(Integer.valueOf(rs.getInt("rol_id")));
                        grupo.setFormularioId(Integer.valueOf(rs.getInt("formulario")));
                        grupo.setEstado(rs.getString("estado"));
                        grupo.setUrl(rs.getString("url"));
                        grupo.setName(rs.getString("nombre"));
                        grupo.setDepende(Integer.valueOf(rs.getInt("depende")));
                        grupo.setNivel(rs.getString("nivel"));
                        result.add(grupo);
                    }
                    list = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getList] Error al intentar obtener rol con Id: " + idRol + "SQLException:", e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getList] Error al intentar cerrar el ResultSet rol con Id: " + idRol + " SQLException:", ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex2) {
                    LOG.warn("[getList] Error al intentar cerrar Statemental obtener lista de rol con id:" + idRol + "SQLException:", ex2);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getList] Error al intentar cerrar Connection despues de obtener lista de rol SQLException:", ex);
                }
            }
        }
        return list;
    }

    public void insert(RolFormulario group) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            String sql = "INSERT INTO ROL_FORMULARIO (ROL_ID, FORMULARIO_ID, ESTADO) VALUES (?, ?, ?)";

            LOG.debug("[insert] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, group.getRolId().intValue());
                pstmt.setInt(2, group.getFormularioId().intValue());
                pstmt.setString(3, group.getEstado());
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insert] Error al intentar adicionar a la SQLException:", e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[insert] Error al intentar cerrar ", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insert] Error al intentar cerrar Connection despues de insertar un role SQLException:", ex);
                }
            }
        }
    }

    public void update(RolFormulario group) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = FactoryNative.getConnection();
            String sql = "UPDATE ROL_FORMULARIO SET ESTADO=(?) WHERE ROL_ID=(?) AND FORMULARIO_ID=(?)";
            LOG.debug("[update] SQL: " + sql);
            if (conn != null) {
                // st = conn.createStatement();
                st = conn.prepareStatement(sql);
                st.setString(1, group.getEstado());
                st.setInt(2, group.getRolId());
                st.setInt(3, group.getFormularioId());
                if (st.executeUpdate() == 0) {
                    insert(group);
                }

            }
        } catch (SQLException e) {
            LOG.warn("[update] Error al intentar actualizar la acreditacion con id ciclo " + group.getRolId() + ". SQLException:", e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[update] Error al intentar cerrar Statement despues de actualizar la acreditacion con id ciclo " + group.getRolId() + ". SQLException:", ex);
                }
            }
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[update] Error al intentar cerrar Connection despues de actualizar un roleFormulario SQLException:", ex);
                }
        }
    }


    public List<RolFormulario> getFormularios() {
        List<RolFormulario> list = new ArrayList<>();
        String sql = "SELECT * FROM FORMULARIO WHERE ESTADO='t' or ESTADO='true' ORDER BY NIVEL ASC";

        try (Connection conn = FactoryNative.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql);) {
            LOG.debug("[getUsers] SQL: " + sql);
            if (rs != null) {
                List<RolFormulario> result = new ArrayList<>();
                while (rs.next()) {
                    RolFormulario rolFormulario = new RolFormulario();
                    rolFormulario.setFormularioId(Integer.valueOf(rs.getInt("formulario_id")));
                    rolFormulario.setUrl(rs.getString("url"));
                    rolFormulario.setName(rs.getString("nombre"));
                    rolFormulario.setDepende(Integer.valueOf(rs.getInt("depende")));
                    rolFormulario.setNivel(rs.getString("nivel"));
                    rolFormulario.setEstado("false");
                    result.add(rolFormulario);
                }
                list = result;
            }

        } catch (SQLException e3) {
            LOG.error("[getFormularios] Error al intentar obtener la lista de Formularios SQLException:" + e3.getMessage());
        }

        return list;

    }


}


