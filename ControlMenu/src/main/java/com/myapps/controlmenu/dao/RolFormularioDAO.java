package com.myapps.controlmenu.dao;

import com.myapps.controlmenu.model.RolFormulario;

import java.util.List;

public interface RolFormularioDAO {
    List<RolFormulario> getList(int paramInt);

    void insert(RolFormulario paramRolFormulario);

    void update(RolFormulario paramRolFormulario);

    List<RolFormulario> getFormularios();
}


