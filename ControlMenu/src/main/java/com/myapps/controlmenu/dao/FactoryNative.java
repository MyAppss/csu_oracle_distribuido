
package com.myapps.controlmenu.dao;

import com.myapps.controlmenu.util.ParametersCM;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;


public class FactoryNative {
    private static final Logger LOG = Logger.getLogger(FactoryNative.class);

    private static final String HOST = ParametersCM.HOST;
    private static final String DB = ParametersCM.DB;
    private static final String PORT = ParametersCM.PORT;
    private static final String USER = ParametersCM.USER;
    private static final String PASSWORD = ParametersCM.PASSWORD;
    public static DataSource datasource = null;


    public static synchronized Connection getConnection() throws SQLException {
        if (datasource == null) {
            datasource = setupDataSource();
        }
        return datasource.getConnection();
    }



    private static DataSource setupDataSource() {

        PoolProperties p = new PoolProperties();
        String url = "jdbc:oracle:thin:@" + HOST + ":" + PORT + "/" + DB;
        p.setUrl(url);
        p.setDriverClassName("oracle.jdbc.OracleDriver");
        p.setUsername(USER);
        p.setPassword(PASSWORD);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setTestOnReturn(false);
        p.setValidationQuery("SELECT 1 FROM DUAL");
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMinEvictableIdleTimeMillis(Integer.parseInt(ParametersCM.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS));
        p.setMaxActive(Integer.parseInt(ParametersCM.DB_MAX_ACTIVE));
        p.setMinIdle(Integer.parseInt(ParametersCM.DB_MIN_IDLE));
        p.setMaxIdle(Integer.parseInt(ParametersCM.DB_MAX_IDLE));
        p.setMaxWait(Integer.parseInt(ParametersCM.DB_MAX_WAIT));
        p.setInitialSize(2);
        p.setRemoveAbandonedTimeout(60);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(false);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource dataSource = new DataSource();
        dataSource.setPoolProperties(p);

        
        return dataSource;
    }

}


