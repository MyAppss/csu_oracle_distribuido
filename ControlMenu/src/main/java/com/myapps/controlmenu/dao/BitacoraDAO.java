package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Bitacora;

import java.sql.Timestamp;
import java.util.List;

public interface BitacoraDAO  {
  List<Bitacora> getLogWebs();
  
  void insertLogWeb(Bitacora paramBitacora);
  
  void insertLogWebBase(Timestamp paramTimestamp, String paramString1, String paramString2, String paramString3, String paramString4);
}


