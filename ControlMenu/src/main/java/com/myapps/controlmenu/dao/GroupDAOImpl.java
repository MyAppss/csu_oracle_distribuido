package com.myapps.controlmenu.dao;

import com.myapps.controlmenu.model.GrupoAd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class GroupDAOImpl implements GroupDAO {
    private static final Logger LOG = Logger.getLogger(GroupDAOImpl.class);

    private static final String GRUPO_ID = "grupo_id";
    private static final String ROL_ID = "rol_id";
    private static final String NOMBRE = "nombre";
    private static final String ESTADO = "estado";
    private static final String DETALLE = "detalle";
    private static final String SQLEXCEPTION = " SQLException: ";


    public GrupoAd getGroupId(int groupId) {
        GrupoAd grupoAd = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM GRUPO_AD WHERE GRUPO_ID =(?)";
            LOG.debug("[getGroupId] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, groupId);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    GrupoAd result = new GrupoAd();
                    result.setGrupoId(Integer.valueOf(rs.getInt(GRUPO_ID)));
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setDetalle(rs.getString(DETALLE));
                    result.setEstado(rs.getString(ESTADO));
                    grupoAd = result;
                }
            }

        } catch (SQLException e3) {
            LOG.error("[getGroupId] Error al intentar obtener grupo con Id: " + groupId + SQLEXCEPTION, e3);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroupId] Error al intentar cerrar Statemental obtener al grupo con id:" + groupId + SQLEXCEPTION, ex);
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getGroupId] Error al intentar cerrar el ResultSet grupo con Id: " + groupId + " SQLException:", ex22);
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getFormularios] Error al intentar cerrar Connection despues de obtener grupoAD SQLException:", ex);
                }
            }
        }
        return grupoAd;
    }


    public GrupoAd getGroupName(String name) {

        GrupoAd grupoAd = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM GRUPO_AD WHERE NOMBRE=(?) AND (ESTADO='true' OR ESTADO='t')";
            LOG.debug("[getGroupName] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    GrupoAd result = new GrupoAd();
                    result.setGrupoId(Integer.valueOf(rs.getInt(GRUPO_ID)));
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setDetalle(rs.getString(DETALLE));
                    result.setEstado(rs.getString(ESTADO));
                    grupoAd = result;
                }
            }
        } catch (SQLException e2) {
            LOG.error("[getGroupName] Error al intentar obtener nombre del grupo  SQLException:", e2);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex222) {
                    LOG.warn("[getGroupName] Error al intentar cerrar Statemental obtener al usuario con nombre: " + name + SQLEXCEPTION, ex222);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getGroupName] Error al intentar cerrar el ResultSet usuario con nombre: " + name + SQLEXCEPTION, ex22);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroupName] Error al intentar cerrar Connection despues de obtener el nombre del grupo SQLException:", ex);
                }
            }
        }
        return grupoAd;
    }


    public List<GrupoAd> getGroups() {
        List<GrupoAd> list = null;
        Statement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT G.GRUPO_ID,G.ROL_ID as rol_id,G.NOMBRE as NOMBRE,G.DETALLE,G.ESTADO as ESTADO_GRUPO, R.NOMBRE as NOMBRE_ROL FROM GRUPO_AD G INNER JOIN ROL R ON G.ROL_ID=R.ROL_ID WHERE G.ESTADO='true' OR G.ESTADO='t'  ORDER BY  nombre_rol";
            LOG.debug("[getGroups] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
                if (rs.isBeforeFirst()) {
                    List<GrupoAd> result = new ArrayList<>();
                    while (rs.next()) {
                        GrupoAd grupo = new GrupoAd();
                        grupo.setGrupoId(Integer.valueOf(rs.getInt(GRUPO_ID)));
                        grupo.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                        grupo.setNombre(rs.getString(NOMBRE));
                        grupo.setDetalle(rs.getString(DETALLE));
                        grupo.setEstado(rs.getString("estado_grupo"));
                        grupo.setNameRole(rs.getString("nombre_rol"));
                        result.add(grupo);
                    }
                    list = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getGroups] Error al intentar obtenerSQLException:", e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroups] Error al intentar cerrar el ResultSet SQLException:", ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex2) {
                    LOG.warn("[getGroups] Error al intentar cerrar StatementSQLException:", ex2);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroups] Error al intentar cerrar Connection despues de obtener la lista de grupos SQLException:", ex);
                }
            }
        }
        return list;
    }


    public void insertGroup(GrupoAd group) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            String sql = "INSERT INTO GRUPO_AD (GRUPO_ID, ROL_ID, NOMBRE, DETALLE, ESTADO) VALUES (GRUPO_AD_GRUPO_ID_SEQ.nextval, ?, ?, ?, ?)";
            LOG.debug("[insertGroup] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, group.getRolId().intValue());
                pstmt.setString(2, group.getNombre());
                pstmt.setString(3, group.getDetalle());
                pstmt.setString(4, group.getEstado());
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insertGroup] Error al intentar adicionarSQLException:", e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertGroup] Error al intentar cerrar SQLException:", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertGroup] Error al intentar cerrar Connection despues de insertar gurpo SQLException:", ex);
                }
            }
        }
    }


    public void updateGroup(GrupoAd group) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            String sql = "UPDATE GRUPO_AD SET ROL_ID=(?), NOMBRE =(?), DETALLE =(?) WHERE GRUPO_ID=(?)";
            LOG.debug("[updateGroup] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setInt(1, group.getRolId());
                st.setString(2, group.getNombre());
                st.setString(3, group.getDetalle());
                st.setInt(4, group.getGrupoId());

                st.executeUpdate();
            }

        } catch (SQLException e) {

            LOG.warn("[updateGroup] Error al intentar actualizar al grupo con id" + group.getGrupoId() + "SQLException:", e);

        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateGroup] Error al intentar cerrar Statement despues de actualizar grupo con id" + group.getGrupoId() + ". SQLException:", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateGroup] Error al intentar cerrar Connection despues de actualizar grupo SQLException:", ex);
                }
            }
        }
    }


    public void deleteGroup(int groupId) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            String sql = "UPDATE GRUPO_AD SET ESTADO = 'f' WHERE GRUPO_ID=(?)";
            LOG.debug("[deleteGroup] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setInt(1, groupId);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error("[deleteGroup] Error al intentar actualizar al grupo con id" + groupId + "SQLException:", e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteGroup] Error al intentar cerrar Statement despues de actualizaral grupo con id" + groupId + ". SQLException:", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteGroup] Error al intentar cerrar Connection despues de eliminar grupo SQLException:", ex);

                }

            }

        }

    }


    public List<GrupoAd> getGroupsDeleteRole(int idRole) {
        List<GrupoAd> list = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM GRUPO_AD WHERE (ESTADO = 'true' OR ESTADO = 't') and ROL_ID = (?)";
            LOG.debug("[getGroupsDeleteRole] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, idRole);
                rs = stmt.executeQuery();
                if (rs != null) {
                    List<GrupoAd> result = new ArrayList<>();
                    while (rs.next()) {
                        GrupoAd grupo = new GrupoAd();
                        grupo.setGrupoId(Integer.valueOf(rs.getInt(GRUPO_ID)));
                        grupo.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                        grupo.setNombre(rs.getString(NOMBRE));
                        grupo.setDetalle(rs.getString(DETALLE));
                        grupo.setEstado(rs.getString(ESTADO));
                        result.add(grupo);
                    }
                    list = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getGroupsDeleteRole] Error al intentar obtenerSQLException:", e3);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar el ResultSet SQLException:", ex);
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex2) {
                    LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar StatementSQLException:", ex2);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getGroupsDeleteRole] Error al intentar cerrar Connection despues de obtener eliminar gurpo por roles SQLException:", ex);
                }
            }
        }
        return list;
    }


}

