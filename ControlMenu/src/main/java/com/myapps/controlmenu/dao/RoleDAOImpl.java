package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Rol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class RoleDAOImpl implements RoleDAO {
    private static final Logger LOG = Logger.getLogger(RoleDAOImpl.class);
    private static final String ROL_ID = "rol_id";
    private static final String NOMBRE = "nombre";
    private static final String DESCRIPCION = "descripcion";
    private static final String ESTADO = "estado";
    private static final String SQL_EXCEPTION = ". SQLException: ";


    public Rol getRoleId(int roleId) {
        Rol rol = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ROL WHERE ROL.ROL_ID =(?)";
            LOG.debug("[getUsers] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, roleId);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    Rol result = new Rol();
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setDescripcion(rs.getString(DESCRIPCION));
                    result.setEstado(rs.getString(ESTADO));
                    rol = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getRoleId] Error al intentar obtener el Roles  con id  SQLException:", e3);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getRoleId] Error al intentar cerrar Statement despues de obtener  Role SQLException:", ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getRoleId] Error al intentar cerrar el ResultSet despues de obtener el Roles por  SQLException:", ex22);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getRoleId] Error al intentar cerrar Connection despues obtener un role SQLException:", ex);
                }
            }
        }
        return rol;
    }


    public Rol getRoleName(String name) {
        Rol rol = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ROL WHERE NOMBRE=(?) AND (ESTADO='true' OR ESTADO='t')";
            LOG.debug("[getRoleName] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, name);
                rs = stmt.executeQuery();
                if (rs != null && rs.next()) {
                    Rol result = new Rol();
                    result.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    result.setNombre(rs.getString(NOMBRE));
                    result.setDescripcion(rs.getString(DESCRIPCION));
                    result.setEstado(rs.getString(ESTADO));
                    rol = result;
                }
            }
        } catch (SQLException e3) {
            LOG.error("[getRoleName] Error al intentar obtener de Roles  SQLException:", e3);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[getRoleName] Error al intentar cerrar Statement despues de obtener la  Roles SQLException:", ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex22) {
                    LOG.warn("[getRoleName] Error al intentar cerrar el ResultSet despues de obtener  de Roles SQLException:", ex22);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[getRoleName] Error al intentar cerrar Connection despues obtener nombre del role SQLException:", ex);
                }
            }
        }
        return rol;
    }


    public List<Rol> getRoles() {
        List<Rol> list = null;
        String sql = "SELECT * FROM ROL WHERE ESTADO='true' OR ESTADO='t' ORDER BY NOMBRE";
        try (
                Connection conn = FactoryNative.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            LOG.debug("[getRoles] SQL: " + sql);
            if (rs != null) {
                List<Rol> result = new ArrayList<>();
                while (rs.next()) {
                    Rol grupo = new Rol();
                    grupo.setRolId(Integer.valueOf(rs.getInt(ROL_ID)));
                    grupo.setNombre(rs.getString(NOMBRE));
                    grupo.setDescripcion(rs.getString(DESCRIPCION));
                    grupo.setEstado(rs.getString(ESTADO));
                    result.add(grupo);
                }
                list = result;
            }

        } catch (SQLException e3) {
            LOG.error("[getRoles] Error al intentar obtener la lista de Roles SQLException:", e3);
        }
        return list;

    }


    public void insertRole(Rol role) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sql = "INSERT INTO ROL ( ROL_ID,NOMBRE, DESCRIPCION, ESTADO) VALUES (?,?,?,?)";
        try {
            conn = FactoryNative.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1, role.getRolId().intValue());
                pstmt.setString(2, role.getNombre());
                pstmt.setString(3, role.getDescripcion());
                pstmt.setString(4, role.getEstado());
                pstmt.execute();
            }
        } catch (SQLException e) {
            LOG.error("[insertRole] Error al intentar adicionar a la SQLException:", e);
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertRole] Error al intentar cerrar ", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[insertRole] Error al intentar cerrar Connection despues de insertar role SQLException:", ex);
                }
            }
        }
    }

    public void updateRole(Rol role) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            String sql = "UPDATE ROL SET NOMBRE =(?), DESCRIPCION =(?) WHERE ROL_ID=(?)";
            LOG.debug("[updateRole] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setString(1, role.getNombre());
                st.setString(2, role.getDescripcion());
                st.setInt(3, role.getRolId());
                st.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.warn("[updateRole] Error al intentar actualizar la Role con id  " + role.getRolId() + SQL_EXCEPTION, e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateRole] Error al intentar cerrar Statement despues de actualizar Role" + role.getRolId() + SQL_EXCEPTION, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateRole] Error al intentar cerrar Connection despues de actualizar role SQLException:", ex);
                }
            }
        }
    }


    public void deleteRole(int roleId) {
        Connection conn = null;
        PreparedStatement st = null;
        try {
            String sql = "UPDATE ROL SET ESTADO = 'f' WHERE ROL_ID=(?)";
            LOG.debug("[deleteRole] SQL: " + sql);
            conn = FactoryNative.getConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setInt(1, roleId);
                st.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error("[deleteRole] Error al intentar actualizar Role " + roleId + SQL_EXCEPTION, e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteRole] Error al intentar cerrar Statement despues de actualizar Role " + roleId + SQL_EXCEPTION, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[deleteRole] Error al intentar cerrar Connection despues de eliminar role SQLException:", ex);
                }
            }
        }
    }


    public Integer getLastId() {

        String sql = "select ROL_ROL_ID_SEQ.nextval from DUAL";
        Integer roleID = Integer.valueOf(0);
        try (
                Connection conn = FactoryNative.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            LOG.debug("[getLastId] SQL: " + sql);
            if (rs != null && rs.next()) {
                roleID = Integer.valueOf(rs.getInt("nextval"));
            }

        } catch (SQLException e3) {
            LOG.error("[getLastId] Error al intentar obtener el ID del Rol  SQLException:", e3);
        }

        return roleID;
    }
}


