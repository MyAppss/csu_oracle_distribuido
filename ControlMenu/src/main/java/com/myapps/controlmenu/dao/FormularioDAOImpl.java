package com.myapps.controlmenu.dao;


import com.myapps.controlmenu.model.Formulario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public class FormularioDAOImpl implements FormularioDAO {
    private static final Logger LOG = Logger.getLogger(FormularioDAOImpl.class);

    public List<Formulario> getFormularios() {
        List<Formulario> list = new ArrayList<>();
        String sql = "SELECT * FROM FORMULARIO WHERE  ESTADO='t' OR ESTADO='true'";
        try (
                Connection conn = FactoryNative.getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            LOG.debug("[getFormularios] SQL: " + sql);
            if (rs != null) {

                List<Formulario> result = new ArrayList<>();

                while (rs.next()) {
                    Formulario usuario = new Formulario();
                    usuario.setFormularioId(Integer.valueOf(rs.getInt("formulario_id")));
                    usuario.setNombre(rs.getString("nombre"));
                    usuario.setEstado(rs.getString("estado").equals("t"));
                    usuario.setUrl(rs.getString("url"));
                    result.add(usuario);
                }
                list = result;
            }

        } catch (SQLException e3) {
            LOG.error("[getFormularios] Error al intentar obtener la lista de formularios  SQLException: ", e3);
        }
        return list;
    }


    public void prueba() {
        String sql = "INSERT INTO REPORTE_CONSULTA VALUES (RC_SEQ.nextval,?, '77816006', '*189#', 'USSD', 'f', '1000', '0:0:0:0:0:0:0:1', '{\"Linea\":\"77816006\",\"codigo\":\"200\",\"mensaje\":\"Consulta Satisfactoria\",\"pantallas\":[{\"idPantalla\":1,\"cabecera\":\"Linea:77816006\\nSaldo Total: 0 Bs\\nInternet: 30705,93 MB\\n1.-Saldo Paquetigos\\n2.-Otros Saldos\",\"opciones\":\"1.-Saldo Paquetigos\\n2.-Otros Saldos\",\"ListaNroOpcion\":[1,2],\"listaNavegacion\":[{\"opcion\":1,\"pantalla\":2},{\"opcion\":2,\"pantalla\":3}],\"pantallaPadre\":0,\"opcionPadre\":0},{\"idPantalla\":2,\"cabecera\":\"\",\"opciones\":\"1.-Paq Mes Internet: 30705,93 MB\",\"ListaNroOpcion\":[1],\"listaNavegacion\":[{\"opcion\":1,\"pantalla\":4}],\"pantallaPadre\":1,\"opcionPadre\":1},{\"idPantalla\":3,\"cabecera\":\"\",\"opciones\":\"Divertido: 20 Bs \\nSMS promo: 1000 sms \\nLlamadas Tigo: 440 Bs \",\"ListaNroOpcion\":[],\"listaNavegacion\":[],\"pantallaPadre\":1,\"opcionPadre\":2},{\"idPantalla\":4,\"cabecera\":\"\",\"opciones\":\"Paq Mes Internet: 30705,93 MB Activo hasta el 01/04/2020 00:00:00\",\"ListaNroOpcion\":[],\"listaNavegacion\":[],\"pantallaPadre\":2,\"opcionPadre\":1}]}', '0051                                              ', NULL)";
        try {
            Connection conn = FactoryNative.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            stmt.execute();

        } catch (SQLException e3) {
            LOG.error("[getFormularios] Error al intentar obtener la lista de formularios  SQLException: ", e3);
        }

    }



}


