package com.myapps.controlmenu.dao;

import com.myapps.controlmenu.model.GrupoAd;

import java.util.List;

public interface GroupDAO {
    void deleteGroup(int paramInt);

    GrupoAd getGroupId(int paramInt);

    GrupoAd getGroupName(String paramString);

    List<GrupoAd> getGroups();

    List<GrupoAd> getGroupsDeleteRole(int paramInt);

    void insertGroup(GrupoAd paramGrupoAd);

    void updateGroup(GrupoAd paramGrupoAd);
}

