package com.myapps.controlmenu.dao;

import com.myapps.controlmenu.model.Rol;
import java.util.List;

public interface RoleDAO {
  Integer getLastId();
  
  void deleteRole(int paramInt);
  
  Rol getRoleId(int paramInt);
  
  Rol getRoleName(String paramString);
  
  List<Rol> getRoles();
  
  void insertRole(Rol paramRol);
  
  void updateRole(Rol paramRol);
}


