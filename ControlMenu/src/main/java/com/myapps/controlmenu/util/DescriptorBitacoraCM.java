package com.myapps.controlmenu.util;

public enum DescriptorBitacoraCM {
    GRUPO("Gestor Grupo"),
    ROL("Gestor ROL"),
    USUARIO("Gestor Usuario");

    private final String formulario;

    DescriptorBitacoraCM(String detalle) {

        this.formulario = detalle;

    }


    @Override
    public String toString() {
        return this.formulario;
    }
}

