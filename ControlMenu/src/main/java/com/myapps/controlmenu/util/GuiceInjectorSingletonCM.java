package com.myapps.controlmenu.util;

import com.google.inject.Guice;
 import com.google.inject.Injector;
 import com.google.inject.Module;

public class GuiceInjectorSingletonCM<T> {
    private static Injector injector;

    public static Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new Module[]{(Module) new GuiceMainModule()});
        }
        return injector;
    }

    public static <T> T getInstance(Class<T> t) {

        return (T) getInjector().getInstance(t);

    }
}


