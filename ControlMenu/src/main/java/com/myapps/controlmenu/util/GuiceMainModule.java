package com.myapps.controlmenu.util;


import com.google.inject.AbstractModule;
/*    */ import com.myapps.controlmenu.bussines.*;
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */ import com.myapps.controlmenu.controler.ControlLogin;
/*    */ import com.myapps.controlmenu.controler.ControlLoginImpl;
/*    */ import com.myapps.controlmenu.controler.ControlTimeOut;
/*    */ import com.myapps.controlmenu.controler.ControlTimeOutImpl;
/*    */ import com.myapps.controlmenu.dao.BitacoraDAO;
/*    */ import com.myapps.controlmenu.dao.BitacoraDAOImpl;
/*    */ import com.myapps.controlmenu.dao.FormularioDAO;
/*    */ import com.myapps.controlmenu.dao.FormularioDAOImpl;
/*    */ import com.myapps.controlmenu.dao.GroupDAO;
/*    */ import com.myapps.controlmenu.dao.GroupDAOImpl;
/*    */ import com.myapps.controlmenu.dao.RolFormularioDAO;
/*    */ import com.myapps.controlmenu.dao.RolFormularioDAOImpl;
/*    */ import com.myapps.controlmenu.dao.RoleDAO;
/*    */ import com.myapps.controlmenu.dao.RoleDAOImpl;
/*    */ import com.myapps.controlmenu.dao.UserDAO;
/*    */ import com.myapps.controlmenu.dao.UserDAOImpl;


 public class GuiceMainModule extends AbstractModule {

    protected void configure() {
        bind(UserBL.class).to(UserBLImpl.class);
        bind(UserDAO.class).to(UserDAOImpl.class);
        bind(GroupBL.class).to(GroupBLImpl.class);
        bind(GroupDAO.class).to(GroupDAOImpl.class);
        bind(RoleBL.class).to(RoleBLImpl.class);
        bind(RoleDAO.class).to(RoleDAOImpl.class);
        bind(BitacoraBL.class).to(BitacoraBLImpl.class);
        bind(BitacoraDAO.class).to(BitacoraDAOImpl.class);
        bind(FormularioBL.class).to(FormularioBLImpl.class);
        bind(FormularioDAO.class).to(FormularioDAOImpl.class);
        bind(RolFormularioDAO.class).to(RolFormularioDAOImpl.class);
        bind(ControlTimeOut.class).to(ControlTimeOutImpl.class);
        bind(ControlLogin.class).to(ControlLoginImpl.class);
    }

}


