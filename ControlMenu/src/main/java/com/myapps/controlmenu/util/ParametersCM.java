package com.myapps.controlmenu.util;

public class ParametersCM {
    public static String DB;
    public static String HOST = "";

    static {
        DB = "";
        PORT = "";
        USER = "";
        PASSWORD = "";

        LOGIN = "";
        PASSWORD_USER = "";
        NRO_INTENTOS = 0;
        MINUTOS_FUERA = 0;

        DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = "";
        DB_MAX_ACTIVE = "";
        DB_MIN_IDLE = "";
        DB_MAX_IDLE = "";
        DB_MAX_WAIT = "";
    }

    public static String LOGIN;
    public static int MINUTOS_FUERA;
    public static int NRO_INTENTOS;
    public static String PASSWORD_USER;
    public static String PORT;
    public static String USER;
    public static String PASSWORD;
    public static String DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS;
    public static String DB_MAX_ACTIVE;
    public static String DB_MIN_IDLE;
    public static String DB_MAX_IDLE;
    public static String DB_MAX_WAIT;
}

