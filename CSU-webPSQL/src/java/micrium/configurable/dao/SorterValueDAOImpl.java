package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.SorterValue;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class SorterValueDAOImpl implements SorterValueDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(SorterValueDAOImpl.class);

    public void deleteSorterValue(SorterValue sorterValue) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.SorterValueDAO.deleteSorterValue", sorterValue);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public SorterValue getSorterValue(SorterValue sorterValue) {
        SorterValue result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (SorterValue) session.selectOne("micrium.configurable.dao.SorterValueDAO.getSorterValue", sorterValue);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<SorterValue> getSorterValues() {
        List<SorterValue> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.SorterValueDAO.getSorterValues");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertSorterValue(SorterValue sorterValue) {
        sorterValue.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.configurable.dao.SorterValueDAO.insertSorterValue", sorterValue);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateSorterValue(SorterValue sorterValue) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.SorterValueDAO.updateSorterValue", sorterValue);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<SorterValue> getSorterValuesId(int sorterId) {
        List<SorterValue> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.SorterValueDAO.getSorterValuesId", sorterId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

}
