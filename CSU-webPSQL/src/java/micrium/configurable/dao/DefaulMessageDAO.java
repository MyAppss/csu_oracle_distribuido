package micrium.configurable.dao;

import  micrium.configurable.vo.DefaulMessage;
/**
 *
 * @author marciano
 */
public interface DefaulMessageDAO {
    

    public void updateDefaulMessage(DefaulMessage defaulMessage);
    public void enable();
    public int isEnable();
    public DefaulMessage getDefaulMessage(int messageId);
    
}
