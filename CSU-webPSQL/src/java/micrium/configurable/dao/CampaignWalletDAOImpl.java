package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.CampaignWallet;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author Pepe
 */
public class CampaignWalletDAOImpl implements CampaignWalletDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(CampaignWalletDAOImpl.class);

    public void deleteCampaignWallet(CampaignWallet campaignWallet) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignWalletDAO.deleteCampaignWallet", campaignWallet);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<CampaignWallet> getCampaignWallets() {
        List<CampaignWallet> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignWalletDAO.getCampaignWallets");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertCampaignWallet(CampaignWallet campaignWallet) {
        SqlSession session = sqlMapper.openSession();
        campaignWallet.setStatus(1);
        try {
            session.insert("micrium.configurable.dao.CampaignWalletDAO.insertCampaignWallet", campaignWallet);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignWallet(CampaignWallet campaignWallet) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignWalletDAO.updateCampaignWallet", campaignWallet);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignId(int campaingId) {

        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignWalletDAO.updateCampaignId", campaingId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public CampaignWallet getCampaignWallet(CampaignWallet CampaignWallet) {
        CampaignWallet result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (CampaignWallet) session.selectOne("micrium.configurable.dao.CampaignWalletDAO.getCampaignWallet", CampaignWallet);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignWallet> getCampaignsWalletsCampaign(int campaingId) {
        List<CampaignWallet> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignWalletDAO.getCampaignsWalletsCampaign", campaingId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignWallet> getCampaignsWalletsWallet(int walletId) {
        List<CampaignWallet> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignWalletDAO.getCampaignsWalletsWallet", walletId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

}
