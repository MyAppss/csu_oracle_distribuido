package micrium.configurable.dao;

import  micrium.configurable.vo.Configuration;
import  java.util.List;
/**
 *
 * @author marciano
 */
public interface ConfigurationDAO {
    

    public void updateConfig(Configuration config);
    public void enable();
    public int isEnable();
    
}
