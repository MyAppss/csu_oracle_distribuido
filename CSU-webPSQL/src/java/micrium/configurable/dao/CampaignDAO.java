package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.Campaign;
/**
 *
 * @author Pepe
 */
public interface CampaignDAO {
    public Campaign getCampaign(int campaignId);
    public List<Campaign> getCampaigns();
    public void updateCampaign(Campaign campaign);
    public void insertCampaign(Campaign campaign);
    public void deleteCampaign(int campaignId);
    public int maxId();
}
