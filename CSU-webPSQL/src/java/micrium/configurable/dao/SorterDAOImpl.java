package micrium.configurable.dao;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.vo.Sorter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Ivan
 */
public class SorterDAOImpl implements SorterDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(SorterDAOImpl.class);

    public Sorter getSorter(int sorterId) {
        Sorter result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Sorter) session.selectOne("micrium.configurable.dao.SorterDAO.getSorter", sorterId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<Sorter> getSorters() {
        List<Sorter> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.SorterDAO.getSorters");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertSorter(Sorter sorter) {
        SqlSession session = sqlMapper.openSession();
        sorter.setStatus(1);
        try {
            session.insert("micrium.configurable.dao.SorterDAO.insertSorter", sorter);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateSorter(Sorter sorter) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.SorterDAO.updateSorter", sorter);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void deleteSorter(int sorterId) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.SorterDAO.deleteSorter", sorterId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public int maxId() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.configurable.dao.SorterDAO.maxId").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }

    public List<Integer> listaIds() {

        List<Integer> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.SorterDAO.listaIds");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }
}
