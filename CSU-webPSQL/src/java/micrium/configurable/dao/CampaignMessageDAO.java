package micrium.configurable.dao;

import  micrium.configurable.vo.CampaignMessage;
import  java.util.List;
/**
 *
 * @author Pepe
 */
public interface CampaignMessageDAO {
    public List<CampaignMessage> getCampaignMessages();
    public void deleteCampaignMessage(CampaignMessage campaignMessage);
    public void insertCampaignMessage(CampaignMessage campaignMessage);
    public CampaignMessage       getCampaignMessage(CampaignMessage campaignMessage);
    public void            updateCampaignMessage(CampaignMessage campaignMessage);
    public List<CampaignMessage> getCampaignsMessagesCampaign(int campaingId);
    public List<CampaignMessage> getCampaignsMessagesMessage(int messageId);
    public void updateCampaignId(int campaingId);
}
