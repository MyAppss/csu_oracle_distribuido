package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.CampaignSorter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import micrium.configurable.vo.SorterValue;
import org.apache.log4j.Logger;

/**
 *
 * @author Pepe
 */
public class CampaignSorterDAOImpl implements CampaignSorterDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(CampaignSorterDAOImpl.class);

    public void deleteCampaignSorter(CampaignSorter campaignSorter) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignSorterDAO.deleteCampaignSorter", campaignSorter);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<CampaignSorter> getCampaignSorters() {
        List<CampaignSorter> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignSorterDAO.getCampaignSorters");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertCampaignSorter(CampaignSorter campaignSorter) {
        SqlSession session = sqlMapper.openSession();
        campaignSorter.setStatus(1);
        try {
            session.insert("micrium.configurable.dao.CampaignSorterDAO.insertCampaignSorter", campaignSorter);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignSorter(CampaignSorter campaignSorter) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignSorterDAO.updateCampaignSorter", campaignSorter);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignID(int campaingId) {

        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignSorterDAO.updateCampaignID", campaingId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }

    }

    public CampaignSorter getCampaignSorter(CampaignSorter CampaignSorter) {
        CampaignSorter result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (CampaignSorter) session.selectOne("micrium.configurable.dao.CampaignSorterDAO.getCampaignSorter", CampaignSorter);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignSorter> getCampaignsSortersCampaign(int campaingId) {
        List<CampaignSorter> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignSorterDAO.getCampaignsSortersCampaign", campaingId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignSorter> getCampaignsSortersSorter(int Sorterid) {
        List<CampaignSorter> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignSorterDAO.getCampaignsSortersSorter", Sorterid);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignSorter> getCampaignsSortersSorterValue(SorterValue sorterValue) {

        List<CampaignSorter> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignSorterDAO.getCampaignsSortersSorterValue", sorterValue);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }
}
