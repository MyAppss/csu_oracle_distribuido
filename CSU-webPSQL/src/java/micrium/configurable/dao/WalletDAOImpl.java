package micrium.configurable.dao;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.vo.Wallet;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Ivan
 */
public class WalletDAOImpl implements WalletDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(WalletDAOImpl.class);

    public Wallet getWallet(int walletId) {
        Wallet result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Wallet) session.selectOne("micrium.configurable.dao.WalletDAO.getWallet", walletId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<Wallet> getWallets() {
        List<Wallet> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.WalletDAO.getWallets");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<Wallet> getWalletsVisible() {

        List<Wallet> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.WalletDAO.getWalletsVisible");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertWallet(Wallet wallet) {
        SqlSession session = sqlMapper.openSession();
        wallet.setStatus(1);
        try {
            session.insert("micrium.configurable.dao.WalletDAO.insertWallet", wallet);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateWallet(Wallet wallet) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.WalletDAO.updateWallet", wallet);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void deleteWallet(int walletId) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.WalletDAO.deleteWallet", walletId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public int maxId() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.configurable.dao.WalletDAO.maxId").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }
}
