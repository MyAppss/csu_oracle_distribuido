package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.Campaign;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author Pepe
 */
public class CampaignDAOImpl implements CampaignDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(CampaignDAOImpl.class);

    public void deleteCampaign(int campaignId) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignDAO.deleteCampaign", campaignId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public Campaign getCampaign(int campaignId) {
        Campaign result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Campaign) session.selectOne("micrium.configurable.dao.CampaignDAO.getCampaign", campaignId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<Campaign> getCampaigns() {
        List<Campaign> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignDAO.getCampaigns");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertCampaign(Campaign campaign) {
        campaign.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.configurable.dao.CampaignDAO.insertCampaign", campaign);
            session.commit();

        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaign(Campaign campaign) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignDAO.updateCampaign", campaign);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public int maxId() {

        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.configurable.dao.CampaignDAO.maxId").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }

}
