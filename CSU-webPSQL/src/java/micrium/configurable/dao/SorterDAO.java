package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.Sorter;

/**
 *
 * @author Ivan
 */
public interface SorterDAO {
    public Sorter getSorter(int sorterId);
    public List<Sorter> getSorters();
    public void updateSorter(Sorter sorter);
    public void insertSorter(Sorter sorter);
    public void deleteSorter(int sorterId);
    public int maxId();
    public List<Integer> listaIds();
}
