package micrium.configurable.dao;

import  micrium.configurable.vo.CampaignWallet;
import  java.util.List;
/**
 *
 * @author Pepe
 */
public interface CampaignWalletDAO {
    public List<CampaignWallet> getCampaignWallets();
    public void deleteCampaignWallet(CampaignWallet campaignWallet);
    public void insertCampaignWallet(CampaignWallet campaignWallet);
    public CampaignWallet       getCampaignWallet(CampaignWallet campaignWallet);
    public void            updateCampaignWallet(CampaignWallet campaignWallet);
    public List<CampaignWallet> getCampaignsWalletsCampaign(int campaingId);
    public List<CampaignWallet> getCampaignsWalletsWallet(int  walletId);
    public void updateCampaignId(int campaingId);
}
