package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.CampaignMessage;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author Pepe
 */
public class CampaignMessageDAOImpl implements CampaignMessageDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(CampaignMessageDAOImpl.class);

    public void deleteCampaignMessage(CampaignMessage campaignMessage) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignMessageDAO.deleteCampaignMessage", campaignMessage);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public List<CampaignMessage> getCampaignMessages() {
        List<CampaignMessage> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignMessageDAO.getCampaignMessages");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertCampaignMessage(CampaignMessage campaignMessage) {
        SqlSession session = sqlMapper.openSession();
        campaignMessage.setStatus(1);
        try {
            session.insert("micrium.configurable.dao.CampaignMessageDAO.insertCampaignMessage", campaignMessage);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignMessage(CampaignMessage campaignMessage) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignMessageDAO.updateCampaignMessage", campaignMessage);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateCampaignId(int campaingId) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.CampaignMessageDAO.updateCampaignId", campaingId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public CampaignMessage getCampaignMessage(CampaignMessage campaignMessage) {
        CampaignMessage result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (CampaignMessage) session.selectOne("micrium.configurable.dao.CampaignMessageDAO.getCampaignMessage", campaignMessage);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignMessage> getCampaignsMessagesCampaign(int campaingId) {
        List<CampaignMessage> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignMessageDAO.getCampaignsMessagesCampaign", campaingId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<CampaignMessage> getCampaignsMessagesMessage(int messageId) {
        List<CampaignMessage> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.CampaignMessageDAO.getCampaignsMessagesMessage", messageId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

}
