package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.Message;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class MessageDAOImpl implements MessageDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(MessageDAOImpl.class);

    public void deleteMessage(int messageId) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.MessageDAO.deleteMessage", messageId);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public Message getMessage(int messageId) {
        Message result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Message) session.selectOne("micrium.configurable.dao.MessageDAO.getMessage", messageId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public List<Message> getMessages() {
        List<Message> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.configurable.dao.MessageDAO.getMessages");
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void insertMessage(Message message) {
        message.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.configurable.dao.MessageDAO.insertMessage", message);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateMessage(Message message) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.MessageDAO.updateMessage", message);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public int maxId() {

        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.configurable.dao.MessageDAO.maxId").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }
}
