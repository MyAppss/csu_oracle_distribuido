package micrium.configurable.dao;

import java.util.List;
import micrium.configurable.vo.Wallet;

/**
 *
 * @author Ivan
 */
public interface WalletDAO {
    public Wallet getWallet(int walletId);
    public List<Wallet> getWallets();
    public List<Wallet> getWalletsVisible();
    public void updateWallet(Wallet wallet);
    public void insertWallet(Wallet wallet);
    public void deleteWallet(int walletId);
    public int maxId();
}
