package micrium.configurable.dao;

import micrium.configurable.vo.Configuration;
import com.google.inject.Inject;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigurationDAOImpl implements ConfigurationDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(ConfigurationDAOImpl.class);
    
    @Override
    public void updateConfig(Configuration config) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.ConfigurationDAO.updateConfig", config);
            session.commit();
        }catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        }  finally {
            session.close();
        }
    }

    @Override
    public void enable() {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.ConfigurationDAO.enable");
            session.commit();
        }catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        }  finally {
            session.close();
        }
    }

    @Override
    public int isEnable() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.configurable.dao.ConfigurationDAO.isEnable").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }

}
