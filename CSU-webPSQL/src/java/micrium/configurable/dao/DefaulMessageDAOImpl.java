package micrium.configurable.dao;

import micrium.configurable.vo.DefaulMessage;
import com.google.inject.Inject;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class DefaulMessageDAOImpl implements DefaulMessageDAO {

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(DefaulMessageDAOImpl.class);

    public void updateDefaulMessage(DefaulMessage defaulMessage) {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.DefaulMessageDAO.updateDefaulMessage", defaulMessage);
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public DefaulMessage getDefaulMessage(int messageId) {
        DefaulMessage result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (DefaulMessage) session.selectOne("micrium.configurable.dao.DefaulMessageDAO.getDefaulMessage", messageId);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public void enable() {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.configurable.dao.DefaulMessageDAO.enable");
            session.commit();
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
    }

    public int isEnable() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.configurable.dao.DefaulMessageDAO.isEnable").toString();
            maxId = Integer.parseInt(str);
        } catch (RuntimeException e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            session.close();
        }
        return maxId;
    }

}
