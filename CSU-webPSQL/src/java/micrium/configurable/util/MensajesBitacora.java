package micrium.configurable.util;

/**
 *
 * @author Ivan
 */
public interface MensajesBitacora {
    
    
    
    public static String  WALLWT_FORM="BILLETERA";
    public static String  WALLWT_ACC_ADD="BILLETERA adicionado Descrip:";
    public static String  WALLWT_ACC_DELET="BILLETERA eliminado Descrip:";
    public static String  WALLWT_ACC_UPDATE="BILLETERA actualizado Descrip:";
    
    public static String  SORTER_FORM="CLASIFICADOR";
    public static String  SORTER_ACC_ADD="CLASIFICADOR adicionado ID:";
    public static String  SORTER_ACC_DELET="CLASIFICADOR eliminado ID:";
    public static String  SORTER_ACC_UPDATE="CLASIFICADOR actualizado ID:";
    
    public static String  SORTER_VALUE_FORM="CLASIFICADOR VALOR";
    public static String  SORTER_VALUE_ACC_ADD="CLASIFICADOR VALOR  adicionado VALOR:";
    public static String  SORTER_VALUE_ACC_DELET="CLASIFICADOR VALOR eliminado VALOR:";
    public static String  SORTER_VALUE_ACC_UPDATE="CLASIFICADOR VALOR actualizado VALOR:";
    
    public static String  MESSAGE_FORM="MENSAJE";
    public static String  MESSAGE_ACC_ADD="MENSAJE adicionado ID:";
    public static String  MESSAGE_ACC_DELET="MENSAJE eliminado ID:";
    public static String  MESSAGE_ACC_UPDATE="MENSAJE actualizado ID:";
    
    public static String  DECIL_FORM="DECIL";
    public static String  DECIL_ACC_ADD="DECIL adicionado ID:";
    public static String  DECIL_ACC_DELET="DECIL eliminado ID:";
    public static String  DECIL_ACC_UPDATE="DECIL actualizado ID:";
    
    public static String  BCCS_FORM="ESTADO BSSC";
    public static String  BCCS_ACC_ADD="ESTADO BSSC adicionado ID:";
    public static String  BCCS_ACC_DELET="ESTADO BSSC eliminado ID:";
    public static String  BCCS_ACC_UPDATE="ESTADO BSSC actualizado ID:";
    
    public static String  CAMPAIGN_FORM="CAMPAÑA";
    public static String  CAMPAIGN_ACC_ADD="CAMPAÑA adicionado ID:";
    public static String  CAMPAIGN_ACC_DELET="CAMPAÑA eliminado ID:";
    public static String  CAMPAIGN_ACC_UPDATE="CAMPAÑA actualizado ID:";
    
    public static String  ROLE_FORM="ROL";
    public static String  ROLE_ACC_UPDATE="ROL actualizado ID:";
   
}
