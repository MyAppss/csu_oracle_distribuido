package micrium.configurable.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.sql.Time;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class UtilFecha {

    private static final Logger log = Logger.getLogger(UtilFecha.class);

    public static Calendar stringToCalendar(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return cal;
    }

    public static Calendar stringToCalendarPM_AM(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return cal;
    }

    public static Timestamp stringToTimestamp(String fecha) {
        Calendar date = stringToCalendar(fecha);
        if (date != null) {
            Timestamp t = new Timestamp(date.getTimeInMillis());
            return t;
        } else {
            Calendar d = Calendar.getInstance();
            return (new Timestamp(d.getTimeInMillis()));
        }

    }

    public static Timestamp stringToTimestampPM_AM(String fecha) {
        Calendar date = stringToCalendarPM_AM(fecha);
        if (date != null) {
            Timestamp t = new Timestamp(date.getTimeInMillis());
            return t;
        } else {
            Calendar d = Calendar.getInstance();
            return (new Timestamp(d.getTimeInMillis()));
        }
    }

    public static Date stringToDate(String fecha) {
        fecha = fecha + " 00:00:00";
        Calendar date = stringToCalendar(fecha);

        if (date != null) {
            Timestamp t = new Timestamp(date.getTimeInMillis());
            return t;
        } else {
            Calendar d = Calendar.getInstance();
            return (new Timestamp(d.getTimeInMillis()));
        }
    }

    public static Time stringToTime(String fecha) {
        fecha = fecha + ":00";
        Time t = Time.valueOf(fecha);
        return t;
    }

    public static String dateToString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Timestamp dateToTimetampSW_IF(Date fecha, boolean swIni) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String str = formatter.format(fecha);
        if (swIni) {
            str = str + " 00:00:00";
        } else {
            str = str + " 23:59:59";
        }

        return stringToTimestamp(str);
    }

    public static Timestamp getTimetampActual() {
        Date dd = new Date();
        String str = dd.getTime() + "";
        str = str.substring(0, 10);
        str = str + "000";
        long tt = Long.parseLong(str);
        Timestamp t = new Timestamp(tt);
        return t;
    }

    public static String timetampToString(Timestamp fecha) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Date smDias(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static Date smMeses(Date fecha, int meses) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.MONTH, meses);
        return calendar.getTime();
    }

    public static Date smYear(Date fecha, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.YEAR, year);
        return calendar.getTime();
    }
}
