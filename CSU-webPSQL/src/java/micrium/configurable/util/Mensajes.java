package micrium.configurable.util;

/**
 *
 * @author Ivan
 */
public interface Mensajes {

    public static String VACIO_NOMBRE = "El campo NOMBRE esta vacio";
    public static String VACIO_DESCRIPCION = "El campo DESCRIPCION esta vacio";
    public static String NO_ZERO = "Ponga un numero Mayor a cero";
    public static String NO_NUMERO = "El campo Id no es numerico";

    public static String WALLET_EXISTE = "Esta Billetera ya existe";
    public static String WALLET_DEPENDENCIA = "Esta Billetera no se puede eliminar, tiene dependencias";
    public static String WALLET_EXISTE_NOMBRE = "Existe un nombre de esta Billetera";
    public static String WALLET_IN_LISTA = "EL nombre de esta Billetera ya se encuentra Registrado por favor introduzca otro nombre";
    public static String WALLET_SELEC = "Por favor seleccione una Billetera";

    public static String SORTER_EXISTE = "Este CLASIFICADOR ya existe";
    public static String SORTER_DEPENDENCIA = "Este CLASIFICADOR no se puede eliminar, tiene dependencias";
    public static String SORTER_EXISTE_NOMBRE = "Existe un nombre de este CLASIFICADOR";
    public static String SORTER_IN_LISTA = "EL nombre de este CLASIFICADOR ya se encuentra Registrado por favor introduzca otro nombre";
    public static String SORTER_SELEC = "Por favor seleccione un CLASIFICADOR";

    public static String MESSAGE_EXISTE = "Este MENSAJE ya existe";
    public static String MESSAGE_DEPENDENCIA = "Este MENSAJE no se puede eliminar, tiene dependencias";
    public static String MESSAGE_EXISTE_NOMBRE = "Existe un nombre de este MENSAJE";
    public static String MESSAGE_IN_LISTA = "EL nombre de este MENSAJE ya se encuentra Registrado por favor introduzca otro nombre";
    public static String MESSAGE_SELEC = "Por favor seleccione un MENSAJE";

    public static String CMV_EXISTE = "Este ESTADO COMVERSE ya existe";
    public static String CMV_DEPENDENCIA = "Este ESTADO COMVERSE no se puede eliminar, tiene dependencias";
    public static String CMV_EXISTE_NOMBRE = "Existe un nombre de este ESTADO COMVERSE";
    public static String CMV_IN_LISTA = "EL nombre de este ESTADO COMVERSE ya se encuentra Registrado por favor introduzca otro nombre";
    public static String CMV_SELEC = "Por favor seleccione un ESTADO COMVERSE";

    public static String BCCS_EXISTE = "Este ESTADO BCCS ya existe";
    public static String BCCS_DEPENDENCIA = "Este ESTADO BCCS no se puede eliminar, tiene dependencias";
    public static String BCCS_EXISTE_NOMBRE = "Existe un nombre de este ESTADO BCCS";
    public static String BCCS_IN_LISTA = "EL nombre de este ESTADO BCCS ya se encuentra Registrado por favor introduzca otro nombre";
    public static String BCCS_SELEC = "Por favor seleccione un ESTADO BCCS";

    public static String DECIL_EXISTE = "Este DECIL ya existe";
    public static String DECIL_DEPENDENCIA = "Este DECIL no se puede eliminar, tiene dependencias";
    public static String DECIL_EXISTE_NOMBRE = "Existe un nombre de este DECIL";
    public static String DECIL_IN_LISTA = "EL nombre de este DECIL ya se encuentra Registrado por favor introduzca otro nombre";
    public static String DECIL_SELEC = "Por favor seleccione un DECIL";

    public static String OLD_EXISTE = "Esta ANTIGUEDAD  ya existe";
    public static String OLD_DEPENDENCIA = "Esta ANTIGUEDAD no se puede eliminar, tiene dependencias";
    public static String OLD_EXISTE_NOMBRE = "Existe un nombre de esta ANTIGUEDAD";
    public static String OLD_IN_LISTA = "EL nombre de esta ANTIGUEDAD ya se encuentra Registrado por favor introduzca otro nombre";
    public static String OLD_SELEC = "Por favor seleccione una ANTIGUEDAD";

    public static String CAMPAIGN_EXISTE = "Esta CAMPAÑA ya existe";
    public static String CAMPAIGN_DEPENDENCIA = "Esta CAMPAÑA no se puede eliminar, tiene dependencias";
    public static String CAMPAIGN_EXISTE_NOMBRE = "Existe un nombre de esta CAMPAÑA";
    public static String CAMPAIGN_IN_LISTA = "EL nombre de esta CAMPAÑA ya se encuentra Registrado por favor introduzca otro nombre";
    public static String CAMPAIGN_SELEC = "Por favor seleccione una CAMPAÑA";

    public static String VALIDOR_HORA = "La Hora Inicial es mayor a la Hora Final";
    public static String VALIDOR_FECHA_INICIAL_MAY = "La Fecha Inicial es mayor a la Fecha Final";
    public static String VALIDOR_FECHA_SOLAPAMIENTO = "Existe solapamiento de Fechas";
    public static String VALIDOR_FECHA_FUERA_RANGO = "Las fechas sobrepasan el rango de fechas del ciclo";

    public static String ROLE_EXISTE = "Este ROL ya existe";
    public static String ROLE_DEPENDENCIA = "Este ROL no se puede eliminar, tiene dependencias";
    public static String ROLE_EXISTE_NOMBRE = "Existe un nombre de este ROL";
    public static String ROLE_IN_LISTA = "EL nombre de este ROL ya se encuentra Registrado por favor introduzca otro nombre";
    public static String ROLE_SELEC = "Por favor seleccione un ROL";

    public static String GROUPING_EXISTE = "Este AGRUPACION ya existe";
    public static String GROUPING_DEPENDENCIA = "Este AGRUPACION no se puede eliminar, tiene dependencias";
    public static String GROUPING_EXISTE_NOMBRE = "Existe un nombre de este AGRUPACION";
    public static String GROUPING_IN_LISTA = "EL nombre de este AGRUPACION ya se encuentra Registrado por favor introduzca otro nombre";
    public static String GROUPING_SELEC = "Por favor seleccione un AGRUPACION";

}
