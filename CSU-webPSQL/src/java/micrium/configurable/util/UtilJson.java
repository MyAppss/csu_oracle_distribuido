/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import micrium.csu.model.Resultado;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class UtilJson {

    private static final Logger LOG = Logger.getLogger(UtilJson.class);

    public static Resultado getFromJson(String textoGenerado) {
        Resultado resultado = null;
        if (textoGenerado != null) {
            try {
                Gson gson = new Gson();
                resultado = gson.fromJson(textoGenerado, Resultado.class);
            } catch (JsonSyntaxException e) {
                LOG.debug("NO CUMPLE CON UN FORMATO JSON: NO ES DE RESTFULL WAVENET: "+ textoGenerado);
            }
        }
        return resultado;
    }
}
