/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.util;

import org.apache.log4j.Logger;

/**
 *
 * @author Ivan
 */
public class UtilNumeric {
    
    private static final Logger log = Logger.getLogger(UtilNumeric.class);
    
    public static boolean isNumeric(String cadena) {
        try {
            Integer aux = Integer.parseInt(cadena);
            log.info("isNumeric :" + aux);
            return true;
        } catch (NumberFormatException nfe) {
            log.error("ERROR: " + nfe.getMessage());
            return false;
        }
    }
    
    public static boolean isDecimal(String cadena) {
        try {
            Double aux = Double.parseDouble(cadena);
            log.info("isDecimal: " + aux);
            return true;
        } catch (NumberFormatException nfe) {
            log.error("ERROR: " + nfe.getMessage());
            return false;
        }
    }
    
    public static String validateDouble(String valor, String campo) {
        
        if (valor.equals("")) {
            return "El campo " + campo + " esta Vacio";
        }
        
        if (!isDecimal(valor)) {
            return "El campo " + campo + " no es numerico";
        }
        
        Double objDou = new Double(valor);
        int k = objDou.intValue();
        if (k > 999) {
            return "El campo " + campo + " tiene que ser menor a 1000";
        }
        
        if (k < 0) {
            return "El campo " + campo + " tiene que ser datos positivos";
        }
        
        return "";
    }
}
