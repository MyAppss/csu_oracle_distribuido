/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.util;

/**
 *
 * @author marciano
 */
public class ReplaceStr {
    
     
    final static Character[][]   word={
        {'`',' '},
        {'´',' '},
        {'^',' '},
        {'¨',' '},
        {'À','A'},
        {'Á','A'},
        {'Â','A'},
        {'Ä','A'},
        
        {'È','E'},
        {'É','E'},
        {'Ê','E'},
        {'Ë','E'},
        
        {'Ì','I'},
        {'Í','I'},
        {'Î','I'},
        {'Ï','I'},
        {'Ò','O'},
        {'Ó','O'},
        {'Ô','O'},
        {'Ö','O'},
        {'Ù','U'},
        {'Ú','U'},
        {'Û','U'},
        {'Ü','U'},
        
        {'à','a'},
        {'á','a'},
        {'â','a'},
        {'ä','a'},
        {'è','e'},
        {'é','e'},
        {'ê','e'},
        {'ë','e'},
        {'ì','i'},
        {'í','i'},
        {'î','i'},
        {'ï','i'},
        {'ò','o'},
        {'ó','o'},
        {'ô','o'},
        {'ö','o'},
        {'ù','u'},
        {'ú','u'},
        {'û','u'},
        {'ü','u'},
        {'ñ','n'},
        {'Ñ','N'}
    };
     
   public String replaceBase(String str){
   
       for (int i = 0; i < word.length; i++) {
         str=str.replace(word[i][0], word[i][1]);  
       }
     return str;  
   }
}
