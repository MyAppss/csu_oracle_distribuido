/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;


/**
 *
 * @author Pepe
 */
public class Campaign implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int     campaignId;
    private String  description;
    private String  starMessage;
    private int     priority;
    private int     validityVisible;
    private int     status;

    /**
     * @return the campaignId
     */
    public int getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the starMessage
     */
    public String getStarMessage() {
        return starMessage;
    }

    /**
     * @param starMessage the starMessage to set
     */
    public void setStarMessage(String starMessage) {
        this.starMessage = starMessage;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
/**
     * @return the validityVisible
     */
    public int getValidityVisible() {
        return validityVisible;
    }

    /**
     * @param validityVisible the validityVisible to set
     */
    public void setValidityVisible(int validityVisible) {
        this.validityVisible = validityVisible;
    }
   
}
