/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author Pepe
 */
public class CampaignMessage implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int     campaignId;
    private int     messageId;
    private int     status;

    /**
     * @return the campaignId
     */
    public int getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
