/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author Ivan
 */
public class Wallet implements  Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int     walletId;
    private String  nameCmv;
    private String  nameCommercial;
    private String  unid;
    private String  operator;
    private double  value;
    private int     visibleDB;
    private int     lengthDecimal;
    private int     status;
    
   
    
    /**
     * @return the nameCmv
     */
    public String getNameCmv() {
        return nameCmv;
    }

    /**
     * @param nameCmv the nameCmv to set
     */
    public void setNameCmv(String nameCmv) {
        this.nameCmv = nameCmv;
    }

    /**
     * @return the nameCommercial
     */
    public String getNameCommercial() {
        return nameCommercial;
    }

    /**
     * @param nameCommercial the nameCommercial to set
     */
    public void setNameCommercial(String nameCommercial) {
        this.nameCommercial = nameCommercial;
    }

    /**
     * @return the unid
     */
    public String getUnid() {
        return unid;
    }

    /**
     * @param unid the unid to set
     */
    public void setUnid(String unid) {
        this.unid = unid;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the visibleDB
     */
    public int getVisibleDB() {
        return visibleDB;
    }

    /**
     * @param visible the operator
     */
    public void setVisibleDB(int visible) {
        this.visibleDB = visible;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param valor is double
     */
    public void setValue(double valor) {
        this.value = valor;
    }

    /**
     * @return the lengthDecimal
     */
    public int getLengthDecimal() {
        return lengthDecimal;
    }

    /**
     * @param lengthDecimal the lengthDecimal to set
     */
    public void setLengthDecimal(int lengthDecimal) {
        this.lengthDecimal = lengthDecimal;
    }

    /**
     * @return the walletId
     */
    public int getWalletId() {
        return walletId;
    }

    /**
     * @param walletId the walletId to set
     */
    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

  
}
