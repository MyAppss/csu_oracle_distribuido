/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author marciano
 */
public class Message implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int     messageId;
    private String  description;
    private String  messageText;
    private Timestamp  startDate;
    private Timestamp  endDate;
    private int      priority;

    private int     length;
    private int     status;
    
    private String  strStarDate;
    private String  strEndDate;
   
    private String  nameStatus;
    /**
     * @return the messageId
     */
    public int getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * @param messageText the messageText to set
     */
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    /**
     * @return the startDate
     */
    public Timestamp getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Timestamp getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

   
    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the strStarDate
     */
    public String getStrStarDate() {
        return strStarDate;
    }

    /**
     * @param strStarDate the strStarDate to set
     */
    public void setStrStarDate(String strStarDate) {
        this.strStarDate = strStarDate;
    }

    /**
     * @return the strEndDate
     */
    public String getStrEndDate() {
        return strEndDate;
    }

    /**
     * @param strEndDate the strEndDate to set
     */
    public void setStrEndDate(String strEndDate) {
        this.strEndDate = strEndDate;
    }

    /**
     * @return the nameStatus
     */
    public String getNameStatus() {
        return nameStatus;
    }

    /**
     * @param nameStatus the nameStatus to set
     */
    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    
    
}
