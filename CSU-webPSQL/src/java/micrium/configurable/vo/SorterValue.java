/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author marciano
 */
public class SorterValue implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int sorterId;
    private String nameValue;
    private int status;
    private String nameSorter;

    /**
     * @return the sorterId
     */
    public int getSorterId() {
        return sorterId;
    }

    /**
     * @param sorterId the sorterId to set
     */
    public void setSorterId(int sorterId) {
        this.sorterId = sorterId;
    }

    /**
     * @return the nameValue
     */
    public String getNameValue() {
        return nameValue;
    }

    /**
     * @param nameValue the nameValue to set
     */
    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the nameSorter
     */
    public String getNameSorter() {
        return nameSorter;
    }

    /**
     * @param nameSorter the nameSorter to set
     */
    public void setNameSorter(String nameSorter) {
        this.nameSorter = nameSorter;
    }
    
}
