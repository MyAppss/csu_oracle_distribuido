/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import micrium.configurable.util.WordKey;

/**
 *
 * @author marciano
 */
public class NodoCombo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int  sorterId;
    private List<SelectItem>      selectItemsSorter;
    private String                selectedItemSorter;
    private List<String>          listaSelection;
    private boolean               sw=true;
  
    
      
    public NodoCombo() {
        selectItemsSorter=new ArrayList<SelectItem> ();
        listaSelection=new ArrayList<String>();
        selectItemsSorter.add(new SelectItem(WordKey.NODO_COMBO ,WordKey.NODO_COMBO));        
    }
    public void ponerSeleccion(String sel){
       
        if(sw){
             selectedItemSorter=sel;
             sw=false;
          }else
            selectedItemSorter="Todos";
    }
    public void addSelectItem(SelectItem sel){
       selectItemsSorter.add(sel);          
    }
    public void addSelection(String str){
        SelectItem  sel  = new SelectItem(str,str);
        listaSelection.add(str);
        selectItemsSorter.add(sel);          
    }
    
    /**
     * @return the selectItemsSorter
     */
    public List<SelectItem> getSelectItemsSorter() {
        return selectItemsSorter;
    }

    /**
     * @param selectItemsSorter the selectItemsSorter to set
     */
    public void setSelectItemsSorter(List<SelectItem> selectItemsSorter) {
        this.selectItemsSorter = selectItemsSorter;
    }

    /**
     * @return the selectedItemSorter
     */
    public String getSelectedItemSorter() {
        return selectedItemSorter;
    }

    /**
     * @param selectedItemSorter the selectedItemSorter to set
     */
    public void setSelectedItemSorter(String selectedItemSorter) {
        this.selectedItemSorter = selectedItemSorter;
    }

    /**
     * @return the sorterId
     */
    public int getSorterId() {
        return sorterId;
    }

    /**
     * @param sorterId the sorterId to set
     */
    public void setSorterId(int sorterId) {
        this.sorterId = sorterId;
    }
    
    public List<String> getListaSelection() {
        return listaSelection;
    }

    public void setListaSelection(List<String> listaSelection) {
        this.listaSelection = listaSelection;
    }
    
    
}
