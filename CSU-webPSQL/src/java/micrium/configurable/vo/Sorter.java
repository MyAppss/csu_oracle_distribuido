/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;


/**
 *
 * @author Ivan
 */
public class Sorter implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int sorterId;
    private String name;
    private String nameQuestion;
    private int status;

     private NodoCombo ndCombo;
    /**
     * @return the sorterId
     */
    public int getSorterId() {
        return sorterId;
    }

    /**
     * @param sorterId the sorterId to set
     */
    public void setSorterId(int sorterId) {
        this.sorterId = sorterId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the nameQuestion
     */
    public String getNameQuestion() {
        return nameQuestion;
    }

    /**
     * @param nameQuestion the nameQuestion to set
     */
    public void setNameQuestion(String nameQuestion) {
        this.nameQuestion = nameQuestion;
    }

    /**
     * @return the ndCombo
     */
    public NodoCombo getNdCombo() {
        return ndCombo;
    }

    /**
     * @param ndCombo the ndCombo to set
     */
    public void setNdCombo(NodoCombo ndCombo) {
        this.ndCombo = ndCombo;
    }
    
    
    
}

