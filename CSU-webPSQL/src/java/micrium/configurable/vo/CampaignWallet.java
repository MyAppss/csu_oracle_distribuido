/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author Pepe
 */
public class CampaignWallet implements  Serializable {
    
    private static final long serialVersionUID = 1L;
   
    private int    campaignId;
    private int    walletId;
    private int    status;
    private String nameCampaign;
    private String nameCmv;
    /**
     * @return the campaignId
     */
    public int getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the nameCmv
     */
    public String getNameCmv() {
        return nameCmv;
    }

    /**
     * @param nameCmv the nameCmv to set
     */
    public void setNameCmv(String nameCmv) {
        this.nameCmv = nameCmv;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the nameCampaign
     */
    public String getNameCampaign() {
        return nameCampaign;
    }

    /**
     * @param nameCampaign the nameCampaign to set
     */
    public void setNameCampaign(String nameCampaign) {
        this.nameCampaign = nameCampaign;
    }

    /**
     * @return the walletId
     */
    public int getWalletId() {
        return walletId;
    }

    /**
     * @param walletId the walletId to set
     */
    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

   
    
}
