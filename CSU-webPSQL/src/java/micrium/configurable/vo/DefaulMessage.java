/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author marciano
 */
public class DefaulMessage implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int     defaulMessageId;
    private String  initial;
    private String  promotional;
    private String  postPaid;
    private int     visibleValidity;
    private String  comverse_errores;
    private String  comverse_off;

    /**
     * @return the defaulMessageId
     */
    public int getDefaulMessageId() {
        return defaulMessageId;
    }

    /**
     * @param defaulMessageId the defaulMessageId to set
     */
    public void setDefaulMessageId(int defaulMessageId) {
        this.defaulMessageId = defaulMessageId;
    }

    /**
     * @return the initial
     */
    public String getInitial() {
        return initial;
    }

    /**
     * @param initial the initial to set
     */
    public void setInitial(String initial) {
        this.initial = initial;
    }

    /**
     * @return the promotional
     */
    public String getPromotional() {
        return promotional;
    }

    /**
     * @param promotional the promotional to set
     */
    public void setPromotional(String promotional) {
        this.promotional = promotional;
    }

    /**
     * @return the postPaid
     */
    public String getPostPaid() {
        return postPaid;
    }

    /**
     * @param postPaid the postPaid to set
     */
    public void setPostPaid(String postPaid) {
        this.postPaid = postPaid;
    }

    /**
     * @return the visibleValidity
     */
    public int getVisibleValidity() {
        return visibleValidity;
    }

    /**
     * @param visibleValidity the visibleValidity to set
     */
    public void setVisibleValidity(int visibleValidity) {
        this.visibleValidity = visibleValidity;
    }

    /**
     * @return the comverse_errores
     */
    public String getComverse_errores() {
        return comverse_errores;
    }

    /**
     * @param comverse_errores the comverse_errores to set
     */
    public void setComverse_errores(String comverse_errores) {
        this.comverse_errores = comverse_errores;
    }

    /**
     * @return the comverse_off
     */
    public String getComverse_off() {
        return comverse_off;
    }

    /**
     * @param comverse_off the comverse_off to set
     */
    public void setComverse_off(String comverse_off) {
        this.comverse_off = comverse_off;
    }
    
}
