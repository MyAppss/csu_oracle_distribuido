/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.vo;

import java.io.Serializable;

/**
 *
 * @author marciano
 */
public class Configuration implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private int configId;
    private int statusCMV;
    private int tableActuali;

    /**
     * @return the configId
     */
    public int getConfigId() {
        return configId;
    }

    /**
     * @param configId the configId to set
     */
    public void setConfigId(int configId) {
        this.configId = configId;
    }

    /**
     * @return the statusCMV
     */
    public int getStatusCMV() {
        return statusCMV;
    }

    /**
     * @param statusCMV the statusCMV to set
     */
    public void setStatusCMV(int statusCMV) {
        this.statusCMV = statusCMV;
    }

    /**
     * @return the tableActuali
     */
    public int getTableActuali() {
        return tableActuali;
    }

    /**
     * @param tableActuali the tableActuali to set
     */
    public void setTableActuali(int tableActuali) {
        this.tableActuali = tableActuali;
    }
    
    
}
