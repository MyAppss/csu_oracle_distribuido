package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.configurable.bl.CampaignMessageBL;
import micrium.configurable.bl.MessageBL;
import micrium.configurable.util.Mensajes;
import micrium.configurable.util.ReplaceStr;
import micrium.configurable.util.UtilFecha;
import micrium.configurable.util.WordKey;
import micrium.configurable.vo.Message;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.Validador;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author marciano
 */
@ManagedBean
@ViewScoped
public class MessageForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Message messageObj;
    private String messageId;
    private Boolean edit;
    private String message = "";
    private String strPriority = "01";
    private String hidemessageId;

    private List<Message> messagesList;
    private static final Logger log = Logger.getLogger(MessageForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";

    private Date dateStartDate = new Date();
    private Date dateEndDate = new Date();

    public MessageForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
    }

    @PostConstruct
    public void init() {
        try {

            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("messageId");
            edit = str != null ? true : false;
            messageObj = new Message();
            MessageBL messageBL = GuiceInjectorSingleton.getInstance(MessageBL.class);
            messagesList = messageBL.getMessages();
            loadData();
        } catch (Exception e) {
            log.error("[init] Error: " + e.getMessage());
        }
    }

    private void loadData() {

        Date fechaHoy = new Date();

        for (Message messag : messagesList) {

            if (messag.getStartDate().after(fechaHoy)) {
                messag.setNameStatus(WordKey.MESSAGE_STAND);
            } else if (messag.getEndDate().before(fechaHoy)) {
                messag.setNameStatus(WordKey.MESSAGE_INACTIVE);
            } else if (messag.getStartDate().before(fechaHoy) && messag.getEndDate().after(fechaHoy)) {
                messag.setNameStatus(WordKey.MESSAGE_ACTIVO);
            }

            messag.setStrStarDate(UtilFecha.timetampToString(messag.getStartDate()));
            messag.setStrEndDate(UtilFecha.timetampToString(messag.getEndDate()));
        }

    }

    public String saveMessage() {
        
        String str = messageObj.getDescription();
        str = str.trim();
        messageObj.setDescription(str);

        if ( !messageObj.getDescription().equals("")) {
            if (!Validador.validateLength(messageObj.getDescription(), 0, 50)) {
                message = "El campo DESCRIPCION es demasiado largo";
                return "";
            }
        }

        if ( messageObj.getDescription().equals("")) {
            message = "El campo DESCRIPCION esta vacio";
            return "";
        }

        str = messageObj.getMessageText();
        str = str.trim();
        str = str.replaceAll("\n", "");
        messageObj.setMessageText(str);

        if ( messageObj.getMessageText().equals("")) {
            message = "El campo MENSAJE esta vacio";
            return "";
        }

        if ( messageObj.getMessageText().length() > 200) {
            message = "El campo MENSAJE tiene que ser menor a 200 caracteres";
            return "";
        }

        if ( dateStartDate.getTime() >= dateEndDate.getTime()) {
            message = "La Fecha inicial es MAYOR a la fecha final";
            return "";
        }

        str = recoredWord(messageObj.getDescription());

        if (!str.equals("")) {
            message = str;
            return "";
        }

        Timestamp fecha = UtilFecha.dateToTimetampSW_IF(dateStartDate, true);
        messageObj.setStartDate(fecha);

        fecha = UtilFecha.dateToTimetampSW_IF(dateEndDate, false);
        messageObj.setEndDate(fecha);

        messageObj.setPriority(Integer.parseInt(strPriority));
        try {
            MessageBL messageBL = GuiceInjectorSingleton.getInstance(MessageBL.class);
            str = messageObj.getMessageText();
            ReplaceStr rpc = new ReplaceStr();
            str = rpc.replaceBase(str);
            messageObj.setMessageText(str);
            messageObj.setLength(messageObj.getMessageText().length());
            if (!edit) {  //Insertar

                int k = messageBL.maxId();
                messageObj.setMessageId(k);
                messageBL.insertMessage(messageObj);
                //setBitacora(MensajesBitacora.MESSAGE_ACC_ADD);
                newMessage();
                init();
            } else {
                messageObj.setMessageId(Integer.parseInt(hidemessageId));
                messageObj.setStatus(1);
                messageBL.updateMessage(messageObj);
                // setBitacora(MensajesBitacora.MESSAGE_ACC_UPDATE);
                newMessage();
                init();
            }
        } catch (Exception ex) {
            log.error("[saveMessage] Error: " + ex.getMessage());
        }
        return "";
    }

    public String newMessage() {

        messageId = "";
        message = "";
        messageObj.setDescription("");
        messageObj.setMessageText("");
        strPriority = "01";
        edit = false;
        return "";
    }

    public String editMessage() {
        //*
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("messageId");
            int id = Integer.parseInt(str);
            MessageBL datoBL = GuiceInjectorSingleton.getInstance(MessageBL.class);
            messageObj = datoBL.getMessage(id);
            dateStartDate = (Date) messageObj.getStartDate();
            dateEndDate = (Date) messageObj.getEndDate();

            if (messageObj.getPriority() < 10) {
                strPriority = "0" + messageObj.getPriority();
            } else {
                strPriority = messageObj.getPriority() + "";
            }
            hidemessageId = messageObj.getMessageId() + "";
            messageId = hidemessageId;
            edit = true;
        } catch (Exception ex) {
            log.error("[editMessage] Error: " + ex.getMessage());
        }//*/
        return "";
    }

    public String deleteMessage() {
        //*
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("messageId");
            int id = Integer.parseInt(str);

            CampaignMessageBL datoBL = GuiceInjectorSingleton.getInstance(CampaignMessageBL.class);
            List lista = datoBL.getCampaignsMessagesMessage(id);
            if (lista.isEmpty()) {
                MessageBL datoBLAux = GuiceInjectorSingleton.getInstance(MessageBL.class);
                datoBLAux.deleteMessage(id);
                messageObj = datoBLAux.getMessage(id);
                //setBitacora(MensajesBitacora.MESSAGE_ACC_DELET);
                newMessage();
                init();
            } else {
                message = Mensajes.MESSAGE_DEPENDENCIA;
            }

        } catch (Exception ex) {
            log.error("[deleteMessage] Error: " + ex.getMessage());
        }
        return "";
    }

    private String recoredWord(String palabra) {

        palabra = palabra.toUpperCase();
        try {
            MessageBL datoBL = GuiceInjectorSingleton.getInstance(MessageBL.class);
            List<Message> lista = datoBL.getMessages();

            if (!edit) {
                for (int i = 0; i < lista.size(); i++) {
                    Message dato = lista.get(i);
                    String str = dato.getDescription().toUpperCase();
                    if (str.equals(palabra)) {
                        return Mensajes.MESSAGE_IN_LISTA;
                    }
                }
            } else {
                int id = Integer.parseInt(hidemessageId);
                for (int i = 0; i < lista.size(); i++) {
                    Message dato = lista.get(i);
                    if (dato.getMessageId() != id) {
                        String str = dato.getDescription().toUpperCase();
                        if (str.equals(palabra)) {
                            return Mensajes.MESSAGE_IN_LISTA;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("[validar] Error: " + e.getMessage());
        }
        return "";
    }

//    private void setBitacora(String accion){
//        //*
//        LogWebBL    logBL= GuiceInjectorSingleton.getInstance(LogWebBL.class);
//        LogWeb   logg=new LogWeb();
//        logg.setForm(MensajesBitacora.MESSAGE_FORM);
//        logg.setAction(accion+ messageObj.getMessageId() + " Descrip: "+messageObj.getDescription());
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
//        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr(); 
//        logg.setAddress(remoteAddr);
//        logg.setUser(strIdUs);
//        logBL.insertLogWeb(logg);//*/
//    }
    /**
     * @return the MessageId
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * @param messageId as String
     */
    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String returnList() {
        return "goMessageList";
    }

    /**
     * @return the MessagesList
     */
    public List<Message> getMessagesList() {
        return messagesList;
    }

    /**
     * @param messagesList as Lista
     */
    public void setMessagesList(List<Message> messagesList) {
        this.messagesList = messagesList;
    }

    /**
     * @return the messageObj
     */
    public Message getMessageObj() {
        return messageObj;
    }

    /**
     * @param messageObj the messageObj to set
     */
    public void setMessageObj(Message messageObj) {
        this.messageObj = messageObj;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the strPriority
     */
    public String getStrPriority() {
        return strPriority;
    }

    /**
     * @param strPriority the strPriority to set
     */
    public void setStrPriority(String strPriority) {
        this.strPriority = strPriority;
    }

    /**
     * @return the hidemessageId
     */
    public String getHidemessageId() {
        return hidemessageId;
    }

    /**
     * @param hidemessageId the hidemessageId to set
     */
    public void setHidemessageId(String hidemessageId) {
        this.hidemessageId = hidemessageId;
    }

    /**
     * @return the dateStartDate
     */
    public Date getDateStartDate() {
        return dateStartDate;
    }

    /**
     * @param dateStartDate the dateStartDate to set
     */
    public void setDateStartDate(Date dateStartDate) {
        this.dateStartDate = dateStartDate;
    }

    /**
     * @return the dateEndDate
     */
    public Date getDateEndDate() {
        return dateEndDate;
    }

    /**
     * @param dateEndDate the dateEndDate to set
     */
    public void setDateEndDate(Date dateEndDate) {
        this.dateEndDate = dateEndDate;
    }

}
