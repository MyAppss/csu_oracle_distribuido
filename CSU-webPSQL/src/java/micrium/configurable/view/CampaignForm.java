package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.configurable.bl.CampaignBL;
import micrium.configurable.bl.CampaignMessageBL;
import micrium.configurable.bl.CampaignSorterBL;

import micrium.configurable.bl.MessageBL;
import micrium.configurable.bl.SorterBL;
import micrium.configurable.bl.SorterValueBL;
import micrium.configurable.util.Mensajes;
import micrium.configurable.util.UtilFecha;
import micrium.configurable.util.WordKey;
import micrium.configurable.vo.Campaign;
import micrium.configurable.vo.CampaignMessage;
import micrium.configurable.vo.CampaignSorter;

import micrium.configurable.vo.Message;
import micrium.configurable.vo.NodoCombo;
import micrium.configurable.vo.Sorter;
import micrium.configurable.vo.SorterValue;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.Validador;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@ManagedBean
@ViewScoped
public class CampaignForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Campaign campaign;
    private String campaignId;
    private Boolean edit;
    private String message = "";
    private String messageInfAdm = "";

    private String strPriority = "01";
    private List<Sorter> sorterList;

    private static final Logger log = Logger.getLogger(CampaignForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";

    private Map<Integer, NodoCombo> mapCombo = new HashMap<Integer, NodoCombo>();

    private List<Message> messagesList;
    private List<Message> selectedDataListMessage;
    private Map<Integer, Boolean> selectedMessageIds = new HashMap<Integer, Boolean>();

    public CampaignForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
    }

    @PostConstruct
    public void init() {
        try {
            campaignId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("campaignId");
            campaign = new Campaign();
            loadPlanificadores();
            loadMessaje();
            if (campaignId != null) {
                CampaignBL campaignBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
                campaign = campaignBL.getCampaign(Integer.parseInt(campaignId));
                if (campaign.getPriority() < 10) {
                    strPriority = "0" + campaign.getPriority();
                } else {
                    strPriority = campaign.getPriority() + "";
                }
                loadUpdate(campaign.getCampaignId());
                edit = true;
            } else {
                edit = false;
            }

        } catch (Exception e) {
            log.error("[init] Error: " + e.getMessage());
        }
    }

    private void loadPlanificadores() {

        SorterBL sorterBL = GuiceInjectorSingleton.getInstance(SorterBL.class);
        SorterValueBL sorterValueBL = GuiceInjectorSingleton.getInstance(SorterValueBL.class);
        //*
        List<Sorter> sorterAuxList = sorterBL.getSorters();

        sorterList = new ArrayList<Sorter>();
        for (int i = 0; i < sorterAuxList.size(); i++) {

            Sorter sor = sorterAuxList.get(i);
            List<SorterValue> listaSV = sorterValueBL.getSorterValuesId(sor.getSorterId());

            if (listaSV.size() > 0) {
                NodoCombo nd = new NodoCombo();
                nd.setSorterId(sor.getSorterId());
                for (int j = 0; j < listaSV.size(); j++) {
                    SorterValue sorVal = listaSV.get(j);
                    nd.addSelection(sorVal.getNameValue());
                }
                sor.setNdCombo(nd);
                mapCombo.put(sor.getSorterId(), nd);
                sorterList.add(sor);
            }
        }
    }

    private void loadMessaje() {

        try {
            MessageBL messageBL = GuiceInjectorSingleton.getInstance(MessageBL.class);
            List<Message> messagesAuxList = messageBL.getMessages();
            messagesList = new ArrayList<Message>();
            Date fechaHoy = new Date();

            for (int i = 0; i < messagesAuxList.size(); i++) {

                Message mess = messagesAuxList.get(i);
                if (mess.getStartDate().after(fechaHoy)) {
                    mess.setNameStatus(WordKey.MESSAGE_STAND);
                    mess.setStrStarDate(UtilFecha.timetampToString(mess.getStartDate()));
                    mess.setStrEndDate(UtilFecha.timetampToString(mess.getEndDate()));
                    messagesList.add(mess);
                } else if (mess.getStartDate().before(fechaHoy) && mess.getEndDate().after(fechaHoy)) {
                    mess.setNameStatus(WordKey.MESSAGE_ACTIVO);
                    mess.setStrStarDate(UtilFecha.timetampToString(mess.getStartDate()));
                    mess.setStrEndDate(UtilFecha.timetampToString(mess.getEndDate()));
                    messagesList.add(mess);
                }
            }

        } catch (Exception e) {
            log.error("[loadMesages] al intentar cargar Mensajes:" + e.getMessage());
        }

    }

    private void loadUpdate(int id) {

        CampaignMessageBL campaignMessageBL = GuiceInjectorSingleton.getInstance(CampaignMessageBL.class);
        List<CampaignMessage> listaMessage = campaignMessageBL.getCampaignsMessagesCampaign(id);
        for (int i = 0; i < listaMessage.size(); i++) {//se guarda todas las realciones de los mensajes
            CampaignMessage camMess = listaMessage.get(i);
            int k = camMess.getMessageId();
            selectedMessageIds.put(k, true);
        }

        CampaignSorterBL campaignSorterBL = GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);
        List<CampaignSorter> listaSorter = campaignSorterBL.getCampaignsSortersCampaign(id);

        for (int i = 0; i < listaSorter.size(); i++) {
            CampaignSorter camSor = listaSorter.get(i);
            NodoCombo ndC = mapCombo.get(camSor.getSorterId()); //(NodoCombo)tablaNodoCombo.get(camSor.getSorterId());
            ndC.ponerSeleccion(camSor.getNameValue());
        }

    }

    public String saveCampaign() {
        log.info("[saveCampaign] Iniciando proceso..");
        if (campaign != null && !campaign.getDescription().equals("")) {
            if (!Validador.validateLength(campaign.getDescription(), 0, 80)) {
                message = "El campo Descripcion es demasiado largo";
                return "";
            }
        }

        String str = "";
        if (campaign != null) {
            str = campaign.getDescription();
            str = str.trim();
            campaign.setDescription(str);
            if (campaign.getDescription().isEmpty()) {
                message = "El campo Descripcion esta vacio";
                return "";
            }
        }

        if (campaign != null) {
            campaign.setStarMessage("");
        }

        List<Integer> listaIdMessage = new ArrayList<Integer>();

        selectedDataListMessage = new ArrayList<Message>();
        for (Message dataItem : messagesList) {

            if (selectedMessageIds.get(dataItem.getMessageId()).booleanValue()) {
                listaIdMessage.add(dataItem.getMessageId());
                selectedDataListMessage.add(dataItem);
            }
        }

        if (listaIdMessage.isEmpty()) {
            message = "Seleccione un mensaje";
            return "";
        }

        if (campaign != null) {
            str = recoredWord(campaign.getDescription());

            if (!str.equals("")) {
                message = str;
                return "";
            }
        }
        //*
        try {
            CampaignBL campaignBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
            int k = Integer.parseInt(strPriority);
            campaign.setPriority(k);
            int id = 0;

            campaign.setValidityVisible(0);
            if (!edit) {  // Insertar 

                id = campaignBL.maxId();
                campaign.setCampaignId(id);
                campaignBL.insertCampaign(campaign);
                log.debug("---guardo campania------=" + campaign.getDescription());

                CampaignMessageBL campaignMessageBL = GuiceInjectorSingleton.getInstance(CampaignMessageBL.class);
                for (int i = 0; i < listaIdMessage.size(); i++) {//se guarda todas las realciones de los mensajes
                    CampaignMessage camMess = new CampaignMessage();
                    camMess.setCampaignId(id);
                    camMess.setMessageId(listaIdMessage.get(i));
                    campaignMessageBL.insertCampaignMessage(camMess);
                }
                log.debug("---campana_message------=" + campaign.getDescription());

                CampaignSorterBL campaignSorterBL = GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);

                Iterator e = mapCombo.keySet().iterator();
                while (e.hasNext()) {        //se guarda todas las relaciones de los planificadores

                    int kId = Integer.parseInt(e.next().toString());
                    NodoCombo ndC = mapCombo.get(kId);
                    int idSorter = ndC.getSorterId();
                    if (!ndC.getSelectedItemSorter().equals(WordKey.NODO_COMBO)) {

                        CampaignSorter camSor = new CampaignSorter();
                        camSor.setCampaignId(id);
                        camSor.setSorterId(idSorter);
                        camSor.setNameValue(ndC.getSelectedItemSorter());
                        campaignSorterBL.insertCampaignSorter(camSor);
                    }
                }

//                
                //setBitacora(MensajesBitacora.CAMPAIGN_ACC_ADD);
                return "/view/CampaignList.xhtml?faces-redirect=true";
            } else {
                campaign.setStatus(1);
                campaignBL.updateCampaign(campaign);
                id = campaign.getCampaignId();

                CampaignMessageBL campaignMessageBL = GuiceInjectorSingleton.getInstance(CampaignMessageBL.class);
                campaignMessageBL.updateCampaignId(id);
                for (int i = 0; i < listaIdMessage.size(); i++) {//se guarda todas las realciones de los mensajes
                    CampaignMessage camMess = new CampaignMessage();
                    camMess.setCampaignId(id);
                    camMess.setMessageId(listaIdMessage.get(i));
                    CampaignMessage cCAux = campaignMessageBL.getCampaignMessage(camMess);
                    if (cCAux == null) {
                        campaignMessageBL.insertCampaignMessage(camMess);

                    } else {
                        campaignMessageBL.updateCampaignMessage(camMess);
                    }

                }

                CampaignSorterBL campaignSorterBL = GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);
                campaignSorterBL.updateCampaignID(id);
                Iterator e = mapCombo.keySet().iterator();
                while (e.hasNext()) {        //se guarda todas las relaciones de los planificadores       
                    int kId = Integer.parseInt(e.next().toString());
                    NodoCombo ndC = mapCombo.get(kId);
                    int idSorter = ndC.getSorterId();
                    CampaignSorter camSor;
                    if (!ndC.getSelectedItemSorter().equals(WordKey.NODO_COMBO)) {

                        camSor = new CampaignSorter();
                        camSor.setCampaignId(id);
                        camSor.setSorterId(idSorter);
                        camSor.setNameValue(ndC.getSelectedItemSorter());
                        CampaignSorter cCAux = campaignSorterBL.getCampaignSorter(camSor);
                        if (cCAux == null) {
                            campaignSorterBL.insertCampaignSorter(camSor);
                        } else {
                            campaignSorterBL.updateCampaignSorter(camSor);
                        }
                    }
                }

//              setBitacora(MensajesBitacora.CAMPAIGN_ACC_UPDATE);
                return "/view/CampaignList.xhtml?faces-redirect=true";
            }
        } catch (Exception ex) {
            log.error("[saveCampaign] Error: " + ex.getMessage());
        }//*/
        return "";
    }

    public String newCampaign() {
        campaign.setCampaignId(0);
        campaign.setDescription("");
        campaignId = "";
        edit = false;
        return "";
    }

    public String editCampaign() {
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("CampaignId");
            int id = Integer.parseInt(str);
            CampaignBL datoBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
            campaign = datoBL.getCampaign(id);
            campaignId = campaign.getCampaignId() + "";
            edit = true;
        } catch (Exception ex) {
            log.error("[editCampaign] Error: " + ex.getMessage());
        }
        return "";
    }

    private String recoredWord(String palabra) {

        palabra = palabra.toUpperCase();
        try {
            CampaignBL datoBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
            List<Campaign> lista = datoBL.getCampaigns();

            if (!edit) {
                for (Campaign campa : lista) {
                    String str = campa.getDescription().toUpperCase();
                    if (str.equals(palabra)) {
                        return Mensajes.CAMPAIGN_IN_LISTA;
                    }
                }
            } else {
                int id = campaign.getCampaignId();
                for (Campaign campa : lista) {

                    if (campa.getCampaignId() != id) {
                        String str = campa.getDescription().toUpperCase();
                        if (str.equals(palabra)) {
                            return Mensajes.CAMPAIGN_IN_LISTA;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("[validar] Error: " + e.getMessage());
        }
        return "";
    }

//    public void setBitacora(String accion){
//        
//        LogWebBL    logBL= GuiceInjectorSingleton.getInstance(LogWebBL.class);
//        LogWeb   logg=new LogWeb();
//        logg.setForm(MensajesBitacora.CAMPAIGN_FORM);
//        logg.setAction(accion+campaign.getCampaignId() +" Descrip: "+campaign.getDescription());
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
//        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr(); 
//        logg.setAddress(remoteAddr);
//        logg.setUser(strIdUs);
//        logBL.insertLogWeb(logg);//*/
//    } 
    public String returnList() {
        return "goCampaignList";
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param mensaje as String
     */
    public void setMessage(String mensaje) {
        this.message = mensaje;
    }

    /**
     * @return the Campaign
     */
    public Campaign getCampaign() {
        return campaign;
    }

    /**
     * @param campaign is Campaign
     */
    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    /**
     * @return the CampaignId
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId is Campaign
     */
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the messageInfAdm
     */
    public String getMessageInfAdm() {
        return messageInfAdm;
    }

    /**
     * @param messageInfAdm the messageInfAdm to set
     */
    public void setMessageInfAdm(String messageInfAdm) {
        this.messageInfAdm = messageInfAdm;
    }

    /**
     * @return the strPriority
     */
    public String getStrPriority() {
        return strPriority;
    }

    /**
     * @param strPriority the strPriority to set
     */
    public void setStrPriority(String strPriority) {
        this.strPriority = strPriority;
    }

    /**
     * @return the sorterList
     */
    public List<Sorter> getSorterList() {
        return sorterList;
    }

    /**
     * @param sorterList the sorterList to set
     */
    public void setSorterList(List<Sorter> sorterList) {
        this.sorterList = sorterList;
    }

    /**
     * @return the messagesList
     */
    public List<Message> getMessagesList() {
        return messagesList;
    }

    /**
     * @param messagesList the messagesList to set
     */
    public void setMessagesList(List<Message> messagesList) {
        this.messagesList = messagesList;
    }

    /**
     * @return the selectedDataListMessage
     */
    public List<Message> getSelectedDataListMessage() {
        return selectedDataListMessage;
    }

    /**
     * @param selectedDataListMessage the selectedDataListMessage to set
     */
    public void setSelectedDataListMessage(List<Message> selectedDataListMessage) {
        this.selectedDataListMessage = selectedDataListMessage;
    }

    /**
     * @return the selectedMessageIds
     */
    public Map<Integer, Boolean> getSelectedMessageIds() {
        return selectedMessageIds;
    }

    /**
     * @param selectedMessageIds the selectedMessageIds to set
     */
    public void setSelectedMessageIds(Map<Integer, Boolean> selectedMessageIds) {
        this.selectedMessageIds = selectedMessageIds;
    }

}
