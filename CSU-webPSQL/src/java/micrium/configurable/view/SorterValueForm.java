package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.configurable.bl.CampaignSorterBL;
import micrium.configurable.bl.SorterBL;
import micrium.configurable.bl.SorterValueBL;
import micrium.configurable.util.Mensajes;
import micrium.configurable.vo.Sorter;
import micrium.configurable.vo.SorterValue;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
/**
 *
 * @author marciano
 */
@ManagedBean
@ViewScoped
public class SorterValueForm implements  Serializable{
    private static final long serialVersionUID = 1L;

    private List<SelectItem>      selectItemsSorter;
    private String                selectedItemSorter;
    private String                namesorterValue;
    private List<SorterValue>     sorterValueList;
    private String                message="";
    private String                idSorter;
    private static Logger         log = Logger.getLogger(SorterValueForm.class);
    private static String         ARCHIVO_LOG4J = "log.properties";
    
    public SorterValueForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
    }
    
    
    @PostConstruct
    public void init() {
        try {
            loadFillSelectItems();
            actualizarLista();            
        }catch (Exception e){
            log.error("[init] Error: "+e.getMessage());
        }
    }
    
    private void loadFillSelectItems() {
        
        selectItemsSorter=new ArrayList<SelectItem>();
        selectItemsSorter.add(new SelectItem("-1","Seleccione un Clasificador"));
        SorterBL   sorterBL=GuiceInjectorSingleton.getInstance(SorterBL.class);
        List<Sorter>  sorterList=sorterBL.getSorters();
        
        for (Sorter sort : sorterList) {
             selectItemsSorter.add(new SelectItem(sort.getSorterId(),sort.getName()));
        }
    }
    
    private void actualizarLista(){
        SorterValueBL   datoBL=GuiceInjectorSingleton.getInstance(SorterValueBL.class);
        sorterValueList=datoBL.getSorterValues(); 
    }
    public String newSortervalue(){
        selectedItemSorter="-1";
        namesorterValue="";
        return "";
    }
    public String saveSorterValue() {
        int sorterId=Integer.parseInt(getSelectedItemSorter());
        
        if(sorterId==-1){
            setMessage(Mensajes.SORTER_SELEC);
            return "";
        }
        
        namesorterValue=namesorterValue.trim();
        
        if(namesorterValue.equals("")){
            message="El VALOR esta vacio";
            return "";
          }
        String str=recoredWord(namesorterValue, sorterId);
        if(!str.equals("")){
            message=str;
            return "";
        }
        
            try {
                SorterValueBL sorterValueBL = GuiceInjectorSingleton.getInstance(SorterValueBL.class);
                SorterValue  cp=new SorterValue();
                cp.setSorterId(sorterId);
                cp.setNameValue(namesorterValue);
                SorterValue cCAux =sorterValueBL.getSorterValue(cp);
                if(cCAux==null){
                    sorterValueBL.insertSorterValue(cp);
                    //setBitacora(MensajesBitacora.SORTER_VALUE_ACC_ADD, sorterId);
                    actualizarLista();
                    newSortervalue();
                }else{
                    if(cCAux.getStatus()==0){
                        sorterValueBL.updateSorterValue(cp);
                       // setBitacora(MensajesBitacora.SORTER_VALUE_ACC_ADD, sorterId);
                        actualizarLista();
                        newSortervalue();
                    }else
                        setMessage(Mensajes.SORTER_IN_LISTA);
                }    
            } catch (Exception ex) {
                log.error("[savesorterValue] Error: "+ex.getMessage());
            }
        
           
        return "";
    }
    
    public String deleteSorterValue() {
        
        try {   
            
            String cad =  FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sorterId");
            String strNameValue=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nameValue");
            
            int sorterId=Integer.parseInt(cad);
            SorterValue  cp=new SorterValue();
            cp.setSorterId(sorterId);
            cp.setNameValue(strNameValue);
            
            CampaignSorterBL   camSorBL=GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);
            
            List  lista=camSorBL.getCampaignsSortersSorterValue(cp);
            if(lista.isEmpty()){
                SorterValueBL sorterValueBL = GuiceInjectorSingleton.getInstance(SorterValueBL.class);
                namesorterValue=strNameValue;
                sorterValueBL.deleteSorterValue(cp);
             //   setBitacora(MensajesBitacora.SORTER_VALUE_ACC_DELET, sorterId);
                actualizarLista();
                newSortervalue();
            }else{
                message="Existen dependencias con este VALOR";
                return "";
            }

        } catch (Exception ex) {
            log.error("[deletesorterValue] Error: "+ex.getMessage());
        }
        return "";
    }
   
//    private void setBitacora(String accion,int idRel){
//        
//        LogWebBL    logBL= GuiceInjectorSingleton.getInstance(LogWebBL.class);
//        LogWeb   logg=new LogWeb();
//        logg.setForm(MensajesBitacora.SORTER_VALUE_FORM);
//        
//        SorterBL  datoBL=GuiceInjectorSingleton.getInstance(SorterBL.class);
//        Sorter dato=datoBL.getSorter(idRel);
//        
//        logg.setAction(accion+ namesorterValue+  " - ID Clasificador: "+ idRel+" Nom: "+dato.getName());
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
//        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr(); 
//        logg.setAddress(remoteAddr);
//        logg.setUser(strIdUs);
//        logBL.insertLogWeb(logg);
//    }

   private String recoredWord(String palabra,int id){
           
        palabra=palabra.toUpperCase();
        try {
            SorterValueBL datoBL = GuiceInjectorSingleton.getInstance(SorterValueBL.class);
            List<SorterValue> lista = datoBL.getSorterValuesId(id);
            
            for (SorterValue sorVal : lista) {
                String str=sorVal.getNameValue().toUpperCase();
                if(str.equals(palabra))
                    return Mensajes.SORTER_IN_LISTA;
            }
            
        }catch (Exception e){
            log.error("[validar] Error: "+e.getMessage());
        }
        return "";
    }

    /**
     * @return the selectItemsSorter
     */
    public List<SelectItem> getSelectItemsSorter() {
        return selectItemsSorter;
    }

    /**
     * @param selectItemsSorter the selectItemsSorter to set
     */
    public void setSelectItemsSorter(List<SelectItem> selectItemsSorter) {
        this.selectItemsSorter = selectItemsSorter;
    }

    /**
     * @return the selectedItemSorter
     */
    public String getSelectedItemSorter() {
        return selectedItemSorter;
    }

    /**
     * @param selectedItemSorter the selectedItemSorter to set
     */
    public void setSelectedItemSorter(String selectedItemSorter) {
        this.selectedItemSorter = selectedItemSorter;
    }

   
    /**
     * @return the sorterValueList
     */
    public List<SorterValue> getSorterValueList() {
        return sorterValueList;
    }

    /**
     * @param sorterValueList the sorterValueList to set
     */
    public void setSorterValueList(List<SorterValue> sorterValueList) {
        this.sorterValueList = sorterValueList;
    }

   
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the idSorter
     */
    public String getIdSorter() {
        return idSorter;
    }

    /**
     * @param idSorter the idSorter to set
     */
    public void setIdSorter(String idSorter) {
        this.idSorter = idSorter;
    }

    

    /**
     * @return the namesorterValue
     */
    public String getNamesorterValue() {
        return namesorterValue;
    }

    /**
     * @param namesorterValue the namesorterValue to set
     */
    public void setNamesorterValue(String namesorterValue) {
        this.namesorterValue = namesorterValue;
    }
    

}
