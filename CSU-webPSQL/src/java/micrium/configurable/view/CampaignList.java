package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.configurable.bl.CampaignBL;
import micrium.configurable.bl.CampaignMessageBL;
import micrium.configurable.bl.CampaignSorterBL;
import micrium.configurable.vo.Campaign;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Ivan
 */
@ManagedBean
@ViewScoped
public class CampaignList implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Campaign> campaignsList;
    private String message = "";
    private static final Logger log = Logger.getLogger(CampaignList.class);
    private static final String ARCHIVO_LOG4 = "log.properties";

    public CampaignList() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4);
        PropertyConfigurator.configure(url);
    }

    @PostConstruct
    public void init() {
        try {
            CampaignBL CampaignBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
            campaignsList = CampaignBL.getCampaigns();
        } catch (Exception e) {
            log.error("[init] Error: " + e.getMessage());
        }
    }

    public String newCampaign() {
        String result = null;
        try {
            result = "goCampaignForm";
        } catch (Exception ex) {
            log.error("[newCampaign] Error: " + ex.getMessage());
        }
        return result;
    }

    public String deleteCampaign() {

        try {
            Integer campaignId1 = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("campaignId"));
            eliminarRelation(campaignId1);
            if (campaignId1 != null) {
                CampaignBL CampaignBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
                CampaignBL.deleteCampaign(campaignId1);
                //setBitacora(MensajesBitacora.CAMPAIGN_ACC_DELET, campaignId1);
                init();
            }

        } catch (NumberFormatException ex) {
            log.error("[deleteCampaign] Error: " + ex.getMessage());
        }
        return "";
    }

    private void eliminarRelation(Integer id) {
//        CampaignWalletBL campaignWalletBL = GuiceInjectorSingleton.getInstance(CampaignWalletBL.class);
//        campaignWalletBL.updateCampaignId(id);
        if (id != null) {
            CampaignMessageBL campaignMessageBL = GuiceInjectorSingleton.getInstance(CampaignMessageBL.class);
            campaignMessageBL.updateCampaignId(id);
            CampaignSorterBL campaignSorterBL = GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);
            campaignSorterBL.updateCampaignID(id);
        }
    }
//    private void setBitacora(String accion,int id){
//       
//         //*
//        LogWebBL    logBL= GuiceInjectorSingleton.getInstance(LogWebBL.class);
//        LogWeb   logg=new LogWeb();
//        logg.setForm(MensajesBitacora.CAMPAIGN_FORM);
//        
//        CampaignBL CampaignBL = GuiceInjectorSingleton.getInstance(CampaignBL.class);
//        Campaign campaign = CampaignBL.getCampaign(id);
//        
//        logg.setAction(accion+campaign.getCampaignId()+" Descrip: "+campaign.getDescription());
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
//        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr(); 
//        logg.setAddress(remoteAddr);
//        logg.setUser(strIdUs);
//        logBL.insertLogWeb(logg);//*/
//     } 

    public String editCampaign() {
        return "goCampaignForm";
    }

    /**
     * @return the CampaignsList
     */
    public List<Campaign> getCampaignsList() {
        return campaignsList;
    }

    /**
     * @param campaignsList as Lista
     */
    public void setCampaignsList(List<Campaign> campaignsList) {
        this.campaignsList = campaignsList;
    }

    /**
     * @return the mensaje
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message as String
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
