/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.BilleteraBUSSI;
import micrium.csu.bussines.ConfigBUSSI;
import micrium.csu.bussines.DetalleMenuBUSSI;
import micrium.csu.bussines.MenuBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.bussines.UnitTypeBUSSI;
import micrium.csu.model.Billetera;
import micrium.csu.model.Config;
import micrium.csu.model.DetalleMenu;
import micrium.csu.model.Menu;
import micrium.csu.model.Parametro;
import micrium.csu.model.UnitType;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.view.ControlerBitacora;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.primefaces.model.DualListModel;

import micrium.csu.util.CustomMessage;
import micrium.csu.util.RecargarServicios;

/**
 *
 * @author Vehimar
 */
@ManagedBean
@SessionScoped
//@RequestScoped
//@ViewScoped
public class ConfigMenus implements Serializable {

    private DualListModel<Billetera> billeteraDual;
    private Billetera billeteraSelect;

    private static final long serialVersionUID = 1L;
    private static final String ARCHIVO_LOG4J = "log.properties";
    private static final Logger log = Logger.getLogger(ConfigMenus.class);

    public static final String CANAL_NINGUNO = "NINGUNO";
    public static final String CANAL_USSD = "USSD";
    public static final String CANAL_SMS = "SMS";
    public static final String CANAL_TODOS = "TODOS";

    public static final String INFO_NINGUNO = "NINGUNO";
    public static final String INFO_SALDO_PAQUETIGO = "SALDO_PAQUETIGO";
    public static final String INFO_OTROS_SALDOS = "OTROS_SALDOS";
    public static final String INFO_INFO_DE_RESERVA = "INFO_DE_RESERVA";
    public static final String INFO_PAQUETIGOS_ACUMULADOS = "PAQUETIGOS_ACUMULADOS";
    public static final String INFO_SELECCIONAR = "SELECCIONAR";

    public static final String INFO_PAQUETIGOS_DOBLE = "PAQUETIGO_DOBLE";

    private Integer UNITTYPE_TODAS = 0;
    public static final String UNITTYPE_TODAS_NOMBRE = "Todas";
    public static final Integer UNITTYPE_NINGUNO = 0 /*new Integer(0)*/;

    private String rowMaxTable = Parameters.NroFilasDataTable;

    private String userId;
    private Boolean edit;
    private Parametro CantidadCabecera;

    private Config configuracion;
    private Menu menu;
    private List<Menu> listaMenu;
    private List<DetalleMenu> listaDetalle;

    private boolean visible;
    private boolean mostrarMayorQueCero;
    private boolean mostrarMayorQueCeroAcumulado;
    private boolean tieneHijos;
    private List<SelectItem> selectItemsPadre;
    private String selPadre;
    private String selCanal;

    private List<SelectItem> selectItemsNivel;
    private String selNivel;

    private List<SelectItem> selectItemsInformacion;
    private String selInformacion;
    private Integer selectedUnitType;
    private boolean disabledPickList;
    private boolean disabledUnitType;
    private List<UnitType> unitTypes;
    private boolean tieneBillletera;
    private CustomMessage messages;

    @PostConstruct
    public void init() {
        try {
            URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
            PropertyConfigurator.configure(url);
            userId = ControlerBitacora.getNombreUser();
            listaMenu = new ArrayList<>();
            messages = null;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void cargarComboPadre() {
        try {
            List<Menu> list = listaMenu;
            selPadre = "-1";
            selectItemsPadre = new LinkedList<>();
            SelectItem s = new SelectItem("-1", "Seleccionar Padre");
            selectItemsPadre.add(s);
            if (list != null) {
                for (Menu menu : list) {
                    if (menu.getNivel().equals(1)) {
                        SelectItem sel = new SelectItem(menu.getMenuId(), menu.getNombre());
                        selectItemsPadre.add(sel);
                    }
                }
            }
        } catch (Exception e) {
            log.error("[cargarComboPadre] Error al intentar cargar combo padre: ", e);
        }
    }

    private void cargarComboNivel() {
        try {
            selNivel = "1";
            selectItemsNivel = new LinkedList<SelectItem>();
            SelectItem s1 = new SelectItem("1", "Nivel 1");
            SelectItem s2 = new SelectItem("2", "Nivel 2");
            selectItemsNivel.add(s1);
            selectItemsNivel.add(s2);
        } catch (Exception e) {
            log.error("[cargarComboNivel] Error al intentar cargar combo nivel: ", e);
        }
    }

    private void cargarComboInformacion(int nivel) {
        try {
            selectItemsInformacion = new LinkedList<SelectItem>();

            if (nivel == 1) {
                if (tieneBillletera && tieneHijos) {
                    selectItemsInformacion.add(new SelectItem(INFO_NINGUNO, "Ninguno"));
                } else if (tieneBillletera) {
                    disabledPickList = (tieneHijos);
                    selectItemsInformacion.add(new SelectItem(INFO_SELECCIONAR, "--Seleccionar--------"));
                    selectItemsInformacion.add(new SelectItem(INFO_OTROS_SALDOS, "Otros Saldos"));

                } else if (!tieneBillletera && !tieneHijos) {
                    selectItemsInformacion.add(new SelectItem(INFO_SELECCIONAR, "--Seleccionar--------"));
                    selectItemsInformacion.add(new SelectItem(INFO_INFO_DE_RESERVA, "Mas Info de Reserva"));
                } else if (tieneHijos && !tieneBillletera) {
                    selectItemsInformacion.add(new SelectItem(INFO_SELECCIONAR, "--Seleccionar--------"));
                    selectItemsInformacion.add(new SelectItem(INFO_SALDO_PAQUETIGO, "Saldo Paquetigos"));
                }
            } else if (nivel == 2) {
                disabledPickList = false;
                disabledUnitType = false;
                tieneHijos = false;
                selectItemsInformacion.add(new SelectItem(INFO_SELECCIONAR, "--Seleccionar--------"));
                selectItemsInformacion.add(new SelectItem(INFO_PAQUETIGOS_ACUMULADOS, "Paquetigos Acumulados"));
                selectItemsInformacion.add(new SelectItem(INFO_SALDO_PAQUETIGO, "Saldo Paquetigos"));
                selectItemsInformacion.add(new SelectItem(INFO_PAQUETIGOS_DOBLE, "Paquetigo Doble"));
            }

        } catch (Exception e) {
            log.error("[cargarComboNivel] Error al intentar cargar combo nivel: ", e);
        }
    }

    public void iniciarConfiguracion() {
        try {
            String IdConfig = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");

            configuracion = obtenerConfiguracion(Integer.valueOf(IdConfig));
            UnitTypeBUSSI unitTypeBUSSI = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            unitTypes = unitTypeBUSSI.obtenerLista();
            for (UnitType type : unitTypes) {
                if (type.getNombre().equals(UNITTYPE_TODAS_NOMBRE)) {
                    UNITTYPE_TODAS = type.getUnitTypeId();
                    break;
                }
            }
            if (configuracion != null) {
                menu = new Menu();
                menu.setPosicion(1);
                menu.setNivel(1);
                selectedUnitType = UNITTYPE_NINGUNO;
                cargarListaBilleterea();
                cargarConfigCabecera();
                obtenerCantidadCabecera();
            }
            messages = null;
            edit = false;
            visible = true;
            mostrarMayorQueCero = false;
            mostrarMayorQueCeroAcumulado = false;
            tieneHijos = false;
            tieneBillletera = false;
            selCanal = "NINGUNO";
            selInformacion = "NINGUNO";
            cargarComboPadre();
            cargarComboNivel();
            cargarComboInformacion(1);

        } catch (NumberFormatException e) {
            log.error("[iniciarNuevo]:" + e);
        }
    }

    private void obtenerCantidadCabecera() {
        ParametroBUSSI parametroBUSI = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
        CantidadCabecera = parametroBUSI.obtenerName("default_cantidad_cabeceras");
    }

    private Config obtenerConfiguracion(Integer idConfi) {
        Config confi;
        ConfigBUSSI configBussi = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
        confi = configBussi.obtenerId(idConfi);
        if (confi != null) {
            return confi;
        }
        return null;
    }

    private void cargarListaBilleterea() {
        try {
            BilleteraBUSSI billeteraBussi = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
            List<Billetera> list = billeteraBussi.obtenerListaByIdUnitType(selectedUnitType);
            List<Billetera> listaSources = new ArrayList<>();
            List<Billetera> listaTarget = new ArrayList<>();
            if (list != null) {
                for (Billetera billetera : list) {
                    listaSources.add(billetera);
                }
            }
            billeteraDual = new DualListModel<>(listaSources, listaTarget);
        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Origen:" + e.getMessage(), e);
        }
    }

    private Integer cargarConfigCabecera() {
        Integer cantidad = 0;
        listaMenu.clear();
        MenuBUSSI menuBUSSI = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
        DetalleMenuBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
        List<Menu> lista = menuBUSSI.obtenerListaByConfigId(configuracion.getConfigId());

        if (lista != null) {
            for (int i = 0; i < lista.size(); i++) {
                Menu m = lista.get(i);
                List<DetalleMenu> list = detalleBUSII.obtenerIdCabecera(m.getMenuId());
                StringBuilder stb = new StringBuilder();
                for (int j = 0; j < list.size(); j++) {
                    DetalleMenu detalle = list.get(j);
                    if (stb.length() > 0) {
                        stb.append("<br>").append(detalle.getNombreComverseBilletera());
                    } else {
                        stb.append(detalle.getNombreComverseBilletera());
                    }
                }
                m.setStrBilleteras(stb.toString());
                listaMenu.add(m);
            }
            if (!lista.isEmpty()) {
                cantidad = lista.size();
            }
        }

        return cantidad;
    }

    private String save() {
        log.info("[saveMenu]userId=" + userId + "|Iniciando proceso...");
        String str = "";
        MenuBUSSI menuBUSII = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);

        Integer idCabe = 0;
        idCabe = 0;
        if (menu.getTieneHijos().equals("true")) {
            menu.setInformacion(INFO_NINGUNO);
        }
        int posicionLevel1 = 1;
        int posicionLevel2 = 1;
        for (Menu me : this.listaMenu) {
            if (me.getNivel() == 1) {
                posicionLevel1++;
            } else {
                posicionLevel2++;
            }
        }
        menu.setPosicion((menu.getNivel() == 1) ? posicionLevel1 : posicionLevel2);
        idCabe = menuBUSII.insert(menu);

        if (idCabe > 0) {

            List<Billetera> lista = billeteraDual.getTarget();
            if (selInformacion.equals(INFO_INFO_DE_RESERVA) || UNITTYPE_NINGUNO.equals(selectedUnitType)) {
                lista = new ArrayList<>();
            }

            guardarDetalle(idCabe, lista);
            log.info("[saveMenu]userId=" + userId + "|Inserto correctamente|MenuId=" + menu.getMenuId());
            str = ParameterMessage.Menu_MESSAGE_GUARDAR + menu.getMenuId();
            ControlerBitacora.insert(DescriptorBitacoraLC.MENU, menu.getMenuId() + "", menu.getNombre());

            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
        }
        return str;
    }

    private String editar() {
        log.info("[editarConfiCabecera]userId=" + userId + "|Iniciando proceso...");
        String str = "";
        try {
            MenuBUSSI menuBUSII = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
            DetalleMenuBUSSI detalleBUSSI = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
            if (menu.getTieneHijos().equals("true")) {
                menu.setInformacion(INFO_NINGUNO);
            }
            menuBUSII.update(menu);

            List<Billetera> listaSelect = billeteraDual.getTarget();

            if (selInformacion.equals(INFO_INFO_DE_RESERVA) || (selNivel.equals("1") && tieneHijos) || UNITTYPE_NINGUNO.equals(selectedUnitType)) {
                listaSelect = new ArrayList<>();
            }

            for (int i = 0; i < listaSelect.size(); i++) {
                Billetera billetera = listaSelect.get(i);
                DetalleMenu detalleMenu = exiteBilleteraInDetalle(billetera, listaDetalle);
                if (detalleMenu == null) { //NUEVO 
                    guardarDetalle(menu.getMenuId(), billetera, i + 1);
                } else {//ACTUALIZAR SOLO EL ORDEN
                    guardarUpdateDetalle(detalleMenu, i + 1);
                }
            }

            for (int i = 0; i < listaDetalle.size(); i++) {
                DetalleMenu detalle = listaDetalle.get(i);
                boolean existe = exiteInSelected(detalle.getBilleteraId(), listaSelect);
                if (!existe) {
                    detalleBUSSI.delete(detalle.getMenuDetalleId());
                }
            }
            log.info("[editarMenu]userId=" + userId + "|Modifico correctamente|menuId=" + menu.getMenuId());
            str = ParameterMessage.Menu_MESSAGE_MODIFICAR + menu.getMenuId();
            ControlerBitacora.update(DescriptorBitacoraLC.MENU, menu.getMenuId() + "", menu.getNombre());

            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
        } catch (Exception e) {
            log.error("Error al modificar menu: ", e);
        }

        return str;
    }

    public void eliminarConfiguracion(Menu me) {
        log.info("[eliminarMenu]userId=" + userId + "|Iniciando proceso...");
        String idMenu = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId");

        MenuBUSSI menuBUSII = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
        DetalleMenuBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);

        menuBUSII.delete(Integer.parseInt(idMenu));
        List<DetalleMenu> lista = detalleBUSII.obtenerIdCabecera(Integer.parseInt(idMenu));
        for (int i = 0; i < lista.size(); i++) {
            DetalleMenu detalle = lista.get(i);
            detalleBUSII.delete(detalle.getMenuDetalleId());
        }
        log.info("[eliminarMenu]userId=" + userId + "|Elimino correctamente|MenuId=" + me.getMenuId());
        String str = ParameterMessage.Menu_MESSAGE_ELIMINAR + idMenu;
        ControlerBitacora.delete(DescriptorBitacoraLC.MENU, me.getMenuId() + "", me.getNombre() + "");
        cargarConfigCabecera();
        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", str));
        messages = new CustomMessage(CustomMessage.INFO, str);
        clearMenu();

        //-------------------------------------------------------------------------------------------------
        ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
        bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
        ReloadServicios();
        //-------------------------------------------------------------------------------------------------
        savePosicionesMenu();
    }

    public String savePosicionesMenu() {
        messages = null;
        MenuBUSSI menuBUSII = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
        try {
            int nivel1 = 0;
            int nivel2 = 0;

            for (Menu item : listaMenu) {
                if (item.getNivel() == 1) {
                    nivel1++;
                    item.setPosicion(nivel1);
                } else {
                    nivel2++;
                    item.setPosicion(nivel2);
                }
                menuBUSII.updatePos(item);
            }
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Posiciones actualizados"));
            messages = new CustomMessage(CustomMessage.INFO, "Posiciones actualizados");
        } catch (Exception e) {
            log.error("[saveCabecera]userId=" + userId + "|Error al guardar: ", e);
        }
        return "/view/configMenu.xhtml?faces-redirect=true";
    }

    public String saveConfiguracion() {
        String str = "";
        String html = "/view/configMenu.xhtml?faces-redirect=true";
        messages = null;
        try {
            menu.setVisible(visible ? "t" : "f");
            menu.setMostarMayorCero(mostrarMayorQueCero ? "t" : "f");
            menu.setMostarMayorCeroAcumulado(mostrarMayorQueCeroAcumulado ? "t" : "f");
            menu.setTieneHijos(tieneHijos ? "t" : "f");
            menu.setTieneBilletera(tieneBillletera ? "t" : "f");
            menu.setConfiguracionId(configuracion.getConfigId());
            menu.setCanal(selCanal);
            menu.setNivel(Integer.parseInt(selNivel));
            menu.setInformacion(selInformacion);
            menu.setEstado("t");
            menu.setUnitTypeId(selectedUnitType);

            if (selPadre != null && Integer.parseInt(selPadre) > 0) {
                menu.setPadre(Integer.parseInt(selPadre));
            } else {
                menu.setPadre(null);
            }

            if (menu.getNombre() == null || menu.getNombre().isEmpty()) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre esta vacio"));
                messages = new CustomMessage(CustomMessage.WARN, "El campo Nombre esta vacio");
                return html;
            }
            if (menu.getNombre() != null && !menu.getNombre().matches("^[0-9A-Za-z_\\s*:]+$")) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre esta vacio"));
                messages = new CustomMessage(CustomMessage.ERROR, "El campo Nombre contiene caracteres Invalidos");
                return html;
            }
            if (menu.getPosicion() == null || menu.getPosicion() <= 0) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Posicion no es valido"));
                messages = new CustomMessage(CustomMessage.WARN, "El campo Posicion no es valido");
                return html;
            }
            if (menu.getNivel() == null || menu.getNivel() <= 0 || menu.getNivel() > 2) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nivel no es valido"));
                messages = new CustomMessage(CustomMessage.WARN, "El campo Nivel no es valido");
                return html;
            }
            if (selCanal == null || selCanal.equals("NINGUNO")) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Seleccione un Canal valido"));
                messages = new CustomMessage(CustomMessage.WARN, "Seleccione un Canal valido");
                return html;
            }
            if (selInformacion.equals(INFO_INFO_DE_RESERVA) && (menu.getDescripcion() == null || menu.getDescripcion().isEmpty())) {
                messages = new CustomMessage(CustomMessage.WARN, "El campo Descripcion esta vacio");
                // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Descripcion esta vacio"));
                return html;
            }
            if (selInformacion.equals(INFO_SELECCIONAR)) {
                messages = new CustomMessage(CustomMessage.WARN, "Seleccionar una Informacion Valida");
                // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Descripcion esta vacio"));
                return html;
            }

            // NIVEL 1: NINGUNO, INFORMACION DE RESERVA, OTROS SALDOS
            // NIVEL 2: SALDO PAQUETIGO, PAQUETIGOS ACUMULADOS
//            if (billeteraDual == null || billeteraDual.getTarget() == null || billeteraDual.getTarget().isEmpty()) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Debe seleccionar como minimo una billetera"));
//                return;
//            }
            if (!edit) {
                Integer valor = Integer.parseInt(CantidadCabecera.getValor());
                if (listaMenu.size() >= valor) {
                    str = ParameterMessage.Conf_Cabecera_MSG_ERROR + valor;
                    messages = new CustomMessage(CustomMessage.WARN, str);
                    //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", str));
                    return html;
                }
                str = save();
            } else {

                str = editar();
            }
            cargarConfigCabecera();
            messages = new CustomMessage(CustomMessage.INFO, str);
            // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", str));
            clearMenu();
        } catch (NumberFormatException e) {
            log.error("[saveCabecera]userId=" + userId + "|Error al guardar: ", e);
        }
        return html;
    }

    public void onChangeComboNivel() {
        try {
            log.debug("selNivel: " + selNivel);
            cargarComboInformacion(Integer.parseInt(selNivel));
        } catch (NumberFormatException e) {
            log.error("[onChangeComboNivel] Error en el evento on change del combo nivel: ", e);
        }
    }

    public void onChangeComboInformacion() {
        try {
            log.debug("selInformacion: " + selInformacion);
            tieneBillletera = false;
            billeteraDual.getTarget().clear();
            if (selNivel.equals("1")) {
                selectedUnitType = UNITTYPE_NINGUNO;
                disabledUnitType = false;
                disabledPickList = false;
                if (!tieneHijos) {
                    if (selInformacion.equals(INFO_INFO_DE_RESERVA)) {
                        disabledUnitType = true;
                        disabledPickList = true;
                    }
                } else {
                    disabledUnitType = true;
                    disabledPickList = true;
                }
            } else {
                disabledUnitType = false;
                disabledPickList = false;
            }

        } catch (Exception e) {
            log.error("[onChangeComboNivel] Error en el evento on change del combo nivel: ", e);
        }
    }

    public void selectConfiguracion() {
        edit = true;
        messages = null;
        MenuBUSSI menuBUSII = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
        BilleteraBUSSI billeteraBUSII = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
        DetalleMenuBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);

        String idMenu = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId");
        Menu cabeAux = menuBUSII.obtenerId(Integer.parseInt(idMenu));
        menu = cabeAux;
        selCanal = menu.getCanal();
        selNivel = menu.getNivel() + "";

        selInformacion = menu.getInformacion();
        tieneHijos = (menu.getTieneHijos() != null && menu.getTieneHijos().equals("t"));
        tieneBillletera = (menu.getTieneBilletera() != null && menu.getTieneBilletera().equals("t"));
        selectedUnitType = menu.getUnitTypeId();
        if (menu.getPadre() != null && menu.getPadre() > 0) {
            selPadre = menu.getPadre() + "";
        } else {
            selPadre = "-1";
        }
        if (menu.getVisible().equals("t")) {
            visible = true;
        } else {
            visible = false;
        }

        if (menu.getMostarMayorCero().equals("t")) {
            mostrarMayorQueCero = true;
        } else {
            mostrarMayorQueCero = false;
        }

        if (selNivel.equals("1")) {
            disabledUnitType = false;
            disabledPickList = false;
            if (!tieneHijos) {
                if (selInformacion.equals(INFO_INFO_DE_RESERVA)) {
                    disabledUnitType = true;
                    disabledPickList = true;
                }
            }
        } else {
            disabledUnitType = false;
            disabledPickList = false;
        }

        mostrarMayorQueCeroAcumulado = (menu.getMostarMayorCeroAcumulado() != null && menu.getMostarMayorCeroAcumulado().equals("t"));
        List<Billetera> listaSeleccion = billeteraBUSII.obtenerListaByIdMenu(Integer.parseInt(idMenu));
        List<Billetera> listaTodas = new ArrayList<>();

        if (UNITTYPE_TODAS > 0 && UNITTYPE_TODAS.equals(selectedUnitType)) {
            listaTodas = billeteraBUSII.obtenerLista();
        } else if (selectedUnitType != null && selectedUnitType > 0) {
            listaTodas = billeteraBUSII.obtenerListaByIdConfigMenu(selectedUnitType, 0);
        }
        cargarComboInformacion(menu.getNivel());

        List<Billetera> result = limpiarElementos(listaSeleccion, listaTodas);

        billeteraDual = new DualListModel<>();
        billeteraDual.setSource(result);
        billeteraDual.setTarget(listaSeleccion);

        listaDetalle = new ArrayList<>();
        listaDetalle = detalleBUSII.obtenerIdCabecera(menu.getMenuId());

    }

    /**
     * @param seleccionados elementos seleccionados
     * @param lista source lista completa de elementos
     * @return resultado de la lista source menos las lista seleccionada.
     */
    private List<Billetera> limpiarElementos(List<Billetera> seleccionados, List<Billetera> lista) {
        for (int i = 0; i < seleccionados.size(); i++) {
            Billetera select = seleccionados.get(i);
            lista = eliminarBilletar(select, lista);
        }
        return lista;
    }

    private List<Billetera> eliminarBilletar(Billetera billetera, List<Billetera> lista) {
        for (int i = 0; i < lista.size(); i++) {
            Billetera get = lista.get(i);
            if (billetera.getBilleteraId().equals(get.getBilleteraId())) {
                lista.remove(i);
            }
        }
        return lista;
    }

    public String mostrarInformacion(String info) {
        switch (info) {
            case INFO_NINGUNO:
                return "Ninguno";
            case INFO_SALDO_PAQUETIGO:
                return "Saldo Paquetigo";
            case INFO_OTROS_SALDOS:
                return "Otros Saldos";
            case INFO_INFO_DE_RESERVA:
                return "Mas Info. de Reserva";
            case INFO_PAQUETIGOS_ACUMULADOS:
                return "Paquetigos Acumulados";
            case INFO_PAQUETIGOS_DOBLE:
                return "Paquetigos Dobles";
            default:
                return "";
        }
    }

    private DetalleMenu exiteBilleteraInDetalle(Billetera billetera, List<DetalleMenu> lista) {
        for (int i = 0; i < lista.size(); i++) {
            DetalleMenu detalle = lista.get(i);
            if (billetera.getBilleteraId().equals(detalle.getBilleteraId())) {
                return detalle;
            }
        }
        return null;
    }

    private boolean exiteInSelected(Integer idBilletera, List<Billetera> lista) {
        for (int i = 0; i < lista.size(); i++) {
            Billetera billetera = lista.get(i);
            if (billetera.getBilleteraId().equals(idBilletera)) {
                return true;
            }
        }
        return false;
    }

    private void guardarDetalle(Integer idCabecera, List<Billetera> lista) {
        DetalleMenuBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
        for (int i = 0; i < lista.size(); i++) {
            Billetera billetera = lista.get(i);
            DetalleMenu detalle = new DetalleMenu();
            detalle.setMenuId(idCabecera);
            detalle.setBilleteraId(billetera.getBilleteraId());
            detalle.setOrder(i + 1);
            detalleBussi.insert(detalle);
        }
    }

    private void guardarDetalle(Integer idCabecera, Billetera billetera, int order) {
        DetalleMenuBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
        DetalleMenu detalle = new DetalleMenu();
        detalle.setMenuId(idCabecera);
        detalle.setBilleteraId(billetera.getBilleteraId());
        detalle.setOrder(order);
        detalleBussi.insert(detalle);
    }

    private void guardarUpdateDetalle(DetalleMenu detalle, int order) {
        DetalleMenuBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
        detalle.setOrder(order);
        detalleBussi.update(detalle);
    }

    public void clearMenu() {
        edit = false;
        menu = new Menu();
        menu.setPosicion(1);
        menu.setNivel(1);
        visible = true;
        mostrarMayorQueCero = false;
        mostrarMayorQueCeroAcumulado = false;
        selCanal = "NINGUNO";
        selInformacion = "NINGUNO";
        selPadre = "-1";
        selectedUnitType = UNITTYPE_NINGUNO;
        tieneHijos = false;
        tieneBillletera = false;
        cargarListaBilleterea();
        cargarComboPadre();
        cargarComboNivel();
        cargarComboInformacion(1);
    }

    public void upPosicion(ActionEvent actionEvent) {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posMenu");
            int posicion = Integer.parseInt(StrPosicion);
            if (posicion > 0) {

                int posicionIntercambiar = posicion - 1;
                Menu menuCurrent = listaMenu.get(posicion);
                Menu menuInter = listaMenu.get(posicionIntercambiar);
                if (menuCurrent.getNivel().equals(menuInter.getNivel())) {
                    int currentPosicion = menuCurrent.getPosicion();
                    menuCurrent.setPosicion(menuInter.getPosicion());
                    menuInter.setPosicion(currentPosicion);
                    listaMenu.set(posicionIntercambiar, menuCurrent);
                    listaMenu.set(posicion, menuInter);
                }
                log.debug("[subirBilletera]Pos Intercambio=" + posicionIntercambiar);
            }
        } catch (NumberFormatException e) {
            log.error("[subirbilletera] error:" + e.getMessage());
        }
    }

    public void downPosicion(ActionEvent actionEvent) {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posMenu");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[bajarBilletera] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion < listaMenu.size() - 1) {
                int posicionIntercambiar = posicion + 1;
                Menu menuCurrent = listaMenu.get(posicion);
                Menu menuInter = listaMenu.get(posicionIntercambiar);
                if (menuCurrent.getNivel().equals(menuInter.getNivel())) {
                    int currentPosicion = menuCurrent.getPosicion();
                    menuCurrent.setPosicion(menuInter.getPosicion());
                    menuInter.setPosicion(currentPosicion);
                    listaMenu.set(posicionIntercambiar, menuCurrent);
                    listaMenu.set(posicion, menuInter);
                }
            }
        } catch (NumberFormatException e) {
            log.error("[bajarBilletera] error:" + e.getMessage());
        }
    }

    public boolean convertStringToBoolean(String estado) {
        return estado.equals("t");
    }

    public void onChangeUnitType() {
        BilleteraBUSSI billeteraBUSII = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
        List<Billetera> billeterasByUnit = new ArrayList<>();
        if (UNITTYPE_TODAS > 0 && UNITTYPE_TODAS.equals(selectedUnitType)) {
            billeterasByUnit = billeteraBUSII.obtenerLista();
        } else if (selectedUnitType > 0) {
            billeterasByUnit = billeteraBUSII.obtenerListaByIdConfigMenu(selectedUnitType, 0);
        }
        List<Billetera> billeterasSelect = new ArrayList<>();
        if (menu.getMenuId() != null && selectedUnitType > 0) {
            billeterasSelect = billeteraBUSII.obtenerListaByIdMenu(menu.getMenuId());
        }
        if (selectedUnitType != null && selectedUnitType.equals(menu.getUnitTypeId()) && billeterasSelect != null && billeterasSelect.size() > 0) {
            billeterasByUnit = limpiarElementos(billeterasSelect, billeterasByUnit);
        } else {
            billeterasSelect = new ArrayList<>();
        }

        billeteraDual = new DualListModel<>();
        billeteraDual.setSource(billeterasByUnit);
        billeteraDual.setTarget(billeterasSelect);
    }

    public void onChangeTieneHijos() {
        tieneBillletera = false;
        if (tieneHijos) {
            selectedUnitType = UNITTYPE_NINGUNO;
            selInformacion = INFO_NINGUNO;
            billeteraDual.getSource().clear();
            billeteraDual.getTarget().clear();
            disabledUnitType = false;
            cargarComboInformacion(new Integer(selNivel));
        }
    }

    public void onChangeTieneBilletera() {
        if (selNivel != null && selNivel.equals("1")) {
            disabledPickList = !(tieneBillletera);
            selectedUnitType = UNITTYPE_NINGUNO;
            selInformacion = INFO_NINGUNO;
            billeteraDual.getSource().clear();
            billeteraDual.getTarget().clear();
            disabledUnitType = !(tieneBillletera);
            cargarComboInformacion(new Integer(selNivel));
        } else {
            disabledUnitType = false;
            disabledPickList = false;

            if (!tieneBillletera) {
                billeteraDual.getSource().clear();
                billeteraDual.getTarget().clear();
                selectedUnitType = UNITTYPE_NINGUNO;
            }
        }
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public List<UnitType> getUnitTypes() {
        return unitTypes;
    }

    public void setUnitTypes(List<UnitType> unitTypes) {
        this.unitTypes = unitTypes;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public DualListModel<Billetera> getBilleteraDual() {
        return billeteraDual;
    }

    public void setBilleteraDual(DualListModel<Billetera> billeteraDual) {
        this.billeteraDual = billeteraDual;
    }

    public Config getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(Config configuracion) {
        this.configuracion = configuracion;
    }

    public Billetera getBilleteraSelect() {
        return billeteraSelect;
    }

    public void setBilleteraSelect(Billetera billeteraSelect) {
        this.billeteraSelect = billeteraSelect;
    }

    public List<Menu> getListaMenu() {
        return listaMenu;
    }

    public void setListaMenu(List<Menu> listaMenu) {
        this.listaMenu = listaMenu;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public Boolean isEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public List<DetalleMenu> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<DetalleMenu> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isMostrarMayorQueCero() {
        return mostrarMayorQueCero;
    }

    public void setMostrarMayorQueCero(boolean mostrarMayorQueCero) {
        this.mostrarMayorQueCero = mostrarMayorQueCero;
    }

    public boolean isMostrarMayorQueCeroAcumulado() {
        return mostrarMayorQueCeroAcumulado;
    }

    public void setMostrarMayorQueCeroAcumulado(boolean mostrarMayorQueCeroAcumulado) {
        this.mostrarMayorQueCeroAcumulado = mostrarMayorQueCeroAcumulado;
    }

    public List<SelectItem> getSelectItemsPadre() {
        return selectItemsPadre;
    }

    public void setSelectItemsPadre(List<SelectItem> selectItemsPadre) {
        this.selectItemsPadre = selectItemsPadre;
    }

    public String getSelPadre() {
        return selPadre;
    }

    public void setSelPadre(String selPadre) {
        this.selPadre = selPadre;
    }

    public String getSelCanal() {
        return selCanal;
    }

    public void setSelCanal(String selCanal) {
        this.selCanal = selCanal;
    }

    public String getSelInformacion() {
        return selInformacion;
    }

    public void setSelInformacion(String selInformacion) {
        this.selInformacion = selInformacion;
    }

    public List<SelectItem> getSelectItemsNivel() {
        return selectItemsNivel;
    }

    public void setSelectItemsNivel(List<SelectItem> selectItemsNivel) {
        this.selectItemsNivel = selectItemsNivel;
    }

    public String getSelNivel() {
        return selNivel;
    }

    public void setSelNivel(String selNivel) {
        this.selNivel = selNivel;
    }

    public List<SelectItem> getSelectItemsInformacion() {
        return selectItemsInformacion;
    }

    public void setSelectItemsInformacion(List<SelectItem> selectItemsInformacion) {
        this.selectItemsInformacion = selectItemsInformacion;
    }

    public boolean isDisabledPickList() {
        return disabledPickList;
    }

    public void setDisabledPickList(boolean disabledPickList) {
        this.disabledPickList = disabledPickList;
    }

    public boolean isTieneHijos() {
        return tieneHijos;
    }

    public void setTieneHijos(boolean tieneHijos) {
        this.tieneHijos = tieneHijos;
    }

    public void setSelectedUnitType(Integer selectedUnitType) {
        this.selectedUnitType = selectedUnitType;
    }

    public Integer getSelectedUnitType() {
        return selectedUnitType;
    }

    public void setDisabledUnitType(boolean disabledUnitType) {
        this.disabledUnitType = disabledUnitType;
    }

    public boolean isDisabledUnitType() {
        return disabledUnitType;
    }

    public boolean isTieneBillletera() {
        return tieneBillletera;
    }

    public void setTieneBillletera(boolean tieneBillletera) {
        this.tieneBillletera = tieneBillletera;
    }

    public CustomMessage getMessages() {
        return messages;
    }

    public void setMessages(CustomMessage messages) {
        this.messages = messages;
    }

    public Integer getUNITTYPE_TODAS() {
        return UNITTYPE_TODAS;
    }

    public void setUNITTYPE_TODAS(Integer UNITTYPE_TODAS) {
        this.UNITTYPE_TODAS = UNITTYPE_TODAS;
    }

}
