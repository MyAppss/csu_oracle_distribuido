package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.configurable.bl.CampaignSorterBL;
import micrium.configurable.bl.ConfigurationBL;
import micrium.configurable.bl.SorterBL;
import micrium.configurable.util.Mensajes;
import micrium.configurable.vo.Sorter;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.Validador;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Vehimar
 */
@ManagedBean
@ViewScoped
public class SorterForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Sorter sorter;
    private String sorterId;
    private Boolean edit;
    private String message = "";
    private List<Sorter> sortersList;
    private String hideSorderId;
    private static final Logger log = Logger.getLogger(SorterForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";

    public SorterForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
    }

    @PostConstruct
    public void init() {
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sorterId");
            edit = str != null ? true : false;

            SorterBL sorterBL = GuiceInjectorSingleton.getInstance(SorterBL.class);
            sortersList = sorterBL.getSorters();
            sorter = new Sorter();
        } catch (Exception e) {
            log.error("[init] Error: " + e.getMessage());
        }
    }

    public String saveSorter() {
        if (!sorter.getName().equals("")) {
            if (!Validador.validateLength(sorter.getName(), 0, 50)) {
                message = "El campo Nombre es demasiado largo";
                return "";
            }
        }
        if (!sorter.getNameQuestion().equals("")) {
            if (!Validador.validateLength(sorter.getNameQuestion(), 0, 50)) {
                message = "El campo Nombre de Consulta es demasiado largo";
                return "";
            }
        }

        String str = "";

        str = sorter.getName();
        str = str.trim();
        sorter.setName(str);

        if (sorter.getName().equals("")) {
            message = Mensajes.VACIO_NOMBRE;
            return "";
        }

        if (Character.isDigit(str.charAt(0))) {
            message = "El campo NOMBRE no puede comenzar con un Digito";
            return "";
        }

        int pos = getValidateName(str);
        if (pos != -1) {
            message = "El campo NOMBRE existe una letra no permitida en la posición:" + pos;
            return "";
        }

        str = sorter.getNameQuestion();
        str = str.trim();
        sorter.setNameQuestion(str);

        if (sorter.getNameQuestion().equals("")) {
            message = "El campo NOMBRE CONSULTA esta vacio";
            return "";
        }

        pos = str.indexOf(' ');
        if (pos != -1) {
            pos++;
            message = "El campo NOMBRE CONSULTA existe un espacio en la posición:" + pos;
            return "";
        }

        str = recoredWord(sorter.getName());
        if (!str.equals("")) {
            message = str;
            return "";
        }

        try {
            SorterBL sorterBL = GuiceInjectorSingleton.getInstance(SorterBL.class);
            str = sorter.getName();
            str = str.toUpperCase();
            sorter.setName(str);
            if (!edit) {  // Insertar
                int k = sorterBL.maxId();
                sorter.setSorterId(k);
                sorterBL.insertSorter(sorter);
                actualizarSw();
                sorter.setSorterId(k);
                sortersList = sorterBL.getSorters();
                //setBitacora(MensajesBitacora.SORTER_ACC_ADD);
                newSorter();
            } else {
                int id = Integer.parseInt(hideSorderId);
                sorter.setSorterId(id);
                sorter.setStatus(1);
                sorterBL.updateSorter(sorter);
                sortersList = sorterBL.getSorters();
                //setBitacora(MensajesBitacora.SORTER_ACC_UPDATE);
                newSorter();
            }
        } catch (Exception ex) {
            log.error("[saveSorter] Error: " + ex.getMessage());
        }
        return "";
    }

    public void newSorter() {
        sorter.setSorterId(0);
        sorter.setName("");
        sorter.setNameQuestion("");
        sorterId = "";
        edit = false;
        //return "";
    }

    public String editSorter() {
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sorterId");
            int id = Integer.parseInt(str);
            SorterBL datoBL = GuiceInjectorSingleton.getInstance(SorterBL.class);
            sorter = datoBL.getSorter(id);
            sorterId = sorter.getSorterId() + "";
            hideSorderId = sorterId;
            edit = true;
        } catch (Exception ex) {
            log.error("[editSorter] Error: " + ex.getMessage());
        }
        return "";
    }

    public String deleteSorter() {
        try {
            String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sorterId");
            int id = Integer.parseInt(str);

            CampaignSorterBL datoBL = GuiceInjectorSingleton.getInstance(CampaignSorterBL.class);
            List lista = datoBL.getCampaignsSortersSorter(id);
            if (lista.isEmpty()) {
                SorterBL datoBLAux = GuiceInjectorSingleton.getInstance(SorterBL.class);
                datoBLAux.deleteSorter(id);
                actualizarSw();
                sortersList = datoBLAux.getSorters();
                sorter = datoBLAux.getSorter(id);
                // setBitacora(MensajesBitacora.SORTER_ACC_DELET);
                newSorter();
            } else {
                message = Mensajes.SORTER_DEPENDENCIA;
                return "";
            }
        } catch (Exception ex) {
            log.error("[deleteSorter] Error: " + ex.getMessage());
        }
        return "";
    }

    private int getValidateName(String str) {

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            boolean sw = Character.isLetter(ch) || Character.isDigit(ch) || (ch == '_');
            if (!sw) {
                return i + 1;
            }
        }
        return -1;
    }

    private String recoredWord(String palabra) {

        palabra = palabra.toUpperCase();
        try {
            SorterBL datoBL = GuiceInjectorSingleton.getInstance(SorterBL.class);
            List<Sorter> lista = datoBL.getSorters();

            if (!edit) {
                for (Sorter sor : lista) {
                    String str = sor.getName().toUpperCase();
                    if (str.equals(palabra)) {
                        return Mensajes.SORTER_IN_LISTA;
                    }
                }
            } else {
                int id = Integer.parseInt(hideSorderId);
                for (Sorter sor : lista) {
                    if (sor.getSorterId() != id) {
                        String str = sor.getName().toUpperCase();
                        if (str.equals(palabra)) {
                            return Mensajes.SORTER_IN_LISTA;
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("[validar] Error: " + e.getMessage());
        }
        return "";
    }

    private void actualizarSw() {

        try {
            ConfigurationBL datoBL = GuiceInjectorSingleton.getInstance(ConfigurationBL.class);
            int k = datoBL.isEnable();
            if (k == 0) {
                datoBL.enable();
            }

        } catch (Exception ex) {
            System.out.println("Guardar Camapaña:" + ex.getMessage());
        }
    }

    /**
     * @return the Sorter
     */
    public Sorter getSorter() {
        return sorter;
    }

    /**
     * @param sorter as Sorter
     */
    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * @return the SorterId
     */
    public String getSorterId() {
        return sorterId;
    }

    /**
     * @param sorterId as String
     */
    public void setSorterId(String sorterId) {
        this.sorterId = sorterId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param mensaje as String
     */
    public void setMessage(String mensaje) {
        this.message = mensaje;
    }

    /**
     * @return the SortersList
     */
    public List<Sorter> getSortersList() {
        return sortersList;
    }

    /**
     * @param sortersList as List
     */
    public void setSortersList(List<Sorter> sortersList) {
        this.sortersList = sortersList;
    }

    /**
     * @return the hideSorderId
     */
    public String getHideSorderId() {
        return hideSorderId;
    }

    /**
     * @param hideSorderId the hideSorderId to set
     */
    public void setHideSorderId(String hideSorderId) {
        this.hideSorderId = hideSorderId;
    }
}
