/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.configurable.view;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import micrium.csu.bussines.BilleteraBUSSI;
import micrium.csu.bussines.CabeceraBUSSI;
import micrium.csu.bussines.ConfigBUSSI;
import micrium.csu.bussines.DetalleCabeceraBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.bussines.UnitTypeBUSSI;
import micrium.csu.model.Billetera;
import micrium.csu.model.Cabecera;
import micrium.csu.model.Config;
import micrium.csu.model.DetalleCabecera;
import micrium.csu.model.Parametro;
import micrium.csu.model.UnitType;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.CustomMessage;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import micrium.csu.view.ControlerBitacora;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Vehimar
 */
@ManagedBean
@SessionScoped
public class ConfigCabecera implements Serializable {

    private DualListModel<Billetera> billeteraDual;
    private List<UnitType> unitTypes;
    private Billetera billeteraSelect;

    private static final long serialVersionUID = 1L;
    private static final String ARCHIVO_LOG4J = "log.properties";
    private static final Logger log = Logger.getLogger(ConfigCabecera.class);
    private String rowMaxTable = Parameters.NroFilasDataTable;

    private String userId;
    private Boolean edit;
    private Parametro CantidadCabecera;

    private Config configuracion;
    private Cabecera cabecera;
    private List<Cabecera> listaCabeceras;
    private List<DetalleCabecera> listaDetalle;

    private List<Parametro> listParametros;
    private int default_cantidad_cabeceras = 0;
    private boolean visible;
    private boolean saldoCalculado;
    private boolean whatsappIlimitado;
    private String selTipoNavegacion;
    private Integer selectedUnitType;
    private Integer unitTypeTodas = 0;
    private CustomMessage messages;
    public static final String UNITTYPE_TODAS_NOMBRE = "Todas";

    @PostConstruct
    public void init() {
        try {
            URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
            PropertyConfigurator.configure(url);
            userId = ControlerBitacora.getNombreUser();
            this.listaCabeceras = new ArrayList<>();
            messages = null;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public void iniciarConfiguracion() {
        try {
            visible = true;
            saldoCalculado = false;
            whatsappIlimitado = false;
            selTipoNavegacion = "TODOS";
            messages = null;
            ParametroBUSSI parBuss = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            UnitTypeBUSSI unitTypeBUSSI = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            unitTypes = unitTypeBUSSI.obtenerLista();
            for (UnitType type : unitTypes) {
                if (type.getNombre().equals(UNITTYPE_TODAS_NOMBRE)) {
                    unitTypeTodas = type.getUnitTypeId();
                    break;
                }
            }
            listParametros = parBuss.obtenerLista();

            Map<String, String> MapParam = new HashMap();
            for (Parametro parametro : listParametros) {
                MapParam.put(parametro.getNombre(), parametro.getValor());
            }

            default_cantidad_cabeceras = Integer.parseInt(MapParam.get("default_cantidad_cabeceras"));

            String IdConfig = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");
            configuracion = obtenerConfiguracion(Integer.valueOf(IdConfig));
            if (configuracion != null) {
                cabecera = new Cabecera();
                cabecera.setPosicion(1);
                selectedUnitType = unitTypes.get(0).getUnitTypeId();
                cargarListaBilleterea();
                edit = false;
                cargarConfigCabecera();
                obtenerCantidadCabecera();
            }
        } catch (NumberFormatException e) {
            log.error("[iniciarNuevo]:" + e);
        }
    }

    private void obtenerCantidadCabecera() {
        ParametroBUSSI parametroBUSI = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
        CantidadCabecera = parametroBUSI.obtenerName("default_cantidad_cabeceras");
    }

    private Config obtenerConfiguracion(Integer idConfi) {
        Config confi;
        ConfigBUSSI configBussi = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
        confi = configBussi.obtenerId(idConfi);
        if (confi != null) {
            return confi;
        }
        return null;
    }

    private void cargarListaBilleterea() {
        try {
            BilleteraBUSSI billeteraBussi = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
            List<Billetera> list = billeteraBussi.obtenerListaByIdConfigCabecera(selectedUnitType, configuracion.getConfigId());
            List<Billetera> listaSources = new ArrayList<>();
            List<Billetera> listaTarget = new ArrayList<>();

            if (list != null) {
                for (Billetera billetera : list) {
                    listaSources.add(billetera);
                }
            }
            billeteraDual = new DualListModel<>(listaSources, listaTarget);

        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Origen:" + e.getMessage(), e);
        }
    }

    private void cargarConfigCabecera() {
        try {
            listaCabeceras.clear();
            CabeceraBUSSI cabeceraBUSII = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
            DetalleCabeceraBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
            List<Cabecera> lista = cabeceraBUSII.obtenerByIdConfig(configuracion.getConfigId());

            if (lista != null) {
                for (int i = 0; i < lista.size(); i++) {
                    Cabecera cabe = lista.get(i);
                    List<DetalleCabecera> list = detalleBUSII.obtenerIdCabecera(cabe.getCabeceraId());
                    StringBuilder stb = new StringBuilder();
                    for (int j = 0; j < list.size(); j++) {
                        DetalleCabecera detalle = list.get(j);
                        if (stb.length() > 0) {
                            stb.append("<br>").append(detalle.getNombreComverseBilletera());
                        } else {
                            stb.append(detalle.getNombreComverseBilletera());
                        }
                    }
                    cabe.setStrBilleteras(stb.toString());
                    listaCabeceras.add(cabe);
                }
                if (!lista.isEmpty()) {
                    log.info("[saveConfiCabecera] Se cargaron " + lista.size() + " elementos");
                }
            }
            // return cantidad;
        } catch (Exception e) {
            log.info("[saveConfiCabecera]cargarConfigCabecera=" + e.getMessage());
        }
    }

    private String save() {
        log.info("[saveConfiCabecera]userId=" + userId + "|Iniciando proceso...");
        String str = "";
        CabeceraBUSSI cabeceraBussi = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
        Integer idCabe = 0;
        cabecera.setConfiguracionId(configuracion.getConfigId());
        cabecera.setUnitTypeId(selectedUnitType);
        cabecera.setEstado("t");
        cabecera.setPosicion(this.listaCabeceras.size() + 1);
        idCabe = cabeceraBussi.insert(cabecera);
        if (idCabe > 0) {
            List<Billetera> lista = billeteraDual.getTarget();
            guardarDetalle(idCabe, lista);
            log.info("[saveConfiCabecera]userId=" + userId + "|Inserto correctamente|Config Cabecera=" + cabecera.getCabeceraId());
            str = ParameterMessage.Conf_Cabecera_MSG_GUARDAR + cabecera.getCabeceraId();
            ControlerBitacora.insert(DescriptorBitacoraLC.CABECERA, cabecera.getCabeceraId() + "", cabecera.getNombre());
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
        }
        return str;
    }

    private String editar() {
        log.info("[editarConfiCabecera]userId=" + userId + "|Iniciando proceso...");
        String str = "";
        try {
            CabeceraBUSSI cabeceraBussi = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
            DetalleCabeceraBUSSI detalleBUSSI = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
            cabecera.setUnitTypeId(selectedUnitType);
            cabeceraBussi.update(cabecera);

            List<Billetera> listaSelect = billeteraDual.getTarget();
            for (int i = 0; i < listaSelect.size(); i++) {
                Billetera billetera = listaSelect.get(i);
                DetalleCabecera detalle = exiteBilleteraInDetalle(billetera, listaDetalle);
                if (detalle == null) {
                    guardarDetalle(cabecera.getCabeceraId(), billetera, i + 1);
                } else {
                    guardarUpdateDetalle(detalle, i + 1);
                }
            }

            for (int i = 0; i < listaDetalle.size(); i++) {
                DetalleCabecera detalle = listaDetalle.get(i);
                boolean existe = exiteInSelected(detalle.getBilleteraId(), listaSelect);
                if (!existe) {
                    detalleBUSSI.delete(detalle.getCabeceraDetalleId());
                }
            }
            log.info("[editarConfiCabecera]userId=" + userId + "|Inserto correctamente|Config Cabecera=" + cabecera.getCabeceraId());
            str = ParameterMessage.Conf_Cabecera_MSG_MODIFICAR + cabecera.getCabeceraId();
            ControlerBitacora.update(DescriptorBitacoraLC.CABECERA, cabecera.getCabeceraId() + "", cabecera.getNombre());

            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return str;
    }

    public void eliminarConfiguracion(Cabecera cb) {
        log.info("[eliminarConfiCabecera]userId=" + userId + "|Iniciando proceso...");
        String idCabecera = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cabeceraId");
//        String idCabecera = cb.getCabeceraId().intValue() + "";

        CabeceraBUSSI cabeceraBUSSI = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
        DetalleCabeceraBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);

        cabeceraBUSSI.delete(Integer.parseInt(idCabecera));
        List<DetalleCabecera> lista = detalleBUSII.obtenerIdCabecera(Integer.parseInt(idCabecera));
        for (int i = 0; i < lista.size(); i++) {
            DetalleCabecera detalle = lista.get(i);
            detalleBUSII.delete(detalle.getCabeceraDetalleId());
        }
        log.info("[eliminarConfiCabecera]userId=" + userId + "|Inserto correctamente|Config Cabecera=" + cabecera.getCabeceraId());
        String str = ParameterMessage.Conf_Cabecera_MSG_ELIMINAR + idCabecera;
//        ControlerBitacora.update(DescriptorBitacoraLC.MENU, idCabecera + "", "");
        ControlerBitacora.delete(DescriptorBitacoraLC.CABECERA, cb.getCabeceraId() + "", cb.getNombre());

        //-------------------------------------------------------------------------------------------------
        ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
        bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
        ReloadServicios();
        //-------------------------------------------------------------------------------------------------

        cargarConfigCabecera();
        // FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", str));
        messages = new CustomMessage(CustomMessage.INFO, str);
        clearCabecera();
        savePosicionesCabecera();
    }

    public String savePosicionesCabecera() {
        CabeceraBUSSI cabeceraBUSSI = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
        try {
            for (int i = 0; i < this.listaCabeceras.size(); i++) {
                Cabecera item = this.listaCabeceras.get(i);
                item.setPosicion(i + 1);
                cabeceraBUSSI.updatePos(item);
            }
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Posiciones actualizados"));
            messages = new CustomMessage(CustomMessage.INFO, "Posiciones actualizados");
        } catch (Exception e) {
            log.error("[saveCabecera]userId=" + userId + "|Error al guardar: ", e);
        }
        return "/view/configCabecera.xhtml?faces-redirect=true";
    }

    public String saveConfiguracion() {
        messages = null;
        String html = "/view/configCabecera.xhtml?faces-redirect=true";
        try {
            cabecera.setVisible(visible ? "t" : "f");
            cabecera.setSaldoCalculado(saldoCalculado ? "t" : "f");
            cabecera.setWhatsappIlimitado(whatsappIlimitado ? "t" : "f");
            cabecera.setTipoNavegacion(selTipoNavegacion);

            if (cabecera.getNombre() == null || cabecera.getNombre().isEmpty()) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre esta vacio"));
                messages = new CustomMessage(CustomMessage.WARN, "El campo Nombre esta vacio");
                return html;
            }
            if (cabecera.getNombre() != null && !cabecera.getNombre().matches("^[0-9A-Za-z_\\s*:]+$")) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre esta vacio"));
                messages = new CustomMessage(CustomMessage.ERROR, "El campo Nombre contiene caracteres Invalidos");
                return html;
            }
            if (selTipoNavegacion == null || selTipoNavegacion.equals("NINGUNO")) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Seleccione un Tipo de Navegacion valido"));
                messages = new CustomMessage(CustomMessage.WARN, "Seleccione un Tipo de Navegacion valido");
                return html;
            }
            if (selectedUnitType == null) {
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Seleccione Unit Type"));
                messages = new CustomMessage(CustomMessage.WARN, "Seleccione Unit Type");
                return html;
            }
            // if (listaCabeceras != null) {
            for (Cabecera cb : listaCabeceras) {
                if (edit) { // modificar
                    if (cb.getCabeceraId().intValue() != cabecera.getCabeceraId().intValue()) {
                        if (cb.getNombre().toUpperCase().trim().equals(cabecera.getNombre().toUpperCase().trim())) {
                            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre ya existe"));
                            messages = new CustomMessage(CustomMessage.WARN, "El campo Nombre ya existe");
                            return html;
                        }
                    }
                } else { // guardar
                    if (cb.getNombre().toUpperCase().trim().equals(cabecera.getNombre().toUpperCase().trim())) {
                        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "El campo Nombre ya existe"));
                        messages = new CustomMessage(CustomMessage.WARN, "El campo Nombre ya existe");
                        return html;
                    }
                }
            }
            // }

            if (!edit) {
                Integer valor = Integer.parseInt(CantidadCabecera.getValor());
                if (listaCabeceras.size() >= default_cantidad_cabeceras) {
                    String str = ParameterMessage.Conf_Cabecera_MSG_ERROR + valor;
                    messages = new CustomMessage(CustomMessage.WARN, str);
                    return html;
                }
                String str = save();
                messages = new CustomMessage(CustomMessage.INFO, str);
            } else {
                String str = editar();
                messages = new CustomMessage(CustomMessage.INFO, str);
            }

            cargarConfigCabecera();
            clearCabecera();
        } catch (NumberFormatException e) {
            log.error("[saveCabecera]userId=" + userId + "|Error al guardar. Error:", e);
        }
        return html;
    }

    public void selectConfiguracion() {
        edit = true;
        messages = null;
        CabeceraBUSSI cabeceraBussi = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
        BilleteraBUSSI billeteraBUSII = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
        DetalleCabeceraBUSSI detalleBUSII = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);

        String idCabecera = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cabeceraId");
        Cabecera cabeAux = cabeceraBussi.obtenerId(Integer.parseInt(idCabecera));
        cabecera = cabeAux;
        selTipoNavegacion = (cabecera.getTipoNavegacion() != null) ? cabecera.getTipoNavegacion() : selTipoNavegacion;
        if (cabecera.getVisible().equals("t")) {
            visible = true;
        } else {
            visible = false;
        }
        if (cabecera.getSaldoCalculado().equals("t")) {
            saldoCalculado = true;
        } else {
            saldoCalculado = false;
        }

        if (cabecera.getWhatsappIlimitado().equals("t")) {
            whatsappIlimitado = true;
        } else {
            whatsappIlimitado = false;
        }

        selectedUnitType = cabecera.getUnitTypeId();
        List<Billetera> listaTodas = null;
        if (unitTypeTodas > 0 && unitTypeTodas.equals(selectedUnitType)) {
            listaTodas = billeteraBUSII.obtenerListaByIdConfigCabecera(0, cabecera.getConfiguracionId());
        } else {
            listaTodas = billeteraBUSII.obtenerListaByIdConfigCabecera(cabecera.getUnitTypeId(), cabecera.getConfiguracionId());
        }

        List<Billetera> listaSeleccion = billeteraBUSII.obtenerListaByIdCabecera(Integer.parseInt(idCabecera));
        List<Billetera> result = limpiarElementos(listaSeleccion, listaTodas);
        billeteraDual = new DualListModel<>();
        billeteraDual.setSource(result);
        billeteraDual.setTarget(listaSeleccion);

        listaDetalle = new ArrayList<>();
        listaDetalle = detalleBUSII.obtenerIdCabecera(cabecera.getCabeceraId());

    }

    private List<Billetera> limpiarElementos(List<Billetera> seleccionados, List<Billetera> lista) {
        for (int i = 0; i < seleccionados.size(); i++) {
            Billetera select = seleccionados.get(i);
            lista = eliminarBilletar(select, lista);
        }
        return lista;
    }

    private List<Billetera> eliminarBilletar(Billetera billetera, List<Billetera> lista) {
        for (int i = 0; i < lista.size(); i++) {
            Billetera get = lista.get(i);
            if (billetera.getBilleteraId().equals(get.getBilleteraId())) {
                lista.remove(i);
            }
        }
        return lista;
    }

    private DetalleCabecera exiteBilleteraInDetalle(Billetera billetera, List<DetalleCabecera> lista) {
        for (int i = 0; i < lista.size(); i++) {
            DetalleCabecera detalle = lista.get(i);
            if (billetera.getBilleteraId().equals(detalle.getBilleteraId())) {
                return detalle;
            }
        }
        return null;
    }

    private boolean exiteInSelected(Integer idBilletera, List<Billetera> lista) {
        for (int i = 0; i < lista.size(); i++) {
            Billetera billetera = lista.get(i);
            if (billetera.getBilleteraId().equals(idBilletera)) {
                return true;
            }
        }
        return false;
    }

    private void guardarDetalle(Integer idCabecera, List<Billetera> lista) {
        DetalleCabeceraBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
        for (int i = 0; i < lista.size(); i++) {
            Billetera billetera = lista.get(i);
            DetalleCabecera detalle = new DetalleCabecera();
            detalle.setCabeceraId(idCabecera);
            detalle.setBilleteraId(billetera.getBilleteraId());
            detalle.setOrder(i + 1);
            detalleBussi.insert(detalle);
        }
    }

    private void guardarDetalle(Integer idCabecera, Billetera billetera, int order) {
        DetalleCabeceraBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
        DetalleCabecera detalle = new DetalleCabecera();
        detalle.setCabeceraId(idCabecera);
        detalle.setBilleteraId(billetera.getBilleteraId());
        detalle.setOrder(order);
        detalleBussi.insert(detalle);
    }

    private void guardarUpdateDetalle(DetalleCabecera detalle, int order) {
        DetalleCabeceraBUSSI detalleBussi = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
        detalle.setOrder(order);
        detalleBussi.update(detalle);
    }

    public void clearCabecera() {
        edit = false;
        selTipoNavegacion = "TODOS";
        visible = true;
        saldoCalculado = false;
        whatsappIlimitado = false;
        cabecera = new Cabecera();
        cabecera.setPosicion(1);
        cargarListaBilleterea();
    }

    public boolean convertStringToBoolean(String estado) {
        return estado.equals("t");
    }

    public void upPosicion(ActionEvent actionEvent) {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posCabecera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[subirBilletera] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion > 0) {
                int posicionIntercambiar = posicion - 1;
                Cabecera cabeceraCurrent = this.listaCabeceras.get(posicion);
                int currentPosicion = cabeceraCurrent.getPosicion();
                Cabecera cabeceraInter = this.listaCabeceras.get(posicionIntercambiar);
                cabeceraCurrent.setPosicion(cabeceraInter.getPosicion());
                cabeceraInter.setPosicion(currentPosicion);
                this.listaCabeceras.set(posicionIntercambiar, cabeceraCurrent);
                this.listaCabeceras.set(posicion, cabeceraInter);
                log.debug("[subirBilletera]Pos Intercambio=" + posicionIntercambiar);
            }
        } catch (NumberFormatException e) {
            log.error("[subirbilletera] error:" + e.getMessage());
        }
    }

    public void downPosicion(ActionEvent actionEvent) {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posCabecera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[bajarBilletera] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion < this.listaCabeceras.size() - 1) {
                int posicionIntercambiar = posicion + 1;
                Cabecera cabeceraCurrent = this.listaCabeceras.get(posicion);
                int currentPosicion = cabeceraCurrent.getPosicion();
                Cabecera cabeceraInter = this.listaCabeceras.get(posicionIntercambiar);
                cabeceraCurrent.setPosicion(cabeceraInter.getPosicion());
                cabeceraInter.setPosicion(currentPosicion);
                this.listaCabeceras.set(posicionIntercambiar, cabeceraCurrent);
                this.listaCabeceras.set(posicion, cabeceraInter);
            }
        } catch (NumberFormatException e) {
            log.error("[bajarBilletera] error:" + e.getMessage());
        }
    }

    public void onChangeUnitType() {
        BilleteraBUSSI billeteraBUSII = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
        List<Billetera> billeterasByUnit = new ArrayList<>();
        if (unitTypeTodas > 0 && unitTypeTodas.equals(selectedUnitType)) {
            billeterasByUnit = billeteraBUSII.obtenerListaByIdConfigCabecera(0, configuracion.getConfigId());
        } else if (selectedUnitType > 0) {
            billeterasByUnit = billeteraBUSII.obtenerListaByIdConfigCabecera(selectedUnitType, configuracion.getConfigId());
        }
        List<Billetera> billeterasSelect = new ArrayList<>();
        if (cabecera.getCabeceraId() != null && selectedUnitType > 0) {
            billeterasSelect = billeteraBUSII.obtenerListaByIdCabecera(cabecera.getCabeceraId());
        }

        //List<Billetera> billeterasTarget = new ArrayList<>();
        if (selectedUnitType != null && selectedUnitType.equals(cabecera.getUnitTypeId()) && billeterasSelect != null && billeterasSelect.size() > 0) {
            billeterasByUnit = limpiarElementos(billeterasSelect, billeterasByUnit);
        } else {
            billeterasSelect = new ArrayList<>();
        }
        billeteraDual = new DualListModel<>();
        billeteraDual.setSource(billeterasByUnit);
        billeteraDual.setTarget(billeterasSelect);
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public DualListModel<Billetera> getBilleteraDual() {
        return billeteraDual;
    }

    public void setBilleteraDual(DualListModel<Billetera> billeteraDual) {
        this.billeteraDual = billeteraDual;
    }

    public Config getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(Config configuracion) {
        this.configuracion = configuracion;
    }

    public Billetera getBilleteraSelect() {
        return billeteraSelect;
    }

    public void setBilleteraSelect(Billetera billeteraSelect) {
        this.billeteraSelect = billeteraSelect;
    }

    public List<Cabecera> getListaCabeceras() {
        return listaCabeceras;
    }

    public void setListaCabeceras(List<Cabecera> listaCabeceras) {
        this.listaCabeceras = listaCabeceras;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getSelTipoNavegacion() {
        return selTipoNavegacion;
    }

    public void setSelTipoNavegacion(String selTipoNavegacion) {
        this.selTipoNavegacion = selTipoNavegacion;
    }

    public boolean isSaldoCalculado() {
        return saldoCalculado;
    }

    public void setSaldoCalculado(boolean saldoCalculado) {
        this.saldoCalculado = saldoCalculado;
    }

    public boolean isWhatsappIlimitado() {
        return whatsappIlimitado;
    }

    public void setWhatsappIlimitado(boolean whatsappIlimitado) {
        this.whatsappIlimitado = whatsappIlimitado;
    }

    public List<UnitType> getUnitTypes() {
        return unitTypes;
    }

    public void setUnitTypes(List<UnitType> unitTypes) {
        this.unitTypes = unitTypes;
    }

    public Integer getSelectedUnitType() {
        return selectedUnitType;
    }

    public void setSelectedUnitType(Integer selectedUnitType) {
        this.selectedUnitType = selectedUnitType;
    }

    public CustomMessage getMessages() {
        return messages;
    }

    public void setMessages(CustomMessage messages) {
        this.messages = messages;
    }

    public Integer getUNITTYPE_TODAS() {
        return unitTypeTodas;
    }

    public void setUNITTYPE_TODAS(Integer UNITTYPE_TODAS) {
        this.unitTypeTodas = UNITTYPE_TODAS;
    }

}
