package micrium.configurable.bl;

import  micrium.configurable.vo.Configuration;
import  java.util.List;
/**
 *
 * @author marciano
 */
public interface ConfigurationBL {
    
    public void updateConfig(Configuration config);
   public void enable();
   public int isEnable();
}
