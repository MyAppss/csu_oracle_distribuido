package micrium.configurable.bl;

import  micrium.configurable.vo.CampaignMessage;
import  java.util.List;
/**
 *
 * @author Pepe
 */
public interface CampaignMessageBL {
    public List<CampaignMessage> getCampaignMessages();
    public void deleteCampaignMessage(CampaignMessage CampaignMessage);
    public void insertCampaignMessage(CampaignMessage CampaignMessage);
    public CampaignMessage       getCampaignMessage(CampaignMessage CampaignMessage);
    public void            updateCampaignMessage(CampaignMessage CampaignMessage);
     public List<CampaignMessage> getCampaignsMessagesCampaign(int campaingId);
    public List<CampaignMessage> getCampaignsMessagesMessage(int messageId);
    public void updateCampaignId(int campaingId);
}