package micrium.configurable.bl;

import micrium.configurable.vo.SorterValue;
import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.SorterValueDAO;

/**
 *
 * @author marciano
 */
public class SorterValueBLImpl implements SorterValueBL {
    @Inject
    private SorterValueDAO dao;
  
    public void deleteSorterValue(SorterValue sorterValue) {
        dao.deleteSorterValue(sorterValue);
    }

    public SorterValue getSorterValue(SorterValue sorterValue) {
        return dao.getSorterValue(sorterValue);
    }

    public List<SorterValue> getSorterValues() {
        return dao.getSorterValues();
    }

 
    public void insertSorterValue(SorterValue sorterValue) {
        dao.insertSorterValue(sorterValue);
    }

 
    public void updateSorterValue(SorterValue sorterValue) {
        dao.updateSorterValue(sorterValue);
    }

    
    public List<SorterValue> getSorterValuesId(int sorterId) {
       return  dao.getSorterValuesId(sorterId);    
    }
    
    
}
