package micrium.configurable.bl;

import  java.util.List;
import  micrium.configurable.vo.SorterValue;
/**
 *
 * @author marciano
 */
public interface SorterValueBL {
   
    public SorterValue getSorterValue(SorterValue sorterValue);
    public void updateSorterValue(SorterValue sorterValue);
    public void insertSorterValue(SorterValue sorterValue);
    public void deleteSorterValue(SorterValue sorterValue);
    public List<SorterValue> getSorterValues();
    public List<SorterValue> getSorterValuesId(int sorterId);
   
}
