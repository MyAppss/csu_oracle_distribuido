package micrium.configurable.bl;

import  micrium.configurable.vo.CampaignWallet;
import  java.util.List;
/**
 *
 * @author Pepe
 */
public interface CampaignWalletBL {
    public List<CampaignWallet> getCampaignWallets();
    public void deleteCampaignWallet(CampaignWallet CampaignWallet);
    public void insertCampaignWallet(CampaignWallet CampaignWallet);
    public CampaignWallet       getCampaignWallet(CampaignWallet CampaignWallet);
    public void            updateCampaignWallet(CampaignWallet CampaignWallet);
    public List<CampaignWallet> getCampaignsWalletsCampaign(int campaingId);
    public List<CampaignWallet> getCampaignsWalletsWallet(int  walletId);
    public void updateCampaignId(int campaingId);
}
