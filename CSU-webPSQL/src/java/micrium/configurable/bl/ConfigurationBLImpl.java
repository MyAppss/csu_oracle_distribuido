package micrium.configurable.bl;

import micrium.configurable.vo.Configuration;
import com.google.inject.Inject;
import micrium.configurable.dao.ConfigurationDAO;

/**
 *
 * @author marciano
 */
public class ConfigurationBLImpl implements ConfigurationBL{
    @Inject
    private ConfigurationDAO dao;
   
   public void updateConfig(Configuration config) {
        dao.updateConfig(config);
    }
    public void enable(){
        dao.enable();
     }
    public int isEnable(){
       return dao.isEnable();
    }
}
