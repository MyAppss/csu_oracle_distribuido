package micrium.configurable.bl;

import java.util.List;
import micrium.configurable.vo.CampaignMessage;
import com.google.inject.Inject;
import micrium.configurable.dao.CampaignMessageDAO;
/**
 *
 * @author marciano
 */
public class CampaignMessageBLImpl implements CampaignMessageBL{
  @Inject
    private CampaignMessageDAO dao;
    
    public void deleteCampaignMessage(CampaignMessage CampaignMessage) {
        dao.deleteCampaignMessage(CampaignMessage);
    }

    
    public List<CampaignMessage> getCampaignMessages() {
        return  dao.getCampaignMessages();
    }
  public List<CampaignMessage> getCampaignsMessagesCampaign(int campaingId){
    return  dao.getCampaignsMessagesCampaign(campaingId);
  }
    public List<CampaignMessage> getCampaignsMessagesMessage(int messageId){
       return  dao.getCampaignsMessagesMessage(messageId);
    }
    
    

    
    public void insertCampaignMessage(CampaignMessage CampaignMessage) {
        dao.insertCampaignMessage(CampaignMessage);
    }


    public CampaignMessage getCampaignMessage(CampaignMessage CampaignMessage) {
        return dao.getCampaignMessage(CampaignMessage);
    }

    public void updateCampaignMessage(CampaignMessage CampaignMessage) {
        dao.updateCampaignMessage(CampaignMessage);
    }

    
    public void updateCampaignId(int campaingId) {
       dao.updateCampaignId(campaingId);
    }
    
    
    
}
