package micrium.configurable.bl;

import  micrium.configurable.vo.DefaulMessage;
/**
 *
 * @author marciano
 */
public interface DefaulMessageBL {
    
    public void updateDefaulMessage(DefaulMessage defaulMessage);
   public void enable();
   public int isEnable();
   public DefaulMessage getDefaulMessage(int messageId);
}
