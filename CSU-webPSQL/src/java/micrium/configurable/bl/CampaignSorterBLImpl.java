package micrium.configurable.bl;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.CampaignSorterDAO;
import micrium.configurable.vo.CampaignSorter;
import micrium.configurable.vo.SorterValue;

/**
 *
 * @author Pepe
 */
public class CampaignSorterBLImpl implements CampaignSorterBL{

    @Inject
    private CampaignSorterDAO dao;
    public void deleteCampaignSorter(CampaignSorter campaignSorter) {
        dao.deleteCampaignSorter(campaignSorter);
    }

    
    public List<CampaignSorter> getCampaignSorters() {
        return dao.getCampaignSorters();
    }

    public List<CampaignSorter> getCampaignsSortersCampaign(int campaingId){
       return dao.getCampaignsSortersCampaign(campaingId);
    }
            
    public List<CampaignSorter> getCampaignsSortersSorter(int Sorterid){
      return  dao.getCampaignsSortersSorter(Sorterid);
    }
    
    public void insertCampaignSorter(CampaignSorter campaignSorter) {
         dao.insertCampaignSorter(campaignSorter);
    }
     public void updateCampaignSorter(CampaignSorter campaignSorter) {
        dao.updateCampaignSorter(campaignSorter);
    }

    public CampaignSorter getCampaignSorter(CampaignSorter campaignSorter) {
        return dao.getCampaignSorter(campaignSorter);
    }
    
    public void updateCampaignID(int campaingId){
       dao.updateCampaignID(campaingId);
    }


    public List<CampaignSorter> getCampaignsSortersSorterValue(SorterValue sorterValue) {
         return dao.getCampaignsSortersSorterValue(sorterValue);
    }
    
    
}
