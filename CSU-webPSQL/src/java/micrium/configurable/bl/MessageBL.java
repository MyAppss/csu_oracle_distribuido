package micrium.configurable.bl;

import  java.util.List;
import  micrium.configurable.vo.Message;
/**
 *
 * @author marciano
 */
public interface MessageBL {
    
    public Message getMessage(int messageId);
    public List<Message> getMessages();
    public void updateMessage(Message message);
    public void insertMessage(Message message);
    public void deleteMessage(int messageId);
    public int maxId();
}
