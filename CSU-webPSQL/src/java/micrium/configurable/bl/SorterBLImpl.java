package micrium.configurable.bl;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.SorterDAO;
import micrium.configurable.vo.Sorter;

/**
 *
 * @author Ivan
 */
public class SorterBLImpl implements SorterBL {
    @Inject
    private SorterDAO dao;


    public Sorter getSorter(int sorterId) {
        return dao.getSorter(sorterId);
    }

    public List<Sorter> getSorters() {
        return dao.getSorters();
    }

    public void updateSorter(Sorter sorter){
        dao.updateSorter(sorter);
    }

    public void insertSorter(Sorter sorter){
        dao.insertSorter(sorter);
    }

    public void deleteSorter(int sorterId){
        dao.deleteSorter(sorterId);
    }
    
    public int maxId(){
       return dao.maxId();
    }
    public List<Integer> listaIds(){
       return  dao.listaIds();
    }
    
}
