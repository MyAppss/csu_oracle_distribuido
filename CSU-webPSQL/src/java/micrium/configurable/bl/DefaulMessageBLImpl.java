package micrium.configurable.bl;

import micrium.configurable.vo.DefaulMessage;
import com.google.inject.Inject;
import micrium.configurable.dao.DefaulMessageDAO;

/**
 *
 * @author marciano
 */
public class DefaulMessageBLImpl implements DefaulMessageBL{
    @Inject
    private DefaulMessageDAO dao;
   
   public void updateDefaulMessage(DefaulMessage defaulMessage) {
        dao.updateDefaulMessage(defaulMessage);
    }
    public void enable(){
        dao.enable();
     }
    public int isEnable(){
       return dao.isEnable();
    }
    public DefaulMessage getDefaulMessage(int messageId){
       return dao.getDefaulMessage(messageId);
    }
}
