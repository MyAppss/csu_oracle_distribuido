package micrium.configurable.bl;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.WalletDAO;
import micrium.configurable.vo.Wallet;

/**
 *
 * @author Ivan
 */
public class WalletBLImpl implements WalletBL {
    @Inject
    private WalletDAO dao;


    @Override
    public Wallet getWallet(int walletId) {
        return dao.getWallet(walletId);
    }

    @Override
    public List<Wallet> getWallets() {
        return dao.getWallets();
    }
    @Override
    public List<Wallet> getWalletsVisible(){
        return dao.getWalletsVisible();
    }
    @Override
    public void updateWallet(Wallet wallet){
        dao.updateWallet(wallet);
    }

    @Override
    public void insertWallet(Wallet wallet){
        dao.insertWallet(wallet);
    }

    @Override
    public void deleteWallet(int walletId){
        dao.deleteWallet(walletId);
    }
    @Override
    public int maxId(){
      return dao.maxId();
    }
    
}
