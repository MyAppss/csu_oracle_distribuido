package micrium.configurable.bl;

import micrium.configurable.vo.Message;
import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.MessageDAO;

/**
 *
 * @author marciano
 */
public class MessageBLImpl implements MessageBL {
    @Inject
    private MessageDAO dao;
  
    public void deleteMessage(int messageId) {
        dao.deleteMessage(messageId);
    }

    public Message getMessage(int messageId) {
        return dao.getMessage(messageId);
    }

    public List<Message> getMessages() {
        return dao.getMessages();
    }

 
    public void insertMessage(Message message) {
        dao.insertMessage(message);
    }

 
    public void updateMessage(Message message) {
        dao.updateMessage(message);
    }
     public int maxId(){
       return dao.maxId();
     }
}
