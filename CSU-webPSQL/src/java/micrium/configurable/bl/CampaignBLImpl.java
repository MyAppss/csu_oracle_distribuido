package micrium.configurable.bl;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.CampaignDAO;
import micrium.configurable.vo.Campaign;


/**
 *
 * @author Pepe
 */
public class CampaignBLImpl implements CampaignBL {
    @Inject
    private CampaignDAO dao;
   
    
    public void deleteCampaign(int campaignId) {
        dao.deleteCampaign(campaignId);
    }

   
    public Campaign getCampaign(int campaignId) {
        return dao.getCampaign(campaignId);
    }

   
    public List<Campaign> getCampaigns() {
        return dao.getCampaigns();
    }

   
    public void insertCampaign(Campaign campaign) {
        dao.insertCampaign(campaign);
    }

   
    public void updateCampaign(Campaign campaign) {
        dao.updateCampaign(campaign);
    }
    public int maxId(){
      return dao.maxId();
    }
}
