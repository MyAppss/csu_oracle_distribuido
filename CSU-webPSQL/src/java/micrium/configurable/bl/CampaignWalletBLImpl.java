package micrium.configurable.bl;

import com.google.inject.Inject;
import java.util.List;
import micrium.configurable.dao.CampaignWalletDAO;
import micrium.configurable.vo.CampaignWallet;

/**
 *
 * @author Pepe
 */
public class CampaignWalletBLImpl implements CampaignWalletBL{

    @Inject
    private CampaignWalletDAO dao;
    public void deleteCampaignWallet(CampaignWallet CampaignWallet) {
        dao.deleteCampaignWallet(CampaignWallet);
    }

    
    public List<CampaignWallet> getCampaignWallets() {
        return dao.getCampaignWallets();
    }

    public List<CampaignWallet> getCampaignsWalletsCampaign(int campaingId){
       return dao.getCampaignsWalletsCampaign(campaingId);
    }
            
    public List<CampaignWallet> getCampaignsWalletsWallet(int  walletId){
      return  dao.getCampaignsWalletsWallet(walletId);
    }
    
    public void insertCampaignWallet(CampaignWallet CampaignWallet) {
         dao.insertCampaignWallet(CampaignWallet);
    }
     public void updateCampaignWallet(CampaignWallet CampaignWallet) {
        dao.updateCampaignWallet(CampaignWallet);
    }

    public CampaignWallet getCampaignWallet(CampaignWallet CampaignWallet) {
        return dao.getCampaignWallet(CampaignWallet);
    }
    
    
    public void updateCampaignId(int campaingId) {
       dao.updateCampaignId(campaingId);
    }
}
