package micrium.configurable.bl;

import java.util.List;
import micrium.configurable.vo.Campaign;

/**
 *
 * @author Pepe
 */
public interface CampaignBL {
    public Campaign getCampaign(int campaignId);
    public List<Campaign> getCampaigns();
    public void updateCampaign(Campaign campaign);
    public void insertCampaign(Campaign campaign);
    public void deleteCampaign(int campaignId);
    public int maxId();
}
