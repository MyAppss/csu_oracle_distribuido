package micrium.configurable.bl;

import  micrium.configurable.vo.CampaignSorter;
import  java.util.List;
import micrium.configurable.vo.SorterValue;
/**
 *
 * @author Pepe
 */
public interface CampaignSorterBL {
    public List<CampaignSorter> getCampaignSorters();
    public void deleteCampaignSorter(CampaignSorter campaignSorter);
    public void insertCampaignSorter(CampaignSorter campaignSorter);
    public CampaignSorter       getCampaignSorter(CampaignSorter campaignSorter);
    public void            updateCampaignSorter(CampaignSorter campaignSorter);
    public List<CampaignSorter> getCampaignsSortersCampaign(int campaingId);
    public List<CampaignSorter> getCampaignsSortersSorter(int Sorterid);
    //getCampaignsSortersSorterValue
    public List<CampaignSorter> getCampaignsSortersSorterValue(SorterValue sorterValue);
    public void updateCampaignID(int campaingId);
}
