package micrium.context;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

@WebListener
public class InitContextListener implements ServletContextListener {

    public static final Logger LOG = Logger.getLogger(InitContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent ce) {

        LOG.info("...::: WEB INICIO CORRECTAMNETE :::...");
     
    }

    @Override
    public void contextDestroyed(ServletContextEvent ce) {
           LOG.info("************** Shutting down! **************");
              LOG.info("Destroying Context...");
               LOG.info("Calling ORACLE AbandonedConnectionCleanupThread checkedShutdown");

        ClassLoader cl = Thread.currentThread().getContextClassLoader();

        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();

            if (driver.getClass().getClassLoader() == cl) {
                try {
                     LOG.info("Deregistering JDBC driver {}: ");
                    DriverManager.deregisterDriver(driver);

                } catch (SQLException ex) {
                     LOG.error("Error deregistering JDBC driver {} " +ex.getMessage(),ex);
                }
            } else {
                 LOG.info("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader");
            }
        }
        LOG.info("...::: WEB FINALIZO CORRECTAMNETE :::...");
    }

 


}
