package micrium.csu.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import micrium.csu.model.GrupoBilletera;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class GrupoBilleteraDAOImpl implements GrupoBilleteraDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(GrupoBilleteraDAOImpl.class);

    @Override
    public GrupoBilletera obtenerId(int id) {
        GrupoBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (GrupoBilletera) session.selectOne("micrium.csu.mybatis.GrupoBilletera.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public GrupoBilletera obtenerName(String name) {
        GrupoBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (GrupoBilletera) session.selectOne("micrium.csu.mybatis.GrupoBilletera.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<GrupoBilletera> obtenerLista() {
        List<GrupoBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.GrupoBilletera.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<GrupoBilletera> obtenerListaByIdBilletera(int IdBilletera) {
        List<GrupoBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.GrupoBilletera.selectListByIdBanco", IdBilletera);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.GrupoBilletera.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(GrupoBilletera dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.GrupoBilletera.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void update(GrupoBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.GrupoBilletera.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

}
