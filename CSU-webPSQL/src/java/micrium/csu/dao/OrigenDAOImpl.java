package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Origen;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class OrigenDAOImpl implements OrigenDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(OrigenDAOImpl.class);

    @Override
    public Origen obtenerId(int id) {
        Origen result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Origen) session.selectOne("micrium.csu.mybatis.Origen.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Origen obtenerName(String name) {
        Origen result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Origen) session.selectOne("micrium.csu.mybatis.Origen.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Origen> obtenerLista() {
        List<Origen> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Origen.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Origen.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Origen dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Origen.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Origen dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Origen.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Origen.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
}
