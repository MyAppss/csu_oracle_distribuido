package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Origen;

/**
 *
 * @author marciano
 */
public interface OrigenDAO {
    
    public Origen obtenerId(int id);
	public Origen obtenerName(String name);
    public List<Origen> obtenerLista();
    public void update(Origen dato) throws Exception;
    public void insert(Origen dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
	
//bind(OrigenDAO.class).to(OrigenDAOImpl.class);	
}
