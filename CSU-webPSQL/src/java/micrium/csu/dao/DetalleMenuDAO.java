/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.DetalleMenu;

/**
 *
 * @author Vehimar
 */
public interface DetalleMenuDAO {
    
    public List<DetalleMenu> obtenerIdCabecera(int id);
    public DetalleMenu obtenerName(String name);
    public List<DetalleMenu> obtenerLista();
    public void update(DetalleMenu dato) throws Exception;
    public void insert(DetalleMenu dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    
}
