package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.ComposicionBilletera;

/**
 *
 * @author marciano
 */
public interface ComposicionBilleteraDAO {
    
    public ComposicionBilletera obtenerId(int id);
    public ComposicionBilletera obtenerName(String name);
    public List<ComposicionBilletera> obtenerLista();
    public void update(ComposicionBilletera dato) throws Exception;
    public void insert(ComposicionBilletera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public List<ComposicionBilletera> obtenerListaByIdConfig(int idConfig);
	
//bind(ComposicionBilleteraDAO.class).to(ComposicionBilleteraDAOImpl.class);	
}
