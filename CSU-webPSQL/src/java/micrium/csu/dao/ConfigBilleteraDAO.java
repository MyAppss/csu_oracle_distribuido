package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.ConfigBilletera;

/**
 *
 * @author marciano
 */
public interface ConfigBilleteraDAO {
    
    public ConfigBilletera obtenerId(int id);
    public ConfigBilletera obtenerName(String name);
    public List<ConfigBilletera> obtenerLista();
    public void update(ConfigBilletera dato) throws Exception;
    public void insert(ConfigBilletera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public void updateOffAllByIdConfig(int configId) throws Exception;
    public ConfigBilletera obtenerbyIdBilleteraAndConfig(ConfigBilletera dato) throws Exception;
	
//bind(ConfigBilleteraDAO.class).to(CosBilleteraDAOImpl.class);	
}
