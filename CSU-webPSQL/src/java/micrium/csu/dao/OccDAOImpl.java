package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Occ;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import micrium.csu.view.OccView;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class OccDAOImpl implements OccDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(OccDAOImpl.class);

    @Override
    public Occ obtenerId(int id) {
        Occ result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Occ) session.selectOne("micrium.csu.mybatis.Occ.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Occ> obtenerListConfigId(int configId) {
        List<Occ> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Occ.selectListConfigId", configId);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: idConfig=" + configId + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Occ> obtenerLista() {
        List<Occ> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Occ.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Occ.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Occ dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Occ.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Occ dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Occ.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Occ.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public List<Occ> obtenerLista(OccView occView) {
        List<Occ> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Occ.selectListOcc", occView);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: occView=" + occView + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Occ obtenerOcc(OccView occView) {
        Occ result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Occ) session.selectOne("micrium.csu.mybatis.Occ.selectOcc", occView);
        } catch (Exception ex) {
            log.error("[obtenerOcc] Error: occView=" + occView.toString() + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<OccView> obtenerListaView(int configId) {
        List<OccView> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Occ.selectListOccView", configId);
        } catch (Exception ex) {
            log.error("[obtenerListaView] Error: configId=" + configId + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void deleteConfig(int configId) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Occ.deleteConfig", configId);
            session.commit();
        } catch (Exception ex) {
            log.error("[deleteConfig] Error: configId=" + configId + " ", ex);
        } finally {
            session.close();
        }
    }
}
