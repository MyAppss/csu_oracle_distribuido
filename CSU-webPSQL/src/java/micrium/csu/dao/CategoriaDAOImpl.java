/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.Categoria;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Leandro
 */
public class CategoriaDAOImpl implements CategoriaDAO, Serializable{
private static final long serialVersionUID = 1L;
@Inject
transient SqlSessionFactory sqlMapper;
private static final Logger log = Logger.getLogger(CategoriaDAOImpl.class);
    @Override
    public Categoria obtenerId(int id) {
          Categoria result = null;
          SqlSession session = sqlMapper.openSession();
        try {
            result = (Categoria) session.selectOne("micrium.csu.mybatis.Categoria.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();     
        }
        return result;
    }

    @Override
    public List<Categoria> obtenerLista() {
        List<Categoria> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Categoria.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(Categoria dato) throws Exception {
         SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Categoria.update", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Categoria dato) throws Exception {
         SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Categoria.insert", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Categoria.delete", id);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Categoria.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (Exception ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public Categoria obtenerName(String name) {
        Categoria result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Categoria) session.selectOne("micrium.csu.mybatis.Categoria.selectName", name);
        }catch(Exception ex){
          log.error("[obtenerName] Error: nombre="+name+" "
					,ex);      
        } 
        finally {
            session.close();   
        }
        return result;
    }
    
}
