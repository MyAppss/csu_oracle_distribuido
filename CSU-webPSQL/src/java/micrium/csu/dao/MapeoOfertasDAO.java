/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.MapeoOfertas;

/**
 *
 * @author User
 */
public interface MapeoOfertasDAO {

    public MapeoOfertas obtenerId(int id);

    public MapeoOfertas obtenerName(String name);

    public List<MapeoOfertas> obtenerLista();

    public void update(MapeoOfertas dato) throws Exception;

    public void insert(MapeoOfertas dato) throws Exception;

    public void delete(int id) throws Exception;

    public int obtenerIdSecuencia();

    public boolean existeOfferingId(int id);
}
