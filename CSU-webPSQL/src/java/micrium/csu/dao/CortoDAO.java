package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Corto;

/**
 *
 * @author marciano
 */
public interface CortoDAO {
    
    public Corto obtenerId(int id);
	public Corto obtenerName(String name);
    public List<Corto> obtenerLista();
    public void update(Corto dato) throws Exception;
    public void insert(Corto dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
	
//bind(CortoDAO.class).to(CortoDAOImpl.class);	
}
