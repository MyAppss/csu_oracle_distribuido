/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.DetalleCabecera;

/**
 *
 * @author Vehimar
 */
public interface DetalleCabeceraDAO {
    
    public List<DetalleCabecera> obtenerIdCabecera(int id);
    public DetalleCabecera obtenerName(String name);
    public List<DetalleCabecera> obtenerLista();
    public void update(DetalleCabecera dato) throws Exception;
    public void insert(DetalleCabecera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    
}
