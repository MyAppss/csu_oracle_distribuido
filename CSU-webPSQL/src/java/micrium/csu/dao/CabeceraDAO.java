/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Cabecera;

/**
 *
 * @author Vehimar
 */
public interface CabeceraDAO {
    
    public Cabecera obtenerId(int id);
    public Cabecera obtenerName(String name);
    public List<Cabecera> obtenerLista();
    public void update(Cabecera dato) throws Exception;
    public void updatePos(Cabecera dato) throws Exception;
    public void insert(Cabecera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public List<Cabecera> obtenerByIdConfiguracion(int idConfig);
    
}
