package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Parametro;

/**
 *
 * @author marciano
 */
public interface ParametroDAO {
    
    public Parametro obtenerId(int id);
    public Parametro obtenerName(String name);
    public List<Parametro> obtenerLista();
    public void update(Parametro dato) throws Exception;
    public void insert(Parametro dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public void updateChangeConfiguration(String nombre) throws Exception;
    public void updateValueByName(Parametro dato) throws Exception;
	
//bind(ParametroDAO.class).to(ParametroDAOImpl.class);	
} 
