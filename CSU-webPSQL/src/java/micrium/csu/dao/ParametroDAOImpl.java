package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Parametro;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ParametroDAOImpl implements ParametroDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ParametroDAOImpl.class);

    @Override
    public Parametro obtenerId(int id) {
        Parametro result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Parametro) session.selectOne("micrium.csu.mybatis.Parametro.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Parametro obtenerName(String name) {
        Parametro result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Parametro) session.selectOne("micrium.csu.mybatis.Parametro.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Parametro> obtenerLista() {
        List<Parametro> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Parametro.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Parametro.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Parametro dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Parametro.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Parametro dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Parametro.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    //public void setChangeConfiguration(String nombre) throws Exception;
    @Override
    public void updateChangeConfiguration(String nombre) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Parametro.setChangeConfiguration", nombre);
            session.commit();
        } catch (Exception ex) {
            log.error("[updateChangeConfiguration] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Parametro.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public void updateValueByName(Parametro dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Parametro.updateValueByName", dato);
            session.commit();
        } catch (NumberFormatException ex) {
            log.error("[updateValueByName] Error: ", ex);
        } finally {
            session.close();
        }
    }
}
