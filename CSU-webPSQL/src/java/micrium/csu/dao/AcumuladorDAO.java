/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Acumulador;

/**
 *
 * @author User
 */
public interface AcumuladorDAO {
 
    public Acumulador obtenerId(int id);
    public Acumulador obtenerName(String name);
    public List<Acumulador> obtenerLista();
    public void update(Acumulador dato) throws Exception;
    public void insert(Acumulador dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
}
