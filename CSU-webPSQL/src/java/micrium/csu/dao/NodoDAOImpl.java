/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.Nodo;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class NodoDAOImpl implements NodoDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(NodoDAOImpl.class);

    @Override
    public Nodo obtenerId(int id) {
        Nodo result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Nodo) session.selectOne("micrium.csu.mybatis.Nodo.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Nodo> obtenerLista() {
        List<Nodo> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Nodo.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(Nodo dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Nodo.update", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

     @Override
    public void insert(Nodo dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Nodo.insert", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Nodo.delete", id);
            session.commit();

        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Nodo.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

}
