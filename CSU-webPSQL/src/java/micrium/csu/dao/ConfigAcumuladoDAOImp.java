/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.ConfigAcumulado;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class ConfigAcumuladoDAOImp implements ConfigAcumuladoDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(ConfigAcumuladoDAOImp.class);

    @Override
    public void update(ConfigAcumulado dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigAcumulado.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error:", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(ConfigAcumulado dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.ConfigAcumulado.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error:", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public List<ConfigAcumulado> listByIdConfig(int idConfig) {
        List<ConfigAcumulado> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ConfigAcumulado.selectByIdConfig", idConfig);
        } catch (Exception ex) {
            log.error("[listByIdConfig] Error: ", ex);
        } finally {
            session.close();
        }
        return result;

    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.ConfigAcumulado.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

}
