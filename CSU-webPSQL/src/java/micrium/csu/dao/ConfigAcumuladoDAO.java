/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ConfigAcumulado;

/**
 *
 * @author Leandro
 */
public interface ConfigAcumuladoDAO {
    public void update(ConfigAcumulado dato) throws Exception;
    public void insert(ConfigAcumulado dato) throws Exception;
    public List<ConfigAcumulado> listByIdConfig(int idConfig);
    public int obtenerIdSecuencia();
}
