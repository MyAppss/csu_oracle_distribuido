package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Config;

/**
 *
 * @author marciano
 */
public interface ConfigDAO {
    
    public Config obtenerId(int id);
    public Config obtenerName(String name);
    public List<Config> obtenerLista();
    public void update(Config dato) throws Exception;
    public void insert(Config dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
	
//bind(ConfigDAO.class).to(ConfigDAOImpl.class);	
}
