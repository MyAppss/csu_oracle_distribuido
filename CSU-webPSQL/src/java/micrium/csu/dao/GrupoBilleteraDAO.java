package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.GrupoBilletera;


/**
 *
 * @author marciano
 */
public interface GrupoBilleteraDAO {
    
    public GrupoBilletera obtenerId(int id);
    public GrupoBilletera obtenerName(String name);
    public List<GrupoBilletera> obtenerLista();
    public List<GrupoBilletera> obtenerListaByIdBilletera(int IdBilletera);
    public void update(GrupoBilletera dato) throws Exception;
    public void insert(GrupoBilletera dato) throws Exception;
    public void delete(int id) throws Exception;
   
}
