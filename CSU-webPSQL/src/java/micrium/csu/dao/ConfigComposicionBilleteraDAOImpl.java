package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ConfigComposicionBilletera;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigComposicionBilleteraDAOImpl implements ConfigComposicionBilleteraDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ConfigComposicionBilleteraDAOImpl.class);

    @Override
    public ConfigComposicionBilletera obtenerId(int id) {
        ConfigComposicionBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigComposicionBilletera) session.selectOne("micrium.csu.mybatis.ConfigComposicionBilletera.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public ConfigComposicionBilletera obtenerName(String name) {
        ConfigComposicionBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigComposicionBilletera) session.selectOne("micrium.csu.mybatis.ConfigComposicionBilletera.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ConfigComposicionBilletera> obtenerLista() {
        List<ConfigComposicionBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ConfigComposicionBilletera.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigComposicionBilletera.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(ConfigComposicionBilletera dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.ConfigComposicionBilletera.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(ConfigComposicionBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigComposicionBilletera.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.ConfigComposicionBilletera.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public void updateOffAllByIdConfig(int configId) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigComposicionBilletera.updateOffAllByIdConfig", configId);
            session.commit();
        } catch (NumberFormatException ex) {
            log.error("[updateOffAllByIdConfig] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public ConfigComposicionBilletera obtenerbyIdCompBilleteraAndConfig(ConfigComposicionBilletera dato) throws Exception {
        ConfigComposicionBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigComposicionBilletera) session.selectOne("micrium.csu.mybatis.ConfigComposicionBilletera.obtenerbyIdCompBilleteraAndConfig", dato);
        } catch (Exception ex) {
            log.error("[obtenerbyIdCompBilleteraAndConfig] Error: Grupo nombre Comercial=" + dato.getNombreComercial() + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }
}
