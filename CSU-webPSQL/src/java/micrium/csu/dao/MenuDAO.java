/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Menu;

/**
 *
 * @author Leandro
 */
public interface MenuDAO {
    public Menu obtenerId(int id);
    public Menu obtenerName(String name);
    public List<Menu> obtenerLista();
    public void update(Menu dato) throws Exception;
    public void updatePos(Menu dato) throws Exception;
    public void insert(Menu dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public List<Menu> obtenerListaByConfigId(int configId);

}
