package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ReporteConsulta;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import micrium.csu.model.NodoBusquedaReporteConsultas;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ReporteConsultaDAOImpl implements ReporteConsultaDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ReporteConsultaDAOImpl.class);

    @Override
    public ReporteConsulta obtenerId(int id) {
        ReporteConsulta result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ReporteConsulta) session.selectOne("micrium.csu.mybatis.ReporteConsulta.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public ReporteConsulta obtenerName(String name) {
        ReporteConsulta result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ReporteConsulta) session.selectOne("micrium.csu.mybatis.ReporteConsulta.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ReporteConsulta> obtenerLista() {
        List<ReporteConsulta> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ReporteConsulta.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    //public List<ReporteConsulta> obtenerListaByIsdn(String isdn);
    @Override
    public List<ReporteConsulta> obtenerListaByIsdn(String isdn) {
        List<ReporteConsulta> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ReporteConsulta.selectListByIsdn", isdn);
        } catch (Exception ex) {
            log.error("[obtenerListaByIsdn] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ReporteConsulta> obtenerListaByMsidnGroup(NodoBusquedaReporteConsultas nod) {
        //Vehimar
        List<ReporteConsulta> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ReporteConsulta.selectListByMsisdnGroup", nod);
        } catch (Exception ex) {
            log.error("[obtenerListaByIsdn] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ReporteConsulta> obtenerListaByMsidnSessionID(NodoBusquedaReporteConsultas nod) {
        List<ReporteConsulta> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ReporteConsulta.selectListByMsisdnSessionId", nod);
        } catch (Exception ex) {
            log.error("[obtenerListaByIsdn] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ReporteConsulta> obtenerListaByMsisdnDate(NodoBusquedaReporteConsultas nod) {
        List<ReporteConsulta> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ReporteConsulta.selectListByMsisdnDate", nod);
        } catch (Exception ex) {
            log.error("[obtenerListaByMsisdnDate] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ReporteConsulta.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(ReporteConsulta dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.ReporteConsulta.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public void update(ReporteConsulta dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ReporteConsulta.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.ReporteConsulta.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
}
