package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Cos;

/**
 *
 * @author marciano
 */
public interface CosDAO {
    
    public Cos obtenerId(int id);
    public Cos obtenerName(String name);
    public List<Cos> obtenerLista();
    public void update(Cos dato) throws Exception;
    public void insert(Cos dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
	
//bind(CosDAO.class).to(CosDAOImpl.class);	
}
