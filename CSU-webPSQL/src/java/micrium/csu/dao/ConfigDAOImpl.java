package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Config;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigDAOImpl implements ConfigDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ConfigDAOImpl.class);

    @Override
    public Config obtenerId(int id) {
        Config result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Config) session.selectOne("micrium.csu.mybatis.Config.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Config obtenerName(String name) {
        Config result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Config) session.selectOne("micrium.csu.mybatis.Config.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Config> obtenerLista() {
        List<Config> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Config.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Config.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Config dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Config.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Config dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Config.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Config.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
}
