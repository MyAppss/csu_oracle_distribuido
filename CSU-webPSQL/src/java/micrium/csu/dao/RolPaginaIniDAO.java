package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.RolPaginaIni;

/**
 *
 * @author marciano
 */
public interface RolPaginaIniDAO {
    
    public RolPaginaIni obtenerId(int id);
//	public RolPaginaIni obtenerName(String name);
//    public List<RolPaginaIni> obtenerLista();
    public void update(RolPaginaIni dato) throws Exception;
//    public void insert(RolPaginaIni dato) throws Exception;
//    public void delete(int id) throws Exception;
//    public int obtenerIdSecuencia();
	
//bind(RolPaginaIniDAO.class).to(RolPaginaIniDAOImpl.class);	
}
