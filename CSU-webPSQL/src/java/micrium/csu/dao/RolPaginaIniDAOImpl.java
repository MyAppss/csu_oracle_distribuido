package micrium.csu.dao;

import micrium.csu.model.RolPaginaIni;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class RolPaginaIniDAOImpl implements RolPaginaIniDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(RolPaginaIniDAOImpl.class);

    @Override
    public RolPaginaIni obtenerId(int id) {
        RolPaginaIni result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (RolPaginaIni) session.selectOne("micrium.csu.mybatis.RolPaginaIni.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }
//  
//    @Override
//    public RolPaginaIni obtenerName(String name) {
//        RolPaginaIni result = null;
//        SqlSession session = sqlMapper.openSession();
//        try {
//            result = (RolPaginaIni) session.selectOne("micrium.csu.mybatis.RolPaginaIni.selectName", name);
//        }catch(Exception ex){
//          log.error("[obtenerName] Error: nombre="+name+" "
//					,ex);      
//        } 
//		finally {
//            session.close();
//            return result;
//        }
//    }
//  
//    @Override
//    public List<RolPaginaIni> obtenerLista()  {
//        List<RolPaginaIni> result = null;
//        SqlSession session = sqlMapper.openSession();
//        try {
//            result = session.selectList("micrium.csu.mybatis.RolPaginaIni.selectList");
//        }catch(Exception ex){
//          log.error("[obtenerLista] Error: " 
//					,ex);      
//        }
//		finally {
//            session.close();
//        }
//        return result;
//    }

//  
//	@Override
//    public void delete(int id) throws Exception{
//        SqlSession session = sqlMapper.openSession();
//        try {
//            session.update("micrium.csu.mybatis.RolPaginaIni.delete", id);
//            session.commit();
//        } finally {
//            session.close();
//        }
//    }
//    @Override
//    public void insert(RolPaginaIni dato)  throws Exception{
//        //yyy.setStatus(1);
//         SqlSession session = sqlMapper.openSession();
//        try {
//            session.insert("micrium.csu.mybatis.RolPaginaIni.insert", dato);
//            session.commit();
//        } finally {
//            session.close();
//        }
//    }
    @Override
    public void update(RolPaginaIni dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.RolPaginaIni.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

//	@Override
//	public int obtenerIdSecuencia(){
//       
//        int maxId=-1;
//        SqlSession session = sqlMapper.openSession();
//        try {
//            
//            String str=session.selectOne("micrium.csu.mybatis.RolPaginaIni.idSecuencial").toString();
//            maxId =Integer.parseInt(str);
//        }catch(Exception ex){
//          log.error("[obtenerIdSecuencia] Error: " 
//					,ex);      
//        }
//		finally {
//            session.close();
//            return maxId;
//        }
//    }
}
