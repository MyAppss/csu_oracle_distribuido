package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ComposicionBilletera;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ComposicionBilleteraDAOImpl implements ComposicionBilleteraDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ComposicionBilleteraDAOImpl.class);

    @Override
    public ComposicionBilletera obtenerId(int id) {
        ComposicionBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ComposicionBilletera) session.selectOne("micrium.csu.mybatis.ComposicionBilletera.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public ComposicionBilletera obtenerName(String name) {
        ComposicionBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ComposicionBilletera) session.selectOne("micrium.csu.mybatis.ComposicionBilletera.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ComposicionBilletera> obtenerLista() {
        List<ComposicionBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ComposicionBilletera.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ComposicionBilletera> obtenerListaByIdConfig(int idConfig) {
        List<ComposicionBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ComposicionBilletera.selectListByIdConfig", idConfig);
        } catch (Exception ex) {
            log.error("[obtenerListaByIdConfig] Error: "
                    + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ComposicionBilletera.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(ComposicionBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.ComposicionBilletera.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(ComposicionBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ComposicionBilletera.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {

        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.csu.mybatis.ComposicionBilletera.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (Exception ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();

        }
        return maxId;
    }
}
