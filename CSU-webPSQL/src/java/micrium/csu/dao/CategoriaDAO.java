/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Categoria;

/**
 *
 * @author Leandro
 */
public interface CategoriaDAO {
    public Categoria obtenerId(int id);
    public Categoria obtenerName(String name);
    public List<Categoria> obtenerLista();
    public void update(Categoria dato) throws Exception;
    public void insert(Categoria dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();

}
