package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Occ;
import micrium.csu.view.OccView;

/**
 *
 * @author marciano
 */
public interface OccDAO {

    public Occ obtenerId(int id);
    public List<Occ> obtenerListConfigId(int configId);
    public List<Occ> obtenerLista();
    public List<Occ> obtenerLista(OccView occView);
    public List<OccView> obtenerListaView(int configId);
    public Occ obtenerOcc(OccView occView);
    public void update(Occ dato) throws Exception;
    public void insert(Occ dato) throws Exception;
    public void delete(int id) throws Exception;
    public void deleteConfig(int configId) throws Exception;
    public int obtenerIdSecuencia();
//bind(OccDAO.class).to(OccDAOImpl.class);	
}
