package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ConfigBilletera;
import  micrium.csu.model.ConfigComposicionBilletera;

/**
 *
 * @author marciano
 */
public interface ConfigComposicionBilleteraDAO {
    
    public ConfigComposicionBilletera obtenerId(int id);
    public ConfigComposicionBilletera obtenerName(String name);
    public List<ConfigComposicionBilletera> obtenerLista();
    public void update(ConfigComposicionBilletera dato) throws Exception;
    public void insert(ConfigComposicionBilletera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public void updateOffAllByIdConfig(int configId) throws Exception;
    public ConfigComposicionBilletera obtenerbyIdCompBilleteraAndConfig(ConfigComposicionBilletera dato) throws Exception;
	
//bind(ConfigComposicionBilleteraDAO.class).to(CosComposicionBilleteraDAOImpl.class);	
}
