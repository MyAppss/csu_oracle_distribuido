/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Nodo;

/**
 *
 * @author Leandro
 */
public interface NodoDAO {

    public Nodo obtenerId(int id);

    public List<Nodo> obtenerLista();

    public void update(Nodo dato) throws Exception;

    public void insert(Nodo dato) throws Exception;

    public void delete(int id) throws Exception;

    public int obtenerIdSecuencia();

}
