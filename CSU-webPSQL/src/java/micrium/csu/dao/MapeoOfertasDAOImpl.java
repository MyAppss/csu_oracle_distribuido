/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.MapeoOfertas;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class MapeoOfertasDAOImpl implements MapeoOfertasDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(MapeoOfertasDAOImpl.class);

    @Override
    public MapeoOfertas obtenerId(int id) {
        MapeoOfertas result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (MapeoOfertas) session.selectOne("micrium.csu.mybatis.MapeoOfertas.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public MapeoOfertas obtenerName(String name) {
        MapeoOfertas result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (MapeoOfertas) session.selectOne("micrium.csu.mybatis.MapeoOfertas.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<MapeoOfertas> obtenerLista() {
        List<MapeoOfertas> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.MapeoOfertas.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(MapeoOfertas dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.MapeoOfertas.update", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(MapeoOfertas dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.MapeoOfertas.insert", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.MapeoOfertas.delete", id);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.csu.mybatis.MapeoOfertas.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public boolean existeOfferingId(int id) {
        SqlSession session = sqlMapper.openSession();
        int resultado = 0;
        try {
            resultado = (int) session.selectOne("micrium.csu.mybatis.MapeoOfertas.obtenerOfferingParent", id);
            session.commit();
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return resultado != 0;

    }

}
