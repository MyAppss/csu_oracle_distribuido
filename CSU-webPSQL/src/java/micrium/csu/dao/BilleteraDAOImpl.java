package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Billetera;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class BilleteraDAOImpl implements BilleteraDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(BilleteraDAOImpl.class);

    @Override
    public Billetera obtenerId(int id) {
        Billetera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Billetera) session.selectOne("micrium.csu.mybatis.Billetera.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Billetera obtenerName(String name) {
        Billetera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Billetera) session.selectOne("micrium.csu.mybatis.Billetera.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerLista() {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Billetera.delete", id);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Billetera dato) throws Exception {
        //yyy.setStatus(1);
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Billetera.insert", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Billetera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Billetera.update", dato);
            if (dato.getAcumulado() != null && dato.getAcumulado().equals("f")) {
                session.update("micrium.csu.mybatis.Billetera.updateBillleraAcumulado", dato.getBilleteraId());
            }

            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {

        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.csu.mybatis.Billetera.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (Exception ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public List<Billetera> obtenerListaByIdConfig(int idConfig) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdConfig", idConfig);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: "
                    + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerListaByIdConfigMenu(int idUnitType, int idConfig) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            Map<String, Object> params = new HashMap();
            params.put("idUnitType", idUnitType);
            params.put("idConfig", idConfig);
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdConfigMenu", params);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }
    //public List<Billetera> obtenerListaByIdUnitType(int idUnitType);

    @Override
    public List<Billetera> obtenerListaByIdUnitType(int idUnitType) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdUnitType", idUnitType);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }
    //public List<Billetera> obtenerListaByIdCompoBilletera(int idCompBilletera);

    @Override
    public List<Billetera> obtenerListaByIdCompoBilletera(int idCompBilletera) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdCompBilletera", idCompBilletera);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerListaByIdUnitTypeSinAlco(int idUnitType) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdUnitTypeSinAlco", idUnitType);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerListaByIdCabecera(int idCabecera) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdCabecera", idCabecera);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerListaByIdMenu(int idMenu) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdMenu", idMenu);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Billetera> obtenerListaByIdConfigCabecera(int idUnitType, int idConfig) {
        List<Billetera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            Map<String, Object> params = new HashMap();
            params.put("idUnitType", idUnitType);
            params.put("idConfig", idConfig);
            result = session.selectList("micrium.csu.mybatis.Billetera.selectListByIdConfigCabecera", params);
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: " + ex.getMessage());
        } finally {
            session.close();
        }
        return result;
    }
}
