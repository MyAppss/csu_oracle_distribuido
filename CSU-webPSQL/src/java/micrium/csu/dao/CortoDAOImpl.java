package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.Corto;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class CortoDAOImpl implements CortoDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(CortoDAOImpl.class);

    @Override
    public Corto obtenerId(int id) {
        Corto result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Corto) session.selectOne("micrium.csu.mybatis.Corto.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Corto obtenerName(String name) {
        Corto result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Corto) session.selectOne("micrium.csu.mybatis.Corto.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Corto> obtenerLista() {
        List<Corto> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Corto.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Corto.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Corto dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Corto.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(Corto dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Corto.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Corto.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
}
