package micrium.csu.dao;

import java.util.List;
import micrium.csu.model.ConfigBilletera;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import com.google.inject.Inject;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigBilleteraDAOImpl implements ConfigBilleteraDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;

    private static final Logger log = Logger.getLogger(ConfigBilleteraDAOImpl.class);

    @Override
    public ConfigBilletera obtenerId(int id) {
        ConfigBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigBilletera) session.selectOne("micrium.csu.mybatis.ConfigBilletera.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public ConfigBilletera obtenerName(String name) {
        ConfigBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigBilletera) session.selectOne("micrium.csu.mybatis.ConfigBilletera.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    //public ConfigBilletera obtenerbyIdBilleteraAndConfig(ConfigBilletera dato) throws Exception;
    @Override
    public ConfigBilletera obtenerbyIdBilleteraAndConfig(ConfigBilletera dato) throws Exception {
        ConfigBilletera result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (ConfigBilletera) session.selectOne("micrium.csu.mybatis.ConfigBilletera.obtenerbyIdBilleteraAndConfig", dato);
        } catch (Exception ex) {
            log.error("[obtenerbyIdBilleteraAndConfig] Error: nombre=" + dato.getNombreComercial() + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<ConfigBilletera> obtenerLista() {
        List<ConfigBilletera> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.ConfigBilletera.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigBilletera.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(ConfigBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.ConfigBilletera.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void update(ConfigBilletera dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigBilletera.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    //public boolean updateOffAllByIdConfig(int idConfig);
    @Override
    public void updateOffAllByIdConfig(int configId) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.ConfigBilletera.updateOffAllByIdConfig", configId);
            session.commit();
        } catch (Exception ex) {
            log.error("[updateOffAllByIdConfig] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.ConfigBilletera.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (Exception ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
}
