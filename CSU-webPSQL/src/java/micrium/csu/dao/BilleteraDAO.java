package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.Billetera;

/**
 *
 * @author marciano
 */
public interface BilleteraDAO {
    
    public Billetera obtenerId(int id);
    public Billetera obtenerName(String name);
    public List<Billetera> obtenerLista();
    public void update(Billetera dato) throws Exception;
    public void insert(Billetera dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public List<Billetera> obtenerListaByIdConfig(int idConfig);
    public List<Billetera> obtenerListaByIdConfigMenu(int idUnitType,int idConfig);
    public List<Billetera> obtenerListaByIdConfigCabecera(int idUnitType,int idConfig);
    public List<Billetera> obtenerListaByIdUnitType(int idUnitType);
    public List<Billetera> obtenerListaByIdUnitTypeSinAlco(int idUnitType);
    public List<Billetera> obtenerListaByIdCompoBilletera(int idCompBilletera);
    public List<Billetera> obtenerListaByIdCabecera(int idCabecera);
    public List<Billetera> obtenerListaByIdMenu(int idMenu);
    
	
//bind(BilleteraDAO.class).to(BilleteraDAOImpl.class);	
}
