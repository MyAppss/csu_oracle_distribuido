/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.Acumulador;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class AcumuladorDAOImpl implements AcumuladorDAO, Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(AcumuladorDAOImpl.class);

    @Override
    public Acumulador obtenerId(int id) {
        Acumulador result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Acumulador) session.selectOne("micrium.csu.mybatis.Acumulador.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public Acumulador obtenerName(String name) {
        Acumulador result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Acumulador) session.selectOne("micrium.csu.mybatis.Acumulador.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Acumulador> obtenerLista() {
        List<Acumulador> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Acumulador.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(Acumulador dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Acumulador.update", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Acumulador dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Acumulador.insert", dato);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Acumulador.delete", id);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {

            String str = session.selectOne("micrium.csu.mybatis.Acumulador.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (Exception ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }
   
}
