/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.dao;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.model.Menu;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Leandro
 */
public class MenuDAOImpl implements MenuDAO, Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    transient SqlSessionFactory sqlMapper;
    private static final Logger log = Logger.getLogger(MenuDAOImpl.class);

    @Override
    public Menu obtenerId(int id) {
        Menu result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Menu) session.selectOne("micrium.csu.mybatis.Menu.selectId", id);
        } catch (Exception ex) {
            log.error("[obtenerId] Error: id=" + id + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Menu> obtenerLista() {
        List<Menu> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Menu.selectList");
        } catch (Exception ex) {
            log.error("[obtenerLista] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(Menu dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Menu.update", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[update] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(Menu dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.insert("micrium.csu.mybatis.Menu.insert", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[insert] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(int id) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Menu.delete", id);
            session.commit();
        } catch (Exception ex) {
            log.error("[delete] Error: ", ex);
        } finally {
            session.close();
        }
    }

    @Override
    public int obtenerIdSecuencia() {
        int maxId = -1;
        SqlSession session = sqlMapper.openSession();
        try {
            String str = session.selectOne("micrium.csu.mybatis.Menu.idSecuencial").toString();
            maxId = Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            log.error("[obtenerIdSecuencia] Error: ", ex);
        } finally {
            session.close();
        }
        return maxId;
    }

    @Override
    public Menu obtenerName(String name) {
        Menu result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = (Menu) session.selectOne("micrium.csu.mybatis.Menu.selectName", name);
        } catch (Exception ex) {
            log.error("[obtenerName] Error: nombre=" + name + " ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Menu> obtenerListaByConfigId(int configId) {
        List<Menu> result = null;
        SqlSession session = sqlMapper.openSession();
        try {
            result = session.selectList("micrium.csu.mybatis.Menu.selectListByConfigId", configId);
        } catch (Exception ex) {
            log.error("[obtenerListaByConfigId] Error: ", ex);
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void updatePos(Menu dato) throws Exception {
        SqlSession session = sqlMapper.openSession();
        try {
            session.update("micrium.csu.mybatis.Menu.updatePos", dato);
            session.commit();
        } catch (Exception ex) {
            log.error("[updatePos] Error: ", ex);
        } finally {
            session.close();
        }
    }

}
