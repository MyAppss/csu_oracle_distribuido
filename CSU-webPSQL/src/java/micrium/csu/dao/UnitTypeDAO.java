package micrium.csu.dao;

import java.util.List;
import  micrium.csu.model.UnitType;

/**
 *
 * @author marciano
 */
public interface UnitTypeDAO {
    
    public UnitType obtenerId(int id);
    public UnitType obtenerName(String name);
    public List<UnitType> obtenerLista();
    public void update(UnitType dato) throws Exception;
    public void insert(UnitType dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
	
//bind(UnitTypeDAO.class).to(UnitTypeDAOImpl.class);	
}
