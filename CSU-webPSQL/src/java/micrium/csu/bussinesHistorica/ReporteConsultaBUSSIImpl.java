package micrium.csu.bussinesHistorica;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import micrium.csu.model.ReporteConsulta;
import com.google.inject.Inject;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import micrium.configurable.util.UtilJson;
import micrium.csu.daoHistorica.ReporteConsultaDAO;
import micrium.csu.model.NodoBusquedaReporteConsultas;
import micrium.csu.model.Pantalla;
import micrium.csu.model.ReporteConsultaGroup;
import micrium.csu.model.Resultado;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ReporteConsultaBUSSIImpl implements ReporteConsultaBUSSI, Serializable {

    @Inject
    private ReporteConsultaDAO dao;

    private static final Logger LOG = Logger.getLogger(ReporteConsultaBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            LOG.error("[delete] Error al eliminar ReporteConsulta con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(ReporteConsulta dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                //dato.setReporteConsultaId(id);
                dao.insert(dato);
                sw = true;
            } else {
                LOG.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            LOG.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public ReporteConsulta obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<ReporteConsulta> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public List<ReporteConsulta> obtenerListaByIsdn(String isdn) {
        return dao.obtenerListaByIsdn(isdn);
    }

    @Override
    public List<ReporteConsulta> obtenerListaByMsisdnDate(String isdn, Date fechaIni, Date fechaFin) {
        NodoBusquedaReporteConsultas nod = new NodoBusquedaReporteConsultas();
        nod.setMsisdn(isdn);
        nod.setFechaIni(fechaIni);
        nod.setFechaFin(fechaFin);
        return dao.obtenerListaByMsisdnDate(nod);
    }

    @Override
    public boolean update(ReporteConsulta dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            LOG.error("[update]Error al modificar ReporteConsulta con Id:" + dato.getIdentificadorServicio(),
                    e);
        }
        return sw;
    }

    @Override
    public String validate(ReporteConsulta dato, String idStr) {

        if (dato.getMsisdn().isEmpty()) {
            return ParameterMessage.ReporteConsulta_ERROR_msisdn;
        }
        if (dato.getIdentificadorServicio().isEmpty()) {
            return ParameterMessage.ReporteConsulta_ERROR_identificadorServicio;
        }
        if (dato.getNombreCanal().isEmpty()) {
            return ParameterMessage.ReporteConsulta_ERROR_nombreCanal;
        }
        if (dato.getLongitudMaxSolicitada() > 0) {
            return ParameterMessage.ReporteConsulta_ERROR_longitudMaxSolicitada;
        }
        if (dato.getIpCliente().isEmpty()) {
            return ParameterMessage.ReporteConsulta_ERROR_ipCliente;
        }
        if (dato.getTextoGenerado().isEmpty()) {
            return ParameterMessage.ReporteConsulta_ERROR_textoGenerado;
        }

        //ReporteConsulta  datoAux=dao.obtenerName(dato.getNombre());
        //if(datoAux==null)
        //        return "";
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getReporteConsultaId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
        return "este Nombre ya existe";
    }

    /*@Override
    public List<ReporteConsultaGroup> obtenerListaByMsidnGroup(String isdn, Date fechaIni, Date fechaFin) {

        NodoBusquedaReporteConsultas nod = new NodoBusquedaReporteConsultas();
        nod.setMsisdn(isdn);
        nod.setFechaIni(fechaIni);
        nod.setFechaFin(fechaFin);        
        List<ReporteConsulta> groups = dao.obtenerListaByMsidnGroup(nod);
        List<ReporteConsultaGroup> groupsSessionID = new ArrayList<>();
        for (ReporteConsulta groupConsulta : groups) {
                nod.setFechaAux(groupConsulta.getFecha());
                nod.setSessionID(groupConsulta.getSessionId());
                List<ReporteConsulta> consultas = dao.obtenerListaByMsidnSessionID(nod);
                ReporteConsultaGroup group = new ReporteConsultaGroup(groupConsulta, consultas);
                groupsSessionID.add(group);
        }
        return groupsSessionID;
    }*/
    @Override
    public List<ReporteConsultaGroup> obtenerListaByMsidnGroup(String isdn, Date fechaIni, Date fechaFin) {

        NodoBusquedaReporteConsultas nod = new NodoBusquedaReporteConsultas();
        nod.setMsisdn(isdn);
        nod.setFechaIni(fechaIni);
        nod.setFechaFin(fechaFin);

        List<ReporteConsulta> groups = dao.obtenerListaByMsidnGroup(nod);
        List<ReporteConsultaGroup> groupsSessionID = new ArrayList<>();

        for (ReporteConsulta groupConsulta : groups) {
            nod.setFechaAux(groupConsulta.getFecha());
            nod.setSessionID(groupConsulta.getSessionId());

            List<ReporteConsulta> consultas1 = dao.obtenerListaByMsidnSessionID(nod);

            String rest = consultas1.get(0).getTextoGenerado();

            List<Pantalla> pantallas = getFromJson(rest);
            List<ReporteConsulta> listaConsultas = new ArrayList<>();

            if (pantallas != null) {
                for (int i = 0; i < pantallas.size(); i++) {

                    ReporteConsulta p = new ReporteConsulta();
                    p.setFecha(groupConsulta.getFecha());
                    p.setIdentificadorServicio(consultas1.get(0).getIdentificadorServicio());

                    p.setNombreCanal(consultas1.get(0).getNombreCanal());
                    p.setOpcion(consultas1.get(0).getOpcion());
                    if (pantallas.get(i).getIdPantalla() == 1) {
                        String aux = "Nro Pant: " + pantallas.get(i).getIdPantalla();
                        p.setTextoGenerado(aux + "\n \n" + pantallas.get(i).getCabecera());
                    } else {
                        String aux = "Nro Pant: " + pantallas.get(i).getIdPantalla() + "\nPant. Padre: " + pantallas.get(i).getPantallaPadre() + " - Opc Padre: " + pantallas.get(i).getOpcionPadre();
                        p.setTextoGenerado(aux + "\n" + pantallas.get(i).getCabecera() + "\n" + pantallas.get(i).getOpciones());
                    }

                    listaConsultas.add(p);
                }
            }

            ReporteConsultaGroup group = new ReporteConsultaGroup(groupConsulta, listaConsultas);
            groupsSessionID.add(group);
        }

        return groupsSessionID;
    }

    private List<Pantalla> getFromJson(String textoGenerado) {
        List<Pantalla> pantalla = null;
        Resultado resultado = UtilJson.getFromJson(textoGenerado);
        if (resultado != null) {
            pantalla = resultado.getPantallas();
        }

        return pantalla;
    }

}
