package micrium.csu.model;

import java.io.Serializable;

public class Cos implements Serializable {

    private Integer cosId;
    private String nombre;
    private String descripcion;
    private String habilitado;
    private String estado;

    public Integer getCosId() {
        return cosId;
    }

    public void setCosId(Integer dato) {
        this.cosId = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String dato) {
        this.descripcion = dato;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String dato) {
        this.habilitado = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }
}
