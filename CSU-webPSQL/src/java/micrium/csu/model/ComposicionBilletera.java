package micrium.csu.model;

import java.io.Serializable;

public class ComposicionBilletera implements Serializable {

    private Integer composicionBilleteraId;
    private String nombre;
    private String nombreComercial;
    private String prefijoUnidad;
    private String operador;
    private Double valor;
    private Integer cantidadDecimales;
    private String estado;
    private Integer unitTypeId;
    private double montoMinimo;
    private double montoMaximo;
    //-------------------------------------------------------------------------
    private String Config_nombreComercial;
    private boolean Config_mostrar_siempre;
    private boolean Config_mostrar_saldo_mayor_cer;
    private boolean Config_mostrar_saldo_menor_cer;
    private boolean Config_mostrar_saldo_cero;
    private boolean Config_no_mostrar_saldo_expira;
    private boolean Config_mostrar_vigencia;
    private boolean Config_mostrar_saldo_expirado;
    private int Config_posicion;
    private boolean newForConfig;

    //-------------------------------------------------------------------------
    private boolean mostrarSegundaFechaExp;
    private boolean mostrarHoraSegundaFechaExp;
    private boolean asumirFormatoHoraPrimeraFecha;

    public Integer getComposicionBilleteraId() {
        return composicionBilleteraId;
    }

    public void setComposicionBilleteraId(Integer dato) {
        this.composicionBilleteraId = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String dato) {
        this.nombreComercial = dato;
    }

    public String getPrefijoUnidad() {
        return prefijoUnidad;
    }

    public void setPrefijoUnidad(String dato) {
        this.prefijoUnidad = dato;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String dato) {
        this.operador = dato;
    }

    public Integer getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(Integer dato) {
        this.cantidadDecimales = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public boolean isConfig_mostrar_saldo_cero() {
        return Config_mostrar_saldo_cero;
    }

    public void setConfig_mostrar_saldo_cero(boolean Config_mostrar_saldo_cero) {
        this.Config_mostrar_saldo_cero = Config_mostrar_saldo_cero;
    }

    public boolean isConfig_no_mostrar_saldo_expirado() {
        return Config_no_mostrar_saldo_expira;
    }

    public void setConfig_no_mostrar_saldo_expirado(boolean Config_no_mostrar_saldo_expirado) {
        this.Config_no_mostrar_saldo_expira = Config_no_mostrar_saldo_expirado;
    }

    public boolean isConfig_mostrar_saldo_mayor_cero() {
        return Config_mostrar_saldo_mayor_cer;
    }

    public void setConfig_mostrar_saldo_mayor_cero(boolean Config_mostrar_saldo_mayor_cero) {
        this.Config_mostrar_saldo_mayor_cer = Config_mostrar_saldo_mayor_cero;
    }

    public boolean isConfig_mostrar_saldo_menor_cero() {
        return Config_mostrar_saldo_menor_cer;
    }

    public void setConfig_mostrar_saldo_menor_cero(boolean Config_mostrar_saldo_menor_cero) {
        this.Config_mostrar_saldo_menor_cer = Config_mostrar_saldo_menor_cero;
    }

    public boolean isConfig_mostrar_siempre() {
        return Config_mostrar_siempre;
    }

    public void setConfig_mostrar_siempre(boolean Config_mostrar_siempre) {
        this.Config_mostrar_siempre = Config_mostrar_siempre;
    }

    public String getConfig_nombreComercial() {
        return Config_nombreComercial;
    }

    public void setConfig_nombreComercial(String Config_nombreComercial) {
        this.Config_nombreComercial = Config_nombreComercial;
    }

    public boolean isNewForConfig() {
        return newForConfig;
    }

    public void setNewForConfig(boolean newForConfig) {
        this.newForConfig = newForConfig;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public double getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(double montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public int getConfig_posicion() {
        return Config_posicion;
    }

    public void setConfig_posicion(int Config_posicion) {
        this.Config_posicion = Config_posicion;
    }

    public boolean isConfig_mostrar_vigencia() {
        return Config_mostrar_vigencia;
    }

    public void setConfig_mostrar_vigencia(boolean Config_mostrar_vigencia) {
        this.Config_mostrar_vigencia = Config_mostrar_vigencia;
    }

    public boolean isConfig_mostrar_saldo_expirado() {
        return Config_mostrar_saldo_expirado;
    }

    public void setConfig_mostrar_saldo_expirado(boolean Config_mostrar_saldo_expirado) {
        this.Config_mostrar_saldo_expirado = Config_mostrar_saldo_expirado;
    }

    public boolean isMostrarSegundaFechaExp() {
        return mostrarSegundaFechaExp;
    }

    public void setMostrarSegundaFechaExp(boolean mostrarSegundaFechaExp) {
        this.mostrarSegundaFechaExp = mostrarSegundaFechaExp;
    }

    public boolean isMostrarHoraSegundaFechaExp() {
        return mostrarHoraSegundaFechaExp;
    }

    public void setMostrarHoraSegundaFechaExp(boolean mostrarHoraSegundaFechaExp) {
        this.mostrarHoraSegundaFechaExp = mostrarHoraSegundaFechaExp;
    }

    public boolean isAsumirFormatoHoraPrimeraFecha() {
        return asumirFormatoHoraPrimeraFecha;
    }

    public void setAsumirFormatoHoraPrimeraFecha(boolean asumirFormatoHoraPrimeraFecha) {
        this.asumirFormatoHoraPrimeraFecha = asumirFormatoHoraPrimeraFecha;
    }

}
