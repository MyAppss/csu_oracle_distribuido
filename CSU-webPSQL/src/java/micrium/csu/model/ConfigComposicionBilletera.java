package micrium.csu.model;

import java.io.Serializable;

public class ConfigComposicionBilletera implements Serializable {

    private Integer composicionBilleteraId;
    private Integer configId;
    private String nombreComercial;
    private String mostrarSiempre;
    private String mostrarSaldoMayorCero;
    private String mostrarSaldoMenorCero;
    private String mostrarSaldoCero;
    private String noMostrarSaldoExpirado;
    private String mostrarVigencia;
    private String mostrarSaldoExpirado;
    /////////////////////////////////////////
    private String mostrarSegundaFechaExp;
    private String mostrarHoraSegundaFechaExp;
    private String asumirFormatoHoraPrimeraFecha;
    /////////////////////////////////////////
    private int posicion;
    private String estado;

    public Integer getComposicionBilleteraId() {
        return composicionBilleteraId;
    }

    public void setComposicionBilleteraId(Integer dato) {
        this.composicionBilleteraId = dato;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String dato) {
        this.nombreComercial = dato;
    }

    public String getMostrarSiempre() {
        return mostrarSiempre;
    }

    public void setMostrarSiempre(String dato) {
        this.mostrarSiempre = dato;
    }

    public String getMostrarSaldoMayorCero() {
        return mostrarSaldoMayorCero;
    }

    public void setMostrarSaldoMayorCero(String dato) {
        this.mostrarSaldoMayorCero = dato;
    }

    public String getMostrarSaldoMenorCero() {
        return mostrarSaldoMenorCero;
    }

    public void setMostrarSaldoMenorCero(String dato) {
        this.mostrarSaldoMenorCero = dato;
    }

    public String getMostrarSaldoCero() {
        return mostrarSaldoCero;
    }

    public void setMostrarSaldoCero(String dato) {
        this.mostrarSaldoCero = dato;
    }

    public String getNoMostrarSaldoExpirado() {
        return noMostrarSaldoExpirado;
    }

    public void setNoMostrarSaldoExpirado(String noMostrarSaldoExpirado) {
        this.noMostrarSaldoExpirado = noMostrarSaldoExpirado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public String getMostrarVigencia() {
        return mostrarVigencia;
    }

    public void setMostrarVigencia(String mostrarVigencia) {
        this.mostrarVigencia = mostrarVigencia;
    }

    public String getMostrarSaldoExpirado() {
        return mostrarSaldoExpirado;
    }

    public void setMostrarSaldoExpirado(String mostrarSaldoExpirado) {
        this.mostrarSaldoExpirado = mostrarSaldoExpirado;
    }

    public String getMostrarSegundaFechaExp() {
        return mostrarSegundaFechaExp;
    }

    public void setMostrarSegundaFechaExp(String mostrarSegundaFechaExp) {
        this.mostrarSegundaFechaExp = mostrarSegundaFechaExp;
    }

    public String getMostrarHoraSegundaFechaExp() {
        return mostrarHoraSegundaFechaExp;
    }

    public void setMostrarHoraSegundaFechaExp(String mostrarHoraSegundaFechaExp) {
        this.mostrarHoraSegundaFechaExp = mostrarHoraSegundaFechaExp;
    }

    public String getAsumirFormatoHoraPrimeraFecha() {
        return asumirFormatoHoraPrimeraFecha;
    }

    public void setAsumirFormatoHoraPrimeraFecha(String asumirFormatoHoraPrimeraFecha) {
        this.asumirFormatoHoraPrimeraFecha = asumirFormatoHoraPrimeraFecha;
    }
    
    
}