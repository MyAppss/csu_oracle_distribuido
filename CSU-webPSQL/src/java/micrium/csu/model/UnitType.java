package micrium.csu.model;

import java.io.Serializable;


public class UnitType implements Serializable {

     private  Integer  unitTypeId;
     private  String  nombre;
     private  String  descripcion;
     private  String  estado;

   public Integer getUnitTypeId() {
        return unitTypeId;
    }
   public void setUnitTypeId(Integer dato) {
        this.unitTypeId=dato;
    }
   public String getNombre() {
        return nombre;
    }
   public void setNombre(String dato) {
        this.nombre=dato;
    }
   public String getDescripcion() {
        return descripcion;
    }
   public void setDescripcion(String dato) {
        this.descripcion=dato;
    }
   public String getEstado() {
        return estado;
    }
   public void setEstado(String dato) {
        this.estado=dato;
    }

}