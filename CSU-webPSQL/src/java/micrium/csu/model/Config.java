package micrium.csu.model;

import java.io.Serializable;

public class Config implements Serializable {

    private Integer configId;
    private String nombre;
    private String saludoInicial;
    private String descripcion;
    private String mostrarDpi;
    private String mostrarAcumuladosMegas;
    private String mostrarVigencia;
    private String mostrarBilleterasNoConfig;
    private String habilitado;
    private String estado;
    private String StrCos;
    private String lineas;
    private String nombreAcumulado;
    private String mostrarSiempre;

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer dato) {
        this.configId = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getSaludoInicial() {
        return saludoInicial;
    }

    public void setSaludoInicial(String dato) {
        this.saludoInicial = dato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String dato) {
        this.descripcion = dato;
    }

    public String getMostrarDpi() {
        return mostrarDpi;
    }

    public void setMostrarDpi(String dato) {
        this.mostrarDpi = dato;
    }

    public String getMostrarVigencia() {
        return mostrarVigencia;
    }

    public void setMostrarVigencia(String dato) {
        this.mostrarVigencia = dato;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String dato) {
        this.habilitado = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public String getMostrarBilleterasNoConfig() {
        return mostrarBilleterasNoConfig;
    }

    public void setMostrarBilleterasNoConfig(String mostrarBilleterasNoConfig) {
        this.mostrarBilleterasNoConfig = mostrarBilleterasNoConfig;
    }

    public String getStrCos() {
        return StrCos;
    }

    public void setStrCos(String StrCos) {
        this.StrCos = StrCos;
    }

    public String getMostrarAcumuladosMegas() {
        return mostrarAcumuladosMegas;
    }

    public void setMostrarAcumuladosMegas(String mostrarAcumuladosMegas) {
        this.mostrarAcumuladosMegas = mostrarAcumuladosMegas;
    }

    public String getLineas() {
        return lineas;
    }

    public void setLineas(String lineas) {
        this.lineas = lineas;
    }

    public String getNombreAcumulado() {
        return nombreAcumulado;
    }

    public void setNombreAcumulado(String nombreAcumulado) {
        this.nombreAcumulado = nombreAcumulado;
    }

    public String getMostrarSiempre() {
        return mostrarSiempre;
    }

    public void setMostrarSiempre(String mostrarSiempre) {
        this.mostrarSiempre = mostrarSiempre;
    }

    
    
}
