/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class ConfigAcumulado implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer configId;
    private Integer billeteraId;
    private Integer configAcumuladoId;
    private String nombreComercial;
    private String nombreAcumulado;
    private String segundaFechaExp;
    public String getSegundaFechaExp() {
        return segundaFechaExp;
    }
    public void setSegundaFechaExp(String segundaFechaExp) {
        this.segundaFechaExp = segundaFechaExp;
    }
    public Integer getBilleteraId() {
        return billeteraId;
    }

    public Integer getConfigId() {
        return configId;
    }

    public String getNombreAcumulado() {
        return nombreAcumulado;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setBilleteraId(Integer billeteraId) {
        this.billeteraId = billeteraId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public void setNombreAcumulado(String nombreAcumulado) {
        this.nombreAcumulado = nombreAcumulado;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public void setConfigAcumuladoId(Integer configAcumuladoId) {
        this.configAcumuladoId = configAcumuladoId;
    }

    public Integer getConfigAcumuladoId() {
        return configAcumuladoId;
    }

}
