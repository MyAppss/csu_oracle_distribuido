/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Leandro
 */
public class Menu implements Serializable {

    private Integer menuId;
    private String nombre;
    private Integer posicion;
    private String visible;
    private String estado;
    private Integer nivel;
    private Integer padre;
   // private Integer categoriaId;
    private String descripcion;
    private String mostarMayorCero;
    private String mostarMayorCeroAcumulado;
    private String canal;
    private String padreString;
    private String informacion;

    private Integer configuracionId;
    private String StrBilleteras;
    private String tieneHijos;
    private Integer unitTypeId;
    private String tieneBilletera;

    public Menu() {
    }

    public String getTieneBilletera() {
        return tieneBilletera;
    }

    public void setTieneBilletera(String tieneBilletera) {
        this.tieneBilletera = tieneBilletera;
    }
    
    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }
    
    public Integer getMenuId() {
        return menuId;
    }

    public String getTieneHijos() {
        return tieneHijos;
    }

    public void setTieneHijos(String tieneHijos) {
        this.tieneHijos = tieneHijos;
    }
    
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getPadre() {
        return padre;
    }

    public void setPadre(Integer padre) {
        this.padre = padre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMostarMayorCero() {
        return mostarMayorCero;
    }

    public void setMostarMayorCero(String mostarMayorCero) {
        this.mostarMayorCero = mostarMayorCero;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPadreString() {
        return padreString;
    }

    public void setPadreString(String padreString) {
        this.padreString = padreString;
    }

    public String getStrBilleteras() {
        return StrBilleteras;
    }

    public void setStrBilleteras(String StrBilleteras) {
        this.StrBilleteras = StrBilleteras;
    }

    public Integer getConfiguracionId() {
        return configuracionId;
    }

    public void setConfiguracionId(Integer configuracionId) {
        this.configuracionId = configuracionId;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String getMostarMayorCeroAcumulado() {
        return mostarMayorCeroAcumulado;
    }

    public void setMostarMayorCeroAcumulado(String mostarMayorCeroAcumulado) {
        this.mostarMayorCeroAcumulado = mostarMayorCeroAcumulado;
    }
    
    
}
