/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class DetalleCabecera implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer cabeceraDetalleId;
    private Integer cabeceraId;
    private Integer billeteraId;
    private String estado;
    private String nombreBilletera;
    private String nombreComverseBilletera;
    private Integer order;
    public DetalleCabecera() {
        estado = "t";
        order=0;
    }
    

    public DetalleCabecera(Integer cabeceraDetalleId, Integer cabeceraId,
            Integer billeteraId, String estado) {
        this.cabeceraDetalleId = cabeceraDetalleId;
        this.cabeceraId = cabeceraId;
        this.billeteraId = billeteraId;
        this.estado = estado;
        this.order=0;
    }

    public Integer getCabeceraDetalleId() {
        return cabeceraDetalleId;
    }

    public void setCabeceraDetalleId(Integer cabeceraDetalleId) {
        this.cabeceraDetalleId = cabeceraDetalleId;
    }

    public Integer getCabeceraId() {
        return cabeceraId;
    }

    public void setCabeceraId(Integer cabeceraId) {
        this.cabeceraId = cabeceraId;
    }

    public Integer getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(Integer billeteraId) {
        this.billeteraId = billeteraId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombreBilletera() {
        return nombreBilletera;
    }

    public void setNombreBilletera(String nombreBilletera) {
        this.nombreBilletera = nombreBilletera;
    }

    public String getNombreComverseBilletera() {
        return nombreComverseBilletera;
    }

    public void setNombreComverseBilletera(String nombreComverseBilletera) {
        this.nombreComverseBilletera = nombreComverseBilletera;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
