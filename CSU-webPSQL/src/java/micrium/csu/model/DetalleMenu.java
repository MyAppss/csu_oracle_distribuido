/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class DetalleMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer menuDetalleId;
    private Integer menuId;
    private Integer billeteraId;
    private String estado;
    private String nombreBilletera;
    private String nombreComverseBilletera;
    private Integer order;
    public DetalleMenu() {
        estado = "t";
        this.order=0;
    }

    public DetalleMenu(Integer cabeceraDetalleId, Integer cabeceraId,
            Integer billeteraId, String estado) {
        this.menuDetalleId = cabeceraDetalleId;
        this.menuId = cabeceraId;
        this.billeteraId = billeteraId;
        this.estado = estado;
        this.order=0;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
    
    public Integer getMenuDetalleId() {
        return menuDetalleId;
    }

    public void setMenuDetalleId(Integer menuDetalleId) {
        this.menuDetalleId = menuDetalleId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(Integer billeteraId) {
        this.billeteraId = billeteraId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombreBilletera() {
        return nombreBilletera;
    }

    public void setNombreBilletera(String nombreBilletera) {
        this.nombreBilletera = nombreBilletera;
    }

    public String getNombreComverseBilletera() {
        return nombreComverseBilletera;
    }

    public void setNombreComverseBilletera(String nombreComverseBilletera) {
        this.nombreComverseBilletera = nombreComverseBilletera;
    }
    

}
