/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author HP
 */
public class Nodo implements Serializable{
    
   private Integer id_nodo;
   private String url;
   private String detalle;
   private String estado;

    public Integer getNodo_id() {
        return id_nodo;
    }

    public void setNodo_id(Integer nodo_id) {
        this.id_nodo = nodo_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }



    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Nodo{" + "nodo_id=" + id_nodo + ", ip=" + url + ", detalle=" + detalle + ", estado=" + estado + '}';
    }
   
    
}
