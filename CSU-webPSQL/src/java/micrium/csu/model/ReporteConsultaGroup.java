/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ALI
 */
public class ReporteConsultaGroup  implements Serializable{
    private ReporteConsulta group;
    private List<ReporteConsulta> childrens;
   

    public ReporteConsultaGroup(ReporteConsulta group, List<ReporteConsulta> items) {
        this.group = group;
        this.childrens = items;
    }
    
    public ReporteConsulta getGroup() {
        return group;
    }

    public List<ReporteConsulta> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<ReporteConsulta> childrens) {
        this.childrens = childrens;
    }

    

    public void setGroup(ReporteConsulta group) {
        this.group = group;
    }

   
    
    
    
}
