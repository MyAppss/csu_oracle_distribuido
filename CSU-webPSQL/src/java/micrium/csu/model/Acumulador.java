/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class Acumulador implements Serializable {

    private Integer acumuladorId;
    private String nombre;
    private Integer limite;
    private String estado;

    public Integer getAcumuladorId() {
        return acumuladorId;
    }

    public void setAcumuladorId(Integer acumuladorId) {
        this.acumuladorId = acumuladorId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getLimite() {
        return limite;
    }

    public void setLimite(Integer limite) {
        this.limite = limite;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
