package micrium.csu.model;

import java.io.Serializable;

public class Parametro implements Serializable {

    private String nombre;
    private String valor;
    private String descripcion;
    private String estado;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String dato) {
        this.nombre = dato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String dato) {
        this.valor = dato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String dato) {
        this.descripcion = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

}
