/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author HP
 */
public class MapeoOfertas implements Serializable {

    private Integer id_mapeo;
    private Integer offeringid;
    private Integer offeringparent;
    private String offeringname;
    private String type;
    private String offeringcode;
    private Integer paymentmode;
    private String estado;

    public Integer getId_mapeo() {
        return id_mapeo;
    }

    public void setId_mapeo(Integer id_mapeo) {
        this.id_mapeo = id_mapeo;
    }

    public Integer getOfferingid() {
        return offeringid;
    }

    public void setOfferingid(Integer offeringid) {
        this.offeringid = offeringid;
    }

    public Integer getOfferingparent() {
        return offeringparent;
    }

    public void setOfferingparent(Integer offeringparent) {
        this.offeringparent = offeringparent;
    }

    public String getOfferingname() {
        return offeringname;
    }

    public void setOfferingname(String offeringname) {
        this.offeringname = offeringname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOfferingcode() {
        return offeringcode;
    }

    public void setOfferingcode(String offeringcode) {
        this.offeringcode = offeringcode;
    }

    public Integer getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(Integer paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

}
