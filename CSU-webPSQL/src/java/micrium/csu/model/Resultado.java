/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class Resultado {
    
    private String Linea;
    private String codigo;
    private String mensaje;
    private ArrayList<Pantalla> pantallas;

    /**
     * @return the Linea
     */
    public String getLinea() {
        return Linea;
    }

    /**
     * @param Linea the Linea to set
     */
    public void setLinea(String Linea) {
        this.Linea = Linea;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the pantallas
     */
    public ArrayList<Pantalla> getPantallas() {
        return pantallas;
    }

    /**
     * @param pantallas the pantallas to set
     */
    public void setPantallas(ArrayList<Pantalla> pantallas) {
        this.pantallas = pantallas;
    }
    
    
    
}
