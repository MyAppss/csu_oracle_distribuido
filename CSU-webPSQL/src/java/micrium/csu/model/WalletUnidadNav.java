/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author toshiba satelite
 */
public class WalletUnidadNav implements Serializable{
    private String id;
    private String nombre;
    
    public  WalletUnidadNav(){
        this.id = "";
        this.nombre = "";
    }

    public  WalletUnidadNav(String id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
