/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class Cabecera implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer cabeceraId;
    private String nombre;
    private String descripcion;
    private String estado;
    private Integer configuracionId;
    private String visible;
    private Integer posicion;
    private String tipoNavegacion;
    private String saldoCalculado;
    private String whatsappIlimitado;
    private Integer unitTypeId;
    private String StrBilleteras;

    public Cabecera() {
        this.configuracionId = 0;
        this.cabeceraId = 0;
        this.posicion = 0;
    }

    public Cabecera(Integer cabeceraId, String nombre, String descripcion,
            String estado, Integer configuracionId, String visible, Integer posicion, String tipoNavegacion, String saldoCalculado, String whatsappIlimitado) {
        this.cabeceraId = cabeceraId;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.configuracionId = configuracionId;
        this.visible = visible;
        this.posicion = posicion;
        this.tipoNavegacion = tipoNavegacion;
        this.saldoCalculado = saldoCalculado;
        this.whatsappIlimitado = whatsappIlimitado;
    }

    public void setUnitTypeId(Integer unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }

   
    public Integer getCabeceraId() {
        return cabeceraId;
    }

    public void setCabeceraId(Integer cabeceraId) {
        this.cabeceraId = cabeceraId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getConfiguracionId() {
        return configuracionId;
    }

    public void setConfiguracionId(Integer configuracionId) {
        this.configuracionId = configuracionId;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public String getStrBilleteras() {
        return StrBilleteras;
    }

    public void setStrBilleteras(String StrBilleteras) {
        this.StrBilleteras = StrBilleteras;
    }

    public String getTipoNavegacion() {
        return tipoNavegacion;
    }

    public void setTipoNavegacion(String tipoNavegacion) {
        this.tipoNavegacion = tipoNavegacion;
    }

    public String getSaldoCalculado() {
        return saldoCalculado;
    }

    public void setSaldoCalculado(String saldoCalculado) {
        this.saldoCalculado = saldoCalculado;
    }

    public String getWhatsappIlimitado() {
        return whatsappIlimitado;
    }

    public void setWhatsappIlimitado(String whatsappIlimitado) {
        this.whatsappIlimitado = whatsappIlimitado;
    }

    
}
