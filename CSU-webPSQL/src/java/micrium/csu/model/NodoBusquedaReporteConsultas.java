/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author usuario
 */
public class NodoBusquedaReporteConsultas implements Serializable {

    private String msisdn;
    private Date fechaIni;
    private Date fechaFin;
    private String sessionId;
    private Date fechaAux;

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSessionID() {
        return sessionId;
    }

    public void setSessionID(String sessionID) {
        this.sessionId = sessionID;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getFechaAux() {
        return fechaAux;
    }

    public void setFechaAux(Date fechaAux) {
        this.fechaAux = fechaAux;
    }

}
