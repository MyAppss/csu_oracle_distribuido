package micrium.csu.model;

import java.io.Serializable;


public class RolPaginaIni implements Serializable {

     private  Integer  rolId;
     private  String  paginaInicio;

   public Integer getRolId() {
        return rolId;
    }
   public void setRolId(Integer dato) {
        this.rolId=dato;
    }
   public String getPaginaInicio() {
        return paginaInicio;
    }
   public void setPaginaInicio(String dato) {
        this.paginaInicio=dato;
    }

}