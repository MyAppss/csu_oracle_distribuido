package micrium.csu.model;

import java.io.Serializable;


public class GrupoBilletera implements Serializable {

     private  Integer  billeteraId;
     private  Integer  composicionBilleteraId;
     private  String  estado;

   public Integer getBilleteraId() {
        return billeteraId;
    }
   public void setBilleteraId(Integer dato) {
        this.billeteraId=dato;
    }
   public Integer getComposicionBilleteraId() {
        return composicionBilleteraId;
    }
   public void setComposicionBilleteraId(Integer dato) {
        this.composicionBilleteraId=dato;
    }
   public String getEstado() {
        return estado;
    }
   public void setEstado(String dato) {
        this.estado=dato;
    }

}