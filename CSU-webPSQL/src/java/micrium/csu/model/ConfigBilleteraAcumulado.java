/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.io.Serializable;

/**
 *
 * @author Vehimar
 */
public class ConfigBilleteraAcumulado implements Serializable {
  private ConfigAcumulado configAcumulado;
  private Billetera billetera;

    public void setBilletera(Billetera billetera) {
        this.billetera = billetera;
    }

    public void setConfigAcumulado(ConfigAcumulado configAcumulado) {
        this.configAcumulado = configAcumulado;
    }

    public Billetera getBilletera() {
        return billetera;
    }

    public ConfigAcumulado getConfigAcumulado() {
        return configAcumulado;
    }
  
}
