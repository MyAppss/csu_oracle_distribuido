package micrium.csu.model;

import java.io.Serializable;


public class Origen implements Serializable {

     private  Integer  origenId;
     private  String  nombre;
     private  String  descripcion;
     private  String  estado;

   public Integer getOrigenId() {
        return origenId;
    }
   public void setOrigenId(Integer dato) {
        this.origenId=dato;
    }
   public String getNombre() {
        return nombre;
    }
   public void setNombre(String dato) {
        this.nombre=dato;
    }
   public String getDescripcion() {
        return descripcion;
    }
   public void setDescripcion(String dato) {
        this.descripcion=dato;
    }
   public String getEstado() {
        return estado;
    }
   public void setEstado(String dato) {
        this.estado=dato;
    }

}