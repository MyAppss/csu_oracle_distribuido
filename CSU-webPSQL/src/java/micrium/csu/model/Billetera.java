package micrium.csu.model;

import java.io.Serializable;

public class Billetera implements Serializable {

    private Integer billeteraId;
    private Integer unitTypeId;
    private String nombreComverse;
    private String nombreComercial;
    private String prefijoUnidad;
    private String operador;
    private double valor;
    private Integer cantidadDecimales;
    private double montoMinimo;
    private double montoMaximo;
    private String estado;
    private String alco;
    private Integer acumuladorId;
    private String textoIlimintado;
    private String acumulado;
    private String reserva;

    //-------------------------------------------------------------------------
    private String ConfBilletera_nombreComercial;

    //private boolean ConfigBilletera_mostrar_siempre;
    private boolean ConfigBilletera_mostrar_siempr;

    //private boolean ConfigBilletera_mostrar_saldo_mayor_cero;
    private boolean ConfBilletera_most_salo_may_ce;

    //private boolean ConfigBilletera_mostrar_saldo_menor_cero;
    private boolean ConfigBilletera_most_sald_men_ce;

    //private boolean ConfigBilletera_mostrar_saldo_cero;
    private boolean ConfBilletera_mostrar_saldo_ce;

    //private boolean ConfigBilletera_no_mostrar_saldo_expirado;
    private boolean ConfBilletera_no_most_sal_exp;

    //private boolean ConfigBilletera_mostrar_vigencia;
    private boolean ConfBilletera_mostrar_vigencia;

    //private boolean ConfigBilletera_mostrar_saldo_expirado;
    private boolean ConfBilletera_mostrar_saldo_ex;

    private int ConfigBilletera_posicion;
    private boolean newForConfig;

    //-------------------------------------------------------------------------
    private boolean mostrarSegundaFechaExp;

    private boolean mostrarHoraSegundaFechaExp;
    private boolean asumirFormatoHoraPrimeraFecha;

    public Billetera() {
        newForConfig = false;
        alco = "false";
    }

    public String getReserva() {
        return reserva;
    }

    public void setReserva(String reserva) {
        this.reserva = reserva;
    }
    
    public String getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(String acumulado) {
        this.acumulado = acumulado;
    }
    

    public Integer getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(Integer dato) {
        this.billeteraId = dato;
    }

    public Integer getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(Integer dato) {
        this.unitTypeId = dato;
    }

    public String getNombreComverse() {
        return nombreComverse;
    }

    public void setNombreComverse(String dato) {
        this.nombreComverse = dato;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String dato) {
        this.nombreComercial = dato;
    }

    public String getPrefijoUnidad() {
        return prefijoUnidad;
    }

    public void setPrefijoUnidad(String dato) {
        this.prefijoUnidad = dato;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String dato) {
        this.operador = dato;
    }

    public void setValor(Integer dato) {
        this.valor = dato;
    }

    public Integer getCantidadDecimales() {
        return cantidadDecimales;
    }

    public void setCantidadDecimales(Integer dato) {
        this.cantidadDecimales = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public double getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(double montoMaximo) {
        this.montoMaximo = montoMaximo;
    }

    public double getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(double montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isConfigBilletera_mostrar_saldo_cero() {
        return ConfBilletera_mostrar_saldo_ce;
    }

    public void setConfigBilletera_mostrar_saldo_cero(boolean ConfigBilletera_mostrar_saldo_cero) {
        this.ConfBilletera_mostrar_saldo_ce = ConfigBilletera_mostrar_saldo_cero;
    }

    public boolean isConfigBilletera_no_mostrar_saldo_expirado() {
        return ConfBilletera_no_most_sal_exp;
    }

    public void setConfigBilletera_no_mostrar_saldo_expirado(boolean ConfigBilletera_no_mostrar_saldo_expirado) {
        this.ConfBilletera_no_most_sal_exp = ConfigBilletera_no_mostrar_saldo_expirado;
    }

    public boolean isConfigBilletera_mostrar_saldo_mayor_cero() {
        return ConfBilletera_most_salo_may_ce;
    }

    public void setConfigBilletera_mostrar_saldo_mayor_cero(boolean ConfigBilletera_mostrar_saldo_mayor_cero) {
        this.ConfBilletera_most_salo_may_ce = ConfigBilletera_mostrar_saldo_mayor_cero;
    }

    public boolean isConfigBilletera_mostrar_saldo_menor_cero() {
        return ConfigBilletera_most_sald_men_ce;
    }

    public void setConfigBilletera_mostrar_saldo_menor_cero(boolean ConfigBilletera_mostrar_saldo_menor_cero) {
        this.ConfigBilletera_most_sald_men_ce = ConfigBilletera_mostrar_saldo_menor_cero;
    }

    public boolean isConfigBilletera_mostrar_siempre() {
        return ConfigBilletera_mostrar_siempr;
    }

    public void setConfigBilletera_mostrar_siempre(boolean ConfigBilletera_mostrar_siempre) {
        this.ConfigBilletera_mostrar_siempr = ConfigBilletera_mostrar_siempre;
    }

    public String getConfigBilletera_nombreComercial() {
        return ConfBilletera_nombreComercial;
    }

    public void setConfigBilletera_nombreComercial(String ConfigBilletera_nombreComercial) {
        this.ConfBilletera_nombreComercial = ConfigBilletera_nombreComercial;
    }

    public boolean isNewForConfig() {
        return newForConfig;
    }

    public void setNewForConfig(boolean newForConfig) {
        this.newForConfig = newForConfig;
    }

    public int getConfigBilletera_posicion() {
        return ConfigBilletera_posicion;
    }

    public void setConfigBilletera_posicion(int ConfigBilletera_posicion) {
        this.ConfigBilletera_posicion = ConfigBilletera_posicion;
    }

    public boolean isConfigBilletera_mostrar_vigencia() {
        return ConfBilletera_mostrar_vigencia;
    }

    public void setConfigBilletera_mostrar_vigencia(boolean ConfigBilletera_mostrar_vigencia) {
        this.ConfBilletera_mostrar_vigencia = ConfigBilletera_mostrar_vigencia;
    }

    public String getAlco() {
        return alco;
    }

    public void setAlco(String alco) {
        this.alco = alco;
    }

    public Integer getAcumuladorId() {
        return acumuladorId;
    }

    public void setAcumuladorId(Integer acumuladorId) {
        this.acumuladorId = acumuladorId;
    }

    public boolean isConfigBilletera_mostrar_saldo_expirado() {
        return ConfBilletera_mostrar_saldo_ex;
    }

    public void setConfigBilletera_mostrar_saldo_expirado(boolean ConfigBilletera_mostrar_saldo_expirado) {
        this.ConfBilletera_mostrar_saldo_ex = ConfigBilletera_mostrar_saldo_expirado;
    }

    public boolean isMostrarSegundaFechaExp() {
        return mostrarSegundaFechaExp;
    }

    public void setMostrarSegundaFechaExp(boolean mostrarSegundaFechaExp) {
        this.mostrarSegundaFechaExp = mostrarSegundaFechaExp;
    }

    public boolean isMostrarHoraSegundaFechaExp() {
        return mostrarHoraSegundaFechaExp;
    }

    public void setMostrarHoraSegundaFechaExp(boolean mostrarHoraSegundaFechaExp) {
        this.mostrarHoraSegundaFechaExp = mostrarHoraSegundaFechaExp;
    }

    public boolean isAsumirFormatoHoraPrimeraFecha() {
        return asumirFormatoHoraPrimeraFecha;
    }

    public void setAsumirFormatoHoraPrimeraFecha(boolean asumirFormatoHoraPrimeraFecha) {
        this.asumirFormatoHoraPrimeraFecha = asumirFormatoHoraPrimeraFecha;
    }


    public String getTextoIlimintado() {
        return textoIlimintado;
    }

    public void setTextoIlimintado(String textoIlimintado) {
        this.textoIlimintado = textoIlimintado;
    }
    
    /*
        Vehimar Lopez Terrazas
    */

}
