package micrium.csu.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class ReporteConsulta implements Serializable {

    private Timestamp fecha;
    private String msisdn;
    private String identificadorServicio;
    private String nombreCanal;
    private Boolean publicidadSolicitada;
    private Integer longitudMaxSolicitada;
    private String ipCliente;
    private String textoGenerado;
    private String sessionId;
    private String opcion;

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp dato) {
        this.fecha = dato;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String dato) {
        this.msisdn = dato;
    }

    public String getIdentificadorServicio() {
        return identificadorServicio;
    }

    public void setIdentificadorServicio(String dato) {
        this.identificadorServicio = dato;
    }

    public String getNombreCanal() {
        return nombreCanal;
    }

    public void setNombreCanal(String dato) {
        this.nombreCanal = dato;
    }

    public Boolean getPublicidadSolicitada() {
        return publicidadSolicitada;
    }

    public void setPublicidadSolicitada(Boolean dato) {
        this.publicidadSolicitada = dato;
    }

    public Integer getLongitudMaxSolicitada() {
        return longitudMaxSolicitada;
    }

    public void setLongitudMaxSolicitada(Integer dato) {
        this.longitudMaxSolicitada = dato;
    }

    public String getIpCliente() {
        return ipCliente;
    }

    public void setIpCliente(String dato) {
        this.ipCliente = dato;
    }

    public String getTextoGenerado() {
        return textoGenerado;
    }

    public void setTextoGenerado(String dato) {
        this.textoGenerado = dato;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
