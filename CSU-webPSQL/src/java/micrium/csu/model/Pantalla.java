/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.model;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class Pantalla {

    private int idPantalla;
    private String cabecera;
    private String opciones;
    private ArrayList<Integer> Listaopcion;
    private ArrayList<Navega> listaNavegacion;
    private Integer pantallaPadre;
    private Integer opcionPadre;

    public Pantalla(String string) {
        
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    /**
     * @return the opciones
     */
    public String getOpciones() {
        return opciones;
    }

    /**
     * @param opciones the opciones to set
     */
    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public Integer getPantallaPadre() {
        return pantallaPadre;
    }

    public void setPantallaPadre(Integer pantallaPadre) {
        this.pantallaPadre = pantallaPadre;
    }

    /**
     * @return the opcionPadre
     */
    public Integer getOpcionPadre() {
        return opcionPadre;
    }

    /**
     * @param opcionPadre the opcionPadre to set
     */
    public void setOpcionPadre(Integer opcionPadre) {
        this.opcionPadre = opcionPadre;
    }

    public ArrayList<Integer> getListaopcion() {
        return Listaopcion;
    }

    public void setListaopcion(ArrayList<Integer> Listaopcion) {
        this.Listaopcion = Listaopcion;
    }

    public int getIdPantalla() {
        return idPantalla;
    }

    public void setIdPantalla(int idPantalla) {
        this.idPantalla = idPantalla;
    }

    public ArrayList<Navega> getListaNavegacion() {
        return listaNavegacion;
    }

    public void setListaNavegacion(ArrayList<Navega> listaNavegacion) {
        this.listaNavegacion = listaNavegacion;
    }
}
