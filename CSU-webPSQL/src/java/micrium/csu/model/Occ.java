package micrium.csu.model;

import java.io.Serializable;

public class Occ  implements Serializable {

    private Integer occId;
    private Integer configId;
    private Integer origenId;
    private Integer cortoId;
    private Integer cosId;
    private Integer posicion;
    private String estado;

    public Integer getOccId() {
        return occId;
    }

    public void setOccId(Integer dato) {
        this.occId = dato;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer dato) {
        this.configId = dato;
    }

    public Integer getOrigenId() {
        return origenId;
    }

    public void setOrigenId(Integer dato) {
        this.origenId = dato;
    }

    public Integer getCortoId() {
        return cortoId;
    }

    public void setCortoId(Integer dato) {
        this.cortoId = dato;
    }

    public Integer getCosId() {
        return cosId;
    }

    public void setCosId(Integer dato) {
        this.cosId = dato;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String dato) {
        this.estado = dato;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }
    
    
}