package micrium.csu.filter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import micrium.csu.util.Parameters;
import micrium.csu.util.UtilDate;



import org.apache.log4j.Logger;

/**
 * @author alepaco.maton
 */
public class RefererFilter implements Filter {

    public static final Logger log = Logger.getLogger(RefererFilter.class);

    // the domains that you will accept a referrer from
    private static List<String> acceptableDomains;

    String context = "";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        context = filterConfig.getServletContext().getContextPath();

        acceptableDomains = Arrays.asList(Parameters.dominios.split(";"));
        log.debug(acceptableDomains.toString());
    }

   @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        HttpServletResponse response = (HttpServletResponse) servletResponse;


        String host = request.getHeader("Host");
        if (host != null) {
            log.debug("-----------------------HOST: " + host + " -----------------------");
            boolean hostValido = false;
            for (String subDomain : acceptableDomains) {
                if (host.contains(subDomain)) {
                    //  log.debug("sub dominio que machea " + subDomain);
                    hostValido = true;
                    break;
                }
            }

            if (!hostValido) {
                log.info("-----------------------HOST: " + host + "  NO VALIDO-----------------------");
                request.getSession().invalidate();
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }

      /*  String path = request.getRequestURI();

        log.debug("Method " + request.getMethod() + ",  path " + path);

        log.debug("parametros ***************************");
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            String key = entry.getKey();
            String[] value = entry.getValue();
            log.debug("key " + key);
            String values = "";
            for (String val : value) {
                values = values + val + ", ";
            }
            log.debug("value  " + values);
        }

        if ((path.equals(context + "/view/login.xhtml") && "GET".equals(request.getMethod()))
                || (path.equals(context + "/view/dashboard.xhtml") && "GET".equals(request.getMethod()))) {
            /// ?formLogin=formLogin&formLogin%3AihIp=&formLogin%3Aj_idt11=appsc
           
            if (request.getParameterMap().size() > 0) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
//            log.warn("Method " + request.getMethod() + ",   path " + path);

        }
*/
        try {
            String usuario = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            String refererHeader = request.getHeader("referer");
            if (refererHeader == null && usuario == null) {
                filterChain.doFilter(request, response);
                return;
            }

            // no need to continue if the header is missing


            /*if (refererHeader == null && usuario != null && usuario.length() > 0) {
                //validar volver atras
                if (request.getRequestURI().contains("login.xhtml")) {
                    log.debug("Atras");
                } else {
                    request.getSession().invalidate();
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
            if (refererHeader == null) {
                request.getSession().invalidate();
                filterChain.doFilter(request, response);
                return;
            }*/

            if (refererHeader == null) {
                refererHeader = UtilDate.getClientIp(request);
            }


            // parse the given referrer
            URL refererURL = new URL(refererHeader);
            // split the host name by the '.' character (but quote that as it is a regex special char)
            String[] hostParts = refererURL.getHost().split(Pattern.quote("."));

            StringBuilder temp = new StringBuilder();
            for (String hostPart : hostParts) {
                temp.append(hostPart).append(".");
            }

            String dominio = temp.toString().substring(0, temp.length() - 1);


            boolean existeDominio = false;
            for (String subDomain : acceptableDomains) {
                if (dominio.endsWith(subDomain)) {
                    //  log.debug("sub dominio que machea " + subDomain);
                    existeDominio = true;
                    break;
                }
            }

            if (!existeDominio) {
                log.warn("posible ataque refererHeader " + refererHeader + ", Dominio invalido, " + dominio);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            // if we've gotten this far then the domain is ok, how about the path and query?
//        if (!(refererURL.getPath() + "?" + refererURL.getQuery()).equals(PATH)) {
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            return;
//        }


            // all tests pass - continue filter chain
            filterChain.doFilter(request, response);
        } catch (MalformedURLException url) {
            filterChain.doFilter(request, response);
        } catch (IOException | ServletException e) {
            log.error("Error al validar el referer " + e.getMessage(), e);
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}

