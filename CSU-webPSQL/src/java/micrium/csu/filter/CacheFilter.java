/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author toshiba satelite
 */
//@WebFilter(filterName = "CacheFilter", urlPatterns = {"/*"})
public class CacheFilter implements Filter {

    public static final Logger log = Logger.getLogger(CacheFilter.class);

    private static final boolean debug = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public CacheFilter() {
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
//        if (debug) {
//            log("CacheFilter:DoBeforeProcessing");
//        }

        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        HttpSession session = ((HttpServletRequest) request).getSession();
        HttpServletRequest req = (HttpServletRequest) request;
        Object user = session.getAttribute("TEMP$USER_NAME");
        if (request.getContentLength() != -1) {
            Map<String, Object> values = new HashMap<>();
            for (String name : session.getValueNames()) {
                values.put(name, session.getAttribute(name));
            }

            session.invalidate();
            session = req.getSession(true);
            for (Map.Entry<String, Object> e : values.entrySet()) {
                session.setAttribute(e.getKey(), e.getValue());
            }
//            log("Requested_SessionId Renovado:" + session.getId());

//            if (user != null) {
//                HttpServletResponse hres = (HttpServletResponse) response;
//                hres.sendRedirect(rederingMenu);
//            }
        }
        String path = req.getRequestURI();
        if ((user == null) && (request.getContentLength() == -1) && path.contains("/view/Login.xhtml")) {
            session.invalidate();
        }

    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
//            log("CacheFilter:DoAfterProcessing");
        }

    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (debug) {
//            log("CacheFilter:doFilter()");
        }

//        doBeforeProcessing(request, response);
        Throwable problem = null;
        try {
            ////////////////////////////////////////////
//            HttpServletRequest req = (HttpServletRequest) request;
//            HttpServletResponse httpResponse = (HttpServletResponse) response;
//            httpResponse.setHeader("x-ua-compatible", "IE=8");
//            httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
//            httpResponse.setHeader("Pragma", "no-cache");
//            httpResponse.setHeader("Strict-Transport-Security", "max-age=31622400; includeSubDomains");
////            httpResponse.setHeader("Content-Security-Policy", "default-src 'self'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';");
//            httpResponse.setHeader("X-Content-Type-Options", "nosniff");
//            httpResponse.setDateHeader("X-XSS-Protection", 0);
//            httpResponse.setDateHeader("Expires", 0);
            HttpServletRequest httpRequest = (HttpServletRequest) request;
//            log.info("path: " + httpRequest.getRequestURI());

            HttpServletResponse httpResponse = (HttpServletResponse) response;
            // httpResponse.setHeader("x-ua-compatible", "IE=8");
            httpResponse.setHeader("X-UA-Compatible", "IE=edge");
            httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            httpResponse.setHeader("Pragma", "no-cache");
            httpResponse.setHeader("Strict-Transport-Security", "max-age=31622400; includeSubDomains");
            /*httpResponse.setHeader("Set-Cookie", "JSESSIONID=" + httpRequest.getSession().getId() + ";"
                    + "Domain=" + httpRequest.getContextPath() + "/;"
                    + "Path=/;"
                    + "HttpOnly;"
                    + "Secure;"
                    + "SameSite=Strict");
             */
            // httpResponse.setHeader("Content-Security-Policy", "default-src 'self'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';");
            if (request.getScheme() != null && request.getScheme().toLowerCase().trim().equals("https")) {
//			httpResponse.setHeader("Content-Security-Policy", "default-src https:; connect-src https:; font-src https: data:; frame-src https:; frame-ancestors https:; img-src https: data:; media-src https:; object-src https:; script-src 'unsafe-inline' 'unsafe-eval' https:; style-src 'unsafe-inline' https:;");
                //v1 httpResponse.setHeader("Content-Security-Policy", "default-src https:; connect-src https:; font-src https: data:; frame-src https:; frame-ancestors https:; img-src https: data:; media-src https:; object-src https:; script-src 'unsafe-inline' 'unsafe-eval' https:; style-src 'unsafe-inline' https:;");
                 httpResponse.setHeader("Content-Security-Policy", "default-src 'self'   https://ajax.googleapis.com https://fonts.gstatic.com https://fonts.googleapis.com https://maps.google.com https://maps.googleapis.com https://maps.gstatic.com https://firebaseinstallations.googleapis.com https://www.gstatic.com https://fcmregistrations.googleapis.com  https:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https:; style-src 'self' 'unsafe-inline' 'unsafe-eval' https://ajax.googleapis.com https://fonts.gstatic.com https://fonts.googleapis.com https://maps.google.com https://maps.googleapis.com https://maps.gstatic.com https://fcmregistrations.googleapis.com  https:; connect-src 'self' 'unsafe-inline' 'unsafe-eval' https://ajax.googleapis.com https://fonts.gstatic.com https://fonts.googleapis.com https://maps.google.com https://maps.gstatic.com https://maps.googleapis.com https://firebaseinstallations.googleapis.com https://www.gstatic.com https://fcmregistrations.googleapis.com ; font-src 'self' 'unsafe-inline' 'unsafe-eval' https://ajax.googleapis.com https://fonts.gstatic.com https://fonts.googleapis.com https://maps.google.com https://maps.gstatic.com https://maps.googleapis.com   https:; img-src 'self' data: 'unsafe-inline' 'unsafe-eval' https://ajax.googleapis.com https://fonts.gstatic.com https://fonts.googleapis.com https://maps.google.com https://maps.gstatic.com https://maps.googleapis.com   https://maps.gstatic.com  https://*.googleapis.com https://www.gstatic.com https://*.ggpht.com https:; frame-ancestors 'none'; object-src 'self';");
               // httpResponse.setHeader("Content-Security-Policy", "default-src 'self' https:; script-src 'self' 'unsafe-inline' https:; style-src 'self' 'unsafe-inline' 'unsafe-eval' https:; connect-src 'self' 'unsafe-inline' 'unsafe-eval' ; font-src 'self' 'unsafe-inline' 'unsafe-eval' https:; img-src 'self' data: 'unsafe-inline' 'unsafe-eval' https:; frame-ancestors 'none'; object-src 'none';");

            }

            httpResponse.setHeader("X-Content-Type-Options", "nosniff");
//		httpResponse.setDateHeader("X-XSS-Protection", 0);
            httpResponse.setHeader("X-XSS-Protection", "1; mode=block");
            httpResponse.setDateHeader("Expires", 0);
            httpResponse.setHeader("X-Frame-Options", "DENY");
            httpResponse.setHeader("Referrer-Policy", "origin");
            httpResponse.setHeader("Referrer", "strict-origin-when-cross-origin");

            HttpServletResponse respon = (HttpServletResponse) response;

            Cookie[] listaCookie = httpRequest.getCookies();
            if (listaCookie != null) {
                for (Cookie cookie : listaCookie) {
                    cookie.setDomain(httpRequest.getContextPath() + "/");
                    cookie.setPath("/");
                    cookie.setHttpOnly(true);
                    cookie.setSecure(true);

                }

            }

            Cookie cookie = new Cookie("JSESSIONID", httpRequest.getSession().getId());

            cookie.setDomain(httpRequest.getContextPath() + "/");
            cookie.setPath("/");
            if (request.getScheme() != null && request.getScheme().toLowerCase().trim().equals("https")) {
                cookie.setHttpOnly(true);
                cookie.setSecure(true);

            }
            respon.addCookie(cookie);

            ///////////////////////////////////////////
            chain.doFilter(request, response);

        } catch (IOException | ServletException e) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            String contextPath = httpRequest.getContextPath();
            httpResponse.sendRedirect(contextPath); // So, just perform standard synchronous redirect.
            log.info("Redireccionando por error " + e.getMessage());
        } catch (Throwable t) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            log.info("Throwable: " + t.getMessage());
            //problem = t;
            //t.printStackTrace();
        }

//        doAfterProcessing(request, response);
        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        /* if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            
//            sendProcessingError(problem, response);
        }*/
    }

    /**
     * Return the filter configuration object for this filter.
     *
     * @return the filterConfig
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
//                log("CacheFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("CacheFilter()");
        }
        StringBuilder sb = new StringBuilder("CacheFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

//    private void sendProcessingError(Throwable t, ServletResponse response) {
//        String stackTrace = getStackTrace(t);
//
//        if (stackTrace != null && !stackTrace.equals("")) {
//            try {
//                response.setContentType("text/html");
//                PrintStream ps = new PrintStream(response.getOutputStream(),true,"UTF-8");
//                PrintWriter pw = new PrintWriter(ps);
//                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N
//
//                // PENDING! Localize this for next official release
//                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
//                pw.print(stackTrace);
//                pw.print("</pre></body>\n</html>"); //NOI18N
//                pw.close();
//                ps.close();
//                response.getOutputStream().close();
//            } catch (Exception ex) {
//                log("ERROR:"+ ex.getMessage());
//            }
//        } else {
//            try {
//                PrintStream ps = new PrintStream(response.getOutputStream(),true,"UTF-8");
//                t.printStackTrace(ps);
//                ps.close();
//                response.getOutputStream().close();
//            } catch (Exception ex) {
//                log("ERROR:"+ ex.getMessage());
//            }
//        }
//    }
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (IOException ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
