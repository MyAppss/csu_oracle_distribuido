package micrium.csu.filter;

import com.myapps.controlmenu.controler.ControlTimeOut;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import com.myapps.controlmenu.util.ParametersCM;
import java.io.IOException;
import java.net.URL;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.PropertyConfigurator;

import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;

public class loginFilter implements Filter {

    private FilterConfig filterConfig = null;

    private static final String excludeLogin = "/view/Login.xhtml";
    private static final String rederingLogin = "/consultadesaldo/view/Login.xhtml";
    private static final String rederingMenu = "/consultadesaldo/view/Menu.xhtml";


    private ControlTimeOut controler = GuiceInjectorSingletonCM.getInstance(ControlTimeOut.class);
    private static final Logger log = Logger.getLogger(loginFilter.class);

    public loginFilter() {

        initLog4J();
        ParametersCM.HOST = Parameters.BD_server;//; "127.0.0.1";
        ParametersCM.DB = Parameters.BD_name;//"tigoTv";
        ParametersCM.PORT = Parameters.BD_port;//"5432";
        ParametersCM.USER = Parameters.BD_user;//"postgres";
        ParametersCM.PASSWORD = Parameters.BD_pass;//"123";

        ParametersCM.LOGIN = Parameters.login;//"123";
        ParametersCM.PASSWORD_USER = Parameters.password;//"123";

        ParametersCM.NRO_INTENTOS = Integer.parseInt(Parameters.nroOption);
        ParametersCM.MINUTOS_FUERA = Integer.parseInt(Parameters.timeOut);

        ParametersCM.DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = Parameters.BD_setminevictableidletimemillis;
        ParametersCM.DB_MAX_ACTIVE = Parameters.BD_setmaxactive;
        ParametersCM.DB_MIN_IDLE = Parameters.BD_setminidle;
        ParametersCM.DB_MAX_IDLE = Parameters.BD_setMaxIdle;
        ParametersCM.DB_MAX_WAIT = Parameters.BD_setMaxWait;

        controler.addRutaBase(excludeLogin);
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        Throwable problem = null;
        request.setCharacterEncoding("UTF-8");

        HttpSession session = ((HttpServletRequest) request).getSession();
        HttpServletRequest req = (HttpServletRequest) request;
       

        String strIdUs = (String) req.getSession().getAttribute("TEMP$USER_NAME");
        log.debug("-----------------------------------------------------");
        String path = req.getServletPath();
        log.debug("PATCH  " + path);
        try {
            if (path.equals(excludeLogin)) {
                log.debug("ENTRO A EXCLUSION");

                if (strIdUs == null) {
                    log.debug("REQUEST " + request.getContentLength());

                    if (request.getContentLength() == -1) {
                        chain.doFilter(request, response);
                        log.debug("REQUEST VALIDO");
                    } else {
                        if (session.getCreationTime() == session.getLastAccessedTime()) {
                            log.debug("REDIRIGIENDO");
                            session.invalidate();
                            HttpServletResponse hres = (HttpServletResponse) response;
                            hres.sendRedirect(rederingLogin);
                        }else {
                            log.debug("CHAIN");
                                chain.doFilter(request, response);
                        }
                    }
                } else {
                    HttpServletResponse hres = (HttpServletResponse) response;
                    hres.sendRedirect(rederingMenu);
                }
                return;
            }

            if (strIdUs != null) {

                String addressIP = request.getRemoteAddr();
//                log.info("+++++++++++++ ip: " + addressIP);
//                log.info("+++++++++++++ strIdUs: " + strIdUs);
                String addressUser = controler.getAddressIP(strIdUs);
//                log.info("+++++++++++++ controler: " + controler);

                if (addressIP.equals(addressUser)) {

                    if (controler.isPaginaUsuario(strIdUs, path)) {
                        long tp = session.getLastAccessedTime();
                        controler.setDatos(strIdUs, tp);
                        chain.doFilter(request, response);
                    } else {
                        RequestDispatcher dispatcher = request.getRequestDispatcher(excludeLogin);
                        if (dispatcher != null) {
                            dispatcher.forward(request, response);
                        }
                    }
                } else {
                    session.setAttribute("TEMP$USER_NAME", "");
                    session.invalidate();
                    HttpServletResponse hres = (HttpServletResponse) response;
                    hres.sendRedirect(rederingLogin);
                }

            } else {
                Integer timeMax = session.getMaxInactiveInterval() * 1000;
                controler.registerOutTime(timeMax);
                session.setAttribute("TEMP$USER_NAME", "");
                session.invalidate();
                HttpServletResponse hres = (HttpServletResponse) response;
                hres.sendRedirect(rederingLogin);
            }

        } catch (ServletException e) {

            log.error("ERROR EN EL FILTRO", e);
            Throwable rootCause = e.getRootCause();
            if (rootCause instanceof RuntimeException) { // This is true for any FacesException.
                throw (RuntimeException) rootCause; // Throw wrapped RuntimeException instead of ServletException.
            } else {
                throw e;
            }
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("loginFilter()");
        }
        StringBuilder sb = new StringBuilder("loginFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    private static void initLog4J() {
        URL url = Thread.currentThread().getContextClassLoader().getResource("log.properties");
        PropertyConfigurator.configure(url);
    }
}
