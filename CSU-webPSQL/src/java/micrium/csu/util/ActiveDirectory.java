/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ActiveDirectory {

    public static final int EXIT_USER = 1;
    public static final int NOT_EXIT_USER = 2;
    public static final int ERROR = 3;

    private static final Logger log = Logger.getLogger(ActiveDirectory.class);

//leer los parametro de conexion a LDAP desde el .properties
//al metodo solo se debe pasar user y pass
//DIRECTORIO_ACTIVO.initial_context_factory = com.sun.jndi.ldap.LdapCtxFactory
//DIRECTORIO_ACTIVO.provider_url = ldap://73.20.0.6
//DIRECTORIO_ACTIVO.security_authentication = simple
//DIRECTORIO_ACTIVO.security_principal= TIGOBO\\
//DIRECTORIO_ACTIVO.security_credentials = sysp0rt4l
//DIRECTORIO_ACTIVO.security_user = sysportal
//DIRECTORIO_ACTIVO.dominio= TIGOBO
    public static int existUser(String user, String password) {
        URL url = Thread.currentThread().getContextClassLoader().getResource("log4j.properties");
        PropertyConfigurator.configure(url);
        log.info("[ExistUser] Iniciando validacion en AD");

        if (password == null || password.isEmpty()) {
            return NOT_EXIT_USER;
        }
        Hashtable env = new Hashtable();

        try {
            env.put(Context.INITIAL_CONTEXT_FACTORY,Parameters.activeInitialContext);
            env.put(Context.PROVIDER_URL, Parameters.activeProviderUrl);
            env.put(Context.SECURITY_AUTHENTICATION, Parameters.activeSecurityAuthentication);
            // Set up environment for creating initial context
            env.put(Context.SECURITY_PRINCIPAL, Parameters.activeSecurityPrincipal + user);
            env.put(Context.SECURITY_CREDENTIALS, password);
            
       
        } catch (Exception e) {
            log.error("[existUser] Error al verificar la conectividad con el direcctorio activo"
                    + "Directory: " + e.getMessage());
            return ERROR;
        }

        try {
            // Create initial context
            InitialDirContext ctx = new InitialDirContext(env);
            // Close the context when we're done
            ctx.close();

            return EXIT_USER;
        } catch (NamingException ex) {
            log.error("[existUser] Error al autenticar en el servidor de Active "
                    + "Directory: " + ex.getMessage());
            return NOT_EXIT_USER;
        } catch (Exception e) {
            log.error("[existUser] Error al autenticar en el servidor de Active "
                    + "Directory: " + e.getMessage());
            return NOT_EXIT_USER;
        }
    }

    /**
     * Obtener Los Grupos a los que pertenece un Usuario
     *
 
     * @param user Nombre del usuario que se esta buscando
     * @return Una lista con los grupos a los que pertenece un usuario
     */
    public static ArrayList getGroups(String user) {
        Logger log = Logger.getLogger(ActiveDirectory.class);
        URL url = Thread.currentThread().getContextClassLoader().getResource("log4j.properties");
        PropertyConfigurator.configure(url);
        InitialLdapContext ctx = null;
        ArrayList list = new ArrayList();

        Hashtable env = new Hashtable();

        env.put("java.naming.factory.initial", Parameters.activeInitialContext);
        env.put("java.naming.provider.url", Parameters.activeProviderUrl);
        env.put("java.naming.security.authentication", Parameters.activeSecurityAuthentication);
        env.put("java.naming.security.principal", Parameters.activeSecurityPrincipal + Parameters.activeSecurityUser);
        env.put("java.naming.security.credentials", Parameters.activeSecurityCredentials);

        try {
            //Create the initial directory context
            ctx = new InitialLdapContext(env, null);
            //Especificar La base Para Buscar
            String searchBase = "DC=tigo,DC=net,DC=bo";
            //Create the search controls
            SearchControls searchCtls = new SearchControls();

            //Especificar el alcance de búsqueda
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String returnAtts[] = {"memberOf"};

            //Especificado el filtro de busqueda LDAP
            String searchFilter = "(&(objectCategory=person)(objectClass=user)(mailNickname=" + user + "))";

            searchCtls.setReturningAttributes(returnAtts);

            //Buscar un Objeto usando el Filtro ss
            NamingEnumeration answer = ctx.search(searchBase, searchFilter, searchCtls);
            int totalResults = 0;
            //ctx.close();
            //Bucle a través de los resultados de la búsqueda
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                //Print out the groups
                Attributes attrs = sr.getAttributes();
                if (attrs != null) {
                    for (NamingEnumeration ne = attrs.getAll(); ne.hasMore();) {
                        Attribute attr = (Attribute) ne.next();
                        String grupo;
                        for (NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
                            grupo = e.next().toString().trim();
                            grupo = grupo.substring(3, grupo.indexOf(",")).trim();
                            list.add(grupo);
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("[getGroups] Error al obtener el listado de grupos: " + e.getMessage());
        } finally {
            try {
                if (ctx != null) {
                    ctx.close();
                }
            } catch (Exception e) {
                log.error("[getGroups] Error al obtener el listado de grupos: " + e.getMessage());
            }

        }
        return list;
    }

    //-------------------------------------------------------------------------
    public static String obtenerNombreCompletoActiveDirectory(String usuario) {
        Logger log = Logger.getLogger(ActiveDirectory.class);
        log.info("[obtenerNombreCompletoActiveDirectory]Iniciando recuperacion de Nombre para loginAD=" + usuario);

        Conexion conexion = new Conexion();
        conexion.setDominio(Parameters.activeSecurityDominio);
        conexion.setUrl(Parameters.activeProviderUrl);
        conexion.setUsuario(Parameters.activeSecurityUser);
        conexion.setClave(Parameters.activeSecurityCredentials);
        Ldap ld = new Ldap(conexion);
        try {
            String[] returnAtts = {"cn", "sAMAccountName", "mail", "sn", "givenName", "initials"};
            Map mapa = ld.obtenerDatos(usuario, returnAtts);
            if (mapa.isEmpty()) {
                return "";
            }
            return (String) mapa.get("givenName") + " " + (String) mapa.get("sn");

        } catch (Exception e) {
            log.error("[obtenerNombreCompletoActiveDirectory]Al intentar recuperar nombres para loginAD" + usuario);
        }
        return "";
    }
//------------------------------------------------------------------------------

    public static boolean validarGrupo(String grupo) throws NamingException, Exception {
        InitialDirContext dirC = null;
        NamingEnumeration answer = null;
        NamingEnumeration ae = null;

        try {
            Hashtable env = new Hashtable();
            env.put("java.naming.factory.initial", Parameters.activeInitialContext);
            env.put("java.naming.provider.url", Parameters.activeProviderUrl);
            env.put("java.naming.security.authentication", Parameters.activeSecurityAuthentication);
            env.put("java.naming.security.principal", Parameters.activeSecurityPrincipal + Parameters.activeSecurityUser);
            env.put("java.naming.security.credentials", Parameters.activeSecurityCredentials);

            dirC = new InitialDirContext(env);
            if (dirC != null) {
                String searchBase = "DC=tigo,DC=net,DC=bo";
                SearchControls searchCtls = new SearchControls();
                searchCtls.setSearchScope(2);
                String searchFilter = "(objectclass=group)";
                String[] returnAtts = {"cn"};
                searchCtls.setReturningAttributes(returnAtts);
                answer = dirC.search(searchBase, searchFilter, searchCtls);

                while (answer.hasMoreElements()) {
                    SearchResult sr = (SearchResult) answer.next();
                    Attributes attrs = sr.getAttributes();
                    if (attrs != null) {
                        for (ae = attrs.getAll(); ae.hasMore();) {
                            Attribute attr = (Attribute) ae.next();
                            if (attr.get().toString().equals(grupo)) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (NamingException e) {
            log.error("ERROR: " + e.getMessage());

        } catch (Exception e) {
            log.error("ERROR: " + e.getMessage());
        } finally {
            try {
                if (dirC != null) {
                    dirC.close();
                }
            } catch (Exception e) {
                log.error("ERROR: " + e.getMessage());
            }

        }

        return false;
    }

    public static void main(String[] args) {
        try {
            //boolean a = ActiveDirectory.validarGrupo("CALL CENTER");
            int a= ActiveDirectory.existUser("admin", "a");
           // String a = obtenerNombreCompletoActiveDirectory("admin");
            System.out.println(a);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
