/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

/**
 *
 * @author jose_luis
 */
public class VariablesCostantes {

    public static final int INQUEST_LINE = 1;
    public static final int INQUEST_MASIVA = 2;

    public static final String LN_WEB = "SAC/Call Center";
    public static final String LN_FILE = "Carga Masiva";
    public static final String LN_CNT_REC = "Cantidad de Rechazos";
    public static final String LN_P_CASTER = "PC";

    public static final int ESTADO_ENC_CORRER = 1;
    public static final int ESTADO_ENC_PARADO = 2;
    public static final int ESTADO_ENC_TERMINADO = 3;
    public static final int ESTADO_ENC_NUEVO = 4;

}
