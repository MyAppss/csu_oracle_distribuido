/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 *
 * @author marciano_user
 */
public class ParameterMessage {

    protected final static String configFile = "Messages.properties";
    protected static final Properties prop = new Properties();

    static {
        init();
    }
    public static final String Pregunta_ERROR_titulo = prop.getProperty("Pregunta_ERROR_titulo");
    public static final String Pregunta_ERROR_texto = prop.getProperty("Pregunta_ERROR_texto");
    public static final String Pregunta_ERROR_posicion = prop.getProperty("Pregunta_ERROR_posicion");
    public static final String Pregunta_MESSAGE_GUARDAR = prop.getProperty("Pregunta_MESSAGE_GUARDAR");
    public static final String Pregunta_MESSAGE_MODIFICAR = prop.getProperty("Pregunta_MESSAGE_MODIFICAR");
    public static final String Pregunta_MESSAGE_ELIMINAR = prop.getProperty("Pregunta_MESSAGE_ELIMINAR");
    public static final String Pregunta_MESSAGE_ERROR_GUARDAR = prop.getProperty("Pregunta_MESSAGE_ERROR_GUARDAR");
    public static final String Pregunta_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Pregunta_MESSAGE_ERROR_MODIFICAR");
    public static final String Pregunta_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Pregunta_MESSAGE_ERROR_ELIMINAR");
    /**
     * Ruta del archivo de configuracion
     */
    public static final String Canal_ERROR_nombre = prop.getProperty("Canal_ERROR_nombre");
    public static final String Canal_ERROR_descripcion = prop.getProperty("Canal_ERROR_descripcion");
    public static final String Canal_ERROR_longitudTexto = prop.getProperty("Canal_ERROR_longitudTexto");
    public static final String Canal_ERROR_estado = prop.getProperty("Canal_ERROR_estado");
    public static final String Canal_MESSAGE_GUARDAR = prop.getProperty("Canal_MESSAGE_GUARDAR");
    public static final String Canal_MESSAGE_MODIFICAR = prop.getProperty("Canal_MESSAGE_MODIFICAR");
    public static final String Canal_MESSAGE_ELIMINAR = prop.getProperty("Canal_MESSAGE_ELIMINAR");
    public static final String Canal_MESSAGE_ERROR_GUARDAR = prop.getProperty("Canal_MESSAGE_ERROR_GUARDAR");
    public static final String Canal_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Canal_MESSAGE_ERROR_MODIFICAR");
    public static final String Canal_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Canal_MESSAGE_ERROR_ELIMINAR");
    public static final String Banco_ERROR_canalId = prop.getProperty("Banco_ERROR_canalId");
    public static final String Banco_ERROR_nombre = prop.getProperty("Banco_ERROR_nombre");
    public static final String Banco_ERROR_estado = prop.getProperty("Banco_ERROR_estado");
    public static final String Banco_MESSAGE_GUARDAR = prop.getProperty("Banco_MESSAGE_GUARDAR");
    public static final String Banco_MESSAGE_MODIFICAR = prop.getProperty("Banco_MESSAGE_MODIFICAR");
    public static final String Banco_MESSAGE_ELIMINAR = prop.getProperty("Banco_MESSAGE_ELIMINAR");
    public static final String Banco_MESSAGE_ERROR_GUARDAR = prop.getProperty("Banco_MESSAGE_ERROR_GUARDAR");
    public static final String Banco_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Banco_MESSAGE_ERROR_MODIFICAR");
    public static final String Banco_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Banco_MESSAGE_ERROR_ELIMINAR");
    public static final String Opcion_ERROR_preguntaId = prop.getProperty("Opcion_ERROR_preguntaId");
    public static final String Opcion_ERROR_nroOpcion = prop.getProperty("Opcion_ERROR_nroOpcion");
    public static final String Opcion_ERROR_texto = prop.getProperty("Opcion_ERROR_texto");
    public static final String Opcion_ERROR_estado = prop.getProperty("Opcion_ERROR_estado");
    public static final String Opcion_MESSAGE_GUARDAR = prop.getProperty("Opcion_MESSAGE_GUARDAR");
    public static final String Opcion_MESSAGE_MODIFICAR = prop.getProperty("Opcion_MESSAGE_MODIFICAR");
    public static final String Opcion_MESSAGE_ELIMINAR = prop.getProperty("Opcion_MESSAGE_ELIMINAR");
    public static final String Opcion_MESSAGE_ERROR_GUARDAR = prop.getProperty("Opcion_MESSAGE_ERROR_GUARDAR");
    public static final String Opcion_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Opcion_MESSAGE_ERROR_MODIFICAR");
    public static final String Opcion_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Opcion_MESSAGE_ERROR_ELIMINAR");
    public static final String Encuesta_ERROR_bancoId = prop.getProperty("Encuesta_ERROR_bancoId");
    public static final String Encuesta_ERROR_billeteraId = prop.getProperty("Encuesta_ERROR_billeteraId");
    public static final String Encuesta_ERROR_nombre = prop.getProperty("Encuesta_ERROR_nombre");
    public static final String Encuesta_ERROR_fechaIni = prop.getProperty("Encuesta_ERROR_fechaIni");
    public static final String Encuesta_ERROR_fechaFin = prop.getProperty("Encuesta_ERROR_fechaFin");
    public static final String Encuesta_ERROR_enviarBroadcast = prop.getProperty("Encuesta_ERROR_enviarBroadcast");
    public static final String Encuesta_ERROR_textoInvitacion = prop.getProperty("Encuesta_ERROR_textoInvitacion");
    public static final String Encuesta_ERROR_textoNotificacion = prop.getProperty("Encuesta_ERROR_textoNotificacion");
    public static final String Encuesta_ERROR_filtrarLista = prop.getProperty("Encuesta_ERROR_filtrarLista");
    public static final String Encuesta_ERROR_estadoProcesar = prop.getProperty("Encuesta_ERROR_estadoProcesar");
    public static final String Encuesta_ERROR_premioValor = prop.getProperty("Encuesta_ERROR_premioValor");
    public static final String Encuesta_ERROR_premioVigencia = prop.getProperty("Encuesta_ERROR_premioVigencia");
    public static final String Encuesta_ERROR_premioTipoVigencia = prop.getProperty("Encuesta_ERROR_premioTipoVigencia");
    public static final String Encuesta_ERROR_estado = prop.getProperty("Encuesta_ERROR_estado");
    public static final String Encuesta_MESSAGE_GUARDAR = prop.getProperty("Encuesta_MESSAGE_GUARDAR");
    public static final String Encuesta_MESSAGE_MODIFICAR = prop.getProperty("Encuesta_MESSAGE_MODIFICAR");
    public static final String Encuesta_MESSAGE_ELIMINAR = prop.getProperty("Encuesta_MESSAGE_ELIMINAR");
    public static final String Encuesta_MESSAGE_ERROR_GUARDAR = prop.getProperty("Encuesta_MESSAGE_ERROR_GUARDAR");
    public static final String Encuesta_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Encuesta_MESSAGE_ERROR_MODIFICAR");
    public static final String Encuesta_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Encuesta_MESSAGE_ERROR_ELIMINAR");
    public static final String Parametro_ERROR_canalId = prop.getProperty("Parametro_ERROR_canalId");
    public static final String Parametro_ERROR_nombre = prop.getProperty("Parametro_ERROR_nombre");
    public static final String Parametro_ERROR_valor = prop.getProperty("Parametro_ERROR_valor");
    public static final String Parametro_ERROR_estado = prop.getProperty("Parametro_ERROR_estado");
    public static final String Parametro_MESSAGE_GUARDAR = prop.getProperty("Parametro_MESSAGE_GUARDAR");
    public static final String Parametro_MESSAGE_MODIFICAR = prop.getProperty("Parametro_MESSAGE_MODIFICAR");
    public static final String Parametro_MESSAGE_ELIMINAR = prop.getProperty("Parametro_MESSAGE_ELIMINAR");
    public static final String Parametro_MESSAGE_ERROR_GUARDAR = prop.getProperty("Parametro_MESSAGE_ERROR_GUARDAR");
    public static final String Parametro_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Parametro_MESSAGE_ERROR_MODIFICAR");
    public static final String Parametro_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Parametro_MESSAGE_ERROR_ELIMINAR");
    private static URL configURL;
    /**
     * flujo de entrada relacionado con el archivo de configuracion
     */
    private static InputStream configFileStream;

    public static void init() {

        //configURL = Thread.currentThread().getContextClassLoader().getResource(configFile);
        Thread thr = Thread.currentThread();
        ClassLoader cL;
        if (thr != null) {
            cL = thr.getContextClassLoader();
            if (cL != null) {
                configURL = cL.getResource(configFile);
                try {
                    configFileStream = new FileInputStream(configURL.getFile());
                    prop.load(configFileStream);
                } catch (Exception e) {
                    //e.printStackTrace(); 
                }

            }
        }
    }
    public static final String Billetera_ERROR_unitTypeId = prop.getProperty("Billetera_ERROR_unitTypeId");
    public static final String Billetera_ERROR_nombreComverse = prop.getProperty("Billetera_ERROR_nombreComverse");
    public static final String Billetera_ERROR_nombreComercial = prop.getProperty("Billetera_ERROR_nombreComercial");
    public static final String Billetera_ERROR_prefijoUnidad = prop.getProperty("Billetera_ERROR_prefijoUnidad");
    public static final String Billetera_ERROR_operador = prop.getProperty("Billetera_ERROR_operador");
    public static final String Billetera_ERROR_valor = prop.getProperty("Billetera_ERROR_valor");
    public static final String Billetera_ERROR_cantidadDecimales = prop.getProperty("Billetera_ERROR_cantidadDecimales");
    public static final String Billetera_ERROR_montoMinimo = prop.getProperty("Billetera_ERROR_montoMinimo");
    public static final String Billetera_ERROR_montoMaximo = prop.getProperty("Billetera_ERROR_montoMaximo");
    public static final String Billetera_ERROR_estado = prop.getProperty("Billetera_ERROR_estado");
    public static final String Billetera_MESSAGE_GUARDAR = prop.getProperty("Billetera_MESSAGE_GUARDAR");
    public static final String Billetera_MESSAGE_MODIFICAR = prop.getProperty("Billetera_MESSAGE_MODIFICAR");
    public static final String Billetera_MESSAGE_ELIMINAR = prop.getProperty("Billetera_MESSAGE_ELIMINAR");
    public static final String Billetera_MESSAGE_ERROR_GUARDAR = prop.getProperty("Billetera_MESSAGE_ERROR_GUARDAR");
    public static final String Billetera_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Billetera_MESSAGE_ERROR_MODIFICAR");
    public static final String Billetera_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Billetera_MESSAGE_ERROR_ELIMINAR");
    public static final String ComposicionBilletera_ERROR_nombre = prop.getProperty("ComposicionBilletera_ERROR_nombre");
    public static final String ComposicionBilletera_ERROR_nombreComercial = prop.getProperty("ComposicionBilletera_ERROR_nombreComercial");
    public static final String ComposicionBilletera_ERROR_prefijoUnidad = prop.getProperty("ComposicionBilletera_ERROR_prefijoUnidad");
    public static final String ComposicionBilletera_ERROR_operador = prop.getProperty("ComposicionBilletera_ERROR_operador");
    public static final String ComposicionBilletera_ERROR_valor = prop.getProperty("ComposicionBilletera_ERROR_valor");
    public static final String ComposicionBilletera_ERROR_cantidadDecimales = prop.getProperty("ComposicionBilletera_ERROR_cantidadDecimales");
    public static final String ComposicionBilletera_ERROR_montoMinimo = prop.getProperty("ComposicionBilletera_ERROR_montoMinimo");
    public static final String ComposicionBilletera_ERROR_montoMaximo = prop.getProperty("ComposicionBilletera_ERROR_montoMaximo");
    public static final String ComposicionBilletera_ERROR_estado = prop.getProperty("ComposicionBilletera_ERROR_estado");
    public static final String ComposicionBilletera_MESSAGE_GUARDAR = prop.getProperty("ComposicionBilletera_MESSAGE_GUARDAR");
    public static final String ComposicionBilletera_MESSAGE_MODIFICAR = prop.getProperty("ComposicionBilletera_MESSAGE_MODIFICAR");
    public static final String ComposicionBilletera_MESSAGE_ELIMINAR = prop.getProperty("ComposicionBilletera_MESSAGE_ELIMINAR");
    public static final String ComposicionBilletera_MESSAGE_ERROR_GUARDAR = prop.getProperty("ComposicionBilletera_MESSAGE_ERROR_GUARDAR");
    public static final String ComposicionBilletera_MESSAGE_ERROR_MODIFICAR = prop.getProperty("ComposicionBilletera_MESSAGE_ERROR_MODIFICAR");
    public static final String ComposicionBilletera_MESSAGE_ERROR_ELIMINAR = prop.getProperty("ComposicionBilletera_MESSAGE_ERROR_ELIMINAR");
    public static final String GrupoBilletera_ERROR_composicionBilleteraId = prop.getProperty("GrupoBilletera_ERROR_composicionBilleteraId");
    public static final String GrupoBilletera_MESSAGE_GUARDAR = prop.getProperty("GrupoBilletera_MESSAGE_GUARDAR");
    public static final String GrupoBilletera_MESSAGE_MODIFICAR = prop.getProperty("GrupoBilletera_MESSAGE_MODIFICAR");
    public static final String GrupoBilletera_MESSAGE_ELIMINAR = prop.getProperty("GrupoBilletera_MESSAGE_ELIMINAR");
    public static final String GrupoBilletera_MESSAGE_ERROR_GUARDAR = prop.getProperty("GrupoBilletera_MESSAGE_ERROR_GUARDAR");
    public static final String GrupoBilletera_MESSAGE_ERROR_MODIFICAR = prop.getProperty("GrupoBilletera_MESSAGE_ERROR_MODIFICAR");
    public static final String GrupoBilletera_MESSAGE_ERROR_ELIMINAR = prop.getProperty("GrupoBilletera_MESSAGE_ERROR_ELIMINAR");
    public static final String CosComposicionBilletera_ERROR_cosId = prop.getProperty("CosComposicionBilletera_ERROR_cosId");
    public static final String CosComposicionBilletera_ERROR_nombreComercial = prop.getProperty("CosComposicionBilletera_ERROR_nombreComercial");
    public static final String CosComposicionBilletera_ERROR_mostrarSiempre = prop.getProperty("CosComposicionBilletera_ERROR_mostrarSiempre");
    public static final String CosComposicionBilletera_ERROR_mostrarSaldoMayorCero = prop.getProperty("CosComposicionBilletera_ERROR_mostrarSaldoMayorCero");
    public static final String CosComposicionBilletera_ERROR_mostrarSaldoMenorCero = prop.getProperty("CosComposicionBilletera_ERROR_mostrarSaldoMenorCero");
    public static final String CosComposicionBilletera_ERROR_mostrarSaldoCero = prop.getProperty("CosComposicionBilletera_ERROR_mostrarSaldoCero");
    public static final String CosComposicionBilletera_ERROR_mostrarSaldoExpiro = prop.getProperty("CosComposicionBilletera_ERROR_mostrarSaldoExpiro");
    public static final String CosComposicionBilletera_ERROR_estado = prop.getProperty("CosComposicionBilletera_ERROR_estado");
    public static final String CosComposicionBilletera_MESSAGE_GUARDAR = prop.getProperty("CosComposicionBilletera_MESSAGE_GUARDAR");
    public static final String CosComposicionBilletera_MESSAGE_MODIFICAR = prop.getProperty("CosComposicionBilletera_MESSAGE_MODIFICAR");
    public static final String CosComposicionBilletera_MESSAGE_ELIMINAR = prop.getProperty("CosComposicionBilletera_MESSAGE_ELIMINAR");
    public static final String CosComposicionBilletera_MESSAGE_ERROR_GUARDAR = prop.getProperty("CosComposicionBilletera_MESSAGE_ERROR_GUARDAR");
    public static final String CosComposicionBilletera_MESSAGE_ERROR_MODIFICAR = prop.getProperty("CosComposicionBilletera_MESSAGE_ERROR_MODIFICAR");
    public static final String CosComposicionBilletera_MESSAGE_ERROR_ELIMINAR = prop.getProperty("CosComposicionBilletera_MESSAGE_ERROR_ELIMINAR");
    public static final String UnitType_ERROR_nombre = prop.getProperty("UnitType_ERROR_nombre");
    public static final String UnitType_ERROR_descripcion = prop.getProperty("UnitType_ERROR_descripcion");
    public static final String UnitType_ERROR_estado = prop.getProperty("UnitType_ERROR_estado");
    public static final String UnitType_MESSAGE_GUARDAR = prop.getProperty("UnitType_MESSAGE_GUARDAR");
    public static final String UnitType_MESSAGE_MODIFICAR = prop.getProperty("UnitType_MESSAGE_MODIFICAR");
    public static final String UnitType_MESSAGE_ELIMINAR = prop.getProperty("UnitType_MESSAGE_ELIMINAR");
    public static final String UnitType_MESSAGE_ERROR_GUARDAR = prop.getProperty("UnitType_MESSAGE_ERROR_GUARDAR");
    public static final String UnitType_MESSAGE_ERROR_MODIFICAR = prop.getProperty("UnitType_MESSAGE_ERROR_MODIFICAR");
    public static final String UnitType_MESSAGE_ERROR_ELIMINAR = prop.getProperty("UnitType_MESSAGE_ERROR_ELIMINAR");
    public static final String Config_ERROR_nombre = prop.getProperty("Config_ERROR_nombre");
    public static final String Config_ERROR_saludoInicial = prop.getProperty("Config_ERROR_saludoInicial");
    public static final String Config_ERROR_descripcion = prop.getProperty("Config_ERROR_descripcion");
    public static final String Config_ERROR_mostrarDpi = prop.getProperty("Config_ERROR_mostrarDpi");
    public static final String Config_ERROR_mostrarVigencia = prop.getProperty("Config_ERROR_mostrarVigencia");
    public static final String Config_ERROR_habilitado = prop.getProperty("Config_ERROR_habilitado");
    public static final String Config_ERROR_estado = prop.getProperty("Config_ERROR_estado");
    public static final String Config_MESSAGE_GUARDAR = prop.getProperty("Config_MESSAGE_GUARDAR");
    public static final String Config_MESSAGE_MODIFICAR = prop.getProperty("Config_MESSAGE_MODIFICAR");
    public static final String Config_MESSAGE_ELIMINAR = prop.getProperty("Config_MESSAGE_ELIMINAR");
    public static final String Config_MESSAGE_ERROR_GUARDAR = prop.getProperty("Config_MESSAGE_ERROR_GUARDAR");
    public static final String Config_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Config_MESSAGE_ERROR_MODIFICAR");
    public static final String Config_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Config_MESSAGE_ERROR_ELIMINAR");
    public static final String Cos_ERROR_configId = prop.getProperty("Cos_ERROR_configId");
    public static final String Cos_ERROR_nombre = prop.getProperty("Cos_ERROR_nombre");
    public static final String Cos_ERROR_descripcion = prop.getProperty("Cos_ERROR_descripcion");
    public static final String Cos_ERROR_habilitado = prop.getProperty("Cos_ERROR_habilitado");
    public static final String Cos_ERROR_estado = prop.getProperty("Cos_ERROR_estado");
    public static final String Cos_MESSAGE_GUARDAR = prop.getProperty("Cos_MESSAGE_GUARDAR");
    public static final String Cos_MESSAGE_MODIFICAR = prop.getProperty("Cos_MESSAGE_MODIFICAR");
    public static final String Cos_MESSAGE_ELIMINAR = prop.getProperty("Cos_MESSAGE_ELIMINAR");
    public static final String Cos_MESSAGE_ERROR_GUARDAR = prop.getProperty("Cos_MESSAGE_ERROR_GUARDAR");
    public static final String Cos_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Cos_MESSAGE_ERROR_MODIFICAR");
    public static final String Cos_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Cos_MESSAGE_ERROR_ELIMINAR");
    public static final String Acumulador_ERROR_nombre = prop.getProperty("Acumulador_ERROR_nombre");
    public static final String Acumulador_ERROR_limite = prop.getProperty("Acumulador_ERROR_limite");
    public static final String Acumulador_ERROR_estado = prop.getProperty("Acumulador_ERROR_estado");
    public static final String Acumulador_MESSAGE_GUARDAR = prop.getProperty("Acumulador_MESSAGE_GUARDAR");
    public static final String Acumulador_MESSAGE_MODIFICAR = prop.getProperty("Acumulador_MESSAGE_MODIFICAR");
    public static final String Acumulador_MESSAGE_ELIMINAR = prop.getProperty("Acumulador_MESSAGE_ELIMINAR");
    public static final String Acumulador_MESSAGE_ERROR_GUARDAR = prop.getProperty("Acumulador_MESSAGE_ERROR_GUARDAR");
    public static final String Acumulador_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Acumulador_MESSAGE_ERROR_MODIFICAR");
    public static final String Acumulador_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Acumulador_MESSAGE_ERROR_ELIMINAR");
    public static final String ReporteConsulta_ERROR_msisdn = prop.getProperty("ReporteConsulta_ERROR_msisdn");
    public static final String ReporteConsulta_ERROR_identificadorServicio = prop.getProperty("ReporteConsulta_ERROR_identificadorServicio");
    public static final String ReporteConsulta_ERROR_nombreCanal = prop.getProperty("ReporteConsulta_ERROR_nombreCanal");
    public static final String ReporteConsulta_ERROR_publicidadSolicitada = prop.getProperty("ReporteConsulta_ERROR_publicidadSolicitada");
    public static final String ReporteConsulta_ERROR_longitudMaxSolicitada = prop.getProperty("ReporteConsulta_ERROR_longitudMaxSolicitada");
    public static final String ReporteConsulta_ERROR_ipCliente = prop.getProperty("ReporteConsulta_ERROR_ipCliente");
    public static final String ReporteConsulta_ERROR_textoGenerado = prop.getProperty("ReporteConsulta_ERROR_textoGenerado");
    public static final String ReporteConsulta_MESSAGE_GUARDAR = prop.getProperty("ReporteConsulta_MESSAGE_GUARDAR");
    public static final String ReporteConsulta_MESSAGE_MODIFICAR = prop.getProperty("ReporteConsulta_MESSAGE_MODIFICAR");
    public static final String ReporteConsulta_MESSAGE_ELIMINAR = prop.getProperty("ReporteConsulta_MESSAGE_ELIMINAR");
    public static final String ReporteConsulta_MESSAGE_ERROR_GUARDAR = prop.getProperty("ReporteConsulta_MESSAGE_ERROR_GUARDAR");
    public static final String ReporteConsulta_MESSAGE_ERROR_MODIFICAR = prop.getProperty("ReporteConsulta_MESSAGE_ERROR_MODIFICAR");
    public static final String ReporteConsulta_MESSAGE_ERROR_ELIMINAR = prop.getProperty("ReporteConsulta_MESSAGE_ERROR_ELIMINAR");
    public static final String RolPaginaIni_ERROR_paginaInicio = prop.getProperty("RolPaginaIni_ERROR_paginaInicio");
    public static final String RolPaginaIni_MESSAGE_GUARDAR = prop.getProperty("RolPaginaIni_MESSAGE_GUARDAR");
    public static final String RolPaginaIni_MESSAGE_MODIFICAR = prop.getProperty("RolPaginaIni_MESSAGE_MODIFICAR");
    public static final String RolPaginaIni_MESSAGE_ELIMINAR = prop.getProperty("RolPaginaIni_MESSAGE_ELIMINAR");
    public static final String RolPaginaIni_MESSAGE_ERROR_GUARDAR = prop.getProperty("RolPaginaIni_MESSAGE_ERROR_GUARDAR");
    public static final String RolPaginaIni_MESSAGE_ERROR_MODIFICAR = prop.getProperty("RolPaginaIni_MESSAGE_ERROR_MODIFICAR");
    public static final String RolPaginaIni_MESSAGE_ERROR_ELIMINAR = prop.getProperty("RolPaginaIni_MESSAGE_ERROR_ELIMINAR");
    public static final String Corto_ERROR_configId = prop.getProperty("Corto_ERROR_configId");
    public static final String Corto_ERROR_nombre = prop.getProperty("Corto_ERROR_nombre");
    public static final String Corto_ERROR_nombre_no_valido = prop.getProperty("Corto_ERROR_nombre_no_valido");
    public static final String Corto_ERROR_descripcion_no_valido = prop.getProperty("Corto_ERROR_descripcion_no_valido");
    public static final String Corto_ERROR_descripcion = prop.getProperty("Corto_ERROR_descripcion");
    public static final String Corto_ERROR_habilitado = prop.getProperty("Corto_ERROR_habilitado");
    public static final String Corto_ERROR_estado = prop.getProperty("Corto_ERROR_estado");
    public static final String Corto_MESSAGE_GUARDAR = prop.getProperty("Corto_MESSAGE_GUARDAR");
    public static final String Corto_MESSAGE_MODIFICAR = prop.getProperty("Corto_MESSAGE_MODIFICAR");
    public static final String Corto_MESSAGE_ELIMINAR = prop.getProperty("Corto_MESSAGE_ELIMINAR");
    public static final String Corto_MESSAGE_ERROR_GUARDAR = prop.getProperty("Corto_MESSAGE_ERROR_GUARDAR");
    public static final String Corto_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Corto_MESSAGE_ERROR_MODIFICAR");
    public static final String Corto_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Corto_MESSAGE_ERROR_ELIMINAR");
    public static final String Origen_ERROR_nombre = prop.getProperty("Origen_ERROR_nombre");
    public static final String Origen_ERROR_descripcion = prop.getProperty("Origen_ERROR_descripcion");
    public static final String Origen_ERROR_habilitado = prop.getProperty("Origen_ERROR_habilitado");
    public static final String Origen_ERROR_estado = prop.getProperty("Origen_ERROR_estado");
    public static final String Origen_MESSAGE_GUARDAR = prop.getProperty("Origen_MESSAGE_GUARDAR");
    public static final String Origen_MESSAGE_MODIFICAR = prop.getProperty("Origen_MESSAGE_MODIFICAR");
    public static final String Origen_MESSAGE_ELIMINAR = prop.getProperty("Origen_MESSAGE_ELIMINAR");
    public static final String Origen_MESSAGE_ERROR_GUARDAR = prop.getProperty("Origen_MESSAGE_ERROR_GUARDAR");
    public static final String Origen_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Origen_MESSAGE_ERROR_MODIFICAR");
    public static final String Origen_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Origen_MESSAGE_ERROR_ELIMINAR");
    public static final String Occ_ERROR_configId = prop.getProperty("Occ_ERROR_configId");
    public static final String Occ_ERROR_origenId = prop.getProperty("Occ_ERROR_origenId");
    public static final String Occ_ERROR_cortoId = prop.getProperty("Occ_ERROR_cortoId");
    public static final String Occ_ERROR_cosId = prop.getProperty("Occ_ERROR_cosId");
    public static final String Occ_ERROR_estado = prop.getProperty("Occ_ERROR_estado");
    public static final String Occ_MESSAGE_GUARDAR = prop.getProperty("Occ_MESSAGE_GUARDAR");
    public static final String Occ_MESSAGE_MODIFICAR = prop.getProperty("Occ_MESSAGE_MODIFICAR");
    public static final String Occ_MESSAGE_ELIMINAR = prop.getProperty("Occ_MESSAGE_ELIMINAR");
    public static final String Occ_MESSAGE_ERROR_GUARDAR = prop.getProperty("Occ_MESSAGE_ERROR_GUARDAR");
    public static final String Occ_MESSAGE_ERROR_MODIFICAR = prop.getProperty("Occ_MESSAGE_ERROR_MODIFICAR");
    public static final String Occ_MESSAGE_ERROR_ELIMINAR = prop.getProperty("Occ_MESSAGE_ERROR_ELIMINAR");

    public static final String Categoria_ERROR_nombre = prop.getProperty("Categoria_ERROR_nombre");
    public static final String Categoria_MESSAGE_GUARDAR = prop.getProperty("Categoria_MESSAGE_GUARDAR");
    public static final String Categoria_MESSAGE_MODIFICAR = prop.getProperty("Categoria_MESSAGE_MODIFICAR");
    public static final String Categoria_MESSAGE_ELIMINAR = prop.getProperty("Categoria_MESSAGE_ELIMINAR");

    public static final String Menu_ERROR_nombre = prop.getProperty("Menu_ERROR_nombre");
    public static final String Menu_MESSAGE_GUARDAR = prop.getProperty("Menu_MESSAGE_GUARDAR");
    public static final String Menu_MESSAGE_MODIFICAR = prop.getProperty("Menu_MESSAGE_MODIFICAR");
    public static final String Menu_MESSAGE_ELIMINAR = prop.getProperty("Menu_MESSAGE_ELIMINAR");

    public static final String Conf_Cabecera_MSG_ERROR = prop.getProperty("Conf_Cabecera_ERROR");
    public static final String Conf_Cabecera_MSG_GUARDAR = prop.getProperty("Conf_Cabecera_GUARDAR");
    public static final String Conf_Cabecera_MSG_MODIFICAR = prop.getProperty("Conf_Cabecera_MODIFICAR");
    public static final String Conf_Cabecera_MSG_ELIMINAR = prop.getProperty("Conf_Cabecera_ELIMINAR");

    public static final String MapeoOfertas_ERROR_OfferingId = prop.getProperty("MapeoOfertas_ERROR_OfferingId");
    public static final String MapeoOfertas_ERROR_OfferingName = prop.getProperty("MapeoOfertas_ERROR_OfferingName");
    public static final String MapeoOfertas_ERROR_Type = prop.getProperty("MapeoOfertas_ERROR_Type");
    public static final String MapeoOfertas_ERROR_PaymentMode = prop.getProperty("MapeoOfertas_ERROR_PaymentMode");

    public static final String MapeoOfertas_MESSAGE_GUARDAR = prop.getProperty("MapeoOfertas_MESSAGE_GUARDAR");
    public static final String MapeoOfertas_MESSAGE_MODIFICAR = prop.getProperty("MapeoOfertas_MESSAGE_MODIFICAR");
    public static final String MapeoOfertas_MESSAGE_ELIMINAR = prop.getProperty("MapeoOfertas_MESSAGE_ELIMINAR");
    public static final String MapeoOfertas_MESSAGE_ERROR_GUARDAR = prop.getProperty("MapeoOfertas_MESSAGE_ERROR_GUARDAR");

    public static final String MapeoOfertas_MESSAGE_ERROR_MODIFICAR = prop.getProperty("MapeoOfertas_MESSAGE_ERROR_MODIFICAR");
    public static final String MapeoOfertas_MESSAGE_ERROR_ELIMINAR = prop.getProperty("MapeoOfertas_MESSAGE_ERROR_ELIMINAR");

    public static final String MapeoOfertas_MESSAGE_OFFERINGID_YA_EXISTE = prop.getProperty("MapeoOfertas_MESSAGE_OFFERINGID_YA_EXISTE");
    public static final String MapeoOfertas_MESSAGE_OFFERINGPARENT_NO_EXISTE = prop.getProperty("MapeoOfertas_MESSAGE_OFFERINGPARENT_NO_EXISTE");
    public static final String MapeoOfertas_MESSAGE_OFFERINGNAME_YA_EXSTE = prop.getProperty("MapeoOfertas_MESSAGE_OFFERINGNAME_YA_EXISTE");
 

}
