/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import java.io.Serializable;

/**
 *
 * @author ALI
 */
public class CustomMessage implements Serializable{
    private int type;
    private String message;
    private static final long serialVersionUID = 1L;
    public static final int INFO=0,WARN=1,ERROR=2,FATAL=3;
    public CustomMessage(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
