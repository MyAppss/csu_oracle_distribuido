package micrium.csu.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.sql.Time;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class UtilDate {

    private static final Logger log = Logger.getLogger(UtilDate.class);

    public static Timestamp dateToTimestamp(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = formatter.format(fecha);
        return stringToTimestamp(str);
    }

    public static Calendar stringToCalendar(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            log.error("ERROR: " + e.getMessage());
        }
        return cal;
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = null;
        try {
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (Exception e) {
            log.error("ERROR: " + e.getMessage());
        }
        return cal;
    }

    public static Calendar stringToCalendarPM_AM(String fecha) {
        Calendar cal = null;
        try {
            DateFormat formatter;
            Date date;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = (Date) formatter.parse(fecha);
            cal = Calendar.getInstance();
            cal.setTime(date);
        } catch (ParseException e) {
            log.error("ERROR: " + e.getMessage());
        }
        return cal;
    }

    public static Timestamp stringToTimestamp(String fecha) {
        Calendar date = stringToCalendar(fecha);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Timestamp stringToTimestampPM_AM(String fecha) {
        Calendar date = stringToCalendarPM_AM(fecha);
        Timestamp t = new Timestamp(new Date().getTime());
        if (date != null) {
            t = new Timestamp(date.getTimeInMillis());
        }
        return t;
    }

    public static Date stringToDate(String fecha) {
        fecha = fecha + " 00:00:00";
        Calendar date = stringToCalendar(fecha);
        Date dat = new Date();
        if (date != null) {
            dat = new Date(date.getTimeInMillis());
        }
        return dat;
    }

    public static Time stringToTime(String fecha) {
        fecha = fecha + ":00";
        Time t = Time.valueOf(fecha);
        return t;
    }

    public static String dateToString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Timestamp dateToTimetampSW_IF(Date fecha, boolean swIni) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String str = formatter.format(fecha);
        if (swIni) {
            str = str + " 00:00:00";
        } else {
            str = str + " 23:59:59";
        }

        return stringToTimestamp(str);
    }

    public static Timestamp getTimetampActual() {
        Date dd = new Date();
        String str = dd.getTime() + "";
        str = str.substring(0, 10);
        str = str + "000";
        long tt = Long.parseLong(str);
        Timestamp t = new Timestamp(tt);
        return t;
    }

    public static String timetampToString(Timestamp fecha) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static String dateToTimeString(Date fecha) {
        DateFormat formatter = new SimpleDateFormat("HH:mm");//("yyyy-MM-dd");
        return formatter.format(fecha);
    }

    public static Date unirTwoDate(Date day, Date time) {

        String str1 = UtilDate.dateToString(day);

        String str = UtilDate.dateToTimeString(time);
        str = str1 + " " + str;
        return UtilDate.stringToTimestamp(str);
    }

    public static boolean intervalo(Date mI, Date mF, Date fecha) {
        if (fecha.getTime() == mI.getTime()) {
            return true;
        }

        if (fecha.getTime() == mF.getTime()) {
            return true;
        }

        if (fecha.after(mI) && (fecha.before(mF))) {
            return true;
        } else {
            return false;
        }
    }
    
    
        public static String timetampToStringPatter(Timestamp fecha,String pattern) {
        DateFormat formatter = new SimpleDateFormat(pattern);//("yyyy-MM-dd");
        return formatter.format(fecha);
    }
        
        
    public static synchronized String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = (String) request.getSession().getAttribute("TEMP$IP_CLIENT");

        }
        /*   if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // else {
        // log.debug("X-FORWARDED RESUELTO: " + ip);
        // }*/
        return ip;
    }
}
