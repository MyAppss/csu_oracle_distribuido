package micrium.csu.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Parameters {

    // Lee los datos del Archivo de configuración
    protected final static String configFile = "CSU-config.properties";
    protected static final Properties prop = new Properties();
    private static final Logger log = Logger.getLogger(Parameters.class);

    static {
        init();
    }
    public static final String activeInitialContext = prop.getProperty("DIRECTORIO_ACTIVO.initial_context_factory");
    public static final String activeProviderUrl = prop.getProperty("DIRECTORIO_ACTIVO.provider_url");
    public static final String activeSecurityAuthentication = prop.getProperty("DIRECTORIO_ACTIVO.security_authentication");
    public static final String activeSecurityPrincipal = prop.getProperty("DIRECTORIO_ACTIVO.security_principal");
    public static final String activeSecurityCredentials = prop.getProperty("DIRECTORIO_ACTIVO.security_credentials");
    public static final String activeSecurityUser = prop.getProperty("DIRECTORIO_ACTIVO.security_user");
    public static final String activeSecurityDominio = prop.getProperty("DIRECTORIO_ACTIVO.dominio");
    //DIRECTORIO_ACTIVO.dominio

    public static final String swActive = prop.getProperty("USUARIO.sw_active_directory");
    public static final String login = prop.getProperty("USUARIO.login");
    public static final String password = prop.getProperty("USUARIO.password");
    public static final String nroOption = prop.getProperty("USUARIO.nro_opciones");
    public static final String timeOut = prop.getProperty("USUARIO.tiempo_fuera");

    /**
     * Ruta del archivo de configuracion
     */
    private static URL configURL;
    /**
     * flujo de entrada relacionado con el archivo de configuracion
     */
    private static InputStream configFileStream;

    public static final String BD_server = prop.getProperty("bd.servidor");
    public static final String BD_name = prop.getProperty("bd.nombre");
    public static final String BD_port = prop.getProperty("bd.puerto");
    public static final String BD_user = prop.getProperty("bd.usuario");
    public static final String BD_pass = prop.getProperty("bd.contrasenna");

    public static final String BD_setminevictableidletimemillis = prop.getProperty("bd.setminevictableidletimemillis");
    public static final String BD_setmaxactive = prop.getProperty("bd.setmaxactive");
    public static final String BD_setminidle = prop.getProperty("bd.setminidle");
    public static final String BD_setMaxIdle = prop.getProperty("bd.setMaxIdle");
    public static final String BD_setMaxWait = prop.getProperty("bd.setMaxWait");

    //public static final String FILE_EXTENCION = prop.getProperty("FILE.extencion") ;
    //public static final String EXTENCIONES  = prop.getProperty("PREFIJOS.extencio");
    public static final String ER_NrosTIGO = prop.getProperty("ExprRegularValidarNroTIGO");
    public static final String NroFilasDataTable = prop.getProperty("NroFilas.Table");
    public static final String NroFilasTableCallSac = prop.getProperty("NroFilas.Table.CallSac");
    public static final String BILLETERA_NO_VIGENTE = prop.getProperty("BILLETERA.SALDO_NO_VIGENTE");
    //public static List<String> listaExtencion = new ArrayList<String>();

    public static final String DiasRetraso = prop.getProperty("DiasRetraso.Rango.Busqueda.CallSac");

    public static final String expresionRegularCorto = prop.getProperty("expresion_regular_corto");
    public static final String expresionRegularTexto = prop.getProperty("expresion_regular_texto");
    public static final String separadorOcteto = prop.getProperty("separador_octeto");
    public static final int meses_Consulta = Integer.parseInt(prop.getProperty("meses_consulta"));
    public static final int meses_Consulta_Historica = Integer.parseInt(prop.getProperty("meses_consulta_historica"));
    public static final String etiquetaMostrarSiempre = prop.getProperty("etiqueta.mostrarSiempre");
        public static final String dominios = prop.getProperty("dominios.aceptados");

    public static void init() {
        Thread thr = Thread.currentThread();
        ClassLoader cL;
        if (thr != null) {
            cL = thr.getContextClassLoader();
            if (cL != null) {
                configURL = cL.getResource(configFile);
                try {
                    configFileStream = new FileInputStream(configURL.getFile());
                    prop.load(configFileStream);
                } catch (FileNotFoundException f) {
                    log.error("FileNotFoundException: " + f.getMessage());
                } catch (IOException e) {
                    log.error("ERROR: " + e.getMessage());
                }

            }
        }
    }

    public static void main(String[] args) {
        Parameters.init();
    }

}
