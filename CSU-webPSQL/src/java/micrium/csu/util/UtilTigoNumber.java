/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class UtilTigoNumber {

    private static final Logger log = Logger.getLogger(UtilTigoNumber.class);

    private static int getErrorNroCelFone(String valorStr) {

        if (valorStr.equals("")) {
            return -1;
        }
        int k = 0;
        if (!esNumeroTIGO(valorStr)) {
            k = -1;
        }

        if (k == -1) {
            return -2;
        }

        try {
            return Integer.parseInt(valorStr);
        } catch (NumberFormatException nfe) {
            return -4;
        }
    }
//    public static int getErrorNroCelPhone(String valorStr){
//            
//           String strExt = Parameters.EXTENCIONES;
//           String[] vec = strExt.split(",");
//           List<String> listaExtencion = new ArrayList<String>();
//           listaExtencion.addAll(Arrays.asList(vec));
//            
//        
//           if(valorStr.equals("")){
//                 return -1;    
//            }
//            
//            if(valorStr.length()<8)
//                return -3;
//            String str=valorStr.substring(0, 2);
//            
//            
//            int k=listaExtencion.indexOf(str);
//            
//            if(k==-1)
//                return -2;
//            
//            
//            
//           try {
//                return Integer.parseInt(valorStr);
//            } catch (NumberFormatException nfe){
//                return -4;
//            } 
//     }

    public static String tipoError(int idTipo, int fila) {
        String message = "";
        if (idTipo == -1) {
            message = "ERROR En la fila nro:" + fila + " esta vacio";
            return message;
        }

        if (idTipo == -2) {
            message = "ERROR En la fila nro:" + fila + " no es un numero TIGO";
            return message;
        }

        if (idTipo == -3) {
            message = "ERROR En la fila nro:" + fila + " NO tiene los 8 digitos";
            return message;
        }
        if (idTipo == -4) {
            message = "ERROR En la fila nro:" + fila + " no es numerico tiene un caracter";
            return message;
        }

        message = "ERROR El archivo esta vacio";
        return message;
    }

//     public static List<NroCel>  getNrosCelPhone(String strNros){
//        
//        List<NroCel>  listaNros=new ArrayList<NroCel>();
//        strNros=strNros.substring(0, strNros.length()-1);
//        strNros=strNros.trim();
//        strNros=strNros.replaceAll("\n", ",");
//        if(strNros.length()<7){
//             NroCel nCel = new NroCel();
//             nCel.setNro("-1");
//             nCel.setCodeError(-1);
//             listaNros.add(nCel);
////             listaNros.add(-5);
////             listaNros.add(-1);
//             return listaNros;
//        }
//        //String strExt=Parameters.EXTENCIONES;
////        String[] vec=strExt.split(",");
////        List<String> listaExtencion=new ArrayList<String>();
////        listaExtencion.addAll(Arrays.asList(vec));
//        
//        String[]  strVec=strNros.split(",");
//        int n=strVec.length; 
//        for (int i = 0; i <n ; i++) {
//            String string=strVec[i];
//            string =string.trim();
//            log.debug("|getNrosCelFon|valor recuperado de archivo:|"+string+"|");
//            int k=getErrorNroCelFone(string);
//            log.debug("|getNrosCelFon|valor="+string+"|codError="+k);
//                 NroCel nc  = new NroCel();
//                 nc.setNro("-1");
//                 nc.setCodeError(-1);
//                if(listaNros.indexOf(nc)==-1){
//                   NroCel nCel = new NroCel();
//                   nCel.setNro(string);
//                   nCel.setCodeError(k);
//                   listaNros.add(nCel);    
//                }
//                   
////            else{
////                listaNros.add(0,i+1);
////                listaNros.add(0,k);
////                i=n;
////             }    
//        }
//        return listaNros;
//    }
    public static List<Integer> getNrosCelFon(String strNros) {

        List<Integer> listaNros = new ArrayList<Integer>();
        strNros = strNros.substring(0, strNros.length() - 1);
        strNros = strNros.trim();
        strNros = strNros.replaceAll("\n", ",");
        if (strNros.length() < 7) {
            listaNros.add(-5);
            listaNros.add(-1);
            return listaNros;
        }
//        String strExt = Parameters.EXTENCIONES;
//        String[] vec = strExt.split(",");
//        List<String> listaExtencion = new ArrayList<String>();
//        listaExtencion.addAll(Arrays.asList(vec));

        String[] strVec = strNros.split(",");
        int n = strVec.length;
        for (int i = 0; i < n; i++) {
            String string = strVec[i];
            string = string.trim();
            int k = getErrorNroCelFone(string);
            if (k > 0) {
                if (listaNros.indexOf(k) == -1) {
                    listaNros.add(k);
                } else {
                    listaNros.add(0, i + 1);
                    listaNros.add(0, k);
                    i = n;
                }
            }
        }
        return listaNros;
    }
    //-----------------------------------------------------

    //-----------------------------------------------------
//    public static boolean extencionCorrecta(String name){
//        List<String>   listaExtencion = new ArrayList<String>();
//        String[] vec=Parameters.FILE_EXTENCION.split(",");
//       listaExtencion.addAll(Arrays.asList(vec));
//        
//        int k=name.lastIndexOf(".");
//        String str=name.substring(k+1);
//        str=str.toLowerCase();
//        k=listaExtencion.indexOf(str);
//        
//        return   (k!=-1)? true : false; 
//    }
    //--------------------------------------------------------
    public static boolean esNumeroTIGO(String dato) {
        Pattern pat = Pattern.compile(Parameters.ER_NrosTIGO);
        //Pattern pat = Pattern.compile("^[a-zA-Z0-9_-]{2,}@[a-zA-Z0-9_-]{2,}\\.[a-zA-Z]{2,4}(\\.[a-zA-Z]{2,4})?$");
        Matcher mat = pat.matcher(dato);
        if (mat.find()) {
            //System.out.println(""+dato+" -- yes");
            return true;
            //JOptionPane.showMessageDialog(null, "el texto escrito es un correo");
        } else {
            // JOptionPane.showMessageDialog(null, "el texto escrito NO! es un correo");
            return false;
        }

    }

}
