/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import com.opencsv.CSVWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import micrium.csu.model.ReporteConsulta;
import micrium.csu.model.ReporteConsultaGroup;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author HP
 */
public class UtilExcel {

    private static final Logger LOG = Logger.getLogger(UtilExcel.class);

    public static byte[] writeExcel(List<Object[]> objectList) throws IOException {

        //String excelFileName = "D:/Test.xlsx";//name of excel file
        String sheetName = "Sheet1";//name of sheet

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet(sheetName);
        Map<String, CellStyle> styles = createStyles(wb);
        int contRow = 0;
        for (Object[] obj : objectList) {
            XSSFRow row = sheet.createRow(contRow);
            int contCell = 0;

            for (Object s : obj) {

                XSSFCell cell = row.createCell(contCell);

                if (contRow == 0) {
                    cell.setCellValue(s.toString());
                    cell.setCellStyle(styles.get("header"));
                } else {
                    /*if (nomAct.equals(nomAnt) && contCell == 0) {
                        cell.setCellValue("");
                        cell.setCellStyle(styles.get("cell_normal_rol"));
                    }
                    if (descripAct.equals(descripAnt) && contCell == 1) {
                        cell.setCellValue("");
                        cell.setCellStyle(styles.get("cell_normal_rol"));
                    } else {*/
                    cell.setCellValue(s.toString());
                    /*  if (contCell == 0 || contCell==1) {
                            cell.setCellStyle(styles.get("cell_normal_rol"));
                            nomAnt = nomAct;
                            descripAnt = descripAct;
                        } else {
                            cell.setCellStyle(styles.get("cell_normal"));
                        }*/
                    //}
                }

                contCell++;
            }
            contRow++;
        }

        /*
        sheet.addMergedRegion(new CellRangeAddress(1, contRow - 1, 0, 0));
        sheet.addMergedRegion(new CellRangeAddress(1, contRow - 1, 1, 1));*/
 /*//iterating r number of rows
        for (int r = 0; r < 5; r++) {
            XSSFRow row = sheet.createRow(r);

            //iterating c number of columns
            for (int c = 0; c < 5; c++) {
            for (int c = 0; c < 5; c++) {
                XSSFCell cell = row.createCell(c);

                cell.setCellValue("Cell " + r + " " + c);
            }
        }*/
        // FileOutputStream fileOut = new FileOutputStream(excelFileName);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        wb.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();

        //write this workbook to an Outputstream.
/*        wb.write(fileOut);
        fileOut.flush();
        fileOut.close();*/
    }

    private static Map<String, CellStyle> createStyles(Workbook wb) {
        Map<String, CellStyle> styles = new HashMap<>();
        DataFormat df = wb.createDataFormat();

        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put("header", style);

        Font font1 = wb.createFont();
        font1.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font1);
        styles.put("cell_b", style);

        Font font2 = wb.createFont();
        font2.setColor(IndexedColors.BLUE.getIndex());
        font2.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font2);
        styles.put("cell_bb", style);

        Font font3 = wb.createFont();
        font3.setFontHeightInPoints((short) 14);
        font3.setColor(IndexedColors.DARK_BLUE.getIndex());
        font3.setBold(true);
        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setFont(font3);
        style.setWrapText(true);
        styles.put("cell_h", style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setWrapText(true);
        styles.put("cell_normal", style);

        style = createBorderedStyle(wb);
        //style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setWrapText(true);
        styles.put("cell_normal_rol", style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        styles.put("cell_normal_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setWrapText(true);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_normal_date", style);

        style = createBorderedStyle(wb);
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setIndention((short) 1);
        style.setWrapText(true);
        styles.put("cell_indented", style);

        style = createBorderedStyle(wb);
        style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styles.put("cell_blue", style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb) {
        BorderStyle thin = BorderStyle.THIN;
        short black = IndexedColors.BLACK.getIndex();

        CellStyle style = wb.createCellStyle();
        style.setBorderRight(thin);
        style.setRightBorderColor(black);
        style.setBorderBottom(thin);
        style.setBottomBorderColor(black);
        style.setBorderLeft(thin);
        style.setLeftBorderColor(black);
        style.setBorderTop(thin);
        style.setTopBorderColor(black);
        return style;
    }

    public static byte[] generarReporte(List<ReporteConsultaGroup> lista) {
        try {
            Map<String, Object[]> data = new TreeMap<>();
            data.put("0", new Object[]{"TELEFONO", "FECHA", "SESSION", "SERVICIO", "CANAL", "OPCION", "TEXTO GENERADO"});
            int i = 1;
            List<ReporteConsulta> listaFinal = formatearLista(lista);
            for (ReporteConsulta rc : listaFinal) {
                data.put(String.valueOf(i), new Object[]{rc.getMsisdn(), rc.getFecha(), rc.getSessionId(), rc.getIdentificadorServicio(), rc.getNombreCanal(), rc.getOpcion(), rc.getTextoGenerado()});
                i++;
            }

            XSSFWorkbook workbook = new XSSFWorkbook();
            // Create a blank sheet
            XSSFSheet sheet = workbook.createSheet("Consultas " + UtilDate.dateToString(new Date()));
            int rownum = 0;
            for(Map.Entry<String, Object[]> entry :data.entrySet()){
                // this creates a new row in the sheet
                Row row = sheet.createRow(rownum++);
                Object[] objArr = entry.getValue();
                int cellnum = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row
                    Cell cell = row.createCell(cellnum++);
                    if (entry.getKey().equals("0")) {
                        CellStyle style = workbook.createCellStyle();

                        style.setAlignment(HorizontalAlignment.CENTER);
                        style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());

                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        Font font1 = workbook.createFont();
                        font1.setColor(IndexedColors.WHITE.getIndex());
                        font1.setBold(true);

                        style.setFont(font1);
                        cell.setCellStyle(style);
                    }

                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Timestamp) {

                        cell.setCellValue(UtilDate.timetampToStringPatter((Timestamp) obj, "dd-MM-YYYY HH:mm:ss"));
                    }
                }
            }

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            workbook.write(byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            LOG.error("[generarReporte] " + e.getMessage());
        }
        return null;
    }

    public static List<ReporteConsulta> formatearLista(List<ReporteConsultaGroup> lista) {
        List<ReporteConsulta> resultado = new ArrayList<>();
        try {
            for (ReporteConsultaGroup group : lista) {
                ReporteConsulta consulta = group.getGroup();
                StringBuilder opcion = new StringBuilder();
                StringBuilder textoGenerado = new StringBuilder();
                for (ReporteConsulta children : group.getChildrens()) {
                    consulta.setIdentificadorServicio(children.getIdentificadorServicio());
                    consulta.setNombreCanal(children.getNombreCanal());
                    opcion.append(children.getOpcion() != null ? children.getOpcion() : "").append(" ");
                    textoGenerado.append(children.getTextoGenerado()).append("\n");
                }
                consulta.setOpcion(opcion.toString().trim());
                consulta.setTextoGenerado(textoGenerado.toString());
                resultado.add(consulta);
            }
        } catch (Exception e) {
            LOG.error("[formatearLista] " + e.getMessage()
            );
        }

        return resultado;
    }

    public static byte[] generarReporteCsv(List<ReporteConsultaGroup> lista) {

        try {
            List<ReporteConsulta> listaFinal = formatearLista(lista);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            OutputStreamWriter streamWriter = new OutputStreamWriter(stream, StandardCharsets.UTF_8);

            OutputStream out;

            CSVWriter writer = new CSVWriter(streamWriter);
            String[] titulo = {"TELEFONO", "FECHA", "SESSION", "SERVICIO", "CANAL", "OPCION", "TEXTO GENERADO"};

            writer.writeNext(titulo);
            for (ReporteConsulta rc : listaFinal) {
                writer.writeNext(new String[]{rc.getMsisdn(), UtilDate.timetampToStringPatter(rc.getFecha(), "dd-MM-YYYY HH:mm:ss"), rc.getSessionId().trim(),rc.getIdentificadorServicio(), rc.getNombreCanal(), rc.getOpcion(), rc.getTextoGenerado().replace("\n", "")});
            }

            // ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //OutputStreamWriter streamWriter = new OutputStreamWriter(stream);
            // CSVWriter = new CSVWriter(streamWriter);
            streamWriter.flush();

            return stream.toByteArray();

        } catch (IOException ex) {
            LOG.error(ex.getMessage());
        }
        return null;
    }

}
