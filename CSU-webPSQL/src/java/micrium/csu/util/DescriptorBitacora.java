/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

/**
 *
 * @author jose_luis
 */
public enum DescriptorBitacora {

    GRUPO("Gestor Grupo Billetera"),
    BANCO("Gestor Bancos"),
    BILLETERA("Gestor Billeteras"),    
    PREGUNTA("Gestor Preguntas"),
    PARTICIPANTES_REPORTE("Reporte Encuestados"),
    UNITTYPE("Gestor Unit Types"),
    COS("Gestor COS"),
    ACUMULADOR("Gestor ACUMULADOR"),
    GRUPO_BILLETERAS("Gestionar Billetera Agrupada"),
    CONFIGURACION("Gestionar Configuracion"),
    OPCIONES_GENERALES("Opciones Generales"),
    OPCIONES_DPI("Opciones DPI"),
    REPORTE_CONSULTAS("Reporte Consultas");
    private  final String formulario;

    DescriptorBitacora(String detalle){
          this.formulario=detalle;
    }

    @Override
    public String toString(){
      return formulario;
    }

}
