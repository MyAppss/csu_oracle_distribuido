/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import org.apache.log4j.Logger;
import org.apache.regexp.RE;
import org.apache.regexp.RESyntaxException;

/**
 *
 * @author Vehimar
 */
public class Validador {

    private static final Logger log = Logger.getLogger(Validador.class);

    public static boolean validateRequired(String value) {
        boolean isFieldValid = false;
        if (value != null && value.trim().length() > 0) {
            isFieldValid = true;
        }
        return isFieldValid;
    }

    public static boolean validateInt(String value) {
        boolean isFieldValid = false;
        try {
            Integer aux = Integer.parseInt(value);
            isFieldValid = true;
            log.info("validateInt :" + aux);
        } catch (NumberFormatException e) {
            isFieldValid = false;
            log.error("ERROR: " + e.getMessage());
        }
        return isFieldValid;
    }

    public static boolean validateLength(String value, int minLength, int maxLength) {
        String validatedValue = value;
        if (!validateRequired(value)) {
            validatedValue = "";
        }
        return (validatedValue.length() >= minLength && validatedValue.length() <= maxLength);
    }

    public static boolean matchPattern(String value, String expression) throws RESyntaxException {
        boolean match = false;
        if (validateRequired(expression)) {
            RE r = new RE(expression);
            match = r.match(value);
        }
        return match;
    }

}
