/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import micrium.csu.bussines.NodoBUSSI;
import micrium.csu.model.Nodo;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class RecargarServicios extends Thread {

    private static final Logger log = Logger.getLogger(RecargarServicios.class);

    @Override
    public void run() {
        super.run();
        /* log.info("Inicio Recargar Nodos");
        recargar();
        log.info("Finalizo Recargar Nodos");
         */
    }

    public void recargar() {
        NodoBUSSI bussi = GuiceInjectorSingleton.getInstance(NodoBUSSI.class);
        List<Nodo> listaNodos = bussi.obtenerLista();
        for (Nodo nodo : listaNodos) {
            log.info("url nodo: " + nodo.toString());
            if (recargarNodo(nodo)) {
                log.info("Nodo " + nodo.getUrl() + " Recargdo con Exito");
            } else {
                log.info("Nodo " + nodo.getUrl() + " Sin conexion");
            }

        }
    }

    public boolean recargarNodo(Nodo nodo) {
        boolean valor = false;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;

        try {
     
            URL url = new URL(nodo.getUrl());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            // Send request
            // DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            // wr.writeBytes(urlParameters);
            // wr.close();
            // Get Response
            // InputStream inputStream = connection.getInputStream();
            try {
                inputStream = connection.getInputStream();
            } catch (FileNotFoundException e) {
                log.error("Error de conexion al servlet: " + nodo.getUrl() + " [ERROR] " + e.getMessage());
                //  throw (new Exception("Error de conexion al servlet: " + nodo.toString()));
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            log.info("respuesta del servlet " + nodo.getUrl() + ": " + response);
            valor = true;
        } catch (IOException e) {
            log.error("Error al hacer request al servlet: " + nodo.getUrl(), e);
            //    errores.add("Error al hacer request al servlet: " + nodo.toString() + ", " + e.getMessage());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    log.error("[recargarNodo] " + ex.getMessage());
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    log.error("[recargarNodo] " + ex.getMessage());
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return valor;

    }

    

   
}
