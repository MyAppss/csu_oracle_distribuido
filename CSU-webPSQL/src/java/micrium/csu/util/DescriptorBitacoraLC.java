/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.util;

/**
 *
 * @author jose_luis
 */
public enum DescriptorBitacoraLC {

    CABECERA("Gestor Cabecera"),
    GRUPO("Gestor Grupo Billetera"),
    BANCO("Gestor Bancos"),
    BILLETERA("Gestor Billeteras"),
    PREGUNTA("Gestor Preguntas"),
    PARTICIPANTES_REPORTE("Reporte Encuestados"),
    UNITTYPE("Gestor Unit Types"),
    COS("Gestor COS"),
    CORTO("Gestor CORTO"),
    ORIGEN("Gestor ORIGEN"),
    ACUMULADOR("Gestor ACUMULADOR"),
    MAPEO_OFERTAS("Mapeo de Ofertas"),
    NODO("Nodo"),
    GRUPO_BILLETERAS("Gestionar Billetera Agrupada"),
    CONFIGURACION("Gestionar Configuracion"),
    OPCIONES_GENERALES("Opciones Generales"),
    OPCIONES_DPI("Opciones DPI"),
    OPCIONES_ACUMULADOS_MEGAS("Opciones Acumulados Megas"),
    REPORTE_CONSULTAS("Reporte Consultas"),
    CATEGORIA("Gestor Categorias"),
    MENU("Gestor Menus");

    private final String formulario;

    DescriptorBitacoraLC(String detalle) {
        this.formulario = detalle;
    }

    @Override
    public String toString() {
        return formulario;
    }

}
