package micrium.csu.daoHistorica;

import java.util.List;
import micrium.csu.model.NodoBusquedaReporteConsultas;
import  micrium.csu.model.ReporteConsulta;

/**
 *
 * @author marciano
 */
public interface ReporteConsultaDAO {

    public ReporteConsulta obtenerId(int id);
    public ReporteConsulta obtenerName(String name);
    public List<ReporteConsulta> obtenerLista();
    public List<ReporteConsulta> obtenerListaByIsdn(String isdn);
    public List<ReporteConsulta> obtenerListaByMsisdnDate(NodoBusquedaReporteConsultas nod);
    public void update(ReporteConsulta dato) throws Exception;
    public void insert(ReporteConsulta dato) throws Exception;
    public void delete(int id) throws Exception;
    public int obtenerIdSecuencia();
    public List<ReporteConsulta> obtenerListaByMsidnGroup(NodoBusquedaReporteConsultas nod);
    public List<ReporteConsulta> obtenerListaByMsidnSessionID(NodoBusquedaReporteConsultas nod);

}
