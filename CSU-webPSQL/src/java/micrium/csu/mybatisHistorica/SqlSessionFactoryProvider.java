package micrium.csu.mybatisHistorica;

import micrium.csu.mybatis.*;
import com.google.inject.Provider;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

/**
 *
 * @author Ivan
 */
public class SqlSessionFactoryProvider implements Provider<SqlSessionFactory> {
    
    private static final Logger LOG = Logger.getLogger(SqlSessionFactoryProvider.class);
    
    @Override
    public SqlSessionFactory get() {
        Reader reader = null;
        SqlSessionFactory sqlMapper = null;
        try {
            String resource = "micrium/csu/mybatisHistorica/Configuration.xml";
            reader = Resources.getResourceAsReader(resource);
            sqlMapper = new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException ex) {
            LOG.error("error " + ex.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                 LOG.error("error " + ex.getMessage());
            }
        }
        return sqlMapper;
    }

}
