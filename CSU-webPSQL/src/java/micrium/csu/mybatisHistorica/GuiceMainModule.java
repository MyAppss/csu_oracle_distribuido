package micrium.csu.mybatisHistorica;


import com.google.inject.AbstractModule;
import micrium.csu.bussinesHistorica.ReporteConsultaBUSSI;
import micrium.csu.bussinesHistorica.ReporteConsultaBUSSIImpl;
import micrium.csu.daoHistorica.ReporteConsultaDAO;
import micrium.csu.daoHistorica.ReporteConsultaDAOImpl;


import org.apache.ibatis.session.SqlSessionFactory;

/**
 *
 * @author Ivan
 */
public class GuiceMainModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(SqlSessionFactory.class).toProvider(SqlSessionFactoryProvider.class);
        bind(ReporteConsultaBUSSI.class).to(ReporteConsultaBUSSIImpl.class);
        bind(ReporteConsultaDAO.class).to(ReporteConsultaDAOImpl.class);

       
    }
}
