/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import micrium.csu.dao.BilleteraDAO;
import micrium.csu.dao.ConfigAcumuladoDAO;
import micrium.csu.model.Billetera;
import micrium.csu.model.ConfigAcumulado;
import micrium.csu.model.ConfigBilleteraAcumulado;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class ConfigAcumuladoBUSSIImpl implements ConfigAcumuladoBUSSI, Serializable {

    @Inject
    private ConfigAcumuladoDAO dao;
    @Inject
    private BilleteraDAO daoBilletera;

    private static final Logger log = Logger.getLogger(ConfigAcumuladoBUSSIImpl.class);

    @Override
    public List<ConfigAcumulado> listByIdConfig(int idConfig) {
        return dao.listByIdConfig(idConfig);
    }

    @Override
    public boolean update(ConfigAcumulado dato) throws Exception {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Cabecera con Id:" + dato.getConfigAcumuladoId(), e);
        }
        return sw;
    }

    @Override
    public int insert(ConfigAcumulado dato) throws Exception {
        int id = dao.obtenerIdSecuencia();
        try {
            if (id > 0) {
                dato.setConfigAcumuladoId(id);
                dao.insert(dato);
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
            id = 0;
        }
        return id;

    }

    private ConfigAcumulado getAcumulado(List<ConfigAcumulado> acumulados, Billetera billetera) {

        for (ConfigAcumulado acum : acumulados) {
            if (acum.getBilleteraId().equals(billetera.getBilleteraId())) {
                //cba.setConfigAcumulado(acum);
                return acum;
            }
        }
        return null;
    }

    @Override
    public List<ConfigBilleteraAcumulado> listByIdConfigBilletera(int idConfig) {
        List<ConfigBilleteraAcumulado> billetaraAcumulados = new ArrayList<>();
        List<ConfigAcumulado> acumulados = listByIdConfig(idConfig);
        List<Billetera> billeteras = daoBilletera.obtenerLista();
        for (int i = 0; i < billeteras.size(); i++) {
            Billetera billetera = billeteras.get(i);            
            //if (billetera.getAcumulado() != null && billetera.getAcumulado().equals("t")) {
            ConfigBilleteraAcumulado cba = new ConfigBilleteraAcumulado();
            ConfigAcumulado aux=null;
            cba.setBilletera(billetera);
            if (acumulados != null && !acumulados.isEmpty()) {
                /*for (ConfigAcumulado acum : acumulados) {
                    if (acum.getBilleteraId().equals(billetera.getBilleteraId())) {
                        cba.setConfigAcumulado(acum);
                        break;
                    }
                }*/
                aux = getAcumulado(acumulados, billetera);
                if (aux != null) {
                    cba.setConfigAcumulado(aux);
                }
            } 
            
            if(aux==null){
                ConfigAcumulado newAcumulado = new ConfigAcumulado();
                newAcumulado.setNombreComercial(billetera.getNombreComercial());
                cba.setConfigAcumulado(newAcumulado);
            }
            
            if (cba.getConfigAcumulado() != null) {
                if (billetera.getAcumulado() != null
                        && billetera.getAcumulado().equals("t")
                        && (cba.getConfigAcumulado().getNombreAcumulado() == null
                        || (cba.getConfigAcumulado().getNombreAcumulado() != null && "".equals(cba.getConfigAcumulado().getNombreAcumulado().trim())))) {

                    cba.getConfigAcumulado().setNombreAcumulado(billetera.getNombreComercial() + " " + Parameters.BILLETERA_NO_VIGENTE);
                }
            }
            if (cba.getConfigAcumulado() != null) {
                if (cba.getConfigAcumulado().getSegundaFechaExp() == null
                        || (cba.getConfigAcumulado().getSegundaFechaExp() != null
                        && (cba.getConfigAcumulado().getSegundaFechaExp().equals("f") || cba.getConfigAcumulado().getSegundaFechaExp().equals("false")))) {

                    cba.getConfigAcumulado().setSegundaFechaExp("false");
                } else {
                    cba.getConfigAcumulado().setSegundaFechaExp("true");
                }
                billetaraAcumulados.add(cba);
            }
            //}
        }        
        return billetaraAcumulados;
    }

    @Override
    public boolean saveListConfigBilleteraAcumulado(List<ConfigBilleteraAcumulado> acumulados, Integer idConfig) {
        try {
            for (ConfigBilleteraAcumulado acumulado : acumulados) {
                if (acumulado.getConfigAcumulado().getBilleteraId() == null || acumulado.getConfigAcumulado().getBilleteraId() == 0) {
                    //NUEVO
                    acumulado.getConfigAcumulado().setBilleteraId(acumulado.getBilletera().getBilleteraId());
                    acumulado.getConfigAcumulado().setConfigId(idConfig);
                    acumulado.getConfigAcumulado().setConfigAcumuladoId(dao.obtenerIdSecuencia());

                    if (acumulado.getConfigAcumulado().getSegundaFechaExp() == null
                            || (acumulado.getConfigAcumulado().getSegundaFechaExp() != null && acumulado.getConfigAcumulado().getSegundaFechaExp().equals("false"))) {
                        acumulado.getConfigAcumulado().setSegundaFechaExp("f");
                    } else {
                        acumulado.getConfigAcumulado().setSegundaFechaExp("t");
                    }
                    dao.insert(acumulado.getConfigAcumulado());
                } else {
                    //ACTUALIZAR
                    if (acumulado.getConfigAcumulado().getSegundaFechaExp() == null
                            || (acumulado.getConfigAcumulado().getSegundaFechaExp() != null && acumulado.getConfigAcumulado().getSegundaFechaExp().equals("false"))) {
                        acumulado.getConfigAcumulado().setSegundaFechaExp("f");
                    } else {
                        acumulado.getConfigAcumulado().setSegundaFechaExp("t");
                    }
                    dao.update(acumulado.getConfigAcumulado());
                }
            }
            return true;

        } catch (Exception ex) {
            log.error("[insert]Error al insertar", ex);
            return false;
        }

    }

}
