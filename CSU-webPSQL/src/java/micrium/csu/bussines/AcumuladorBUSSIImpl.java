/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.AcumuladorDAO;
import micrium.csu.model.Acumulador;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class AcumuladorBUSSIImpl implements AcumuladorBUSSI, Serializable {

    @Inject
    private AcumuladorDAO dao;
    private static final Logger log = Logger.getLogger(AcumuladorBUSSIImpl.class);

    @Override
    public Acumulador obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public Acumulador obtenerByName(String name) {
        return dao.obtenerName(name);
    }

    @Override
    public List<Acumulador> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Acumulador dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Acumulador con Id:" + dato.getAcumuladorId(), e);
        }
        return sw;
    }

    @Override
    public boolean insert(Acumulador dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setAcumuladorId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Acumulador con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(Acumulador dato, String idStr) {
        if (dato.getNombre().isEmpty()) {
            return ParameterMessage.Acumulador_ERROR_nombre;
        }
        if (dato.getLimite() == null || dato.getLimite() < 0) {
            return ParameterMessage.Acumulador_ERROR_limite;
        }

        Acumulador datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getAcumuladorId()) {
                if (dato.getNombre().equals(datoAux.getNombre())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }
}
