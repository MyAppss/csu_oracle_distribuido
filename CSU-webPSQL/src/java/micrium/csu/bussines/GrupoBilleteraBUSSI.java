package micrium.csu.bussines;

import  java.util.List;
import micrium.csu.model.GrupoBilletera;
/**
 *
 * @author marciano
 */
public interface GrupoBilleteraBUSSI {
    
    public GrupoBilletera obtenerId(int id);
    public List<GrupoBilletera> obtenerLista();
    public List<GrupoBilletera> obtenerListaByIdBilletera(int IdBanco);
    public boolean update(GrupoBilletera dato);
    public boolean insert(GrupoBilletera dato);
    public boolean delete(int id);
    public String validate(GrupoBilletera dato,String idStr);
}
