package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Parametro;
/**
 *
 * @author marciano
 */
public interface ParametroBUSSI {
    
    public Parametro obtenerId(int id);
    public Parametro obtenerName(String name);
    public List<Parametro> obtenerLista();
    public boolean update(Parametro dato);
    public boolean insert(Parametro dato);
    public boolean delete(int id);
    public String validate(Parametro dato,String idStr);
    public void updateListaParametros(List<Parametro> list); 
    public void updateChangeConfiguration(String nombre);

//bind(ParametroBUSSI.class).to(ParametroBUSSIImpl.class);	
}
