package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.ConfigComposicionBilletera;
/**
 *
 * @author marciano
 */
public interface ConfigComposicionBilleteraBUSSI {
    
    public ConfigComposicionBilletera obtenerId(int id);
    public List<ConfigComposicionBilletera> obtenerLista();
    public boolean update(ConfigComposicionBilletera dato);
    public boolean insert(ConfigComposicionBilletera dato);
    public boolean delete(int id);
    public String validate(ConfigComposicionBilletera dato,String idStr);
    public boolean existe(ConfigComposicionBilletera dato);
    public boolean updateOffAllByIdConfig(int configId);

//bind(ConfigComposicionBilleteraBUSSI.class).to(CosComposicionBilleteraBUSSIImpl.class);	
}
