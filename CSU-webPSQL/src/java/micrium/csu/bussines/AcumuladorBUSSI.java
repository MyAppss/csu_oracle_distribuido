/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.Acumulador;

/**
 *
 * @author User
 */
public interface AcumuladorBUSSI {

    public Acumulador obtenerId(int id);

    public Acumulador obtenerByName(String name);

    public List<Acumulador> obtenerLista();

    public boolean update(Acumulador dato);

    public boolean insert(Acumulador dato);

    public boolean delete(int id);

    public String validate(Acumulador dato, String idStr);
}
