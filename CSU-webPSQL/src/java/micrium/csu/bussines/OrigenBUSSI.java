package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Origen;
/**
 *
 * @author marciano
 */
public interface OrigenBUSSI {
    
    public Origen obtenerId(int id);
    public List<Origen> obtenerLista();
    public boolean update(Origen dato);
    public boolean insert(Origen dato);
    public boolean delete(int id);
    public String validate(Origen dato,String idStr);

//bind(OrigenBUSSI.class).to(OrigenBUSSIImpl.class);	
}
