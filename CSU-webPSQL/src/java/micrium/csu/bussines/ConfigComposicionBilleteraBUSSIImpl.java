package micrium.csu.bussines;

import micrium.csu.model.ConfigComposicionBilletera;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.ConfigComposicionBilleteraDAO;
//import micrium.csu.dao.CosComposicionBilleteraDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigComposicionBilleteraBUSSIImpl implements ConfigComposicionBilleteraBUSSI, Serializable {

    @Inject
    private ConfigComposicionBilleteraDAO dao;
    private static final Logger log = Logger.getLogger(ConfigComposicionBilleteraBUSSIImpl.class);
    
    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar CosComposicionBilletera con Id:" + id + " ", e);            
        }
        return sw;        
    }
    
    @Override
    public boolean insert(ConfigComposicionBilletera dato) {
        boolean sw = false;
        try {
            //int id=dao.obtenerIdSecuencia();
//            if(id>0){
            //dato.setCosComposicionBilleteraId(id);
            dao.insert(dato);
            sw = true;
//             }else
            log.info("[insert] insertando registro Cos_composicion_billetera");            
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);            
        }
        return sw;        
    }
    
    @Override
    public ConfigComposicionBilletera obtenerId(int id) {
        return dao.obtenerId(id);
    }
    
    @Override
    public List<ConfigComposicionBilletera> obtenerLista() {
        return dao.obtenerLista();
    }
    
    @Override
    public boolean update(ConfigComposicionBilletera dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar CosComposicionBilletera con Id:" + dato.getConfigId(), e);            
        }
        return sw;        
    }
    
    @Override
    public String validate(ConfigComposicionBilletera dato, String idStr) {
        
        if (dato.getConfigId() > 0) {
            return ParameterMessage.CosComposicionBilletera_ERROR_cosId;
        }
        if (dato.getNombreComercial().isEmpty()) {
            return ParameterMessage.CosComposicionBilletera_ERROR_nombreComercial;
        }


//        ConfigComposicionBilletera  datoAux=dao.obtenerName(dato.);
//        if(datoAux==null)
//                return "";
//
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getCosComposicionBilleteraId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
        return "este Nombre ya existe";
    }

    @Override
    public boolean updateOffAllByIdConfig(int configId) {
        boolean sw = false;
        try {            
            dao.updateOffAllByIdConfig(configId);
            sw = true;
        } catch (Exception e) {
            log.error("[updateOffAllByIdConfig]Error al modificar Config con Id:" + configId, e);            
        }
        return sw;        
    }    

    @Override
    public boolean existe(ConfigComposicionBilletera dato) {
        try {
            ConfigComposicionBilletera con = dao.obtenerbyIdCompBilleteraAndConfig(dato);
            if (con != null) {
                if (con.getConfigId().intValue() == dato.getConfigId().intValue()) {
                    return true;
                }
            }
            
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    /* parametros
     PPPPMMMMTTTT
     */

    /* properties
     PPPPRRRRSSSS
     */
}
