/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.MapeoOfertasDAO;
import micrium.csu.model.MapeoOfertas;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class MapeoOfertasBUSSIImpl implements MapeoOfertasBUSSI, Serializable {

    @Inject
    private MapeoOfertasDAO dao;
    private static final Logger log = Logger.getLogger(MapeoOfertasBUSSIImpl.class);

    @Override
    public MapeoOfertas obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public MapeoOfertas obtenerByName(String name) {
        return dao.obtenerName(name);
    }

    @Override
    public List<MapeoOfertas> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(MapeoOfertas dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar MapeoOferta con Id:" + dato.getId_mapeo(), e);
        }
        return sw;
    }

    @Override
    public boolean insert(MapeoOfertas dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setId_mapeo(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar MapeoOferta con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(MapeoOfertas dato, String idStr) {
        List<MapeoOfertas> lista = dao.obtenerLista();

        if (dato.getId_mapeo() == null && !idStr.isEmpty()) {
            dato.setId_mapeo(Integer.parseInt(idStr));
        } else {
            dato.setId_mapeo(0);
        }

        if (dato.getOfferingid() == null || dato.getOfferingid() == 0) {
            return ParameterMessage.MapeoOfertas_ERROR_OfferingId;
        } else {
            for (MapeoOfertas mapeo : lista) {
                if (dato.getId_mapeo().intValue() != mapeo.getId_mapeo().intValue() && dato.getOfferingid().intValue() == mapeo.getOfferingid().intValue()) {
                    return ParameterMessage.MapeoOfertas_MESSAGE_OFFERINGID_YA_EXISTE;
                }
       
            }
        }

        if (dato.getOfferingparent() != null && !dao.existeOfferingId(dato.getOfferingparent())) {
            return ParameterMessage.MapeoOfertas_MESSAGE_OFFERINGPARENT_NO_EXISTE;
        }

        if (dato.getOfferingname().isEmpty()) {
            return ParameterMessage.MapeoOfertas_ERROR_OfferingName;
        } else {
            for (MapeoOfertas mapeo : lista) {
                if (dato.getId_mapeo().intValue() != mapeo.getId_mapeo().intValue() && dato.getOfferingname().equals(mapeo.getOfferingname())) {
                    return ParameterMessage.MapeoOfertas_MESSAGE_OFFERINGNAME_YA_EXSTE;
                }
              
            }
        }

        if (dato.getType().isEmpty()) {
            return ParameterMessage.MapeoOfertas_ERROR_Type;
        }

        MapeoOfertas datoAux = dao.obtenerName(dato.getOfferingname());
        if (datoAux == null) {
            return "";
        }

        return "";
    }

}
