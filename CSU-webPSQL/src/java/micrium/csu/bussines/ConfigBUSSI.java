package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Config;
/**
 *
 * @author marciano
 */
public interface ConfigBUSSI {
    
    public Config obtenerId(int id);
    public Config obtenerByName(String name);
    public List<Config> obtenerLista();
    public boolean update(Config dato);
    public boolean insert(Config dato);
    public boolean delete(int id);
    public String validate(Config dato,String idStr);

//bind(ConfigBUSSI.class).to(ConfigBUSSIImpl.class);	
}
