/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.CabeceraDAO;
import micrium.csu.model.Cabecera;
import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class CabeceraBUSSIImpl implements CabeceraBUSSI, Serializable {

    @Inject
    private CabeceraDAO dao;
    private static final Logger log = Logger.getLogger(CabeceraBUSSIImpl.class);

    @Override
    public List<Cabecera> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Cabecera dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Cabecera con Id:" + dato.getCabeceraId(), e);
        }
        return sw;
    }

    @Override
    public Integer insert(Cabecera dato) {
        int id = dao.obtenerIdSecuencia();
        try {
            if (id > 0) {
                dato.setCabeceraId(id);
                dao.insert(dato);
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
            id = 0;
        }
        return id;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Cabecera con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(Cabecera dato, String idStr) {
        return "";
    }

    @Override
    public Cabecera obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Cabecera> obtenerByIdConfig(int idConf) {
        return dao.obtenerByIdConfiguracion(idConf);
    }

    @Override
    public boolean updatePos(Cabecera dato) {
         boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Cabecera con Id:" + dato.getCabeceraId(), e);
        }
        return sw;
    }

}
