package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Cos;
/**
 *
 * @author marciano
 */
public interface CosBUSSI {
    
    public Cos obtenerId(int id);
    public Cos obtenerByName(String name);
    public List<Cos> obtenerLista();
    public boolean update(Cos dato);
    public boolean insert(Cos dato);
    public boolean delete(int id);
    public String validate(Cos dato,String idStr);

//bind(CosBUSSI.class).to(CosBUSSIImpl.class);	
}
