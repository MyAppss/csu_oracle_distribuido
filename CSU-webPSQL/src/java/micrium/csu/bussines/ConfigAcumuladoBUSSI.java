/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.ConfigAcumulado;
import micrium.csu.model.ConfigBilleteraAcumulado;

/**
 *
 * @author Vehimar
 */
public interface ConfigAcumuladoBUSSI {
    public boolean update(ConfigAcumulado dato) throws Exception;
    public int insert(ConfigAcumulado dato) throws Exception;
    public List<ConfigAcumulado> listByIdConfig(int idConfig);
    public List<ConfigBilleteraAcumulado> listByIdConfigBilletera(int idConfig);
    public boolean saveListConfigBilleteraAcumulado(List<ConfigBilleteraAcumulado> acumulados,Integer idConfig);
    
       
}
