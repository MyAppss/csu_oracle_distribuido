package micrium.csu.bussines;

import micrium.csu.model.ConfigBilletera;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.ConfigBilleteraDAO;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ConfigBilleteraBUSSIImpl implements ConfigBilleteraBUSSI, Serializable {

    @Inject
    private ConfigBilleteraDAO dao;
    private static final Logger log = Logger.getLogger(ConfigBilleteraBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar ConfigBilletera con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(ConfigBilletera dato) {
        boolean sw = false;
        try {
            //int id=dao.obtenerIdSecuencia();
//            if(id>0){
            //dato.setCosBilleteraId(id);
            dao.insert(dato);
            sw = true;
//             }else
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public ConfigBilletera obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<ConfigBilletera> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(ConfigBilletera dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar CosBilletera con Id:" + dato.getBilleteraId(), e);
        }
        return sw;
    }
    //public boolean updateOffAllByIdConfig(int idConfig);

    @Override
    public boolean updateOffAllByIdConfig(int configId) {
        boolean sw = false;
        try {
            dao.updateOffAllByIdConfig(configId);
            sw = true;
        } catch (Exception e) {
            log.error("[updateOffAllByIdConfig]Error al modificar Config con Id:" + configId, e);
        }
        return sw;
    }

    @Override
    public String validate(ConfigBilletera dato, String idStr) {
//       
//        if(dato.getCosId() > 0)
//            return ParameterMessage.CosBilletera_ERROR_cosId;
//        if(dato.getNombreComercial().isEmpty())
//            return ParameterMessage.CosBilletera_ERROR_nombreComercial;


        //CosBilletera  datoAux=dao.obtenerName(dato.getBilleteraId());
//        if(datoAux==null)
//                return "";
//
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getCosBilleteraId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
        return "este Nombre ya existe";
    }
    //public boolean existe(ConfigBilletera dato);

    @Override
    public boolean existe(ConfigBilletera dato) {
        try {
            ConfigBilletera con = dao.obtenerbyIdBilleteraAndConfig(dato);
            if (con != null) {
                if (con.getConfigId().intValue() == dato.getConfigId().intValue()) {
                    return true;
                }
            }

        } catch (Exception e) {
            return false;
        }
        return false;
    }

    /* parametros
     PPPPMMMMTTTT
     */

    /* properties
     PPPPRRRRSSSS
     */
}
