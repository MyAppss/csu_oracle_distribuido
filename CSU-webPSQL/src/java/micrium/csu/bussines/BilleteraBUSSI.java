package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Billetera;
/**
 *
 * @author marciano
 */
public interface BilleteraBUSSI {
    
    public Billetera obtenerId(int id);
    public List<Billetera> obtenerLista();
    public boolean update(Billetera dato);
    public boolean insert(Billetera dato);
    public boolean delete(int id);
    public String validate(Billetera dato,String idStr);
    public List<Billetera> obtenerListaByIdConfig(int idConfig);
    public List<Billetera> obtenerListaByIdConfigMenu(int idUnitType,int idConfig);
    public List<Billetera> obtenerListaByIdConfigCabecera(int idUnitType,int idConfig);
    public List<Billetera> obtenerListaByIdUnitType(int idUnitType);
    public List<Billetera> obtenerListaByIdUnitTypeSinAlco(int idUnitType);
    public List<Billetera> obtenerListaByIdCompBilletera(int idCompoBilletera);
    public List<Billetera> obtenerListaByIdCabecera(int idCabecera);
    public List<Billetera> obtenerListaByIdMenu(int idMenu);

//bind(BilleteraBUSSI.class).to(BilleteraBUSSIImpl.class);	
}
