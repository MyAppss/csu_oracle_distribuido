/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.DetalleMenu;

/**
 *
 * @author Vehimar
 */
public interface DetalleMenuBUSSI {
    public List<DetalleMenu> obtenerIdCabecera(int id);
    public List<DetalleMenu> obtenerLista();
    public boolean update(DetalleMenu dato);
    public boolean insert(DetalleMenu dato);
    public boolean delete(int id);
    public String validate(DetalleMenu dato,String idStr);
    
    
}
