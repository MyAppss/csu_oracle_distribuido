package micrium.csu.bussines;

import micrium.csu.model.Origen;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.OrigenDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;
/**
 *
 * @author marciano
 */
public class OrigenBUSSIImpl implements OrigenBUSSI, Serializable {
    @Inject
    private OrigenDAO dao;
	
    private static final Logger log = Logger.getLogger(OrigenBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw=false;
        try {
            dao.delete(id);
            sw=true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Origen con Id:"+id+" "
                    ,e);             
        }
       return sw; 
    }

    @Override
    public boolean insert(Origen dato) {
         boolean sw=false;
        try {
            int id=dao.obtenerIdSecuencia();
            if(id>0){
                dato.setOrigenId(id);
                dao.insert(dato);
                sw=true;
             }else
               log.info("[insert] El valor secuencial es -1");              
        } catch (Exception e) {
            log.error("[insert]Error al insertar",e);             
        }
         return sw; 
    }

    @Override
    public Origen obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Origen> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Origen dato) {
         boolean sw=false;
        try {
            dao.update(dato);
            sw=true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Origen con Id:"+dato.getOrigenId()
                    ,e);             
        }
         return sw; 
    }

    @Override
    public String validate(Origen dato, String idStr) {
       
        if(dato.getNombre().isEmpty())
            return ParameterMessage.Origen_ERROR_nombre;
        if(dato.getDescripcion().isEmpty())
            return ParameterMessage.Origen_ERROR_descripcion;


        Origen  datoAux=dao.obtenerName(dato.getNombre());
        if(datoAux==null)
                return "";

        if(idStr!=null && !idStr.isEmpty()){
                int id=Integer.parseInt(idStr);
                if(id==datoAux.getOrigenId())
                   if(dato.getNombre().toUpperCase().equals(datoAux.getNombre().toUpperCase()))
                          return "";
        }
        return "este Nombre ya existe";
    }

/* parametros
PPPPMMMMTTTT
*/

/* properties
PPPPRRRRSSSS
*/
  
}
