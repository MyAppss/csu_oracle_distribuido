package micrium.csu.bussines;

import micrium.csu.model.Cos;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.CosDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class CosBUSSIImpl implements CosBUSSI, Serializable {

    @Inject
    private CosDAO dao;
    private static final Logger log = Logger.getLogger(CosBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Cos con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(Cos dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setCosId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public Cos obtenerId(int id) {
        return dao.obtenerId(id);
    }
    //public Cos obtenerByName(int id);

    @Override
    public Cos obtenerByName(String name) {
        return dao.obtenerName(name);
    }

    @Override
    public List<Cos> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Cos dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Cos con Id:" + dato.getCosId(), e);
        }
        return sw;
    }

    @Override
    public String validate(Cos dato, String idStr) {

        if (dato.getNombre().isEmpty()) {
            return ParameterMessage.Cos_ERROR_nombre;
        }
        if (dato.getDescripcion().isEmpty()) {
            return ParameterMessage.Cos_ERROR_descripcion;
        }


        Cos datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getCosId()) {
                if (dato.getNombre().equals(datoAux.getNombre())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }

    /* parametros
     PPPPMMMMTTTT
     */

    /* properties
     PPPPRRRRSSSS
     */
}
