/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.NodoDAO;
import micrium.csu.model.Nodo;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class NodoBUSSIImpl implements NodoBUSSI, Serializable {

    @Inject
    private NodoDAO dao;
    private static final Logger log = Logger.getLogger(NodoBUSSIImpl.class);

    @Override
    public Nodo obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Nodo> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Nodo dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Nodo con Id:" + dato.getNodo_id(), e);
        }
        return sw;
    }

    @Override
    public boolean insert(Nodo dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setNodo_id(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Nodo con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(Nodo dato, String idStr ,boolean edit) {
        List<Nodo> lista = dao.obtenerLista();

        if (dato.getNodo_id() == null && !idStr.isEmpty()) {
            dato.setNodo_id(Integer.parseInt(idStr));
        } else {
            dato.setNodo_id(0);
        }
        
        if(dato.getUrl()==null || dato.getUrl().isEmpty()){
            return "EL CAMPO URL ES OBLIGATORIO";
        }

        for (Nodo nodo : lista) {
            if (nodo.getUrl().equals(dato.getUrl()) && !edit) {
                return "EL NODO YA ESTA REGISTRADO";
            }
        }

        return "";

    }

}
