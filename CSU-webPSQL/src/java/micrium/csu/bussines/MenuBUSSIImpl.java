/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.MenuDAO;
import micrium.csu.model.Menu;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author Leandro
 */
public class MenuBUSSIImpl implements MenuBUSSI, Serializable {

    @Inject
    private MenuDAO dao;

    private static final Logger log = Logger.getLogger(MenuBUSSIImpl.class);

    @Override
    public Menu obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Menu> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Menu dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Menu con Id:" + dato.getMenuId(), e);
        }
        return sw;
    }

    @Override
    public Integer insert(Menu dato) {
        int id = 0;
        try {
            id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setMenuId(id);
                dao.insert(dato);
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
            id = 0;
        }
        return id;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Menu con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(Menu dato, String idStr) {
        if (dato.getNombre().isEmpty()) {
            return ParameterMessage.Categoria_ERROR_nombre;
        }

        Menu datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getMenuId()) {
                if (dato.getNombre().equals(datoAux.getNombre())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }

    @Override
    public List<Menu> obtenerListaByConfigId(int configId) {
        return dao.obtenerListaByConfigId(configId);
    }

    @Override
    public boolean updatePos(Menu dato) {
          boolean sw = false;
        try {
            dao.updatePos(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Menu con Id:" + dato.getMenuId(), e);
        }
        return sw;
    }
}
