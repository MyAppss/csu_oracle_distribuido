package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Corto;
/**
 *
 * @author marciano
 */
public interface CortoBUSSI {
    
    public Corto obtenerId(int id);
    public List<Corto> obtenerLista();
    public boolean update(Corto dato);
    public boolean insert(Corto dato);
    public boolean delete(int id);
    public String validate(Corto dato,String idStr);

//bind(CortoBUSSI.class).to(CortoBUSSIImpl.class);	
}
