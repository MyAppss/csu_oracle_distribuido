
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.MapeoOfertas;

/**
 *
 * @author User
 */
public interface MapeoOfertasBUSSI {

    public MapeoOfertas obtenerId(int id);
    public MapeoOfertas obtenerByName(String name);
    public List<MapeoOfertas> obtenerLista();
    public boolean update(MapeoOfertas dato);
    public boolean insert(MapeoOfertas dato);
    public boolean delete(int id);
    public String validate(MapeoOfertas dato, String idStr);
}
