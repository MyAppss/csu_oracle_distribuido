package micrium.csu.bussines;

import micrium.csu.model.Parametro;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.ParametroDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class ParametroBUSSIImpl implements ParametroBUSSI, Serializable {

    @Inject
    private ParametroDAO dao;

    private static final Logger log = Logger.getLogger(ParametroBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Parametro con Id:" + id + " ",
                    e);
        }
        return sw;
    }

    @Override
    public boolean insert(Parametro dato) {
        boolean sw = false;
        try {
            //int id=dao.obtenerIdSecuencia();
//            if(id>0){
            //dato.setParametroId(id);
            dao.insert(dato);
            sw = true;
            //}else
            log.info("[insert] El valor secuencial es -1");
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public Parametro obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Parametro> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Parametro dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Parametro con Id:" + dato.getNombre(),
                    e);
        }
        return sw;
    }

    @Override
    public String validate(Parametro dato, String idStr) {

        if (dato.getValor().isEmpty()) {
            return ParameterMessage.Parametro_ERROR_valor;
        }
//        if(dato.getDescripcion().isEmpty())
//            return ParameterMessage.Parametro_ERROR_descripcion;

        Parametro datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            //int id=Integer.parseInt(idStr);
//                if(id==datoAux.getNombre())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
            return "";
        }
        return "este Nombre ya existe";
    }

    @Override
    public void updateListaParametros(List<Parametro> list) {
        try {
            for (Parametro parametro : list) {
                dao.updateValueByName(parametro);
            }

        } catch (Exception e) {
        }

    }

    //    public void updateChangeConfiguration(String nombre);
    @Override
    public void updateChangeConfiguration(String nombre) {
        try {
            dao.updateChangeConfiguration(nombre);
        } catch (Exception e) {
            log.error("[updateChangeConfiguration]", e);
        }
    }

    @Override
    public Parametro obtenerName(String name) {
        return dao.obtenerName(name);
    }

}
