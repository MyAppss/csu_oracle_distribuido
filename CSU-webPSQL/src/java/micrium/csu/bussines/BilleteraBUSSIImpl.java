package micrium.csu.bussines;

import micrium.csu.model.Billetera;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.BilleteraDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class BilleteraBUSSIImpl implements BilleteraBUSSI, Serializable {

    @Inject
    private BilleteraDAO dao;
    private static final Logger log = Logger.getLogger(BilleteraBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Billetera con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(Billetera dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setBilleteraId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public Billetera obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Billetera> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Billetera dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Billetera con Id:" + dato.getBilleteraId(), e);
        }
        return sw;
    }

    @Override
    public String validate(Billetera dato, String idStr) {

        if (dato.getNombreComverse().isEmpty()) {
            return ParameterMessage.Billetera_ERROR_nombreComverse;
        }
        if (dato.getNombreComercial().isEmpty()) {
            return ParameterMessage.Billetera_ERROR_nombreComercial;
        }
        if (dato.getPrefijoUnidad().isEmpty()) {
            return ParameterMessage.Billetera_ERROR_prefijoUnidad;
        }
        if (dato.getOperador().isEmpty()) {
            return ParameterMessage.Billetera_ERROR_operador;
        }
        if (dato.getValor() > 0) {
            return ParameterMessage.Billetera_ERROR_valor;
        }
        if (dato.getCantidadDecimales() > 0) {
            return ParameterMessage.Billetera_ERROR_cantidadDecimales;
        }
        if (dato.getMontoMinimo() > 0) {
            return ParameterMessage.Billetera_ERROR_montoMinimo;
        }
        if (dato.getMontoMaximo() > 0) {
            return ParameterMessage.Billetera_ERROR_montoMaximo;
        }


        Billetera datoAux = dao.obtenerName(dato.getNombreComverse());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getBilleteraId()) {
                if (dato.getNombreComverse().equals(datoAux.getNombreComverse())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }

    @Override
    public List<Billetera> obtenerListaByIdConfig(int idConfig) {
        return dao.obtenerListaByIdConfig(idConfig);

    }
    //public List<Billetera> obtenerListaByIdUnitType(int idUnitType);

    @Override
    public List<Billetera> obtenerListaByIdUnitType(int idUnitType) {
        return dao.obtenerListaByIdUnitType(idUnitType);

    }

    @Override
    public List<Billetera> obtenerListaByIdCompBilletera(int idCompoBilletera) {
        return dao.obtenerListaByIdCompoBilletera(idCompoBilletera);

    }
    
    @Override
    public List<Billetera> obtenerListaByIdUnitTypeSinAlco(int idUnitType) {
        return dao.obtenerListaByIdUnitTypeSinAlco(idUnitType);
    }

    @Override
    public List<Billetera> obtenerListaByIdCabecera(int idCabecera) {
       return dao.obtenerListaByIdCabecera(idCabecera);
    }

    @Override
    public List<Billetera> obtenerListaByIdMenu(int idMenu) {
        return dao.obtenerListaByIdMenu(idMenu);
    }

    @Override
    public List<Billetera> obtenerListaByIdConfigMenu(int idUnitType,int idConfig) {
        return dao.obtenerListaByIdConfigMenu(idUnitType,idConfig);
    }

    @Override
    public List<Billetera> obtenerListaByIdConfigCabecera(int idUnitType, int idConfig) {
         return dao.obtenerListaByIdConfigCabecera(idUnitType, idConfig);
    }
}
