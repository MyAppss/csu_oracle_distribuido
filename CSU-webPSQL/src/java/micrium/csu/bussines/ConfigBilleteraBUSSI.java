package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.ConfigBilletera;
/**
 *
 * @author marciano
 */
public interface ConfigBilleteraBUSSI {
    
    public ConfigBilletera obtenerId(int id);
//    public boolean  existe(String name);
    public List<ConfigBilletera> obtenerLista();
    public boolean update(ConfigBilletera dato);
    public boolean insert(ConfigBilletera dato);
    public boolean delete(int id);
    public String validate(ConfigBilletera dato,String idStr);
    public boolean existe(ConfigBilletera dato);
    public boolean updateOffAllByIdConfig(int configId);

//bind(ConfigBilleteraBUSSI.class).to(CosBilleteraBUSSIImpl.class);	
}
