package micrium.csu.bussines;

import micrium.csu.model.RolPaginaIni;
import com.google.inject.Inject;
import java.io.Serializable;
import micrium.csu.dao.RolPaginaIniDAO;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class RolPaginaIniBUSSIImpl implements RolPaginaIniBUSSI, Serializable {

    @Inject
    private RolPaginaIniDAO dao;

    private static final Logger log = Logger.getLogger(RolPaginaIniBUSSIImpl.class);

//    @Override
//    public boolean delete(int id) {
//        boolean sw=false;
//        try {
//            dao.delete(id);
//            sw=true;
//        } catch (Exception e) {
//            log.error("[delete] Error al eliminar RolPaginaIni con Id:"+id+" "
//                    ,e);             
//        }
//       return sw; 
//    }
//
//    @Override
//    public boolean insert(RolPaginaIni dato) {
//         boolean sw=false;
//        try {
//            int id=dao.obtenerIdSecuencia();
//            if(id>0){
//                dato.setRolPaginaIniId(id);
//                dao.insert(dato);
//                sw=true;
//             }else
//               log.info("[insert] El valor secuencial es -1");              
//        } catch (Exception e) {
//            log.error("[insert]Error al insertar",e);             
//        }
//         return sw; 
//    }
//
    @Override
    public RolPaginaIni obtenerId(int id) {
        return dao.obtenerId(id);
    }
//
//    @Override
//    public List<RolPaginaIni> obtenerLista() {
//        return dao.obtenerLista();
//    }

    @Override
    public boolean update(RolPaginaIni dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar RolPaginaIni con Id:" + dato.getRolId(),
                     e);
        }
        return sw;
    }
//
//    @Override
//    public String validate(RolPaginaIni dato, String idStr) {
//       
//        if(dato.getPaginaInicio().isEmpty())
//            return ParameterMessage.RolPaginaIni_ERROR_paginaInicio;
//
//
//        RolPaginaIni  datoAux=dao.obtenerName(dato.getNombre());
//        if(datoAux==null)
//                return "";
//
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getRolPaginaIniId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
//        return "este Nombre ya existe";
//    }
   
}
