/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.DetalleMenuDAO;
import micrium.csu.model.DetalleMenu;

import org.apache.log4j.Logger;

/**
 *
 * @author Vehimar
 */
public class DetalleMenuBUSIImp implements DetalleMenuBUSSI, Serializable {

    @Inject
    private DetalleMenuDAO dao;
    private static final Logger log = Logger.getLogger(DetalleMenuBUSIImp.class);

    @Override
    public List<DetalleMenu> obtenerLista() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(DetalleMenu dato) {
       boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar DetalleCabecera con Id:" + dato + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(DetalleMenu dato) {
        boolean sw = false;
        int id = dao.obtenerIdSecuencia();
        try {
            if (id > 0) {
                dato.setMenuDetalleId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar DetalleCabecera con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(DetalleMenu dato, String idStr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DetalleMenu> obtenerIdCabecera(int id) {
        return dao.obtenerIdCabecera(id);
    }

}
