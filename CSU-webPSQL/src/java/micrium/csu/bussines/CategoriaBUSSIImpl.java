/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.CategoriaDAO;
import micrium.csu.model.Categoria;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author Leandro
 */
public class CategoriaBUSSIImpl implements CategoriaBUSSI, Serializable {

    @Inject
    private CategoriaDAO dao;

    private static final Logger log = Logger.getLogger(CategoriaBUSSIImpl.class);

    @Override
    public Categoria obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Categoria> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Categoria dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Categoria con Id:" + dato.getCategoriaId(), e);
        }
        return sw;
    }

    @Override
    public boolean insert(Categoria dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setCategoriaId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Categoria con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public String validate(Categoria dato, String idStr) {
        if (dato.getNombre().isEmpty()) {
            return ParameterMessage.Categoria_ERROR_nombre;
        }

        Categoria datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getCategoriaId()) {
                if (dato.getNombre().equals(datoAux.getNombre())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }


}
