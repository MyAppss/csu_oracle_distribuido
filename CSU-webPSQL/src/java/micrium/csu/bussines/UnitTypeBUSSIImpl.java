package micrium.csu.bussines;

import micrium.csu.model.UnitType;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.UnitTypeDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;
/**
 *
 * @author marciano
 */
public class UnitTypeBUSSIImpl implements UnitTypeBUSSI, Serializable {
    @Inject
    private UnitTypeDAO dao;
	
    private static final Logger log = Logger.getLogger(UnitTypeBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw=false;
        try {
            dao.delete(id);
            sw=true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar UnitType con Id:"+id+" "
                    ,e);             
        }
       return sw; 
    }

    @Override
    public boolean insert(UnitType dato) {
         boolean sw=false;
        try {
            int id=dao.obtenerIdSecuencia();
            if(id>0){
                dato.setUnitTypeId(id);
                dao.insert(dato);
                sw=true;
             }else
               log.info("[insert] El valor secuencial es -1");              
        } catch (Exception e) {
            log.error("[insert]Error al insertar",e);             
        }
         return sw; 
    }

    @Override
    public UnitType obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<UnitType> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(UnitType dato) {
         boolean sw=false;
        try {
            dao.update(dato);
            sw=true;
        } catch (Exception e) {
            log.error("[update]Error al modificar UnitType con Id:"+dato.getUnitTypeId()
                    ,e);             
        }
         return sw; 
    }

    @Override
    public String validate(UnitType dato, String idStr) {
        
        if(dato.getNombre().isEmpty())
            return ParameterMessage.UnitType_ERROR_nombre;
        if(dato.getDescripcion().isEmpty())
            return ParameterMessage.UnitType_ERROR_descripcion;


        UnitType  datoAux=dao.obtenerName(dato.getNombre());
        if(datoAux==null)
                return "";

        if(idStr!=null && !idStr.isEmpty()){
                int id=Integer.parseInt(idStr);
                if(id==datoAux.getUnitTypeId())
                   if(dato.getNombre().equals(datoAux.getNombre()))
                          return "";
        }
        return "este Nombre ya existe";
    }

/* parametros
PPPPMMMMTTTT
*/

/* properties
PPPPRRRRSSSS
*/
  
}
