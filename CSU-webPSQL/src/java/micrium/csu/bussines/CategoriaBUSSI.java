/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.Categoria;

/**
 *
 * @author Leandro
 */
public interface CategoriaBUSSI {
    public Categoria obtenerId(int id);
    public List<Categoria> obtenerLista();
    public boolean update(Categoria dato);
    public boolean insert(Categoria dato);
    public boolean delete(int id);
    public String validate(Categoria dato,String idStr);
   
    //bind(CategoriaBUSSI.class).to(CategoriaBUSSI.class);
}
