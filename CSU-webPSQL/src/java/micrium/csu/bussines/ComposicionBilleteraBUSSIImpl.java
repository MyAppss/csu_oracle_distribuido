package micrium.csu.bussines;

import micrium.csu.model.ComposicionBilletera;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.ComposicionBilleteraDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;
/**
 *
 * @author marciano
 */
public class ComposicionBilleteraBUSSIImpl implements ComposicionBilleteraBUSSI, Serializable {
    @Inject
    private ComposicionBilleteraDAO dao;
	
    private static final Logger log = Logger.getLogger(ComposicionBilleteraBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw=false;
        try {
            dao.delete(id);
            sw=true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar ComposicionBilletera con Id:"+id+" "
                    ,e);             
        }
       return sw; 
    }

    @Override
    public boolean insert(ComposicionBilletera dato) {
         boolean sw=false;
        try {
            int id=dao.obtenerIdSecuencia();
            if(id>0){
                dato.setComposicionBilleteraId(id);
                dao.insert(dato);
                sw=true;
             }else
               log.info("[insert] El valor secuencial es -1");              
        } catch (Exception e) {
            log.error("[insert]Error al insertar",e);             
        }
         return sw; 
    }

    @Override
    public ComposicionBilletera obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<ComposicionBilletera> obtenerLista() {
        return dao.obtenerLista();
    }
    @Override
    public List<ComposicionBilletera> obtenerListaByIdConfig(int idConfig) {
        return dao.obtenerListaByIdConfig(idConfig);
                
    }
    @Override
    public boolean update(ComposicionBilletera dato) {
         boolean sw=false;
        try {
            dao.update(dato);
            sw=true;
        } catch (Exception e) {
            log.error("[update]Error al modificar ComposicionBilletera con Id:"+dato.getComposicionBilleteraId()
                    ,e);             
        }
         return sw; 
    }
    

    @Override
    public String validate(ComposicionBilletera dato, String idStr) {
       
        if(dato.getNombre().isEmpty())
            return ParameterMessage.ComposicionBilletera_ERROR_nombre;
        if(dato.getNombreComercial().isEmpty())
            return ParameterMessage.ComposicionBilletera_ERROR_nombreComercial;
        if(dato.getPrefijoUnidad().isEmpty())
            return ParameterMessage.ComposicionBilletera_ERROR_prefijoUnidad;
        if(dato.getOperador().isEmpty())
            return ParameterMessage.ComposicionBilletera_ERROR_operador;
        if(dato.getValor() > 0)
            return ParameterMessage.ComposicionBilletera_ERROR_valor;
        if(dato.getCantidadDecimales() > 0)
            return ParameterMessage.ComposicionBilletera_ERROR_cantidadDecimales;
      


        ComposicionBilletera  datoAux=dao.obtenerName(dato.getNombre());
        if(datoAux==null)
                return "";

        if(idStr!=null && !idStr.isEmpty()){
                int id=Integer.parseInt(idStr);
                if(id==datoAux.getComposicionBilleteraId())
                   if(dato.getNombre().equals(datoAux.getNombre()))
                          return "";
        }
        return "este Nombre ya existe";
    }
    
    @Override
     public boolean existe(String nombre){
        try {
            ComposicionBilletera co= dao.obtenerName(nombre);    
            if(co!=null){
                return true;
            }else
                return false;
        } catch (Exception e) {
            log.error("[existe]error:"+e.getMessage());
        }
        
        return false; 
     }

/* parametros
PPPPMMMMTTTT
*/

/* properties
PPPPRRRRSSSS
*/
  
}
