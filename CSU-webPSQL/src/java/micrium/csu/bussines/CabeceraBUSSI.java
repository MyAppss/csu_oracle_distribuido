/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.Cabecera;

/**
 *
 * @author Vehimar
 */
public interface CabeceraBUSSI {
    public Cabecera obtenerId(int id);
    public List<Cabecera> obtenerLista();
    public boolean update(Cabecera dato);
    public boolean updatePos(Cabecera dato);
    public Integer insert(Cabecera dato);
    public boolean delete(int id);
    public String validate(Cabecera dato,String idStr);
    public List<Cabecera> obtenerByIdConfig(int idConf);
       
}
