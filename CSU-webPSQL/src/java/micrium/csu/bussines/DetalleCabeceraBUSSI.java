/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.DetalleCabecera;

/**
 *
 * @author Vehimar
 */
public interface DetalleCabeceraBUSSI {
    public List<DetalleCabecera> obtenerIdCabecera(int id);
    public List<DetalleCabecera> obtenerLista();
    public boolean update(DetalleCabecera dato);
    public boolean insert(DetalleCabecera dato);
    public boolean delete(int id);
    public String validate(DetalleCabecera dato,String idStr);
    
    
}
