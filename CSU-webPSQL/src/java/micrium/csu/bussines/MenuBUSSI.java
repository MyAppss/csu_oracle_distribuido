/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.Menu;

/**
 *
 * @author Leandro
 */
public interface MenuBUSSI {
    public Menu obtenerId(int id);
    public List<Menu> obtenerLista();
    public boolean update(Menu dato);
    public boolean updatePos(Menu dato);
    public Integer insert(Menu dato);
    public boolean delete(int id);
    public String validate(Menu dato,String idStr);
    public List<Menu> obtenerListaByConfigId(int configId);
    //bind(CategoriaBUSSI.class).to(CategoriaBUSSI.class);
}
