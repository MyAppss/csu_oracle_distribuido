
package micrium.csu.bussines;

import java.util.List;
import micrium.csu.model.Nodo;

/**
 *
 * @author User
 */
public interface NodoBUSSI {

    public Nodo obtenerId(int id);
    public List<Nodo> obtenerLista();
    public boolean update(Nodo dato);
    public boolean insert(Nodo dato);
    public boolean delete(int id);
    public String validate(Nodo dato, String idStr, boolean edit);
}
