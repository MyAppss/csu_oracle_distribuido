package micrium.csu.bussines;

import micrium.csu.model.Occ;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.OccDAO;
import micrium.csu.view.OccView;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class OccBUSSIImpl implements OccBUSSI, Serializable {

    @Inject
    private OccDAO dao;
    private static final Logger log = Logger.getLogger(OccBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Occ con Id:" + id + " ", e);
        }
        return sw;
    }

    @Override
    public boolean insert(Occ dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setOccId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public Occ obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Occ> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Occ dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Occ con Id:" + dato.getOccId(), e);
        }
        return sw;
    }

    @Override
    public String validate(Occ dato, String idStr) {



//        Occ  datoAux=dao.obtenerName(dato.getNombre());
//        
//        if(datoAux==null)
//                return "";
//
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getOccId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
        return "este Nombre ya existe";
    }

    /* parametros
     PPPPMMMMTTTT
     */

    /* properties
     PPPPRRRRSSSS
     */
    @Override
    public List<Occ> obtenerListConfigId(int configId) {
        return dao.obtenerListConfigId(configId);
    }

    @Override
    public List<Occ> obtenerLista(OccView occView) {
        return dao.obtenerLista(occView);
    }

    @Override
    public Occ obtenerOcc(OccView occView) {
        return dao.obtenerOcc(occView);
    }

    @Override
    public List<OccView> obtenerListaView(int configId) {
        return dao.obtenerListaView(configId);
    }

    @Override
    public boolean deleteConfig(int configId) {
        boolean sw = false;
        try {
            dao.deleteConfig(configId);
            sw = true;
        } catch (Exception e) {
            log.error("[deleteConfig] Error al eliminar Occ con configId:" + configId + " ", e);
        }
        return sw;
    }
}
