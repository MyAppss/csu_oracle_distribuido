package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.Occ;
import micrium.csu.view.OccView;
/**
 *
 * @author marciano
 */
public interface OccBUSSI {
    
    public Occ obtenerId(int id);
    public List<Occ> obtenerLista();
    public List<Occ> obtenerListConfigId(int configId);
    public List<Occ> obtenerLista(OccView occView);
    public List<OccView> obtenerListaView(int configId);
    public Occ obtenerOcc(OccView occView);
    public boolean update(Occ dato);
    public boolean insert(Occ dato);
    public boolean delete(int id);
    public boolean deleteConfig(int configId);
    public String validate(Occ dato,String idStr);

//bind(OccBUSSI.class).to(OccBUSSIImpl.class);	
}
