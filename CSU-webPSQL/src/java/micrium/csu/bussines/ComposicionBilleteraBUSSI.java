package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.ComposicionBilletera;
/**
 *
 * @author marciano
 */
public interface ComposicionBilleteraBUSSI {
    
    public ComposicionBilletera obtenerId(int id);
    public List<ComposicionBilletera> obtenerLista();
    public boolean update(ComposicionBilletera dato);
    public boolean insert(ComposicionBilletera dato);
    public boolean delete(int id);
    public String validate(ComposicionBilletera dato,String idStr);
    public List<ComposicionBilletera> obtenerListaByIdConfig(int idConfig);
    public boolean existe(String nombre);

//bind(ComposicionBilleteraBUSSI.class).to(ComposicionBilleteraBUSSIImpl.class);	
}
