package micrium.csu.bussines;

import  java.util.List;
import  micrium.csu.model.UnitType;
/**
 *
 * @author marciano
 */
public interface UnitTypeBUSSI {
    
    public UnitType obtenerId(int id);
    public List<UnitType> obtenerLista();
    public boolean update(UnitType dato);
    public boolean insert(UnitType dato);
    public boolean delete(int id);
    public String validate(UnitType dato,String idStr);

//bind(UnitTypeBUSSI.class).to(UnitTypeBUSSIImpl.class);	
}
