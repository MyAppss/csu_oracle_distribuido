package micrium.csu.bussines;

import micrium.csu.model.Config;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.ConfigDAO;
import micrium.csu.util.ParameterMessage;
import org.apache.log4j.Logger;
/**
 *
 * @author marciano
 */
public class ConfigBUSSIImpl implements ConfigBUSSI, Serializable {
    @Inject
    private ConfigDAO dao;
	
    private static final Logger log = Logger.getLogger(ConfigBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw=false;
        try {
            dao.delete(id);
            sw=true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Config con Id:"+id+" "
                    ,e);             
        }
       return sw; 
    }

    @Override
    public boolean insert(Config dato) {
         boolean sw=false;
        try {
            int id=dao.obtenerIdSecuencia();
            if(id>0){
                dato.setConfigId(id);
                dao.insert(dato);
                sw=true;
             }else
               log.info("[insert] El valor secuencial es -1");              
        } catch (Exception e) {
            log.error("[insert]Error al insertar",e);             
        }
         return sw; 
    }

    @Override
    public Config obtenerId(int id) {
        return dao.obtenerId(id);
    }
    //public Config obtenerByName(String name);
    @Override
    public Config obtenerByName(String name){
        return dao.obtenerName(name);
    }
    
    @Override
    public List<Config> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Config dato) {
         boolean sw=false;
        try {
            dao.update(dato);
            sw=true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Config con Id:"+dato.getConfigId()
                    ,e);             
        }
         return sw; 
    }

    @Override
    public String validate(Config dato, String idStr) {
       
        if(dato.getNombre().isEmpty())
            return ParameterMessage.Config_ERROR_nombre;
        if(dato.getSaludoInicial().isEmpty())
            return ParameterMessage.Config_ERROR_saludoInicial;
        if(dato.getDescripcion().isEmpty())
            return ParameterMessage.Config_ERROR_descripcion;


        Config  datoAux=dao.obtenerName(dato.getNombre());
        if(datoAux==null)
                return "";

        if(idStr!=null && !idStr.isEmpty()){
                int id=Integer.parseInt(idStr);
                if(id==datoAux.getConfigId())
                   if(dato.getNombre().equals(datoAux.getNombre()))
                          return "";
        }
        return "este Nombre ya existe";
    }

/* parametros
PPPPMMMMTTTT
*/

/* properties
PPPPRRRRSSSS
*/
  
}
