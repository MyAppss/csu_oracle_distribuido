package micrium.csu.bussines;

import micrium.csu.model.Corto;
import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import micrium.csu.dao.CortoDAO;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
public class CortoBUSSIImpl implements CortoBUSSI, Serializable {

    @Inject
    private CortoDAO dao;

    private static final Logger log = Logger.getLogger(CortoBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw = false;
        try {
            dao.delete(id);
            sw = true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar Corto con Id:" + id + " ",
                     e);
        }
        return sw;
    }

    @Override
    public boolean insert(Corto dato) {
        boolean sw = false;
        try {
            int id = dao.obtenerIdSecuencia();
            if (id > 0) {
                dato.setCortoId(id);
                dao.insert(dato);
                sw = true;
            } else {
                log.info("[insert] El valor secuencial es -1");
            }
        } catch (Exception e) {
            log.error("[insert]Error al insertar", e);
        }
        return sw;
    }

    @Override
    public Corto obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<Corto> obtenerLista() {
        return dao.obtenerLista();
    }

    @Override
    public boolean update(Corto dato) {
        boolean sw = false;
        try {
            dao.update(dato);
            sw = true;
        } catch (Exception e) {
            log.error("[update]Error al modificar Corto con Id:" + dato.getCortoId(),
                     e);
        }
        return sw;
    }

    @Override
    public String validate(Corto dato, String idStr) {

        if (dato.getNombre().isEmpty()) {
            return ParameterMessage.Corto_ERROR_nombre;
        }
        if (!esValidoAtributoVsExpresion(dato.getNombre(), Parameters.expresionRegularCorto)) {
            return ParameterMessage.Corto_ERROR_nombre_no_valido;
        }
        if (dato.getDescripcion().isEmpty()) {
            return ParameterMessage.Corto_ERROR_descripcion;
        }
        if (!esValidoAtributoVsExpresion(dato.getDescripcion(), Parameters.expresionRegularTexto)) {
            return ParameterMessage.Corto_ERROR_descripcion_no_valido;
        }
        
        Corto datoAux = dao.obtenerName(dato.getNombre());
        if (datoAux == null) {
            return "";
        }

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == datoAux.getCortoId()) {
                if (dato.getNombre().equals(datoAux.getNombre())) {
                    return "";
                }
            }
        }
        return "este Nombre ya existe";
    }

    private boolean esValidoAtributoVsExpresion(String corto, String expresionRegular) {
        Pattern pattern = Pattern.compile(expresionRegular);
        Matcher matcher = pattern.matcher(corto);
        if (matcher.find()) {
            return true;
        } 
        return false;
    }

}
