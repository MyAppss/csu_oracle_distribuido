package micrium.csu.bussines;


import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import micrium.csu.dao.GrupoBilleteraDAO;
import micrium.csu.model.GrupoBilletera;
import org.apache.log4j.Logger;
/**
 *
 * @author marciano
 */
public class GrupoBilleteraBUSSIImpl implements GrupoBilleteraBUSSI, Serializable {
    @Inject
    private GrupoBilleteraDAO dao;
	
    private static final Logger log = Logger.getLogger(GrupoBilleteraBUSSIImpl.class);

    @Override
    public boolean delete(int id) {
        boolean sw=false;
        try {
            dao.delete(id);
            sw=true;
        } catch (Exception e) {
            log.error("[delete] Error al eliminar GrupoBilletera con Id:"+id+" "
                    +e);             
        }
       return sw; 
    }

    @Override
    public boolean insert(GrupoBilletera dato) {
         boolean sw=false;
        try {
//                dato.setGrupoBilleteraId(id);
            dao.insert(dato);
            sw = true;
                        
        } catch (Exception e) {
            log.error("[insert]Error al insertar"+e);             
        }
         return sw; 
    }

    @Override
    public GrupoBilletera obtenerId(int id) {
        return dao.obtenerId(id);
    }

    @Override
    public List<GrupoBilletera> obtenerLista() {
        return dao.obtenerLista();
    }
     @Override
    public List<GrupoBilletera> obtenerListaByIdBilletera(int IdBilletera) {
        return dao.obtenerListaByIdBilletera(IdBilletera);
    }

    @Override
    public boolean update(GrupoBilletera dato) {
         boolean sw=false;
        try {
            dao.update(dato);
            sw=true;
        } catch (Exception e) {
            log.error("[update]Error al modificar GrupoBilletera con Id:"//+dato.getGrupoBilleteraId()
                    +e);             
        }
         return sw; 
    }

    @Override
    public String validate(GrupoBilletera dato, String idStr) {
       
//        if(dato.getCanalId() > 0)
//            return ParameterMessage.GrupoBilletera_ERROR_canalId;
//        if(dato.getDescripcion().isEmpty())
//            return ParameterMessage.GrupoBilletera_ERROR_descripcion;
//
//
//        GrupoBilletera  datoAux=dao.obtenerName(dato.getNombre());
//        if(datoAux==null)
//                return "";
//
//        if(idStr!=null && !idStr.isEmpty()){
//                int id=Integer.parseInt(idStr);
//                if(id==datoAux.getGrupoBilleteraId())
//                   if(dato.getNombre().equals(datoAux.getNombre()))
//                          return "";
//        }
        return "este Nombre ya existe";
    }

/* parametros
    public static String GrupoBilletera_ERROR_canalId= prop.getProperty("GrupoBilletera_ERROR_canalId");
    public static String GrupoBilletera_ERROR_descripcion= prop.getProperty("GrupoBilletera_ERROR_descripcion");
    public static String GrupoBilletera_ERROR_estado= prop.getProperty("GrupoBilletera_ERROR_estado");
    public static String GrupoBilletera_MESSAGE_GUARDAR= prop.getProperty("GrupoBilletera_MESSAGE_GUARDAR");
    public static String GrupoBilletera_MESSAGE_MODIFICAR= prop.getProperty("GrupoBilletera_MESSAGE_MODIFICAR");
    public static String GrupoBilletera_MESSAGE_ELIMINAR= prop.getProperty("GrupoBilletera_MESSAGE_ELIMINAR");
    public static String GrupoBilletera_MESSAGE_ERROR_GUARDAR= prop.getProperty("GrupoBilletera_MESSAGE_ERROR_GUARDAR");
    public static String GrupoBilletera_MESSAGE_ERROR_MODIFICAR= prop.getProperty("GrupoBilletera_MESSAGE_ERROR_MODIFICAR");
    public static String GrupoBilletera_MESSAGE_ERROR_ELIMINAR= prop.getProperty("GrupoBilletera_MESSAGE_ERROR_ELIMINAR");

*/

/* properties
GrupoBilletera_ERROR_canalId = El campo canal_id esta vacio 
GrupoBilletera_ERROR_descripcion = El campo descripcion esta vacio 
GrupoBilletera_ERROR_estado = El campo estado esta vacio 
GrupoBilletera_MESSAGE_GUARDAR = Se GUARDAR con Id: 
GrupoBilletera_MESSAGE_MODIFICAR = Se MODIFICAR con Id: 
GrupoBilletera_MESSAGE_ELIMINAR = Se ELIMINAR con Id: 
GrupoBilletera_MESSAGE_ERROR_GUARDAR = Se ERROR_GUARDAR con Id: 
GrupoBilletera_MESSAGE_ERROR_MODIFICAR = Se ERROR_MODIFICAR con Id: 
GrupoBilletera_MESSAGE_ERROR_ELIMINAR = Se ERROR_ELIMINAR con Id: 

*/
  
}
