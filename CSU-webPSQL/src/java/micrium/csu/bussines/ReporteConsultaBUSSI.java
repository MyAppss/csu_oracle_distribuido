package micrium.csu.bussines;


import java.util.Date;
import  java.util.List;
import  micrium.csu.model.ReporteConsulta;
import micrium.csu.model.ReporteConsultaGroup;
/**
 *
 * @author marciano
 */
public interface ReporteConsultaBUSSI {
    
    public ReporteConsulta obtenerId(int id);
    public List<ReporteConsulta> obtenerLista();
    public List<ReporteConsulta> obtenerListaByIsdn(String isdn);
    public List<ReporteConsulta> obtenerListaByMsisdnDate(String isdn, Date fechaIni,Date fechaFin);
    public boolean update(ReporteConsulta dato);
    public boolean insert(ReporteConsulta dato);
    public boolean delete(int id);
    public String validate(ReporteConsulta dato,String idStr);
    public List<ReporteConsultaGroup> obtenerListaByMsidnGroup(String isdn, Date fechaIni, Date fechaFin);


//bind(ReporteConsultaBUSSI.class).to(ReporteConsultaBUSSIImpl.class);	
}
