package micrium.csu.mybatis;

import com.google.inject.Provider;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 *
 * @author Ivan
 */
public class SqlSessionFactoryProvider implements Provider<SqlSessionFactory> {

    @Override
    public SqlSessionFactory get() {
        Reader reader = null;
        SqlSessionFactory sqlMapper = null;
        try {
            String resource = "micrium/csu/mybatis/Configuration.xml";
            reader = Resources.getResourceAsReader(resource);
            //InputStream inputStream = Resources.getResourceAsStream(resource);
            //sqlMapper = new SqlSessionFactoryBuilder().build(inputStream);
            sqlMapper = new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException ex) {
            System.out.println("error " + ex.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                System.out.println("error " + ex.getMessage());
            }
        }
        return sqlMapper;
    }

}
