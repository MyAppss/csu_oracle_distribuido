package micrium.csu.mybatis;

import micrium.csu.bussines.OccBUSSIImpl;
import micrium.csu.bussines.OccBUSSI;
import micrium.csu.dao.OccDAOImpl;
import micrium.csu.dao.OccDAO;
import micrium.csu.dao.OrigenDAOImpl;
import micrium.csu.dao.OrigenDAO;
import micrium.csu.bussines.OrigenBUSSIImpl;
import micrium.csu.bussines.OrigenBUSSI;
import micrium.csu.bussines.CortoBUSSIImpl;
import micrium.csu.bussines.CortoBUSSI;
import micrium.csu.dao.CortoDAOImpl;
import micrium.csu.dao.CortoDAO;
import micrium.csu.bussines.RolPaginaIniBUSSIImpl;
import micrium.csu.bussines.RolPaginaIniBUSSI;
import micrium.csu.dao.RolPaginaIniDAOImpl;
import micrium.csu.dao.RolPaginaIniDAO;
import micrium.csu.bussines.ReporteConsultaBUSSIImpl;
import micrium.csu.bussines.ReporteConsultaBUSSI;
import micrium.csu.dao.ReporteConsultaDAOImpl;
import micrium.csu.dao.ReporteConsultaDAO;
import micrium.csu.bussines.CosBUSSIImpl;
import micrium.csu.bussines.CosBUSSI;
import micrium.csu.dao.CosDAOImpl;
import micrium.csu.dao.CosDAO;
import micrium.csu.bussines.ConfigBUSSIImpl;
import micrium.csu.bussines.ConfigBUSSI;
import micrium.csu.dao.ConfigDAOImpl;
import micrium.csu.dao.ConfigDAO;
import micrium.csu.bussines.ConfigComposicionBilleteraBUSSIImpl;
import micrium.csu.bussines.ConfigComposicionBilleteraBUSSI;
import micrium.csu.dao.ConfigComposicionBilleteraDAOImpl;
import micrium.csu.dao.ConfigComposicionBilleteraDAO;
import micrium.csu.bussines.ParametroBUSSIImpl;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.dao.ParametroDAOImpl;
import micrium.csu.dao.ParametroDAO;

import micrium.csu.bussines.ComposicionBilleteraBUSSIImpl;
import micrium.csu.bussines.ComposicionBilleteraBUSSI;
import micrium.csu.dao.ComposicionBilleteraDAOImpl;
import micrium.csu.dao.ComposicionBilleteraDAO;

import com.google.inject.AbstractModule;
import micrium.configurable.bl.*;
import micrium.configurable.dao.*;
import micrium.csu.bussines.*;
import micrium.csu.dao.*;

import org.apache.ibatis.session.SqlSessionFactory;

/**
 *
 * @author Ivan
 */
public class GuiceMainModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(SqlSessionFactory.class).toProvider(SqlSessionFactoryProvider.class);

        bind(OccDAO.class).to(OccDAOImpl.class);
        bind(OccBUSSI.class).to(OccBUSSIImpl.class);

        bind(OrigenDAO.class).to(OrigenDAOImpl.class);
        bind(OrigenBUSSI.class).to(OrigenBUSSIImpl.class);

        bind(CortoBUSSI.class).to(CortoBUSSIImpl.class);
        bind(CortoDAO.class).to(CortoDAOImpl.class);

        bind(RolPaginaIniBUSSI.class).to(RolPaginaIniBUSSIImpl.class);
        bind(RolPaginaIniDAO.class).to(RolPaginaIniDAOImpl.class);

        bind(ReporteConsultaBUSSI.class).to(ReporteConsultaBUSSIImpl.class);
        bind(ReporteConsultaDAO.class).to(ReporteConsultaDAOImpl.class);

        bind(CosBUSSI.class).to(CosBUSSIImpl.class);
        bind(CosDAO.class).to(CosDAOImpl.class);

        bind(AcumuladorBUSSI.class).to(AcumuladorBUSSIImpl.class);
        bind(AcumuladorDAO.class).to(AcumuladorDAOImpl.class);

        bind(ConfigBUSSI.class).to(ConfigBUSSIImpl.class);
        bind(ConfigDAO.class).to(ConfigDAOImpl.class);

        bind(ConfigComposicionBilleteraBUSSI.class).to(ConfigComposicionBilleteraBUSSIImpl.class);
        bind(ConfigComposicionBilleteraDAO.class).to(ConfigComposicionBilleteraDAOImpl.class);
        bind(ParametroBUSSI.class).to(ParametroBUSSIImpl.class);
        bind(ParametroDAO.class).to(ParametroDAOImpl.class);

        bind(ComposicionBilleteraBUSSI.class).to(ComposicionBilleteraBUSSIImpl.class);
        bind(ComposicionBilleteraDAO.class).to(ComposicionBilleteraDAOImpl.class);

        bind(BilleteraBUSSI.class).to(BilleteraBUSSIImpl.class);
        bind(BilleteraDAO.class).to(BilleteraDAOImpl.class);

        bind(UnitTypeBUSSI.class).to(UnitTypeBUSSIImpl.class);
        bind(UnitTypeDAO.class).to(UnitTypeDAOImpl.class);

        // bind(CategoriaBUSSI.class).to(CategoriaBUSSIImpl.class);
        // bind(CategoriaDAO.class).to(CategoriaDAOImpl.class);
        bind(MenuBUSSI.class).to(MenuBUSSIImpl.class);
        bind(MenuDAO.class).to(MenuDAOImpl.class);

        bind(GrupoBilleteraBUSSI.class).to(GrupoBilleteraBUSSIImpl.class);
        bind(GrupoBilleteraDAO.class).to(GrupoBilleteraDAOImpl.class);

        bind(ConfigBilleteraBUSSI.class).to(ConfigBilleteraBUSSIImpl.class);
        bind(ConfigBilleteraDAO.class).to(ConfigBilleteraDAOImpl.class);

        bind(CabeceraBUSSI.class).to(CabeceraBUSSIImpl.class);
        bind(CabeceraDAO.class).to(CabeceraDAOImp.class);

        bind(DetalleCabeceraBUSSI.class).to(DetalleCabeceraBUSIImp.class);
        bind(DetalleCabeceraDAO.class).to(DetalleCabeceraDAOImp.class);

        bind(DetalleMenuBUSSI.class).to(DetalleMenuBUSIImp.class);
        bind(DetalleMenuDAO.class).to(DetalleMenuDAOImp.class);

//=========================================================================
        bind(SorterBL.class).to(SorterBLImpl.class);
        bind(SorterDAO.class).to(SorterDAOImpl.class);

        bind(CampaignBL.class).to(CampaignBLImpl.class);
        bind(CampaignDAO.class).to(CampaignDAOImpl.class);

        bind(SorterValueBL.class).to(SorterValueBLImpl.class);
        bind(SorterValueDAO.class).to(SorterValueDAOImpl.class);

        bind(MessageBL.class).to(MessageBLImpl.class);
        bind(MessageDAO.class).to(MessageDAOImpl.class);

        bind(CampaignWalletBL.class).to(CampaignWalletBLImpl.class);
        bind(CampaignWalletDAO.class).to(CampaignWalletDAOImpl.class);

        bind(CampaignMessageBL.class).to(CampaignMessageBLImpl.class);
        bind(CampaignMessageDAO.class).to(CampaignMessageDAOImpl.class);

        bind(CampaignSorterBL.class).to(CampaignSorterBLImpl.class);
        bind(CampaignSorterDAO.class).to(CampaignSorterDAOImpl.class);

        bind(ConfigurationBL.class).to(ConfigurationBLImpl.class);
        bind(ConfigurationDAO.class).to(ConfigurationDAOImpl.class);

        bind(DefaulMessageBL.class).to(DefaulMessageBLImpl.class);
        bind(DefaulMessageDAO.class).to(DefaulMessageDAOImpl.class);

        bind(ConfigAcumuladoBUSSI.class).to(ConfigAcumuladoBUSSIImpl.class);
        bind(ConfigAcumuladoDAO.class).to(ConfigAcumuladoDAOImp.class);

        bind(MapeoOfertasBUSSI.class).to(MapeoOfertasBUSSIImpl.class);
        bind(MapeoOfertasDAO.class).to(MapeoOfertasDAOImpl.class);
        
         bind(NodoBUSSI.class).to(NodoBUSSIImpl.class);
        bind(NodoDAO.class).to(NodoDAOImpl.class);

    }
}
