/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.BilleteraBUSSI;
import micrium.csu.bussines.ComposicionBilleteraBUSSI;
import micrium.csu.bussines.ConfigBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Billetera;
import micrium.csu.model.ComposicionBilletera;
import micrium.csu.model.Config;
import micrium.csu.model.Parametro;
import micrium.csu.model.WalletUnidadNav;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.RecargarServicios;
import micrium.csu.util.ReplaceStrCar_Esp;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author jose_luis
 */
@ManagedBean
@ViewScoped
public class OptionsDefault implements Serializable {

    private static final long serialVersionUID = 1L;
//
//    private String canalId;
//    private Boolean edit;

    private List<Parametro> listParametros;
//    private List<SelectItem> selectItemsConfig;
    //private String selectedItemConfig;
    //------------------------------------------
    private String default_saludo_inicial;
    private String default_mostrar_vigencia;
    private String default_mostrar_saldo_expirado;
    private String default_respuesta_falla_conex_comverse;
    private String default_respuesta_fuera_servicio;
    private String default_respuesta_usuario_no_comverse;
    private String default_texto_saldo_min;
    private String default_texto_saldo_max;
    private String default_texto_saldo_min_sw_valor;
    private String vigencia_saldo_formato_date;
    private String vigencia_saldo_texto;
    private String sistema_consulta_saldo_sw_enable;
    private String textEstadoSistema;
    private String default_wallet_cantidad_decimal;
    private String default_wallet_texto_separador;
    private String vigencia_saldo_vigencia_ilimitada;
    private String default_wallet_sw_mostrar_vigencia;
    private String vigencia_saldo_txt_vigencia_ilimitada;
    ////////////////////////////////////////////////////
    private String seg_vigencia_formato_fecha;
    private String seg_vigencia_formato_hora;
    private String seg_vigencia_text_saldo;
    private String seg_vigencia_dias_exp;

    private String valor_conversion_unidad_navegacion;
    private String valor_cambio_unidad_navegacion;
    private String valor_unidades_navegacion;
    private String valor_billeteras_simple_unidad_navegacion;
    // private String valor_billeteras_reserva;
    private String valor_billeteras_agrupadas_unidad_navegacion;
    private String default_mostar_minutos;

    private List<WalletUnidadNav> wallestsSimple;
    private List<WalletUnidadNav> wallestsComp;
    private List<WalletUnidadNav> wallestsReserva;

    private List<Billetera> billeterasSimples;
    private List<SelectItem> selectItemsBilleraSimple;
    private List<SelectItem> selectItemsBilleraReserva;
    private static final BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
    private String selectedItemBilleteraSimple;
    private String selectedItemBilleteraReserva;

    private List<ComposicionBilletera> billeterasComp;
    private List<SelectItem> selectItemsBilleraComp;
    private static ComposicionBilleteraBUSSI COMPOSITION_WALLET_BL = GuiceInjectorSingleton.getInstance(ComposicionBilleteraBUSSI.class);
    private String selectedItemBilleteraComp;
    //////////////////////////////////////////////////////

    private static final Logger log = Logger.getLogger(OptionsDefault.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private String userId;

    private String default_cantidad_cabeceras;

//    private static BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
    /////////////////////////////////////////////////////
    private Integer cantidadCabecera;

    public OptionsDefault() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        userId = ControlerBitacora.getNombreUser();
    }

    @PostConstruct
    public void init() {
        try {
            //canalId = "2";//canal sms
            ParametroBUSSI parBuss = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            listParametros = parBuss.obtenerLista();
            //cargarItemsBilleteras();
            cargarParametros();
            updateTextState();
            this.billeterasSimples = walletBL.obtenerLista();
            this.billeterasComp = COMPOSITION_WALLET_BL.obtenerLista();

            this.cantidadCabecera = 0;

            cargarBilleterasSimples();
            // cargarBilleterasReserva();
            cargarBilleterasComp();

            cargarItemsBilleterasSimples();
            cargarItemsBilleterasReserva();
            cargarItemsBilleterasComp();
        } catch (Exception e) {
            log.error("[init]|user=" + userId + "|Error: " + e.getMessage());
        }
    }

    private void cargarItemsBilleterasSimples() {
        try {
            selectedItemBilleteraSimple = "-1";
            selectItemsBilleraSimple = new LinkedList<>();
            SelectItem s = new SelectItem("-1", "- Seleccionar Billetera -");
            selectItemsBilleraSimple.add(s);

            for (Billetera billetera : this.billeterasSimples) {
                if (!containBilletera(billetera.getNombreComverse(), wallestsSimple)) {
                    SelectItem sel = new SelectItem(billetera.getBilleteraId(), billetera.getNombreComverse());
                    selectItemsBilleraSimple.add(sel);
                }
            }

        } catch (Exception e) {
            log.error("[cargarItemsBilleteras]al intentar cargar Items Billeteras:" + e.getMessage());
        }

    }

    private void cargarItemsBilleterasReserva() {
        try {
            selectedItemBilleteraReserva = "-1";
            selectItemsBilleraReserva = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "- Seleccionar Billetera -");
            selectItemsBilleraReserva.add(s);

            for (Billetera billetera : this.billeterasSimples) {
                if (!containBilletera(billetera.getNombreComverse(), wallestsReserva)) {
                    SelectItem sel = new SelectItem(billetera.getBilleteraId(), billetera.getNombreComverse());
                    selectItemsBilleraReserva.add(sel);
                }
            }

        } catch (Exception e) {
            log.error("[cargarItemsBilleteras]al intentar cargar Items Billeteras:" + e.getMessage());
        }

    }

    private void cargarItemsBilleterasComp() {
        try {
            selectedItemBilleteraComp = "-1";
            selectItemsBilleraComp = new LinkedList<>();
            SelectItem s = new SelectItem("-1", "- Seleccionar Billetera Comp -");
            selectItemsBilleraComp.add(s);

            for (ComposicionBilletera billetera : this.billeterasComp) {
                if (!containBilletera(billetera.getNombre(), wallestsComp)) {
                    SelectItem sel = new SelectItem(billetera.getComposicionBilleteraId(), billetera.getNombre());
                    selectItemsBilleraComp.add(sel);
                }
            }

        } catch (Exception e) {
            log.error("[cargarItemsBilleterasComp]al intentar cargar Items BilleterasComp:" + e.getMessage());
        }

    }

    private boolean containBilletera(String nameBilletera, List<WalletUnidadNav> lista) {
        try {
            if (lista != null) {
                for (WalletUnidadNav billetera : lista) {
                    if (billetera.getNombre().equals(nameBilletera)) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            log.error("[containBilleteras]:" + e.getMessage());
            return false;
        }
    }

    private void cargarBilleterasSimples() {
        if (this.valor_billeteras_simple_unidad_navegacion == null) {
            this.valor_billeteras_simple_unidad_navegacion = "";
        }

        log.info("*** CARGA DE  BILLETERAS SIMPLES...");
        this.wallestsSimple = new ArrayList<>();
        String[] idsWallets = this.valor_billeteras_simple_unidad_navegacion.split(",");
        List<String> simples = (List<String>) Arrays.asList(idsWallets);
        for (Billetera bi : billeterasSimples) {
            if (simples.contains(bi.getBilleteraId() + "")) {
                this.wallestsSimple.add(new WalletUnidadNav(bi.getBilleteraId() + "", bi.getNombreComverse()));
            }
        }
    }

    /* private void cargarBilleterasReserva() {
        this.valor_billeteras_reserva=(this.valor_billeteras_reserva==null)?"":this.valor_billeteras_reserva;
        log.info("*** CARGA DE  BILLETERAS RESERVA...");
        this.wallestsReserva= new ArrayList<>();
        String[] idsWallets = this.valor_billeteras_reserva.split(",");
        List<String> ids = (List<String>) Arrays.asList(idsWallets);
        for (Iterator<Billetera> it = billeterasSimples.iterator(); it.hasNext();) {
            Billetera bi = it.next();
            if (ids.contains(bi.getBilleteraId() + "")) {
                this.wallestsReserva.add(new WalletUnidadNav(bi.getBilleteraId() + "", bi.getNombreComverse()));
            }
        }
    }*/
    private void cargarBilleterasComp() {
        if (this.valor_billeteras_agrupadas_unidad_navegacion == null) {
            this.valor_billeteras_agrupadas_unidad_navegacion = "";
        }

        this.wallestsComp = new ArrayList<>();
        String[] idsWallets = this.valor_billeteras_agrupadas_unidad_navegacion.split(",");
        List<String> simples = (List<String>) Arrays.asList(idsWallets);
        for (Iterator<ComposicionBilletera> it = billeterasComp.iterator(); it.hasNext();) {
            ComposicionBilletera comp = it.next();
            if (simples.contains(comp.getComposicionBilleteraId() + "")) {
                this.wallestsComp.add(new WalletUnidadNav(comp.getComposicionBilleteraId() + "", comp.getNombre()));
            }
        }
    }

    private boolean isConfiguracionValida(int idConfig) {
        try {
            ConfigBUSSI qFlBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
            List<Config> list = qFlBL.obtenerLista();
            for (Config conf : list) {
                if (conf.getConfigId() == idConfig) {
                    return true;
                }
            }

        } catch (Exception e) {
            log.error("[isConfiguracionValida]", e);
        }
        return false;
    }

    public void removedWalletSimple(String idWallet) {
        for (Iterator<WalletUnidadNav> it = wallestsSimple.iterator(); it.hasNext();) {
            WalletUnidadNav wa = it.next();
            if (wa.getId().equals(idWallet)) {
                wallestsSimple.remove(wa);
                break;
            }
        }
        cargarItemsBilleterasSimples();
    }

    public void removedWalletReserva(String idWallet) {
        for (Iterator<WalletUnidadNav> it = wallestsReserva.iterator(); it.hasNext();) {
            WalletUnidadNav wa = it.next();
            if (wa.getId().equals(idWallet)) {
                wallestsReserva.remove(wa);
                break;
            }
        }
        cargarItemsBilleterasSimples();
    }

    public void removedWalletComp(String idWallet) {
        for (Iterator<WalletUnidadNav> it = wallestsComp.iterator(); it.hasNext();) {
            WalletUnidadNav wa = it.next();
            if (wa.getId().equals(idWallet)) {
                wallestsComp.remove(wa);
                break;
            }
        }
        cargarItemsBilleterasComp();
    }

    //////////////////********************** ADD - SIMPLES
    public void addBilleteraSimple() {
        System.out.print(selectedItemBilleteraSimple);
        if (this.selectedItemBilleteraSimple.equals("-1")) {
            return;
        }
        int id = Integer.parseInt(this.selectedItemBilleteraSimple);
        boolean sw = true;
        //for (Iterator<Billetera> it = billeterasSimples.iterator(); it.hasNext();) {
        for (int i = 0; i < billeterasSimples.size() && sw; i++) {
            // Billetera bi = it.next();
            Billetera bi = billeterasSimples.get(i);
            if (id == bi.getBilleteraId()) {
                this.wallestsSimple.add(new WalletUnidadNav(String.valueOf(id), bi.getNombreComverse()));
                removedSelectItem(id, selectItemsBilleraSimple);
                sw = false;
            }
        }
        this.selectedItemBilleteraSimple = "-1";
    }

    public void addBilleteraReserva() {
        System.out.print(selectedItemBilleteraReserva);
        if (this.selectedItemBilleteraReserva.equals("-1")) {
            return;
        }
        int id = Integer.parseInt(this.selectedItemBilleteraReserva);
        boolean sw = true;
        for (int i = 0; i < billeterasSimples.size() && sw; i++) {
            // Billetera bi = it.next();
            Billetera bi = billeterasSimples.get(i);
            if (id == bi.getBilleteraId()) {
                this.wallestsReserva.add(new WalletUnidadNav(String.valueOf(id), bi.getNombreComverse()));
                removedSelectItem(id, selectItemsBilleraReserva);
                sw = false;
            }
        }
        this.selectedItemBilleteraReserva = "-1";
    }

    //////////////////**************** ADD - COMPUETSA
    public void addBilleteraCompuesta() {
        if (this.selectedItemBilleteraComp.equals("-1")) {
            return;
        }
        int id = Integer.parseInt(this.selectedItemBilleteraComp);

        for (Iterator<ComposicionBilletera> it = billeterasComp.iterator(); it.hasNext();) {
            ComposicionBilletera bi = it.next();
            if (id == bi.getComposicionBilleteraId()) {
                this.wallestsComp.add(new WalletUnidadNav(id + "", bi.getNombre()));
                removedSelectItem(id, selectItemsBilleraComp);
                return;
            }
        }
        this.selectedItemBilleteraComp = "-1";
    }

    private void removedSelectItem(int id, List<SelectItem> selectsItems) {
        int index = -1;
        for (int i = 1; i < selectsItems.size(); i++) {
            SelectItem item = selectsItems.get(i);

            if ((Integer) item.getValue() == id) {
                index = i;
                break;
            }

        }

        if (index > 0) {
            selectsItems.remove(index);
        }
    }

    private void cargarParametros() {
        Map<String, String> MapParam = new HashMap();
        for (Parametro parametro : listParametros) {
            MapParam.put(parametro.getNombre(), parametro.getValor());
        }

        default_saludo_inicial = MapParam.get("default_saludo_inicial");
        default_mostrar_saldo_expirado = MapParam.get("default_mostrar_saldo_expirado");
        default_mostrar_vigencia = MapParam.get("default_mostrar_vigencia");

        default_respuesta_falla_conex_comverse = MapParam.get("default_respuesta_falla_conex_comverse");
        default_respuesta_fuera_servicio = MapParam.get("default_respuesta_fuera_servicio");
        default_respuesta_usuario_no_comverse = MapParam.get("default_respuesta_usuario_no_comverse");

        default_texto_saldo_min = MapParam.get("default_texto_saldo_min");
        default_texto_saldo_max = MapParam.get("default_texto_saldo_max");

        default_texto_saldo_min_sw_valor = MapParam.get("default_texto_saldo_min_sw_valor");

        vigencia_saldo_texto = MapParam.get("vigencia_saldo_texto");
        vigencia_saldo_formato_date = MapParam.get("vigencia_saldo_formato_date");

        sistema_consulta_saldo_sw_enable = MapParam.get("sistema_consulta_saldo_sw_enable");

//        default_configuracion_id = MapParam.get("default_configuracion_id");        
        default_wallet_texto_separador = MapParam.get("default_wallet_texto_separador");
        default_wallet_cantidad_decimal = MapParam.get("default_wallet_cantidad_decimal");

        vigencia_saldo_vigencia_ilimitada = MapParam.get("vigencia_saldo_vigencia_ilimitada");
        default_wallet_sw_mostrar_vigencia = MapParam.get("default_wallet_sw_mostrar_vigencia");
        vigencia_saldo_txt_vigencia_ilimitada = MapParam.get("vigencia_saldo_txt_vigencia_ilimitada");

        ///////////////////////////////////////////////////////////////////////////////////////////////
        seg_vigencia_formato_fecha = MapParam.get("seg_vigencia_formato_fecha");
        seg_vigencia_formato_hora = MapParam.get("seg_vigencia_formato_hora");
        seg_vigencia_text_saldo = MapParam.get("seg_vigencia_text_saldo");
        seg_vigencia_dias_exp = MapParam.get("seg_vigencia_dias_exp");

        valor_conversion_unidad_navegacion = MapParam.get("valor_conversion_unidad_navegacion");
        valor_cambio_unidad_navegacion = MapParam.get("valor_cambio_unidad_navegacion");
        valor_unidades_navegacion = MapParam.get("valor_unidades_navegacion");
        valor_billeteras_simple_unidad_navegacion = MapParam.get("valor_billeteras_simple_unidad_navegacion");
        valor_billeteras_agrupadas_unidad_navegacion = MapParam.get("valor_billeteras_agrupadas_unidad_navegacion");
        //valor_billeteras_reserva = MapParam.get("valor_billeteras_reserva");   
        default_cantidad_cabeceras = MapParam.get("default_cantidad_cabeceras");
        default_mostar_minutos = MapParam.get("default_mostar_minutos");

        ///////////////////////////////////////////////////////////////////////////////////////////////
    }

    public String saveChanges() {
        try {
            String str = null;
            //====================================
            if (!isValidFormatDateWithSimpleDate(vigencia_saldo_formato_date)) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "", "El formato para fecha vigencia es incorrecto"));
                return "";
            }
            //====================================
//            if (!isConfiguracionValida(Integer.parseInt(default_configuracion_id))) {
//                FacesContext.getCurrentInstance().addMessage(
//                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                        "", "Configuracion por DEFAULT no válida"));
//                return "";
//            }

            List<Parametro> listParam = new LinkedList<>();
            Parametro ParamSave = new Parametro();
            ParamSave.setNombre("default_saludo_inicial");
            str = ReplaceStrCar_Esp.replaceBase(default_saludo_inicial);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_mostrar_saldo_expiradoo");
            str = ReplaceStrCar_Esp.replaceBase(default_mostrar_saldo_expirado);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_mostrar_vigencia");
            str = default_mostrar_vigencia;
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_falla_conex_comverse");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_falla_conex_comverse);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_fuera_servicio");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_fuera_servicio);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_usuario_no_comverse");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_usuario_no_comverse);
            ParamSave.setValor(str);
            listParam.add(ParamSave);
            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_falla_conex_comverse");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_falla_conex_comverse);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_fuera_servicio");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_fuera_servicio);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_respuesta_usuario_no_comverse");
            str = ReplaceStrCar_Esp.replaceBase(default_respuesta_usuario_no_comverse);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_texto_saldo_min");
            str = ReplaceStrCar_Esp.replaceBase(default_texto_saldo_min);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_texto_saldo_max");
            str = ReplaceStrCar_Esp.replaceBase(default_texto_saldo_max);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_texto_saldo_min_sw_valor");
            str = ReplaceStrCar_Esp.replaceBase(default_texto_saldo_min_sw_valor);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("vigencia_saldo_texto");
            str = ReplaceStrCar_Esp.replaceBase(vigencia_saldo_texto);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("vigencia_saldo_formato_date");
            str = ReplaceStrCar_Esp.replaceBase(vigencia_saldo_formato_date);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            //sistema_consulta_saldo_sw_enable
            ParamSave = new Parametro();
            ParamSave.setNombre("sistema_consulta_saldo_sw_enable");
            str = ReplaceStrCar_Esp.replaceBase(sistema_consulta_saldo_sw_enable);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_wallet_cantidad_decimal");
            str = default_wallet_cantidad_decimal;
            ParamSave.setValor(str);
            listParam.add(ParamSave);
            //default_wallet_cantidad_decimal

            //default_wallet_texto_separador            
            ParamSave = new Parametro();
            ParamSave.setNombre("default_wallet_texto_separador");
            str = ReplaceStrCar_Esp.replaceBase(default_wallet_texto_separador);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("vigencia_saldo_vigencia_ilimitada");
            str = ReplaceStrCar_Esp.replaceBase(vigencia_saldo_vigencia_ilimitada);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_wallet_sw_mostrar_vigencia");
            str = ReplaceStrCar_Esp.replaceBase(default_wallet_sw_mostrar_vigencia);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("vigencia_saldo_txt_vigencia_ilimitada");
            str = ReplaceStrCar_Esp.replaceBase(vigencia_saldo_txt_vigencia_ilimitada);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ///////////////////////////////////////////////////////////////////////////
            ParamSave = new Parametro();
            ParamSave.setNombre("seg_vigencia_formato_fecha");
            str = ReplaceStrCar_Esp.replaceBase(seg_vigencia_formato_fecha);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("seg_vigencia_formato_hora");
            str = ReplaceStrCar_Esp.replaceBase(seg_vigencia_formato_hora);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("seg_vigencia_text_saldo");
            str = ReplaceStrCar_Esp.replaceBase(seg_vigencia_text_saldo);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("seg_vigencia_dias_exp");
            str = ReplaceStrCar_Esp.replaceBase(seg_vigencia_dias_exp);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            mapedWalletsSelects_To_IDs();//mapedo de IDS

            ParamSave = new Parametro();
            ParamSave.setNombre("valor_conversion_unidad_navegacion");
            str = ReplaceStrCar_Esp.replaceBase(valor_conversion_unidad_navegacion);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("valor_cambio_unidad_navegacion");
            str = ReplaceStrCar_Esp.replaceBase(valor_cambio_unidad_navegacion);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("valor_unidades_navegacion");
            str = ReplaceStrCar_Esp.replaceBase(valor_unidades_navegacion);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("valor_billeteras_simple_unidad_navegacion");
            str = ReplaceStrCar_Esp.replaceBase(valor_billeteras_simple_unidad_navegacion);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("valor_billeteras_agrupadas_unidad_navegacion");
            str = ReplaceStrCar_Esp.replaceBase(valor_billeteras_agrupadas_unidad_navegacion);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_cantidad_cabeceras");
            str = default_cantidad_cabeceras;
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParamSave = new Parametro();
            ParamSave.setNombre("default_mostar_minutos");
            str = default_mostar_minutos;
            ParamSave.setValor(str);
            listParam.add(ParamSave);
            ///////////////////////////////////////////////////////////////////////////
            //default_wallet_sw_mostrar_vigencia = MapParam.get("default_wallet_sw_mostrar_vigencia");
            //vigencia_saldo_txt_vigencia_ilimitada = MapParam.get("vigencia_saldo_txt_vigencia_ilimitada");
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateListaParametros(listParam);
            //ACTUALIZAR LISTA EN BD
            ControlerBitacora.update(DescriptorBitacoraLC.OPCIONES_GENERALES, "todos", "parametros");
            //ControlerBitacora.insertBitacora(DescriptorBitacoraLC.SMS, "Actualizo parametros Canal SMS");
            updateTextState();
            //marcar variable de aplicaion para q se actualice
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "", "Cambios guardados correctamente"));

        } catch (Exception e) {
            log.error("[saveChanges]|user=" + userId + " intentando guardar : " + e.getMessage());
        }
        return "";
    }

    private void mapedWalletsSelects_To_IDs() {
        valor_billeteras_simple_unidad_navegacion = "";
        for (WalletUnidadNav bi : wallestsSimple) {
            if (valor_billeteras_simple_unidad_navegacion.isEmpty()) {
                valor_billeteras_simple_unidad_navegacion = bi.getId();
            } else {
                valor_billeteras_simple_unidad_navegacion = valor_billeteras_simple_unidad_navegacion + "," + bi.getId();
            }
        }
        /* valor_billeteras_reserva = "";
        for (Iterator<WalletUnidadNav> it = wallestsReserva.iterator(); it.hasNext();) {
        WalletUnidadNav bi = it.next();
        if (valor_billeteras_reserva.isEmpty()) {
        valor_billeteras_reserva = bi.getId();
        } else {
        valor_billeteras_reserva = valor_billeteras_reserva + "," + bi.getId();
        }
        }*/

        valor_billeteras_agrupadas_unidad_navegacion = "";
        for (WalletUnidadNav bi : wallestsComp) {
            if (valor_billeteras_agrupadas_unidad_navegacion.isEmpty()) {
                valor_billeteras_agrupadas_unidad_navegacion = bi.getId();
            } else {
                valor_billeteras_agrupadas_unidad_navegacion = valor_billeteras_agrupadas_unidad_navegacion + "," + bi.getId();
            }
        }
    }

    public boolean isValidFormatDateWithSimpleDate(String format) {
        //String s = "";
        Calendar cal = Calendar.getInstance();
        try {
            String formatDateExpiration = format;
            SimpleDateFormat formateador = new SimpleDateFormat(formatDateExpiration);
            formateador.format(cal.getTime());
            return true;
        } catch (Exception e) {
            log.warn("[formatDateWithSimpleDate] Al intentar dar formato fecha:" + e.getMessage());
            return false;
        }

    }

    public void updateTextState() {
        if (sistema_consulta_saldo_sw_enable.equals("true") || sistema_consulta_saldo_sw_enable.equals("t")) {
            textEstadoSistema = "HABILITADO";
        } else {
            textEstadoSistema = "DESHABILITADO";
        }
    }
//     private void cargarItemsBilleteras(){       
//        List<Billetera> list = walletBL.obtenerLista();
//        selectedItemBilletera= "-1";
//        selectItemsBillera = new LinkedList<SelectItem>();
//        SelectItem s = new SelectItem("-1", "Seleccionar Billetera");
//        selectItemsBillera.add(s);
//      
//       for (Billetera billetera : list) {
//               SelectItem sel = new SelectItem(billetera.getBilleteraId(),billetera.getNombreComverse());
//               selectItemsBillera.add(sel);
//        }
//    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public String getDefault_mostrar_saldo_expirado() {
        return default_mostrar_saldo_expirado;
    }

    public void setDefault_mostrar_saldo_expirado(String default_mostrar_saldo_expirado) {
        this.default_mostrar_saldo_expirado = default_mostrar_saldo_expirado;
    }

    public String getDefault_mostrar_vigencia() {
        return default_mostrar_vigencia;
    }

    public void setDefault_mostrar_vigencia(String default_mostrar_vigencia) {
        this.default_mostrar_vigencia = default_mostrar_vigencia;
    }

    public String getDefault_saludo_inicial() {
        return default_saludo_inicial;
    }

    public void setDefault_saludo_inicial(String default_saludo_inicial) {
        this.default_saludo_inicial = default_saludo_inicial;
    }

    public String getDefault_respuesta_falla_conex_comverse() {
        return default_respuesta_falla_conex_comverse;
    }

    public void setDefault_respuesta_falla_conex_comverse(String default_respuesta_falla_conex_comverse) {
        this.default_respuesta_falla_conex_comverse = default_respuesta_falla_conex_comverse;
    }

    public String getDefault_respuesta_fuera_servicio() {
        return default_respuesta_fuera_servicio;
    }

    public void setDefault_respuesta_fuera_servicio(String default_respuesta_fuera_servicio) {
        this.default_respuesta_fuera_servicio = default_respuesta_fuera_servicio;
    }

    public String getDefault_respuesta_usuario_no_comverse() {
        return default_respuesta_usuario_no_comverse;
    }

    public void setDefault_respuesta_usuario_no_comverse(String default_respuesta_usuario_no_comverse) {
        this.default_respuesta_usuario_no_comverse = default_respuesta_usuario_no_comverse;
    }

    public String getDefault_texto_saldo_max() {
        return default_texto_saldo_max;
    }

    public void setDefault_texto_saldo_max(String default_texto_saldo_max) {
        this.default_texto_saldo_max = default_texto_saldo_max;
    }

    public String getDefault_texto_saldo_min() {
        return default_texto_saldo_min;
    }

    public void setDefault_texto_saldo_min(String default_texto_saldo_min) {
        this.default_texto_saldo_min = default_texto_saldo_min;
    }

    public String getDefault_texto_saldo_min_sw_valor() {
        return default_texto_saldo_min_sw_valor;
    }

    public void setDefault_texto_saldo_min_sw_valor(String default_texto_saldo_min_sw_valor) {
        this.default_texto_saldo_min_sw_valor = default_texto_saldo_min_sw_valor;
    }

    public String getVigencia_saldo_formato_date() {
        return vigencia_saldo_formato_date;
    }

    public void setVigencia_saldo_formato_date(String vigencia_saldo_formato_date) {
        this.vigencia_saldo_formato_date = vigencia_saldo_formato_date;
    }

    public String getVigencia_saldo_texto() {
        return vigencia_saldo_texto;
    }

    public void setVigencia_saldo_texto(String vigencia_saldo_texto) {
        this.vigencia_saldo_texto = vigencia_saldo_texto;
    }

    public String getSistema_consulta_saldo_sw_enable() {
        return sistema_consulta_saldo_sw_enable;
    }

    public void setSistema_consulta_saldo_sw_enable(String sistema_consulta_saldo_sw_enable) {
        this.sistema_consulta_saldo_sw_enable = sistema_consulta_saldo_sw_enable;
    }

    public String getTextEstadoSistema() {
        return textEstadoSistema;
    }

    public void setTextEstadoSistema(String textEstadoSistema) {
        this.textEstadoSistema = textEstadoSistema;
    }

    public String getDefault_wallet_cantidad_decimal() {
        return default_wallet_cantidad_decimal;
    }

    public void setDefault_wallet_cantidad_decimal(String default_wallet_cantidad_decimal) {
        this.default_wallet_cantidad_decimal = default_wallet_cantidad_decimal;
    }

    public String getDefault_wallet_texto_separador() {
        return default_wallet_texto_separador;
    }

    public void setDefault_wallet_texto_separador(String default_wallet_texto_separador) {
        this.default_wallet_texto_separador = default_wallet_texto_separador;
    }

    public String getVigencia_saldo_vigencia_ilimitada() {
        return vigencia_saldo_vigencia_ilimitada;
    }

    public void setVigencia_saldo_vigencia_ilimitada(String vigencia_saldo_vigencia_ilimitada) {
        this.vigencia_saldo_vigencia_ilimitada = vigencia_saldo_vigencia_ilimitada;
    }

    public String getDefault_wallet_sw_mostrar_vigencia() {
        return default_wallet_sw_mostrar_vigencia;
    }

    public void setDefault_wallet_sw_mostrar_vigencia(String default_wallet_sw_mostrar_vigencia) {
        this.default_wallet_sw_mostrar_vigencia = default_wallet_sw_mostrar_vigencia;
    }

    public String getVigencia_saldo_txt_vigencia_ilimitada() {
        return vigencia_saldo_txt_vigencia_ilimitada;
    }

    public void setVigencia_saldo_txt_vigencia_ilimitada(String vigencia_saldo_txt_vigencia_ilimitada) {
        this.vigencia_saldo_txt_vigencia_ilimitada = vigencia_saldo_txt_vigencia_ilimitada;
    }

    public String getSeg_vigencia_formato_fecha() {
        return seg_vigencia_formato_fecha;
    }

    public void setSeg_vigencia_formato_fecha(String seg_vigencia_formato_fecha) {
        this.seg_vigencia_formato_fecha = seg_vigencia_formato_fecha;
    }

    public String getSeg_vigencia_formato_hora() {
        return seg_vigencia_formato_hora;
    }

    public void setSeg_vigencia_formato_hora(String seg_vigencia_formato_hora) {
        this.seg_vigencia_formato_hora = seg_vigencia_formato_hora;
    }

    public String getSeg_vigencia_text_saldo() {
        return seg_vigencia_text_saldo;
    }

    public void setSeg_vigencia_text_saldo(String seg_vigencia_text_saldo) {
        this.seg_vigencia_text_saldo = seg_vigencia_text_saldo;
    }

    public String getSeg_vigencia_dias_exp() {
        return seg_vigencia_dias_exp;
    }

    public void setSeg_vigencia_dias_exp(String seg_vigencia_dias_exp) {
        this.seg_vigencia_dias_exp = seg_vigencia_dias_exp;
    }

    public String getValor_conversion_unidad_navegacion() {
        return valor_conversion_unidad_navegacion;
    }

    public void setValor_conversion_unidad_navegacion(String valor_conversion_unidad_navegacion) {
        this.valor_conversion_unidad_navegacion = valor_conversion_unidad_navegacion;
    }

    public String getValor_cambio_unidad_navegacion() {
        return valor_cambio_unidad_navegacion;
    }

    public void setValor_cambio_unidad_navegacion(String valor_cambio_unidad_navegacion) {
        this.valor_cambio_unidad_navegacion = valor_cambio_unidad_navegacion;
    }

    public String getValor_unidades_navegacion() {
        return valor_unidades_navegacion;
    }

    public void setValor_unidades_navegacion(String valor_unidades_navegacion) {
        this.valor_unidades_navegacion = valor_unidades_navegacion;
    }

    public String getValor_billeteras_simple_unidad_navegacion() {
        return valor_billeteras_simple_unidad_navegacion;
    }

    public void setValor_billeteras_simple_unidad_navegacion(String valor_billeteras_simple_unidad_navegacion) {
        this.valor_billeteras_simple_unidad_navegacion = valor_billeteras_simple_unidad_navegacion;
    }

    public String getValor_billeteras_agrupadas_unidad_navegacion() {
        return valor_billeteras_agrupadas_unidad_navegacion;
    }

    public void setValor_billeteras_agrupadas_unidad_navegacion(String valor_billeteras_agrupadas_unidad_navegacion) {
        this.valor_billeteras_agrupadas_unidad_navegacion = valor_billeteras_agrupadas_unidad_navegacion;
    }

    public List<Billetera> getBilleterasSimples() {
        return billeterasSimples;
    }

    public void setBilleterasSimples(List<Billetera> billeterasSimples) {
        this.billeterasSimples = billeterasSimples;
    }

    public List<ComposicionBilletera> getBilleterasComp() {
        return billeterasComp;
    }

    public void setBilleterasComp(List<ComposicionBilletera> billeterasComp) {
        this.billeterasComp = billeterasComp;
    }

    public List<WalletUnidadNav> getWallestsSimple() {
        return wallestsSimple;
    }

    public void setWallestsSimple(List<WalletUnidadNav> wallestsSimple) {
        this.wallestsSimple = wallestsSimple;
    }

    public List<WalletUnidadNav> getWallestsComp() {
        return wallestsComp;
    }

    public void setWallestsComp(List<WalletUnidadNav> wallestsComp) {
        this.wallestsComp = wallestsComp;
    }

    public String getSelectedItemBilleteraSimple() {
        return selectedItemBilleteraSimple;
    }

    public void setSelectedItemBilleteraSimple(String selectedItemBilleteraSimple) {
        this.selectedItemBilleteraSimple = selectedItemBilleteraSimple;
    }

    public String getSelectedItemBilleteraComp() {
        return selectedItemBilleteraComp;
    }

    public void setSelectedItemBilleteraComp(String selectedItemBilleteraComp) {
        this.selectedItemBilleteraComp = selectedItemBilleteraComp;
    }

    public List<SelectItem> getSelectItemsBilleraSimple() {
        return selectItemsBilleraSimple;
    }

    public void setSelectItemsBilleraSimple(List<SelectItem> selectItemsBilleraSimple) {
        this.selectItemsBilleraSimple = selectItemsBilleraSimple;
    }

    public List<SelectItem> getSelectItemsBilleraComp() {
        return selectItemsBilleraComp;
    }

    public void setSelectItemsBilleraComp(List<SelectItem> selectItemsBilleraComp) {
        this.selectItemsBilleraComp = selectItemsBilleraComp;
    }

    public Integer getCantidadCabecera() {
        return cantidadCabecera;
    }

    public void setCantidadCabecera(Integer cantidadCabecera) {
        this.cantidadCabecera = cantidadCabecera;
    }

    public String getDefault_cantidad_cabeceras() {
        return default_cantidad_cabeceras;
    }

    public void setDefault_cantidad_cabeceras(String default_cantidad_cabeceras) {
        this.default_cantidad_cabeceras = default_cantidad_cabeceras;
    }

    public List<WalletUnidadNav> getWallestsReserva() {
        return wallestsReserva;
    }

    public void setWallestsReserva(List<WalletUnidadNav> wallestsReserva) {
        this.wallestsReserva = wallestsReserva;
    }

    public List<SelectItem> getSelectItemsBilleraReserva() {
        return selectItemsBilleraReserva;
    }

    public void setSelectItemsBilleraReserva(List<SelectItem> selectItemsBilleraReserva) {
        this.selectItemsBilleraReserva = selectItemsBilleraReserva;
    }

    public String getSelectedItemBilleteraReserva() {
        return selectedItemBilleteraReserva;
    }

    public void setSelectedItemBilleteraReserva(String selectedItemBilleteraReserva) {
        this.selectedItemBilleteraReserva = selectedItemBilleteraReserva;
    }

    public String getDefault_mostar_minutos() {
        return default_mostar_minutos;
    }

    public void setDefault_mostar_minutos(String default_mostar_minutos) {
        this.default_mostar_minutos = default_mostar_minutos;
    }

}
