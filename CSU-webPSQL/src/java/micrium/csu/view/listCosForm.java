/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import micrium.csu.bussines.BilleteraBUSSI;
//import micrium.csu.bussines.CosBUSSI;

import micrium.csu.model.Billetera;
//import micrium.csu.model.Cos;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@RequestScoped
public class listCosForm implements Serializable{
    private static final long serialVersionUID = 1L;

   
    private String idUser;
   
    private static final Logger log = Logger.getLogger(listCosForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
  //  private static CosBUSSI cosBL = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
//    private List<Cos>cosList;
    
//    
    //private List<> listCOS;

    /** Creates a new instance of walletForm */
    public listCosForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
    }

    @PostConstruct
    public void init() {

        try {
           
            idUser = ControlerBitacora.getNombreUser();
            
            cargarListCOS();
        } catch (Exception e) {
            log.error("[init]:" + e);
        }
    }

//    public void editWallet() {
//        //String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("walletId");
//        try {
//           // int id = Integer.parseInt(Idstr);
//           
//        } catch (Exception e) {
//            log.error("[editWallet]|user=" + idUser + "|Error al intentar guardar cambios en Billetera:" + e.getMessage());
//        }
//
//
//    }
    void cargarListCOS(){
//       cosList = cosBL.obtenerLista();    
    }
//    public void deleteWallet() {
////        String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("walletId");
////        int id = Integer.parseInt(Idstr);
////        log.debug("id billetera " + Idstr);
////        try {
////            billetera = walletBL.obtenerId(id);
////            walletBL.delete(id);
////            //ControlerBitacora.delete(DescriptorBitacoraLC.BILLETERA, wallet.getWalletId() + "", wallet.getName());
////            billeteraList = walletBL.obtenerLista();
////            newWallet();
////            log.info("[deleteWallet]|user=" + idUser + "BilleteraId=" + id + "  Eliminado registro correctamente");
////        } catch (Exception e) {
////            log.error("[deleteWallet]|user=" + idUser + "Al intentar eliminar billetera:" + e.getMessage());
////
////        }
//    }

    private String validar(String palabra) {
        if (palabra.equals("")) {
            return "campo vacio";
        }
        palabra = palabra.toUpperCase();
        try {
            BilleteraBUSSI datoBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
            List<Billetera> lista = datoBL.obtenerLista();
            for (int i = 0; i < lista.size(); i++) {
                Billetera dato = lista.get(i);
                String str = dato.getNombreComverse().toUpperCase();
                if (str.equals(palabra)) {
                    return "Ya existe una billetera con ese nombre";
                }
            }
        } catch (Exception e) {
            log.error("[validar]|user=" + idUser + " Error al validar: " + e.getMessage());
        }
        return "";
    }

    public void newWallet() {
//        edit = false;
//        billetera = new Billetera();
    }

//  private void cargarItemsCOS(){
//        UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
//        List<UnitType> list = qFlBL.obtenerLista();
//        selItemUnitType= "-1";
//        selectItemsUnitType = new LinkedList<SelectItem>();
//        SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
//        selectItemsUnitType.add(s);
//      
//       for (UnitType unitType : list) {
//            SelectItem sel = new SelectItem(unitType.getUnitTypeId(),unitType.getNombre());
//            selectItemsUnitType.add(sel);
//        }
//    }
//    
//
//    public List<Cos> getCosList() {
//        return cosList;
//    }
//
//    public void setCosList(List<Cos> cosList) {
//        this.cosList = cosList;
//    }
//    

    
}
