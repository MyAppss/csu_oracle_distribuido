/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.*;
import micrium.csu.model.*;
//import micrium.csu.model.Cos;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@ViewScoped
public class listConfigForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idUser;
    private static final Logger log = Logger.getLogger(listConfigForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private static ConfigBUSSI configBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
    private List<Config> configList;
    private String rowMaxTable = Parameters.NroFilasDataTable;
    private static OccBUSSI occBL = GuiceInjectorSingleton.getInstance(OccBUSSI.class);

    //private String nameNewConfigClone;
//    
    //private List<> listCOS;
    /**
     * Creates a new instance of walletForm
     */
    public listConfigForm() {
        log.debug("[listConfigForm]......................constructors");
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();

    }

    @PostConstruct
    public void init() {
        log.debug("[init] inciando metodo............");
        try {

            idUser = ControlerBitacora.getNombreUser();
            //nameNewConfigClone = "";
            cargarListConfigs();
        } catch (Exception e) {
            log.error("[init]:" + e);
        }

        log.debug("[init] finalizando metodo.........");
    }

//    public void editWallet() {
//        String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("walletId");
//        try {
//            int id = Integer.parseInt(Idstr);
//           
//        } catch (Exception e) {
//            log.error("[editWallet]|user=" + idUser + "|Error al intentar guardar cambios en Billetera:" + e.getMessage());
//        }
//
//
//    }
    public void cloneConfig() {

        try {

            log.debug("[cloneConfig]UserId=" + idUser + "|iniciando..");
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");
            int id = Integer.parseInt(Idstr);
            log.info("[cloneConfig]UserId=" + idUser + "|ConfigId=" + id + " iniciando proceso para clonar");

            Config myConfig = configBL.obtenerId(id);
            log.debug("[cloneConfig]-----1");
            //myConfig.setNombre(myConfig.getNombre()+"_clone");
            String NameClone = generarNombreValido(myConfig);
            myConfig.setNombre(NameClone);
            idUser = ControlerBitacora.getNombreUser();

            BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
            CabeceraBUSSI cabeceraBL = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
            MenuBUSSI menuBL = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
            ConfigAcumuladoBUSSI configAcumuladoBL = GuiceInjectorSingleton.getInstance(ConfigAcumuladoBUSSI.class);
            OccBUSSI occBL = GuiceInjectorSingleton.getInstance(OccBUSSI.class);
            //Obtener la lista de Billeteras de la Configuraion a Clonar
            List<Billetera> billeteraList = walletBL.obtenerListaByIdConfig(id);
            //Obtener la lista de Cabeceras 
            List<Cabecera> cabeceraList = cabeceraBL.obtenerByIdConfig(id);

            //Obtener Menu para la COnfig
            List<Menu> menuList = menuBL.obtenerListaByConfigId(id);
            //Obtener ConfigAcumulado
            List<ConfigAcumulado> configAcumuladoList = configAcumuladoBL.listByIdConfig(id);
            List<Occ> occList = occBL.obtenerListConfigId(id);

            if (billeteraList != null) {
                log.debug("[cloneConfig] lista billetera recuperar  size=" + billeteraList.size());
            }
            if (cabeceraList != null) {
                log.debug("[cloneConfig] lista cabecera recuperar  size=" + cabeceraList.size());
            }
            if (menuList != null) {
                log.debug("[cloneConfig] lista menu recuperar  size=" + menuList.size());
            }
            if (configAcumuladoList != null) {
                log.debug("[cloneConfig] lista ConfigAcumulado recuperar  size=" + configAcumuladoList.size());
            }
            if (occList != null) {
                log.debug("[cloneConfig] lista ConfigAcumulado recuperar  size=" + occList.size());
            }

            ComposicionBilleteraBUSSI compositionWalletBL = GuiceInjectorSingleton.getInstance(ComposicionBilleteraBUSSI.class);
            List<ComposicionBilletera> composicionBilleteraList = compositionWalletBL.obtenerListaByIdConfig(id);
            if (composicionBilleteraList != null) {
                log.debug("[cloneConfig] lista ComposicionBilletera recuperar  size=" + composicionBilleteraList.size());
            }

            //guardar copia-----------------------------------------------------
            Config newConfig = new Config();
            newConfig.setNombre(myConfig.getNombre());
            newConfig.setSaludoInicial(myConfig.getSaludoInicial());
            newConfig.setDescripcion(myConfig.getDescripcion());
            newConfig.setMostrarDpi(myConfig.getMostrarDpi());
            newConfig.setMostrarAcumuladosMegas(myConfig.getMostrarAcumuladosMegas());
            newConfig.setMostrarVigencia(myConfig.getMostrarVigencia());
            newConfig.setMostrarBilleterasNoConfig(myConfig.getMostrarBilleterasNoConfig());
            newConfig.setHabilitado(myConfig.getHabilitado());
            newConfig.setEstado("t");
            newConfig.setLineas(myConfig.getLineas());
            newConfig.setNombreAcumulado(myConfig.getNombreAcumulado());
            newConfig.setMostrarSiempre(myConfig.getMostrarSiempre());
            configBL.insert(newConfig);
            //------------------------------------------------------------------
            //guardar copia de lista billeteras
            if (billeteraList != null) {
                saveListaConfigBilletera(billeteraList, newConfig.getConfigId());
            }
            //------------------------------------------------------------------
            if (composicionBilleteraList != null) {
                saveListaConfigCompBilletera(composicionBilleteraList, newConfig.getConfigId());
            }
            if (cabeceraList != null) {
                guardarCabeceras(cabeceraList, newConfig.getConfigId());
            }
            if (menuList != null) {
                guardarMenus(menuList, newConfig.getConfigId());
            }
            if (configAcumuladoList != null) {
                guardarConfigAcumulado(configAcumuladoList, newConfig.getConfigId());
            }

            if (occList != null) {
                guardarOcc(occList, newConfig.getConfigId());
            }

            cargarListConfigs();

            ControlerBitacora.insert(DescriptorBitacoraLC.CONFIGURACION, myConfig.getConfigId() + "", myConfig.getNombre());
        } catch (Exception e) {
            log.error("[cloneConfig]|user=" + idUser + "|Error al intentar clonar Configuracion:" + e.getMessage());
        }

    }

    public void deleteConfig() {
        try {
            log.debug("[deleteConfig]UserId=" + idUser + "|iniciando..");
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");
            int id = Integer.parseInt(Idstr);
            log.info("[deleteConfig]UserId=" + idUser + "|ConfigId=" + id + " iniciando proceso para eliminar");
            if (!configBL.delete(id)) {
                log.warn("[deleteConfig]UserId=" + idUser + "|ConfigId=" + id + "]No se pudo eliminar correctamente CONFIG");
            } else {
//                CosBUSSI CosBussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
//                List<Cos> listCos = null;
                //eliminar relacion con este config
//                UpdateListCosSelectConfig(listCos, 0);

                if (!occBL.deleteConfig(id)) {
                    log.warn("[deleteConfig]UserId=" + idUser + "|ConfigId=" + id + "]No se pudo eliminar correctamente los OCC");
                }

                log.info("[deleteConfig]UserId=" + idUser + "|ConfigId=" + id + "|Se elimino correctamente");
                cargarListConfigs();
                ControlerBitacora.delete(DescriptorBitacoraLC.CONFIGURACION, Idstr + "", "");
            }
        } catch (NumberFormatException e) {
            log.error("[deleteConfig]|user=" + idUser + "|Error al intentar eliminar Configuracion:" + e.getMessage());
        }
    }

    private String generarNombreValido(Config newConfig) {
        try {
            String nameBase = newConfig.getNombre() + "_clone";
            String newName = nameBase;
            int count = 1;

            while (configBL.obtenerByName(newName) != null) {
                count++;
                newName = nameBase + count;
            }
            return newName;
        } catch (Exception e) {
            log.error("[generarNombreValido]UserId=" + idUser + "|ConfigId=" + newConfig.getConfigId() + "|Error:" + e.getMessage());
        }

        return "";
    }

    private String saveListaConfigBilletera(List<Billetera> listConfigBill, int idConfig) {
        log.info("[saveListaConfigBilletera]UserId=" + idUser + "|ConfigId=" + idConfig + " iniciando proceso de guardar Config_billetera");
        ConfigBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigBilleteraBUSSI.class);
        String str = "";
        try {
            for (Billetera bill : listConfigBill) {
                ConfigBilletera configBill = new ConfigBilletera();
                configBill.setBilleteraId(bill.getBilleteraId());
                configBill.setConfigId(idConfig);
                configBill.setEstado("t");
                configBill.setMostrarSaldoCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_cero()));
                configBill.setNoMostrarSaldoExpirado(BooleantoString(bill.isConfigBilletera_no_mostrar_saldo_expirado()));
                configBill.setMostrarSaldoMayorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_mayor_cero()));
                configBill.setMostrarSaldoMenorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_menor_cero()));
                configBill.setMostrarSiempre(BooleantoString(bill.isConfigBilletera_mostrar_siempre()));
                configBill.setNombreComercial(bill.getConfigBilletera_nombreComercial());
                log.debug("[saveListaConfigBilletera]nombre comercial=" + configBill.getNombreComercial());

                bl.insert(configBill);
            }

        } catch (Exception e) {
            log.error("[saveListaConfigBilletera] Al intentar actualizar COS:" + e.getMessage());
        }
        return str;
    }

    private String saveListaConfigCompBilletera(List<ComposicionBilletera> listCompCos, int idConfig) {
        log.info("[saveListaConfigCompBilletera]IdConfig=" + idConfig + " Iniciando proceso para guardar registro..");
        String str = "";
        ConfigComposicionBilleteraBUSSI configCompBillBL = GuiceInjectorSingleton.getInstance(ConfigComposicionBilleteraBUSSI.class);
        for (ComposicionBilletera dat : listCompCos) {
            ConfigComposicionBilletera cb = new ConfigComposicionBilletera();
            cb.setComposicionBilleteraId(dat.getComposicionBilleteraId());
            cb.setConfigId(idConfig);
            cb.setEstado("t");
            cb.setMostrarSaldoCero(BooleantoString(dat.isConfig_mostrar_saldo_cero()));
            cb.setNoMostrarSaldoExpirado(BooleantoString(dat.isConfig_no_mostrar_saldo_expirado()));
            cb.setMostrarSaldoMayorCero(BooleantoString(dat.isConfig_mostrar_saldo_mayor_cero()));
            cb.setMostrarSaldoMenorCero(BooleantoString(dat.isConfig_mostrar_saldo_menor_cero()));
            cb.setMostrarSiempre(BooleantoString(dat.isConfig_mostrar_siempre()));
            cb.setNombreComercial(dat.getConfig_nombreComercial());
            configCompBillBL.insert(cb);
        }
        return str;
    }

    void cargarListConfigs() {
        try {
            log.info("[cargarListConfigs] Iniciando proceso de cargar Lista Configs");
            configList = configBL.obtenerLista();
            for (Config config : configList) {
                List<OccView> listOccView = occBL.obtenerListaView(config.getConfigId());
                StringBuilder stb = new StringBuilder();
                if (config.getMostrarDpi() != null
                        && config.getMostrarDpi().equals("t")) {
                    config.setMostrarDpi("true");
                }

                if (config.getMostrarAcumuladosMegas() != null
                        && config.getMostrarAcumuladosMegas().equals("t")) {
                    config.setMostrarAcumuladosMegas("true");
                }

                for (OccView occView : listOccView) {
                    if (stb.length() > 0) {
                        stb.append("<br>" + occView.getLabel());
                    } else {
                        stb.append(occView.getLabel());
                    }

                }
                config.setStrCos(stb.toString());
            }

        } catch (Exception e) {
            log.error("[cargarListConfigs] error al intentar cargar Lista :" + e.getMessage());
        }
    }

    private String validar(String palabra) {
        if (palabra.equals("")) {
            return "campo vacio";
        }
        palabra = palabra.toUpperCase();
        try {
            BilleteraBUSSI datoBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
            List<Billetera> lista = datoBL.obtenerLista();
            for (int i = 0; i < lista.size(); i++) {
                Billetera dato = lista.get(i);
                String str = dato.getNombreComverse().toUpperCase();
                if (str.equals(palabra)) {
                    return "Ya existe una billetera con ese nombre";
                }
            }
        } catch (Exception e) {
            log.error("[validar]|user=" + idUser + " Error al validar: " + e.getMessage());
        }
        return "";
    }

    private String UpdateListCosSelectConfig(List<Cos> listCos, int idConfig) {
        log.info("[UpdateListCosSelectConfig]UserId=" + idUser + "|ConfigId=" + idConfig + " iniciando proceso de guardar Config_billetera");
        CosBUSSI CosBussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
        String str = "";
        try {
            for (Cos myCos : listCos) {
                if (myCos != null) {
//                      myCos.setConfigId(idConfig);
                    CosBussi.update(myCos);
                }
            }

        } catch (Exception e) {
            log.error("[UpdateListCosSelectConfig] Al intentar actualizar COS:" + e.getMessage());
        }
        return str;
    }

//  private void cargarItemsCOS(){
//        UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
//        List<UnitType> list = qFlBL.obtenerLista();
//        selItemUnitType= "-1";
//        selectItemsUnitType = new LinkedList<SelectItem>();
//        SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
//        selectItemsUnitType.add(s);
//      
//       for (UnitType unitType : list) {
//            SelectItem sel = new SelectItem(unitType.getUnitTypeId(),unitType.getNombre());
//            selectItemsUnitType.add(sel);
//        }
//    }
//    
    public List<Config> getConfigList() {
        return configList;
    }

    public void setConfigList(List<Config> configList) {
        this.configList = configList;
    }

//    public String getNameNewConfigClone() {
//        return nameNewConfigClone;
//    }
//
//    public void setNameNewConfigClone(String nameNewConfigClone) {
//        this.nameNewConfigClone = nameNewConfigClone;
//    }
    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public String BooleantoString(Boolean value) {
        return value ? "t" : "f";
    }

    public void guardarCabeceras(List<Cabecera> listaCabecera, int idConfig) {
        try {
            CabeceraBUSSI cabeceraBL = GuiceInjectorSingleton.getInstance(CabeceraBUSSI.class);
            DetalleCabeceraBUSSI detalleCabeceraBL = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);
            if (listaCabecera != null && idConfig > 0) {
                for (Cabecera cabecera : listaCabecera) {
                    int idCabeceraOld = cabecera.getCabeceraId();
                    cabecera.setConfiguracionId(idConfig);
                    cabeceraBL.insert(cabecera);
                    List<DetalleCabecera> detalleCabeceraList = detalleCabeceraBL.obtenerIdCabecera(idCabeceraOld);
                    if (detalleCabeceraList != null) {
                        guardarDetalleCabecera(detalleCabeceraList, cabecera.getCabeceraId());
                    }

                }
            }
        } catch (Exception e) {
            log.error("[guardarDetalleCabecera] [PROCESO DE CLONACION] [Error al guardar la lista de Cabecera para la Configuración con ID " + idConfig + " ] " + e.getMessage());
        }
    }

    public void guardarDetalleCabecera(List<DetalleCabecera> detalleCabeceraList, int idCabecera) {
        try {
            DetalleCabeceraBUSSI detalleCabeceraBL = GuiceInjectorSingleton.getInstance(DetalleCabeceraBUSSI.class);

            if (detalleCabeceraList != null && idCabecera > 0) {
                for (DetalleCabecera detalleCabecera : detalleCabeceraList) {
                    detalleCabecera.setCabeceraId(idCabecera);
                    detalleCabeceraBL.insert(detalleCabecera);

                }
            }

        } catch (Exception e) {
            log.error("[guardarDetalleCabecera] [PROCESO DE CLONACION] [Error al guardar la lista de DetalleCabecera para la Cabecera con ID " + idCabecera + " ] " + e.getMessage());
        }
    }

    public void guardarMenus(List<Menu> listaMenu, int idConfig) {
        try {
            MenuBUSSI menuBL = GuiceInjectorSingleton.getInstance(MenuBUSSI.class);
            DetalleMenuBUSSI detalleMenuBL = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);
            if (listaMenu != null && idConfig > 0) {
                for (Menu menu : listaMenu) {
                    int idMenuOld = menu.getMenuId();
                    menu.setConfiguracionId(idConfig);
                    menuBL.insert(menu);

                    List<DetalleMenu> detalleMenuList = detalleMenuBL.obtenerIdCabecera(idMenuOld);
                    if (detalleMenuList != null) {
                        guardarDetalleMenu(detalleMenuList, menu.getMenuId());
                    }

                }
            }
        } catch (Exception e) {
            log.error("[guardarMenus] [PROCESO DE CLONACION] [Error al guardar la lista de Menu para la COnfiguración con ID " + idConfig + " ] " + e.getMessage());
        }
    }

    public void guardarDetalleMenu(List<DetalleMenu> detalleMenuList, int idMenu) {
        try {
            DetalleMenuBUSSI detalleMenuBL = GuiceInjectorSingleton.getInstance(DetalleMenuBUSSI.class);

            if (detalleMenuList != null && idMenu > 0) {
                for (DetalleMenu detalleMenu : detalleMenuList) {
                    detalleMenu.setMenuId(idMenu);
                    detalleMenuBL.insert(detalleMenu);

                }
            }

        } catch (Exception e) {
            log.error("[guardarDetalleMenu] [PROCESO DE CLONACION] [Error al guardar la lista de DetalleMenu para el Menu con ID " + idMenu + " ] " + e.getMessage());
        }
    }

    public void guardarConfigAcumulado(List<ConfigAcumulado> configAcumuladoList, int idConfig) {
        try {
            ConfigAcumuladoBUSSI configAcumuladoBL = GuiceInjectorSingleton.getInstance(ConfigAcumuladoBUSSI.class);
            if (configAcumuladoList != null && idConfig > 0) {
                for (ConfigAcumulado configAcumulado : configAcumuladoList) {
                    configAcumulado.setConfigId(idConfig);
                    configAcumuladoBL.insert(configAcumulado);

                }
            }
        } catch (Exception e) {
            log.error("[guardarConfigAcumulado] [PROCESO DE CLONACION] [Error al guardar la lista de ConfigAcumulado para la Configuración con ID " + idConfig + " ] " + e.getMessage());
        }
    }

    public void guardarOcc(List<Occ> occList, int idConfig) {
        try {
            OccBUSSI occBl = GuiceInjectorSingleton.getInstance(OccBUSSI.class);
            if (occList != null && idConfig > 0) {
                for (Occ occ : occList) {
                    occ.setConfigId(idConfig);
                    occBl.insert(occ);

                }
            }
        } catch (Exception e) {
            log.error("[guardarConfigAcumulado] [PROCESO DE CLONACION] [Error al guardar la lista de ConfigAcumulado para la Configuración con ID " + idConfig + " ] " + e.getMessage());
        }
    }
}
