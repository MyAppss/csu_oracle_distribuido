package micrium.csu.view;

import com.myapps.controlmenu.bussines.GroupBL;
//ctl+o  para eliminar 

import com.myapps.controlmenu.bussines.RoleBL;
import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.model.GrupoAd;
import com.myapps.controlmenu.util.DescriptorBitacoraCM;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.util.ActiveDirectory;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
@ManagedBean
@RequestScoped

public class RoleGroupForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private GroupBL groupBL = GuiceInjectorSingletonCM.getInstance(GroupBL.class);
    private List<GrupoAd> listGroup;

    private GrupoAd group = new GrupoAd();
    private String groupId;
    private Boolean edit;

    private List<SelectItem> selectItems;
    private String select;
    private static final Logger log = Logger.getLogger(RoleUserForm.class);

    @PostConstruct
    public void init() {

        try {
            group = new GrupoAd();
            listGroup = groupBL.getGroups();
            cargarLista();
            edit = false;
        } catch (Exception e) {
            log.error(e);
        }

    }

    private void cargarLista() {
        RoleBL rolBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
        selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("-1", "Grupos_Rol"));
        List<Rol> listaRol = rolBL.getRoles();
        for (Rol role : listaRol) {
            SelectItem sel = new SelectItem(role.getRolId(), role.getNombre());
            selectItems.add(sel);
        }
    }

    public String saveGroup() {
        int idRole = Integer.parseInt(select);
        if (idRole == -1) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", "Seleccione un Rol"));
            if (edit) {
                int k = Integer.parseInt(groupId);
                group.setGrupoId(k);
            }
            return "";
        }

        String str = groupBL.validate(group, groupId);
        if (str.isEmpty()) {
            str = validarExisteEnAD(group.getNombre());
        }
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", str));

            if (edit) {
                int k = Integer.parseInt(groupId);
                group.setGrupoId(k);
            }
            return "";
        }
        try {

            group.setRolId(idRole);
            if (!edit) {
                groupBL.insertGroup(group);
                ControlerBitacora.insert(DescriptorBitacoraCM.GRUPO, group.getGrupoId() + "", group.getNombre());
            } else {
                int id = Integer.parseInt(groupId);
                group.setGrupoId(id);
                groupBL.updateGroup(group);
                ControlerBitacora.update(DescriptorBitacoraCM.GRUPO, group.getGrupoId() + "", group.getNombre());
            }
            listGroup = groupBL.getGroups();
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                            "Se guardó correctamente"));
            newGroup();
        } catch (Exception e) {
            log.error(e);
        }
        return "";
    }

    public void editRoleGroup() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("groupId");
        int id = Integer.parseInt(Idstr);
        group = groupBL.getGroup(id);
        //select = group.getRol().getRolId() + "";
        select = String.valueOf(group.getRolId());
        groupId = Idstr;
        edit = true;
    }

    public void deleteRoleGroup() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("groupId");
        int id = Integer.parseInt(Idstr);
        try {
            group = groupBL.getGroup(id);
            groupBL.deleteGroup(id);
            ControlerBitacora.delete(DescriptorBitacoraCM.GRUPO, group.getGrupoId() + "", group.getNombre());
            listGroup = groupBL.getGroups();
            newGroup();
        } catch (Exception e) {
            log.error(e);
        }
    }

    public void newGroup() {
        edit = false;
        group = new GrupoAd();
        select = "-1";
        groupId = null;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String GroupId) {
        this.groupId = GroupId;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public GrupoAd getGroup() {
        return group;
    }

    public void setGroup(GrupoAd Group) {
        this.group = Group;
    }

    public List<SelectItem> getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(List<SelectItem> selectItems) {
        this.selectItems = selectItems;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public List<GrupoAd> getListGroup() {
        return listGroup;
    }

    public void setListGroup(List<GrupoAd> listGroup) {
        this.listGroup = listGroup;
    }

    //------------------------------------------------
    private String validarExisteEnAD(String nameNewGrupo) {
        try {
            if (!ActiveDirectory.validarGrupo(nameNewGrupo)) {
                return "No existe un grupo  '" + nameNewGrupo + "' en Active Directory";
            }
        } catch (Exception e) {
            log.error("[validarExisteEnAD] Error al intentar verificar si grupo existe en AD  nombre=" + nameNewGrupo);
            return "No se puede comprobar si este grupo existe en Active Directory";
        }
        return "";
    }
}
