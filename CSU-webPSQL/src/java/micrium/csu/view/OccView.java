/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class OccView implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer configId;
    private String label;
    private Integer origenId;
    private String nombreOrigen;
    private Integer cortoId;
    private String nombreCorto;
    private Integer cosId;
    private String nombreCos;
    private Integer posicion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public Integer getOrigenId() {
        return origenId;
    }

    public void setOrigenId(Integer origenId) {
        this.origenId = origenId;
    }

    public String getNombreOrigen() {
        return nombreOrigen;
    }

    public void setNombreOrigen(String nombreOrigen) {
        this.nombreOrigen = nombreOrigen;
    }

    public Integer getCortoId() {
        return cortoId;
    }

    public void setCortoId(Integer cortoId) {
        this.cortoId = cortoId;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public Integer getCosId() {
        return cosId;
    }

    public void setCosId(Integer cosId) {
        this.cosId = cosId;
    }

    public String getNombreCos() {
        return nombreCos;
    }

    public void setNombreCos(String nombreCos) {
        this.nombreCos = nombreCos;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public OccView clonar() {
        OccView ov = new OccView();
        ov.setId(id);
        ov.setConfigId(configId);
        ov.setLabel(label);
        ov.setOrigenId(origenId);
        ov.setNombreOrigen(nombreOrigen);
        ov.setCortoId(cortoId);
        ov.setNombreCorto(nombreCorto);
        ov.setCosId(cosId);
        ov.setNombreCos(nombreCos);
        ov.setPosicion(posicion);
        return ov;
    }

    @Override
    public String toString() {
        return "OccView{" + "id=" + id + ", configId=" + configId + ", label=" + label + ", origenId=" + origenId + ", nombreOrigen=" + nombreOrigen + ", cortoId=" + cortoId + ", nombreCorto=" + nombreCorto + ", cosId=" + cosId + ", nombreCos=" + nombreCos + ", posicion=" + posicion + '}';
    }
    
    
}
