/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.lang.String;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Parametro;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.ReplaceStrCar_Esp;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
/**
 *
 * @author jose_luis
 */
@ManagedBean
@RequestScoped
public class OptionsPublicidad implements Serializable{
    private static final long serialVersionUID = 1L;
    
    //private Boolean         edit;
    private List<Parametro>    listParametros;    
    //------------------------------------------
    private String publicidad_sw_habilitado;
    
    private static final Logger log = Logger.getLogger(OptionsPublicidad.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private String userId;

    public OptionsPublicidad() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);    
        userId = ControlerBitacora.getNombreUser();
    }
        
    @PostConstruct
    public void init() {
        try { 
       
        ParametroBUSSI parBuss = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class); 
        listParametros =parBuss.obtenerLista();
        cargarParametros();  
        } catch (Exception e) {
            log.error("[init]|user=" + userId + "|Error: "+e.getMessage());
        }
    }
    private void cargarParametros(){
        Map<String,String> MapParam = new  HashMap();
        for (Parametro parametro : listParametros) {            
            MapParam.put(parametro.getNombre(),parametro.getValor());        
        }        
       publicidad_sw_habilitado = MapParam.get("publicidad_sw_habilitado");
    }
    
    public String saveChanges(){
        try {
            String str =null;
            List<Parametro> listParam = new LinkedList<Parametro>();
            Parametro ParamSave = new Parametro();
            ParamSave.setNombre("publicidad_sw_habilitado");
            str = ReplaceStrCar_Esp.replaceBase(publicidad_sw_habilitado);
            ParamSave.setValor(str);
            listParam.add(ParamSave);

            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateListaParametros(listParam);
            //ACTUALIZAR LISTA EN BD
            //ControlerBitacora.update(DescriptorBitacoraLC.SMS,"todos" ,"sms");
            //ControlerBitacora.insertBitacora(DescriptorBitacoraLC.SMS, "Actualizo parametros Canal SMS");
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Informacion", "Cambios guardados correctamente"));
            
        } catch (Exception e) {
              log.error("[saveChanges]|user=" + userId + " intentando guardar : "+e.getMessage());              
        }
       return "";
    }
//    public void updateControl(){
//        
//    }

   
    public String getPublicidad_sw_habilitado() {
        return publicidad_sw_habilitado;
    }

    public void setPublicidad_sw_habilitado(String publicidad_sw_habilitado) {
        this.publicidad_sw_habilitado = publicidad_sw_habilitado;
    }
    

}
