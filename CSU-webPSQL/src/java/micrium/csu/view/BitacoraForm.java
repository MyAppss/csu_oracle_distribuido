package micrium.csu.view;

import com.myapps.controlmenu.bussines.BitacoraBL;
import com.myapps.controlmenu.model.Bitacora;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.annotation.PostConstruct;
import micrium.csu.util.Parameters;

/**
 *
 * @author marciano
 */
@ManagedBean
@RequestScoped

public class BitacoraForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private  BitacoraBL logWebBL = GuiceInjectorSingletonCM.getInstance(BitacoraBL.class);
    private transient List<Bitacora> listBitacora;

    @PostConstruct
    public void init() {

        try {
            listBitacora = logWebBL.getLogWebs();
            if (listBitacora != null && !listBitacora.isEmpty()) {
                for (Bitacora item : listBitacora) {
                    item.setAddress(item.getAddress().replace(".", Parameters.separadorOcteto));
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public List<Bitacora> getListBitacora() {
        return listBitacora;
    }

    public void setListBitacora(List<Bitacora> listBitacora) {
        this.listBitacora = listBitacora;
    }

}
