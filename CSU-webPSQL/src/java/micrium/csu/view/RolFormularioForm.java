package micrium.csu.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.myapps.controlmenu.bussines.RoleBL;
import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.model.RolFormulario;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@ManagedBean
@RequestScoped
public class RolFormularioForm implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String idRol;
    private String nameRole;
    
    private TreeNode root;
    private TreeNode[] selectedNodes;
    
    private static final Logger log = Logger.getLogger(RolFormularioForm.class);
    
    @PostConstruct
    public void init() {
        
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("roleId");
        try {
            idRol = Idstr;
            int id = Integer.parseInt(Idstr);
            cargar(id);
        } catch (NumberFormatException e) {
            log.error("[init] error al cargar" + e);
        }
    }
    
    private Boolean StringToBoolean(String estado) {
        return estado.equals("true") || estado.equals("t");
    }
    
    private void cargar(int id) {
        try {
            RoleBL roleBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
            Rol rol = roleBL.getRole(id);
            nameRole = rol.getNombre();
            
            Map<String, TreeNode> mapTreeNode = new HashMap<>();
            root = new DefaultTreeNode("Root", null);
            List<RolFormulario> lista = roleBL.getListRolFormulario(id);
            List<RolFormulario> listaFormularios = roleBL.getFormularios();
            
            for (RolFormulario listaFormulario : listaFormularios) {
                for (RolFormulario rolFormulario : lista) {
                    if (listaFormulario.getFormularioId().equals(rolFormulario.getFormularioId()) && rolFormulario.getEstado().equals("t") || rolFormulario.getEstado().equals("true")) {
                        listaFormulario.setEstado("true");
                    }
                    
                }
                
            }
            
            for (RolFormulario rolFor : listaFormularios) {
                
                String str = rolFor.getNivel();
                if (str != null) {
                    if (!str.isEmpty()) {
                        int k = str.lastIndexOf(".");
                        if (k == -1) {
                            TreeNode node = new DefaultTreeNode(rolFor.getName(), root);
                            node.setSelected(StringToBoolean(rolFor.getEstado()));
                            mapTreeNode.put(str, node);
                        } else {
                            String path = str.substring(0, k);
                            TreeNode nodeF = mapTreeNode.get(path);
                            TreeNode node = new DefaultTreeNode(rolFor.getName(), nodeF);
                            mapTreeNode.put(str, node);
                            node.setSelected(StringToBoolean(rolFor.getEstado()));
                            nodeF.setSelected(false);
                            nodeF.setExpanded(true);
                        }//*/
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        
    }
    
    public String saveRolFormulario() {
        
        int id = Integer.parseInt(idRol);
        
        if (id == 1) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", "Este Rol no puede ser modificado es el rol de Administracion"));
            return "";
        }
        
        List<String> lis = new ArrayList<>();
        for (TreeNode treeNode : selectedNodes) {
            lis.add(treeNode.getData().toString());
        }
        RoleBL roleBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
        log.info("[saveRolFormulario] lista de selecciones:" + lis + " "
                + "id Role:" + id);
        roleBL.updateRoleFormularioList(lis, id);
        return "/view/Role.xhtml?faces-redirect=true";
    }
    
    public String getIdRol() {
        
        return idRol;
    }
    
    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }
    
    public String getNameRole() {
        return nameRole;
    }
    
    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }
    
    public TreeNode getRoot() {
        return root;
    }

    /**
     * @return the selectedNodes
     */
    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }

    /**
     * @param selectedNodes the selectedNodes to set
     */
    public void setSelectedNodes(TreeNode[] selectedNodes) {
        this.selectedNodes = selectedNodes;
    }
}
