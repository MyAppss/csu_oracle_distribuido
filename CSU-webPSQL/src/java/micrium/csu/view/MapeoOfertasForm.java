/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.MapeoOfertasBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.MapeoOfertas;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
@ManagedBean
@RequestScoped
public class MapeoOfertasForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mapeoId;
    private Boolean edit;
    private MapeoOfertas mapeoOfertas;
    private List<MapeoOfertas> listMapeoOfertas;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(MapeoOfertasForm.class);

    @PostConstruct
    public void init() {
        try {
            mapeoId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_mapeo");
            MapeoOfertasBUSSI bussi = GuiceInjectorSingleton.getInstance(MapeoOfertasBUSSI.class);
            //MapeoOfertasDAOImpl bussi= new MapeoOfertasDAOImpl();
            listMapeoOfertas = bussi.obtenerLista();
            mapeoOfertas = new MapeoOfertas();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveMapeoOfertas() {

        MapeoOfertasBUSSI bussi = GuiceInjectorSingleton.getInstance(MapeoOfertasBUSSI.class);

        mapeoOfertas.setEstado("t");
        String str = bussi.validate(mapeoOfertas, mapeoId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(mapeoOfertas);
                str = ParameterMessage.MapeoOfertas_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.MAPEO_OFERTAS, mapeoOfertas.getId_mapeo() + "", mapeoOfertas.getOfferingname());
            } else {
                int id = Integer.parseInt(mapeoId);
                mapeoOfertas.setId_mapeo(id);
                bussi.update(mapeoOfertas);
                str = ParameterMessage.MapeoOfertas_MESSAGE_MODIFICAR;
                ControlerBitacora.update(DescriptorBitacoraLC.MAPEO_OFERTAS, mapeoOfertas.getId_mapeo() + "", mapeoOfertas.getOfferingname());
            }

            //actualizar variable de app-----------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------

            listMapeoOfertas = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
            newMapeoOfertas();
        } catch (NumberFormatException e) {
            log.error("[saveAcumulador]Error al gurdar ", e);
        }
        return "";
    }

    /*  public void toInt() {

        if (mapeoOfertas != null) {
            if (mapeoOfertas.getLimite() < 0) {
                this.mapeoOfertas.setLimite(this.mapeoOfertas.getLimite() * -1);
            }
        }

    }*/
//------------------------------------------------------------------------------
    public void editMapeoOfertas() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("id_mapeo");

        int id = Integer.parseInt(idStr);
        MapeoOfertasBUSSI bussi = GuiceInjectorSingleton.getInstance(MapeoOfertasBUSSI.class);
        mapeoOfertas = bussi.obtenerId(id);
        mapeoId = idStr;
        //selectedItemConfig= String.valueOf(mapeoOfertas.getConfigId());
        edit = true;
    }

    public void deleteMapeoOfertas() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("id_mapeo");
            int id = Integer.parseInt(idStr);
            MapeoOfertasBUSSI bussi = GuiceInjectorSingleton.getInstance(MapeoOfertasBUSSI.class);
            String str = "";
            mapeoOfertas = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Acumulador_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.MAPEO_OFERTAS, mapeoOfertas.getId_mapeo() + "", mapeoOfertas.getOfferingname());
            listMapeoOfertas = bussi.obtenerLista();
            newMapeoOfertas();
            //actualizar variable de app-----------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion", str));
        } catch (NumberFormatException e) {
            log.error("[deleteAcumulador]Error al eliminar ", e);
        }
    }

    public String validarPaymentMode(MapeoOfertas type) {
        String valor = "";
        if (type.getPaymentmode() != null) {
            switch (type.getPaymentmode()) {
                case 0:
                    valor = "Prepago";
                    break;
                case 1:
                    valor = "PostPago";
                    break;
                case 2:
                    valor = "Hibrida ";
                    break;
                default:
                    valor = "";
                    log.info("No se encontro un Type");
                    break;
            }
        }
        return valor;
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public void newMapeoOfertas() {
        edit = false;
        mapeoOfertas = new MapeoOfertas();
        //cargarItemsConfig();
    }

    public String getMapeoId() {
        return mapeoId;
    }

    public void setMapeoId(String mapeoId) {
        this.mapeoId = mapeoId;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public MapeoOfertas getMapeoOfertas() {
        return mapeoOfertas;
    }

    public void setMapeoOfertas(MapeoOfertas mapeoOfertas) {
        this.mapeoOfertas = mapeoOfertas;
    }

    public List<MapeoOfertas> getListMapeoOfertas() {
        return listMapeoOfertas;
    }

    public void setListMapeoOfertas(List<MapeoOfertas> listMapeoOfertas) {
        this.listMapeoOfertas = listMapeoOfertas;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

}
