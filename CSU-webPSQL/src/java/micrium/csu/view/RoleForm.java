package micrium.csu.view;

import com.myapps.controlmenu.bussines.RoleBL;
import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import com.myapps.controlmenu.util.DescriptorBitacoraCM;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.log4j.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.RolPaginaIniBUSSI;
import micrium.csu.model.RolPaginaIni;
import micrium.csu.mybatis.GuiceInjectorSingleton;

@ManagedBean
@RequestScoped
public class RoleForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Rol> listRole;

    private Rol role = new Rol();
    private String roleId;
    private Boolean edit;

    private static final RoleBL roleBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
    private static final Logger log = Logger.getLogger(RoleForm.class);

    private String rolePageIni;

    @PostConstruct
    public void init() {

        try {
            role = new Rol();
            rolePageIni = "/view/Menu.xhtml";
            listRole = roleBL.getRoles();
            edit = false;
        } catch (Exception e) {
            log.error(e);
        }
    }

    private boolean validar(Rol role) {

        boolean sw = true;

        if (role.getNombre() != null && role.getNombre().length() > 30) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", "El campo Nombre es demasiado largo"));
            sw = false;
        }

        if (role.getDescripcion() != null && role.getDescripcion().length() > 40) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", "El campo Descripcion demasido largo"));
            sw = false;
        }

        return sw;
    }

    public String saveRole() {

        String str = roleBL.validate(role, roleId);
        boolean aux = true;
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", str));
            if (edit) {
                int k = Integer.parseInt(roleId);
                role.setRolId(k);
            }
            aux = false;
        }

        if (validar(role) && aux) {
            try {
                if (!edit) {
                    roleBL.insertRole(role);
                    //--------updatePageInicio
                    RolPaginaIniBUSSI RBL = GuiceInjectorSingleton.getInstance(RolPaginaIniBUSSI.class);
                    RolPaginaIni rpi = new RolPaginaIni();
                    rpi.setPaginaInicio(rolePageIni);
                    rpi.setRolId(role.getRolId());
                    RBL.update(rpi);
                    //-----------------------------------------------------------
                    ControlerBitacora.insert(DescriptorBitacoraCM.ROL, role.getRolId() + "", role.getNombre());
                } else {
                    int id = Integer.parseInt(roleId);
                    role.setRolId(id);
                    roleBL.updateRole(role);
                    //--------updatePageInicio
                    RolPaginaIniBUSSI RBL = GuiceInjectorSingleton.getInstance(RolPaginaIniBUSSI.class);
                    RolPaginaIni rpi = new RolPaginaIni();
                    rpi.setPaginaInicio(rolePageIni);
                    rpi.setRolId(role.getRolId());
                    RBL.update(rpi);
                    //-----------------------------------------------------------
                    ControlerBitacora.update(DescriptorBitacoraCM.ROL, role.getRolId() + "", role.getNombre());
                }
                listRole = roleBL.getRoles();
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "", "Se guardó correctamente"));
                newRole();
            } catch (Exception e) {
                log.error(e);
            }
        }
        return "";
    }

    public void editRole() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("roleId");
        try {
            int id = Integer.parseInt(Idstr);
            role = roleBL.getRole(id);
            roleId = Idstr;
            edit = true;

            RolPaginaIniBUSSI rbl = GuiceInjectorSingleton.getInstance(RolPaginaIniBUSSI.class);
            RolPaginaIni rpi = rbl.obtenerId(id);
            rolePageIni = rpi.getPaginaInicio();
        } catch (NumberFormatException e) {
            log.error(e);
        }

    }

    public String deleteRole() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("roleId");
        int id = Integer.parseInt(Idstr);

        String str = roleBL.validateDelete(id);

        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Mensaje de error:", str));
            return "";
        }

        try {
            role = roleBL.getRole(id);
            roleBL.deleteRole(id);
            ControlerBitacora.delete(DescriptorBitacoraCM.ROL, role.getRolId() + "", role.getNombre());
            listRole = roleBL.getRoles();
            newRole();
        } catch (Exception e) {
            log.error(e);
        }
        return "";
    }

    public void newRole() {
        roleId = null;
        edit = false;
        role = new Rol();
        rolePageIni = "/view/Menu.xhtml";
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String RoleId) {
        this.roleId = RoleId;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Rol getRole() {
        return role;
    }

    public void setRole(Rol Role) {
        this.role = Role;
    }

    public List<Rol> getListRole() {
        return listRole;
    }

    public void setListRole(List<Rol> listRole) {
        this.listRole = listRole;
    }

    public String getRolePageIni() {
        return rolePageIni;
    }

    public void setRolePageIni(String rolePageIni) {
        this.rolePageIni = rolePageIni;
    }

}
