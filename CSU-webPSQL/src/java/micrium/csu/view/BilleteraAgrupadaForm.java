/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.*;

import micrium.csu.model.*;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@SessionScoped
public class BilleteraAgrupadaForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private ComposicionBilletera myWalletComposite;
    private List<ComposicionBilletera> billeteraCompuestaList;
    private String billeteraId;
    private Boolean edit;
    private String idUser;
    private static final ComposicionBilleteraBUSSI composicionBilleteraBL = GuiceInjectorSingleton.getInstance(ComposicionBilleteraBUSSI.class);
    private static final Logger log = Logger.getLogger(BilleteraAgrupadaForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private UnitType UT;
    private List<Billetera> billeteraList;
    private static BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
    private List<SelectItem> selectItemsBillera;
    private String selectedItemBilletera;
    private String rowMaxTable = Parameters.NroFilasDataTable;

    /**
     * Creates a new instance of walletForm
     */
    public BilleteraAgrupadaForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
    }

    // @PostConstruct
    public void iniciarlizar() {
        log.debug("[iniciarlizar]---------------------");
        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("composicionBilleteraId");
            int id = Integer.valueOf(Idstr);
            myWalletComposite = composicionBilleteraBL.obtenerId(id);
            billeteraList = walletBL.obtenerListaByIdCompBilletera(id);
            idUser = ControlerBitacora.getNombreUser();
            edit = false;
            UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            UT = qFlBL.obtenerId(myWalletComposite.getUnitTypeId());
            cargarItemsBilleteras();
            //cargarItemsUnit();
        } catch (Exception e) {
            log.error("[inicializar]User=" + idUser + "|Error al intentar inicializar bean:" + e);
        }
    }

    public void iniciarEdit() {
        log.debug("[iniciarEdit]User=" + idUser + "|Iniciando EDICION de Billetera Agrupada");
        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("composicionBilleteraId");
            int id = Integer.valueOf(Idstr);
            myWalletComposite = composicionBilleteraBL.obtenerId(id);
            billeteraList = walletBL.obtenerLista();
            idUser = ControlerBitacora.getNombreUser();
            edit = true;
        } catch (Exception e) {
            log.error("[iniciarEdit]User=" + idUser + "|: al intentar iniciar Edicion de Billetera Agrupada" + e);
        }
    }

    private void cargarItemsBilleteras() {
        try {
            List<Billetera> list = walletBL.obtenerListaByIdUnitType(UT.getUnitTypeId());
//            List<Billetera> list = walletBL.obtenerListaByIdUnitTypeSinAlco(UT.getUnitTypeId());
            selectedItemBilletera = "-1";
            selectItemsBillera = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Billetera");
            selectItemsBillera.add(s);

            for (Billetera billetera : list) {
                if (!containBilletera(billetera.getNombreComverse(), billeteraList)) {
                    SelectItem sel = new SelectItem(billetera.getBilleteraId(), billetera.getNombreComverse());
                    selectItemsBillera.add(sel);
                }

            }

        } catch (Exception e) {
            log.error("[cargarItemsBilleteras]al intentar cargar Items Billeteras:" + e.getMessage());
        }

    }

    private boolean containBilletera(String nameBilletera, List<Billetera> lista) {
        try {
            for (Billetera billetera : lista) {
                if (billetera.getNombreComverse().equals(nameBilletera)) {
                    return true;
                }
            }
            return false;

        } catch (Exception e) {
            log.error("[containBilleteras]:" + e.getMessage());
            return false;
        }
    }

    public String saveBilleterasParaGrupo() {
        log.debug("[saveBilleterasParaGrupo] Iniciando preceso...");
        //falta validar------
        String str = "";
        if (!str.isEmpty()) {//error
            return "";
        }
        try {
            if (!edit) {//guardar
                for (Billetera billetera : billeteraList) {
                    GrupoBilletera gp = new GrupoBilletera();
                    gp.setBilleteraId(billetera.getBilleteraId());
                    gp.setComposicionBilleteraId(myWalletComposite.getComposicionBilleteraId());
                    gp.setEstado("t");
                    GrupoBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(GrupoBilleteraBUSSI.class);
                    if (!bl.insert(gp)) {
                        log.warn("[saveBilleterasParaGrupo] La billetera " + billetera.getNombreComverse() + " ya existe para esta GRUPO BIlletera");

                    } else {
                        //-------------------------------------------------------------------------------------------------
                        ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                        blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                        ReloadServicios();
                        //-------------------------------------------------------------------------------------------------
                        ControlerBitacora.insert(DescriptorBitacoraLC.GRUPO_BILLETERAS, String.valueOf(gp.getComposicionBilleteraId()), billetera.getNombreComverse());
                        log.info("[saveBilleterasParaGrupo]User=" + idUser + "|registrada GrupoBilletera Idbilletera=" + gp.getBilleteraId() + "|IdComposicion=" + gp.getComposicionBilleteraId());
                    }
                }
                return "/view/listBilleterasAgrupadasForm.xhtml?faces-redirect=true";
                //Guardar lista de billeteras_COS
                //regresar a pantalla de listas
            } else {//guardar Change Edit                
                return "/view/listBilleterasAgrupadasForm.xhtml?faces-redirect=true";
            }
        } catch (Exception e) {
            log.error("[saveBilleterasParaGrupo] Error al intentar guardar cambios:" + e.getMessage());

        }

        return "";
    }

    public void newComposicionBilletera() {
        edit = false;
        myWalletComposite = new ComposicionBilletera();
    }

    public void addBilleteraSimple() {
        try {
            int id = Integer.valueOf(selectedItemBilletera);
            if (id >= 0) {
                Billetera bill = walletBL.obtenerId(id);
                billeteraList.add(bill);
                cargarItemsBilleteras();
            }
        } catch (Exception e) {
            log.error("[addBilleteraSimple]:" + e.getMessage());
        }
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }
//     private String validarAll(){
//        if (myWalletComposite != null) {
////            if (billetera.getNombreComercial().trim().isEmpty()) {
////                return "Campo Nombre Comercial no válido";
////            }
//            if(UT.equals("-1")){
//                return "Debe seleccionar Unit Type";
//            }
//            if(billetera.getPrefijoUnidad().isEmpty()){
//                return "Campo Prefijo no válido";
//            }
//            if(selItemOperador.equals("DIVISION")||selItemOperador.equals("MULTIPLICACION")){
//                if(billetera.getValor()==0){
//                   return "El campo Valor Operación debe ser mayor a cero";
//                }
//            }
//            if(billetera.getMontoMinimo()>=billetera.getMontoMaximo()){
//                return "El campo Monto Máximo debe ser mayor al Monto Mínimo";
//           
//         }
//       }else 
//            return "No se pudo cargar los datos";         
//       return "";
//    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public String getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(String billeteraId) {
        this.billeteraId = billeteraId;
    }

    public ComposicionBilletera getMyWalletComposite() {
        return myWalletComposite;
    }

    public void setMyWalletComposite(ComposicionBilletera myWalletComposite) {
        this.myWalletComposite = myWalletComposite;
    }

    public List<Billetera> getBilleteraList() {
        return billeteraList;
    }

    public void setBilleteraList(List<Billetera> billeteraList) {
        this.billeteraList = billeteraList;
    }

    public UnitType getUT() {
        return UT;
    }

    public void setUT(UnitType UT) {
        this.UT = UT;
    }

    public List<SelectItem> getSelectItemsBillera() {
        return selectItemsBillera;
    }

    public void setSelectItemsBillera(List<SelectItem> selectItemsBillera) {
        this.selectItemsBillera = selectItemsBillera;
    }

    public String getSelectedItemBilletera() {
        return selectedItemBilletera;
    }

    public void setSelectedItemBilletera(String selectedItemBilletera) {
        this.selectedItemBilletera = selectedItemBilletera;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }
}
