/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.CosBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Cos;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
//import micrium.csu.util.DescriptorBitacora;
import org.apache.log4j.Logger;

/**
 *
 * @author jose_luis
 */
@ManagedBean
@RequestScoped
public class CosForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cosId;
    private Boolean edit;
    private Cos cos;
    private List<Cos> listCos;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(CosForm.class);

    @PostConstruct
    public void init() {

        try {
            cosId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cosId");
            CosBUSSI bussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
            listCos = bussi.obtenerLista();
            cos = new Cos();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveCos() {

        CosBUSSI bussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
        //cos.setConfigId(Integer.valueOf(selectedItemConfig));
        cos.setEstado("t");
        cos.setHabilitado("t");
        String str = bussi.validate(cos, cosId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(cos);
                str = ParameterMessage.Cos_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.COS, cos.getCosId() + "", cos.getNombre());
            } else {
                int id = Integer.parseInt(cosId);
                cos.setCosId(id);
                bussi.update(cos);
                str = ParameterMessage.Cos_MESSAGE_MODIFICAR;
                //actualizar variable de app-----------------------------------
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------------------------------------------
                ControlerBitacora.update(DescriptorBitacoraLC.COS, cos.getCosId() + "", cos.getNombre());
            }

            listCos = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
            newCos();
        } catch (Exception e) {
            log.error("[saveCos]Error al gurdar ", e);
        }
        return "";
    }
//------------------------------------------------------------------------------

    public void editCos() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("cosId");

        int id = Integer.parseInt(idStr);
        CosBUSSI bussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
        cos = bussi.obtenerId(id);
        cosId = idStr;
        //selectedItemConfig= String.valueOf(cos.getConfigId());
        edit = true;
    }

    public void deleteCos() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("cosId");
            int id = Integer.parseInt(idStr);
            CosBUSSI bussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
            String str = "";
            cos = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Cos_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.COS, cos.getCosId() + "", cos.getNombre());
            listCos = bussi.obtenerLista();
            newCos();
            //actualizar variable de app-----------------------------------
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
        } catch (Exception e) {
            log.error("[deleteCos]Error al eliminar ", e);
        }
    }
//    private void cargarItemsConfig(){
//       ConfigBUSSI qFlBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
//       List<Config> list = qFlBL.obtenerLista();
//       selectedItemConfig= "-1";
//       selectItemsConfig = new LinkedList<SelectItem>();
//       SelectItem s = new SelectItem("-1", "Seleccionar Configuración");
//       selectItemsConfig.add(s);
//      
//       for (Config conf : list) {
//            SelectItem sel = new SelectItem(conf.getConfigId(),conf.getNombre());
//            selectItemsConfig.add(sel);
//        }
//    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public void newCos() {
        edit = false;
        cos = new Cos();
        //cargarItemsConfig();
    }

    /**
     * @return the cosId
     */
    public String getCosId() {
        return cosId;
    }

    /**
     * @param cosId the cosId to set
     */
    public void setCosId(String cosId) {
        this.cosId = cosId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the cos
     */
    public Cos getCos() {
        return cos;
    }

    /**
     * @param cos the cos to set
     */
    public void setCos(Cos cos) {
        this.cos = cos;
    }

    /**
     * @return the listCos
     */
    public List<Cos> getListCos() {
        return listCos;
    }

    /**
     * @param listCos the listCos to set
     */
    public void setListCos(List<Cos> listCos) {
        this.listCos = listCos;
    }

//    public List<SelectItem> getSelectItemsConfig() {
//        return selectItemsConfig;
//    }
//
//    public void setSelectItemsConfig(List<SelectItem> selectItemsConfig) {
//        this.selectItemsConfig = selectItemsConfig;
//    }
//
//    public String getSelectedItemConfig() {
//        return selectedItemConfig;
//    }
//
//    public void setSelectedItemConfig(String selectedItemConfig) {
//        this.selectedItemConfig = selectedItemConfig;
//    }
    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }
}
