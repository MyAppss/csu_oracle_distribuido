/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.ComposicionBilleteraBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
//import micrium.csu.bussines.CosBUSSI;
import micrium.csu.bussines.UnitTypeBUSSI;

import micrium.csu.model.ComposicionBilletera;
import micrium.csu.model.UnitType;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@ViewScoped
public class listBilleteraAgrupadaForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idUser;

    private static final Logger log = Logger.getLogger(listBilleteraAgrupadaForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private static final ComposicionBilleteraBUSSI compositionWalletBL = GuiceInjectorSingleton.getInstance(ComposicionBilleteraBUSSI.class);
    private List<ComposicionBilletera> billeterasAgrupadasList;
    private ComposicionBilletera myCompoBilletera;
    private Boolean edit;
    private String composicionBilleteraId;
    private List<SelectItem> selectItemsUnitType;
    private String selItemUnitType;
    private String nameUnitType;
    private String textoInfo;
    private String selItemOperador;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    
    //private List<> listCOS;

    /**
     * Creates a new instance of walletForm
     */
    public listBilleteraAgrupadaForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
    }

    @PostConstruct
    public void init() {

        try {
            System.out.print("Init");
            idUser = ControlerBitacora.getNombreUser();
//             myCompoBilletera = new ComposicionBilletera();            
//            cargarListBilleterasAgrupadas();
            newComposicionBilletera();
        } catch (Exception e) {
            log.error("[init]:" + e);
        }
    }

    public void editCompoBilletera() {

        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("composicionBilleteraId");
            int id = Integer.parseInt(Idstr);
            composicionBilleteraId = Idstr;
            myCompoBilletera = compositionWalletBL.obtenerId(id);
            selItemUnitType = String.valueOf(myCompoBilletera.getUnitTypeId());
            selItemOperador = myCompoBilletera.getOperador();

            UnitTypeBUSSI ubl = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            UnitType ut = ubl.obtenerId(myCompoBilletera.getUnitTypeId());
            nameUnitType = ut.getNombre();
            edit = true;
            textoInfo = "EDITANDO GRUPO";
        } catch (Exception e) {
            log.error("[editWallet]|user=" + idUser + "|Error al intentar activar edicion Composicion Billetera:" + e.getMessage());
        }
    }

    public String saveBilleteraAgrupada() {
        System.out.print("saveBilleteraAgrupada");
        log.info("[saveBilleteraAgrupada]|Iniciando Guardar billetera Agrupada");
        myCompoBilletera.setEstado("t");
        if (edit) {
            myCompoBilletera.setUnitTypeId(Integer.valueOf(selItemUnitType));
        }
        myCompoBilletera.setOperador(selItemOperador);

        String str = validarAll(myCompoBilletera);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error:", str));
            return "";
        }
        try {
            if (!edit) {
                if (compositionWalletBL.insert(myCompoBilletera)) {
                    ControlerBitacora.insert(DescriptorBitacoraLC.GRUPO, "" + myCompoBilletera.getComposicionBilleteraId(), myCompoBilletera.getNombre());
                    FacesContext.getCurrentInstance().addMessage(
                            null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "", "Cambios guardados correctamente"));
                }
            } else {
                int id = Integer.parseInt(composicionBilleteraId);
                myCompoBilletera.setComposicionBilleteraId(id);
                if (compositionWalletBL.update(myCompoBilletera)) {
                    str = ParameterMessage.GrupoBilletera_MESSAGE_MODIFICAR;
                    ControlerBitacora.update(DescriptorBitacoraLC.GRUPO, "" + id, myCompoBilletera.getNombre());
                    FacesContext.getCurrentInstance().addMessage(
                            null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                    "", str));
                }
            }
            //actualizar variable de app
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            
            newComposicionBilletera();
        } catch (NumberFormatException e) {
            log.error("[saveBanco]|user=" + idUser + "|Error al guardar " + e);
        }
        return "";
    }

    private String validarAll(ComposicionBilletera myCompBillet) {
        try {
            if (myCompBillet.getNombre().trim().isEmpty()) {
                return "El campo Nombre esta vacío";
            }
            if (compositionWalletBL.existe(idUser)) {
                return "Ya existe una Billetera agrupada con este nombre";
            }
            if (myCompBillet.getNombreComercial().trim().isEmpty()) {
                return "El campo Nombre Comercial esta vacío";
            }
            if (myCompBillet.getUnitTypeId() <= 0) {
                return "Debe seleccionar Unit Type";
            }
            if (myCompBillet.getPrefijoUnidad().trim().isEmpty()) {
                return "El campo Prefijo Unidad esta vacío";
            }
            //log.debug("------"+);
            if (selItemOperador.equals("DIVISION") || selItemOperador.equals("MULTIPLICACION")) {
                if (myCompBillet.getValor() == 0) {
                    return "El campo Valor Operación debe ser mayor a cero";
                }
            }
            if (myCompBillet.getMontoMinimo() >= myCompBillet.getMontoMaximo()) {
                return "El campo Monto Máximo debe ser mayor al Monto Mínimo";
            }

            log.debug("[validarAll]|user=" + idUser + "|Validacion realizada correctamente");
        } catch (Exception e) {
            log.error("[validarAll]|user=" + idUser + "|Error intentar validar" + e.getMessage());
            return "Desconocido";
        }

        return "";
    }

    void cargarListBilleterasAgrupadas() {
        billeterasAgrupadasList = compositionWalletBL.obtenerLista();
    }

    public void deleteBilleteraComp() {
        try {
            log.debug("[deleteBilleteraComp]UserId=" + idUser + "|iniciando..");
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("composicionBilleteraId");
            int id = Integer.parseInt(Idstr);
            ComposicionBilletera cp = compositionWalletBL.obtenerId(id);

            log.info("[deleteBilleteraComp]UserId=" + idUser + "|GrupoBilletera=" + id + " iniciando proceso para eliminar");
            if (!compositionWalletBL.delete(id)) {
                log.warn("[deleteBilleteraComp]UserId=" + idUser + "||GrupoBilletera=" + id + "]No se pudo eliminar correctamente composicion billetera");

            } else {
                //actualizar variable de app-----------------------------------
                ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();

                log.info("[deleteBilleteraComp]UserId=" + idUser + "|GrupoBilletera=" + id + "|Se elimino correctamente");
                ControlerBitacora.delete(DescriptorBitacoraLC.GRUPO, "" + Idstr, cp.getNombre());
                newComposicionBilletera();

            }
        } catch (Exception e) {
            log.error("[deleteBilleteraComp]|user=" + idUser + "|Error al intentar eliminar Grupo billetera:" + e.getMessage());
        }
    }

    public void newComposicionBilletera() {
        textoInfo = "NUEVO GRUPO";
        edit = false;
        myCompoBilletera = new ComposicionBilletera();
        myCompoBilletera.setMontoMaximo(99999.00);
        myCompoBilletera.setMontoMinimo(0.0);
        selItemOperador = "NINGUNO";
        selItemUnitType = "-1";
        nameUnitType = "";
        cargarListBilleterasAgrupadas();
        cargarItemsUnit();
    }

    private void cargarItemsUnit() {
        try {
            UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            List<UnitType> list = qFlBL.obtenerLista();
            selItemUnitType = "-1";
            selectItemsUnitType = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
            selectItemsUnitType.add(s);
            if (list != null) {
                for (UnitType unitType : list) {
                    SelectItem sel = new SelectItem(unitType.getUnitTypeId(), unitType.getNombre());
                    selectItemsUnitType.add(sel);
                }
            }
        } catch (Exception e) {
            log.error("[cargarItemsUnit]Al intentar cargar lista de UnitTypes:" + e.getMessage(), e);
        }

    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

//  private void cargarItemsCOS(){
//        UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
//        List<UnitType> list = qFlBL.obtenerLista();
//        selItemUnitType= "-1";
//        selectItemsUnitType = new LinkedList<SelectItem>();
//        SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
//        selectItemsUnitType.add(s);
//      
//       for (UnitType unitType : list) {
//            SelectItem sel = new SelectItem(unitType.getUnitTypeId(),unitType.getNombre());
//            selectItemsUnitType.add(sel);
//        }
//    }
//    
    public List<ComposicionBilletera> getBilleterasAgrupadasList() {
        return billeterasAgrupadasList;
    }

    public void setBilleterasAgrupadasList(List<ComposicionBilletera> billeterasAgrupadasList) {
        this.billeterasAgrupadasList = billeterasAgrupadasList;
    }

    public String getSelItemUnitType() {
        return selItemUnitType;
    }

    public void setSelItemUnitType(String selItemUnitType) {
        this.selItemUnitType = selItemUnitType;
    }

    public List<SelectItem> getSelectItemsUnitType() {
        return selectItemsUnitType;
    }

    public void setSelectItemsUnitType(List<SelectItem> selectItemsUnitType) {
        this.selectItemsUnitType = selectItemsUnitType;
    }

    public ComposicionBilletera getMyCompoBilletera() {
        return myCompoBilletera;
    }

    public void setMyCompoBilletera(ComposicionBilletera myCompoBilletera) {
        this.myCompoBilletera = myCompoBilletera;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public String getTextoInfo() {
        return textoInfo;
    }

    public void setTextoInfo(String textoInfo) {
        this.textoInfo = textoInfo;
    }

    public String getSelItemOperador() {
        return selItemOperador;
    }

    public void setSelItemOperador(String selItemOperador) {
        this.selItemOperador = selItemOperador;
    }

    public String getNameUnitType() {
        return nameUnitType;
    }

    public void setNameUnitType(String nameUnitType) {
        this.nameUnitType = nameUnitType;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public String getComposicionBilleteraId() {
        return composicionBilleteraId;
    }

    public void setComposicionBilleteraId(String composicionBilleteraId) {
        this.composicionBilleteraId = composicionBilleteraId;
    }

}
