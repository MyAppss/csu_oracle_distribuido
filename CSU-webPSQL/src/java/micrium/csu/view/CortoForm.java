/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.CortoBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Corto;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
@ManagedBean
@RequestScoped
public class CortoForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cortoId;
    private Boolean edit;
    private Corto corto;
    private List<Corto> listCorto;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(CortoForm.class);

    @PostConstruct
    public void init() {

        try {
            cortoId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cortoId");
            CortoBUSSI bussi = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
            listCorto = bussi.obtenerLista();
            corto = new Corto();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveCorto() {

        CortoBUSSI bussi = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
        //corto.setConfigId(Integer.valueOf(selectedItemConfig));
        corto.setEstado("t");
        String str = bussi.validate(corto, cortoId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(corto);
                str = ParameterMessage.Corto_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.CORTO, corto.getCortoId() + "", corto.getNombre());
            } else {
                int id = Integer.parseInt(cortoId);
                corto.setCortoId(id);
                bussi.update(corto);
                str = ParameterMessage.Corto_MESSAGE_MODIFICAR;
                //actualizar variable de app-----------------------------------
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------------------------------------------
                //-------------------------------------------------------------
                ControlerBitacora.update(DescriptorBitacoraLC.CORTO, corto.getCortoId() + "", corto.getNombre());
            }

            listCorto = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
            newCorto();
        } catch (Exception e) {
            log.error("[saveCorto]Error al gurdar ", e);
        }
        return "";
    }
//------------------------------------------------------------------------------

    public void editCorto() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("cortoId");

        int id = Integer.parseInt(idStr);
        CortoBUSSI bussi = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
        corto = bussi.obtenerId(id);
        cortoId = idStr;
        //selectedItemConfig= String.valueOf(corto.getConfigId());
        edit = true;
    }

    public void deleteCorto() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("cortoId");
            int id = Integer.parseInt(idStr);
            CortoBUSSI bussi = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
            String str = "";
            corto = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Corto_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.CORTO, corto.getCortoId() + "", corto.getNombre());
            listCorto = bussi.obtenerLista();
            newCorto();
            //actualizar variable de app-----------------------------------
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
        } catch (Exception e) {
            log.error("[deleteCorto]Error al eliminar ", e);
        }
    }
//    private void cargarItemsConfig(){
//       ConfigBUSSI qFlBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
//       List<Config> list = qFlBL.obtenerLista();
//       selectedItemConfig= "-1";
//       selectItemsConfig = new LinkedList<SelectItem>();
//       SelectItem s = new SelectItem("-1", "Seleccionar Configuración");
//       selectItemsConfig.add(s);
//      
//       for (Config conf : list) {
//            SelectItem sel = new SelectItem(conf.getConfigId(),conf.getNombre());
//            selectItemsConfig.add(sel);
//        }
//    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public void newCorto() {
        edit = false;
        corto = new Corto();
        //cargarItemsConfig();
    }

    /**
     * @return the cortoId
     */
    public String getCortoId() {
        return cortoId;
    }

    /**
     * @param cortoId the cortoId to set
     */
    public void setCortoId(String cortoId) {
        this.cortoId = cortoId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the corto
     */
    public Corto getCorto() {
        return corto;
    }

    /**
     * @param corto the corto to set
     */
    public void setCorto(Corto corto) {
        this.corto = corto;
    }

    /**
     * @return the listCorto
     */
    public List<Corto> getListCorto() {
        return listCorto;
    }

    /**
     * @param listCorto the listCorto to set
     */
    public void setListCorto(List<Corto> listCorto) {
        this.listCorto = listCorto;
    }

//    public List<SelectItem> getSelectItemsConfig() {
//        return selectItemsConfig;
//    }
//
//    public void setSelectItemsConfig(List<SelectItem> selectItemsConfig) {
//        this.selectItemsConfig = selectItemsConfig;
//    }
//
//    public String getSelectedItemConfig() {
//        return selectedItemConfig;
//    }
//
//    public void setSelectedItemConfig(String selectedItemConfig) {
//        this.selectedItemConfig = selectedItemConfig;
//    }
    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }
}
