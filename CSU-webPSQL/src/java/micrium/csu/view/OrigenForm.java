/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.OrigenBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Origen;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
@ManagedBean
@RequestScoped
public class OrigenForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String origenId;
    private Boolean edit;
    private Origen origen;
    private List<Origen> listOrigen;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(OrigenForm.class);

    @PostConstruct
    public void init() {

        try {
            origenId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("origenId");
            OrigenBUSSI bussi = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
            listOrigen = bussi.obtenerLista();
            origen = new Origen();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveOrigen() {

        OrigenBUSSI bussi = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
        //origen.setConfigId(Integer.valueOf(selectedItemConfig));
        origen.setEstado("t");
        String str = bussi.validate(origen, origenId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(origen);
                str = ParameterMessage.Origen_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.ORIGEN, origen.getOrigenId() + "", origen.getNombre());
            } else {
                int id = Integer.parseInt(origenId);
                origen.setOrigenId(id);
                bussi.update(origen);
                str = ParameterMessage.Origen_MESSAGE_MODIFICAR;
                //actualizar variable de app-----------------------------------
                ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------
                ControlerBitacora.update(DescriptorBitacoraLC.ORIGEN, origen.getOrigenId() + "", origen.getNombre());
            }

            listOrigen = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
            newOrigen();
        } catch (Exception e) {
            log.error("[saveOrigen]Error al gurdar ", e);
        }
        return "";
    }
//------------------------------------------------------------------------------

    public void editOrigen() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("origenId");

        int id = Integer.parseInt(idStr);
        OrigenBUSSI bussi = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
        origen = bussi.obtenerId(id);
        origenId = idStr;
        //selectedItemConfig= String.valueOf(origen.getConfigId());
        edit = true;
    }

    public void deleteOrigen() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("origenId");
            int id = Integer.parseInt(idStr);
            OrigenBUSSI bussi = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
            String str = "";
            origen = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Origen_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.ORIGEN, origen.getOrigenId() + "", origen.getNombre());
            listOrigen = bussi.obtenerLista();
            newOrigen();
            //actualizar variable de app-----------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
        } catch (Exception e) {
            log.error("[deleteOrigen]Error al eliminar ", e);
        }
    }
//    private void cargarItemsConfig(){
//       ConfigBUSSI qFlBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
//       List<Config> list = qFlBL.obtenerLista();
//       selectedItemConfig= "-1";
//       selectItemsConfig = new LinkedList<SelectItem>();
//       SelectItem s = new SelectItem("-1", "Seleccionar Configuración");
//       selectItemsConfig.add(s);
//      
//       for (Config conf : list) {
//            SelectItem sel = new SelectItem(conf.getConfigId(),conf.getNombre());
//            selectItemsConfig.add(sel);
//        }
//    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public void newOrigen() {
        edit = false;
        origen = new Origen();
        //cargarItemsConfig();
    }

    /**
     * @return the origenId
     */
    public String getOrigenId() {
        return origenId;
    }

    /**
     * @param origenId the origenId to set
     */
    public void setOrigenId(String origenId) {
        this.origenId = origenId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the origen
     */
    public Origen getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(Origen origen) {
        this.origen = origen;
    }

    /**
     * @return the listOrigen
     */
    public List<Origen> getListOrigen() {
        return listOrigen;
    }

    /**
     * @param listOrigen the listOrigen to set
     */
    public void setListOrigen(List<Origen> listOrigen) {
        this.listOrigen = listOrigen;
    }

//    public List<SelectItem> getSelectItemsConfig() {
//        return selectItemsConfig;
//    }
//
//    public void setSelectItemsConfig(List<SelectItem> selectItemsConfig) {
//        this.selectItemsConfig = selectItemsConfig;
//    }
//
//    public String getSelectedItemConfig() {
//        return selectedItemConfig;
//    }
//
//    public void setSelectedItemConfig(String selectedItemConfig) {
//        this.selectedItemConfig = selectedItemConfig;
//    }
    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }
}
