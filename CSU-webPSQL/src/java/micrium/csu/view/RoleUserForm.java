package micrium.csu.view;

import com.myapps.controlmenu.bussines.RoleBL;
import com.myapps.controlmenu.bussines.UserBL;
import com.myapps.controlmenu.model.Rol;
import com.myapps.controlmenu.model.Usuario;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import com.myapps.controlmenu.util.DescriptorBitacoraCM;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.util.ActiveDirectory;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;

/**
 *
 * @author marciano
 */
@ManagedBean
@RequestScoped
public class RoleUserForm implements Serializable {

    private static final long serialVersionUID = 1L;
    private  UserBL userBL = GuiceInjectorSingletonCM.getInstance(UserBL.class);
    private List<Usuario> listUser;
    private Usuario user = new Usuario();
    private String userId;
    private Boolean edit;
    private List<SelectItem> selectItems;
    private String select;
    private static final Logger log = Logger.getLogger(RoleUserForm.class);

    @PostConstruct
    public void init() {

        try {
            user = new Usuario();
            listUser = userBL.getUsers();
            cargarLista();
            edit =false;
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void cargarLista() {
        RoleBL rolBL = GuiceInjectorSingletonCM.getInstance(RoleBL.class);
        selectItems = new ArrayList<SelectItem>();
        selectItems.add(new SelectItem("-1", "Grupos_Rol"));
        List<Rol> listaRol = rolBL.getRoles();
        for (Rol role : listaRol) {
            SelectItem sel = new SelectItem(role.getRolId(), role.getNombre());
            selectItems.add(sel);
        }
    }

    public String saveUser() {
        int idRole = Integer.parseInt(select);
        if (idRole == -1) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Mensaje de error:", "Seleccione un Rol"));
            if (edit) {
                int k = Integer.parseInt(userId);
                user.setUsuarioId(k);
            }
            return "";
        }
        //----------------------------------------------------------------------
        //recuperar desde bd
        String name = ActiveDirectory.obtenerNombreCompletoActiveDirectory(user.getLogin());

        log.debug(" nameAD=" + name);
        if (!name.isEmpty()) {
            user.setNombre(name);
        }
        //----------------------------------------------------------------------

        String str = userBL.validate(user, userId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Mensaje de error:", str));
            if (edit) {
                int k = Integer.parseInt(userId);
                user.setUsuarioId(k);
            }
            return "";
        }

        if (Parameters.swActive.equals("true")) {
            List<?> groups = ActiveDirectory.getGroups(user.getLogin());
            if (groups == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Mensaje de error:", "Este usuario no existe en AD."));
                if (edit) {
                    int k = Integer.parseInt(userId);
                    user.setUsuarioId(k);
                }
                return "";
            }

            if (groups.size() <= 0) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Mensaje de error:", "Este usuario no tiene grupo en AD."));
                if (edit) {
                    int k = Integer.parseInt(userId);
                    user.setUsuarioId(k);
                }
                return "";
            }
        }

        try {
            user.setRolId(idRole);

            if (!edit) {

                userBL.insertUser(user);
                ControlerBitacora.insert(DescriptorBitacoraCM.USUARIO, user.getUsuarioId() + "", user.getNombre());
            } else {
                int id = Integer.parseInt(userId);
                user.setUsuarioId(id);
                userBL.updateUser(user);
                ControlerBitacora.update(DescriptorBitacoraCM.USUARIO, user.getUsuarioId() + "", user.getNombre());
            }
            listUser = userBL.getUsers();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "", "Se guardó correctamente"));
            newUser();
        } catch (Exception e) {
            log.error(e);
        }
        return "";
    }

    public void editRoleUser() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("user_id");
        int id = Integer.parseInt(Idstr);
        user = userBL.getUser(id);
        userId = Idstr;
        select = String.valueOf(user.getRolId());
        //int idRole=Integer.parseInt(select);            

        edit = true;
    }

    public String deleteRoleUser() {
        String Idstr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("user_id");
        int id = Integer.parseInt(Idstr);

        if (id == 1) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Mensaje de error:",
                    "El Usuario no se puede eliminar es de Administracion"));
            return "";
        }

        try {
            user = userBL.getUser(id);
            userBL.deleteUser(id);
            ControlerBitacora.delete(DescriptorBitacoraCM.USUARIO, user.getUsuarioId() + "", user.getNombre());
            listUser = userBL.getUsers();
            newUser();
        } catch (Exception e) {
            log.error(e);
        }
        return "";
    }

    public void newUser() {
        edit = false;
        user = new Usuario();
        select = "-1";
        userId = null;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public List<SelectItem> getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(List<SelectItem> selectItems) {
        this.selectItems = selectItems;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public List<Usuario> getListUser() {
        return listUser;
    }

    public void setListUser(List<Usuario> listUser) {
        this.listUser = listUser;
    }
}
