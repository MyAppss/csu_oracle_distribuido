/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.*;
import micrium.csu.model.*;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;
import javax.faces.event.ActionEvent;
import micrium.csu.util.RecargarServicios;

/**
 *
 * @author micrium
 */
@ManagedBean
@SessionScoped
public class ConfigAcumuladoForm implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<ConfigBilleteraAcumulado> billeteraAcumuladoList;
    private Config config;
    private static final ConfigAcumuladoBUSSI configAcumuladoBUSSI = GuiceInjectorSingleton.getInstance(ConfigAcumuladoBUSSI.class);
    private static final Logger log = Logger.getLogger(ConfigAcumuladoForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private List<ConfigBilleteraAcumulado> billeteraAcumuladoListFilter;

    /**
     * Creates a new instance of walletForm
     */
    //@PostConstruct
    public void iniciarConfiguracion() {
        log.debug("iniciar---------------------");
        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");
            billeteraAcumuladoList = configAcumuladoBUSSI.listByIdConfigBilletera(Integer.parseInt(Idstr));
            config = obtenerConfiguracion(Integer.parseInt(Idstr));
        } catch (Exception e) {
            log.error("[iniciar]:" + e);
        }
    }

    private Config obtenerConfiguracion(Integer idConfi) {
        Config confi;
        ConfigBUSSI configBussi = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
        confi = configBussi.obtenerId(idConfi);
        if (confi != null) {
            return confi;
        }
        return null;
    }
    private String rowMaxTable = Parameters.NroFilasDataTable;

    public void saveConfig(ActionEvent actionEvent) throws IOException {
        log.info("guardando config acumulados...");
        try {
            boolean save = configAcumuladoBUSSI.saveListConfigBilleteraAcumulado(billeteraAcumuladoList, config.getConfigId());
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------
            log.info("guardado config acumulados: " + save);
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            FacesContext.getCurrentInstance().getExternalContext().redirect(context.getRequestContextPath() + "/view/listConfigForm.xhtml");

            cleanValues();

        } catch (Exception e) {
            log.info("No se puede guardar los cambios.", e);
            error("No se puede guardar los cambios.");
        }
    }

    public void error(String msg) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", msg));
    }

    public List<ConfigBilleteraAcumulado> getBilleteraAcumuladoList() {
        return billeteraAcumuladoList;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public void setBilleteraAcumuladoList(List<ConfigBilleteraAcumulado> billeteraAcumuladoList) {
        this.billeteraAcumuladoList = billeteraAcumuladoList;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public List<ConfigBilleteraAcumulado> getBilleteraAcumuladoListFilter() {
        return billeteraAcumuladoListFilter;
    }

    public void setBilleteraAcumuladoListFilter(List<ConfigBilleteraAcumulado> billeteraAcumuladoListFilter) {
        this.billeteraAcumuladoListFilter = billeteraAcumuladoListFilter;
    }

    public String cleanValues() {
        billeteraAcumuladoList = null;
        config = null;
        billeteraAcumuladoListFilter = null;
        return "/view/listConfigForm.xhtml?faces-redirect=true";
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

}
