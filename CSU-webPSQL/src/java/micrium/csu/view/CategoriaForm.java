///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package micrium.csu.view;
//
//import java.io.Serializable;
//import java.util.List;
//import javax.annotation.PostConstruct;
//import javax.faces.application.FacesMessage;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ViewScoped;
//import javax.faces.context.FacesContext;
//import micrium.csu.bussines.CategoriaBUSSI;
//import micrium.csu.bussines.ParametroBUSSI;
//import micrium.csu.model.Categoria;
//import micrium.csu.mybatis.GuiceInjectorSingleton;
//import micrium.csu.util.DescriptorBitacoraLC;
//import micrium.csu.util.ParameterMessage;
//import micrium.csu.util.Parameters;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author Leandro
// */
//@ManagedBean
//@ViewScoped
//public class CategoriaForm implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    private String categoriaId;
//    private Boolean edit;
//    private Categoria categoria;
//    private List<Categoria> listCategoria;
//    private String userId;
//    private String rowMaxTable = Parameters.NroFilasDataTable;
//
//    private static final Logger log = Logger.getLogger(CategoriaForm.class);
//
//    @PostConstruct
//    public void init() {
//        try {
//            categoriaId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("categoriaId");
//            CategoriaBUSSI bussi = GuiceInjectorSingleton.getInstance(CategoriaBUSSI.class);
//            listCategoria = bussi.obtenerLista();
//            categoria = new Categoria();
//            userId = ControlerBitacora.getNombreUser();
//            edit = false;
//        } catch (Exception e) {
//            log.error("[init] Error: ", e);
//        }
//    }
//
//    public String saveCategoria() {
//        log.info("[saveCategoria]userId=" + userId + "|Iniciando proceso...");
//        try {
//            CategoriaBUSSI bussi = GuiceInjectorSingleton.getInstance(CategoriaBUSSI.class);
//            categoria.setEstado("t");
//
//            //log.debug("---------------------------------------------------1");
//            String str = bussi.validate(categoria, categoriaId);
//            //log.debug("---------------------------------------------------2");
//            if (!str.isEmpty()) {
//                FacesContext.getCurrentInstance().addMessage(
//                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//                                "", str));
//                return "";
//            }
//            //log.debug("---------------------------------------------------3");
//            if (!edit) {
//                if (bussi.insert(categoria)) {
//                    log.info("[saveCategoria]userId=" + userId + "|Inserto correctamente|Categoria=" + categoria.getCategoriaId());
//                    str = ParameterMessage.Categoria_MESSAGE_GUARDAR + categoria.getCategoriaId();
//                    ControlerBitacora.insert(DescriptorBitacoraLC.CATEGORIA, categoria.getCategoriaId() + "", categoria.getNombre());
//                } else {
//                    log.warn("[saveCategoria]userId=" + userId + "|No inserto|");
//                }
//            } else {
//                int id = Integer.parseInt(categoriaId);
//                categoria.setCategoriaId(id);
//                bussi.update(categoria);
//                str = ParameterMessage.Categoria_MESSAGE_MODIFICAR + categoriaId;
//                ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
//                bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
//                ControlerBitacora.update(DescriptorBitacoraLC.CATEGORIA, categoria.getCategoriaId() + "", categoria.getNombre());
//            }
//            listCategoria = bussi.obtenerLista();
//            FacesContext.getCurrentInstance().addMessage(
//                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
//                            "", str));
//            log.info("[saveCategoria]userId=" + userId + "|Finalizando proceso correctamente");
//            newCategoria();
//        } catch (Exception e) {
//            log.error("[saveCategoria]userId=" + userId + "|Error al guardar. Error:" + e.getMessage());
//        }
//        return "";
//    }
//
//    public void editCategoria() {
//        String idStr = FacesContext.getCurrentInstance().getExternalContext()
//                .getRequestParameterMap().get("categoriaId");
//
//        int id = Integer.parseInt(idStr);
//        CategoriaBUSSI bussi = GuiceInjectorSingleton.getInstance(CategoriaBUSSI.class);
//        categoria = bussi.obtenerId(id);
//
//        categoriaId = idStr;
//        edit = true;
//    }
//
//    public void deleteCategoria() {
//
//        try {
//            String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("categoriaId");
//            int id = Integer.parseInt(idStr);
//            CategoriaBUSSI bussi = GuiceInjectorSingleton.getInstance(CategoriaBUSSI.class);
//            String str = "";
//            categoria = bussi.obtenerId(id);
//            bussi.delete(id);
//            str = ParameterMessage.Categoria_MESSAGE_ELIMINAR + categoria.getCategoriaId();
//            // ControlerBitacora.delete(DescriptorBitacora.UNITTYPE, unittype.getUnitTypeId()+"",unittype.getNombre());
//            ControlerBitacora.insert(DescriptorBitacoraLC.CATEGORIA, categoriaId + "", categoria.getNombre());
//            listCategoria = bussi.obtenerLista();
//            newCategoria();
//            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
//            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
//            FacesContext.getCurrentInstance().addMessage(
//                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
//                            "", str));
//        } catch (Exception e) {
//            log.error("[deleteCategoria]Error al eliminar ", e);
//        }
//    }
//
//    public void newCategoria() {
//        edit = false;
//        categoria = new Categoria();
//    }
//
//    public String getCategoriaId() {
//        return categoriaId;
//    }
//
//    public void setCategoriaId(String categoriaId) {
//        this.categoriaId = categoriaId;
//    }
//
//    /**
//     * @return the edit
//     */
//    public Boolean getEdit() {
//        return edit;
//    }
//
//    /**
//     * @param edit the edit to set
//     */
//    public void setEdit(Boolean edit) {
//        this.edit = edit;
//    }
//
//    public Categoria getCategoria() {
//        return categoria;
//    }
//
//    public void setCategoria(Categoria categoria) {
//        this.categoria = categoria;
//    }
//
//    public List<Categoria> getListCategoria() {
//        return listCategoria;
//    }
//
//    public void setListCategoria(List<Categoria> listCategoria) {
//        this.listCategoria = listCategoria;
//    }
//
//    public String getRowMaxTable() {
//        return rowMaxTable;
//    }
//
//    public void setRowMaxTable(String rowMaxTable) {
//        this.rowMaxTable = rowMaxTable;
//    }
//
//}
