/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.AcumuladorBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Acumulador;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
@ManagedBean
@RequestScoped
public class AcumuladorForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String acumuladorId;
    private Boolean edit;
    private Acumulador acumulador;
    private List<Acumulador> listAcumulador;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(AcumuladorForm.class);

    @PostConstruct
    public void init() {

        try {
            acumuladorId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("acumuladorId");
            AcumuladorBUSSI bussi = GuiceInjectorSingleton.getInstance(AcumuladorBUSSI.class);
            listAcumulador = bussi.obtenerLista();
            acumulador = new Acumulador();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveAcumulador() {

        AcumuladorBUSSI bussi = GuiceInjectorSingleton.getInstance(AcumuladorBUSSI.class);
        //acumulador.setConfigId(Integer.valueOf(selectedItemConfig));
        acumulador.setEstado("t");
        String str = bussi.validate(acumulador, acumuladorId);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(acumulador);
                str = ParameterMessage.Acumulador_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.ACUMULADOR, acumulador.getAcumuladorId() + "", acumulador.getNombre());
            } else {
                int id = Integer.parseInt(acumuladorId);
                acumulador.setAcumuladorId(id);
                bussi.update(acumulador);
                str = ParameterMessage.Acumulador_MESSAGE_MODIFICAR;
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------------------------------------------
                ControlerBitacora.update(DescriptorBitacoraLC.ACUMULADOR, acumulador.getAcumuladorId() + "", acumulador.getNombre());
            }

            listAcumulador = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
            newAcumulador();
        } catch (Exception e) {
            log.error("[saveAcumulador]Error al gurdar ", e);
        }
        return "";
    }

    public void toInt() {

        if (acumulador != null) {
            if (acumulador.getLimite() < 0) {
                this.acumulador.setLimite(this.acumulador.getLimite() * -1);
            }
        }

    }
//------------------------------------------------------------------------------

    public void editAcumulador() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("acumuladorId");

        int id = Integer.parseInt(idStr);
        AcumuladorBUSSI bussi = GuiceInjectorSingleton.getInstance(AcumuladorBUSSI.class);
        acumulador = bussi.obtenerId(id);
        acumuladorId = idStr;
        //selectedItemConfig= String.valueOf(acumulador.getConfigId());
        edit = true;
    }

    public void deleteAcumulador() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("acumuladorId");
            int id = Integer.parseInt(idStr);
            AcumuladorBUSSI bussi = GuiceInjectorSingleton.getInstance(AcumuladorBUSSI.class);
            String str = "";
            acumulador = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Acumulador_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.ACUMULADOR, acumulador.getAcumuladorId() + "", acumulador.getNombre());
            listAcumulador = bussi.obtenerLista();
            newAcumulador();
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            //-------------------------------------------------------------------------------------------------

            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
        } catch (NumberFormatException e) {
            log.error("[deleteAcumulador]Error al eliminar ", e);
        }
    }
//    private void cargarItemsConfig(){
//       ConfigBUSSI qFlBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
//       List<Config> list = qFlBL.obtenerLista();
//       selectedItemConfig= "-1";
//       selectItemsConfig = new LinkedList<SelectItem>();
//       SelectItem s = new SelectItem("-1", "Seleccionar Configuración");
//       selectItemsConfig.add(s);
//      
//       for (Config conf : list) {
//            SelectItem sel = new SelectItem(conf.getConfigId(),conf.getNombre());
//            selectItemsConfig.add(sel);
//        }
//    }

    public void newAcumulador() {
        edit = false;
        acumulador = new Acumulador();
        //cargarItemsConfig();
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    /**
     * @return the acumuladorId
     */
    public String getAcumuladorId() {
        return acumuladorId;
    }

    /**
     * @param acumuladorId the acumuladorId to set
     */
    public void setAcumuladorId(String acumuladorId) {
        this.acumuladorId = acumuladorId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the acumulador
     */
    public Acumulador getAcumulador() {
        return acumulador;
    }

    /**
     * @param acumulador the acumulador to set
     */
    public void setAcumulador(Acumulador acumulador) {
        this.acumulador = acumulador;
    }

    /**
     * @return the listAcumulador
     */
    public List<Acumulador> getListAcumulador() {
        return listAcumulador;
    }

    /**
     * @param listAcumulador the listAcumulador to set
     */
    public void setListAcumulador(List<Acumulador> listAcumulador) {
        this.listAcumulador = listAcumulador;
    }

//    public List<SelectItem> getSelectItemsConfig() {
//        return selectItemsConfig;
//    }
//
//    public void setSelectItemsConfig(List<SelectItem> selectItemsConfig) {
//        this.selectItemsConfig = selectItemsConfig;
//    }
//
//    public String getSelectedItemConfig() {
//        return selectedItemConfig;
//    }
//
//    public void setSelectedItemConfig(String selectedItemConfig) {
//        this.selectedItemConfig = selectedItemConfig;
//    }
    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }
}
