/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.*;
import micrium.csu.model.*;

import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import micrium.csu.util.ReplaceStrCar_Esp;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@SessionScoped
public class ConfigForm implements Serializable {

    private static final long serialVersionUID = 1L;
    private Config myConfig;
    private List<Billetera> billeteraList;
    private String billeteraId;
    private Boolean edit;
    private String idUser;
    private static final ConfigBUSSI configBL = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
    private static final OccBUSSI occBL = GuiceInjectorSingleton.getInstance(OccBUSSI.class);
    private static final ComposicionBilleteraBUSSI compositionWalletBL = GuiceInjectorSingleton.getInstance(ComposicionBilleteraBUSSI.class);
    private static final BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
    private static final Logger log = Logger.getLogger(ConfigForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private List<SelectItem> selectItemsUnitType;
    private String selItemUnitType;
    private List<SelectItem> selectItemsBillera;
    private String selectedItemBilletera;
    private List<SelectItem> selectItemsBilleraGrupo;
    private String selectedItemBilleteraGrupo;
    private List<ComposicionBilletera> composicionBilleteraList;
    private String TextInfo;
    private String configId;
    private final CosBUSSI CosBussi = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
    private final OrigenBUSSI origenBussi = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
    private final CortoBUSSI cortoBussi = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
    /**
     * Creates a new instance of walletForm
     */
    private String nameBeforeEdit;
    private String rowMaxTable = Parameters.NroFilasDataTable;
    // CONFIGURACION
    private List<SelectItem> selectItemsOrigen;
    private String selectItemOrigen;
    private List<SelectItem> selectItemsCorto;
    private String selectItemCorto;
    private List<SelectItem> selectItemsCos;
    private String selectItemCos;
    // PANEL
    private List<OccView> listOcc;
    private int idOcc;
    private int posOcc;
    private Integer selectedOcc;

    private List<String> ListaLineaExtra;
    private String lineaExtra;
    private final String MOSTRAR_SIEMPRE = Parameters.etiquetaMostrarSiempre;

    public ConfigForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
        ListaLineaExtra = new ArrayList<>();
    }

    // @PostConstruct
    public void iniciarNuevo() {
        log.debug("iniciarNuevo---------------------");
        try {
            ListaLineaExtra = new ArrayList<>();
            myConfig = new Config();
            myConfig.setMostrarSiempre("true");
            billeteraList = new LinkedList<>();
            composicionBilleteraList = new LinkedList<>();
            idUser = ControlerBitacora.getNombreUser();
            edit = false;
            cargarItemsUnit();
            cargarItemsBilleteras();

            cargarItemsBilleterasGrupo();
            cargarItemsOrigen();
            cargarItemsCorto();
            cargarItemsCos();
            TextInfo = "(NUEVO)";
            //==================================================================

            listOcc = new ArrayList<>();
            idOcc = 1;
            posOcc = 0;
            selectedOcc = -1;
            //cos============================================================

        } catch (Exception e) {
            log.error("[iniciarNuevo]:" + e);
        }
    }

    public void quitConfiguracion() {
        log.debug("[quitConfiguracion] Ingresando a quitar configuracion selectedOcc: " + selectedOcc);
        try {
            if (selectedOcc != -1 && listOcc.size() > 0) {
                boolean eliminado = false;
                int posEliminar = 0;
                for (OccView occ : listOcc) {
                    if (!eliminado) {
                        log.debug("[quitConfiguracion] " + occ.getId() + " == " + selectedOcc.intValue());
                        if (occ.getId().intValue() == selectedOcc.intValue()) {
//                            listOcc.remove(occ);
                            eliminado = true;
                        } else {
                            posEliminar++;
                        }
                    } else {
                        occ.setPosicion(occ.getPosicion().intValue() - 1);
                        log.debug("occId: " + occ.getId() + ", pos: " + (occ.getPosicion().intValue() - 1));
                    }
                }
                if (eliminado) {
                    log.debug("[quitConfiguracion] posEliminar: " + posEliminar);
                    listOcc.remove(posEliminar);
                    posOcc--;
                    selectedOcc = -1;
                    log.debug("[quitConfiguracion] selectedOcc: " + selectedOcc);
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Seleccione una configuracion para quitar"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Seleccione una configuracion para quitar"));
            }
        } catch (Exception e) {
            log.error("[quitConfiguracion] Error al quitar configuracion", e);
        }
    }

    public void print() {
        log.debug("[print] Ingreando a imprimir _______________________________________________");
        for (OccView occ : listOcc) {
            log.debug("id: " + occ.getId() + ", pos: " + occ.getPosicion() + ", label: " + occ.getLabel());
        }
        log.debug("selectedOcc: " + selectedOcc);
        log.debug("posOcc: " + posOcc);
    }

    public void addConfiguracion() {
        log.debug("[addConfiguracion] Ingresando a adicionar configuracion, origen: " + selectItemOrigen + ", corto: " + selectItemCorto + ", cos: " + selectItemCos);
        try {
            if (!selectItemOrigen.equals("-1") && !selectItemCorto.equals("-1") && !selectItemCos.equals("-1")) {

                boolean existe = false;
                for (OccView item : listOcc) {
                    if (item.getOrigenId().intValue() == Integer.parseInt(selectItemOrigen) && item.getCortoId().intValue() == Integer.parseInt(selectItemCorto) && item.getCosId().intValue() == Integer.parseInt(selectItemCos)) {
                        existe = true;
                        break;
                    }
                }

                if (!existe) {
                    OccView ov = new OccView();
                    ov.setOrigenId(Integer.valueOf(selectItemOrigen));
                    ov.setCortoId(Integer.valueOf(selectItemCorto));
                    ov.setCosId(Integer.valueOf(selectItemCos));
                    Occ occ = occBL.obtenerOcc(ov);

                    if (edit) {
                        if (occ != null && occ.getConfigId().intValue() == myConfig.getConfigId().intValue()) {
                            occ = null;
                        }
                    }

                    log.debug("occ: " + occ);
                    if (occ == null) {

                        StringBuilder label = new StringBuilder();

                        for (SelectItem si : selectItemsOrigen) {
                            if (Integer.parseInt(si.getValue().toString()) == Integer.parseInt(selectItemOrigen)) {
                                label.append(si.getLabel());
                            }
                        }
                        for (SelectItem si : selectItemsCorto) {
                            if (Integer.parseInt(si.getValue().toString()) == Integer.parseInt(selectItemCorto)) {
                                label.append(">" + si.getLabel());
//                                label = label + " > " + si.getLabel();
                            }
                        }
                        for (SelectItem si : selectItemsCos) {
                            if (Integer.parseInt(si.getValue().toString()) == Integer.parseInt(selectItemCos)) {
                                label.append(">" + si.getLabel());
//                                label = label + " > " + si.getLabel();
                            }
                        }

                        ov = new OccView();
                        ov.setId(idOcc);
                        idOcc++;
                        ov.setPosicion(posOcc);
                        posOcc++;
                        ov.setLabel(label.toString());
                        ov.setOrigenId(Integer.valueOf(selectItemOrigen));
                        ov.setCortoId(Integer.valueOf(selectItemCorto));
                        ov.setCosId(Integer.valueOf(selectItemCos));
                        listOcc.add(ov);
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Ya existe la configuracion seleccionada"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Ya existe la configuracion seleccionada"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Seleccione una configuracion valida para adicionar"));
            }
        } catch (NumberFormatException n) {
            log.error("[addConfiguracion][NumberFormatException] Error al adicionar configuracion", n);
        } catch (Exception e) {
            log.error("[addConfiguracion] Error al adicionar configuracion", e);
        }
    }

    public void up() {
        log.debug("[up] ingresando a up, selectedOcc: " + selectedOcc);
        try {
            if (selectedOcc != null && selectedOcc.intValue() > -1) {
                int pos = 0;
                OccView ocB = null;
                for (OccView item : listOcc) {
                    if (item.getId().intValue() == selectedOcc.intValue()) {
                        ocB = item.clonar();
                        break;
                    }
                    pos++;
                }

                if (ocB != null && pos > 0) {
                    OccView ocA = listOcc.get(pos - 1).clonar();
                    int posAux = ocA.getPosicion();
                    ocA.setPosicion(ocB.getPosicion());
                    ocB.setPosicion(posAux);

                    listOcc.set(pos, ocA);
                    listOcc.set(pos - 1, ocB);
                }
            }
        } catch (Exception e) {
            log.error("[up] Error en up, selectedOcc: " + selectedOcc, e);
        }
    }

    public void down() {
        log.debug("[down] ingresando a down, selectedOcc: " + selectedOcc);
        try {
            if (selectedOcc != null && selectedOcc.intValue() > -1) {
                int pos = 0;
                OccView ocB = null;
                for (OccView item : listOcc) {
                    if (item.getId().intValue() == selectedOcc.intValue()) {
                        ocB = item.clonar();
                        break;
                    }
                    pos++;
                }

                if (ocB != null && pos + 1 < listOcc.size()) {
                    OccView ocA = listOcc.get(pos + 1).clonar();
                    int posAux = ocA.getPosicion();
                    ocA.setPosicion(ocB.getPosicion());
                    ocB.setPosicion(posAux);

                    listOcc.set(pos, ocA);
                    listOcc.set(pos + 1, ocB);
                }
            }
        } catch (Exception e) {
            log.error("[down] Error en down, selectedOcc: " + selectedOcc, e);
        }
    }

    public void iniciarEdit() {
        log.debug("iniciarEdit----------------------");
        String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("configId");
        try {
            int id = Integer.valueOf(Idstr);
            cargarItemsOrigen();
            cargarItemsCorto();
            cargarItemsCos();
            myConfig = configBL.obtenerId(id);

            if (myConfig.getMostrarSiempre() != null && myConfig.getMostrarSiempre().equals("t")) {
                myConfig.setMostrarSiempre("true");
            } else if (myConfig.getMostrarSiempre() != null && myConfig.getMostrarSiempre().equals("f")) {
                myConfig.setMostrarSiempre("false");
            }

            if (myConfig.getMostrarBilleterasNoConfig() != null && myConfig.getMostrarBilleterasNoConfig().equals("t")) {
                myConfig.setMostrarBilleterasNoConfig("true");
            } else if (myConfig.getMostrarBilleterasNoConfig() != null && myConfig.getMostrarBilleterasNoConfig().equals("f")) {
                myConfig.setMostrarBilleterasNoConfig("false");
            }

            idUser = ControlerBitacora.getNombreUser();
            edit = true;
            cargarItemsUnit();
            cargarListaBilleteras(id);
            cargarItemsBilleteras();

            cargarListaBilleterasGrupo(id);
            cargarItemsBilleterasGrupo();
            cargarOcc(id);
            TextInfo = "(EDITAR)";
            nameBeforeEdit = myConfig.getNombre();
            if (myConfig.getLineas() != null && !"".equals(myConfig.getLineas())) {
                ListaLineaExtra = parserToList(myConfig.getLineas());
            }
        } catch (Exception e) {
            log.error("[iniciarEdit:" + e);
        }
    }

    private void cargarOcc(int configId) {
        log.debug("[cargarOcc] Iniciando cargar Occ, configId: " + configId);
        listOcc = occBL.obtenerListaView(configId);

        if (listOcc == null || listOcc.size() == 0) {
            listOcc = new ArrayList<OccView>();
            idOcc = 1;
            posOcc = 0;
        } else {
            OccView ov = listOcc.get(listOcc.size() - 1);
            idOcc = ov.getId() + 1;
            posOcc = ov.getPosicion() + 1;
            log.debug("[cargarOcc] Max Id OCC: " + idOcc);
        }
        selectedOcc = -1;
    }

    private void cargarListaBilleteras(int idConfig) {
        try {
            billeteraList = walletBL.obtenerListaByIdConfig(idConfig);
            int i = 0;
            //determinar posicion
            for (Billetera billetera : billeteraList) {
                billetera.setConfigBilletera_posicion(i);
                log.info("MOSTRAR SALDO EXPIRADO: " + billetera.isConfigBilletera_mostrar_saldo_expirado());
                i++;
            }

        } catch (Exception e) {
            log.error("[cargarListaBilleteras]", e);
        }
    }

    private void cargarListaBilleterasGrupo(int idConfig) {
        try {
            log.info("[cargarListaBilleterasGrupo] Iniciando carga para IdCos=" + idConfig + " ");
            composicionBilleteraList = compositionWalletBL.obtenerListaByIdConfig(idConfig);
            int i = 0;
            for (ComposicionBilletera compBilletera : composicionBilleteraList) {
                compBilletera.setConfig_posicion(i);
                i++;
            }
        } catch (Exception e) {
            log.error("[cargarListaBilleterasGrupo]Al cargar Lista Billeteras Grupo :" + e.getMessage());
        }

    }

    private void cargarItemsBilleteras() {
        try {
            List<Billetera> list = walletBL.obtenerLista();
            selectedItemBilletera = "-1";
            selectItemsBillera = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Billetera");
            selectItemsBillera.add(s);
            if (billeteraList != null) {
                for (Billetera billetera : list) {
                    if (!containBilletera(billetera.getNombreComverse(), billeteraList)) {
                        SelectItem sel = new SelectItem(billetera.getBilleteraId(), billetera.getNombreComverse());
                        selectItemsBillera.add(sel);
                    }

                }
            }

        } catch (Exception e) {
            log.error("[cargarItemBilleteras] error=" + e.getMessage());
        }

    }

    private void cargarItemsBilleterasGrupo() {
        try {
            List<ComposicionBilletera> list = compositionWalletBL.obtenerLista();

            selectedItemBilleteraGrupo = "-1";
            selectItemsBilleraGrupo = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Billetera Grupo");
            selectItemsBilleraGrupo.add(s);

            for (ComposicionBilletera composicionBilletera : list) {
                if (!containBilleteraGrupo(composicionBilletera.getNombre(), composicionBilleteraList)) {

                    SelectItem sel = new SelectItem(composicionBilletera.getComposicionBilleteraId(), composicionBilletera.getNombre());
                    selectItemsBilleraGrupo.add(sel);

                }
            }
        } catch (Exception e) {
            log.error("[cargaritemsBilleterasGrupo] " + e.getMessage());
        }
    }

    private boolean containBilletera(String nameBilletera, List<Billetera> lista) {
        try {
            if (lista != null) {
                for (Billetera billetera : lista) {
                    if (billetera.getNombreComverse().equals(nameBilletera)) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            log.error("[containBilletera]Error:" + e.getMessage());
        }
        return false;
    }

    private boolean containBilleteraGrupo(String nameBilleteraCompo, List<ComposicionBilletera> lista) {
        if (lista != null) {
            for (ComposicionBilletera compoBilletera : lista) {
                if (compoBilletera.getNombre().equals(nameBilleteraCompo)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addBilleteraSimple() {
        try {
            log.debug("[addBilleteraSimple]iniciando..");
            if (billeteraList == null) {
                billeteraList = new LinkedList<Billetera>();
            }
            int id = Integer.valueOf(selectedItemBilletera);
            if (id >= 0) {
                log.debug("[addBilleteraSimple]add billeteraId=" + id);
                Billetera bill = walletBL.obtenerId(id);
                bill.setConfigBilletera_mostrar_saldo_cero(false);
                bill.setConfigBilletera_no_mostrar_saldo_expirado(false);
                bill.setConfigBilletera_mostrar_saldo_mayor_cero(false);
                bill.setConfigBilletera_mostrar_saldo_menor_cero(false);
                //bill.setConfigBilletera_mostrar_siempr(true);
                bill.setConfigBilletera_mostrar_siempre(true);
                bill.setConfigBilletera_mostrar_vigencia(false);
                ///////////////////////////////////////////////////////////////
                bill.setMostrarSegundaFechaExp(false);
                bill.setMostrarHoraSegundaFechaExp(false);
                bill.setAsumirFormatoHoraPrimeraFecha(false);
                ///////////////////////////////////////////////////////////////
                bill.setConfigBilletera_nombreComercial(bill.getNombreComercial());
                bill.setConfigBilletera_posicion(billeteraList.size());
                log.debug("[addBilleteraSimple]add billeteraId=" + id + "|-----1");
                bill.setNewForConfig(true);
                billeteraList.add(bill);
                log.debug("[addBilleteraSimple]add billeteraId=" + id + "|-----BilleteraSize=" + billeteraList.size());
                cargarItemsBilleteras();
            }

        } catch (Exception e) {
            log.error("[addBilleteraSimple]:" + e.getMessage());
        }
    }

    public void addBilleteraGrupo() {
        try {
            if (composicionBilleteraList == null) {
                composicionBilleteraList = new LinkedList<ComposicionBilletera>();
            }
            int id = Integer.valueOf(selectedItemBilleteraGrupo);
            if (id >= 0) {
                ComposicionBilletera bill = compositionWalletBL.obtenerId(id);
                bill.setEstado("t");
                bill.setConfig_mostrar_saldo_cero(false);
                bill.setConfig_no_mostrar_saldo_expirado(false);
                bill.setConfig_mostrar_saldo_mayor_cero(false);
                bill.setConfig_mostrar_saldo_menor_cero(false);
                bill.setConfig_mostrar_siempre(true);
                bill.setConfig_mostrar_vigencia(false);
                ////////////////////////////////////////////////////////////////
                bill.setMostrarSegundaFechaExp(false);
                bill.setMostrarHoraSegundaFechaExp(false);
                bill.setAsumirFormatoHoraPrimeraFecha(false);
                ////////////////////////////////////////////////////////////////
                bill.setConfig_nombreComercial(bill.getNombreComercial());
                bill.setConfig_posicion(composicionBilleteraList.size());
                bill.setNewForConfig(true);
                composicionBilleteraList.add(bill);
                cargarItemsBilleterasGrupo();
            }
        } catch (Exception e) {
            log.error("[addBilleteraGRUPO]:" + e.getMessage());
        }
    }

    public void delBilleteraSimple() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[delBilleteraSimple] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion >= 0) {
                billeteraList.remove(posicion);
                //return;
                int i = 0;
                for (Billetera billetera : billeteraList) {
                    billetera.setConfigBilletera_posicion(i);
                    i++;
                }
            }
            cargarItemsBilleteras();

        } catch (Exception e) {
            log.error("[delBilleteraSimple] error:" + e.getMessage());
        }
    }

    public void delBilleteraComp() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posCompBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[delBilleteraComp] Iniciando proceso para quitar billeteraComp|pos=" + posicion);
            if (posicion >= 0) {
                composicionBilleteraList.remove(posicion);
                //return;
                int i = 0;
                for (ComposicionBilletera billeteraComp : composicionBilleteraList) {
                    billeteraComp.setConfig_posicion(posicion);
                    i++;
                }
            }
            cargarItemsBilleterasGrupo();

        } catch (Exception e) {
            log.error("[delBilleteraSimple] error:" + e.getMessage());
        }
    }
    //==========================================================================

    public String saveConfig() {
        log.debug("[saveConfig]iniciando save...");
        //falta validar------          
        try {
            String str = validate();
            if (!str.isEmpty()) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "", str));
                return "";
            }

            if (myConfig.getMostrarDpi() != null && myConfig.getMostrarDpi().equals("true")) {
                myConfig.setMostrarDpi("t");
            } else if (myConfig.getMostrarDpi() != null && myConfig.getMostrarDpi().equals("false")) {
                myConfig.setMostrarDpi("f");
            }

            if (myConfig.getMostrarVigencia() != null && myConfig.getMostrarVigencia().equals("true")) {
                myConfig.setMostrarVigencia("t");
            } else if (myConfig.getMostrarVigencia() != null && myConfig.getMostrarVigencia().equals("false")) {
                myConfig.setMostrarVigencia("f");
            }

            if (myConfig.getMostrarBilleterasNoConfig() != null && myConfig.getMostrarBilleterasNoConfig().equals("true")) {
                myConfig.setMostrarBilleterasNoConfig("t");
            } else if (myConfig.getMostrarBilleterasNoConfig() != null && myConfig.getMostrarBilleterasNoConfig().equals("false")) {
                myConfig.setMostrarBilleterasNoConfig("f");
            }

            if (myConfig.getMostrarAcumuladosMegas() != null && myConfig.getMostrarAcumuladosMegas().equals("true")) {
                myConfig.setMostrarAcumuladosMegas("t");
            } else if (myConfig.getMostrarAcumuladosMegas() != null && myConfig.getMostrarAcumuladosMegas().equals("false")) {
                myConfig.setMostrarAcumuladosMegas("f");
            }

            /*MOSTRAR SIEMPRE*/
            if (myConfig.getMostrarSiempre() != null && myConfig.getMostrarSiempre().equals("true")) {
                myConfig.setMostrarSiempre("t");
            } else if (myConfig.getMostrarSiempre() != null && myConfig.getMostrarSiempre().equals("false")) {
                myConfig.setMostrarSiempre("f");
            }

            if (!edit) {//guardar
                myConfig.setEstado("t");
                myConfig.setHabilitado("t");
                // myConfig.setMostrarBilleterasNoConfig("f");
                String lineas = parseToString(ListaLineaExtra);
                myConfig.setLineas(lineas);

                configBL.insert(myConfig);
                //-------------------------------------------------------------
                //guardar billeteras simples
                saveListaConfigBilletera(billeteraList, myConfig.getConfigId());
                //--------------------------------------------------------------
                //guardar billeteras compuestas
                saveListaConfigCompBilletera(composicionBilleteraList, myConfig.getConfigId());
                //--------------------------------------------------------------
//                UpdateListCosSelectConfig(cosDualList.getSource(), 0);
//                UpdateListCosSelectConfig(cosDualList.getTarget(), myConfig.getConfigId());
                for (OccView ov : listOcc) {
                    Occ oc = new Occ();
                    oc.setConfigId(myConfig.getConfigId());
                    oc.setOrigenId(ov.getOrigenId());
                    oc.setCortoId(ov.getCortoId());
                    oc.setCosId(ov.getCosId());
                    oc.setPosicion(ov.getPosicion());
                    oc.setEstado("t");
                    occBL.insert(oc);
                }
                //--------------------------------------------------------------  
                //actualizar variable de app
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------------------------------------------
                //str = ParameterMessage.UnitType_MESSAGE_GUARDAR+unittype.getUnitTypeId();
                ControlerBitacora.insert(DescriptorBitacoraLC.CONFIGURACION, myConfig.getConfigId() + "", myConfig.getNombre());
                //regresar a pantalla de listas

            } else {//guardar Change Edit
                //myConfig.setMostrarBilleterasNoConfig(false); 
                log.debug("update---------------------------------------------");
                String lineas = parseToString(ListaLineaExtra);
                myConfig.setLineas(lineas);

                configBL.update(myConfig);
                //Actualizar la lista de billeteras con estado false------------
                updateOffListaConfigBilletera(myConfig.getConfigId());
                saveOrUpdateListaConfigBilletera(billeteraList, myConfig.getConfigId());
                //--------------------------------------------------------------
                updateOffListaConfigCompBilletera(myConfig.getConfigId());
                saveOrUpdateListaConfigCompBilletera(composicionBilleteraList, myConfig.getConfigId());
                //--------------------------------------------------------------
//                UpdateListCosSelectConfig(cosDualList.getSource(), 0);
//                UpdateListCosSelectConfig(cosDualList.getTarget(), myConfig.getConfigId());
                updateListOcc();
                //--------------------------------------------------------------  
                //actualizar variable de app
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();
                //-------------------------------------------------------------------------------------------------

                ControlerBitacora.update(DescriptorBitacoraLC.CONFIGURACION, myConfig.getConfigId() + "", myConfig.getNombre());
            }
            listOcc = new ArrayList<>();
            selectedOcc = -1;
            idOcc = 1;
            posOcc = 0;

            return "/view/listConfigForm.xhtml?faces-redirect=true";

        } catch (Exception e) {
            log.error("[saveConfig] Error al intentar guardar cambios:" + e.getMessage());

        }

        return "";
    }
    //==========================================================================

    public void updateListOcc() {
        log.debug("[updateListOcc] Ingresando a actualizar los Occ");
        try {
            for (OccView ov : listOcc) {

                Occ occ = occBL.obtenerOcc(ov);
                if (occ != null) {
                    if (occ.getConfigId().intValue() == myConfig.getConfigId().intValue()) {
                        log.debug("La configuracion ya existe solo se actualiza su posicion configId: " + occ.getConfigId() + ", origen: " + ov.getNombreOrigen() + ", corto: " + ov.getNombreCorto() + ", cos: " + ov.getNombreCos());
                        occ.setPosicion(ov.getPosicion());
                        occBL.update(occ);
                    } else {
                        log.warn("Ya existe la configuracion configId: " + occ.getConfigId() + ", origen: " + ov.getNombreOrigen() + ", corto: " + ov.getNombreCorto() + ", cos: " + ov.getNombreCos());
                    }
                } else {
                    Occ oc = new Occ();
                    oc.setConfigId(myConfig.getConfigId());
                    oc.setOrigenId(ov.getOrigenId());
                    oc.setCortoId(ov.getCortoId());
                    oc.setCosId(ov.getCosId());
                    oc.setPosicion(ov.getPosicion());
                    oc.setEstado("t");
                    log.debug("Insertando configuracion para actualizar configId: " + ov.getConfigId() + ", origen: " + ov.getNombreOrigen() + ", corto: " + ov.getNombreCorto() + ", cos: " + ov.getNombreCos());
                    occBL.insert(oc);
                }
            }

            List<OccView> list = occBL.obtenerListaView(myConfig.getConfigId());
            for (OccView item : list) {
                boolean existe = false;
                for (OccView ov : listOcc) {
                    if (item.getOrigenId().intValue() == ov.getOrigenId().intValue() && item.getCortoId().intValue() == ov.getCortoId().intValue() && item.getCosId().intValue() == ov.getCosId().intValue()) {
                        existe = true;
                    }
                }
                if (!existe) {
                    log.debug("Eliminando configuracion para actualizar occId: " + item.getId() + ", configId: " + item.getConfigId() + ", origen: " + item.getNombreOrigen() + ", corto: " + item.getNombreCorto() + ", cos: " + item.getNombreCos());
                    occBL.delete(item.getId());
                }
            }

        } catch (Exception e) {
            log.error("[updateListOcc] Error al actualizar Occ", e);
        }
    }

    //--------------------------------------------------------------------------
    private String validate() {
        myConfig.setNombre(myConfig.getNombre().trim());
        myConfig.setDescripcion(myConfig.getDescripcion().trim());
        myConfig.setSaludoInicial(ReplaceStrCar_Esp.replaceBase(myConfig.getSaludoInicial().trim()));

        if (myConfig.getNombre().isEmpty()) {
            return "El campo Nombre esta vacío";
        }
        if (myConfig.getDescripcion().isEmpty()) {
            return "El campo Descripción esta vacío";
        }

        //------------------------------------------------------------------
        ConfigBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigBUSSI.class);
        Config con = bl.obtenerByName(myConfig.getNombre());

        if (con != null) {
            if (!edit) {
                if (myConfig.getNombre().equals(con.getNombre())) {
                    return "El Nombre ya existe";
                }
            } else {
                if (!myConfig.getNombre().equals(nameBeforeEdit)) {
                    if (myConfig.getNombre().equals(con.getNombre())) {
                        return "El Nombre ya existe";
                    }
                }
            }

        }
        //------------------------------------------------------------------
        boolean sw_hay_billetera_predeterminada = false;
        if (billeteraList != null) {
            if (billeteraList.size() > 0) {
                int i = 0;
                while ((i < billeteraList.size()) && !sw_hay_billetera_predeterminada) {
                    Billetera bil = billeteraList.get(i);
                    if (bil.isConfigBilletera_mostrar_siempre()) {
                        sw_hay_billetera_predeterminada = true;
                    }
                    i++;
                }

            }
        }
        if (myConfig.getMostrarSiempre() != null && myConfig.getMostrarSiempre().equals("true") && !sw_hay_billetera_predeterminada) {
            return "Debe existir por lo menos una Billetera configurada para MOSTRAR SIEMPRE";
        }
        //------------------------------------------------------------------ 
        return "";
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

    private void updateOffListaConfigBilletera(int idConfig) {
        //log.info("[updateOffListaConfigBilletera]UserId="+idUser+"|ConfigId="+idConfig+" iniciando proceso de guardar Config_billetera");
        ConfigBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigBilleteraBUSSI.class);
        //String str="";
        try {
            bl.updateOffAllByIdConfig(idConfig);
        } catch (Exception e) {
            log.error("[updateOffListaConfigBilletera] Al intentar actualizar:" + e.getMessage());
        }
//         return str; 
    }

    private void updateOffListaConfigCompBilletera(int idConfig) {
        //log.info("[updateOffListaConfigCompBilletera]UserId="+idUser+"|ConfigId="+idConfig+" iniciando proceso de guardar Config_billetera");
        ConfigComposicionBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigComposicionBilleteraBUSSI.class);
        //String str="";
        try {
            bl.updateOffAllByIdConfig(idConfig);
        } catch (Exception e) {
            log.error("[updateOffListaConfigBilletera] Al intentar actualizar:" + e.getMessage());
        }
//         return str; 
    }

    public String BooleantoString(Boolean value) {
        return value ? "t" : "f";
    }

    private String saveListaConfigBilletera(List<Billetera> listConfigBill, int idConfig) {
        log.info("[saveListaConfigBilletera]UserId=" + idUser + "|ConfigId=" + idConfig + " iniciando proceso de guardar Config_billetera");
        ConfigBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigBilleteraBUSSI.class);
        String str = "";
        try {
            for (Billetera bill : listConfigBill) {
                ConfigBilletera configBill = new ConfigBilletera();
                configBill.setBilleteraId(bill.getBilleteraId());
                configBill.setConfigId(idConfig);
                configBill.setEstado("t");
                configBill.setMostrarSaldoCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_cero()));
                configBill.setNoMostrarSaldoExpirado(BooleantoString(bill.isConfigBilletera_no_mostrar_saldo_expirado()));
                configBill.setMostrarSaldoMayorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_mayor_cero()));
                configBill.setMostrarSaldoMenorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_menor_cero()));
                configBill.setMostrarSiempre(BooleantoString(bill.isConfigBilletera_mostrar_siempre()));
                configBill.setMostrarVigencia(BooleantoString(bill.isConfigBilletera_mostrar_vigencia()));
                configBill.setMostrarSaldoExpirado(BooleantoString(bill.isConfigBilletera_mostrar_saldo_expirado()));
                if (configBill.getMostrarSiempre() == "true" || configBill.getMostrarSiempre() == "t") {
                    log.debug("[saveListaConfigBilletera]todos a cero");
                    configBill.setMostrarSaldoCero("f");
                    configBill.setNoMostrarSaldoExpirado("f");
                    configBill.setMostrarSaldoMayorCero("f");
                    configBill.setMostrarSaldoMenorCero("f");
                    configBill.setMostrarSegundaFechaExp("f");
                    configBill.setMostrarHoraSegundaFechaExp("f");
                    configBill.setAsumirFormatoHoraPrimeraFecha("f");
                }
                configBill.setNombreComercial(bill.getConfigBilletera_nombreComercial());
                configBill.setPosicion(bill.getConfigBilletera_posicion());
                log.debug("[saveListaConfigBilletera]nombre comercial=" + configBill.getNombreComercial());
                if (bill.isNewForConfig()) {
                    if (!bl.insert(configBill)) {
                        bl.update(configBill);
                    }
                } else {
                    log.debug("[saveListaConfigBilletera][Update Billeteras para Config]" + configBill.getNombreComercial());
                    bl.update(configBill);
                }

            }

        } catch (Exception e) {
            log.error("[saveConfig] Al intentar actualizar COS:" + e.getMessage());
        }
        return str;
    }

    private String saveOrUpdateListaConfigBilletera(List<Billetera> listConfigBill, int idConfig) {
        log.info("[saveListaConfigBilletera]UserId=" + idUser + "|ConfigId=" + idConfig + " iniciando proceso de guardar Config_billetera");
        ConfigBilleteraBUSSI bl = GuiceInjectorSingleton.getInstance(ConfigBilleteraBUSSI.class);
        String str = "";
        try {
            for (Billetera bill : listConfigBill) {
                ConfigBilletera configBill = new ConfigBilletera();
                configBill.setBilleteraId(bill.getBilleteraId());
                configBill.setConfigId(idConfig);
                configBill.setEstado("t");
                configBill.setMostrarSaldoCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_cero()));
                configBill.setNoMostrarSaldoExpirado(BooleantoString(bill.isConfigBilletera_no_mostrar_saldo_expirado()));
                configBill.setMostrarSaldoMayorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_mayor_cero()));
                configBill.setMostrarSaldoMenorCero(BooleantoString(bill.isConfigBilletera_mostrar_saldo_menor_cero()));
                configBill.setMostrarSiempre(BooleantoString(bill.isConfigBilletera_mostrar_siempre()));
                configBill.setNombreComercial(bill.getConfigBilletera_nombreComercial());
                configBill.setMostrarVigencia(BooleantoString(bill.isConfigBilletera_mostrar_vigencia()));
                configBill.setPosicion(bill.getConfigBilletera_posicion());
                configBill.setMostrarSegundaFechaExp(BooleantoString(bill.isMostrarSegundaFechaExp()));
                configBill.setMostrarHoraSegundaFechaExp(BooleantoString(bill.isMostrarHoraSegundaFechaExp()));
                configBill.setAsumirFormatoHoraPrimeraFecha(BooleantoString(bill.isAsumirFormatoHoraPrimeraFecha()));
                configBill.setMostrarSaldoExpirado(BooleantoString(bill.isConfigBilletera_mostrar_saldo_expirado()));

                if (configBill.getMostrarSiempre() == "true" || configBill.getMostrarSiempre() == "t") {
                    log.debug("[]todos a cero");
                    configBill.setMostrarSaldoCero("f");
                    configBill.setNoMostrarSaldoExpirado("f");
                    configBill.setMostrarSaldoMayorCero("f");
                    configBill.setMostrarSaldoMenorCero("f");
                }

                log.debug("[saveListaConfigBilletera]nombre comercial=" + configBill.getNombreComercial());
                if (bl.existe(configBill)) {
                    bl.update(configBill);
                } else {
                    bl.insert(configBill);
                }
            }

        } catch (Exception e) {
            log.error("[saveConfig] Al intentar actualizar COS:" + e.getMessage());
        }
        return str;
    }

    private String UpdateListCosSelectConfig(List<String> listCos, int idConfig) {
        log.info("[UpdateListCosSelectConfig]UserId=" + idUser + "|ConfigId=" + idConfig + " iniciando proceso de guardar Config_billetera");

        String str = "";
        try {
            for (String strCos : listCos) {
                Cos myCos = CosBussi.obtenerByName(strCos);
                if (myCos != null) {
//                      myCos.setConfigId(idConfig);
                    myCos.setHabilitado("t");
                    myCos.setEstado("t");
                    CosBussi.update(myCos);
                }
            }

        } catch (Exception e) {
            log.error("[UpdateListCosSelectConfig] Al intentar actualizar COS:" + e.getMessage());
        }
        return str;
    }
    //------------------------------------------------------------------------

    private String saveOrUpdateListaConfigCompBilletera(List<ComposicionBilletera> listCompCos, int idConfig) {
        log.info("[saveOrUpdateListaConfigCompBilletera]IdConfig=" + idConfig + " Iniciando proceso para guardar registro..");
        String str = "";
        ConfigComposicionBilleteraBUSSI configCompBillBL = GuiceInjectorSingleton.getInstance(ConfigComposicionBilleteraBUSSI.class);
        for (ComposicionBilletera dat : listCompCos) {
            ConfigComposicionBilletera cb = new ConfigComposicionBilletera();
            cb.setComposicionBilleteraId(dat.getComposicionBilleteraId());
            cb.setConfigId(idConfig);
            cb.setEstado("t");
            cb.setMostrarSaldoCero(BooleantoString(dat.isConfig_mostrar_saldo_cero()));
            cb.setNoMostrarSaldoExpirado(BooleantoString(dat.isConfig_no_mostrar_saldo_expirado()));
            cb.setMostrarSaldoMayorCero(BooleantoString(dat.isConfig_mostrar_saldo_mayor_cero()));
            cb.setMostrarSaldoMenorCero(BooleantoString(dat.isConfig_mostrar_saldo_menor_cero()));
            cb.setMostrarSiempre(BooleantoString(dat.isConfig_mostrar_siempre()));
            cb.setMostrarVigencia(BooleantoString(dat.isConfig_mostrar_vigencia()));
            cb.setNombreComercial(dat.getConfig_nombreComercial());
            cb.setPosicion(dat.getConfig_posicion());
            /////////////////////////////////////////////////
            cb.setMostrarSegundaFechaExp(BooleantoString(dat.isMostrarSegundaFechaExp()));
            cb.setMostrarHoraSegundaFechaExp(BooleantoString(dat.isMostrarHoraSegundaFechaExp()));
            cb.setAsumirFormatoHoraPrimeraFecha(BooleantoString(dat.isAsumirFormatoHoraPrimeraFecha()));
            ////////////////////////////////////////////////
            cb.setMostrarSaldoExpirado(BooleantoString(dat.isConfig_mostrar_saldo_expirado()));

            if (cb.getMostrarSiempre().equals("true") || cb.getMostrarSiempre().equals("t")) {
                log.debug("[]todos a cero");
                cb.setMostrarSaldoCero("f");
                cb.setNoMostrarSaldoExpirado("f");
                cb.setMostrarSaldoMayorCero("f");
                cb.setMostrarSaldoMenorCero("f");

            }
            //cb.set
            //cb.setConsultaCosId();
            //cosCompBillBL.insert(cb);
            if (configCompBillBL.existe(cb)) {
                configCompBillBL.update(cb);
            } else {
                configCompBillBL.insert(cb);
            }
        }
        return str;
    }
    //-------------------------------------------------------------------------

    private String saveListaConfigCompBilletera(List<ComposicionBilletera> listCompCos, int idConfig) {
        log.info("[saveListaConfigCompBilletera]IdConfig=" + idConfig + " Iniciando proceso para guardar registro..");
        String str = "";
        ConfigComposicionBilleteraBUSSI configCompBillBL = GuiceInjectorSingleton.getInstance(ConfigComposicionBilleteraBUSSI.class);
        for (ComposicionBilletera dat : listCompCos) {
            ConfigComposicionBilletera cb = new ConfigComposicionBilletera();
            cb.setComposicionBilleteraId(dat.getComposicionBilleteraId());
            cb.setConfigId(idConfig);
            cb.setEstado("t");
            cb.setMostrarSaldoCero(BooleantoString(dat.isConfig_mostrar_saldo_cero()));
            cb.setNoMostrarSaldoExpirado(BooleantoString(dat.isConfig_no_mostrar_saldo_expirado()));
            cb.setMostrarSaldoMayorCero(BooleantoString(dat.isConfig_mostrar_saldo_mayor_cero()));
            cb.setMostrarSaldoMenorCero(BooleantoString(dat.isConfig_mostrar_saldo_menor_cero()));
            //////////////////////////////////////////////////////
            cb.setMostrarSiempre(BooleantoString(dat.isConfig_mostrar_siempre()));
            cb.setMostrarVigencia(BooleantoString(dat.isConfig_mostrar_vigencia()));
            cb.setNombreComercial(dat.getConfig_nombreComercial());
            //////////////////////////////////////////////////////
            cb.setPosicion(dat.getConfig_posicion());

            cb.setMostrarSaldoExpirado(BooleantoString(dat.isConfig_mostrar_saldo_expirado()));

            if (cb.getMostrarSiempre().equals("true")) {
                cb.setMostrarSaldoCero("f");
                cb.setNoMostrarSaldoExpirado("f");
                cb.setMostrarSaldoMayorCero("f");
                cb.setMostrarSaldoMenorCero("f");
                cb.setMostrarSegundaFechaExp("f");
                cb.setMostrarHoraSegundaFechaExp("f");
                cb.setAsumirFormatoHoraPrimeraFecha("f");
            }
            //boolean sw=false;     
            if (dat.isNewForConfig()) {
                if (!configCompBillBL.insert(cb)) {
                    configCompBillBL.update(cb);
                }
            } else {
                log.debug("[saveListaConfigCompBilletera][Update Billeteras para COS]" + dat.getNombreComercial());
//                     bl.update(cosBill);
                configCompBillBL.update(cb);
            }
        }
        return str;
    }
//     public void subirBilletera(){
//        try {
//            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posBilletera");
//            int posicion = Integer.parseInt(StrPosicion);
//            log.info("[subirBilletera] Iniciando cambio de posicion para Billetera|pos="+posicion);
//            if (posicion > 0) {
//                int posicionIntercambiar = posicion - 1;
//                Billetera billCurrent = billeteraList.get(posicion);
//                Billetera billIntercambiar = billeteraList.get(posicionIntercambiar);
//                //billCurrent.setPosicion(posicionIntercambiar);
//                billCurrent.setConfigBilletera_posicion(posicionIntercambiar);
//                //billIntercambiar.setPosicion(posicion);
//                billIntercambiar.setConfigBilletera_posicion(posicion);
//                billeteraList.set(posicionIntercambiar, billCurrent);
//                billeteraList.set(posicion, billIntercambiar);
//                
//                log.debug("[subirBilletera]Pos Intercambio="+posicionIntercambiar);
//                
//                for (Billetera billetera : billeteraList) {
//                    log.debug("[subirBilletera] name="+billetera.getNombreComverse());
//                }
//                //return;
//            }
//        } catch (Exception e) {
//            log.error("[subirbilletera] error:"+e.getMessage());
//        }
//    }
    //--------------------------------------------------------------------------    

    public String upBilletera() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[subirBilletera] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion > 0) {
                int posicionIntercambiar = posicion - 1;
                Billetera billCurrent = billeteraList.get(posicion);
                Billetera billIntercambiar = billeteraList.get(posicionIntercambiar);
                //billCurrent.setPosicion(posicionIntercambiar);
                billCurrent.setConfigBilletera_posicion(posicionIntercambiar);
                //billIntercambiar.setPosicion(posicion);
                billIntercambiar.setConfigBilletera_posicion(posicion);
                billeteraList.set(posicionIntercambiar, billCurrent);
                billeteraList.set(posicion, billIntercambiar);

                log.debug("[subirBilletera]Pos Intercambio=" + posicionIntercambiar);

                for (Billetera billetera : billeteraList) {
                    log.debug("[subirBilletera] name=" + billetera.getNombreComverse());
                }
                return "/view/configForm.xhtml?faces-redirect=true";
            }
        } catch (NumberFormatException e) {
            log.error("[subirbilletera] error:" + e.getMessage());
        }
        return "";
    }
    //--------------------------------------------------------------------------    

    public void bajarBilletera() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[bajarBilletera] Iniciando cambio de posicion para Billetera|pos=" + posicion);
            if (posicion < billeteraList.size() - 1) {
                int posicionIntercambiar = posicion + 1;
                Billetera billCurrent = billeteraList.get(posicion);
                Billetera billIntercambiar = billeteraList.get(posicionIntercambiar);
                //billCurrent.setPosicion(posicionIntercambiar);  
                billCurrent.setConfigBilletera_posicion(posicionIntercambiar);
                //billIntercambiar.setPosicion(posicion);
                billIntercambiar.setConfigBilletera_posicion(posicion);

                billeteraList.set(posicionIntercambiar, billCurrent);
                billeteraList.set(posicion, billIntercambiar);
            }
        } catch (NumberFormatException e) {
            log.error("[bajarBilletera] error:" + e.getMessage());
        }
    }

    public void subirCompBilletera() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posCompBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[subirCompBilletera] Iniciando cambio de posicion para Billetera");
            if (posicion > 0) {
                int posicionIntercambiar = posicion - 1;
                ComposicionBilletera billCurrent = composicionBilleteraList.get(posicion);
                ComposicionBilletera billIntercambiar = composicionBilleteraList.get(posicionIntercambiar);
                //billCurrent.setPosicion(posicionIntercambiar);  
                billCurrent.setConfig_posicion(posicionIntercambiar);
                //billIntercambiar.setPosicion(posicion);
                billIntercambiar.setConfig_posicion(posicion);

                composicionBilleteraList.set(posicionIntercambiar, billCurrent);
                composicionBilleteraList.set(posicion, billIntercambiar);

            }
        } catch (NumberFormatException e) {
            log.error("[subirCompBilletera] error:" + e.getMessage());
        }
    }

    public void bajarCompBilletera() {
        try {
            String StrPosicion = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posCompBilletera");
            int posicion = Integer.parseInt(StrPosicion);
            log.info("[bajarCompBilletera] Iniciando cambio de posicion para Billetera");
            if (posicion < composicionBilleteraList.size() - 1) {
                int posicionIntercambiar = posicion + 1;
                ComposicionBilletera billCurrent = composicionBilleteraList.get(posicion);
                ComposicionBilletera billIntercambiar = composicionBilleteraList.get(posicionIntercambiar);
                //billCurrent.setPosicion(posicionIntercambiar);  
                billCurrent.setConfig_posicion(posicionIntercambiar);
                //billIntercambiar.setPosicion(posicion);
                billIntercambiar.setConfig_posicion(posicion);
                composicionBilleteraList.set(posicionIntercambiar, billCurrent);
                composicionBilleteraList.set(posicion, billIntercambiar);
            }
        } catch (NumberFormatException e) {
            log.error("[bajarCompBilletera] error:" + e.getMessage());
        }
    }
//    

//    private String validar(String palabra) {
//        if (palabra.equals("")) {
//            return "campo vacio";
//        }
//        palabra = palabra.toUpperCase();
//        try {
//            BilleteraBUSSI datoBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
//            List<Billetera> lista = datoBL.obtenerLista();
//            for (int i = 0; i < lista.size(); i++) {
//                Billetera dato = lista.get(i);
//                String str = dato.getNombreComverse().toUpperCase();
//                if (str.equals(palabra)) {
//                    return "Ya existe una billetera con ese nombre";
//                }
//            }
//        } catch (Exception e) {
//            log.error("[validar]|user=" + idUser + " Error al validar: " + e.getMessage());
//        }
//        return "";
//    }
    public void newConfig() {
        edit = false;
        myConfig = new Config();
    }

    private void cargarItemsUnit() {
        UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
        List<UnitType> list = qFlBL.obtenerLista();
        selItemUnitType = "-1";
        selectItemsUnitType = new LinkedList<SelectItem>();
        SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
        selectItemsUnitType.add(s);

        for (UnitType unitType : list) {
            SelectItem sel = new SelectItem(unitType.getUnitTypeId(), unitType.getNombre());
            selectItemsUnitType.add(sel);
        }
    }

    private void cargarItemsOrigen() {
        try {
            OrigenBUSSI qFlBL = GuiceInjectorSingleton.getInstance(OrigenBUSSI.class);
            List<Origen> list = qFlBL.obtenerLista();
            selectItemOrigen = "-1";
            selectItemsOrigen = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Origen");
            selectItemsOrigen.add(s);
            if (list != null) {
                for (Origen origen : list) {
                    SelectItem sel = new SelectItem(origen.getOrigenId(), origen.getNombre());
                    selectItemsOrigen.add(sel);
                }
            }
        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Origen:" + e.getMessage(), e);
        }
    }

    private void cargarItemsCorto() {
        try {
            CortoBUSSI qFlBL = GuiceInjectorSingleton.getInstance(CortoBUSSI.class);
            List<Corto> list = qFlBL.obtenerLista();
            selectItemCorto = "-1";
            selectItemsCorto = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Corto");
            selectItemsCorto.add(s);
            if (list != null) {
                for (Corto corto : list) {
                    SelectItem sel = new SelectItem(corto.getCortoId(), corto.getNombre());
                    selectItemsCorto.add(sel);
                }
            }
        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Corto:" + e.getMessage(), e);

        }
    }

    private void cargarItemsCos() {
        try {
            CosBUSSI qFlBL = GuiceInjectorSingleton.getInstance(CosBUSSI.class);
            List<Cos> list = qFlBL.obtenerLista();
            selectItemCos = "-1";
            selectItemsCos = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Cos");
            selectItemsCos.add(s);
            if (list != null) {
                for (Cos cos : list) {
                    SelectItem sel = new SelectItem(cos.getCosId(), cos.getNombre());
                    selectItemsCos.add(sel);
                }
            }
        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Cos:" + e.getMessage(), e);
        }

    }

    public void addLineaExtra() {
        if (!lineaExtra.equals("")) {
            ListaLineaExtra.add(lineaExtra);
        }
        lineaExtra = "";

    }

    public void removerLineaExtra(String linea) {
        ListaLineaExtra.remove(linea);
    }

    private List<String> parserToList(String lineas) {
        List<String> lista = new ArrayList<>();
        if (!lineas.equals("")) {
            String array[] = lineas.split(";");
            for (int i = 0; i < array.length; i++) {
                String aux = array[i];
                lista.add(aux);
            }
        }
        return lista;
    }

    private String parseToString(List<String> lista) {
        // String result = "";
        StringBuilder aux = new StringBuilder();
        for (int i = 0; i < lista.size(); i++) {
            String get = lista.get(i);
            if (i == lista.size() - 1) {
                //result = result + get;
                aux.append(get);
            } else {
                //result = result + get + ";";
                aux.append(get);
                aux.append(";");
            }
        }
        //return result;
        return aux.toString();
    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public String getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(String billeteraId) {
        this.billeteraId = billeteraId;
    }

    public List<Billetera> getBilleteraList() {
        return billeteraList;
    }

    public void setBilleteraList(List<Billetera> billeteraList) {
        this.billeteraList = billeteraList;
    }

    public String getSelItemUnitType() {
        return selItemUnitType;
    }

    public void setSelItemUnitType(String selItemUnitType) {
        this.selItemUnitType = selItemUnitType;
    }

    public List<SelectItem> getSelectItemsUnitType() {
        return selectItemsUnitType;
    }

    public void setSelectItemsUnitType(List<SelectItem> selectItemsUnitType) {
        this.selectItemsUnitType = selectItemsUnitType;
    }

    public List<SelectItem> getSelectItemsBillera() {
        return selectItemsBillera;
    }

    public void setSelectItemsBillera(List<SelectItem> selectItemsBillera) {
        this.selectItemsBillera = selectItemsBillera;
    }

    public String getSelectedItemBilletera() {
        return selectedItemBilletera;
    }

    public void setSelectedItemBilletera(String selectedItemBilletera) {
        this.selectedItemBilletera = selectedItemBilletera;
    }

    public String getTextInfo() {
        return TextInfo;
    }

    public void setTextInfo(String TextInfo) {
        this.TextInfo = TextInfo;
    }

    public List<ComposicionBilletera> getComposicionBilleteraList() {
        return composicionBilleteraList;
    }

    public void setComposicionBilleteraList(List<ComposicionBilletera> composicionBilleteraList) {
        this.composicionBilleteraList = composicionBilleteraList;
    }

    public List<SelectItem> getSelectItemsBilleraGrupo() {
        return selectItemsBilleraGrupo;
    }

    public void setSelectItemsBilleraGrupo(List<SelectItem> selectItemsBilleraGrupo) {
        this.selectItemsBilleraGrupo = selectItemsBilleraGrupo;
    }

    public String getSelectedItemBilleteraGrupo() {
        return selectedItemBilleteraGrupo;
    }

    public void setSelectedItemBilleteraGrupo(String selectedItemBilleteraGrupo) {
        this.selectedItemBilleteraGrupo = selectedItemBilleteraGrupo;
    }

    public Config getMyConfig() {
        return myConfig;
    }

    public void setMyConfig(Config myConfig) {
        this.myConfig = myConfig;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public List<SelectItem> getSelectItemsOrigen() {
        return selectItemsOrigen;
    }

    public void setSelectItemsOrigen(List<SelectItem> selectItemsOrigen) {
        this.selectItemsOrigen = selectItemsOrigen;
    }

    public String getSelectItemOrigen() {
        return selectItemOrigen;
    }

    public void setSelectItemOrigen(String selectItemOrigen) {
        this.selectItemOrigen = selectItemOrigen;
    }

    public List<SelectItem> getSelectItemsCorto() {
        return selectItemsCorto;
    }

    public void setSelectItemsCorto(List<SelectItem> selectItemsCorto) {
        this.selectItemsCorto = selectItemsCorto;
    }

    public String getSelectItemCorto() {
        return selectItemCorto;
    }

    public void setSelectItemCorto(String selectItemCorto) {
        this.selectItemCorto = selectItemCorto;
    }

    public List<SelectItem> getSelectItemsCos() {
        return selectItemsCos;
    }

    public void setSelectItemsCos(List<SelectItem> selectItemsCos) {
        this.selectItemsCos = selectItemsCos;
    }

    public String getSelectItemCos() {
        return selectItemCos;
    }

    public void setSelectItemCos(String selectItemCos) {
        this.selectItemCos = selectItemCos;
    }

    public List<OccView> getListOcc() {
        return listOcc;
    }

    public void setListOcc(List<OccView> listOcc) {
        this.listOcc = listOcc;
    }

    public Integer getSelectedOcc() {
        return selectedOcc;
    }

    public void setSelectedOcc(Integer selectedOcc) {
        this.selectedOcc = selectedOcc;
    }

    public List<String> getListaLineaExtra() {
        return ListaLineaExtra;
    }

    public void setListaLineaExtra(List<String> ListaLineaExtra) {
        this.ListaLineaExtra = ListaLineaExtra;
    }

    public String getLineaExtra() {
        return lineaExtra;
    }

    public void setLineaExtra(String lineaExtra) {
        this.lineaExtra = lineaExtra;
    }

    public String getMOSTRAR_SIEMPRE() {
        return MOSTRAR_SIEMPRE;
    }

}
