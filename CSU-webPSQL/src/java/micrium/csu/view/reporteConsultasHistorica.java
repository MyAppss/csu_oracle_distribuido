/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.configurable.util.UtilFecha;
import micrium.csu.bussines.ReporteConsultaBUSSI;
import micrium.csu.model.ReporteConsultaGroup;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.Parameters;
import micrium.csu.util.UtilExcel;
import micrium.csu.util.UtilTigoNumber;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author jose_luis
 */
@ManagedBean
@ViewScoped
public class reporteConsultasHistorica implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(reporteConsultas.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private String isdn;
    private Date fechaIni;
    private Date fechaFin;
    private List<ReporteConsultaGroup> listConsultaGroup;
    private String idUser;
    private String rowMaxTableCall = Parameters.NroFilasTableCallSac;

    private  StreamedContent descarga;

    public reporteConsultasHistorica() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
    }

    @PostConstruct
    public void init() {
        try {
            idUser = ControlerBitacora.getNombreUser();
            nuevaBusqueda();

        } catch (Exception e) {
            LOG.error("[init]|user=" + idUser + "| Error al iniciar bean: " + e.getMessage());
        }
    }

    /*public void setListConsultaGroup(List<ReporteConsultaGroup> listConsultaGroup) {
        this.listConsultaGroup = listConsultaGroup;
    }

    public List<ReporteConsultaGroup> getListConsultaGroup() {
        return listConsultaGroup;
    }*/
    public void nuevaBusqueda() {
        try {
            LOG.info("[nuevaBusqueda]|user=" + idUser + "|Reseteando para busqueda..");
            listConsultaGroup = new LinkedList<>();
            fechaIni = new Date();
            Calendar calIni = Calendar.getInstance();
            int diasRetraso = Integer.parseInt(Parameters.DiasRetraso);
            calIni.add(Calendar.DATE, -diasRetraso);

            calIni.set(Calendar.HOUR_OF_DAY, 0);
            calIni.set(Calendar.MINUTE, 0);
            calIni.set(Calendar.SECOND, 0);
            fechaIni.setTime(calIni.getTimeInMillis());

            fechaFin = new Date();
            fechaFin.setHours(23);
            fechaFin.setMinutes(59);
            isdn = "";

        } catch (NumberFormatException e) {
            LOG.error("[nuevaBusqueda]|user=" + idUser + "|Error al intentar resetear para nueva busqueda :" + e.getMessage());
        }

    }

    public String buscarByIsdn() {
        try {

            LOG.info("[buscarByIsdn]|user=" + idUser + "| Iniciando busqueda para msisdn=" + isdn);

            ReporteConsultaBUSSI reporteBUSSI = GuiceInjectorSingleton.getInstance(ReporteConsultaBUSSI.class);
            String str = Validar();
            if (str.isEmpty()) {
                if ((fechaIni != null) && (fechaFin != null)) {
                    LOG.info("[buscarByIsdn]|user=" + idUser + "|msisdn=" + isdn + "|Se ha definido busqueda por fechas");
                    //listResult = ep.obtenerListaByMsisdnDate(isdn, UtilDate.dateToTimestamp(fechaIni), UtilDate.dateToTimestamp(fechaFin));
                    listConsultaGroup = reporteBUSSI.obtenerListaByMsidnGroup(isdn, fechaIni, fechaFin);

                } else {//realizar busqueda sin fechas
                    //listResult = ep.obtenerListaByIsdn(isdn);
                    listConsultaGroup = reporteBUSSI.obtenerListaByMsidnGroup(isdn, fechaIni, fechaFin);
                }
                ControlerBitacora.insertBitacora(DescriptorBitacoraLC.REPORTE_CONSULTAS, "Realizo busqueda para " + isdn);
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error:", str));
            }
        } catch (Exception e) {
            LOG.error("[buscarByIsdn]|user=" + idUser + "|error al hacer busqueda: " + e.getMessage());
        }

        return "";
    }

//    private boolean cargarEnEncuestaInvitado(Invitado inv, List<EncuestaParticipante> listEP) {
//        for (EncuestaParticipante encuestaParticipante : listEP) {
//            if ((inv.getFechaSpam() != null) && (inv.getMsisdn().equals(encuestaParticipante.getMsisdn())) && (inv.getEncuestaId().equals(new Integer(encuestaParticipante.getEncuestaId())))) {
//                encuestaParticipante.setFechaEnvioSpam(inv.getFechaSpam());
//                return true;
//            }
//        }
//        return false;
//    }
    private String Validar() {
        try {
            if (isdn.isEmpty()) {
                return "El campo nro esta vacío";
            }
            if (!UtilTigoNumber.esNumeroTIGO(isdn)) {
                return "No es un número TIGO válido";
            }
            if ((fechaIni != null) && (fechaFin != null)) {
                if (fechaIni.after(fechaFin)) {
                    return "La fecha inicial NO debe ser mayor";
                }

                if (fechaIni.before(UtilFecha.smMeses(fechaFin, -Parameters.meses_Consulta_Historica))) {
                    return "El rango entre fechas no debe ser mayor a  " + Parameters.meses_Consulta_Historica + " meses ";

                }
            }

        } catch (Exception e) {
            LOG.error("[Validar]" + e.getMessage());
        }
        return "";
    }

//    public String formatTextoGenerado() {
//        String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txtcosulta");
//        if (value != null) {
//            String[] textSplit = value.split("|");
//            StringBuilder txtformat = new StringBuilder();
//            if (textSplit.length > 1) {
//                for (String string : textSplit) {
//                    txtformat.append(string).append("\n");
//                }
//                return txtformat.toString();
//            }
//        }
//        return value;
//    }
    public void donwloadReporteExcel() {
        try {
            if (listConsultaGroup != null) {
                byte[] bytes = UtilExcel.generarReporte(listConsultaGroup);
                if (bytes != null) {
                    InputStream stream = new ByteArrayInputStream(bytes);
                    descarga = new DefaultStreamedContent(stream, "application/xlsx",
                            "Consultas.xlsx");
                }
            }

        } catch (Exception e) {
            LOG.error("[donwloadReporteExcel] " + e.getMessage());
        }

    }

    public void donwloadReporteCsv() {
        try {
            if (listConsultaGroup != null) {
                byte[] bytes = UtilExcel.generarReporteCsv(listConsultaGroup);
                if (bytes != null) {
                    InputStream stream = new ByteArrayInputStream(bytes);
                    descarga = new DefaultStreamedContent(stream, "application/csv",
                            "Consultas.csv");
                }
            }
        } catch (Exception e) {
            LOG.error("[donwloadReporteCsv] " + e.getMessage());
        }

    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    /* public List<ReporteConsulta> getListResult() {
        return listResult;
    }

    public void setListResult(List<ReporteConsulta> listResult) {
        this.listResult = listResult;
    }*/
    public String getRowMaxTableCall() {
        return rowMaxTableCall;
    }

    public void setRowMaxTableCall(String rowMaxTableCall) {
        this.rowMaxTableCall = rowMaxTableCall;
    }

    public List<ReporteConsultaGroup> getListConsultaGroup() {
        return listConsultaGroup;
    }

    public void setListConsultaGroup(List<ReporteConsultaGroup> listConsultaGroup) {
        this.listConsultaGroup = listConsultaGroup;
    }

    public StreamedContent getDescarga() {
        return descarga;
    }

    public void setDescarga(StreamedContent descarga) {
        this.descarga = descarga;
    }

}
