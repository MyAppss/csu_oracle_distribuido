/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import micrium.csu.bussines.AcumuladorBUSSI;
import micrium.csu.bussines.BilleteraBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.bussines.UnitTypeBUSSI;
import micrium.csu.model.Acumulador;
import micrium.csu.model.Billetera;
import micrium.csu.model.UnitType;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import micrium.csu.util.RecargarServicios;
import micrium.csu.util.Validador;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author micrium
 */
@ManagedBean
@RequestScoped
public class walletForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Billetera billetera;
    private List<Billetera> billeteraList;
    private String billeteraId;
    private Boolean edit;
    private String idUser;
    private static final BilleteraBUSSI walletBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
    private static final Logger log = Logger.getLogger(walletForm.class);
    private static final String ARCHIVO_LOG4J = "log.properties";
    private List<SelectItem> selectItemsUnitType;

    private String selItemUnitType;
    private String selItemOperador;
    private String rowMaxTable = Parameters.NroFilasDataTable;
    private List<SelectItem> selectItemsAcumuladores;
    private String selectItemAcumulador;

    private List<Billetera> billeteraListFilter;

    /**
     * Creates a new instance of walletForm
     */
    public walletForm() {
        URL url = Thread.currentThread().getContextClassLoader().getResource(ARCHIVO_LOG4J);
        PropertyConfigurator.configure(url);
        idUser = ControlerBitacora.getNombreUser();
    }

    @PostConstruct
    public void init() {

        try {

            billetera = new Billetera();
            billeteraList = walletBL.obtenerLista();
            idUser = ControlerBitacora.getNombreUser();
            edit = false;
            cargarItemsUnit();
            cargarItemsAcumuladores();
            newWallet();

        } catch (Exception e) {
            log.error("[init]:" + e);
        }
    }

    public String saveWallet() {
        log.info("[saveWallet]User=" + idUser + "|Iniciando proceso para guardar/editar billetera");

        //String str = walletB.validate(role, roleId);
        //imprimir para pruebas-------------------------------------------------
        log.debug("[saveWallet]nombreCmV=" + billetera.getNombreComverse());
        log.debug("[saveWallet]nombreComercial=" + billetera.getNombreComercial());
        log.debug("[saveWallet]unitType=" + selItemUnitType);
        log.debug("[saveWallet]prefijoUnidad=" + billetera.getPrefijoUnidad());
        log.debug("[saveWallet]operador=" + selItemOperador);
        log.debug("[saveWallet]valor op=" + billetera.getValor());
        log.debug("[saveWallet]cant dec=" + billetera.getCantidadDecimales());
        log.debug("[saveWallet]Monto min=" + billetera.getMontoMinimo());
        log.debug("[saveWallet]Monto max=" + billetera.getMontoMaximo());
        log.debug("[saveWallet]alco=" + billetera.getAlco());
        log.debug("[saveWallet]acumuladorId=" + selectItemAcumulador);
        log.debug("[saveWallet]textoIlimitado=" + billetera.getTextoIlimintado());
        log.debug("[saveWallet]edit=" + edit);
        //----------------------------------------------------------------------

        billetera.setUnitTypeId(Integer.valueOf(selItemUnitType));

        if (selectItemAcumulador.equals("-1") || billetera.getAlco().equals("f") || billetera.getAlco().equals("false")) {
            billetera.setAcumuladorId(null);
        } else {
            billetera.setAcumuladorId(Integer.valueOf(selectItemAcumulador));
        }

        billetera.setOperador(selItemOperador);
        //billetera.setEstado(true);
        billetera.setEstado("t");
        if (billetera.getAlco().equals("false")) {
            billetera.setAlco("f");
        } else if (billetera.getAlco().equals("true")) {
            billetera.setAlco("t");
        }

        if (billetera.getAcumulado() == null || (billetera.getAcumulado() != null && billetera.getAcumulado().equals("false"))) {
            billetera.setAcumulado("f");
        } else {
            billetera.setAcumulado("t");
        }
        if (billetera.getReserva() == null || (billetera.getReserva() != null && billetera.getReserva().equals("false"))) {
            billetera.setReserva("f");
        } else {
            billetera.setReserva("t");
        }
        String str = validar(billetera.getNombreComverse());
        //log.debug("---------------------------------------------------2" + str);
        if (str.isEmpty()) {
            str = validarAll();
        }
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "", str));
            return "";
        }

        try {
            String msgView = "";
            boolean swError = false;
            if (!edit) {
                if (walletBL.insert(billetera)) {
                    ControlerBitacora.insert(DescriptorBitacoraLC.BILLETERA, "" + billetera.getBilleteraId(), billetera.getNombreComverse());
                    msgView = "Se guardó correctamente";
                    //actualizar variable de app
                    ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                    bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                    ReloadServicios();
                    
                } else {
                    msgView = "Error al intentar guardar";
                    swError = true;
                }

            } else {
                int id = Integer.parseInt(billeteraId);
                billetera.setBilleteraId(id);

                if (walletBL.update(billetera)) {
                    ControlerBitacora.update(DescriptorBitacoraLC.BILLETERA, "" + billetera.getBilleteraId(), billetera.getNombreComverse());
                    msgView = "Se modificó correctamente";
                } else {
                    msgView = "Error al intentar mofidicar";
                    swError = true;
                }
                //ControlerBitacora.update(DescriptorBitacoraLC.BILLETERA, wallet.getWalletId() + "", wallet.getName());
            }
            //actualizar variable de app----------------------------------------
            ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
            ReloadServicios();
            
            billeteraList = walletBL.obtenerLista();

            if (swError) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "", msgView));
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "", msgView));
            }
            newWallet();
            //cleanValues();
        } catch (Exception e) {
            log.error("[saveWallet]|user=" + idUser + "|Error al intentar guardar Billetera:" + e.getMessage());
        }
        return "";
    }
//    private void newWallet(){}

    public void editWallet() {
        edit = true;

        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("walletId");
            int id = Integer.parseInt(Idstr);

            log.debug("---------id: " + id + " ----------------------------------------------------------");

            billetera = walletBL.obtenerId(id);
            billeteraId = Idstr;
            selItemOperador = billetera.getOperador();
            selItemUnitType = String.valueOf(billetera.getUnitTypeId());

            if (billetera.getAcumuladorId() == null) {
                selectItemAcumulador = "-1";
            } else {
                selectItemAcumulador = billetera.getAcumuladorId().toString();
            }

            if (billetera.getAlco().equals("f")) {
                billetera.setAlco("false");
            } else if (billetera.getAlco().equals("t")) {
                billetera.setAlco("true");
            }
            if (billetera.getAcumulado() == null || (billetera.getAcumulado() != null && billetera.getAcumulado().equals("f"))) {
                billetera.setAcumulado("false");
            } else {
                billetera.setAcumulado("true");
            }
            if (billetera.getReserva() == null || (billetera.getReserva() != null && billetera.getReserva().equals("f"))) {
                billetera.setReserva("false");
            } else {
                billetera.setReserva("true");
            }

        } catch (NumberFormatException e) {
            log.error("[editWallet]|user=" + idUser + "|Error al intentar habilitar Edicion de cambios en Billetera: ", e);
        } catch (Exception e) {
            log.error("[editWallet]|user=" + idUser + "|Error al intentar habilitar Edicion de cambios en Billetera: ", e);
        }
    }

    public void deleteWallet(ActionEvent actionEvent) {
        try {
            String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("walletId");
            int id = Integer.parseInt(Idstr);
            log.debug("[deleteWallet]id billetera=" + Idstr);
            billetera = walletBL.obtenerId(id);
            if (walletBL.delete(id)) {
                //actualizar variable de app-----------------------------------
                ParametroBUSSI bl = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                bl.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
                ReloadServicios();

                ControlerBitacora.delete(DescriptorBitacoraLC.BILLETERA, Idstr, billetera.getNombreComverse());
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "", ParameterMessage.Billetera_MESSAGE_ELIMINAR));
            }
            //ControlerBitacora.delete(DescriptorBitacoraLC.BILLETERA, wallet.getWalletId() + "", wallet.getName());
            billeteraList = walletBL.obtenerLista();
            newWallet();

        } catch (Exception e) {
            log.error("[deleteWallet]|user=" + idUser + "Al intentar eliminar billetera:" + e.getMessage());

        }
    }

    private String validar(String palabra) {
        if (palabra.trim().equals("")) {
            return "El campo Nombre no es válido";
        }
        palabra = palabra.toUpperCase();

        try {
            if (!edit) {
                BilleteraBUSSI datoBL = GuiceInjectorSingleton.getInstance(BilleteraBUSSI.class);
                List<Billetera> lista = datoBL.obtenerLista();
                for (int i = 0; i < lista.size(); i++) {
                    Billetera dato = lista.get(i);
                    String str = dato.getNombreComverse().toUpperCase();
                    if (str.equals(palabra)) {
                        return "Ya existe una billetera con ese nombre";
                    }
                }
            }
        } catch (Exception e) {
            log.error("[validar]|user=" + idUser + " Error al validar: " + e.getMessage());
        }
        return "";
    }

    private String validarAll() {
        if (billetera != null) {
//            if (billetera.getNombreComercial().trim().isEmpty()) {
//                return "Campo Nombre Comercial no válido";
//            }
            if (!Validador.validateLength(billetera.getNombreComercial(), 0, 30)) {
                return "El campo Nombre Comercial es demasiado largo";
            }

            if (!Validador.validateLength(billetera.getNombreComverse(), 0, 30)) {
                return "El campo Nombre TSB es demasiado largo";
            }

            if (!Validador.validateLength(billetera.getTextoIlimintado(), 0, 30)) {
                return "El campo Texto Illimitado es demasiado largo";
            }

            if (!Validador.validateLength(billetera.getPrefijoUnidad(), 0, 30)) {
                return "El campo Prefijo Unidad es demasiado largo";
            }

            if (selItemUnitType.equals("-1")) {
                return "Debe seleccionar Unit Type";
            }
            if (billetera.getPrefijoUnidad().isEmpty()) {
                return "Campo Prefijo no válido";
            }
            if (selItemOperador.equals("DIVISION") || selItemOperador.equals("MULTIPLICACION")) {
                if (billetera.getValor() == 0) {
                    return "El campo Valor Operación debe ser mayor a cero";
                }
            }
            if (billetera.getMontoMinimo() >= billetera.getMontoMaximo()) {
                return "El campo Monto Máximo debe ser mayor al Monto Mínimo";
            }
            //if (billetera.getAlco() && selectItemAcumulador.equals("-1")) {
            if ((billetera.getAlco().equals("true") || billetera.getAlco().equals("t")) && selectItemAcumulador.equals("-1")) {
                return "Debe seleccionar un Acumulador valido";
            }
        }
        return "";
    }

    public void newWallet() {
        edit = false;
        billetera = new Billetera();
        selItemOperador = "NINGUNO";
        selItemUnitType = "-1";
        selectItemAcumulador = "-1";
        billetera.setMontoMaximo(99999.00);
        billetera.setMontoMinimo(0.0);
        billetera.setAcumulado("false");
        billeteraId = "";
        // cleanValues();
    }

    private void cargarItemsUnit() {
        try {
            UnitTypeBUSSI qFlBL = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            List<UnitType> list = qFlBL.obtenerLista();
            selItemUnitType = "-1";
            selectItemsUnitType = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Unit Type");
            selectItemsUnitType.add(s);
            if (list != null) {
                for (UnitType unitType : list) {
                    SelectItem sel = new SelectItem(unitType.getUnitTypeId(), unitType.getNombre());
                    selectItemsUnitType.add(sel);
                }
            }

        } catch (Exception e) {
            log.error("[cargarItemsUnit]Al intentar cargar lista de  UnitType:" + e.getMessage(), e);
        }

    }

    private void cargarItemsAcumuladores() {
        try {
            AcumuladorBUSSI qFlBL = GuiceInjectorSingleton.getInstance(AcumuladorBUSSI.class);
            List<Acumulador> list = qFlBL.obtenerLista();
            selectItemAcumulador = "-1";
            selectItemsAcumuladores = new LinkedList<SelectItem>();
            SelectItem s = new SelectItem("-1", "Seleccionar Acumulador");
            selectItemsAcumuladores.add(s);
            if (list != null) {
                for (Acumulador acumulador : list) {
                    SelectItem sel = new SelectItem(acumulador.getAcumuladorId(), acumulador.getNombre());
                    selectItemsAcumuladores.add(sel);
                }
            }
        } catch (Exception e) {
            log.error("[cargarItemsAcumuladores] Al intentar cargar lista de Acumuladores:" + e.getMessage(), e);
        }

    }

    public void ReloadServicios() {
        RecargarServicios rs = new RecargarServicios();
        rs.start();
    }

    public void getAcumuladores() {
        log.info("[getAcumuladores] Ingresando a obtener acumuladores, alco: " + billetera.getAlco());

        if (billetera.getAlco().equals("t")) {
            cargarItemsAcumuladores();
        }
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Billetera getBilletera() {
        return billetera;
    }

    public void setBilletera(Billetera billetera) {
        this.billetera = billetera;
    }

    public String getBilleteraId() {
        return billeteraId;
    }

    public void setBilleteraId(String billeteraId) {
        this.billeteraId = billeteraId;
    }

    public List<Billetera> getBilleteraList() {
        return billeteraList;
    }

    public void setBilleteraList(List<Billetera> billeteraList) {
        this.billeteraList = billeteraList;
    }

    public String getSelItemUnitType() {
        return selItemUnitType;
    }

    public void setSelItemUnitType(String selItemUnitType) {
        this.selItemUnitType = selItemUnitType;
    }

    public List<SelectItem> getSelectItemsUnitType() {
        return selectItemsUnitType;
    }

    public void setSelectItemsUnitType(List<SelectItem> selectItemsUnitType) {
        this.selectItemsUnitType = selectItemsUnitType;
    }

    public String getSelItemOperador() {
        return selItemOperador;
    }

    public void setSelItemOperador(String selItemOperador) {
        this.selItemOperador = selItemOperador;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

    public List<SelectItem> getSelectItemsAcumuladores() {
        return selectItemsAcumuladores;
    }

    public void setSelectItemsAcumuladores(List<SelectItem> selectItemsAcumuladores) {
        this.selectItemsAcumuladores = selectItemsAcumuladores;
    }

    public String getSelectItemAcumulador() {
        return selectItemAcumulador;
    }

    public void setSelectItemAcumulador(String selectItemAcumulador) {
        this.selectItemAcumulador = selectItemAcumulador;
    }

    public List<Billetera> getBilleteraListFilter() {
        return billeteraListFilter;
    }

    public void setBilleteraListFilter(List<Billetera> billeteraListFilter) {
        this.billeteraListFilter = billeteraListFilter;
    }

    /*public String cleanValues() {       
        
        billeteraListFilter = null;
        return "/view/WalletForm.xhtml";
    }*/
}
