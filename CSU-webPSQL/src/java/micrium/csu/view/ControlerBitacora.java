/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import com.myapps.controlmenu.bussines.BitacoraBL;
import com.myapps.controlmenu.model.Bitacora;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import javax.servlet.http.HttpServletRequest;
import javax.faces.context.FacesContext;
import micrium.csu.util.UtilDate;
/**
 *
 * @author jose_luis
 */
public class ControlerBitacora {

    private static final BitacoraBL   logBL = GuiceInjectorSingletonCM.getInstance(BitacoraBL.class); 
    
    public static void insert(Enum dato,String id,String name){
        
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        logBL.accionInsert(strIdUs, remoteAddr,  dato, id, name);
    
    }
    
    public static void update(Enum dato,String id,String name){
        
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        logBL.accionUpdate(strIdUs, remoteAddr,  dato, id, name);
        
    }
    
    public static void delete(Enum dato,String id,String name){
        
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        logBL.accionDelete(strIdUs, remoteAddr,  dato, id, name);
    
    }
    //=====================================================================
    public static String getNombreUser(){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        return "Usuario:"+strIdUs+"; Direccion:"+remoteAddr;
    } 
    public static void insertBitacora(Enum dato,String accion){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String strIdUs=(String)request.getSession().getAttribute("TEMP$USER_NAME");
        String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        Bitacora bit=new Bitacora();
        bit.setStartDate(UtilDate.getTimetampActual());
        bit.setAddress(remoteAddr);
        bit.setUser(strIdUs);
        bit.setForm(dato.name());
        bit.setAction(accion);
        logBL.insertLogWeb(bit);
    }
    
}
