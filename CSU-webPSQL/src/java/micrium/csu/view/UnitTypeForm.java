/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.UnitTypeBUSSI;
import micrium.csu.model.UnitType;
import micrium.csu.util.ParameterMessage;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.Parameters;
//import micrium.csu.util.DescriptorBitacora;
import org.apache.log4j.Logger;

/**
 *
 * @author jose_luis
 */

@ManagedBean
@ViewScoped
public class UnitTypeForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String unittypeId;
    private Boolean edit;
    private UnitType unittype;
    private List<UnitType> listUnitType;
    private String userId;
    private String rowMaxTable = Parameters.NroFilasDataTable;

    private static final Logger log = Logger.getLogger(UnitTypeForm.class);

    @PostConstruct
    public void init() {

        try {
            unittypeId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("unitTypeId");
            UnitTypeBUSSI bussi = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            listUnitType = bussi.obtenerLista();
            unittype = new UnitType();
            userId = ControlerBitacora.getNombreUser();
            edit = false;
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }

    public String saveUnitType() {
        log.info("[saveUnitType]userId=" + userId + "|Iniciando proceso...");
        try {
            UnitTypeBUSSI bussi = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            unittype.setEstado("t");
            //log.debug("---------------------------------------------------1");
            String str = bussi.validate(unittype, unittypeId);
            //log.debug("---------------------------------------------------2");
            if (!str.isEmpty()) {
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "", str));
                return "";
            }
            //log.debug("---------------------------------------------------3");
            if (!edit) {
                if (bussi.insert(unittype)) {
                    log.info("[saveUnitType]userId=" + userId + "|Inserto correctamente|IdUnitType=" + unittype.getUnitTypeId());
                    str = ParameterMessage.UnitType_MESSAGE_GUARDAR + unittype.getUnitTypeId();
                    ControlerBitacora.insert(DescriptorBitacoraLC.UNITTYPE, unittype.getUnitTypeId() + "", unittype.getNombre());
                } else {
                    log.warn("[saveUnitType]userId=" + userId + "|No inserto|");
                }
            } else {
                int id = Integer.parseInt(unittypeId);
                unittype.setUnitTypeId(id);
                bussi.update(unittype);
                str = ParameterMessage.UnitType_MESSAGE_MODIFICAR + unittypeId;
                ControlerBitacora.update(DescriptorBitacoraLC.UNITTYPE, unittype.getUnitTypeId() + "", unittype.getNombre());
            }
            listUnitType = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "", str));
            log.info("[saveUnitType]userId=" + userId + "|Finalizando proceso correctamente");
            newUnitType();
        } catch (Exception e) {
            log.error("[saveUnitType]userId=" + userId + "|Error al guardar. Error:" + e.getMessage());
        }
        return "";
    }

    public void editUnitType() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("unitTypeId");

        int id = Integer.parseInt(idStr);
        UnitTypeBUSSI bussi = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
        unittype = bussi.obtenerId(id);
        unittypeId = idStr;
        edit = true;
    }

    public void deleteUnitType() {

        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("unitTypeId");
            int id = Integer.parseInt(idStr);
            UnitTypeBUSSI bussi = GuiceInjectorSingleton.getInstance(UnitTypeBUSSI.class);
            String str = "";
            unittype = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.UnitType_MESSAGE_ELIMINAR + unittype.getUnitTypeId();
            // ControlerBitacora.delete(DescriptorBitacora.UNITTYPE, unittype.getUnitTypeId()+"",unittype.getNombre());
            ControlerBitacora.insert(DescriptorBitacoraLC.UNITTYPE, unittypeId + "", unittype.getNombre());
            listUnitType = bussi.obtenerLista();
            newUnitType();
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "", str));
        } catch (Exception e) {
            log.error("[deleteUnitType]Error al eliminar ", e);
        }
    }

    public void newUnitType() {
        edit = false;
        unittype = new UnitType();
    }

    /**
     * @return the unittypeId
     */
    public String getUnitTypeId() {
        return unittypeId;
    }

    /**
     * @param unittypeId the unittypeId to set
     */
    public void setUnitTypeId(String unittypeId) {
        this.unittypeId = unittypeId;
    }

    /**
     * @return the edit
     */
    public Boolean getEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the unittype
     */
    public UnitType getUnitType() {
        return unittype;
    }

    /**
     * @param unittype the unittype to set
     */
    public void setUnitType(UnitType unittype) {
        this.unittype = unittype;
    }

    /**
     * @return the listUnitType
     */
    public List<UnitType> getListUnitType() {
        return listUnitType;
    }

    /**
     * @param listUnitType the listUnitType to set
     */
    public void setListUnitType(List<UnitType> listUnitType) {
        this.listUnitType = listUnitType;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

}
