/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package micrium.csu.view;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import micrium.csu.bussines.NodoBUSSI;
import micrium.csu.bussines.ParametroBUSSI;
import micrium.csu.model.Nodo;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.DescriptorBitacoraLC;
import micrium.csu.util.ParameterMessage;
import micrium.csu.util.Parameters;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
@ManagedBean
@RequestScoped
public class NodoForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nodoId;
    private Boolean edit;
    private Nodo nodo;
    private List<Nodo> listNodo;
    private String rowMaxTable = Parameters.NroFilasDataTable;
//    private List<SelectItem> selectItemsConfig;
//    private String selectedItemConfig;
    private static final Logger log = Logger.getLogger(NodoForm.class);

    @PostConstruct
    public void init() {
        try {
            nodoId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_nodo");
            NodoBUSSI bussi = GuiceInjectorSingleton.getInstance(NodoBUSSI.class);
            //MapeoOfertasDAOImpl bussi= new MapeoOfertasDAOImpl();
            listNodo = bussi.obtenerLista();
            nodo = new Nodo();
            edit = false;
            //cargarItemsConfig();
        } catch (Exception e) {
            log.error("[init] Error: ", e);
        }
    }
//------------------------------------------------------------------------------

    public String saveNodo() {

        NodoBUSSI bussi = GuiceInjectorSingleton.getInstance(NodoBUSSI.class);

        nodo.setEstado("t");
        String str = bussi.validate(nodo, nodoId, edit);
        if (!str.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", str));
            return "";
        }
        try {
            if (!edit) {
                bussi.insert(nodo);
                str = ParameterMessage.MapeoOfertas_MESSAGE_GUARDAR;
                ControlerBitacora.insert(DescriptorBitacoraLC.NODO, nodo.getNodo_id() + "", nodo.getUrl());
            } else {
                int id = Integer.parseInt(nodoId);
                nodo.setNodo_id(id);
                bussi.update(nodo);
                str = ParameterMessage.MapeoOfertas_MESSAGE_MODIFICAR;
                //actualizar variable de app-----------------------------------
                //-------------------------------------------------------------------------------------------------
                ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
                blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");

                //-------------------------------------------------------------------------------------------------
                ControlerBitacora.update(DescriptorBitacoraLC.NODO, nodo.getNodo_id() + "", nodo.getUrl());
            }

            listNodo = bussi.obtenerLista();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion", str));
            newNodo();
     
        } catch (NumberFormatException e) {
            log.error("[saveAcumulador]Error al gurdar ", e);
        }
        return "";
    }

    /*  public void toInt() {

        if (nodo != null) {
            if (nodo.getLimite() < 0) {
                this.nodo.setLimite(this.nodo.getLimite() * -1);
            }
        }

    }*/
//------------------------------------------------------------------------------
    public void editNodo() {
        String idStr = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("id_nodo");

        int id = Integer.parseInt(idStr);
        NodoBUSSI bussi = GuiceInjectorSingleton.getInstance(NodoBUSSI.class);
        nodo = bussi.obtenerId(id);
        nodoId = idStr;
        //selectedItemConfig= String.valueOf(nodo.getConfigId());
        edit = true;
    }

    public void deleteNodo() {
        try {
            String idStr = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap().get("id_nodo");
            int id = Integer.parseInt(idStr);
            NodoBUSSI bussi = GuiceInjectorSingleton.getInstance(NodoBUSSI.class);
            String str = "";
            nodo = bussi.obtenerId(id);
            bussi.delete(id);
            str = ParameterMessage.Acumulador_MESSAGE_ELIMINAR;
            ControlerBitacora.delete(DescriptorBitacoraLC.NODO, nodo.getNodo_id() + "", nodo.getUrl());
            listNodo = bussi.obtenerLista();
            newNodo();
            //actualizar variable de app-----------------------------------
            //-------------------------------------------------------------------------------------------------
            ParametroBUSSI blpa = GuiceInjectorSingleton.getInstance(ParametroBUSSI.class);
            blpa.updateChangeConfiguration("sistema_consulta_saldo_sw_onchange");
         
            //-------------------------------------------------------------------------------------------------
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", str));
        } catch (NumberFormatException e) {
            log.error("[deleteAcumulador]Error al eliminar ", e);
        }
    }

    public void reloadServicios() {

        List<Nodo> listaNodos = listNodo;
        if (listaNodos != null) {
            listaNodos.forEach((nodo) -> {
                recargarNodo(nodo);
            });
        }

    }

    public boolean recargarNodo(Nodo nodo) {
        boolean valor = false;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(nodo.getUrl());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            // Send request
            // DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            // wr.writeBytes(urlParameters);
            // wr.close();
            // Get Response
            // InputStream inputStream = connection.getInputStream();
            try {
                inputStream = connection.getInputStream();
            } catch (FileNotFoundException e) {
                log.error("Error de conexion al servlet: " + nodo.getUrl() + " [ERROR] " + e.getMessage());
                //  throw (new Exception("Error de conexion al servlet: " + nodo.toString()));
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            log.info("respuesta del servlet " + nodo.getUrl() + ": " + response);
            valor = true;
            log.info("Nodo " + nodo.getUrl() + " Recargdo con Exito");
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Informacion", "Nodo " + nodo.getUrl() + " Recargado con Exito"));
        } catch (IOException e) {
            log.error("Error al hacer request al servlet: " + nodo.getUrl(), e);
            log.info("Nodo " + nodo.getUrl() + " Sin conexion");
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Informacion", "Nodo " + nodo.getUrl() + " Sin conexion"));
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    log.error("[recargarNodo] " + ex.getMessage());
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    log.error("[recargarNodo] " + ex.getMessage());
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return valor;

    }

    public void newNodo() {
        edit = false;
        nodo = new Nodo();
        //cargarItemsConfig();
    }

    public String getNodoId() {
        return nodoId;
    }

    public void setNodoId(String mapeoId) {
        this.nodoId = mapeoId;
    }

    public Boolean getEdit() {
        return edit;
    }

    public void setEdit(Boolean edit) {
        this.edit = edit;
    }

    public Nodo getNodo() {
        return nodo;
    }

    public void setNodo(Nodo nodo) {
        this.nodo = nodo;
    }

    public List<Nodo> getListNodo() {
        return listNodo;
    }

    public void setListNodo(List<Nodo> listNodo) {
        this.listNodo = listNodo;
    }

    public String getRowMaxTable() {
        return rowMaxTable;
    }

    public void setRowMaxTable(String rowMaxTable) {
        this.rowMaxTable = rowMaxTable;
    }

}
