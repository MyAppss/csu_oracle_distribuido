package micrium.csu.controler;

import com.myapps.controlmenu.controler.ControlLogin;
import com.myapps.controlmenu.controler.NodoMenu;
import com.myapps.controlmenu.util.GuiceInjectorSingletonCM;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import micrium.csu.bussines.RolPaginaIniBUSSI;
import micrium.csu.model.RolPaginaIni;
import micrium.csu.mybatis.GuiceInjectorSingleton;
import micrium.csu.util.ActiveDirectory;
import micrium.csu.util.Parameters;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;

/**
 *
 * @author Ivan
 */
@ManagedBean
@SessionScoped
public class Login implements Serializable {

    private String userSalud;
    private String userId;
    private String password;
    private String messageError;
    private MenuModel model;

    public Login() {

    }

    public String verifyLogin() throws UnknownHostException {

        ControlLogin controler = GuiceInjectorSingletonCM.getInstance(ControlLogin.class);

        String str = controler.validateIngreso(userId, password);
        if (userId.equals("") && password.equals("")) {
            str = "";
        }

        if (!str.isEmpty()) {
            messageError = str;
            return "";
        }

        int valid = (Parameters.swActive.equals("true")) ? ActiveDirectory.existUser(userId, password)
                : controler.existUser(userId, password);

        if (valid == ActiveDirectory.EXIT_USER) {
            List groups = Parameters.swActive.equals("true") ? ActiveDirectory.getGroups(userId)
                    : controler.getListaGrupo();

            if (groups.size() > 0) {
                int idRol = controler.getIdRole(userId, groups);

                if (idRol > 0) {
                    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                    request.getSession().setAttribute("TEMP$USER_NAME", userId);
                    String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
                    controler.addBitacoraLogin(userId, remoteAddr, idRol);
                    userSalud = "Usuario: " + userId;
                    cargar(idRol);
                    //------recuperar pageInicio-----------------
                    RolPaginaIniBUSSI RBL = GuiceInjectorSingleton.getInstance(RolPaginaIniBUSSI.class);
                    RolPaginaIni rpi = RBL.obtenerId(idRol);
                    regenerateSession(request);

                    return rpi.getPaginaInicio() + "?faces-redirect=true";

                } else {
                    messageError = "Usted no esta registrado para ningun Rol";
                    return "";
                }

            } else {
                messageError = "Usted no tiene grupo de trabajo";
                return "";
            }
        } else {
            str = controler.controlError(userId);
            messageError = str;
            return "";
        }
    }

    private void regenerateSession(HttpServletRequest request) {

        HttpSession oldSession = request.getSession();

        @SuppressWarnings("rawtypes")
        java.util.Enumeration attrNames = oldSession.getAttributeNames();
        java.util.Properties props = new java.util.Properties();

        if (attrNames != null) {
            while (attrNames.hasMoreElements()) {
                String key = (String) attrNames.nextElement();
                props.put(key, oldSession.getAttribute(key));
            }
            // Invalidating previous session
            oldSession.invalidate();
            // Generate new session
            HttpSession newSession = request.getSession(true);
            attrNames = props.keys();

            while (attrNames.hasMoreElements()) {
                String key = (String) attrNames.nextElement();
                newSession.setAttribute(key, props.get(key));
            }
        }
    }

    private void cargar(int idRole) {
        ControlLogin controler = GuiceInjectorSingletonCM.getInstance(ControlLogin.class);
        model = new DefaultMenuModel();

        List<NodoMenu> listaNM = controler.getListaMenu(idRole);
        Map<String, Submenu> mapTreeNode = new HashMap<>();
        for (NodoMenu nodoMenu : listaNM) {

            int tipo = nodoMenu.getTipo();

            if (tipo == ControlLogin.MENU_MENU) {
                Submenu submenu = new Submenu();
                submenu.setLabel(nodoMenu.getName());
                submenu.setRendered(nodoMenu.isSwRendered());
                submenu.setTransient(true);
                mapTreeNode.put(nodoMenu.getName(), submenu);
                model.addSubmenu(submenu);

            } else {
                String path = nodoMenu.getPather();
                Submenu submenuF = mapTreeNode.get(path);
                if (tipo == ControlLogin.MENU_RAIZ) {
                    Submenu submenu = new Submenu();
                    submenu.setLabel(nodoMenu.getName());
                    submenu.setRendered(nodoMenu.isSwRendered());
                    submenu.setTransient(true);
                    mapTreeNode.put(nodoMenu.getName(), submenu);
                    submenuF.getChildren().add(submenu);
                } else {
                    MenuItem item = new MenuItem();
                    item.setValue(nodoMenu.getName());
                    item.setUrl(nodoMenu.getUrl());
                    item.setRendered(nodoMenu.isSwRendered());
                    item.setTransient(true);
                    submenuF.getChildren().add(item);

                }
            }
        }

        MenuItem item = new MenuItem();
        //item = new MenuItem();
        item.setValue("Cerrar Sesión");
        item.setIcon("ui-icon ui-icon-close");
        item.setUrl("/Logout");
        item.setTransient(true);
        model.addMenuItem(item);

        //model.getContents().add(new Separator());
        item = new MenuItem();
        item.setValue(userSalud);
        item.setTransient(true);
        model.addMenuItem(item);

    }

    public MenuModel getModel() {
        return model;
    }

    /**
     * @return the username
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId as String
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userSalud
     */
    public String getUserSalud() {
        return userSalud;
    }

    /**
     * @param userSalud the userSalud to set
     */
    public void setUserSalud(String userSalud) {
        this.userSalud = userSalud;
    }

    /**
     * @return the messageError
     */
    public String getMessageError() {
        return messageError;
    }

}
