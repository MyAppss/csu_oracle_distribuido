/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * @author GENOSAURER
 */
public class Controlador implements Serializable {
    private static final Logger log = Logger.getLogger(Controlador.class);
    private static final long serialVersionUID = 1L;

    private boolean valor;
    private static Controlador instance = null;

    private Controlador() {
    }

    public static Controlador getInstance() {
        if (instance == null) {
            log.info("Se creo la primera Instancia");
            instance = new Controlador();
        }
        return instance;
    }

    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        log.info("Cambiando nuevo valor: " + valor);
        this.valor = valor;
    }

}
