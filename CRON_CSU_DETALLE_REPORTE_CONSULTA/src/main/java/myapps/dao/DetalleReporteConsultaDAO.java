package myapps.dao;

import myapps.model.DetalleReporteConsultaEntity;

public interface DetalleReporteConsultaDAO {
    void guardarDetalleConsulta(DetalleReporteConsultaEntity detalleReporteConsultaEntity, String hilo);
}