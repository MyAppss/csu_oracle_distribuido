package myapps.dao;

import myapps.conexion.DatasourceConexion;
import myapps.model.DetalleReporteConsultaEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DetalleDetalleReporteConsultaDAOImplement implements DetalleReporteConsultaDAO {
    private static final Logger logger = LogManager.getLogger(DetalleDetalleReporteConsultaDAOImplement.class);


    public void guardarDetalleConsulta(DetalleReporteConsultaEntity detalleReporteConsultaEntity, String hilo) {
        String identificador = "Identificador: " + hilo;
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            String sql = "INSERT INTO DETALLE_REPORTE_CONSULTA (MSISDN,SESSIONID,OPCION,FECHA) VALUES (?,?,?,?)";
            logger.debug(identificador + " [guardarConsulta] SQL: " + sql);
            conn = DatasourceConexion.getConnection();
            if (conn != null) {
                pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, detalleReporteConsultaEntity.getMsisdn());
                pstmt.setString(2, detalleReporteConsultaEntity.getSessionid());
                pstmt.setString(3, detalleReporteConsultaEntity.getOpcion());
                pstmt.setTimestamp(4, detalleReporteConsultaEntity.getFecha());
                pstmt.execute();
                logger.info(identificador + " [guardarConsulta] Guardado el Objeto " + detalleReporteConsultaEntity.toString());

            }
        } catch (SQLException e) {
            logger.info(identificador + " [guardarConsulta] No se pudo guardar el Objeto " + detalleReporteConsultaEntity.toString());
            logger.error(identificador + " [guardarConsulta] Error al insertar en la DB SQLException:" + e.getMessage() + "-" + detalleReporteConsultaEntity.toString());
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ex) {
                    logger.warn(identificador + " [guardarConsulta] Error al intentar cerrar ", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.warn(identificador + " [guardarConsulta] Error al intentar cerrar:", ex);
                }
            }
        }

    }


}
