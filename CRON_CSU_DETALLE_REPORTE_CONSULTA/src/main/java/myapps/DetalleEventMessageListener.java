package myapps;


import myapps.cmn.vo.ReporteConsulta;
import myapps.dao.DetalleReporteConsultaDAO;
import myapps.model.DetalleReporteConsultaEntity;
import myapps.util.Parametros;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.jms.*;
import java.util.HashMap;

public class DetalleEventMessageListener implements Runnable {
    public static final Logger logger = Logger.getLogger(DetalleEventMessageListener.class);
    private Session session;
    private MessageConsumer consumer;
    private Thread worker;
    private Connection connection;
    private DetalleReporteConsultaDAO dao;
    private String identificador;
    private int maximumRedeliveries = 50;
    private static final String JMSEXCEPTION = " JMSException: ";
    private static final String KEY = "Identificador: ";

    public DetalleEventMessageListener(ThreadGroup group, String identificador, DetalleReporteConsultaDAO detalleReporteConsultaDAO) {
        this.dao = detalleReporteConsultaDAO;
        this.identificador = identificador;
        worker = new Thread(group, this, identificador);
        crearConexion();
    }

    public void iniciar() {
        try {
            connection.start();
            worker.start();
        } catch (JMSException e) {
            logger.error(KEY + identificador + JMSEXCEPTION + e.getMessage(), e);
        }
    }

    public void stop() {
        if (worker != null) worker.stop();
    }

    public void onMessage(Message message) {
        if (message != null) {
            logger.log(Level.INFO, KEY + identificador + " onMessage:" + message);
            long deliveryCount = 0;
            try {
                logger.log(Level.DEBUG, KEY + identificador + " deliveryCount: " + message.getStringProperty("JMSXDeliveryCount"));
                deliveryCount = Long.valueOf(message.getStringProperty("JMSXDeliveryCount"));
            } catch (JMSException e) {
                logger.error(JMSEXCEPTION + e.getMessage(), e);
            }

            if (message instanceof ObjectMessage) {
                ObjectMessage objectMessage = (ObjectMessage) message;
                try {
                    logger.log(Level.INFO, "onMessage data:");
                    HashMap<String, Object> map = (HashMap<String, Object>) objectMessage.getObject();
                    Object object = map.get("DETALLE_CONSULTA");
                    if (object instanceof ReporteConsulta) {
                        ReporteConsulta reporteConsulta = (ReporteConsulta) object;
                        DetalleReporteConsultaEntity entity = new DetalleReporteConsultaEntity();
                        entity.setFecha(reporteConsulta.getFecha());
                        entity.setMsisdn(reporteConsulta.getMsisdn());
                        entity.setSessionid(reporteConsulta.getSessionId());
                        entity.setOpcion(reporteConsulta.getOpcion());
                        dao.guardarDetalleConsulta(entity, identificador);
                        // session.commit();
                        logger.log(Level.INFO, KEY + identificador + " OK:");
                    } else {
                        logger.error(KEY + identificador + " El Objeto es Invalido: " + object.toString());
                    }
                } catch (JMSException e) {
                    logger.error(KEY + identificador + JMSEXCEPTION + e.getMessage(), e);
                    try {
                        if (deliveryCount == maximumRedeliveries) {
                            logger.warn(KEY + identificador + " Se llego al limite " + deliveryCount);
                        }
                        session.rollback();
                    } catch (JMSException e1) {
                        logger.error(KEY + identificador + JMSEXCEPTION + e1.getMessage(), e1);
                    }


                } catch (Exception e) {
                    logger.error(KEY + identificador + " Exception: " + e.getMessage(), e);
                } finally {
                    logger.log(Level.DEBUG, KEY + identificador + " FINISH ITERATION");

                }
            } else {
                try {
                    session.rollback();
                } catch (JMSException e) {
                    logger.error(KEY + identificador + JMSEXCEPTION + e.getMessage(), e);
                }
            }
        }
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException e1) {
                logger.error(KEY + identificador + JMSEXCEPTION + e1.getMessage(), e1);
            }
        }
        if (session != null) {
            try {
                session.close();
            } catch (JMSException e1) {
                logger.error(KEY + identificador + JMSEXCEPTION + e1.getMessage(), e1);
            }
        }
        if (consumer != null) {
            try {
                consumer.close();
            } catch (JMSException e1) {
                logger.error(KEY + identificador + JMSEXCEPTION + e1.getMessage(), e1);
            }
        }
    }

    @Override
    public void run() {
        logger.info(KEY + identificador + " " + worker.getName() + " is running");
        while (Boolean.TRUE.equals(Controlador.getInstance().isValor())) {
            //   while (!Thread.currentThread().isInterrupted()) {
            try {
                onMessage(consumer.receive(5000));
            } catch (JMSException e) {
                logger.error(KEY + identificador + JMSEXCEPTION + e.getMessage(), e);
            } catch (Exception e) {
                logger.error(KEY + identificador + "Exception: " + e.getMessage(), e);
            }
        }
        close();
        logger.info(KEY + identificador + " " + worker.getName() + " is finish");
    }


    private void crearConexion() {
        try {
            connection = new ActiveMQConnectionFactory(Parametros.MQ_USER, Parametros.MQ_PASSWORD, Parametros.MQ_TIPO_CONEXION + Parametros.MQ_IP + ":" + Parametros.MQ_PUERTO + "?wireFormat.maxInactivityDuration=0").createConnection();
            this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            //CREATE JMS SESSION PRUDUCER AND CONSUMER
            this.consumer = session.createConsumer(session.createQueue(Parametros.MQ_COLA));
            logger.info(KEY + identificador + " Conectado a " + Parametros.MQ_TIPO_CONEXION + Parametros.MQ_IP + ":" + Parametros.MQ_PUERTO);
        } catch (JMSException e) {
            logger.error(KEY + identificador + JMSEXCEPTION + e.getMessage(), e);
            close();
        } catch (Exception e) {
            logger.error(KEY + identificador + " Exception: " + e.getMessage(), e);
            close();
        }
    }

    private void esperar(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            logger.error(KEY + identificador + " Exception: " + e.getMessage(), e);

        }
    }
}
