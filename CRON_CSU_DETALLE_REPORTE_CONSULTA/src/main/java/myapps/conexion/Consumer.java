package myapps.conexion;

import myapps.util.Parametros;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import javax.jms.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Consumer {
    private static final Logger log = Logger.getLogger(Consumer.class);


    public List<HashMap<String, Object>> obtenerMensajes() {
        List<HashMap<String, Object>> mapList = new ArrayList<>();
        Connection connection = null;
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Parametros.MQ_USER, Parametros.MQ_PASSWORD, Parametros.MQ_TIPO_CONEXION + Parametros.MQ_IP + ":" + Parametros.MQ_PUERTO);
            connection = connectionFactory.createConnection();
            connection.start();

            final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            final Destination destination = session.createQueue(Parametros.MQ_COLA);
            final MessageConsumer consumer = session.createConsumer(destination);
            List<Message> messageList = new ArrayList<>();
            Message message;
            while ((message = consumer.receive(Parametros.MQ_TIMEOUT)) != null) {
                messageList.add(message);
            }
            consumer.close();
            session.close();


            for (Message mesage : messageList) {
                if (mesage instanceof ObjectMessage) {
                    ObjectMessage objMsg = (ObjectMessage) mesage;
                    mapList.add((HashMap<String, Object>) objMsg.getObject());
                }
            }

            long z = System.currentTimeMillis();

            log.info("Finalizo de Obtener datos de  la Cola en " + (z - System.currentTimeMillis()) / 1000 + " ms");
        } catch (JMSException e) {

            log.error("Error" + e.getErrorCode());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    log.error("Error al Cerrar la conexion a la Cola");
                }
            }

        }

        return mapList;
    }


}