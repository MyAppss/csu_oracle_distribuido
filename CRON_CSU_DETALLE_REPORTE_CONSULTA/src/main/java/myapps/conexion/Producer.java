package myapps.conexion;

import myapps.util.Parametros;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import javax.jms.*;
import java.util.HashMap;

public class Producer {
    private static final Logger log = Logger.getLogger(Producer.class);


    public void sendMessages(HashMap<String, Object> mensaje) {
        Connection connection = null;
        try {
            final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Parametros.MQ_USER, Parametros.MQ_PASSWORD, Parametros.MQ_IP);
            connection = connectionFactory.createConnection();
            connection.start();

            final Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            final Destination destination = session.createQueue(Parametros.MQ_COLA);

            final MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            sendMessage(mensaje, session, producer);
            session.commit();
            session.close();

            log.info("Mensaje enviado correctamentea la Cola");
        } catch (JMSException e) {
            log.error("Error al enviar al Mensaje a la Cola " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    log.error("Error al cerrar la Conexion " + e.getMessage());
                }
            }

        }


    }


    private void sendMessage(HashMap<String, Object> message, Session session, MessageProducer producer) throws JMSException {
        ObjectMessage mensaje = session.createObjectMessage(message);
        producer.send(mensaje);
    }


}
