package myapps.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UtilJava {

    private static final Logger log = Logger.getLogger(UtilJava.class);
    private static final String GMT = "GMT-4";



    public static void esperar(int tiempo) {
        try {
            log.info("Esperando " + tiempo + " ms ");
            Thread.sleep(tiempo);
        } catch (Exception e) {
            log.error("[esperar] " + e.getMessage());
        }
    }

    @SuppressWarnings("rawtypes")
    public static void printMap(Map<String, String> mp) {
        try {
            Iterator it = mp.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if (!pair.getKey().toString().contains("PASSWORD")) {
                    log.info(pair.getKey() + " = " + pair.getValue());
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    public static String reemplazarCaracter(String cadena) {
        if (cadena != null) {
            cadena = cadena.replace(";", ",");
        }
        return cadena;
    }

    public static String formatearFecha(Date date) {
        String fecha = "";
        if (date != null) {
            SimpleDateFormat formateador = new SimpleDateFormat("dd'/'MMMM'/'yyyy", new Locale("ES"));
            formateador.setTimeZone(TimeZone.getTimeZone(GMT));
            fecha = formateador.format(date);
        }
        return fecha;
    }

    public static String formatearPeriodo(Date date) {
        String fecha = "";
        if (date != null) {
            SimpleDateFormat formateador = new SimpleDateFormat("MMMM'/'yyyy", new Locale("ES"));
            formateador.setTimeZone(TimeZone.getTimeZone(GMT));
            fecha = formateador.format(date);
        }
        return fecha;
    }

    public static String convertirMoneda(BigDecimal monto) {
        String montofinal = "";
        if (monto != null) {
            Locale local = new Locale("BO");
            NumberFormat nf = NumberFormat.getInstance(local);
            montofinal = nf.format(monto).replace("B$", "");
        }
        return montofinal;
    }

    public static String obtenerColor(int numero) {
        String color = "";
        if (numero >= 0 && numero <= 50) {
            color = "#009933";
        }
        if (numero >= 51 && numero <= 90) {
            color = "#F5B041";
        }
        if (numero >= 91) {
            color = "#ff0000";
        }
        return color;
    }




    public static Date smDias(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(GMT));
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static Date smMeses(Date fecha, int meses) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.MONTH, meses);
        return calendar.getTime();
    }

    public static Date smYear(Date fecha, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.YEAR, year);
        return calendar.getTime();
    }

    public static int[] ordSelDesc(int[] arreglo) {
        for (int i = 0; i < arreglo.length - 1; i++) {
            int max = i;
            for (int j = i + 1; j < arreglo.length; j++) {
                if (arreglo[j] > arreglo[max]) {
                    max = j;
                }
            }
            if (i != max) {
                int aux = arreglo[i];
                arreglo[i] = arreglo[max];
                arreglo[max] = aux;
            }
        }
        return arreglo;
    }

    public static Long numeroDiasEntreDosFechas(Date fecha1, Date fecha2) {
        return ((fecha2.getTime() - fecha1.getTime()) / 86400000);
    }



    public static Date obtenerFecha(Date fecha) {
        Date newFecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone(GMT));
        try {
            newFecha = dateFormat.parse(dateFormat.format(fecha));
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return newFecha;
    }




    public static long convertToLong(Object object) {
        try {
            BigDecimal valor = (BigDecimal) object;
            return valor.longValue();
        } catch (NullPointerException e) {
            log.log(Level.ERROR, "Existen valores nulos al realizar la conversion" + "|" + e.getMessage(), e);
        }
        return 0;
    }

    public static double convertToDoublr(Object object) {
        try {
            BigDecimal valor = (BigDecimal) object;
            return valor.doubleValue();
        } catch (NullPointerException e) {
            log.log(Level.ERROR, "Existen valores nulos al realizar la conversion" + "|" + e.getMessage(), e);
        }
        return 0;
    }


    public static boolean probarConexion(String url, int puerto) {
        boolean valor = false;
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(url, puerto), Parametros.MQ_TIMEOUT);
            valor = socket.isConnected();
            socket.close();
        } catch (IOException e) {
            log.error("[probarConexion] No se pudo establecer conexion hacia " + url + ":" + puerto);
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                log.error("[probarConexion] Error al cerrar sSocket");
            }
        }

        return valor;
    }

}
