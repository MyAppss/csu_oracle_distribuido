/*
 Navicat Premium Data Transfer

 Source Server         : ADMIN
 Source Server Type    : Oracle
 Source Server Version : 100200
 Source Host           : localhost:1521
 Source Schema         : CSU_DISTRIBUIDO

 Target Server Type    : Oracle
 Target Server Version : 100200
 File Encoding         : 65001

 Date: 18/02/2020 11:09:06
*/


-- ----------------------------
-- Table structure for formulario
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."formulario";
CREATE TABLE "CSU_DISTRIBUIDO"."formulario" (
  "formulario_id" NUMBER(11) NOT NULL ,
  "nombre" NVARCHAR2(50) ,
  "url" NVARCHAR2(50) ,
  "estado" VARCHAR2(5 CHAR) ,
  "depende" NUMBER(11) ,
  "nivel" NVARCHAR2(50) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of formulario
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('1', 'Administración', NULL, 't', '0', '1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('2', 'Configuraciones', '  ', 't', '0', '2');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('3', 'Publicidad', NULL, 't', '0', '3');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('4', 'Parametrización', NULL, 't', '0', '4');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('5', 'Call Center/SAC', NULL, 't', '0', '5');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('6', 'Ayuda', NULL, 't', '0', '6');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('10', 'rol', 'Role.xhtml', 't', '0', '1.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('11', 'Usuarios', 'RoleUser.xhtml', 't', '0', '1.2');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('12', 'Grupos', 'RoleGroup.xhtml', 't', '0', '1.3');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('13', 'Bitacora', 'Bitacora.xhtml', 't', '0', '1.4');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('14', 'Rol_Formulario', 'RolFormulario.xhtml', 't', '10', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('21', 'Configuraciones para Billeteras', 'listConfigForm.xhtml', 't', '0', '2.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('22', 'Gestionar Configuracion Billetera', 'configForm.xhtml', 't', '21', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('31', 'Segmento Clasificador', 'SorterForm.xhtml', 't', '0', '3.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('32', 'Valores de Segmento', 'SorterValueForm.xhtml', 't', '0', '3.2');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('33', 'Mensajes', 'MessageForm.xhtml', 't', '0', '3.3');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('34', 'Campaña Segmentada', 'CampaignList.xhtml', 't', '0', '3.4');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('35', 'Gestionar Campaña', 'CampaignForm.xhtml', 't', '34', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('36', 'Opciones(Publicidad)', 'OptionPublicidad.xhtml', 't', '0', '3.6');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('41', 'Unit Types', 'UnitType.xhtml', 't', '0', '4.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('42', 'Billeteras Simples', 'WalletForm.xhtml', 't', '0', '4.2');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('43', 'Billeteras Agrupadas', 'listBilleterasAgrupadasForm.xhtml', 't', '0', '4.3');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('44', 'Gestionar Billetera Agrupada', 'BilleteraAgrupadaForm.xhtml', 't', '43', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('45', 'COS', 'Cos.xhtml', 't', '0', '4.5');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('46', 'DPI', 'OptionsDPI.xhtml', 't', '0', '4.6');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('47', 'Opciones Generales', 'OptionsDefault.xhtml', 't', '0', '4.7');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('48', 'Acumulador', 'Acumulador.xhtml', 't', '0', '4.8');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('49', 'Origen', 'Origen.xhtml', 't', '0', '4.9');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('50', 'Corto', 'Corto.xhtml', 't', '0', '4.90');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('51', 'Consultas de Saldo Realizadas', 'ReporteConsultas.xhtml', 't', '0', '5.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('61', 'Manual de Administrador', '../resources/templates/MU_CSU.pdf', 't', '0', '6.1');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('62', 'Manual SAC/Call Center', '../resources/templates/MU_CALL_SAC.pdf', 't', '0', '6.2');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('70', 'Acumulados Megas', 'AcumuladosMegas.xhtml', 't', '0', '4.91');
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('74', 'Configuracion Menu', 'configMenu.xhtml', 't', '21', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('73', 'Configuracion Cabeceras', 'configCabecera.xhtml', 't', '21', NULL);
INSERT INTO "CSU_DISTRIBUIDO"."formulario" VALUES ('75', 'Confirugacion de acumulados', 'configAcumuladoForm.xhtml', 't', '21', NULL);

-- ----------------------------
-- Table structure for grupo_ad
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."grupo_ad";
CREATE TABLE "CSU_DISTRIBUIDO"."grupo_ad" (
  "grupo_id" NUMBER(11) NOT NULL ,
  "rol_id" NUMBER(11) ,
  "nombre" NVARCHAR2(30) ,
  "detalle" NVARCHAR2(40) ,
  "estado" VARCHAR2(5 CHAR) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of grupo_ad
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('2', '4', 'Back Office Operator', 'Back Office Operator', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('3', '4', 'BackOffice_fdz', 'BackOffice_fdz', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('4', '4', 'Backoffice Nasis', 'Backoffice Nasis', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('5', '4', 'CALL CENTER NASIS', 'CALL CENTER NASIS', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('6', '1', 'VAP Support', 'VAP Support', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('7', '4', 'CALL CENTER', 'CALL CENTER', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('8', '4', 'CALL_NASIS-proxy', 'CALL_NASIS-proxy', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('9', '4', 'SAC-proxy', 'SAC-proxy', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('10', '4', 'SAC Nacional', 'SAC Nacional', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('11', '4', 'Call_DPI', 'Call_DPI', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('12', '4', 'Call Banda Ancha', 'Call Banda Ancha', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('13', '4', 'Contact Center Pre Pago', 'Contact Center Pre Pago', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('14', '4', 'CORPORATE-proxy', 'CORPORATE-proxy', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('15', '4', 'Comunicados Comerciales', 'Comunicados Comerciales', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('16', '4', 'CAM', 'CAM', 't');
INSERT INTO "CSU_DISTRIBUIDO"."grupo_ad" VALUES ('18', '17', 'Soporte', 'Soporte', 'f');

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."rol";
CREATE TABLE "CSU_DISTRIBUIDO"."rol" (
  "rol_id" NUMBER(11) NOT NULL ,
  "nombre" NVARCHAR2(30) ,
  "descripcion" NVARCHAR2(40) ,
  "estado" VARCHAR2(5 CHAR) ,
  "pagina_inicio" NVARCHAR2(100) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."rol" VALUES ('16', 'Reporte', 'Reporte', 't', '/view/Menu.xhtml');
INSERT INTO "CSU_DISTRIBUIDO"."rol" VALUES ('1', 'Administracion', 'De toda la administracion', 't', '/view/listConfigForm.xhtml');
INSERT INTO "CSU_DISTRIBUIDO"."rol" VALUES ('4', 'Call center', 'Call center', 't', '/view/ReporteConsultas.xhtml');
INSERT INTO "CSU_DISTRIBUIDO"."rol" VALUES ('5', 'Parametrizador', 'Parametrizador', 't', '/view/listConfigForm.xhtml');
INSERT INTO "CSU_DISTRIBUIDO"."rol" VALUES ('17', 'Soporte', 'Soporte', 't', '/view/Menu.xhtml');

-- ----------------------------
-- Table structure for rol_formulario
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."rol_formulario";
CREATE TABLE "CSU_DISTRIBUIDO"."rol_formulario" (
  "rol_id" NUMBER(11) NOT NULL ,
  "formulario_id" NUMBER(11) NOT NULL ,
  "estado" VARCHAR2(5 CHAR) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of rol_formulario
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '73', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '74', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '75', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '73', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '74', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '75', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '1', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '2', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '3', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '4', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '5', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '6', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '10', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '11', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '12', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '13', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '14', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '21', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '22', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '31', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '32', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '33', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '34', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '35', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '36', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '41', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '42', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '43', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '44', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '45', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '46', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '47', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '48', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '49', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '50', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '51', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '61', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '62', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('1', '70', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '1', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '2', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '3', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '4', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '5', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '6', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '10', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '11', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '12', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '13', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '14', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '21', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '22', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '31', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '32', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '33', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '34', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '35', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '36', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '41', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '42', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '43', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '44', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '45', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '46', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '47', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '51', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '61', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('4', '62', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '1', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '2', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '3', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '4', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '5', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '6', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '10', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '11', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '12', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '13', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '14', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '21', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '22', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '31', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '32', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '33', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '34', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '35', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '36', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '41', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '42', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '43', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '44', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '45', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '46', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '47', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '51', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '61', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('5', '62', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '1', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '2', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '3', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '4', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '5', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '6', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '10', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '11', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '12', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '13', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '14', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '21', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '22', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '31', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '32', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '33', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '34', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '35', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '36', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '41', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '42', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '43', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '44', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '45', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '46', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '47', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '48', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '49', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '50', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '51', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '61', 't');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '62', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."rol_formulario" VALUES ('17', '70', 't');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE "CSU_DISTRIBUIDO"."usuario";
CREATE TABLE "CSU_DISTRIBUIDO"."usuario" (
  "usuario_id" NUMBER(11) NOT NULL ,
  "rol_id" NUMBER(11) ,
  "login" NVARCHAR2(40) ,
  "nombre" NVARCHAR2(50) ,
  "estado" VARCHAR2(5 CHAR) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('282', '5', 'uzquianoca', 'Carlos Favio Uzquiano Velasco', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('281', '1', 'bernalp', 'Pablo Bernal', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('275', '1', 'perezc', 'Cristhian Nelson Perez Benitez', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('1', '1', 'admin', 'admin', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('3', '1', 'riveroju', 'Julio Cesar Rivero Oblitas', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('4', '1', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('5', '4', 'osinagac', 'Carlos Rubirh Osinaga Chavez', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('6', '5', 'vacad', 'Daniel Vaca Rua', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('7', '5', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('8', '1', 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('9', '1', 'penama', 'Marco Augusto Peña  Alcocer', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('10', '1', 'guzmany', 'Ysaac Guzman', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('11', '1', 'vargasm', 'Marilin Vargas Perez', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('12', '4', 'eguezc', 'Carlos Eduardo Eguez Rojas', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('13', '4', 'villarroellu', 'Luis Daniel Villarroel', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('14', '4', 'bordaj', 'Jimmy Alejandro Borda Villa', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('15', '4', 'monasteriok', 'Kevin Monasterio Mejia', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('16', '1', 'salazari', 'Iver Salazar Zorilla', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('17', '4', 'montecinosd', 'Denis Maribel Montecinos Coronado', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('18', '1', 'pedrozow', 'Wilfredo Pedrozo Guzman', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('19', '4', 'iralaj', 'Jose Luis Irala', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('20', '4', 'palomog', 'Giovana Palomo Sainz', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('280', '4', 'uzquianoca', 'Carlos Favio Uzquiano Velasco', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('274', '1', 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('278', '1', 'ramose', 'Erika Johana Ramos Peña', 't');
INSERT INTO "CSU_DISTRIBUIDO"."usuario" VALUES ('276', '1', 'vargasm', 'Marilin Vargas Perez', 't');

-- ----------------------------
-- Primary Key structure for table formulario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."formulario" ADD CONSTRAINT "SYS_C004142" PRIMARY KEY ("formulario_id");

-- ----------------------------
-- Checks structure for table formulario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."formulario" ADD CONSTRAINT "SYS_C004023" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."formulario" ADD CONSTRAINT "SYS_C004139" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."formulario" ADD CONSTRAINT "SYS_C004140" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."formulario" ADD CONSTRAINT "SYS_C004141" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004146" PRIMARY KEY ("grupo_id");

-- ----------------------------
-- Checks structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004024" CHECK ("grupo_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004143" CHECK ("grupo_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004144" CHECK ("grupo_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004145" CHECK ("grupo_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table rol
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."rol" ADD CONSTRAINT "SYS_C004212" PRIMARY KEY ("rol_id");

-- ----------------------------
-- Checks structure for table rol
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."rol" ADD CONSTRAINT "SYS_C004043" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol" ADD CONSTRAINT "SYS_C004209" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol" ADD CONSTRAINT "SYS_C004210" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol" ADD CONSTRAINT "SYS_C004211" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004219" PRIMARY KEY ("rol_id", "formulario_id");

-- ----------------------------
-- Checks structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004044" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004045" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004213" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004214" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004215" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004216" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004217" CHECK ("rol_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004218" CHECK ("formulario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004231" PRIMARY KEY ("usuario_id");

-- ----------------------------
-- Checks structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004048" CHECK ("usuario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004228" CHECK ("usuario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004229" CHECK ("usuario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004230" CHECK ("usuario_id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."grupo_ad" ADD CONSTRAINT "SYS_C004248" FOREIGN KEY ("rol_id") REFERENCES "CSU_DISTRIBUIDO"."rol" ("rol_id") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004256" FOREIGN KEY ("formulario_id") REFERENCES "CSU_DISTRIBUIDO"."formulario" ("formulario_id") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "CSU_DISTRIBUIDO"."rol_formulario" ADD CONSTRAINT "SYS_C004257" FOREIGN KEY ("rol_id") REFERENCES "CSU_DISTRIBUIDO"."rol" ("rol_id") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_DISTRIBUIDO"."usuario" ADD CONSTRAINT "SYS_C004258" FOREIGN KEY ("rol_id") REFERENCES "CSU_DISTRIBUIDO"."rol" ("rol_id") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
