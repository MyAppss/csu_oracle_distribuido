CREATE TABLE "CSU_LOCAL"."acumulador" (
  "acumulador_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "limite" integer,
  "estado" varchar(1)
)
;


INSERT INTO "CSU_LOCAL"."acumulador" VALUES (1, 'ALCO_WHATSAPP', 999, 't');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (2, 'ALCO_DIA_WHATSAPP', 999, 't');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (3, 'ALCO_OFER_WHATSAPP', 999, 't');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (4, 'ALCO_SEMANA_WHATSAPP', 999, 't');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (612, '1234', 0, 'f');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (560, '1234WFXSSProbe', 0, 'f');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (562, '1234WFXSSProbe''")/>', 0, 'f');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (558, 'A1234B', 0, 'f');
INSERT INTO "CSU_LOCAL"."acumulador" VALUES (622, 'ALCO_FACEBOOK', 999, 't');



CREATE TABLE "CSU_LOCAL"."billetera" (
  "billetera_id" integer NOT NULL,
  "unit_type_id" integer,
  "nombre_comverse" varchar(50) ,
  "nombre_comercial" varchar(50),
  "prefijo_unidad" varchar(30) ,
  "operador" varchar(20),
  "valor" float,
  "cantidad_decimales" float,
  "monto_minimo" float,
  "monto_maximo" float,
  "estado" varchar(1) ,
  "alco" varchar(5) ,
  "acumulador_id" integer,
  "texto_ilimitado" varchar(50) ,
  "acumulado" varchar(5) ,
  "reserva" varchar(5)
)
;



INSERT INTO "CSU_LOCAL"."billetera" VALUES (2, 2, 'CORE BALANCE', 'Saldo', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (3, 2, 'CORE_BALANCE_2', 'Bono Tigo', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (4, 138, 'SMS_ICX', 'SMS', 'sms', 'DIVISION', 0.2, 0, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (5, 2, 'MMS', 'MMS', 'Bs', 'NINGUNO', 0, 0, 0, 99999000000, 'f', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (6, 137, 'Broadband_Currency', 'Paq Mes Internet', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (7, 137, 'GPRS_SUBSCRIPTIONS', 'Paq Semana Internet', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (8, 137, 'Blackberry', 'Paq Hora Internet', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (9, 138, 'SMS_PROMO_CARGA', 'SMS promo', 'sms', 'DIVISION', 0.2, 0, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (10, 2, 'DIVER_TIGO', 'Divertido', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (11, 2, 'SALDO_PROMO_CARGA', 'Llamadas', 'Bs', 'NINGUNO', 0, 0, 0, 999000000, 'f', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (12, 2, 'NUEVO_SIM', 'Nuevo SIM', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (14, 3, 'LLAM_MOV_LOC', 'Paquetigo llamada', 'Seg', 'NINGUNO', 0, 0, 0, 9999999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (15, 138, 'PROMO_SMS_ICX', 'SMS', 'sms', 'DIVISION', 0.2, 0, 0, 999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (16, 137, 'GPRS_PROMO', 'Paq Dia Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (17, 138, 'SMS_BOLSA', 'SMS extremo', 'sms', 'DIVISION', 0.2, 0, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (18, 2, 'PROMO_ANTICHURN', 'Carga extra', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (20, 2, 'LLAM_ON_NET', 'Llamadas Tigo', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (21, 3, 'VIDEOLLAMADA', 'Videollamada', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (23, 137, 'PROMO_MB', 'Paq Nocturno Internet', 'MB', 'DIVISION', 0.15, 2, 0, 6.6666659e+26, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (24, 3, 'Segundos', 'Paq. Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (25, 2, 'ALCO_WHATSAPP', 'Whatsapp ilimitado', 'Mb', 'NINGUNO', 0, 2, 0, 99999000000, 'f', 't', 1, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."billetera" VALUES (26, 137, 'ALCO_WHATSAPP', 'Whatsapp', 'Ilimitado', 'NINGUNO', 0, 0, -0.01, 1, 't', 't', 1, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (28, 137, 'ALCO_OFER_WHATSAPP', 'ALCO_OFER_WHATSAPP', 'Ilimitado', 'NINGUNO', 0, 0, -0.01, 1, 't', 't', 3, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (29, 137, 'ALCO_SEMANA_WHATSAPP', 'ALCO_SEMANA_WHATSAPP', 'Ilimitado', 'NINGUNO', 0, 0, -0.01, 1, 't', 't', 4, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (30, 3, 'Seg_15Dias', 'Paq 15 Dias Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (31, 3, 'Seg_5Dias', 'Paq 5 Dias Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (32, 3, 'Seg_2Dias', 'Paq 2 Dias Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (33, 138, 'SMS_15Dias', 'Paq 15 Dias SMS', 'sms', 'DIVISION', 0.2, 0, 0, 999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (34, 138, 'SMS_5Dias', 'Paq 5 Dias SMS', 'sms', 'DIVISION', 0.2, 0, 0, 999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (35, 138, 'SMS_2Dias', 'Paq 2 Dias SMS', 'sms', 'DIVISION', 0.2, 0, 0, 999000000, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (36, 137, 'MB_15Dias', 'Paq 15 Dias Internet', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (37, 137, 'MB_5Dias', 'Paq 5 Dias Internet', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (38, 137, 'MB_2Dias', 'Paq 2 Dias Internet', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (39, 3, 'Seg_7Dias', 'Paq 7 Dias Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (40, 2, 'CORE BALANCE RESERVED', 'Saldo Reserva', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (41, 3, 'LDI_USA', 'Paq Dia LDI USA', 'Seg', 'NINGUNO', 0, 0, 0, 1000000000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (42, 3, 'LDI_SUD', 'Paq Dia LDI SUDAMERICA', 'Seg', 'NINGUNO', 0, 0, 0, 1000000000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (43, 137, 'MB_PLAN', 'Plan MB', 'MB', 'DIVISION', 0.15, 2, 0, 99999000000, 't', 'f', NULL, NULL, 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (204, 137, 'GPRS_PROMO RESERVED', 'Paq Dia Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (205, 137, 'Blackberry RESERVED', 'Paq Hora Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (206, 137, 'Broadband_Currency RESERVED', 'Paq Mes Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (208, 2, 'CORE_BALANCE_2 RESERVED', 'Bono Tigo Reserva', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (209, 137, 'GPRS_SUBSCRIPTIONS RESERVED', 'Paq Semana Internet Reserva', 'MB', 'DIVISION', 15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (210, 2, 'PROMO_MB RESERVED', 'Paquetigo Nocturno Reserva', 'MB', 'DIVISION', 15, 2, 0, 6.6666659e+26, 'f', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (211, 137, 'MB_2Dias RESERVED', 'Paq 2 Dias Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (212, 137, 'MB_15Dias RESERVED', 'Paq 15 Dias Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 66666659000000, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (213, 137, 'MB_5Dias RESERVED', 'Paq 5 Dias Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 99999, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (214, 137, 'PROMO_MB RESERVED', 'Paq Norcturno Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (216, 137, 'MB_PLAN RESERVED', 'Plan MB Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999, 't', 'f', NULL, NULL, 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (217, 2, 'MMS', 'MMS', 'Bs', 'NINGUNO', 0, 0, 0, 99999, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (218, 2, 'CORE_BALANCE_STAFF', 'Otros Operadores', 'Bs', 'NINGUNO', 0, 2, 0, 999999999999999, 't', 'f', NULL, NULL, 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (235, 137, 'BILLETERA_TIGO', 'Internet Semana', 'MB', 'DIVISION', 0.15, 2, 0, 66666659, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (236, 3, 'LDI', 'Llamadas Intl.', 'min', 'DIVISION', 60, 2, 0, 99999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (13, 2, 'VOZ_SMS', 'Voz SMS', 'Bs', 'NINGUNO', 0, 2, 0, 99999000000, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (207, 137, 'PROMO_RECARGA RESERVED', 'Promo Recarga Reserved', 'Combo Smart', 'DIVISION', 0.15, 2, 0, 99999000000, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (27, 137, 'ALCO_DIA_WHATSAPP', 'WHATSAPP ILIMITADO DIA', 'ilimitado', 'NINGUNO', 0, 0, -0.01, 1, 't', 't', 2, 'ilimitado', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (19, 3, 'LLAM_LDI_SUD', 'LLAM_LDI_SUD', 'llamadas sudamerica', 'NINGUNO', 0, 0, 0, 99999000000, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (237, 2, 'PROMO_SMSP2P_XTREMO', 'SMS extremo', 'sms', 'DIVISION', 0.2, 0, 0, 99999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (238, 2, 'SALDO_PROMO_CARGA', 'Llamadas', 'Bs', 'NINGUNO', 0, 0, 0, 999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (240, 138, 'SMS_PLAN', 'Plan SMS', 'SMS', 'DIVISION', 0.2, 0, 0, 99999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (242, 137, 'B1K RESERVED', 'B1K Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (241, 137, 'B1K', 'B1K', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (243, 137, 'CMS_PROMO', 'CMS Promo', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (244, 137, 'CMS_PROMO RESERVED', 'CMS Promo Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (245, 137, 'MB_3Dias', 'Paq 3 Dias Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (247, 137, 'MB_3Dias RESERVED', 'Paq 3 Dias Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (248, 137, 'MB_3D_NOA RESERVED', 'Paq 3 Dias NoAc Int Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (246, 137, 'MB_3D_NOA', 'Paq 3 Dias No Acum Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (249, 138, 'SMS_3D_NOA', 'Paq 3 Dias SMS', 'SMS', 'DIVISION', 0.2, 0, 0, 999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (251, 3, 'SEG_3D_NOA', 'Paq 3 Dias Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 999999999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (250, 137, 'MB_1D_NOA', 'Paq 1 Día Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (252, 137, 'MB_1D_NOA RESERVED', 'Paq 1 Dia Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (253, 137, 'MB_A_VENCER', 'Megas a Vencer Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (255, 137, 'MB_A_VENCER RESERVED', 'Megas Vencer Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (254, 3, 'SEG_A_VENCER', 'Llamadas a Vencer', 'Seg', 'NINGUNO', 0, 0, 0, 999999999999999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (256, 137, 'DATA_PLAN', 'Plan Datos nternet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (257, 137, 'DATA_PLAN RESERVED', 'Plan Datos Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (259, 3, 'MIN_PLAN', 'Plan Llamadas', 'Seg', 'NINGUNO', 0, 0, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (258, 137, 'MB_PROMO', 'Mega Yapa Internet', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (260, 137, 'MB_PROMO RESERVED', 'Mega Yapa Internet Reserva', 'MB', 'DIVISION', 0.15, 2, 0, 999999999999999, 't', 'f', NULL, '', 'f', 't');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (263, 3, 'Segundos_Simplificados', 'Segundos Simplificados', 'Seg', 'NINGUNO', 0, 0, 0, 999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (261, 3, 'MIN_PROMO', 'Llamadas Promo', 'Seg', 'NINGUNO', 0, 0, 0, 999999999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (239, 2, 'LLAM_LDI', 'Llamadas Internacionales', 'Bs', 'NINGUNO', 0, 2, 0, 99999, 't', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (22, 137, 'PROMO_RECARGA', 'Promo Recarga', 'MB', 'DIVISION', 0.15, 2, 0, 99999000000, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (267, 137, 'PRUEBA', 'Prueba', 'MB', 'DIVISION', 0.15, 2, 0, 99999999999, 't', 'f', NULL, '', 't', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (265, 137, '1234', '1234', '1234', 'RESTA', 0, 3, 0, 99999, 'f', 'f', NULL, '1234', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (271, 3, 'billetera prueba', 'billetera prueba', 'MB', 'DIVISION', 5, 0, 0, 99999, 'f', 'f', NULL, '', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (269, 137, 'hhh', 'hhh', 'hhh', 'NINGUNO', 0, 0, 0, 99999, 'f', 'f', NULL, 'hhh', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (268, 137, 'OS 2001', 'Whatsapp dia', 'Ilimitado', 'NINGUNO', 0, 0, 0, 1, 'f', 't', 1, 'Ilimitado', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (272, 137, 'ALCO_FACEBOOK', 'FACEBOOK ILIMITADO', 'ilimitado', 'NINGUNO', 0, 0, -0.01, 1, 't', 't', 622, 'illimitado', 'f', 'f');
INSERT INTO "CSU_LOCAL"."billetera" VALUES (274, 137, 'Internet_Ilimitado', 'Internet Ilimitado', 'Ilimitado', 'NINGUNO', 0, 0, 0, 99999, 't', 'f', NULL, 'Ilimitado', 'f', 'f');



CREATE TABLE "CSU_LOCAL"."bitacora" (
  "fecha" timestamp NOT NULL,
  "usuario" varchar(50) ,
  "formulario" varchar(50) ,
  "accion" varchar(200) ,
  "direccion_ip" varchar(50)
)
;




CREATE TABLE "CSU_LOCAL"."cabecera" (
  "cabecera_id" integer NOT NULL,
  "nombre" varchar(100) ,
  "descripcion" varchar(500),
  "estado" varchar(1) ,
  "confi_id" integer,
  "visible" varchar(11) ,
  "posicion" integer,
  "tipo_navegacion" varchar(10),
  "saldo_calculado" varchar(2) ,
  "unit_type_id" integer,
  "whatsapp_ilimitado" varchar(2)
)
;




INSERT INTO "CSU_LOCAL"."cabecera" VALUES (138, 'prueba', 'prueba', 'f', 53, 't', 1, 'TODOS', 'f', 2, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (139, 'test', NULL, 't', 53, 't', 1, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (140, 'PREPAGO', 'hkg', 't', 53, 't', 2, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (141, 'Tu saldo total es:', 'Tu saldo total es:', 't', 53, 't', 3, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (146, 'Llamadas:', 'Llamadas:', 't', 18, 'f', 5, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (148, 'Saldo Total:', NULL, 't', 17, 't', 4, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (149, 'Credito:', NULL, 't', 17, 'f', 5, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (150, 'Reserva:', 'Saldo Reservado:', 't', 17, 'f', 6, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (151, 'Internet:', 'Internet:', 't', 17, 'f', 2, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (152, 'Llamadas:', 'Llamadas:', 't', 17, 'f', 3, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (153, 'Whatsapp ilimitado:', 'Whatsapp ilimitado', 't', 17, 't', 1, 'TODOS', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (154, 'Internet: ', 'Internet', 't', 21, 'f', 2, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (155, 'Llamadas:', 'Llamadas', 't', 21, 'f', 3, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (156, 'Saldo Total:', 'Saldo total', 't', 21, 't', 4, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (157, 'Credito:', 'Credito disponible', 't', 21, 'f', 5, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (158, 'Reserva:', 'Saldo Reservado', 't', 21, 'f', 6, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (159, 'Whatsapp ilimitado:', 'Whatsapp ilimitado', 't', 21, 't', 1, 'TODOS', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (160, 'Whatsapp ilimitado:', 'Whatsapp illimitado', 't', 22, 't', 1, 'TODOS', 'f', 2, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (161, 'Internet:', 'Internet:', 't', 22, 'f', 2, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (162, 'Llamadas:', 'Llamadas:', 'f', 22, 'f', 3, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (163, 'Saldo Total:', 'Saldo Total:', 't', 22, 'f', 3, 'TODOS', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (164, 'Credito:', 'Credito Disp :', 't', 22, 'f', 4, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (165, 'Reserva:', 'Saldo Reservado:', 't', 22, 'f', 5, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (166, 'Whatsapp ilimitado:', 'Whatsapp illimitado', 't', 23, 't', 1, 'TODOS', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (167, 'Internet:', 'Internet:', 't', 23, 'f', 2, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (168, 'Llamadas:', 'Llamadas:', 't', 23, 'f', 3, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (169, 'Saldo Total:', 'Saldo Total:', 't', 23, 'f', 4, 'TODOS', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (170, 'Credito:', 'Credito Disponible', 't', 23, 'f', 5, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (171, 'Reserva:', 'Saldo Reservado:', 't', 23, 'f', 6, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (172, 'Saldo Total:', 'Saldo Total:', 't', 3, 't', 1, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (173, 'Credito:', 'Credito Disp :', 't', 3, 'f', 2, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (174, 'Reserva:', 'Saldo Reservado:', 't', 3, 'f', 3, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (175, 'Internet:', 'Internet:', 't', 3, 'f', 4, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (176, 'Llamadas:', 'Llamadas:', 't', 3, 'f', 5, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (177, 'Whatsapp ilimitado:', 'Whatsapp illim:', 't', 3, 'f', 6, 'TODOS', 't', 139, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (179, 'LLamadas Tigo:', 'LLamadas Tigo:', 'f', 55, 'f', 2, 'TODOS', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (185, 'Llamadas:', 'Llamadas:', 'f', 55, 'f', 3, 'TODOS', 't', 3, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (178, 'Internet:', 'Internet:', 't', 55, 'f', 1, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (180, 'Otros Operadores:', 'Otros Operadores:', 't', 55, 'f', 2, 'TODOS', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (181, 'Saldo Total:', 'Saldo Total:', 't', 55, 'f', 3, 'TODOS', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (182, 'Credito:', 'Credito:', 't', 55, 'f', 4, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (183, 'Reserva:', 'Reserva:', 't', 55, 'f', 5, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (184, 'Whatsapp ilimitado:', 'Whatsapp ilimitado:', 't', 55, 'f', 6, 'TODOS', 't', 137, 't');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (144, 'Reserva:', 'Reserva', 't', 18, 'f', 3, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (143, 'Credito:', 'Credito Disponible', 't', 18, 'f', 2, 'ON_DEMAND', 't', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (145, 'Internet:', 'Internet:', 't', 18, 'f', 4, 'CON_MB', 't', 137, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (142, 'Saldo Total:', 'Tu saldo total es:', 't', 18, 't', 1, 'TODOS', 'f', 2, 'f');
INSERT INTO "CSU_LOCAL"."cabecera" VALUES (147, 'Whatsapp ilimitado:', 'Whatsapp ilimitado', 't', 18, 'f', 6, 'TODOS', 't', 139, 't');




CREATE TABLE "CSU_LOCAL"."campanna" (
  "campanna_id" integer NOT NULL,
  "descripcion" varchar(100) ,
  "mensaje_inicial" varchar(50) ,
  "prioridad" float,
  "visible_vigencia" float,
  "estado" varchar(1)
)
;



CREATE TABLE "CSU_LOCAL"."campanna_clasificador" (
  "campanna_id" integer NOT NULL,
  "clasificador_id" integer NOT NULL,
  "nombre_valor" varchar(50)  NOT NULL,
  "estado" varchar(1)
)
;


CREATE TABLE "CSU_LOCAL"."campanna_mensaje" (
  "campanna_id" integer NOT NULL,
  "mensaje_id" integer NOT NULL,
  "estado" varchar(1)
)
;


CREATE TABLE "CSU_LOCAL"."clasificador" (
  "clasificador_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "nombre_CSU_LOCAL" varchar(50),
  "estado" varchar(1)
)
;


CREATE TABLE "CSU_LOCAL"."clasificador_valor" (
  "clasificador_id" integer NOT NULL,
  "nombre_valor" varchar(50)  NOT NULL,
  "estado" varchar(1)
)
;



CREATE TABLE "CSU_LOCAL"."composicion_billetera" (
  "composicion_billetera_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "nombre_comercial" varchar(50),
  "prefijo_unidad" varchar(30) ,
  "operador" varchar(20) ,
  "valor" float,
  "cantidad_decimales" float,
  "estado" varchar(1) ,
  "unit_type_id" int,
  "monto_maximo" float,
  "monto_minimo" float
)
;



INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (2, 'Paq. Hora no consumido', 'Paq. Hora no consumido', 'MB', 'NINGUNO', 0, 2, 't', 2, 66666659000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (3, 'Paq. Dia no consumido', 'Paq. Dia no consumido', 'MB', 'NINGUNO', 0, 2, 't', 2, 66666659000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (4, 'Paq. Semana no consumido', 'Paq. Semana no consumido', 'MB', 'NINGUNO', 0, 2, 't', 2, 66666659000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (5, 'Paq. Mes no consumido', 'Paq. Mes no consumido', 'MB', 'NINGUNO', 0, 2, 't', 2, 6.6666659e+26, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (6, 'Llamadas Mensual no consumido', 'Llamadas Mensual no consumido', 'Seg', 'NINGUNO', 0, 0, 'f', 3, 9999900000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (7, 'Paq. nocturno no consumido', 'Paq. nocturno no consumido', 'MB', 'NINGUNO', 0, 2, 't', 2, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (8, 'Saldo Total', 'Saldo Total', 'Bs', 'SUMA', 0, 2, 't', 2, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (9, 'Paq Min 15 dias no consumido', 'Paq Min 15 dias no consumido', 'Seg', 'NINGUNO', 0, 0, 't', 3, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (10, 'Paq Min 5 dias no consumido', 'Paq Min 5 dias no consumido', 'Seg', 'NINGUNO', 0, 0, 't', 3, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (11, 'Paq Min 2 dias no consumido', 'Paq Min 2 dias no consumido', 'Seg', 'NINGUNO', 0, 0, 't', 3, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (12, 'Paq Min 7 dias no consumido', 'Paq Min 7 dias no consumido', 'Seg', 'NINGUNO', 0, 0, 't', 3, 99999000000, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (389, 'Paq Hora internet', 'Paq. Hora', 'MB', 'SUMA', 0, 2, 't', 137, 99999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (390, 'Paq Mes Internet', 'Paq. Mes', 'MB', 'SUMA', 0, 2, 'f', 137, 9999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (391, 'Paq Mes Internet', 'Paq. Mes', 'MB', 'SUMA', 0, 2, 't', 137, 9999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (392, 'Paq Semana Internet', 'Paq. Semana', 'MB', 'SUMA', 0, 2, 't', 137, 9999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (393, 'Paq Dia Internet', 'Paq. Dia', 'MB', 'SUMA', 0, 2, 't', 137, 9999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (394, 'Paq 2 Dias Internet', 'Paq MB 2 dias', 'MB', 'SUMA', 0, 2, 't', 137, 9999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (395, 'Paq 5 Dias Internet', 'Paq MB 5 dias', 'MB', 'SUMA', 0, 2, 't', 137, 999999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (396, 'Paq 15 Dias Internet', 'Paq MB 15 dias', 'MB', 'SUMA', 0, 2, 't', 137, 99999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (397, 'Paq Nocturno Internet', 'Paquetigo Nocturno', 'MB', 'SUMA', 0, 2, 't', 137, 99999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (398, 'Plan MB', 'Plan MB', 'MB', 'SUMA', 0, 2, 't', 137, 9999999999999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (404, '1234', '1234', '1234', 'RESTA', 1234, 3, 't', 137, 99999, 0);
INSERT INTO "CSU_LOCAL"."composicion_billetera" VALUES (405, 'Credito Core Balance', 'Credito', 'Bs', 'RESTA', NULL, 2, 't', 2, 999999999, 0);

CREATE TABLE "CSU_LOCAL"."config" (
  "config_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "saludo_inicial" varchar(60) ,
  "descripcion" varchar(80) ,
  "mostrar_dpi" varchar(5) ,
  "mostrar_vigencia" varchar(5) ,
  "habilitado" varchar(5),
  "estado" varchar(1) ,
  "mostrar_billeteras_no_config" varchar(5) ,
  "mostrar_acumulados_megas" varchar(5) ,
  "lineas" varchar(500) ,
  "nombre_acumulado" varchar(500)
)
;




INSERT INTO "CSU_LOCAL"."config" VALUES (0, 'ninguna', 'ninguna', 'ninguna', 'f', 'f', 'f', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (4, 'ConfiguracionDPI', 'Linea:%TELEFONO%', 'Solo para planes DPI', 't', 'f', 't', 'f', 't', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (6, 'DPI_clone', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n para cuentas DPI', 't', 'f', 't', 'f', 't', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (7, 'DPI_clone2', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n para cuentas DPI', 't', 'f', 't', 'f', 't', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (8, 'Default_clone', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n por defecto', 'f', 'f', 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (9, 'Default_clone2', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n por defecto', 'f', 'f', 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (10, 'Prueba_whatsapp', 'Linea: %TELEFONO%', 'Prueba_whatsapp', 'f', NULL, 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (11, 'prueba_whatsapp', 'Linea %TELEFONO%', 'prueba_whatsapp', 'f', NULL, 't', 'f', 't', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (12, 'Default_clone3', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n por defecto', 'f', 'f', 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (13, 'Paquetigos Ilimitados', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n por defecto', 'f', 'f', 't', 'f', 't', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (14, 'Default_clone4_clone', 'Linea:%TELEFONO%', 'ConfiguraciÃ³n por defecto', 'f', 'f', 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (15, 'prueba_whatsapp_clone', 'Linea %TELEFONO%', 'prueba_whatsapp', 'f', NULL, 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (16, 'prueba_whatsapp_clone2', 'Linea %TELEFONO%', 'prueba_whatsapp', 'f', NULL, 't', 'f', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (17, 'SimplificacionOferta', 'Linea:%TELEFONO%', 'SimplificacionOferta', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (19, 'prueba_SimplicacionOferta6', 'Linea:%TELEFONO%', 'prueba_SimplicacionOferta6', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (20, 'SimplificacionOferta_clone', 'Linea:%TELEFONO%', 'SimplificacionOferta', 'f', 'f', 't', 'f', 'f', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (21, 'SimplificacionOferta_B2B_No_WA', 'Linea:%TELEFONO%', 'SimplificacionOferta150', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (22, 'SimplificacionOferta_VOZ_ILIM', 'Linea:%TELEFONO%
Llamadas ilimitadas: SI', 'SimplificacionOferta750', 'f', 'f', 't', 't', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (24, 'Configuracion Test Indep', 'Linea:%TELEFONO%', 'Configuracion Test Indep', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (25, 'Paquetigos Nocturnos_clone', 'Linea:%TELEFONO%', 'Paquetigos Nocturnos', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (26, 'Paquetigos Nocturnos_clone2', 'Linea:%TELEFONO%', 'Paquetigos Nocturnos', 'f', 'f', 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (27, 'PREPAGO', 'Linea:%TELEFONO%', 'PREPAGO', 'f', NULL, 't', 'f', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (53, 'prueba', NULL, 'prueba', 'f', NULL, 't', 'f', 'f', 'f', NULL, 'prueba');
INSERT INTO "CSU_LOCAL"."config" VALUES (55, 'Planes Staff', 'Linea:%TELEFONO%', 'Planes Staff', 'f', 'f', 't', 't', 'f', 'f', NULL, NULL);
INSERT INTO "CSU_LOCAL"."config" VALUES (3, 'Default', 'Linea:%TELEFONO%', 'Configuración por defecto', 'f', 'f', 't', 't', 'f', 'f', 'ALCO', '');
INSERT INTO "CSU_LOCAL"."config" VALUES (23, 'Simplificacion Oferta', 'Linea:%TELEFONO%', 'SimplificacionOferta_B2B', 'f', 'f', 't', 't', 'f', 'f', '', '');
INSERT INTO "CSU_LOCAL"."config" VALUES (18, 'Prepago', 'Linea:%TELEFONO%', 'Prepago', 'f', 'f', 't', 't', 'f', 'f', 'ALCO;a', '');
INSERT INTO "CSU_LOCAL"."config" VALUES (74, 'DPI', 'Linea:%TELEFONO%', 'Configuración para cuentas DPI', 'f', NULL, 't', 't', 'f', 'f', '', '');
INSERT INTO "CSU_LOCAL"."config" VALUES (5, 'Pruebas', 'Linea:%TELEFONO%', 'Configuracion para cuentas DPI', 'f', 'f', 't', 't', 'f', 'f', 'ALCO', '');



CREATE TABLE "CSU_LOCAL"."config_acumulado" (
  "nombre_comercial" varchar(50) ,
  "nombre_acumulado" varchar(50) ,
  "billetera_id" integer,
  "config_id" integer,
  "config_acumulado_id" integer NOT NULL,
  "segunda_fecha_exp" varchar(5)
)
;



INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 18, 1170, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 18, 1179, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 17, 1202, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 17, 1203, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 17, 1204, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO VIGENTE', 8, 17, 1205, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 17, 1206, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO VIGENTE', 6, 17, 1207, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 17, 1208, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 17, 1209, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 17, 1210, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 17, 1211, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 17, 1212, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 17, 1213, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO VIGENTE', 16, 17, 1214, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 17, 1215, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO VIGENTE', 7, 17, 1216, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 17, 1217, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO VIGENTE', 42, 17, 1218, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO VIGENTE', 41, 17, 1219, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 17, 1220, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mensual Llamadas', NULL, 14, 17, 1221, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 17, 1222, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO VIGENTE', 36, 17, 1223, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 17, 1224, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO VIGENTE', 38, 17, 1225, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 17, 1226, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO VIGENTE', 37, 17, 1227, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 17, 1228, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Mensual Internet', 'Plan Mensual Internet NO VIGENTE', 43, 17, 1229, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Mensual Internet Reserva', NULL, 216, 17, 1230, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 17, 1231, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 17, 1232, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 17, 1233, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO VIGENTE', 23, 17, 1234, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet reserva', NULL, 214, 17, 1235, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO VIGENTE', 22, 17, 1236, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 17, 1238, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Internet Reserva', NULL, 206, 18, 1148, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 18, 1149, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 18, 1151, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 18, 1152, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 18, 1150, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 18, 1153, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO ACTIVO', 16, 18, 1154, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', NULL, 204, 18, 1155, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO ACTIVO', 7, 18, 1156, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 18, 1157, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO ACTIVO', 42, 18, 1159, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO ACTIVO', 41, 18, 1160, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Intl.', 'LLAM_LDI_SUD NO VIGENTE', 19, 18, 1158, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 18, 1161, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 18, 1162, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 18, 1164, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO ACTIVO', 38, 18, 1165, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 18, 1166, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO ACTIVO', 37, 18, 1167, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 18, 1168, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MB_PLAN', 'MB_PLAN NO ACTIVO', 43, 18, 1169, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 18, 1171, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 18, 1172, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO ACTIVO', 23, 18, 1173, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet reserva', NULL, 214, 18, 1174, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Promo Recarga', 'Promo Recarga NO ACTIVO', 22, 18, 1175, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 18, 1142, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO ACTIVO', 30, 18, 1185, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO ACTIVO', 32, 18, 1186, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO ACTIVO', 31, 18, 1187, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO ACTIVO', 39, 18, 1188, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq LLamadas', 'Paq LLamadas NO ACTIVO', 24, 18, 1189, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 15 dias', NULL, 33, 18, 1180, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 2 dias', NULL, 35, 18, 1181, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 5 dias', NULL, 34, 18, 1182, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 18, 1177, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 18, 1183, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 18, 1184, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Videollamada', NULL, 21, 18, 1190, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 18, 1191, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 18, 1176, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 17, 1201, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 18, 1143, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 18, 1144, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 18, 1146, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 18, 1178, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 17, 1237, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 17, 1239, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 17, 1240, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 15 dias', NULL, 33, 17, 1241, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 2 dias', NULL, 35, 17, 1242, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 5 dias', NULL, 34, 17, 1243, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 17, 1244, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 17, 1245, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO VIGENTE', 30, 17, 1246, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO VIGENTE', 32, 17, 1247, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO VIGENTE', 31, 17, 1248, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO VIGENTE', 39, 17, 1249, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq LLamadas', 'Paq Llamadas NO VIGENTE', 24, 17, 1250, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Videollamada', NULL, 21, 17, 1251, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 22, 1254, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 22, 1255, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 22, 1256, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO ACTIVO', 8, 22, 1257, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 22, 1258, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO ACTIVO', 6, 22, 1259, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 22, 1260, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 22, 1261, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 22, 1262, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 22, 1263, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 22, 1264, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 22, 1265, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO ACTIVO', 16, 22, 1266, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 22, 1267, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO ACTIVO', 7, 22, 1268, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 22, 1269, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO ACTIVO', 42, 22, 1270, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO ACTIVO', 41, 22, 1271, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 22, 1272, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 22, 1273, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 22, 1274, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO ACTIVO', 36, 22, 1275, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 22, 1276, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO ACTIVO', 38, 22, 1277, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 22, 1278, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO ACTIVO', 37, 22, 1279, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 22, 1280, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet', 'Paq 60 Dias Internet NO ACTIVO', 43, 22, 1281, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet Reserva', NULL, 216, 22, 1282, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 22, 1283, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 22, 1284, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 22, 1285, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO ACTIVO', 23, 22, 1286, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Norcturno Internet reserva', NULL, 214, 22, 1287, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO ACTIVO', 22, 22, 1288, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 22, 1290, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 22, 1291, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 22, 1292, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 15 dias', NULL, 33, 22, 1293, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 2 dias', NULL, 35, 22, 1294, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 5 dias', NULL, 34, 22, 1295, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 22, 1296, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 22, 1297, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO ACTIVO', 30, 22, 1298, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO ACTIVO', 32, 22, 1299, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO ACTIVO', 31, 22, 1300, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO ACTIVO', 39, 22, 1301, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq LLamadas', 'Paq Llamadas NO ACTIVO', 24, 22, 1302, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 21, 22, 1303, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 21, 1306, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 21, 1307, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 21, 1308, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO VIGENTE', 8, 21, 1309, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 21, 1310, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO VIGENTE', 6, 21, 1311, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 21, 1312, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 21, 1313, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 21, 1314, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 21, 1315, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 21, 1316, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 21, 1317, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO VIGENTE', 16, 21, 1318, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 21, 1319, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO VIGENTE', 7, 21, 1320, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 21, 1321, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO VIGENTE', 42, 21, 1322, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO VIGENTE', 41, 21, 1323, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 21, 1324, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 21, 1325, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 21, 1326, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO VIGENTE', 36, 21, 1327, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 22, 1289, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 22, 1253, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 21, 1305, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 21, 1328, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO VIGENTE', 38, 21, 1329, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 21, 1330, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO VIGENTE', 37, 21, 1331, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 21, 1332, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet', 'Paq 60 Dias Internet NO VIGENTE', 43, 21, 1333, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet Reserva', NULL, 216, 21, 1334, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 21, 1335, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 21, 1336, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 21, 1337, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO VIGENTE', 23, 21, 1338, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet reserva', NULL, 214, 21, 1339, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO VIGENTE', 22, 21, 1340, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 21, 1342, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 21, 1343, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 21, 1344, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 15 dias', NULL, 33, 21, 1345, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 2 dias', NULL, 35, 21, 1346, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq SMS 5 dias', NULL, 34, 21, 1347, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 21, 1348, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 21, 1349, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO VIGENTE', 30, 21, 1350, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO VIGENTE', 32, 21, 1351, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO VIGENTE', 31, 21, 1352, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO VIGENTE', 39, 21, 1353, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq LLamadas', 'Paq LLamadas NO VIGENTE', 24, 21, 1354, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Videollamada', NULL, 21, 21, 1355, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 23, 1358, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 23, 1359, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 23, 1360, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO ACTIVO', 8, 23, 1361, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 23, 1362, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO ACTIVO', 6, 23, 1363, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 23, 1364, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 23, 1365, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 23, 1366, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 23, 1367, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 23, 1368, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 23, 1369, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO ACTIVO', 16, 23, 1370, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 23, 1371, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO ACTIVO', 7, 23, 1372, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 23, 1373, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO ACTIVO', 42, 23, 1374, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO ACTIVO', 41, 23, 1375, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 23, 1376, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 23, 1377, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 23, 1378, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO ACTIVO', 36, 23, 1379, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 23, 1380, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO ACTIVO', 38, 23, 1381, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 23, 1382, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO ACTIVO', 37, 23, 1383, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 23, 1384, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan MB', 'Plan MB NO ACTIVO', 43, 23, 1385, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan MB Reserva', NULL, 216, 23, 1386, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 23, 1387, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 23, 1388, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 23, 1389, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO ACTIVO', 23, 23, 1390, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Norcturno Internet Reserva', NULL, 214, 23, 1391, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO ACTIVO', 22, 23, 1392, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 23, 1394, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 23, 1395, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 23, 1396, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias SMS', NULL, 33, 23, 1397, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias SMS', NULL, 35, 23, 1398, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias SMS', NULL, 34, 23, 1399, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 23, 1400, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 23, 1401, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO ACTIVO', 30, 23, 1402, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO ACTIVO', 32, 23, 1403, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO ACTIVO', 31, 23, 1404, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO ACTIVO', 39, 23, 1405, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq LLamadas', 'Paq Llamadas NO ACTIVO', 24, 23, 1406, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 21, 23, 1407, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 3, 1411, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 3, 1412, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado', NULL, 26, 3, 1413, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO ACTIVO', 8, 3, 1414, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 3, 1415, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO ACTIVO', 6, 3, 1416, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 21, 1341, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 21, 1356, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 23, 1408, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 23, 1357, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 3, 1410, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 3, 1417, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 3, 1418, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 3, 1419, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 3, 1420, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 3, 1421, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 3, 1422, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO ACTIVO', 16, 3, 1423, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 3, 1424, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO ACTIVO', 7, 3, 1425, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 3, 1426, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO ACTIVO', 42, 3, 1427, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO ACTIVO', 41, 3, 1428, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 3, 1429, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 3, 1430, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 3, 1431, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO ACTIVO', 36, 3, 1432, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 3, 1433, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO ACTIVO', 38, 3, 1434, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 3, 1435, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO ACTIVO', 37, 3, 1436, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 3, 1437, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan 60 Dias Internet', 'Plan 60 Dias Internet NO ACTIVO', 43, 3, 1438, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet Reserva', NULL, 216, 3, 1439, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 5, 3, 1440, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 3, 1441, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 3, 1442, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO ACTIVO', 23, 3, 1443, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Norcturno Internet Reserva', NULL, 214, 3, 1444, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO ACTIVO', 22, 3, 1445, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 3, 1447, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 3, 1448, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 11, 3, 1449, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias SMS', NULL, 33, 3, 1450, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias SMS', NULL, 35, 3, 1451, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias SMS', NULL, 34, 3, 1452, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 3, 1453, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 3, 1454, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO ACTIVO', 30, 3, 1455, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO ACTIVO', 32, 3, 1456, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO ACTIVO', 31, 3, 1457, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO ACTIVO', 39, 3, 1458, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Llamadas', 'Paq Llamadas NO ACTIVO', 24, 3, 1459, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 21, 3, 1460, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado mensual', NULL, 28, 55, 1471, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado semana', NULL, 29, 55, 1472, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp', NULL, 26, 55, 1473, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO ACTIVO', 8, 55, 1474, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 55, 1475, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO ACTIVO', 6, 55, 1476, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 55, 1477, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 55, 1478, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 55, 1479, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 55, 1480, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 55, 1481, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 55, 1482, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 55, 1483, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO ACTIVO', 16, 55, 1484, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 55, 1485, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO ACTIVO', 7, 55, 1486, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 55, 1487, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO ACTIVO', 42, 55, 1488, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO ACTIVO', 41, 55, 1489, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam Internacionales', NULL, 19, 55, 1490, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 55, 1491, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 55, 1492, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO ACTIVO', 36, 55, 1493, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 55, 1494, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO ACTIVO', 38, 55, 1495, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 55, 1496, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO ACTIVO', 37, 55, 1497, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 55, 1498, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan 60 Dias Internet', 'Plan 60 Dias Internet NO ACTIVO', 43, 55, 1499, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet Reserva', NULL, 216, 55, 1500, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 55, 1501, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 55, 1502, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 55, 1503, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO ACTIVO', 23, 55, 1504, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Norcturno Internet Reserva', NULL, 214, 55, 1505, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO ACTIVO', 22, 55, 1506, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 55, 1508, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias SMS', NULL, 33, 55, 1509, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias SMS', NULL, 35, 55, 1510, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias SMS', NULL, 34, 55, 1511, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 55, 1512, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 55, 1513, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 55, 1514, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 3, 1446, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 55, 1470, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO ACTIVO', 30, 55, 1515, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO ACTIVO', 32, 55, 1516, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO ACTIVO', 31, 55, 1517, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO ACTIVO', 39, 55, 1518, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq. Llamadas', 'Paq. Llamadas NO ACTIVO', 24, 55, 1519, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 21, 55, 1520, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 3, 1530, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 3, 1531, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 23, 1532, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 23, 1533, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 22, 1534, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 22, 1535, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS promo', NULL, 9, 74, 1648, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Videollamada', NULL, 21, 74, 1649, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 74, 1650, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('WHATSAPP ILIMITADO DIA', NULL, 27, 74, 1595, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Internet Semana', 'Internet Semana NO VIGENTE', 235, 74, 1599, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO VIGENTE', 8, 74, 1600, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet Reserva', NULL, 205, 74, 1601, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO VIGENTE', 6, 74, 1602, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet Reserva', NULL, 206, 74, 1603, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo', NULL, 2, 74, 1604, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo', NULL, 3, 74, 1605, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Bono Tigo Reserva', NULL, 208, 74, 1606, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Saldo Reserva', NULL, 40, 74, 1607, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 74, 1608, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Divertido', NULL, 10, 74, 1609, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet', 'Paq Dia Internet NO VIGENTE', 16, 74, 1610, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia Internet Reserva', NULL, 204, 74, 1611, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet', 'Paq Semana Internet NO VIGENTE', 7, 74, 1612, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Semana Internet Reserva', NULL, 209, 74, 1613, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Intl.', NULL, 236, 74, 1614, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI SUDAMERICA', 'Paq Dia LDI SUDAMERICA NO VIGENTE', 42, 74, 1615, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Dia LDI USA', 'Paq Dia LDI USA NO VIGENTE', 41, 74, 1616, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('LLAM_LDI_SUD', 'LLAM_LDI_SUD NO VIGENTE', 19, 74, 1617, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paquetigo llamada', NULL, 14, 74, 1618, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Tigo', NULL, 20, 74, 1619, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Llamadas', 'Paq 5 Dias Llamadas NO VIGENTE', 31, 74, 1640, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 7 Dias Llamadas', 'Paq 7 Dias Llamadas NO VIGENTE', 39, 74, 1641, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq. Llamadas', 'Paq. Llamadas NO VIGENTE', 24, 74, 1642, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias SMS', NULL, 33, 74, 1643, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias SMS', NULL, 35, 74, 1644, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 55, 1521, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias SMS', NULL, 34, 74, 1645, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Otros Operadores', NULL, 218, 18, 1536, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 18, 1537, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 17, 74, 1646, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('ALCO_OFER_WHATSAPP', NULL, 28, 74, 1596, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 4, 74, 1647, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('ALCO_SEMANA_WHATSAPP', NULL, 29, 74, 1597, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp', NULL, 26, 74, 1598, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('MMS', NULL, 217, 74, 1628, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Nuevo SIM', NULL, 12, 74, 1629, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Carga extra', NULL, 18, 74, 1630, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Nocturno Internet', 'Paq Nocturno Internet NO VIGENTE', 23, 74, 1631, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Norcturno Internet Reserva', NULL, 214, 74, 1632, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart', 'Combo Smart NO VIGENTE', 22, 74, 1633, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 74, 1634, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS', NULL, 15, 74, 1635, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Internet Reserva', NULL, 247, 18, 1685, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 237, 74, 1636, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 238, 74, 1637, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Llamadas', 'Paq 15 Dias Llamadas NO VIGENTE', 30, 74, 1638, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Llamadas', 'Paq 2 Dias Llamadas NO VIGENTE', 32, 74, 1639, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO VIGENTE', 36, 74, 1620, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet Reserva', NULL, 212, 74, 1621, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 1 Día Internet', NULL, 250, 74, 1722, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias No Acum Internet', NULL, 246, 18, 1686, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias NoAc Int Reserva', NULL, 248, 18, 1687, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Megas a Vencer Internet', NULL, 253, 18, 1688, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Megas Vencer Internet Reserva', NULL, 255, 18, 1689, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 60 Dias Internet Reserva', NULL, 216, 18, 1409, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Mega Yapa Internet', 'Mega Yapa Internet NO VIGENTE', 258, 18, 1690, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 17, 1252, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 22, 1304, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Mega Yapa Internet Reserva', NULL, 260, 18, 1691, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Voz SMS', NULL, 13, 3, 1461, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Llamadas', 'Plan Llamadas NO VIGENTE', 259, 18, 1692, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 1 Dia Internet Reserva', NULL, 252, 74, 1723, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Whatsapp ilimitado dia', NULL, 27, 18, 1141, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('B1K', 'B1K NO VIGENTE', 241, 18, 1676, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Promo', 'Llamadas Promo NO VIGENTE', 261, 18, 1693, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('B1K Reserva', NULL, 242, 18, 1677, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Internet Semana', 'Internet Semana NO VIGENTE', 235, 18, 1655, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Hora Internet', 'Paq Hora Internet NO ACTIVO', 8, 18, 1145, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('SMS extremo', NULL, 237, 18, 1657, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq Mes Internet', 'Paq Mes Internet NO ACTIVO', 6, 18, 1147, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas', NULL, 238, 18, 1658, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('CMS Promo', 'CMS Promo NO VIGENTE', 243, 18, 1678, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan MB', 'Plan MB NO VIGENTE', 43, 74, 1626, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan MB Reserva', NULL, 216, 74, 1627, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Mega Yapa Internet', 'Mega Yapa Internet NO VIGENTE', 258, 74, 1730, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Mega Yapa Internet Reserva', NULL, 260, 74, 1731, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Llamadas', 'Plan Llamadas NO VIGENTE', 259, 74, 1732, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Promo', 'Llamadas Promo NO VIGENTE', 261, 74, 1733, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('CMS Promo Reserva', NULL, 244, 18, 1679, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Datos nternet', 'Plan Datos nternet NO VIGENTE', 256, 18, 1680, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Datos Internet Reserva', NULL, 257, 18, 1681, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Intl.', NULL, 236, 18, 1656, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llam LDI', NULL, 239, 18, 1675, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 15 Dias Internet', 'Paq 15 Dias Internet NO ACTIVO', 36, 18, 1163, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 1 Día Internet', NULL, 250, 18, 1682, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 1 Dia Internet Reserva', NULL, 252, 18, 1683, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Internet', 'Paq 3 Dias Internet NO VIGENTE', 245, 18, 1684, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Llamadas', NULL, 251, 18, 1694, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas a Vencer', NULL, 254, 18, 1695, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Segundos Simplificados', 'Segundos Simplificados NO VIGENTE', 263, 18, 1698, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias SMS', NULL, 249, 18, 1696, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan SMS', NULL, 240, 18, 1697, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 23, 1393, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Combo Smart Reserved', NULL, 207, 55, 1507, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Llamadas', NULL, 251, 74, 1734, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('B1K', 'B1K NO VIGENTE', 241, 74, 1715, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('B1K Reserva', NULL, 242, 74, 1716, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('CMS Promo', 'CMS Promo NO VIGENTE', 243, 74, 1717, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('CMS Promo Reserva', NULL, 244, 74, 1718, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Datos nternet', 'Plan Datos nternet NO VIGENTE', 256, 74, 1719, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan Datos Internet Reserva', NULL, 257, 74, 1720, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas Internacionales', NULL, 239, 74, 1721, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet', 'Paq 2 Dias Internet NO VIGENTE', 38, 74, 1622, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 2 Dias Internet Reserva', NULL, 211, 74, 1623, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Internet', 'Paq 3 Dias Internet NO VIGENTE', 245, 74, 1724, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias Internet Reserva', NULL, 247, 74, 1725, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias No Acum Internet', NULL, 246, 74, 1726, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias NoAc Int Reserva', NULL, 248, 74, 1727, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet', 'Paq 5 Dias Internet NO VIGENTE', 37, 74, 1624, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 5 Dias Internet Reserva', NULL, 213, 74, 1625, 't');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Megas a Vencer Internet', NULL, 253, 74, 1728, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Megas Vencer Internet Reserva', NULL, 255, 74, 1729, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Llamadas a Vencer', NULL, 254, 74, 1735, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Segundos Simplificados', 'Segundos Simplificados NO VIGENTE', 263, 74, 1736, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Paq 3 Dias SMS', NULL, 249, 74, 1737, 'f');
INSERT INTO "CSU_LOCAL"."config_acumulado" VALUES ('Plan SMS', NULL, 240, 74, 1738, 'f');


CREATE TABLE "CSU_LOCAL"."config_billetera" (
  "billetera_id" integer NOT NULL,
  "config_id" integer NOT NULL,
  "nombre_comercial" varchar(50) ,
  "mostrar_siempre" varchar(5) ,
  "mostrar_saldo_mayor_cero" varchar(5) ,
  "mostrar_saldo_menor_cero" varchar(5) ,
  "mostrar_saldo_cero" varchar(5) ,
  "no_mostrar_saldo_expirado" varchar(5) ,
  "estado" varchar(1) ,
  "posicion" float,
  "mostrar_vigencia" varchar(5) ,
  "mostrar_saldo_expirado" varchar(5) ,
  "mostrar_segunda_fecha_exp" varchar(5) ,
  "mostrar_hora_segunda_fecha_exp" varchar(5) ,
  "asumir_format_hr_primera_fecha" varchar(5) 
)
;



INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (27, 3, 'WHATSAPP ILIMITADO DIA', 't', 'f', 'f', 'f', 'f', 't', 4, 'f', 'f', 'f', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (29, 3, 'ALCO_SEMANA_WHATSAPP', 'f', 't', 'f', 't', 't', 't', 5, 't', 'f', 'f', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (24, 3, 'Paq. Llamadas', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (14, 3, 'Paquetigo llamada', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (21, 3, 'Videollamada', 'f', 't', 'f', 'f', 't', 't', 8, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (3, 3, 'Bono Tigo', 'f', 't', 'f', 'f', 't', 't', 9, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (32, 3, 'Paq 2 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 20, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (2, 5, 'Saldo', 't', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (8, 5, 'Paq Hora Internet', 'f', 't', 'f', 'f', 't', 't', 1, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (4, 74, 'SMS', 'f', 't', 'f', 'f', 't', 't', 0, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (9, 74, 'SMS promo', 'f', 't', 'f', 'f', 't', 't', 1, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (12, 74, 'Nuevo SIM', 'f', 't', 'f', 'f', 't', 't', 2, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (14, 74, 'Paquetigo llamada', 'f', 't', 'f', 'f', 't', 't', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (18, 74, 'Carga extra', 'f', 't', 'f', 'f', 't', 't', 4, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (21, 74, 'Videollamada', 'f', 't', 'f', 'f', 't', 't', 5, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (23, 74, 'Paq Nocturno Internet', 'f', 't', 'f', 'f', 't', 't', 6, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (31, 74, 'Paq 5 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (2, 74, 'Saldo', 't', 'f', 'f', 'f', 'f', 't', 10, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (268, 3, 'Whatsapp dia', 't', 'f', 'f', 'f', 'f', 'f', 25, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (23, 5, 'Paq Nocturno Internet', 'f', 't', 'f', 'f', 't', 't', 2, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (24, 5, 'Paq. Llamadas', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (3, 5, 'Bono Tigo', 'f', 't', 'f', 'f', 't', 't', 10, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (18, 5, 'Carga extra', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (8, 3, 'Paq Hora Internet', 'f', 't', 'f', 'f', 't', 't', 1, 'f', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (6, 3, 'Paq Mes Internet', 'f', 't', 'f', 'f', 't', 't', 3, 'f', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (10, 5, 'Divertido', 'f', 't', 'f', 'f', 't', 't', 12, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (14, 18, 'Paquetigo llamada', 'f', 't', 'f', 'f', 'f', 'f', 1, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (4, 5, 'SMS', 'f', 't', 'f', 'f', 't', 't', 13, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (15, 5, 'SMS', 'f', 't', 'f', 'f', 't', 't', 14, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (36, 5, 'Paq 15 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 21, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (37, 5, 'Paq 5 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 22, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (38, 5, 'Paq 2 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 23, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (39, 5, 'Paq 7 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 24, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (42, 5, 'Paq Dia LDI SUDAMERICA', 't', 'f', 'f', 'f', 'f', 't', 25, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (41, 5, 'Paq Dia LDI USA', 't', 'f', 'f', 'f', 'f', 't', 26, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (26, 18, 'Whatsapp', 't', 'f', 'f', 'f', 'f', 'f', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (2, 18, 'Saldo', 't', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (32, 18, 'Paq 2 Dias Llamadas', 'f', 't', 'f', 't', 'f', 't', 2, 't', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (16, 5, 'Paq Dia Internet', 't', 'f', 'f', 'f', 'f', 't', 27, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (268, 5, 'Whatsapp dia', 't', 'f', 'f', 'f', 'f', 't', 28, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (28, 23, 'ALCO_OFER_WHATSAPP', 'f', 't', 'f', 't', 't', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (13, 23, 'Voz SMS', 't', 'f', 'f', 'f', 'f', 't', 1, 't', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (14, 23, 'Paquetigo llamada', 'f', 't', 'f', 'f', 't', 't', 2, 't', 'f', 'f', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (3, 74, 'Bono Tigo', 'f', 't', 'f', 'f', 't', 'f', 1, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (217, 74, 'MMS', 'f', 't', 'f', 'f', 't', 'f', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (10, 74, 'Divertido', 'f', 't', 'f', 'f', 't', 'f', 5, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (13, 74, 'Voz SMS', 'f', 't', 'f', 'f', 't', 'f', 7, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (15, 74, 'SMS', 'f', 't', 'f', 'f', 't', 'f', 9, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (20, 74, 'Llamadas Tigo', 'f', 't', 'f', 'f', 't', 'f', 11, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (22, 74, 'Combo Smart', 'f', 't', 'f', 'f', 't', 'f', 13, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (30, 74, 'Paq 15 Dias Llamadas', 'f', 't', 'f', 'f', 't', 'f', 15, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (6, 23, 'Paq Mes Internet', 'f', 't', 'f', 'f', 't', 't', 3, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (2, 23, 'Saldo', 't', 'f', 'f', 'f', 'f', 't', 4, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (8, 23, 'Paq Hora Internet', 'f', 't', 'f', 'f', 't', 't', 5, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (23, 23, 'Paq Nocturno Internet', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (24, 23, 'Paq. Llamadas', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (20, 23, 'Llamadas Tigo', 'f', 't', 'f', 'f', 't', 't', 8, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (21, 23, 'Videollamada', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (3, 23, 'Bono Tigo', 'f', 't', 'f', 'f', 't', 't', 10, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (18, 23, 'Carga extra', 'f', 't', 'f', 'f', 't', 't', 11, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (10, 23, 'Divertido', 'f', 't', 'f', 'f', 't', 't', 12, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (4, 23, 'SMS', 'f', 't', 'f', 'f', 't', 't', 13, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (15, 23, 'SMS', 'f', 't', 'f', 'f', 't', 't', 14, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (9, 23, 'SMS promo', 'f', 't', 'f', 'f', 't', 't', 15, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (217, 23, 'MMS', 'f', 't', 'f', 'f', 't', 't', 16, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (12, 23, 'Nuevo SIM', 'f', 't', 'f', 'f', 't', 't', 17, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (30, 23, 'Paq 15 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 18, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (31, 23, 'Paq 5 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 19, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (32, 23, 'Paq 2 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 20, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (33, 23, 'Paq 15 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 21, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (34, 23, 'Paq 5 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 22, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (35, 23, 'Paq 2 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 23, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (37, 23, 'Paq 5 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 24, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (36, 23, 'Paq 15 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 25, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (38, 23, 'Paq 2 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 26, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (39, 23, 'Paq 7 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 27, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (2, 3, 'Saldo', 't', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (23, 3, 'Paq Nocturno Internet', 'f', 't', 'f', 'f', 't', 't', 2, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (18, 3, 'Carga extra', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (10, 3, 'Divertido', 'f', 't', 'f', 'f', 't', 't', 11, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (4, 3, 'SMS', 'f', 't', 'f', 'f', 't', 't', 12, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (15, 3, 'SMS', 'f', 't', 'f', 'f', 't', 't', 13, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (9, 3, 'SMS promo', 'f', 't', 'f', 'f', 't', 't', 14, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (13, 3, 'Voz SMS', 'f', 't', 'f', 'f', 't', 't', 15, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (217, 3, 'MMS', 'f', 't', 'f', 'f', 't', 't', 16, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (12, 3, 'Nuevo SIM', 'f', 't', 'f', 'f', 't', 't', 17, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (29, 5, 'ALCO_SEMANA_WHATSAPP', 'f', 't', 'f', 't', 't', 't', 5, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (32, 74, 'Paq 2 Dias Llamadas', 'f', 't', 'f', 'f', 't', 'f', 17, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (14, 5, 'Paquetigo llamada', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (20, 5, 'Llamadas Tigo', 'f', 't', 'f', 'f', 't', 't', 8, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (21, 5, 'Videollamada', 'f', 't', 'f', 'f', 't', 't', 9, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (30, 3, 'Paq 15 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 18, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (31, 3, 'Paq 5 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 19, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (268, 18, 'Whatsapp dia', 't', 'f', 'f', 'f', 'f', 'f', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (13, 5, 'Voz SMS', 'f', 't', 'f', 'f', 't', 't', 15, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (217, 5, 'MMS', 'f', 't', 'f', 'f', 't', 't', 16, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (12, 5, 'Nuevo SIM', 'f', 't', 'f', 'f', 't', 't', 17, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (30, 5, 'Paq 15 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 18, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (31, 5, 'Paq 5 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 19, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (32, 5, 'Paq 2 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 20, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (34, 74, 'Paq 5 Dias SMS', 'f', 't', 'f', 'f', 't', 'f', 19, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (39, 74, 'Paq 7 Dias Llamadas', 'f', 't', 'f', 'f', 't', 'f', 21, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (16, 18, 'Paq Dia Internet', 't', 'f', 'f', 'f', 'f', 't', 1, 't', 't', 't', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (272, 18, 'Facebook Ilimitado', 't', 'f', 'f', 'f', 'f', 't', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (33, 74, 'Paq 15 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (35, 74, 'Paq 2 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (6, 5, 'Paq Mes Internet', 'f', 't', 'f', 'f', 't', 't', 3, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (27, 5, 'WHATSAPP ILIMITADO DIA', 'f', 't', 'f', 't', 'f', 't', 4, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (33, 3, 'Paq 15 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 21, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (34, 3, 'Paq 5 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 22, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (35, 3, 'Paq 2 Dias SMS', 'f', 't', 'f', 'f', 't', 't', 23, 't', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_billetera" VALUES (39, 3, 'Paq 7 Dias Llamadas', 'f', 't', 'f', 'f', 't', 't', 24, 't', 'f', 'f', 'f', 'f');




CREATE TABLE "CSU_LOCAL"."config_composicion_billetera" (
  "composicion_billetera_id" integer NOT NULL,
  "config_id" integer NOT NULL,
  "nombre_comercial" varchar(50) ,
  "mostrar_siempre" varchar(5) ,
  "mostrar_saldo_mayor_cero" varchar(5) ,
  "mostrar_saldo_menor_cero" varchar(5) ,
  "mostrar_saldo_cero" varchar(5) ,
  "no_mostrar_saldo_expirado" varchar(5) ,
  "estado" varchar(1) ,
  "posicion" float,
  "mostrar_vigencia" varchar(5) ,
  "mostrar_saldo_expirado" varchar(5) ,
  "mostrar_segunda_fecha_exp" varchar(5) ,
  "mostrar_hora_segunda_fecha_exp" varchar(5) ,
  "asumir_format_hr_primera_fecha" varchar(5) 
)
;



INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 8, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 9, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 12, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 13, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 14, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 17, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 19, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 20, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 21, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 22, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 24, 'Paq. Hora no consumido', 'true', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 25, 'Paq. Hora no consumido', 'true', 'f', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 26, 'Paq. Hora no consumido', 'true', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 27, 'Paq. Hora no consumido', 'true', 'f', 'f', 'f', 'f', 't', 0, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 55, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 9, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 8, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 9, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 12, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 13, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 14, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 17, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 19, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 20, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 21, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 22, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 26, 'Paq. Dia no consumido', 'false', 't', 't', 't', 't', 't', 1, 't', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 27, 'Paq. Dia no consumido', 'false', 't', 't', 't', 't', 't', 1, 't', 't', NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 55, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 8, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 8, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 9, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 12, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 13, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 14, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 17, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 19, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 20, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 21, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 22, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 55, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 11, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 8, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 9, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 12, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 13, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 14, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 17, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 19, 'Paq. Mes no consumido', 'f', 'f', 'f', 'f', 'f', 't', 3, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 20, 'Paq. Mes no consumido', 'f', 'f', 'f', 'f', 'f', 't', 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 21, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 22, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 55, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 10, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (6, 17, 'Llamadas Mensual no consumido', 'f', 't', 'f', 'f', 'f', 'f', 4, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (6, 19, 'Llamadas Mensual no consumido', 'f', 't', 'f', 'f', 'f', 'f', 4, 'f', 'f', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 17, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 21, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 22, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 55, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 12, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 17, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 21, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 22, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 55, 'Paq. Hora', 'f', 't', 'f', 'f', 'f', 't', 4, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 23, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 23, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 23, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 23, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 23, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 23, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 17, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 18, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 21, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 22, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 55, 'Paq. Semana', 'f', 't', 'f', 'f', 'f', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 17, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 18, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 21, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 22, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 55, 'Paq. Dia', 'f', 't', 'f', 'f', 'f', 't', 3, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 17, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 18, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 21, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 22, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 55, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 'f', 't', 1, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 17, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 18, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 21, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 22, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 55, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 'f', 't', 2, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 17, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 18, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 21, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 22, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 55, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 'f', 't', 0, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 17, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 21, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 22, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 55, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 'f', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 17, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 21, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 22, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 55, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 'f', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 18, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 't', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 18, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 18, 'Paq. Mes', 'f', 't', 'f', 'f', 'f', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 3, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 18, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (389, 3, 'Paq. Hora', 'f', 't', 'f', 'f', 't', 't', 6, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 3, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 3, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 3, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 3, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 5, 'Paq. Semana no consumido', 't', 'f', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 5, 'Paq. Mes no consumido', 't', 'f', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 3, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 3, 'Paq. Mes', 'f', 't', 'f', 'f', 't', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 17, 'Paq. Mes', 'f', 't', 'f', 'f', 't', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 21, 'Paq. Mes', 'f', 't', 'f', 'f', 't', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 22, 'Paq. Mes', 'f', 't', 'f', 'f', 't', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (397, 23, 'Paquetigo Nocturno', 'f', 't', 'f', 'f', 't', 't', 5, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 23, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 23, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 23, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 23, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 23, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 3, 'Plan 60 Dias Internet', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (398, 23, 'Plan MB', 'f', 't', 'f', 'f', 't', 't', 13, 't', 'f', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 5, 'Paq. nocturno no consumido', 't', 'f', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 5, 'Paq. Hora no consumido', 't', 'f', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 5, 'Paq. Dia no consumido', 't', 'f', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 55, 'Paq. Mes', 'f', 't', 'f', 'f', 'f', 't', 5, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (391, 23, 'Paq. Mes', 'f', 't', 'f', 'f', 't', 't', 12, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (390, 18, 'Paq. Mes', 't', 'f', 'f', 'f', 'f', 'f', 6, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (392, 18, 'Paq. Semana', 'f', 't', 'f', 'f', 't', 't', 10, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (396, 18, 'Paq MB 15 dias', 'f', 't', 'f', 'f', 't', 't', 11, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (2, 3, 'Paq. Hora no consumido', 'f', 't', 'f', 'f', 'f', 't', 0, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (3, 3, 'Paq. Dia no consumido', 'f', 't', 'f', 'f', 'f', 't', 1, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (4, 3, 'Paq. Semana no consumido', 'f', 't', 'f', 'f', 'f', 't', 2, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (5, 3, 'Paq. Mes no consumido', 'f', 't', 'f', 'f', 'f', 't', 3, 'f', 't', 't', 't', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (7, 3, 'Paq. Nocturno no consumido', 'f', 't', 'f', 'f', 'f', 't', 4, 'f', 't', 'f', 'f', 'f');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (393, 18, 'Paq. Dia', 'f', 't', 'f', 'f', 't', 't', 7, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (394, 18, 'Paq MB 2 dias', 'f', 't', 'f', 'f', 't', 't', 8, 't', 'f', 't', 't', 't');
INSERT INTO "CSU_LOCAL"."config_composicion_billetera" VALUES (395, 18, 'Paq MB 5 dias', 'f', 't', 'f', 'f', 't', 't', 9, 't', 'f', 't', 't', 't');




CREATE TABLE "CSU_LOCAL"."configuracion" (
  "id_configuracion" float NOT NULL,
  "estado_comverse" float,
  "actualizacion_tabla" float,
  "importacion_manual" float
)
;

-- ----------------------------
-- Table structure for corto
-- ----------------------------
CREATE TABLE "CSU_LOCAL"."corto" (
  "corto_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "descripcion" varchar(80) ,
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."corto" VALUES (0, '*', 'Todos', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (1, '*123#', 'csu_ussd', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (2, '7255', 'csu_sms', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (3, '172', 'csu_sms', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (4, '174', 'csu_sms', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (5, '*611#', 'csu_ussd', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (23, '1234', '1234', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (16, '*191#', 'corto calidad', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (17, '1013', 'calidad sms', 't');
INSERT INTO "CSU_LOCAL"."corto" VALUES (18, '*105#', 'USSD', 't');




CREATE TABLE "CSU_LOCAL"."cos" (
  "cos_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "descripcion" varchar(80) ,
  "habilitado" varchar(5) ,
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."cos" VALUES (0, '*', 'Todos', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (2, 'BROADBANG_1GB_STAFF', 'BROADBANG_1GB_STAFF', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (3, 'BROADBAND_3000MB', 'BROADBAND_3000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (4, 'BROADBAND_1000MB', 'BROADBAND_1000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (5, 'BROADBAND_2000MB_DPI', 'BROADBAND_2000MB_DPI', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (6, 'TIGO_PREPAGO_DPI', 'TIGO_PREPAGO_DPI', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (7, 'BROADBAND_1024MB', 'BROADBAND_1024MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (8, 'BROADBAND_2000MB', 'BROADBAND_2000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (9, 'BROADBAND_DPI', 'BROADBAND_DPI', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (10, 'TIGO_PREPAGO_DC', 'TIGO_PREPAGO_DC', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (11, 'BROADBAND_1GB', 'BROADBAND_1GB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (12, 'BROADBAND_1100MB', 'BROADBAND_1100MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (13, 'BROADBAND_10000MB', 'BROADBAND_10000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (14, 'BROADBAND_5500MB', 'BROADBAND_5500MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (15, 'BROADBAND_5000MB', 'BROADBAND_5000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (16, 'BROADBAND_8000MB', 'BROADBAND_8000MB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (17, 'BROADBAND_LIBRE', 'BROADBAND_LIBRE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (18, 'BROADBAND_FULL', 'BROADBAND_FULL', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (19, 'BROADBAND_3GB', 'BROADBAND_3GB', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (20, 'BROADBAND_PREMIUM', 'BROADBAND_PREMIUM', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (21, 'PRUEBAS_TIGO', 'PRUEBAS_TIGO', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (22, 'TIGO_PREPAGO_TEMPORAL', 'TIGO_PREPAGO_TEMPORAL', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (23, 'STAFF_VENTAS_LTE', 'STAFF_VENTAS_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (24, 'PLAN_STAFF_LTE', 'PLAN_STAFF_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (25, 'PLAN_STAFF_500_LTE', 'PLAN_STAFF_500_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (26, 'PLAN_STAFF_1000_LTE', 'PLAN_STAFF_1000_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (27, 'PLAN_STAFF_100_LTE', 'PLAN_STAFF_100_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (28, 'PLAN_STAFF_VENTAS_300_LTE', 'PLAN_STAFF_VENTAS_300_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (29, 'PLAN_STAFF_300_LTE', 'PLAN_STAFF_300_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (35, 'F_FIJA_750_NEW', 'F_FIJA_750_NEW', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (36, 'F_FIJA_350_NEW', 'F_FIJA_350_NEW', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (37, 'F_FIJA_750_LTE', 'F_FIJA_750_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (38, 'F_FIJA_350_LTE', 'F_FIJA_350_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (39, 'F_FIJAFACIL_250_LTE', 'F_FIJAFACIL_250_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (40, 'F_FIJA_200_LTE', 'F_FIJA_200_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (41, 'F_FIJA_250_LTE', 'F_FIJA_250_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (42, 'F_FIJA_500_LTE', 'F_FIJA_500_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (43, 'F_FIJAFACIL_150_LTE', 'F_FIJAFACIL_150_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (44, 'F_FIJAFACIL_200_LTE', 'F_FIJAFACIL_200_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (45, 'F_FIJA_150_LTE', 'F_FIJA_150_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (46, 'EMPRESARIAL_750', 'EMPRESARIAL_750', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (47, 'EMPRESARIAL_40', 'EMPRESARIAL_40', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (48, 'EMPRESARIAL_100', 'EMPRESARIAL_100', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (49, 'EMPRESARIAL_250', 'EMPRESARIAL_250', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (50, 'EMPRESARIAL_500', 'EMPRESARIAL_500', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (51, 'EMPRESARIAL_75', 'EMPRESARIAL_75', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (52, 'EMPRESARIAL_150', 'EMPRESARIAL_150', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (53, 'EMPRESARIAL_200', 'EMPRESARIAL_200', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (54, 'EMPRESARIAL_350', 'EMPRESARIAL_350', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (55, 'EMPRESARIAL_1100', 'EMPRESARIAL_1100', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (56, 'EMPRESARIAL_2200', 'EMPRESARIAL_2200', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (57, 'ENTERPRISE_150', 'ENTERPRISE_150', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (58, 'ENTERPRISE_200', 'ENTERPRISE_200', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (59, 'ENTERPRISE_250', 'ENTERPRISE_250', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (60, 'BLUE_SINLIMITE_150', 'BLUE_SINLIMITE_150', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (61, 'BLUE_SINLIMITE_200', 'BLUE_SINLIMITE_200', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (62, 'BLUE_SINLIMITE_250', 'BLUE_SINLIMITE_250', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (63, 'F_FIJA_FACIL_MIXTO_179', 'F_FIJA_FACIL_MIXTO_179', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (64, 'TIGO_PREPAGO', 'TIGO_PREPAGO', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (65, 'TIGO_PREPAGO_LTE', 'TIGO_PREPAGO_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (66, 'NEGOCIO_PLUS_175', 'NEGOCIO_PLUS_175', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (67, 'NEGOCIO_PLUS_225', 'NEGOCIO_PLUS_225', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (68, 'NEGOCIO_PLUS_275', 'NEGOCIO_PLUS_275', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (69, 'NEGOCIO_PLUS_375', 'NEGOCIO_PLUS_375', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (70, 'NEGOCIO_PLUS_525', 'NEGOCIO_PLUS_525', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (71, 'NEGOCIO_PLUS_750', 'NEGOCIO_PLUS_750', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (72, 'F_FIJA_100_LTE', 'F_FIJA_100_LTE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (73, 'NEGOCIO_750', 'NEGOCIO_750', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (74, 'NEGOCIO_175', 'NEGOCIO_175', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (75, 'NEGOCIO_225', 'NEGOCIO_225', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (76, 'NEGOCIO_275', 'NEGOCIO_275', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (77, 'NEGOCIO_375', 'NEGOCIO_375', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (78, 'NEGOCIO_525', 'NEGOCIO_525', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (79, 'TIGOPREPAGOINCREIBLE', 'TIGOPREPAGOINCREIBLE', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (80, 'MB_PLAN', 'MB_PLAN', 't', 'f');
INSERT INTO "CSU_LOCAL"."cos" VALUES (81, 'PLAN_TIGO_TOTAL', 'PLAN_TIGO_TOTAL', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (82, 'PLAN_TIGO_TOTAL_PLUS', 'PLAN_TIGO_TOTAL_PLUS', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (242, 'prueba_cos', 'prueba_cos', 't', 'f');
INSERT INTO "CSU_LOCAL"."cos" VALUES (244, 'PLAN_STAFF', 'PLAN_STAFF', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (245, 'PLAN_STAFF_100', 'PLAN_STAFF_100', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (246, 'PLAN_STAFF_1000', 'PLAN_STAFF_1000', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (247, 'PLAN_STAFF_300', 'PLAN_STAFF_300', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (248, 'PLAN_STAFF_500', 'PLAN_STAFF_500', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (249, 'PLAN_STAFF_VENTAS_300', 'PLAN_STAFF_VENTAS_300', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (250, 'STAFF_VENTAS', 'STAFF_VENTAS', 't', 't');
INSERT INTO "CSU_LOCAL"."cos" VALUES (257, '1234', '1234', 't', 't');





CREATE TABLE "CSU_LOCAL"."detalle_cabecera_billetera" (
  "cabecera_id" integer,
  "billetera_id" integer,
  "detalle_id" integer NOT NULL,
  "estado" varchar(1) ,
  "order" float DEFAULT 0
)
;




INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (138, 6, 321, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (139, 8, 322, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (140, 28, 323, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (141, 2, 324, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 43, 342, 'f', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 22, 345, 'f', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 207, 346, 'f', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 19, 347, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 42, 348, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 41, 349, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (147, 28, 358, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (147, 26, 360, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (148, 2, 363, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (148, 40, 364, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (149, 2, 365, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (150, 3, 366, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 8, 367, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 205, 368, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 6, 369, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 206, 370, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 16, 371, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 204, 372, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 7, 373, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 209, 374, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 36, 375, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 212, 376, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 38, 377, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 211, 378, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 37, 379, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 213, 380, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 43, 381, 't', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 23, 382, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 214, 383, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 22, 384, 'f', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 207, 385, 'f', 19);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 42, 386, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 41, 387, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 14, 388, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 30, 389, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 32, 390, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 31, 391, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 39, 392, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 24, 393, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (152, 21, 394, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (153, 27, 395, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (153, 29, 396, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (153, 28, 397, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (150, 40, 398, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (151, 216, 399, 't', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 8, 400, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 205, 401, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 6, 402, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 206, 403, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 16, 404, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 204, 405, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 7, 406, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 209, 407, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 36, 408, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 212, 409, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 38, 410, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 211, 411, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 37, 412, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 213, 413, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 43, 414, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 216, 415, 'f', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 23, 416, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (154, 214, 417, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 14, 418, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 30, 419, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 32, 420, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 31, 421, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 39, 422, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 24, 423, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (155, 21, 424, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (156, 2, 425, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (156, 40, 426, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (157, 2, 427, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (158, 40, 428, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (159, 27, 429, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (160, 28, 430, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 8, 431, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 205, 432, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 6, 433, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 206, 434, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 16, 435, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 204, 436, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 7, 437, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 209, 438, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 36, 439, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 212, 440, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 38, 441, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 214, 344, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 8, 329, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 205, 330, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 16, 333, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 204, 334, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 38, 339, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 211, 340, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 37, 361, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 213, 341, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 7, 335, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 209, 336, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 36, 337, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 212, 338, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 6, 331, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 206, 332, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (142, 40, 326, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 39, 354, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 30, 351, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 21, 356, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 14, 350, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 23, 343, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (142, 2, 325, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 24, 355, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 31, 353, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (144, 40, 328, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (143, 2, 327, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 211, 442, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 37, 443, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 213, 444, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 43, 445, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 216, 446, 'f', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 23, 447, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 214, 448, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 22, 449, 'f', 19);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (161, 207, 450, 'f', 20);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 42, 451, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 41, 452, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 14, 453, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 30, 454, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 32, 455, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 31, 456, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 39, 457, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 24, 458, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (162, 21, 459, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (163, 2, 460, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (163, 40, 461, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (164, 2, 462, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (165, 40, 463, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (166, 28, 464, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 8, 465, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 205, 466, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 6, 467, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 206, 468, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 16, 469, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 204, 470, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 7, 471, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 209, 472, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 36, 473, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 212, 474, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 38, 475, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 211, 476, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 37, 477, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 213, 478, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 43, 479, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 216, 480, 'f', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 23, 481, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 214, 482, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 22, 483, 'f', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 207, 484, 'f', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 14, 485, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 32, 486, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 31, 487, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 39, 488, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 30, 489, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 24, 490, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (168, 21, 491, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (169, 2, 492, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (169, 40, 493, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (170, 2, 494, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (171, 40, 495, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (172, 2, 496, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (172, 40, 497, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (173, 2, 498, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (174, 40, 499, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 8, 500, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 205, 501, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 6, 502, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 206, 503, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 16, 504, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 204, 505, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 7, 506, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 209, 507, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 36, 508, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 212, 509, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 38, 510, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 211, 511, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 37, 512, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 213, 513, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 43, 514, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 216, 515, 'f', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 23, 516, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 214, 517, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 22, 518, 'f', 19);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 207, 519, 'f', 20);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 14, 520, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 32, 521, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 31, 522, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 39, 523, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 30, 524, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 24, 525, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (176, 21, 526, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (177, 27, 527, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (177, 29, 528, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (177, 28, 529, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 8, 531, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 205, 532, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 6, 533, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 206, 534, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 16, 535, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 204, 536, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 7, 537, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 209, 538, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 36, 539, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 212, 540, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 38, 541, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 211, 542, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 37, 543, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 213, 544, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 43, 545, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 216, 546, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 23, 547, 't', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (178, 214, 548, 't', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (180, 3, 550, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (180, 208, 551, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (181, 2, 552, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (181, 40, 553, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (182, 2, 554, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (183, 40, 555, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (184, 27, 556, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (184, 29, 557, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (180, 218, 558, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 24, 559, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 32, 560, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 39, 562, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (180, 3, 566, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (184, 28, 567, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (180, 218, 569, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (166, 27, 571, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (166, 29, 573, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 43, 574, 't', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (167, 216, 575, 't', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 43, 577, 'f', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 216, 578, 'f', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (175, 216, 579, 'f', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (145, 43, 580, 'f', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (179, 20, 549, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 31, 561, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 30, 563, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 14, 564, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (185, 21, 565, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 2, 599, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 3, 600, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 208, 601, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 40, 602, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 218, 603, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 10, 604, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 20, 605, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 217, 606, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 12, 607, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 2, 609, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 18, 608, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 237, 611, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 3, 610, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 238, 612, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 208, 613, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (519, 13, 615, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 40, 614, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 218, 617, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 10, 618, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 20, 619, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 217, 620, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 12, 621, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 18, 622, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 237, 623, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 238, 624, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (521, 13, 625, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 2, 627, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 3, 628, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 208, 629, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 40, 630, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 218, 631, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 10, 632, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 20, 633, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 217, 634, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 12, 635, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 18, 636, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 237, 637, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 238, 638, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (523, 13, 639, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 2, 641, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 3, 642, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 208, 643, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 40, 644, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 218, 645, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 10, 646, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 20, 647, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 217, 648, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 12, 649, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 18, 650, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 237, 651, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 238, 652, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (525, 13, 653, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 2, 655, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 3, 656, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 208, 657, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 40, 658, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 218, 659, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 10, 660, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 20, 661, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 217, 662, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 12, 663, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 18, 664, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 237, 665, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 238, 666, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (527, 13, 667, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (147, 27, 357, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (147, 29, 359, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (147, 28, 669, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 32, 352, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 42, 671, 'f', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (146, 41, 673, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_billetera" VALUES (142, 40, 675, 't', 2);


CREATE TABLE "CSU_LOCAL"."detalle_cabecera_menu" (
  "menu_id" integer,
  "billetera_id" integer,
  "detalle_id" integer NOT NULL,
  "estado" varchar(1),
  "order" float DEFAULT 0
)
;


INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 43, 407, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 22, 409, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (246, 8, 412, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (246, 205, 413, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (247, 7, 414, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (247, 209, 415, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (248, 6, 416, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (248, 206, 417, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (249, 38, 418, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (249, 211, 419, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (250, 37, 420, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (250, 213, 421, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (251, 36, 422, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (251, 212, 423, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (252, 23, 424, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (252, 214, 425, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (253, 23, 426, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (253, 214, 427, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (243, 10, 428, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (243, 24, 429, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 10, 430, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 5, 431, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 12, 432, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 13, 433, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 33, 434, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 35, 435, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 34, 436, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 4, 437, 'f', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 9, 438, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (254, 11, 439, 'f', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 10, 440, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 5, 441, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 4, 442, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 9, 443, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 11, 444, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 12, 445, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 13, 446, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 15, 447, 'f', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 17, 448, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 18, 449, 'f', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 19, 450, 'f', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (255, 21, 451, 'f', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (256, 14, 453, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (257, 32, 454, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (258, 31, 455, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (259, 39, 456, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (260, 30, 457, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (261, 24, 458, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (246, 8, 459, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (246, 205, 460, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 11, 471, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 21, 475, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 5, 476, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 10, 477, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 4, 478, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 9, 479, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 35, 480, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 34, 481, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 33, 482, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 12, 483, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 13, 484, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 17, 485, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 15, 486, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 18, 487, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 11, 488, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 19, 489, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 20, 490, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 21, 491, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (266, 5, 492, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 23, 493, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 8, 494, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 16, 495, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 38, 496, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 37, 497, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 7, 498, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 36, 499, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 6, 500, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (270, 23, 501, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (270, 214, 502, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (271, 8, 503, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (271, 205, 504, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (272, 16, 505, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (272, 204, 506, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (273, 38, 507, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (273, 211, 508, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (274, 37, 509, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (274, 213, 510, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (275, 7, 511, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (275, 209, 512, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (276, 36, 513, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (276, 212, 514, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (277, 6, 515, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (277, 206, 516, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (278, 14, 517, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (279, 32, 518, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (280, 31, 519, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (281, 39, 520, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 16, 403, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 37, 452, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 7, 404, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 36, 405, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 6, 402, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 34, 465, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (245, 16, 410, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 23, 408, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (245, 204, 411, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 33, 466, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 12, 467, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 13, 468, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 17, 469, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 15, 470, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 19, 473, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 20, 474, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 10, 461, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 4, 462, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 9, 463, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 35, 464, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (282, 30, 521, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (283, 24, 522, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 43, 523, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 42, 526, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 41, 527, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 10, 528, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 4, 529, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 9, 530, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 35, 531, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 34, 532, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 33, 533, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 12, 534, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 13, 535, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 17, 536, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 15, 537, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 18, 538, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 11, 539, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 19, 540, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 20, 541, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 21, 542, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (286, 5, 543, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 10, 544, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 4, 545, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 9, 546, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 35, 547, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 34, 548, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 33, 549, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 12, 550, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 13, 551, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 17, 552, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 15, 553, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 18, 554, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 11, 555, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 19, 556, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 20, 557, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 21, 558, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (291, 5, 559, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 23, 560, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 8, 561, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 16, 562, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 38, 563, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 37, 564, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 7, 565, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 36, 566, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 6, 567, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 43, 568, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (295, 43, 569, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (295, 216, 570, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (296, 14, 571, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (297, 23, 572, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (297, 214, 573, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (298, 8, 574, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (298, 205, 575, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (299, 16, 576, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (299, 204, 577, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (300, 38, 578, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (300, 211, 579, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (301, 37, 580, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (301, 213, 581, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (302, 7, 582, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (302, 209, 583, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (303, 36, 584, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (303, 212, 585, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (304, 6, 586, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (304, 206, 587, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (305, 32, 588, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (306, 31, 589, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (307, 39, 590, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (308, 30, 591, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (309, 24, 592, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 10, 593, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 4, 594, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 9, 595, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 35, 596, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 34, 597, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 33, 598, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 12, 599, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 13, 600, 'f', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 17, 601, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 15, 602, 'f', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 18, 603, 'f', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 11, 604, 'f', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 19, 605, 'f', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 20, 606, 'f', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 21, 607, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (311, 5, 608, 'f', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 10, 609, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 4, 610, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 9, 611, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 35, 612, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 34, 613, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 33, 614, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 12, 615, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 13, 616, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 17, 617, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 15, 618, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 18, 619, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 11, 620, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 19, 621, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 20, 622, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 21, 623, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (313, 5, 624, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 23, 625, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 8, 626, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 16, 627, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 38, 628, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 37, 629, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 7, 630, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 36, 631, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 6, 632, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 43, 633, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 43, 634, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 216, 635, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (318, 14, 636, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (319, 23, 637, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (319, 216, 638, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (320, 8, 639, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (320, 205, 640, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (284, 216, 525, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (321, 16, 641, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (321, 204, 642, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (322, 38, 643, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (322, 211, 644, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (323, 37, 645, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (323, 213, 646, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (324, 7, 647, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (324, 209, 648, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (325, 36, 649, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (325, 212, 650, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (326, 6, 651, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (326, 206, 652, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (327, 32, 653, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (328, 31, 654, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (329, 39, 655, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (330, 30, 656, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (331, 24, 657, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (332, 30, 658, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 23, 659, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 8, 660, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 16, 661, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 38, 662, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 37, 663, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 7, 664, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 36, 665, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 6, 666, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 43, 667, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (335, 14, 670, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (336, 23, 671, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (336, 214, 672, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (337, 8, 673, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (337, 205, 674, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (338, 16, 675, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (338, 204, 676, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (339, 38, 677, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (339, 211, 678, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (340, 37, 679, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (340, 213, 680, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (341, 7, 681, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (341, 209, 682, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (342, 36, 683, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (342, 212, 684, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (343, 32, 685, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (344, 31, 686, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (345, 39, 687, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (346, 30, 688, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (347, 24, 689, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 10, 690, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 4, 691, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 9, 692, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 35, 693, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 34, 694, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 33, 695, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 12, 696, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 13, 697, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 17, 698, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 15, 699, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 11, 700, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 18, 701, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 19, 702, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 20, 703, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 21, 704, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (349, 5, 705, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 23, 706, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 8, 707, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 16, 708, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 38, 709, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 37, 710, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 7, 711, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 36, 712, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (352, 6, 713, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (353, 23, 714, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (353, 214, 715, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (354, 8, 716, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (354, 205, 717, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (355, 16, 718, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (355, 204, 719, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (356, 38, 720, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (356, 211, 721, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (357, 37, 722, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (357, 213, 723, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (358, 7, 724, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (358, 209, 725, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (359, 36, 726, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (359, 212, 727, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (360, 6, 728, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (360, 206, 729, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (361, 14, 730, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (362, 32, 731, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (363, 31, 732, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (364, 39, 733, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (365, 30, 734, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (366, 24, 735, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 32, 736, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 31, 737, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 39, 738, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 30, 739, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 24, 740, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 42, 741, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 41, 742, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (367, 41, 752, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (368, 42, 753, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 23, 754, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 8, 755, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 16, 756, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 38, 757, 'f', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 37, 758, 'f', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 7, 759, 'f', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 36, 760, 'f', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 42, 748, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 31, 744, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 39, 745, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 30, 746, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 6, 750, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 41, 749, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 206, 751, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (334, 6, 668, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (334, 206, 669, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 6, 761, 'f', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 24, 762, 'f', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 42, 763, 'f', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 41, 764, 'f', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 32, 765, 'f', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 31, 766, 'f', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 39, 767, 'f', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (369, 30, 768, 'f', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 24, 769, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 42, 770, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 41, 771, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 32, 772, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 31, 773, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 39, 774, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (294, 30, 775, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (370, 41, 778, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (371, 42, 779, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 24, 780, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 41, 781, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 42, 782, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 32, 783, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 31, 784, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 39, 785, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (333, 30, 786, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (372, 41, 787, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (373, 42, 788, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (374, 41, 789, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (375, 42, 790, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 24, 791, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 41, 792, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 42, 793, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 32, 794, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 31, 795, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 39, 796, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (269, 30, 797, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (376, 41, 800, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (377, 42, 801, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (296, 14, 802, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (378, 41, 803, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (379, 42, 804, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (380, 27, 805, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (380, 29, 806, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (382, 29, 822, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (383, 28, 823, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (384, 27, 824, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (385, 27, 825, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (386, 29, 826, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 10, 842, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 4, 843, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 9, 844, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 33, 845, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 35, 846, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 34, 847, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 12, 848, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 13, 849, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 17, 850, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 15, 851, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 18, 852, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 19, 853, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 21, 854, 'f', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 8, 855, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 23, 856, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 16, 857, 't', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 38, 858, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 37, 859, 't', 5);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 7, 860, 't', 6);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 36, 861, 't', 7);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 6, 862, 't', 8);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 24, 863, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 41, 864, 't', 10);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 42, 865, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 32, 866, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 31, 867, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 39, 868, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (392, 30, 869, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (393, 27, 870, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (394, 29, 871, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (395, 8, 872, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (395, 205, 873, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (396, 23, 874, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (396, 214, 875, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (397, 16, 876, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (397, 204, 877, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (398, 38, 878, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (398, 211, 879, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (399, 37, 880, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (399, 213, 881, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (400, 7, 882, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (400, 209, 883, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (401, 36, 884, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (401, 212, 885, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (402, 6, 886, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (402, 206, 887, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (403, 24, 888, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (404, 41, 889, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (405, 41, 890, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (406, 32, 891, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (407, 31, 892, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (408, 39, 893, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (409, 30, 894, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (410, 14, 895, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (405, 42, 896, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (380, 27, 897, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (411, 28, 898, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (389, 3, 902, 't', 13);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (412, 21, 922, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (413, 21, 923, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (414, 21, 924, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (415, 21, 925, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (319, 214, 926, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (316, 43, 927, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (416, 21, 942, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (417, 43, 943, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (417, 216, 944, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 38, 406, 't', 4);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (284, 6, 798, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (295, 6, 776, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (295, 206, 777, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 8, 401, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 24, 747, 't', 9);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (244, 32, 743, 't', 12);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (284, 206, 799, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (284, 43, 524, 'f', 3);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 6, 1043, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (317, 206, 1063, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 18, 472, 't', 11);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 3, 1083, 't', 14);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 218, 1103, 't', 15);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 217, 1123, 't', 16);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 236, 1143, 't', 17);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 239, 1163, 't', 18);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 249, 1183, 't', 19);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (264, 240, 1203, 't', 20);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (599, 263, 1223, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (605, 243, 1283, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (605, 244, 1284, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (601, 22, 1243, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (601, 207, 1244, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (603, 241, 1263, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (603, 242, 1264, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (607, 43, 1303, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (607, 216, 1304, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (609, 245, 1323, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (609, 247, 1324, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (611, 246, 1343, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (611, 248, 1344, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (613, 251, 1363, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (617, 253, 1403, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (617, 255, 1404, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (621, 256, 1443, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (621, 257, 1444, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (623, 259, 1463, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (625, 258, 1483, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (625, 260, 1484, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (627, 261, 1503, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (615, 250, 1383, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (615, 252, 1384, 'f', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (619, 254, 1423, 'f', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (615, 250, 1523, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (615, 252, 1543, 't', 2);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (619, 254, 1563, 't', 1);
INSERT INTO "CSU_LOCAL"."detalle_cabecera_menu" VALUES (245, 204, 1583, 't', 2);



CREATE TABLE "CSU_LOCAL"."formulario" (
  "formulario_id" integer NOT NULL,
  "nombre" varchar(50) ,
  "url" varchar(50) ,
  "estado" varchar(1) ,
  "depende" integer,
  "nivel" varchar(50) 
)
;


INSERT INTO "CSU_LOCAL"."formulario" VALUES (1, 'Administración', NULL, 't', 0, '1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (2, 'Configuraciones', '  ', 't', 0, '2');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (3, 'Publicidad', NULL, 't', 0, '3');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (4, 'Parametrización', NULL, 't', 0, '4');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (5, 'Call Center/SAC', NULL, 't', 0, '5');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (6, 'Ayuda', NULL, 't', 0, '6');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (10, 'rol', 'Role.xhtml', 't', 0, '1.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (11, 'Usuarios', 'RoleUser.xhtml', 't', 0, '1.2');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (12, 'Grupos', 'RoleGroup.xhtml', 't', 0, '1.3');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (13, 'Bitacora', 'Bitacora.xhtml', 't', 0, '1.4');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (14, 'Rol_Formulario', 'RolFormulario.xhtml', 't', 10, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (21, 'Configuraciones para Billeteras', 'listConfigForm.xhtml', 't', 0, '2.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (22, 'Gestionar Configuracion Billetera', 'configForm.xhtml', 't', 21, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (31, 'Segmento Clasificador', 'SorterForm.xhtml', 't', 0, '3.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (32, 'Valores de Segmento', 'SorterValueForm.xhtml', 't', 0, '3.2');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (33, 'Mensajes', 'MessageForm.xhtml', 't', 0, '3.3');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (34, 'Campaña Segmentada', 'CampaignList.xhtml', 't', 0, '3.4');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (35, 'Gestionar Campaña', 'CampaignForm.xhtml', 't', 34, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (36, 'Opciones(Publicidad)', 'OptionPublicidad.xhtml', 't', 0, '3.6');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (41, 'Unit Types', 'UnitType.xhtml', 't', 0, '4.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (42, 'Billeteras Simples', 'WalletForm.xhtml', 't', 0, '4.2');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (43, 'Billeteras Agrupadas', 'listBilleterasAgrupadasForm.xhtml', 't', 0, '4.3');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (44, 'Gestionar Billetera Agrupada', 'BilleteraAgrupadaForm.xhtml', 't', 43, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (45, 'COS', 'Cos.xhtml', 't', 0, '4.5');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (47, 'Opciones Generales', 'OptionsDefault.xhtml', 't', 0, '4.7');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (48, 'Acumulador', 'Acumulador.xhtml', 't', 0, '4.8');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (49, 'Origen', 'Origen.xhtml', 't', 0, '4.9');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (50, 'Corto', 'Corto.xhtml', 't', 0, '4.90');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (51, 'Consultas de Saldo Realizadas', 'ReporteConsultas.xhtml', 't', 0, '5.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (61, 'Manual de Administrador', '../resources/templates/MU_CSU.pdf', 't', 0, '6.1');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (62, 'Manual SAC/Call Center', '../resources/templates/MU_CALL_SAC.pdf', 't', 0, '6.2');
INSERT INTO "CSU_LOCAL"."formulario" VALUES (73, 'Configuracion de Cabeceras', 'configCabecera.xhtml', 't', 21, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (74, 'Configuracion de Cabeceras', 'configMenu.xhtml', 't', 21, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (75, 'configAcumuladoForm', 'configAcumuladoForm.xhtml', 't', 21, NULL);
INSERT INTO "CSU_LOCAL"."formulario" VALUES (77, 'Maping Ofertas', 'MapeoOfertas.xhtml', 'f', 4, '4.9');



CREATE TABLE "CSU_LOCAL"."grupo_ad" (
  "grupo_id" integer NOT NULL,
  "rol_id" integer,
  "nombre" varchar(30) ,
  "detalle" varchar(40) ,
  "estado" varchar(1) 
)
;


INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (2, 4, 'Back Office Operator', 'Back Office Operator', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (3, 4, 'BackOffice_fdz', 'BackOffice_fdz', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (4, 4, 'Backoffice Nasis', 'Backoffice Nasis', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (5, 4, 'CALL CENTER NASIS', 'CALL CENTER NASIS', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (6, 1, 'VAP Support', 'VAP Support', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (7, 4, 'CALL CENTER', 'CALL CENTER', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (8, 4, 'CALL_NASIS-proxy', 'CALL_NASIS-proxy', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (9, 4, 'SAC-proxy', 'SAC-proxy', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (10, 4, 'SAC Nacional', 'SAC Nacional', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (11, 4, 'Call_DPI', 'Call_DPI', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (12, 4, 'Call Banda Ancha', 'Call Banda Ancha', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (13, 4, 'Contact Center Pre Pago', 'Contact Center Pre Pago', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (14, 4, 'CORPORATE-proxy', 'CORPORATE-proxy', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (15, 4, 'Comunicados Comerciales', 'Comunicados Comerciales', 't');
INSERT INTO "CSU_LOCAL"."grupo_ad" VALUES (16, 4, 'CAM', 'CAM', 't');



CREATE TABLE "CSU_LOCAL"."grupo_billetera" (
  "billetera_id" integer NOT NULL,
  "composicion_billetera_id" integer NOT NULL,
  "estado" varchar(1)
)
;



-- ----------------------------
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (2, 8, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (6, 5, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (6, 390, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (6, 391, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (7, 4, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (7, 392, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (8, 2, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (8, 389, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (14, 6, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (16, 3, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (16, 393, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (23, 7, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (23, 397, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (30, 9, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (31, 10, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (32, 11, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (36, 396, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (37, 395, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (38, 394, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (39, 12, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (40, 8, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (43, 398, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (204, 393, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (205, 389, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (206, 390, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (206, 391, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (209, 392, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (211, 394, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (212, 396, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (213, 395, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (214, 397, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (216, 398, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (2, 405, 't');
INSERT INTO "CSU_LOCAL"."grupo_billetera" VALUES (40, 405, 't');




CREATE TABLE "CSU_LOCAL"."lista_datamart" (
  "fecha" timestamp NOT NULL,
  "formato" varchar(30),
  "estado" varchar(1)
)
;



CREATE TABLE "CSU_LOCAL"."lista_guias" (
  "fecha" timestamp NOT NULL,
  "formato" varchar(30) ,
  "estado" varchar(1)
)
;


CREATE TABLE "CSU_LOCAL"."mapeo_ofertas" (
  "id_mapeo" integer NOT NULL,
  "offeringid" integer,
  "offeringparent" integer,
  "offeringname" varchar(500) ,
  "type" varchar(500) ,
  "offeringcode" varchar(500) ,
  "paymentmode" integer,
  "estado" varchar(1)
)
;


INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (522, 8014, NULL, 'NEGOCIO_525', 'COS', 'AT', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (534, 8035, NULL, 'PLAN_TIGO_AMBAR_TOTAL', 'COS', 'AZ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (508, 8035, NULL, 'PLAN_TIGO_AMBAR_TOTAL', 'COS', 'AZ', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (572, 8042, NULL, 'MEGA_PLAN_225', 'COS', 'MN', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (584, 8048, NULL, 'EMPRESA_150', 'COS', 'HM', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (596, 8054, NULL, 'EMPRESA_PLUS_75', 'COS', 'HS', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (608, 8060, NULL, 'EMPRESA_PLUS_500', 'COS', 'HY', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (25, 688, NULL, 'STAFF_VENTAS', 'COS', 'SV', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (510, 8024, NULL, 'TIGO_PREPAGO_2', 'COS', 'AI', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (524, 8026, NULL, 'ENTERPRISE_PLUS_100', 'COS', 'EH', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (536, 8017, NULL, 'PLAN_TIGO_TOTAL', 'COS', 'AX', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (553, NULL, NULL, '1234', NULL, NULL, NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (574, 8043, NULL, 'MEGA_PLAN_305', 'COS', 'MO', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (586, 8049, NULL, 'EMPRESA_200', 'COS', 'HN', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (598, 8055, NULL, 'EMPRESA_PLUS_100', 'COS', 'HT', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (610, 8061, NULL, 'EMPRESA_PLUS_600', 'COS', 'HZ ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (512, 8008, NULL, 'F_FIJA_100_LTE', 'COS', '8N', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (526, 8027, NULL, 'ENTERPRISE_PLUS_150', 'COS', 'EI', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (538, 8018, NULL, 'PLAN_TIGO_TOTAL_PLUS', 'COS', 'AY', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (564, 8040, NULL, 'MEGA_PLAN_135', 'COS', 'MJ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (576, 8044, NULL, 'MEGA_PLAN_395', 'COS', 'MP', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (588, 8050, NULL, 'EMPRESA_250', 'COS', 'HO', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (600, 8056, NULL, 'EMPRESA_PLUS_150', 'COS', 'HU', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (620, 110, NULL, 'moi', 'ALCO', 'MJ', 0, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (500, 454, NULL, 'lol', 'COS', '', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (514, 8010, NULL, 'NEGOCIO_175', 'COS', 'AP', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (528, 8028, NULL, 'ENTERPRISE_PLUS_200', 'COS', 'EL', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (540, 8031, NULL, 'ENTERPRISE_PLUS_500', 'COS', 'EM', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (552, 407, NULL, 'TIGO_PREPAGO', 'COS', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (566, 5135, 8040, '8040_MEGA_PLAN_135', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (578, 8045, NULL, 'MEGA_PLAN_595', 'COS', 'MQ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (590, 8051, NULL, 'EMPRESA_350', 'COS', 'HP', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (602, 8057, NULL, 'EMPRESA_PLUS_200', 'COS', 'HV', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (502, 727, NULL, 'asdsada', 'COS', '', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (516, 8011, NULL, 'NEGOCIO_225', 'COS', 'AQ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (530, 8029, NULL, 'ENTERPRISE_PLUS_250', 'COS', 'EK', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (554, 704, NULL, 'asas', 'CUG', '', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (568, 5136, 8040, '8040_MEGA_PLAN_135_SMS', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (580, 8046, NULL, 'EMPRESA_75', 'COS', 'HK', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (592, 8052, NULL, 'EMPRESA_500', 'COS', 'HQ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (604, 8058, NULL, 'EMPRESA_PLUS_250', 'COS', 'HW', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (1, 42, NULL, 'CHALEQUERO2', 'COS', 'C1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (2, 44, NULL, 'FACTURA_FIJA', 'COS', 'FF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (3, 45, NULL, 'PLAN_INTERNO_STAFF', 'COS', 'PI', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (4, 47, NULL, 'TELECEL_FREE', 'COS', 'TF', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (5, 48, NULL, 'TELECEL_PREPAGO', 'COS', 'TP', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (6, 81, NULL, 'TELECEL_PREPAGO2', 'COS', 'M2', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (7, 262, NULL, 'MINICABINA', 'COS', 'MC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (8, 382, NULL, 'FACTURA_FIJA200', 'COS', 'A2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (9, 383, NULL, 'FACTURA_FIJA300', 'COS', 'A3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (10, 385, NULL, 'FACTURA_FIJA500', 'COS', 'A5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (11, 407, NULL, 'TIGO_PREPAGO', 'COS', 'TG', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (12, 408, NULL, 'F_FIJA50_GSM', 'COS', 'G1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (13, 409, NULL, 'F_FIJA75_GSM', 'COS', 'G2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (14, 410, NULL, 'F_FIJA100_GSM', 'COS', 'G3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (15, 411, NULL, 'F_FIJA150_GSM', 'COS', 'G4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (16, 412, NULL, 'F_FIJA200_GSM', 'COS', 'G5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (17, 413, NULL, 'F_FIJA300_GSM', 'COS', 'G6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (18, 414, NULL, 'F_FIJA400_GSM', 'COS', 'G7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (19, 415, NULL, 'F_FIJA500_GSM', 'COS', 'G8', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (20, 487, NULL, 'CALEQUERO_GSM', 'COS', 'CG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (21, 507, NULL, 'TELECENTRO_GSM', 'COS', 'TE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (22, 508, NULL, 'MINICABINA_GSM', 'COS', 'MI', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (23, 547, NULL, 'F_FIJA1000_GSM', 'COS', 'FM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (24, 587, NULL, 'PLAN_UBICAR', 'COS', 'UB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (26, 707, NULL, 'F_FIJA2000_GSM', 'COS', 'FN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (27, 767, NULL, 'F_FIJA750_GSM', 'COS', 'S1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (28, 768, NULL, 'PRUEBAS_TIGO', 'COS', 'PT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (29, 827, NULL, 'BOLSA_MIN_1500', 'COS', 'BM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (30, 847, NULL, 'BOLSA_MIN_2500', 'COS', 'T1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (31, 848, NULL, 'BOLSA_MIN_5000', 'COS', 'T3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (33, 850, NULL, 'BOLSA_MIN_10000', 'COS', 'T5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (34, 868, NULL, 'BOLSA_MIN_30000', 'COS', 'N3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (35, 869, NULL, 'BOLSA_MIN_40000', 'COS', 'N4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (36, 927, NULL, 'PROMO_TIGO', 'COS', 'PR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (37, 947, NULL, 'MINICABINA_GSM_250', 'COS', 'FV', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (38, 967, NULL, 'MINICABINA_GSM_NO_EXP', 'COS', 'NE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (39, 987, NULL, 'PRUEBAS_TIGO_2', 'COS', 'TS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (40, 990, NULL, 'BOLSA_MIN_800', 'COS', 'MW', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (41, 1016, NULL, 'AVL', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (42, 1017, NULL, 'BB_500MBextra', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (43, 1018, NULL, 'BBand', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (44, 1019, NULL, 'BBERRY_10000', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (45, 1020, NULL, 'BBERRY_11500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (46, 1021, NULL, 'BBERRY_1200MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (47, 1022, NULL, 'BBERRY_2500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (48, 1023, NULL, 'BBERRY_3000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (49, 1024, NULL, 'BBERRY_300MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (50, 1025, NULL, 'BBERRY_320MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (51, 1026, NULL, 'BBERRY_3500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (52, 1027, NULL, 'BBERRY_5000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (53, 1028, NULL, 'BBERRY_600MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (54, 1029, NULL, 'BBERRY_7000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (55, 1030, NULL, 'BBERRY_700MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (56, 1031, NULL, 'BBERRY_8000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (57, 1032, NULL, 'BBERRY_9000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (58, 1033, NULL, 'BBERRY_AVANZ3', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (59, 1034, NULL, 'BBERRY_FACIL', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (60, 1035, NULL, 'BBERRY_FFBRINTER', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (61, 1036, NULL, 'BBERRY_FFBRINTER_EXT', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (62, 1037, NULL, 'BBERRY_ILIMITADO', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (63, 1038, NULL, 'BBFFINTRO', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (64, 1039, NULL, 'BBRRY_MAXIMO', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (65, 1043, NULL, 'DATA_10000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (66, 1044, NULL, 'DATA_1000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (67, 1045, NULL, 'DATA_100MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (68, 1046, NULL, 'DATA_11000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (69, 1047, NULL, 'DATA_12000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (70, 1048, NULL, 'DATA_1200MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (71, 1049, NULL, 'DATA_1500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (72, 1050, NULL, 'BROADBAND_1GB', 'COS', 'GB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (73, 1051, NULL, 'BROADBAND_3GB', 'COS', 'GD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (74, 1052, NULL, 'DATA_2000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (75, 1053, NULL, 'DATA_200MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (76, 1054, NULL, 'DATA_2500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (77, 1055, NULL, 'DATA_3000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (78, 1056, NULL, 'DATA_300MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (79, 1057, NULL, 'DATA_3500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (80, 1058, NULL, 'DATA_3G_STAFF', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (81, 1059, NULL, 'DATA_40000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (82, 1060, NULL, 'DATA_4000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (83, 1061, NULL, 'DATA_400MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (84, 1062, NULL, 'DATA_4500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (85, 1063, NULL, 'DATA_45MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (86, 1064, NULL, 'DATA_5000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (87, 1065, NULL, 'DATA_500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (88, 1066, NULL, 'DATA_500MBextra', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (89, 1067, NULL, 'DATA_50MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (90, 1068, NULL, 'DATA_5500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (91, 1069, NULL, 'DATA_55MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (92, 1070, NULL, 'BROADBANG_1GB_STAFF', 'COS', 'BS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (93, 1071, NULL, 'DATA_6500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (94, 1072, NULL, 'DATA_7000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (95, 1073, NULL, 'DATA_700MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (96, 1074, NULL, 'DATA_70MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (97, 1075, NULL, 'DATA_75MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (98, 1076, NULL, 'DATA_7800MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (99, 1077, NULL, 'DATA_8000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (100, 1078, NULL, 'DATA_800MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (101, 1079, NULL, 'DATA_80MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (102, 1080, NULL, 'DATA_8500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (103, 1081, NULL, 'DATA_9000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (104, 1082, NULL, 'DATA_ILIMITADO', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (105, 1083, NULL, 'DATAMIXPOS', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (106, 1084, NULL, 'DATAMIXPOS_CPE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (107, 1085, NULL, 'DATAMIXPOS_CPE_MAX', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (108, 1086, NULL, 'DATAMIXPOS_MAX', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (109, 1087, NULL, 'DATAMIXPOS_MIN', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (110, 1090, NULL, 'TIGO_1', 'COS', 'D1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (111, 1100, NULL, 'Extra_Acred_Staff', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (112, 1101, NULL, 'Extra_Acred_Staff_1000', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (113, 1103, NULL, 'INTERNET_TOTAL', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (114, 1104, NULL, 'NFFBRAVANZ', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (115, 1105, NULL, 'NFFBRBASIC', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (116, 1106, NULL, 'NFFBRYMAIL', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (117, 1107, NULL, 'NFFBRYSOC', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (118, 1108, NULL, 'NOM_15_NUEVO_SIM', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (119, 1109, NULL, 'NOM_30_NUEVO_SIM', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (120, 1110, NULL, 'NOM_60_NUEVO_SIM', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (121, 1111, NULL, 'PC_PlanMD90', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (122, 1115, NULL, 'RBT_1', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (123, 1116, NULL, 'Recur_Dia_1ctv', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (124, 1117, NULL, 'SMS_10', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (125, 1118, 3731, 'SMS_100', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (126, 1119, NULL, 'SMS_120', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (127, 1120, NULL, 'SMS_130', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (128, 1121, NULL, 'SMS_150', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (129, 1122, NULL, 'SMS_160', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (130, 1123, NULL, 'SMS_165', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (131, 1124, NULL, 'SMS_20', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (132, 1125, NULL, 'SMS_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (32, 849, NULL, 'BOLSA_MIN_7500', 'COS', 'T4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (133, 1126, NULL, 'SMS_200000', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (134, 1127, NULL, 'SMS_30', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (135, 1128, NULL, 'SMS_35', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (136, 1129, NULL, 'SMS_40', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (137, 1130, NULL, 'METAS_EXPANSION_VSAT', 'COS', 'VS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (138, 1131, NULL, 'SMS_500', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (139, 1132, NULL, 'SMS_500000', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (140, 1133, NULL, 'SMS_55', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (141, 1134, NULL, 'SMS_60', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (142, 1135, NULL, 'SMS_65', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (143, 1136, NULL, 'SMS_70', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (144, 1137, NULL, 'SMS_75', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (145, 1138, NULL, 'SMS_75_TEMP', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (146, 1139, NULL, 'SMS_80', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (147, 1142, NULL, 'Test_Option', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (148, 1143, NULL, 'TEST_SUSCR', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (149, 1144, NULL, 'Tigo_Money_2500 MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (150, 1170, NULL, 'F_FIJA30_GSM', 'COS', 'GG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (151, 1190, NULL, 'BOLSA_CORP_5000', 'COS', 'F1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (152, 1191, NULL, 'BOLSA_CORP_7500', 'COS', 'F3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (153, 1192, NULL, 'BOLSA_CORP_10000', 'COS', 'F4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (154, 1193, NULL, 'BOLSA_CORP_30000', 'COS', 'F5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (155, 1194, NULL, 'BOLSA_CORP_40000', 'COS', 'F6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (156, 1210, NULL, 'MINICARGA', 'COS', 'MA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (157, 1230, NULL, 'CELLULAR', 'COS', 'LL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (158, 1250, NULL, 'MINICABINA_FF_100', 'COS', 'MF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (159, 1251, NULL, 'MINICABINA_FF_50', 'COS', 'NF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (160, 1270, NULL, 'F_FIJA_TIGO_JUNIOR', 'COS', 'TJ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (161, 1271, NULL, 'F_FIJA_TIGO_EST_50', 'COS', 'E5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (162, 1272, NULL, 'F_FIJA_TIGO_EST_75', 'COS', 'E7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (163, 1273, NULL, 'F_FIJA_TIGO_U', 'COS', 'TU', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (164, 1290, NULL, 'BROADBAND_1000MB', 'COS', 'QW', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (165, 1310, NULL, 'BROADBAND_3000MB', 'COS', 'QE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (166, 1311, NULL, 'BROADBAND_5000MB', 'COS', 'QR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (167, 1312, NULL, 'BROADBAND_8000MB', 'COS', 'QT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (168, 1370, NULL, 'VENTAS_FF500', 'COS', 'VF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (169, 1371, NULL, 'VENTAS_FF300', 'COS', 'EF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (170, 1372, NULL, 'VENTAS_FF100', 'COS', 'AF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (171, 1390, NULL, 'VENTAS_PDA', 'COS', 'VP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (172, 1650, NULL, 'F_FIJA220_GSM', 'COS', 'G0', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (173, 1651, NULL, 'F_FIJA280_GSM', 'COS', 'G9', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (174, 1652, NULL, 'F_FIJA480_GSM', 'COS', 'GA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (175, 1752, NULL, 'F_FIJA160_GSM', 'COS', 'FG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (176, 1790, NULL, 'BROADBAND_2000MB', 'COS', 'HJ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (177, 1830, NULL, 'BROADBAND_FULL', 'COS', 'BF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (178, 1850, NULL, 'F_FIJA_SMARTH_150', 'COS', 'P1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (179, 1851, NULL, 'F_FIJA_SMARTH_200', 'COS', 'P2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (180, 1852, NULL, 'F_FIJA_SMARTH_320', 'COS', 'P3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (181, 1853, NULL, 'F_FIJA_SMARTH_420', 'COS', 'P4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (182, 1854, NULL, 'F_FIJA_SMARTH_500', 'COS', 'P5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (183, 1855, NULL, 'F_FIJA_SMARTH_600', 'COS', 'P6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (184, 1890, NULL, 'F_FIJA_SEGURIDAD_75', 'COS', 'Q1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (185, 1891, NULL, 'F_FIJA_SEGURIDAD_100', 'COS', 'Q2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (186, 1892, NULL, 'F_FIJA_SEGURIDAD_150', 'COS', 'Q3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (187, 1893, NULL, 'F_FIJA_SEGURIDAD_200', 'COS', 'Q4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (188, 1894, NULL, 'F_FIJA_SEGURIDAD_300', 'COS', 'Q5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (189, 1895, NULL, 'F_FIJA_SEGURIDAD_400', 'COS', 'Q6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (190, 1896, NULL, 'F_FIJA_SEGURIDAD_500', 'COS', 'Q7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (191, 1910, NULL, 'BROADBAND_1024MB', 'COS', 'QS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (192, 1970, NULL, 'F_FIJA_MINISMART_110', 'COS', 'M1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (193, 1971, NULL, 'F_FIJA_MINISMART_160', 'COS', 'M3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (194, 1972, NULL, 'F_FIJA_MINISMART_220', 'COS', 'M4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (195, 1973, NULL, 'F_FIJA_MINISMART_300', 'COS', 'M5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (196, 1974, NULL, 'F_FIJA_MINISMART_380', 'COS', 'M6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (197, 1975, NULL, 'F_FIJA_MINISMART_450', 'COS', 'M7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (198, 1976, NULL, 'F_FIJA_MINISMART_550', 'COS', 'M8', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (199, 2130, NULL, 'BROADBAND_5500MB', 'COS', 'QN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (200, 2131, NULL, 'BROADBAND_10000MB', 'COS', 'QK', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (201, 2150, NULL, 'BROADBAND_1100MB', 'COS', 'QH', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (202, 2171, NULL, 'F_FIJA_IPHONE_590', 'COS', 'IN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (203, 2172, NULL, 'F_FIJA_IPHONE_780', 'COS', 'IE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (204, 2173, NULL, 'F_FIJA_IPHONE_300', 'COS', 'IP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (205, 2174, NULL, 'F_FIJA_IPHONE_390', 'COS', 'IH', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (206, 2176, NULL, 'F_FIJA_IPHONE_450', 'COS', 'IO', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (207, 2177, NULL, 'F_FIJA_IPHONE_1200', 'COS', 'IM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (208, 2178, NULL, 'POSPAGO_PRUEBA', 'COS', 'PP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (209, 2190, NULL, 'BROADBAND_PREMIUM', 'COS', 'ZP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (210, 2210, NULL, 'BROADBAND_DPI', 'COS', 'ZD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (211, 2270, NULL, 'F_FIJA_SMS_150', 'COS', 'FS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (212, 2271, NULL, 'F_FIJA_SMS_100', 'COS', 'FK', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (213, 2290, NULL, 'BROADBAND_LIBRE', 'COS', 'BL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (214, 2310, NULL, 'F_FIJA_100_P', 'COS', 'FP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (215, 2330, NULL, 'TIGO_PREPAGO_DC', 'COS', 'TD', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (216, 2350, 2390, 'F_FIJA_FACIL_50', 'COS', 'FT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (217, 2351, 2390, 'F_FIJA_FACIL_100', 'COS', 'FA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (218, 2352, 2390, 'F_FIJA_FACIL_150', 'COS', 'FB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (219, 2353, 2390, 'F_FIJA_FACIL_200', 'COS', 'FC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (220, 2354, 2390, 'F_FIJA_FACIL_300', 'COS', 'FD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (221, 2390, NULL, 'TIGO_PRE_FACIL', 'COS', 'PF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (222, 2430, NULL, 'BROADBAND_AVL', 'COS', 'AV', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (223, 2431, NULL, 'TIGO_PREPAGO_TEMPORAL', 'COS', 'TT', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (224, 2450, NULL, 'BOLSA_CORP75_FAM', 'COS', 'B1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (225, 2451, NULL, 'BOLSA_CORP100_FAM', 'COS', 'B2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (226, 2452, NULL, 'BOLSA_CORP150_FAM', 'COS', 'B3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (227, 2453, NULL, 'BOLSA_CORP200_FAM', 'COS', 'B4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (228, 2454, NULL, 'BOLSA_CORP300_FAM', 'COS', 'B5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (229, 2470, NULL, 'BROADBAND_2000MB_DPI', 'COS', 'HI', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (230, 2510, NULL, 'FF_CORP_FAM1000', 'COS', 'B6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (231, 2511, NULL, 'FF_CORP_FAM', 'COS', 'B7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (232, 2530, NULL, 'FF_CORP_FAM50', 'COS', 'B8', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (233, 2551, NULL, 'TIGO_PRE_BLACK', 'COS', 'TB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (234, 2570, NULL, 'TIGO_PREPAGO_MD90', 'COS', 'MD', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (235, 2590, NULL, 'BOLSA_CORP_60000', 'COS', 'B9', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (236, 2610, NULL, 'PLAN_STAFF_1000', 'COS', 'SB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (237, 2611, NULL, 'PLAN_STAFF_500', 'COS', 'SC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (238, 2630, NULL, 'TIGO_PREPAGO_MD180', 'COS', 'NV', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (239, 2670, NULL, 'BROADBAND_FACIL', 'COS', 'DF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (240, 2730, NULL, 'F_FIJA250_GSM', 'COS', 'K1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (241, 2731, 2390, 'F_FIJA_FACIL_250', 'COS', 'K2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (242, 2810, NULL, 'PLAN_STAFF_100', 'COS', 'SD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (243, 2811, NULL, 'PLAN_STAFF_300', 'COS', 'SE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (244, 2850, 2390, 'F_FIJA_FACIL_MIXTO_119', 'COS', 'V1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (245, 2890, 2390, 'F_FIJA_FACIL_MIXTO_179', 'COS', 'V2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (246, 2891, 2390, 'F_FIJA_FACIL_MIXTO_239', 'COS', 'V3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (247, 2892, 2390, 'F_FIIJA_FACIL_MIXTO_299', 'COS', 'V4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (248, 2930, 2390, 'F_FIJA_FACIL_MIXTO_109', 'COS', 'V5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (249, 2932, 2390, 'F_FIJA_FACIL_MIXTO_169', 'COS', 'V6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (250, 3050, NULL, 'DATAMIX_POS', 'COS', 'DX', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (251, 3051, NULL, 'DATAMIX_POS_MAX', 'COS', 'DM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (252, 3052, NULL, 'DATAMIX_CPE', 'COS', 'DC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (253, 3053, NULL, 'DATAMIX_CPE_MAX', 'COS', 'DA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (254, 3090, NULL, 'DATAMIX_POS_MIN', 'COS', 'DN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (255, 3110, 2390, 'F_FACIL_MEGA_SMART_99', 'COS', 'F7', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (256, 3112, 2390, 'F_FACIL_MEGA_SMART_249', 'COS', 'F0', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (257, 3114, 2390, 'F_FACIL_MEGA_SMART_149', 'COS', 'F8', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (258, 3116, 2390, 'F_FACIL_MEGA_SMART_199', 'COS', 'F9', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (259, 3130, NULL, 'BOLSA_MICRO_EMP_400', 'COS', 'BE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (260, 3131, NULL, 'BOLSA_MICRO_EMP_500', 'COS', 'BT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (261, 3170, NULL, 'PLAN_STAFF_VENTAS_300', 'COS', 'LF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (262, 3210, 3250, 'LTE_SMALL_245', 'COS', 'LS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (263, 3211, 3250, 'LTE_MEDIUM_345', 'COS', 'LM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (264, 3212, 3250, 'LTE_LARGE_445', 'COS', 'LG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (265, 3213, 3250, 'LTE_XLARGE_645', 'COS', 'LX', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (266, 3230, NULL, 'BOLSA_CORP_IP', 'COS', 'BI', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (267, 3250, NULL, 'LTE_PRE_FACIL', 'COS', 'LP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (268, 3251, NULL, 'LTE_STAFF', 'COS', 'ST', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (269, 3270, NULL, 'LTE_HOT_SPOT', 'COS', 'LH', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (270, 3290, NULL, 'LTE_FF_BASICO', 'COS', 'TO', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (271, 3291, NULL, 'LTE_FF_INTERMEDIO', 'COS', 'QA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (272, 3292, NULL, 'LTE_FF_AVANZADO', 'COS', 'KP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (273, 3293, NULL, 'LTE_FF_PREMIUM', 'COS', 'LT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (274, 3294, NULL, 'BOLSA_CORP_IP_100', 'COS', 'BO', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (275, 3295, NULL, 'BOLSA_CORP_IP_200', 'COS', 'BP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (276, 3296, NULL, 'BOLSA_CORP_IP_1000', 'COS', 'BC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (277, 3310, 3250, 'LTE_XSMALL_195', 'COS', 'LR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (278, 3330, NULL, 'LTE_FF_INICIAL', 'COS', 'TN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (279, 3350, NULL, 'LTE_PREPAGO_MIFI', 'COS', 'LN', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (280, 3370, NULL, 'F_FIJA100_LTE', 'COS', 'L3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (281, 3371, 2390, 'F_FIJA_FACIL_MIXTO_299_LT', 'COS', 'L4', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (282, 3372, NULL, 'F_FIJA_IPHONE_300_LTE', 'COS', 'IL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (283, 3390, NULL, 'F_FIJA150_LTE', 'COS', 'TL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (284, 3391, NULL, 'PLAN_STAFF_300_LTE', 'COS', 'S3', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (285, 3410, NULL, 'PLAN_STAFF_LTE', 'COS', 'PS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (286, 3411, NULL, 'STAFF_VENTAS_LTE', 'COS', 'VL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (287, 3412, NULL, 'PLAN_STAFF_500_LTE', 'COS', 'S5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (288, 3413, NULL, 'PLAN_STAFF_1000_LTE', 'COS', 'LE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (289, 3414, NULL, 'PLAN_STAFF_100_LTE', 'COS', 'L1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (290, 3415, NULL, 'PLAN_STAFF_VENTAS_300_LTE', 'COS', 'VA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (291, 3416, NULL, 'BROADBAND_STAFF_LTE', 'COS', 'L5', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (292, 3430, NULL, 'F_FIJA50_LTE', 'COS', 'IA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (293, 3431, NULL, 'F_FIJA75_LTE', 'COS', 'IB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (294, 3433, NULL, 'F_FIJA200_LTE', 'COS', 'IC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (295, 3434, NULL, 'F_FIJA300_LTE', 'COS', 'ID', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (296, 3435, NULL, 'F_FIJA400_LTE', 'COS', 'IF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (297, 3436, NULL, 'F_FIJA500_LTE', 'COS', 'IG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (298, 3437, NULL, 'F_FIJA1000_LTE', 'COS', 'II', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (299, 3438, NULL, 'F_FIJA2000_LTE', 'COS', 'IJ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (300, 3439, NULL, 'F_FIJA750_LTE', 'COS', 'IK', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (301, 3440, NULL, 'F_FIJA30_LTE', 'COS', 'IQ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (302, 3441, NULL, 'F_FIJA250_LTE', 'COS', 'IR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (303, 3442, 2390, 'F_FACIL_MEGA_SMART_99_LTE', 'COS', 'IS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (304, 3443, 2390, 'F_FACIL_MEGASMART_149_LTE', 'COS', 'IT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (399, 300002, NULL, 'FN all day 1', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (305, 3444, 2390, 'F_FACIL_MEGASMART_199_LTE', 'COS', 'IU', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (306, 3445, 2390, 'F_FACIL_MEGASMART_249_LTE', 'COS', 'IV', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (307, 3446, NULL, 'PLAN_INTERNO_STAFF_LTE', 'COS', 'IW', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (308, 3447, NULL, 'F_FIJA_100_P_LTE', 'COS', 'IX', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (309, 3450, NULL, 'BOLSA_MIN_800_LTE', 'COS', '1B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (310, 3451, NULL, 'BOLSA_MIN_7500_LTE', 'COS', '2B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (311, 3452, NULL, 'BOLSA_MIN_5000_LTE', 'COS', '3B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (312, 3453, NULL, 'BOLSA_MIN_40000_LTE', 'COS', '4B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (313, 3454, NULL, 'BOLSA_MIN_30000_LTE', 'COS', '5B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (314, 3455, NULL, 'BOLSA_MIN_2500_LTE', 'COS', '6B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (315, 3456, NULL, 'BOLSA_MIN_1500_LTE', 'COS', '7B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (316, 3457, NULL, 'BOLSA_MIN_10000_LTE', 'COS', '8B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (317, 3458, NULL, 'BOLSA_CORP_7500_LTE', 'COS', '9B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (318, 3459, NULL, 'BOLSA_CORP_60000_LTE', 'COS', '0B', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (518, 8012, NULL, 'NEGOCIO_275', 'COS', 'AR', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (319, 3460, NULL, 'BOLSA_CORP_5000_LTE', 'COS', '1C', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (320, 3461, NULL, 'BOLSA_CORP_40000_LTE', 'COS', '2C', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (321, 3462, NULL, 'BOLSA_CORP_30000_LTE', 'COS', '3C', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (322, 3463, NULL, 'BOLSA_CORP_10000_LTE', 'COS', '4C', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (323, 3551, NULL, 'BOLSA_MICRO_EMP_400_LTE', 'COS', 'OG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (324, 3552, NULL, 'BOLSA_MICRO_EMP_500_LTE', 'COS', 'P9', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (325, 3570, NULL, 'TIGO_PREPAGO_SOL', 'COS', '5S', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (326, 3590, NULL, 'PLAN_RASTREO_GPS', 'COS', 'GP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (327, 3610, NULL, 'LTE_FF_AUSPICIO_1500', 'COS', 'A1', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (328, 3611, NULL, 'LTE_FF_AUSPICIO_Ilimitado', 'COS', 'AZ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (329, 3630, 2390, 'F_F_FACIL_VOZ_MB_99_LTE', 'COS', 'LK', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (330, 3650, NULL, 'PLAN_STAFF_DMS_300_LTE', 'COS', '8D', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (331, 3690, NULL, 'PRUEBA_INITIAL_NEG', 'COS', 'NG', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (332, 3730, NULL, 'LTE_FF_180', 'COS', '18', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (333, 3731, NULL, 'LTE_FF_300', 'COS', '30', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (334, 3751, NULL, 'F_FIJA2000_NEW', 'COS', 'W2', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (335, 3773, NULL, 'F_FIJA_750_NEW', 'COS', '7N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (336, 3791, NULL, 'F_FIJA_750_LTE', 'COS', '6N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (337, 3810, NULL, 'EMPRESARIAL_750', 'COS', 'CF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (338, 3811, NULL, 'EMPRESARIAL_500', 'COS', 'CE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (339, 3812, NULL, 'F_FIJA_500_LTE', 'COS', '5N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (340, 3830, NULL, 'F_FIJA_350_LTE', 'COS', '4N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (341, 3831, NULL, 'F_FIJA_250_LTE', 'COS', '3N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (342, 3832, NULL, 'F_FIJA_200_LTE', 'COS', '2N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (343, 3833, NULL, 'F_FIJA_150_LTE', 'COS', '1N', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (344, 3834, 2390, 'F_FIJAFACIL_250_LTE', 'COS', 'PC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (345, 3835, 2390, 'F_FIJAFACIL_200_LTE', 'COS', 'PB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (346, 3836, 2390, 'F_FIJAFACIL_150_LTE', 'COS', 'PA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (347, 3850, NULL, 'TIGO_PRE_FACIL_LTE', 'COS', 'PL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (348, 3870, NULL, 'EMPRESARIAL_100', 'COS', 'CJ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (349, 3910, NULL, 'EMPRESARIAL_40', 'COS', 'CH', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (350, 3911, NULL, 'EMPRESARIAL_250', 'COS', 'CC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (351, 3930, NULL, 'EMPRESARIAL_150', 'COS', 'CA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (352, 3950, NULL, 'EMPRESARIAL_75', 'COS', 'CI', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (353, 3951, NULL, 'EMPRESARIAL_200', 'COS', 'CB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (354, 3952, NULL, 'EMPRESARIAL_350', 'COS', 'CD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (355, 3970, NULL, 'EMPRESARIAL_1100', 'COS', 'CK', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (356, 3971, NULL, 'EMPRESARIAL_2200', 'COS', 'CL', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (357, 3990, NULL, 'ENTERPRISE_150', 'COS', 'CM', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (358, 3991, NULL, 'ENTERPRISE_200', 'COS', 'CN', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (359, 3992, NULL, 'ENTERPRISE_250', 'COS', 'CO', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (360, 3993, NULL, 'BLUE_SINLIMITE_150', 'COS', 'CP', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (361, 3995, NULL, 'BLUE_SINLIMITE_200', 'COS', 'CQ', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (362, 3996, NULL, 'BLUE_SINLIMITE_250', 'COS', 'CR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (363, 4012, NULL, 'BROADBAND_5200MB         ', 'COS', '52', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (364, 4013, NULL, 'BROADBAND_7500MB         ', 'COS', '75', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (365, 4014, NULL, 'BROADBAND_2500MB         ', 'COS', '25', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (366, 4030, NULL, 'LTE_BUSINESS_280         ', 'COS', 'AA', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (367, 4031, NULL, 'LTE_BUSINESS_560         ', 'COS', 'AB', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (368, 4032, NULL, 'LTE_BUSINESS_1000        ', 'COS', 'AC', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (369, 4033, NULL, 'LTE_BUSINESS_1450        ', 'COS', 'AD', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (370, 4034, NULL, 'LTE_BUSINESS_2100        ', 'COS', 'AE', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (371, 4050, NULL, 'ARTURITO_200MB           ', 'COS', 'A6', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (372, 4051, NULL, 'BOLSA_TIGO_BUSINESS      ', 'COS', 'SS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (373, 1150, NULL, 'DATA_200MB_DIARIO', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (374, 1151, NULL, 'DATA_5200MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (375, 1152, NULL, 'DATA_7500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (376, 1153, NULL, 'DATA_10500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (377, 1154, NULL, 'DATA_12500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (378, 1155, NULL, 'DATA_13000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (379, 1156, NULL, 'DATA_1900MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (380, 1157, NULL, 'DATA_1GB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (381, 1160, NULL, 'DATA_20000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (382, 1161, NULL, 'DATA_23000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (383, 1162, NULL, 'DATA_3GB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (384, 1163, NULL, 'DATA_4300MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (385, 1164, NULL, 'DATA_5300MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (386, 1165, NULL, 'DATA_8200MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (387, 1166, NULL, 'DATA_9300MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (388, 1167, NULL, 'DATA_9500MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (389, 2001, NULL, 'ALCO_DIA_WHATSAPP', 'ALCO', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (390, 2002, NULL, 'ALCO_OFER_WHATSAPP', 'ALCO', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (391, 2003, NULL, 'ALCO_SEMANA_WHATSAPP', 'ALCO', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (392, 2490, NULL, 'TIGO_PREPAGO_DPI', 'COS', 'PD', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (393, 2990, 2390, 'F_FIJA_FACIL_VOZ_MB_99', 'COS', 'MR', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (394, 2991, 2390, 'F_FIJA_FACIL_VOZ_MB_199', 'COS', 'MT', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (395, 2992, 2390, 'F_FIJA_FACIL_VOZ_MB_149', 'COS', 'MS', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (396, 2996, 2390, 'F_FIJA_FACIL_VOZ_MB_249', 'COS', 'MU', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (397, 300000, NULL, 'CUG Group Offer', 'CUG', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (398, 300001, NULL, 'VPN Group Offer', 'VPN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (400, 300003, NULL, 'FN all day 2', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (401, 300004, NULL, 'FN all day 3', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (402, 300005, NULL, 'FN all day 4', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (403, 300006, NULL, 'FN all day 5', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (404, 300007, NULL, 'FN time 1', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (405, 300008, NULL, 'FN time 5', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (406, 300009, NULL, 'FN all day 10', 'FN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (407, 3470, NULL, 'TIGO_PREPAGO_LTE', 'COS', 'GT', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (408, 400000, NULL, 'CUG Member Offer', 'CUG', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (409, 400001, NULL, 'VPN Member Offer', 'VPN', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (410, 4011, NULL, 'BROADBAND_3500MB         ', 'COS', 'M9', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (411, 5000, 2610, '2610_PLAN_STAFF_1000', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (412, 5001, 2611, '2611_PLAN_STAFF_500', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (413, 5002, 2810, '2810_PLAN_STAFF_100', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (414, 5003, 2811, '2811_PLAN_STAFF_300', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (415, 5004, 3170, '3170_PLAN_STAFF_VENTAS_300', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (416, 5005, 3391, '3391_PLAN_STAFF_300_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (504, 999, 727, 'kiko', 'ALCO', '110', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (417, 5006, 3410, '3410_PLAN_STAFF_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (418, 5007, 3411, '3411_STAFF_VENTAS_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (419, 5008, 3412, '3412_PLAN_STAFF_500_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (420, 5009, 3413, '3413_PLAN_STAFF_1000_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (421, 5010, 3414, '3414_PLAN_STAFF_100_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (422, 5011, 3415, '3415_PLAN_STAFF_VENTAS_300_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (423, 5012, 3650, '3650_PLAN_STAFF_DMS_300_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (424, 5013, 1890, '1890_F_FIJA_SEGURIDAD_75', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (425, 5014, 1891, '1891_F_FIJA_SEGURIDAD_100', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (426, 5015, 1892, '1892_F_FIJA_SEGURIDAD_150', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (427, 5016, 1893, '1893_F_FIJA_SEGURIDAD_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (428, 5017, 1894, '1894_F_FIJA_SEGURIDAD_300', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (429, 5018, 1895, '1895_F_FIJA_SEGURIDAD_400', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (430, 5019, 1896, '1896_F_FIJA_SEGURIDAD_500', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (431, 5020, 3791, '3791_F_FIJA_750_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (432, 5021, 3810, '3810_EMPRESARIAL_750', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (433, 5022, 3811, '3811_EMPRESARIAL_500', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (434, 5023, 3812, '3812_F_FIJA_500_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (435, 5024, 3830, '3830_F_FIJA_350_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (436, 5025, 3831, '3831_F_FIJA_250_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (437, 5026, 3832, '3832_F_FIJA_200_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (438, 5027, 3833, '3833_F_FIJA_150_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (439, 5028, 3870, '3870_EMPRESARIAL_100', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (440, 5029, 3911, '3911_EMPRESARIAL_250', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (441, 5030, 3930, '3930_EMPRESARIAL_150', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (442, 5031, 3950, '3950_EMPRESARIAL_75', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (443, 5032, 3951, '3951_EMPRESARIAL_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (444, 5033, 3952, '3952_EMPRESARIAL_350', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (445, 5034, 3970, '3970_EMPRESARIAL_1100', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (446, 5035, 3971, '3971_EMPRESARIAL_2200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (447, 5036, 3990, '3990_ENTERPRISE_150', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (448, 5037, 3991, '3991_ENTERPRISE_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (449, 5038, 3992, '3992_ENTERPRISE_250', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (450, 5039, 3993, '3993_BLUE_SINLIMITE_150', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (451, 5040, 3995, '3995_BLUE_SINLIMITE_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (452, 5041, 3996, '3996_BLUE_SINLIMITE_250', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (456, 5045, 1853, '1853_F_FIJA_SMARTH_420', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (457, 5046, 1854, '1854_F_FIJA_SMARTH_500', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (458, 5047, 1855, '1855_F_FIJA_SMARTH_600', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (459, 5048, 1970, '1970_F_FIJA_MINISMART_110', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (460, 5049, 1971, '1971_F_FIJA_MINISMART_160', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (461, 5050, 1972, '1972_F_FIJA_MINISMART_220', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (462, 5051, 1973, '1973_F_FIJA_MINISMART_300', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (463, 5052, 1974, '1974_F_FIJA_MINISMART_380', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (464, 5053, 1975, '1975_F_FIJA_MINISMART_450', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (465, 5054, 1976, '1976_F_FIJA_MINISMART_550', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (466, 5055, 2171, '2171_F_FIJA_IPHONE_590', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (467, 5056, 2172, '2172_F_FIJA_IPHONE_780', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (468, 5057, 2173, '2173_F_FIJA_IPHONE_300', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (469, 5058, 2174, '2174_F_FIJA_IPHONE_390', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (470, 5059, 2176, '2176_F_FIJA_IPHONE_450', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (471, 5060, 2177, '2177_F_FIJA_IPHONE_1200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (472, 5061, 727, '727_PLAN_STAFF', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (473, 5062, 3372, '3372_F_FIJA_IPHONE_300_LTE', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (474, 5063, NULL, 'DATA_150MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (475, 5064, NULL, 'DATA_18000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (476, 5065, NULL, 'DATA_6000MB', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (477, 5066, 3730, 'SMS_50', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (478, 5067, NULL, '3791_F_FIJA_750_LTE_SEG', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (479, 5068, NULL, '3810_EMPRESARIAL_750_SEG', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (480, 5069, NULL, '3970_EMPRESARIAL_1100_SEG', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (481, 5070, NULL, '3971_EMPRESARIAL_2200_SEG', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (453, 5042, 1850, '1850_F_FIJA_SMARTH_150', 'PC', '', 0, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (506, 110, NULL, '110', 'ALCO', '', NULL, 'f');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (482, 727, NULL, 'PLAN_STAFF', 'COS', 'SF', 2, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (455, 5044, 1852, '1852_F_FIJA_SMARTH_320', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (520, 8013, NULL, 'NEGOCIO_375', 'COS', 'AS', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (532, 8030, NULL, 'ENTERPRISE_PLUS_350', 'COS', 'EJ', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (454, 5043, 1851, '1851_F_FIJA_SMARTH_200', 'PC', '', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (570, 8041, NULL, 'MEGA_PLAN_165   ', 'COS', 'MM', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (582, 8047, NULL, 'EMPRESA_100', 'COS', 'HL', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (594, 8053, NULL, 'EMPRESA_600', 'COS', 'HR', NULL, 't');
INSERT INTO "CSU_LOCAL"."mapeo_ofertas" VALUES (606, 8059, NULL, 'EMPRESA_PLUS_350', 'COS', 'HX', NULL, 't');



CREATE TABLE "CSU_LOCAL"."mensaje" (
  "mensaje_id" integer NOT NULL,
  "descripcion" varchar(100) ,
  "mensaje_texto" varchar(300),
  "fecha_inicio" timestamp(6),
  "fecha_fin" timestamp(6),
  "prioridad" float,
  "longitud" float,
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."mensaje" VALUES ('19', '1234', '25', TO_TIMESTAMP('2019-07-10 00:00:00.268000', 'SYYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP('2020-02-13 14:22:03.730000', 'SYYYY-MM-DD HH24:MI:SS:FF6'), '99.000000000000000', '2.0000000000000000', '1');



CREATE TABLE "CSU_LOCAL"."mensaje_defecto" (
  "mensaje_id" integer NOT NULL,
  "inicial" varchar(300) ,
  "promocional" varchar(300) ,
  "postpago" varchar(300) ,
  "visible_vigencia" float,
  "comverse_errores" varchar(300) ,
  "comverse_off" varchar(300) 
)
;



CREATE TABLE "CSU_LOCAL"."menu" (
  "menu_id" number(8) NOT NULL,
  "nombre" varchar(50) ,
  "posicion" number(8),
  "visible" varchar(5) ,
  "nivel" number(4),
  "padre" number(4),
  "descripcion" varchar(350) ,
  "mostarMayorCero" varchar(11) ,
  "canal" varchar(11) ,
  "estado" varchar(1) ,
  "confi_id" number(8),
  "informacion" varchar(30) ,
  "mostarMayorCeroAcumulado" varchar(11) ,
  "tienehijos" varchar(11) ,
  "unit_type_id" number(8) DEFAULT 2,
  "tieneBilletera" varchar(11) 
)
;



INSERT INTO "CSU_LOCAL"."menu" VALUES (284, 'Plan Mensual Internet:', 2, 'f', 2, 265, 'Plan Mensual Internet:', 't', 'TODOS', 't', 17, 'PAQUETIGOS_ACUMULADOS', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (295, 'Plan Mensual Internet:', 2, 'f', 2, 290, '', 't', 'TODOS', 't', 22, 'PAQUETIGOS_ACUMULADOS', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (265, 'Saldo Paquetigos', 1, 'f', 1, NULL, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (266, 'Otros Saldos', 2, 'f', 1, NULL, NULL, 't', 'TODOS', 't', 17, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (267, 'Mas Info de Reserva', 3, 'f', 1, NULL, 'Mas Info. de Reserva', 't', 'USSD', 't', 17, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (268, 'Mas Info de Reserva', 4, 'f', 1, NULL, 'Mas Info. de Reserva', 't', 'SMS', 't', 17, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (269, 'Paq Acumulados', 1, 'f', 2, 265, NULL, 'f', 'TODOS', 't', 17, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (270, 'Paq Nocturno Internet:', 4, 'f', 2, 265, 'Paq Nocturno Internet:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (271, 'Paq Hora Internet:', 5, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (272, 'Paq Dia Internet:', 6, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (273, 'Paq 2 Dias Internet:', 7, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (274, 'Paq 5 Dias Internet:', 8, 'f', 2, 265, 'Paq 5Dias Internet:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (275, 'Paq Semana Internet:', 9, 'f', 2, 265, 'Paq Semana Internet:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (276, 'Paq 15 Dias Internet:', 10, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (278, 'Plan Mensual Llamadas:', 3, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (279, 'Paq 2 Dias Llamadas:', 14, 'f', 2, 265, 'Paq 2 Dias Llamadas:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (280, 'Paq 5 Dias Llamadas:', 15, 'f', 2, 265, 'Paq 5 Dias Llamadas:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (281, 'Paq 7 Dias Llamadas:', 16, 'f', 2, 265, 'Paq 7 Dias Llamadas:', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (283, 'Paq Llamadas:', 11, 'f', 2, 265, 'Segundos', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (285, 'Saldo Paquetigos', 1, 'f', 1, NULL, 'Saldo paquetigos', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (286, 'Otros Saldos', 2, 'f', 1, NULL, NULL, 't', 'TODOS', 't', 21, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (287, 'Mas info reserva', 3, 'f', 1, NULL, 'Prueba ussd', 't', 'USSD', 't', 21, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (288, 'Mas info reserva', 4, 'f', 1, NULL, 'Prueba SMS', 't', 'SMS', 't', 21, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (290, 'Saldo Paquetigos', 1, 'f', 1, NULL, 'Saldo Paquetigos', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (291, 'Otros Saldos', 2, 'f', 1, NULL, 'Otros Saldos', 't', 'TODOS', 't', 22, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (292, 'Que es la Reserva', 3, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en www.tigo.com.bo La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'USSD', 't', 22, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (246, 'Paq Hora Internet:', 4, 'f', 2, 242, 'Paq Hora', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (252, 'Paq Nocturno Internet:', 5, 'f', 2, 242, 'Paq Nocturno Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (247, 'Paq Semana Internet:', 9, 'f', 2, 242, 'Paq Semana Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (245, 'Paq Dia Internet:', 6, 'f', 2, 242, 'Paq Dia Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (249, 'Paq 2 Dias Internet:', 7, 'f', 2, 242, 'Paq 2 Dias Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (250, 'Paq 5 Dias Internet:', 8, 'f', 2, 242, 'Paq 5 Dias Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (251, 'Paq 15 Dias Internet:', 10, 'f', 2, 242, 'Paq 15 Dias Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (248, 'Paq Mes Internet:', 11, 'f', 2, 242, 'Paq Mes Internet', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (256, 'Paq Dia Llamada:', 12, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (257, 'Paq 2 Dias Llamadas:', 15, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (258, 'Paq 5 Dias Llamadas:', 16, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (259, 'Paq 7 Dias Llamadas:', 17, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (260, 'Paq 15 Dias Llamadas:', 18, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (261, 'Paq Llamadas:', 19, 'f', 2, 242, NULL, 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (603, 'B1K', 23, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (613, 'Seg 3 Dias No Acum', 28, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (264, 'Otros Saldos', 2, 'f', 1, NULL, 'Otros Saldos', 't', 'TODOS', 't', 18, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (263, 'Que es la Reserva', 4, 't', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en https://goo.gl/k7NYWR La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 'f', 'SMS', 't', 18, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (242, 'Saldo Paquetigos', 1, 'f', 1, NULL, 'Saldo Paquetigos', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (623, 'Min Plan', 33, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (293, 'Que es la Reserva', 4, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en https://goo.gl/k7NYWR La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'SMS', 't', 22, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (294, 'Paq Acumulados', 1, 'f', 2, 290, NULL, 'f', 'TODOS', 't', 22, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (297, 'Paq Nocturno Internet:', 4, 'f', 2, 290, 'Paq Nocturno Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (298, 'Paq Hora Internet:', 3, 'f', 2, 290, 'Paq Hora Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (299, 'Paq Dia Internet:', 5, 'f', 2, 290, 'Paq Dia Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (300, 'Paq 2Dias Internet:', 6, 'f', 2, 290, 'Paq 2Dias Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (301, 'Paq 5Dias Internet:', 7, 'f', 2, 290, 'Paq 5Dias Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (302, 'Paq Semana Internet:', 8, 'f', 2, 290, 'Paq Semana Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (303, 'Paq 15Dias Internet:', 9, 'f', 2, 290, 'Paq 15Dias Internet:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (305, 'Paq 2 Dias Llamadas:', 13, 'f', 2, 290, 'Paq 2 Dias Llamadas:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (306, 'Paq 5 Dias Llamadas:', 14, 'f', 2, 290, 'Paq 5 Dias Llamadas:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (307, 'Paq 7 Dias Llamadas:', 15, 'f', 2, 290, 'Paq 7 Dias Llamadas:', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (308, 'Paq 15 Dias Llamadas:', 16, 'f', 2, 290, NULL, 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (309, 'Paq Llamadas:', 10, 'f', 2, 290, 'Segundos', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (312, 'Saldo Paquetigos', 1, 'f', 1, NULL, 'Saldo Paquetigos', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (313, 'Otros Saldos', 2, 'f', 1, NULL, 'Otros Saldos', 't', 'TODOS', 't', 23, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (314, 'Que es la Reserva', 3, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en www.tigo.com.bo La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'USSD', 't', 23, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (315, 'Que es la Reserva', 4, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en https://goo.gl/k7NYWR La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'SMS', 't', 23, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (316, 'Paq Acumulados', 1, 'f', 2, NULL, NULL, 'f', 'TODOS', 't', 23, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (318, 'Plan Mensual Llamadas:', 4, 'f', 2, 312, NULL, 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (319, 'Paq Nocturno Internet:', 5, 'f', 2, 312, 'Paq Nocturno Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (320, 'Paq Hora Internet:', 6, 'f', 2, 312, 'Paq Hora Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (321, 'Paq Dia Internet:', 7, 'f', 2, 312, NULL, 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (322, 'Paq 2 Dias Internet:', 8, 'f', 2, 312, 'Paq 2Dias Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (323, 'Paq 5 Dias Internet:', 9, 'f', 2, 312, 'Paq 5Dias Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (324, 'Paq Semana Internet:', 10, 'f', 2, 312, 'Paq Semana Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (325, 'Paq 15 Dias Internet:', 11, 'f', 2, 312, 'Paq 15Dias Internet:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (327, 'Paq 2 Dias Llamadas:', 15, 'f', 2, 312, 'Paq 2 Dias Llamadas:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (328, 'Paq 5 Dias Llamadas:', 16, 'f', 2, 312, 'Paq 5 Dias Llamadas:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (329, 'Paq 7 Dias Llamadas:', 17, 'f', 2, 312, NULL, 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (330, 'Paq 15 Dias Llamadas:', 18, 'f', 2, 312, NULL, 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (331, 'Paq Llamadas:', 12, 'f', 2, 312, 'Segundos', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (332, 'Paq 15 Dias Llamadas:', 17, 'f', 2, 265, NULL, 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (333, 'Paq Acumulados', 1, 'f', 2, 285, 'Paquetigos acumulados', 'f', 'TODOS', 't', 21, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (335, 'Plan Mensual Llamadas:', 4, 'f', 2, 285, 'Paquete mensual llamadas', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (336, 'Paq Nocturno Internet:', 5, 'f', 2, 285, 'Paquetigo nocturno internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (337, 'Paq Hora Internet:', 6, 'f', 2, 285, 'Paquetigo hora internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (338, 'Paq Dia Internet:', 7, 'f', 2, 285, 'paquetigo diario internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (339, 'Paq 2 Dias Internet:', 8, 'f', 2, 285, 'paquetigo 2 dÃ­as internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (340, 'Paq 5 Dias Internet:', 9, 'f', 2, 285, 'paquetigo 5 dÃ­as internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (341, 'Paq Semana Internet:', 10, 'f', 2, 285, 'paquetigo semana internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (334, 'Plan Mensual Internet:', 3, 'f', 2, 285, '', 't', 'TODOS', 't', 21, 'PAQUETIGOS_ACUMULADOS', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (342, 'Paq 15 Dias Internet:', 11, 'f', 2, 285, 'paquetigo 15 dÃ­as internet', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (343, 'Paq 2 Dias Llamadas:', 15, 'f', 2, 285, 'paquetigo 2 dÃ­as llamadas', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (344, 'Paq 5 Dias Llamadas:', 16, 'f', 2, 285, 'paquetigo 5 dÃ­as llamadas', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (345, 'Paq 7 Dias Llamadas', 17, 'f', 2, 285, 'paquetigo 7 dÃ­as llamadas', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (346, 'Paq 15 Dias Llamadas:', 18, 'f', 2, 285, 'paquetigo 15 dÃ­as llamadas', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (347, 'Paq Llamadas:', 12, 'f', 2, 285, 'segundos', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (348, 'Saldo Paquetigos', 1, 'f', 1, NULL, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (349, 'Otros Saldos', 2, 'f', 1, NULL, NULL, 't', 'TODOS', 't', 3, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (350, 'Que es la Reserva', 3, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en www.tigo.com.bo La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'USSD', 't', 3, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (351, 'Que es la Reserva', 4, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en https://goo.gl/k7NYWR La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'SMS', 't', 3, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (352, 'Paq Acumulados', 1, 'f', 2, 348, NULL, 'f', 'TODOS', 't', 3, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (353, 'Paq Nocturno Internet:', 5, 'f', 2, 348, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (354, 'Paq Hora Internet:', 4, 'f', 2, 348, 'Paq Hora Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (355, 'Paq Dia Internet:', 6, 'f', 2, 348, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (356, 'Paq 2 Dias Internet:', 7, 'f', 2, 348, 'Paq 2Dias Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (357, 'Paq 5 Dias Internet:', 8, 'f', 2, 348, 'Paq 5Dias Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (358, 'Paq Semana Internet:', 9, 'f', 2, 348, 'Paq Semana Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (359, 'Paq 15 Dias Internet:', 10, 'f', 2, 348, 'Paq 15Dias Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (360, 'Paq Mes Internet:', 11, 'f', 2, 348, 'Paq Mes Internet:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (361, 'Paq Dia Llamada:', 12, 'f', 2, 348, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (362, 'Paq 2 Dias Llamadas:', 16, 'f', 2, 348, 'Paq 2 Dias Llamadas:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (363, 'Paq 5 Dias Llamadas:', 17, 'f', 2, 348, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (364, 'Paq 7 Dias Llamadas:', 18, 'f', 2, 348, 'Paq 7 Dias Llamadas:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (365, 'Paq 15 Dias Llamadas:', 19, 'f', 2, 348, 'Paq 15 Dias Llamadas:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (366, 'Paq Llamadas:', 13, 'f', 2, 348, NULL, 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (367, 'Paq Dia LDI USA:', 13, 'f', 2, 312, 'PAQUETIGOS LDI USA', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (368, 'Paq Dia LDI SUD:', 14, 'f', 2, 312, 'Paquteigo dia LDI Sudamerica', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (370, 'Paq Dia LDI USA:', 11, 'f', 2, 290, 'Paquetigo dia LDI USA', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (371, 'Paq Dia LDI SUD:', 12, 'f', 2, 290, 'Paquetigo dia LDI SUD', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (372, 'Paq Dia LDI USA:', 13, 'f', 2, 285, 'Paquetigo dia LDI USA', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (373, 'Paq Dia LDI SUD:', 14, 'f', 2, 285, 'Paquetigo dia LDI SUD', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (376, 'Paq Dia LDI USA:', 12, 'f', 2, 265, 'Paquetigo dia LDI USA', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (377, 'Paq Dia LDI SUD:', 13, 'f', 2, 265, 'Paquetigo dia LDI Sudamerica', 't', 'TODOS', 't', 17, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (378, 'Paq Dia LDI USA:', 14, 'f', 2, 348, 'Paquetigo dia LDI USA', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (379, 'Paq Dia LDI SUD:', 15, 'f', 2, 348, 'Paquetigo dia LDI Sudamerica', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (384, 'Whatsapp ilimitado dia', 2, 'f', 2, 285, 'whatsapp', 't', 'TODOS', 't', 21, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (385, 'Whatsapp ilimitado dia', 2, 'f', 2, 348, 'whatsapp', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (386, 'Whatsapp ilimitado semana', 3, 'f', 2, 348, 'whatsapp', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (388, 'Saldo Paquetigos', 1, 'f', 1, NULL, 'Saldo Paquetigos', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (389, 'Otros Saldos', 2, 'f', 1, NULL, 'Otros Saldos', 't', 'TODOS', 't', 55, 'OTROS_SALDOS', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (390, 'Que es la Reserva', 3, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en www.tigo.com.bo La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'USSD', 't', 55, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (391, 'Que es la Reserva', 4, 'f', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en https://goo.gl/k7NYWR La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 't', 'SMS', 't', 55, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (605, 'Cms Promo', 24, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (374, 'Paq Dia LDI USA:', 13, 'f', 2, 242, 'Paquetigo dia LDI USA', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (382, 'Whatsapp ilimitado semana', 3, 'f', 2, 242, 'whatsapp', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (625, 'Mb Promo', 34, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (615, 'MB 1 Dia No Acum', 29, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (392, 'Paq Acumulados', 1, 'f', 2, 388, 'Paq Acumulados', 'f', 'TODOS', 't', 55, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (393, 'Whatsapp ilimitado dia', 2, 'f', 2, 388, 'whatsapp', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (394, 'Whatsapp ilimitado semana', 3, 'f', 2, 388, 'whatsapp', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (395, 'Paq Hora Internet:', 5, 'f', 2, 388, 'Paq Hora Internet:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (396, 'Paq Nocturno Internet:', 6, 'f', 2, 388, 'Paq Nocturno Internet:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (397, 'Paq Dia Internet:', 7, 'f', 2, 388, 'Paq Dia Internet', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (398, 'Paq 2 Dias Internet:', 8, 'f', 2, 388, 'Paq 2 Dias Internet
', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (399, 'Paq 5 Dias Internet:', 9, 'f', 2, 388, 'Paq 5 Dias Internet', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (400, 'Paq Semana Internet:', 10, 'f', 2, 388, 'Paq Semana Internet', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (401, 'Paq 15 Dias Internet:', 11, 'f', 2, 388, 'Paq 15 Dias Internet', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (402, 'Paq Mes Internet:', 12, 'f', 2, 388, 'Paq Mes Internet', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (403, 'Paq Dia Llamadas:', 13, 'f', 2, 388, 'Paq Dia Llamadas', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (404, 'Paq Dia LDI USA:', 14, 'f', 2, 388, 'Paq Dia LDI USA', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (405, 'Paq Dia LDI SUD:', 15, 'f', 2, 388, 'Paq Dia LDI SUD', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (406, 'Paq 2 Dias Llamadas:', 16, 'f', 2, 388, 'Paq 2 Dias Llamadas:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (407, 'Paq 5 Dias Llamadas:', 17, 'f', 2, 388, 'Paq 5 Dias Llamadas:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (408, 'Paq 7 Dias Llamadas:', 18, 'f', 2, 388, 'Paq 7 Dias Llamadas:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (409, 'Paq 15 Dias Llamadas:', 19, 'f', 2, 388, 'Paq 15 Dias Llamadas:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (410, 'Paq Mes Llamadas:', 20, 'f', 2, 388, 'Paq Mes Llamadas:', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (411, 'Whatsapp ilimitado mensual', 4, 'f', 2, 388, 'whatsapp', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (412, 'Llamadas:', 19, 'f', 2, 312, NULL, 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (413, 'Llamadas:', 20, 'f', 2, 348, 'Llamadas:', 't', 'TODOS', 't', 3, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (414, 'LLamadas:', 21, 'f', 2, 388, 'Llamadas', 't', 'TODOS', 't', 55, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (415, 'Llamadas:', 17, 'f', 2, 290, 'Llamadas', 't', 'TODOS', 't', 22, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (317, 'Plan Mensual Internet:', 3, 'f', 2, 312, 'Plan Mensual Internet:', 't', 'TODOS', 't', 23, 'PAQUETIGOS_ACUMULADOS', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (417, 'Plan MB:', 2, 'f', 2, 312, 'Plan MB:', 't', 'TODOS', 't', 23, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (607, 'Mb Plan', 25, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (617, 'MB a Vencer', 30, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (627, 'Min Promo', 35, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (629, 'hhh', 5, 'f', 1, NULL, 'hhh', 't', 'TODOS', 'f', 18, 'SALDO_PAQUETIGO', 't', 't', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (262, 'Que es la Reserva', 3, 't', 1, NULL, 'Siempre puedes disponer de la reserva. Mas informacion en www.tigo.com.bo La reserva es un requisito tecnico para mantener activa tu conexion a internet.', 'f', 'USSD', 't', 18, 'INFO_DE_RESERVA', 'f', 'f', NULL, 'f');
INSERT INTO "CSU_LOCAL"."menu" VALUES (599, 'Segundos Simplificados', 21, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (609, 'Mb 3 Dias', 26, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (619, 'Seg a Vencer', 31, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (244, 'Paq Acumulados', 1, 'f', 2, 242, 'Paq. Acumulados', 'f', 'TODOS', 't', 18, 'PAQUETIGOS_ACUMULADOS', 't', 'f', 139, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (380, 'Whatsapp ilimitado dia', 2, 'f', 2, 242, 'whatsapp', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (375, 'Paq Dia LDI SUD:', 14, 'f', 2, 242, 'Paquetigo dia LDI Suudamerica', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (416, 'Llamadas:', 20, 'f', 2, 242, 'Llamadas:', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 3, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (601, 'Promo Recarga', 22, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (611, 'Mb 3 Dias No Acum', 27, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');
INSERT INTO "CSU_LOCAL"."menu" VALUES (621, 'Data Plan', 32, 'f', 2, 242, '', 't', 'TODOS', 't', 18, 'SALDO_PAQUETIGO', 'f', 'f', 137, 't');



CREATE TABLE "CSU_LOCAL"."occ" (
  "occ_id" number(8) NOT NULL,
  "config_id" number(8),
  "origen_id" number(8),
  "corto_id" number(8),
  "cos_id" number(8),
  "posicion" number(8),
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."occ" VALUES (1, 5, 0, 0, 20, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (2, 5, 0, 0, 17, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (3, 5, 0, 0, 3, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (4, 5, 0, 0, 15, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (5, 5, 0, 0, 4, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (6, 5, 0, 0, 16, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (7, 5, 0, 0, 18, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (8, 5, 0, 0, 7, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (9, 5, 0, 0, 12, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (10, 5, 0, 0, 8, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (11, 5, 0, 0, 14, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (12, 5, 0, 0, 13, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (13, 5, 0, 0, 9, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (14, 5, 0, 0, 11, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (15, 5, 0, 0, 19, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (16, 5, 0, 0, 2, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (17, 5, 0, 0, 5, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (18, 0, 0, 0, 10, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (19, 0, 0, 0, 6, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (20, 10, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (21, 11, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (22, 13, 0, 0, 22, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (24, 11, 0, 0, 26, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (25, 11, 0, 0, 27, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (26, 11, 0, 0, 29, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (27, 11, 0, 0, 25, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (28, 11, 0, 0, 24, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (29, 11, 0, 0, 28, 6, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (30, 17, 0, 0, 36, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (31, 17, 0, 0, 35, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (32, 17, 0, 0, 39, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (33, 17, 0, 0, 38, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (34, 17, 0, 0, 37, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (35, 19, 0, 0, 37, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (36, 17, 0, 0, 41, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (37, 17, 0, 0, 42, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (38, 17, 0, 0, 43, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (39, 17, 0, 0, 44, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (40, 17, 0, 0, 40, 6, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (41, 17, 0, 0, 37, 7, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (42, 17, 0, 1, 41, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (43, 17, 0, 1, 38, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (44, 17, 0, 1, 42, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (45, 17, 0, 1, 43, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (46, 17, 0, 1, 44, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (47, 17, 0, 1, 39, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (48, 17, 0, 1, 40, 6, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (49, 17, 0, 1, 37, 7, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (50, 17, 0, 3, 40, 8, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (51, 17, 0, 3, 38, 9, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (52, 17, 0, 3, 42, 10, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (53, 17, 0, 3, 43, 11, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (54, 17, 0, 3, 44, 12, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (55, 17, 0, 3, 39, 13, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (56, 17, 0, 3, 37, 14, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (57, 17, 0, 4, 41, 15, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (58, 17, 0, 4, 38, 16, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (59, 17, 0, 4, 42, 17, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (60, 17, 0, 4, 43, 18, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (61, 17, 0, 4, 44, 19, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (62, 17, 0, 4, 39, 20, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (63, 17, 0, 4, 40, 21, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (64, 17, 0, 4, 37, 22, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (65, 17, 0, 5, 41, 23, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (66, 17, 0, 5, 38, 24, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (67, 17, 0, 5, 42, 25, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (68, 17, 0, 5, 43, 26, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (69, 17, 0, 5, 44, 27, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (70, 17, 0, 5, 39, 28, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (71, 17, 0, 5, 40, 29, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (72, 17, 0, 5, 37, 30, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (73, 17, 0, 2, 41, 31, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (74, 17, 0, 2, 38, 32, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (75, 17, 0, 2, 42, 33, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (76, 17, 0, 2, 43, 34, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (77, 17, 0, 2, 44, 35, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (78, 17, 0, 2, 39, 36, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (79, 17, 0, 2, 40, 37, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (80, 17, 0, 2, 37, 38, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (81, 17, 0, 0, 41, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (82, 17, 0, 0, 38, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (83, 17, 0, 0, 42, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (84, 17, 0, 0, 43, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (85, 17, 0, 0, 44, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (86, 17, 0, 0, 39, 6, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (87, 17, 0, 0, 40, 7, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (88, 17, 0, 0, 37, 7, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (89, 21, 0, 0, 45, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (90, 22, 0, 0, 37, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (91, 22, 0, 0, 46, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (92, 21, 0, 0, 47, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (93, 21, 0, 0, 48, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (96, 21, 0, 0, 51, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (97, 21, 0, 0, 52, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (95, 23, 0, 0, 50, 1, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (94, 23, 0, 0, 49, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (100, 22, 0, 0, 55, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (101, 22, 0, 0, 56, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (102, 21, 0, 0, 57, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (108, 18, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (109, 18, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (110, 18, 0, 0, 64, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (111, 18, 0, 0, 65, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (112, 18, 0, 0, 64, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (113, 18, 0, 0, 65, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (114, 17, 0, 0, 45, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (120, 23, 0, 0, 71, 14, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (121, 22, 0, 0, 71, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (122, 17, 0, 0, 72, 8, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (123, 24, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (129, 22, 0, 0, 73, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (130, 27, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (131, 17, 0, 0, 81, 9, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (132, 17, 0, 0, 82, 10, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (167, 53, 0, 1, 60, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (168, 18, 0, 16, 64, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (169, 18, 0, 16, 65, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (170, 18, 0, 17, 64, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (171, 18, 0, 17, 65, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (172, 18, 0, 16, 6, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (173, 22, 0, 16, 37, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (174, 22, 0, 16, 46, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (175, 22, 0, 16, 55, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (176, 22, 0, 16, 56, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (177, 22, 0, 16, 71, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (178, 22, 0, 16, 73, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (181, 18, 0, 0, 64, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (182, 18, 0, 0, 21, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (183, 22, 0, 0, 37, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (184, 22, 0, 0, 46, 1, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (185, 22, 0, 0, 55, 2, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (186, 22, 0, 0, 56, 3, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (187, 22, 0, 0, 71, 4, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (188, 22, 0, 0, 73, 5, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (191, 5, 0, 0, 21, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (193, 55, 0, 18, 26, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (194, 55, 0, 18, 27, 1, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (195, 55, 0, 18, 29, 2, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (196, 55, 0, 18, 25, 3, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (197, 55, 0, 18, 24, 4, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (198, 55, 0, 18, 28, 5, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (199, 55, 0, 18, 23, 6, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (200, 55, 0, 18, 245, 7, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (201, 55, 0, 18, 246, 8, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (202, 55, 0, 18, 247, 9, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (203, 55, 0, 18, 248, 10, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (204, 55, 0, 18, 249, 11, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (205, 55, 0, 18, 250, 12, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (206, 55, 0, 2, 250, 13, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (207, 55, 0, 2, 23, 14, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (208, 55, 0, 2, 28, 15, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (209, 55, 0, 2, 249, 16, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (210, 55, 0, 2, 24, 17, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (211, 55, 0, 2, 25, 18, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (212, 55, 0, 2, 248, 19, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (213, 55, 0, 2, 29, 20, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (214, 55, 0, 2, 247, 21, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (215, 55, 0, 2, 27, 22, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (216, 55, 0, 2, 26, 23, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (217, 55, 0, 2, 246, 24, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (218, 55, 0, 2, 245, 25, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (219, 55, 0, 2, 244, 26, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (220, 55, 0, 16, 246, 27, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (221, 55, 0, 16, 29, 28, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (223, 55, 0, 16, 25, 29, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (99, 23, 0, 0, 54, 3, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (179, 23, 0, 0, 57, 4, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (103, 23, 0, 0, 58, 5, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (105, 23, 0, 0, 60, 7, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (106, 23, 0, 0, 61, 8, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (107, 23, 0, 0, 62, 9, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (115, 23, 0, 0, 66, 10, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (116, 23, 0, 0, 67, 11, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (117, 23, 0, 0, 68, 12, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (118, 23, 0, 0, 69, 13, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (119, 23, 0, 0, 70, 14, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (124, 23, 0, 0, 74, 15, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (125, 23, 0, 0, 75, 16, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (126, 23, 0, 0, 76, 17, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (127, 23, 0, 0, 77, 18, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (128, 23, 0, 0, 78, 19, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (224, 23, 0, 0, 47, 20, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (225, 23, 0, 0, 48, 21, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (226, 23, 0, 0, 51, 22, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (227, 23, 0, 0, 52, 23, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (229, 23, 0, 0, 72, 24, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (230, 23, 0, 0, 45, 25, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (231, 23, 0, 0, 40, 26, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (189, 18, 0, 0, 64, 1, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (98, 23, 0, 0, 53, 2, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (242, 55, 0, 0, 25, 27, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (243, 55, 0, 0, 29, 28, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (245, 55, 0, 0, 244, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (246, 55, 0, 0, 245, 1, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (247, 55, 0, 0, 246, 2, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (248, 55, 0, 0, 26, 3, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (249, 55, 0, 0, 27, 4, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (250, 55, 0, 0, 247, 5, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (251, 55, 0, 0, 29, 6, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (252, 55, 0, 0, 248, 7, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (253, 55, 0, 0, 25, 8, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (254, 55, 0, 0, 24, 9, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (255, 55, 0, 0, 249, 10, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (256, 55, 0, 0, 28, 11, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (257, 55, 0, 0, 250, 12, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (258, 55, 0, 0, 23, 13, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (104, 23, 0, 0, 59, 6, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (232, 23, 0, 0, 41, 27, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (233, 23, 0, 0, 38, 28, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (234, 23, 0, 0, 42, 29, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (235, 23, 0, 0, 43, 30, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (236, 23, 0, 0, 44, 31, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (237, 23, 0, 0, 39, 32, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (238, 23, 0, 0, 81, 33, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (239, 23, 0, 0, 82, 34, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (23, 3, 0, 0, 0, 0, 'f');
INSERT INTO "CSU_LOCAL"."occ" VALUES (241, 5, 0, 0, 21, 0, 't');
INSERT INTO "CSU_LOCAL"."occ" VALUES (180, 18, 0, 0, 65, 0, 't');


CREATE TABLE "CSU_LOCAL"."origen" (
  "origen_id" number(8) NOT NULL,
  "nombre" varchar(50) ,
  "descripcion" varchar(80) ,
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."origen" VALUES (0, '*', 'Todos', 't');
INSERT INTO "CSU_LOCAL"."origen" VALUES (1, '77806933', '77806933', 'f');



CREATE TABLE "CSU_LOCAL"."parametro" (
  "nombre" varchar(50)  NOT NULL,
  "valor" varchar(160) ,
  "descripcion" varchar(200) ,
  "estado" varchar(1) 
);



INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_cantidad_decimal', '1', 'asdf', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_direccion_ip', 'http://172.31.89.83/ConsultaAcumuladosMegas/ConsultaAcumuladosMegas?wsdl', 'a', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_monto_maximo', '99999', 'asdf', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_monto_minimo', '0', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_nombre_comercial', 'Internet(DPI)', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_operador', 'NINGUNO', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_prefijo_unidad', 'MB', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_sw_mostrar_si_saldo_mayor_cero', 'true', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_sw_mostrar_vigencia', 'false', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('ac_megas_valor_operacion', '0', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('defaul_texto_CSU_LOCAL_ok', 'Consulta Satifactoria', 'Este parametro es para indicar que se realizo la CSU_LOCAL satisfactoriamente', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_configuracion_id', '3', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_mostrar_dpi', 'false', 'iii', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_mostrar_saldo_expirado', 'false', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_opcion_no_valida', 'Linea : %TELEFONO% |Opcion %opc% No Valida', 'opcion no valida', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_saludo_inicial_final', 'Linea : %TELEFONO%', 'test', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_billetera_equivalente', 'GPRS_SUBSCRIPTIONS', 'AAaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_cantidad_decimal', '0', 'asdf', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_direccion_ip', 'http://172.16.64.52:8787', 'a', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_monto_maximo', '99999', 'asdf', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_monto_minimo', '0', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_nombre_comercial', 'Internet', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_operador', 'NINGUNO', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_prefijo_unidad', 'MB', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_sw_mostrar_si_saldo_mayor_cero', 'true', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_sw_mostrar_vigencia', 'false', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('dpi_valor_operacion', '0', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('CSU_LOCALidad_sw_habilitado', 'false', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('sistema_CSU_LOCAL_saldo_sw_enable', 'true', 'eee', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_wallet_cantidad_decimal', '0', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('valor_billeteras_simple_unidad_navegacion', '', 'Este parametro muestra todas las billeteras simples que se tomaran en cuenta para el cambio de unidad de navegacion,  solo se muestran los IDs de los registros', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('valor_billeteras_agrupadas_unidad_navegacion', '', 'Este parametro muestra todas las billeteras agrupadas que se tomaran en cuenta para el cambio de unidad de navegacion,  solo se muestran los IDs de los registros', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_wallet_texto_separador', ':', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_cantidad_cabeceras', '60', 'este parametro indica la cantidad de lineas por cabecera en cada configuracion de COS', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('vigencia_saldo_vigencia_ilimitada', '', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_mostar_minutos', 'true', NULL, 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_wallet_sw_mostrar_vigencia', 'false', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('vigencia_saldo_txt_vigencia_ilimitada', '', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('seg_vigencia_formato_fecha', 'yyyy-MM-dd''T''HH:mm:ssXXX', 'Formato para la segunda fecha de expiraciÃ³n.', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('seg_vigencia_formato_hora', '', 'Formato para la hora de la segunda fecha de expiraciÃ³n.', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('seg_vigencia_text_saldo', 'activalos hasta %FECHA_VIGENCIA%', 'Texto Vigencia, con el formato de la variable de la fecha y hora para la segunda vigencia. tags habilitados (%FECHA_VIGENCIA%)', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('seg_vigencia_dias_exp', '62', 'Campo entero,  llamado Cantidad dÃ­as expiraciÃ³n, valor para guardar la cantidad de dÃ­as a sumar para tener la segunda fecha de expiraciÃ³n.', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('valor_conversion_unidad_navegacion', '1024', 'Valor numerico que se utilizara para realizar la conversion de unidades de navegacion', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_saludo_inicial', 'El numero %TELEFONO% tiene:', 'aaa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_mostrar_vigencia', 'true', 'eee', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('valor_cambio_unidad_navegacion', '1024', 'Este parametro indica el valor numerico, que establece el  tope para una udidad de navegacion para realizar el cambio de unidad Ej: MB -> GB -> TB', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('valor_unidades_navegacion', 'MB,GB,TB', 'Este parametro indica todas las unidades de navegacion habilitadas en orden de capacidad de izquierda a derecha y separadas por el caracter coma (,)', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('sistema_CSU_LOCAL_saldo_sw_onchange', 'false', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_respuesta_falla_conex_comverse', 'Linea :%TELEFONO%. Servicio no disponible, intente mas tarde.', 'bb', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_respuesta_fuera_servicio', 'Linea :%TELEFONO%. Servicio no disponible, intente mas tarde.', 'a', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_respuesta_usuario_no_comverse', 'Linea :%TELEFONO%. No esta habilitado para este servicio', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_texto_saldo_min', 'saldo insuficiente', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_texto_saldo_max', 'Ilimitado', 'eee', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('default_texto_saldo_min_sw_valor', 'true', 'eee', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('vigencia_saldo_texto', 'activo hasta %FECHA_VIGENCIA%', 'aa', 't');
INSERT INTO "CSU_LOCAL"."parametro" VALUES ('vigencia_saldo_formato_date', 'yyyy-MM-dd''T''HH:mm:ssXXX', 'aa', 't');




CREATE TABLE "CSU_LOCAL"."quota" (
  "nombre" varchar(50)  NOT NULL,
  "bytes" numeric(20),
  "estado" varchar(50) ,
  "perfil" varchar(50) 
)
;


INSERT INTO "CSU_LOCAL"."quota" VALUES ('1 GB Quota Postpaid', 1073741824, NULL, '1G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1 GB Quota Prepaid', 1073741824, NULL, '1G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1 GB Topup Quota Postpaid', 1048576000, 'BB_POS_1G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1.2 GB Quota Postpaid', 1153433600, NULL, '1.2G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1.2 GB Quota Prepaid', 1153433600, NULL, '1.2G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1.5 GB Quota Postpaid', 1610612736, NULL, '1.5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1.5 GB Quota Prepaid', 1610612736, NULL, '1.5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('100000MB Quota Postpaid', 104857600000, NULL, '100000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('100000MB Quota Prepaid', 104857600000, NULL, '100000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('10000MB Quota Postpaid', 10485760000, NULL, '10000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('10000MB Quota Prepaid', 10485760000, NULL, '10000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1000MB Quota Postpaid', 1048576000, NULL, '1000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1000MB Quota Prepaid', 1048576000, NULL, '1000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('100MB Quota Postpaid', 104857600, NULL, '100M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('100MB Quota Prepaid', 104857600, NULL, '100M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('101000MB Quota Postpaid', 105906176000, NULL, '101000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('101000MB Quota Prepaid', 105906176000, NULL, '101000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1050MB Quota Prepaid', 1101004800, NULL, '1050M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('11000MB Quota Postpaid', 11534336000, NULL, '11000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('11000MB Quota Prepaid', 11534336000, NULL, '11000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1100MB Quota Postpaid', 1153433600, NULL, '1100M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1100MB Quota Prepaid', 1153433600, NULL, '1100M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1126MB Quota Postpaid', 1180696576, NULL, '1126M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('12000MB Quota Postpaid', 12582912000, NULL, '12000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('12000MB Quota Prepaid', 12582912000, NULL, '12000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1200MB Quota Postpaid', 1258291200, NULL, '1200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1200MB Quota Prepaid', 1258291200, NULL, '1200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1200MB Topup Quota Postpaid', 1258291200, 'BB_POS_1200M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('13000MB Quota Postpaid', 13631488000, NULL, '13000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('13000MB Quota Prepaid', 13631488000, NULL, '13000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1300MB Quota Postpaid', 1363148800, NULL, '1300M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1300MB Quota Prepaid', 1363148800, NULL, '1300M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('14000MB Quota Postpaid', 14680064000, NULL, '14000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('14000MB Quota Prepaid', 14680064000, NULL, '14000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1400MB Quota Postpaid', 1468006400, NULL, '1400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1400MB Quota Prepaid', 1468006400, NULL, '1400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('15000MB Quota Postpaid', 15728640000, NULL, '15000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('15000MB Quota Prepaid', 15728640000, NULL, '15000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1500MB Quota Postpaid', 1572864000, NULL, '1500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1500MB Quota Prepaid', 1572864000, NULL, '1500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('150MB Quota Postpaid', 157286400, NULL, '150M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('150MB Quota Prepaid', 157286400, NULL, '150M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('150MB Topup Quota Postpaid', 157286400, 'BB_POS_150M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('16000MB Quota Postpaid', 16777216000, NULL, '16000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('16000MB Quota Prepaid', 16777216000, NULL, '16000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1600MB Quota Postpaid', 1677721600, NULL, '1600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1600MB Quota Prepaid', 1677721600, NULL, '1600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('17000MB Quota Postpaid', 17825792000, NULL, '17000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('17000MB Quota Prepaid', 17825792000, NULL, '17000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1700MB Quota Postpaid', 1782579200, NULL, '1700M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1700MB Quota Prepaid', 1782579200, NULL, '1700M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('18000MB Quota Postpaid', 18874368000, NULL, '18000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('18000MB Quota Prepaid', 18874368000, NULL, '18000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1800MB Quota Postpaid', 1887436800, NULL, '1800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1800MB Quota Prepaid', 1887436800, NULL, '1800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('19000MB Quota Postpaid', 19922944000, NULL, '19000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('19000MB Quota Prepaid', 19922944000, NULL, '19000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1900MB Quota Postpaid', 1992294400, NULL, '1900M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('1900MB Quota Prepaid', 1992294400, NULL, '1900M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2 GB Quota Postpaid', 2097152000, NULL, '2G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2 GB Quota Prepaid', 2147483648, NULL, '2G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2 GB Topup Quota Postpaid', 2147483648, 'BB_POS_2G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2.5 GB Quota Postpaid', 2684354560, NULL, '2.5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2.5 GB Quota Prepaid', 2684354560, NULL, '2.5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('20000MB Quota Postpaid', 20971520000, NULL, '20000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('20000MB Quota Prepaid', 20971520000, NULL, '20000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2000MB Quota Postpaid', 2097152000, NULL, '2000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2000MB Quota Prepaid', 2097152000, NULL, '2000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('200MB Quota Postpaid', 209715200, NULL, '200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('200MB Quota Prepaid', 209715200, NULL, '200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('21000MB Quota Postpaid', 22020096000, NULL, '21000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('21000MB Quota Prepaid', 22020096000, NULL, '21000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('22000MB Quota Postpaid', 23068672000, NULL, '22000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('22000MB Quota Prepaid', 23068672000, NULL, '22000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2200MB Quota Postpaid', 2306867200, NULL, '2200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2200MB Quota Prepaid', 2306867200, NULL, '2200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('23000MB Quota Postpaid', 24117248000, NULL, '23000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('23000MB Quota Prepaid', 24117248000, NULL, '23000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('24000MB Quota Postpaid', 25165824000, NULL, '24000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('24000MB Quota Prepaid', 25165824000, NULL, '24000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2400MB Quota Postpaid', 2516582400, NULL, '2400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2400MB Quota Prepaid', 2516582400, NULL, '2400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('25000MB Quota Postpaid', 26214400000, NULL, '25000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('25000MB Quota Prepaid', 26214400000, NULL, '25000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2500MB Quota Postpaid', 2621440000, NULL, '2500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('250MB Quota Postpaid', 262144000, NULL, '250M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('250MB Quota Prepaid', 262144000, NULL, '250M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('26000MB Quota Postpaid', 27262976000, NULL, '26000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('26000MB Quota Prepaid', 27262976000, NULL, '26000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2600MB Quota Postpaid', 2726297600, NULL, '2600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2600MB Quota Prepaid', 2726297600, NULL, '2600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('27000MB Quota Postpaid', 28311552000, NULL, '27000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('27000MB Quota Prepaid', 28311552000, NULL, '27000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('28000MB Quota Postpaid', 29360128000, NULL, '28000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('28000MB Quota Prepaid', 29360128000, NULL, '28000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2800MB Quota Postpaid', 2936012800, NULL, '2800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('2800MB Quota Prepaid', 2936012800, NULL, '2800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('29000MB Quota Postpaid', 30408704000, NULL, '29000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('29000MB Quota Prepaid', 30408704000, NULL, '29000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3 GB Quota Postpaid', 3145728000, NULL, '3G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3 GB Quota Prepaid', 3221225472, NULL, '3G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3 GB Topup Quota Postpaid', 3221225472, 'BB_POS_3G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('30000MB Quota Postpaid', 31457280000, NULL, '30000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('30000MB Quota Prepaid', 31457280000, NULL, '30000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3000MB Quota Postpaid', 3145728000, NULL, '3000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3000MB Quota Prepaid', 3145728000, NULL, '3000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('300MB Quota Postpaid', 314572800, NULL, '300M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('300MB Quota Prepaid', 314572800, NULL, '300M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('300MB Topup Quota Postpaid', 314572800, 'BB_POS_300M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3072MB Quota Postpaid', 3221225472, NULL, '3072M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('31000MB Quota Postpaid', 32505856000, NULL, '31000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('31000MB Quota Prepaid', 32505856000, NULL, '31000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('32000MB Quota Postpaid', 33554432000, NULL, '32000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('32000MB Quota Prepaid', 33554432000, NULL, '32000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3200MB Quota Postpaid', 3355443200, NULL, '3200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3200MB Quota Prepaid', 3355443200, NULL, '3200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('33000MB Quota Postpaid', 34603008000, NULL, '33000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('33000MB Quota Prepaid', 34603008000, NULL, '33000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('34000MB Quota Postpaid', 35651584000, NULL, '34000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('34000MB Quota Prepaid', 35651584000, NULL, '34000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3400MB Quota Postpaid', 3565158400, NULL, '3400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3400MB Quota Prepaid', 3565158400, NULL, '3400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('35000MB Quota Postpaid', 36700160000, NULL, '35000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('35000MB Quota Prepaid', 36700160000, NULL, '35000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3500MB Quota Postpaid', 3670016000, NULL, '3500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('350MB Quota Postpaid', 367001600, NULL, '350M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('350MB Quota Prepaid', 367001600, NULL, '350M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('36000MB Quota Postpaid', 37748736000, NULL, '36000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('36000MB Quota Prepaid', 37748736000, NULL, '36000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3600MB Quota Postpaid', 3774873600, NULL, '3600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3600MB Quota Prepaid', 3774873600, NULL, '3600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('37000MB Quota Postpaid', 38797312000, NULL, '37000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('37000MB Quota Prepaid', 38797312000, NULL, '37000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('38000MB Quota Postpaid', 39845888000, NULL, '38000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('38000MB Quota Prepaid', 39845888000, NULL, '38000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3800MB Quota Postpaid', 3984588800, NULL, '3800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('3800MB Quota Prepaid', 3984588800, NULL, '3800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('39000MB Quota Postpaid', 40894464000, NULL, '39000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('39000MB Quota Prepaid', 40894464000, NULL, '39000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4 GB Quota Postpaid', 4194304000, NULL, '4G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4 GB Quota Prepaid', 4294967296, NULL, '4G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4 GB Topup Quota Postpaid', 4294967296, 'BB_POS_4G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('40000MB Quota Postpaid', 41943040000, NULL, '40000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('40000MB Quota Prepaid', 41943040000, NULL, '40000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4000MB Quota Postpaid', 4194304000, NULL, '4000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4000MB Quota Prepaid', 4194304000, NULL, '4000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('400MB Quota Postpaid', 419430400, NULL, '400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('400MB Quota Prepaid', 419430400, NULL, '400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('41000MB Quota Postpaid', 42991616000, NULL, '41000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('41000MB Quota Prepaid', 42991616000, NULL, '41000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('42000MB Quota Postpaid', 44040192000, NULL, '42000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('42000MB Quota Prepaid', 44040192000, NULL, '42000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4200MB Quota Postpaid', 4404019200, NULL, '4200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4200MB Quota Prepaid', 4404019200, NULL, '4200M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('43000MB Quota Postpaid', 45088768000, NULL, '43000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('43000MB Quota Prepaid', 45088768000, NULL, '43000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('44000MB Quota Postpaid', 46137344000, NULL, '44000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('44000MB Quota Prepaid', 46137344000, NULL, '44000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4400MB Quota Postpaid', 4613734400, NULL, '4400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4400MB Quota Prepaid', 4613734400, NULL, '4400M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('45000MB Quota Postpaid', 47185920000, NULL, '45000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('45000MB Quota Prepaid', 47185920000, NULL, '45000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('450MB Quota Postpaid', 471859200, NULL, '450M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('450MB Quota Prepaid', 471859200, NULL, '450M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('46000MB Quota Postpaid', 48234496000, NULL, '46000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('46000MB Quota Prepaid', 48234496000, NULL, '46000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4600MB Quota Postpaid', 4823449600, NULL, '4600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4600MB Quota Prepaid', 4823449600, NULL, '4600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('47000MB Quota Postpaid', 49283072000, NULL, '47000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('47000MB Quota Prepaid', 49283072000, NULL, '47000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('48000MB Quota Postpaid', 50331648000, NULL, '48000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('48000MB Quota Prepaid', 50331648000, NULL, '48000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4800MB Quota Postpaid', 5033164800, NULL, '4800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('4800MB Quota Prepaid', 5033164800, NULL, '4800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('49000MB Quota Postpaid', 51380224000, NULL, '49000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('49000MB Quota Prepaid', 51380224000, NULL, '49000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5 GB Quota Postpaid', 5368709120, NULL, '5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5 GB Quota Prepaid', 5368709120, NULL, '5G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5 GB Topup Quota Postpaid', 5368709120, 'BB_POS_5G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('50000MB Quota Postpaid', 52428800000, NULL, '50000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('50000MB Quota Prepaid', 52428800000, NULL, '50000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5000MB Quota Postpaid', 5242880000, NULL, '5000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5000MB Quota Prepaid', 5242880000, NULL, '5000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('500MB Quota Postpaid', 524288000, NULL, '500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('500MB Quota Prepaid', 524288000, NULL, '500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('500MB Topup Quota Postpaid', 524288000, 'BB_POS_500M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('50MB Quota Postpaid', 52428800, NULL, '50M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('50MB Quota Prepaid', 52428800, NULL, '50M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('51000MB Quota Postpaid', 53477376000, NULL, '51000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('51000MB Quota Prepaid', 53477376000, NULL, '51000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('52000MB Quota Postpaid', 54525952000, NULL, '52000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('52000MB Quota Prepaid', 54525952000, NULL, '52000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('520MB Quota Prepaid', 545259520, NULL, '520M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('53000MB Quota Postpaid', 55574528000, NULL, '53000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('53000MB Quota Prepaid', 55574528000, NULL, '53000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('54000MB Quota Postpaid', 56623104000, NULL, '54000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('54000MB Quota Prepaid', 56623104000, NULL, '54000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('55000MB Quota Postpaid', 57671680000, NULL, '55000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('55000MB Quota Prepaid', 57671680000, NULL, '55000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5500MB Quota Postpaid', 5767168000, NULL, '5500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('5500MB Quota Prepaid', 5767168000, NULL, '5500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('550MB Quota Postpaid', 576716800, NULL, '550M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('550MB Quota Prepaid', 576716800, NULL, '550M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('55MB Quota Postpaid', 57671680, NULL, '55M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('55MB Quota Prepaid', 57671680, NULL, '55M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('55MB Topup Quota Postpaid', 57671680, 'BB_POS_55M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('56000MB Quota Postpaid', 58720256000, NULL, '56000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('56000MB Quota Prepaid', 58720256000, NULL, '56000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('57000MB Quota Postpaid', 59768832000, NULL, '57000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('57000MB Quota Prepaid', 59768832000, NULL, '57000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('58000MB Quota Postpaid', 60817408000, NULL, '58000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('58000MB Quota Prepaid', 60817408000, NULL, '58000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('59000MB Quota Postpaid', 61865984000, NULL, '59000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('59000MB Quota Prepaid', 61865984000, NULL, '59000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('60000MB Quota Postpaid', 62914560000, NULL, '60000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('60000MB Quota Prepaid', 62914560000, NULL, '60000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('6000MB Quota Postpaid', 6291456000, NULL, '6000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('6000MB Quota Prepaid', 6291456000, NULL, '6000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('600MB Quota Postpaid', 629145600, NULL, '600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('600MB Quota Prepaid', 629145600, NULL, '600M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('61000MB Quota Postpaid', 63963136000, NULL, '61000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('61000MB Quota Prepaid', 63963136000, NULL, '61000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('62000MB Quota Postpaid', 65011712000, NULL, '62000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('62000MB Quota Prepaid', 65011712000, NULL, '62000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('63000MB Quota Postpaid', 66060288000, NULL, '63000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('63000MB Quota Prepaid', 66060288000, NULL, '63000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('64000MB Quota Postpaid', 67108864000, NULL, '64000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('64000MB Quota Prepaid', 67108864000, NULL, '64000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('65000MB Quota Postpaid', 68157440000, NULL, '65000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('65000MB Quota Prepaid', 68157440000, NULL, '65000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('6500MB Quota Postpaid', 6815744000, NULL, '6500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('6500MB Quota Prepaid', 6815744000, NULL, '6500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('650MB Quota Postpaid', 681574400, NULL, '650M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('650MB Quota Prepaid', 681574400, NULL, '650M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('66000MB Quota Postpaid', 69206016000, NULL, '66000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('66000MB Quota Prepaid', 69206016000, NULL, '66000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('67000MB Quota Postpaid', 70254592000, NULL, '67000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('67000MB Quota Prepaid', 70254592000, NULL, '67000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('68000MB Quota Postpaid', 71303168000, NULL, '68000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('68000MB Quota Prepaid', 71303168000, NULL, '68000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('69000MB Quota Postpaid', 72351744000, NULL, '69000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('69000MB Quota Prepaid', 72351744000, NULL, '69000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('70000MB Quota Postpaid', 73400320000, NULL, '70000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('70000MB Quota Prepaid', 73400320000, NULL, '70000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('7000MB Quota Postpaid', 7340032000, NULL, '7000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('7000MB Quota Prepaid', 7340032000, NULL, '7000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('700MB Quota Postpaid', 734003200, NULL, '700M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('700MB Quota Prepaid', 734003200, NULL, '700M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('71000MB Quota Postpaid', 74448896000, NULL, '71000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('71000MB Quota Prepaid', 74448896000, NULL, '71000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('72000MB Quota Postpaid', 75497472000, NULL, '72000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('72000MB Quota Prepaid', 75497472000, NULL, '72000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('73000MB Quota Postpaid', 76546048000, NULL, '73000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('73000MB Quota Prepaid', 76546048000, NULL, '73000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('74000MB Quota Postpaid', 77594624000, NULL, '74000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('74000MB Quota Prepaid', 77594624000, NULL, '74000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('75000MB Quota Postpaid', 78643200000, NULL, '75000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('75000MB Quota Prepaid', 78643200000, NULL, '75000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('7500MB Quota Postpaid', 7864320000, NULL, '7500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('7500MB Quota Prepaid', 7864320000, NULL, '7500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('750MB Quota Postpaid', 786432000, NULL, '750M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('750MB Quota Prepaid', 786432000, NULL, '750M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('76000MB Quota Postpaid', 79691776000, NULL, '76000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('76000MB Quota Prepaid', 79691776000, NULL, '76000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('77000MB Quota Postpaid', 80740352000, NULL, '77000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('77000MB Quota Prepaid', 80740352000, NULL, '77000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('78000MB Quota Postpaid', 81788928000, NULL, '78000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('78000MB Quota Prepaid', 81788928000, NULL, '78000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('79000MB Quota Postpaid', 82837504000, NULL, '79000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('79000MB Quota Prepaid', 82837504000, NULL, '79000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8 GB Quota Postpaid', 8589934592, NULL, '8G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8 GB Quota Prepaid', 8589934592, NULL, '8G');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8 GB Topup Quota Postpaid', 8589934592, 'BB_POS_8G', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('80000MB Quota Postpaid', 83886080000, NULL, '80000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('80000MB Quota Prepaid', 83886080000, NULL, '80000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8000MB Quota Postpaid', 8388608000, NULL, '8000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8000MB Quota Prepaid', 8388608000, NULL, '8000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('800MB Quota Postpaid', 838860800, NULL, '800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('800MB Quota Prepaid', 838860800, NULL, '800M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('80MB Quota Postpaid', 83886080, NULL, '80M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('80MB Quota Prepaid', 83886080, NULL, '80M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('80MB Topup Quota Postpaid', 83886080, 'BB_POS_80M', NULL);
INSERT INTO "CSU_LOCAL"."quota" VALUES ('81000MB Quota Postpaid', 84934656000, NULL, '81000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('81000MB Quota Prepaid', 84934656000, NULL, '81000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('82000MB Quota Postpaid', 85983232000, NULL, '82000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('82000MB Quota Prepaid', 85983232000, NULL, '82000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('83000MB Quota Postpaid', 87031808000, NULL, '83000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('83000MB Quota Prepaid', 87031808000, NULL, '83000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('84000MB Quota Postpaid', 88080384000, NULL, '84000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('84000MB Quota Prepaid', 88080384000, NULL, '84000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('85000MB Quota Postpaid', 89128960000, NULL, '85000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('85000MB Quota Prepaid', 89128960000, NULL, '85000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8500MB Quota Postpaid', 8912896000, NULL, '8500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('8500MB Quota Prepaid', 8912896000, NULL, '8500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('850MB Quota Postpaid', 891289600, NULL, '850M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('850MB Quota Prepaid', 891289600, NULL, '850M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('86000MB Quota Postpaid', 90177536000, NULL, '86000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('86000MB Quota Prepaid', 90177536000, NULL, '86000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('87000MB Quota Postpaid', 91226112000, NULL, '87000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('87000MB Quota Prepaid', 91226112000, NULL, '87000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('88000MB Quota Postpaid', 92274688000, NULL, '88000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('88000MB Quota Prepaid', 92274688000, NULL, '88000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('89000MB Quota Postpaid', 93323264000, NULL, '89000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('89000MB Quota Prepaid', 93323264000, NULL, '89000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('90000MB Quota Postpaid', 94371840000, NULL, '90000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('90000MB Quota Prepaid', 94371840000, NULL, '90000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('9000MB Quota Postpaid', 9437184000, NULL, '9000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('9000MB Quota Prepaid', 9437184000, NULL, '9000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('900MB Quota Postpaid', 943718400, NULL, '900M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('900MB Quota Prepaid', 943718400, NULL, '900M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('91000MB Quota Postpaid', 95420416000, NULL, '91000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('91000MB Quota Prepaid', 95420416000, NULL, '91000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('92000MB Quota Postpaid', 96468992000, NULL, '92000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('92000MB Quota Prepaid', 96468992000, NULL, '92000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('93000MB Quota Postpaid', 97517568000, NULL, '93000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('93000MB Quota Prepaid', 97517568000, NULL, '93000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('94000MB Quota Postpaid', 98566144000, NULL, '94000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('94000MB Quota Prepaid', 98566144000, NULL, '94000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('95000MB Quota Postpaid', 99614720000, NULL, '95000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('95000MB Quota Prepaid', 99614720000, NULL, '95000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('9500MB Quota Postpaid', 9961472000, NULL, '9500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('9500MB Quota Prepaid', 9961472000, NULL, '9500M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('950MB Quota Postpaid', 996147200, NULL, '950M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('950MB Quota Prepaid', 996147200, NULL, '950M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('96000MB Quota Postpaid', 100663296000, NULL, '96000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('96000MB Quota Prepaid', 100663296000, NULL, '96000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('97000MB Quota Postpaid', 101711872000, NULL, '97000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('97000MB Quota Prepaid', 101711872000, NULL, '97000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('98000MB Quota Postpaid', 102760448000, NULL, '98000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('98000MB Quota Prepaid', 102760448000, NULL, '98000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('99000MB Quota Postpaid', 103809024000, NULL, '99000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('99000MB Quota Prepaid', 103809024000, NULL, '99000M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('999999999MB Quota Postpaid', 1048575998951420, NULL, '999999999M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('999999999MB Quota Prepaid', 1048575998951420, NULL, '999999999M');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('CERO Quota Prepaid', 0, NULL, 'CERO');
INSERT INTO "CSU_LOCAL"."quota" VALUES ('ILIMITADO Quota Postpaid', 1072668082176, NULL, 'ILIMITADO');



CREATE TABLE "CSU_LOCAL"."reporte_consulta" (
  "fecha" timestamp(6) NOT NULL,
  "msisdn" varchar(15)  NOT NULL,
  "identificador_servicio" varchar(80)  NOT NULL,
  "nombre_canal" varchar(30) ,
  "publicidad_solicitada" varchar(5) ,
  "longitud_max_solicitada" float,
  "ip_cliente" varchar(30) ,
  "texto_generado" nclob ,
  "sessionId" CHAR(50) ,
  "opcion" CHAR(100)
)
;




CREATE TABLE "CSU_LOCAL"."rol" (
  "rol_id" number(8) NOT NULL,
  "nombre" varchar(30) ,
  "descripcion" varchar(40) ,
  "estado" varchar(1) ,
  "pagina_inicio" varchar(100)
)
;


INSERT INTO "CSU_LOCAL"."rol" VALUES (1, 'Administracion', 'De toda la administracion', 't', '/view/listConfigForm.xhtml');
INSERT INTO "CSU_LOCAL"."rol" VALUES (4, 'Call center', 'Call center', 't', '/view/ReporteConsultas.xhtml');
INSERT INTO "CSU_LOCAL"."rol" VALUES (5, 'Parametrizador', 'Parametrizador', 't', '/view/Menu.xhtml');
INSERT INTO "CSU_LOCAL"."rol" VALUES (52, 'TESTT', 'TEST PROVEEDOR', 't', '/view/listConfigForm.xhtml');




CREATE TABLE "CSU_LOCAL"."rol_formulario" (
  "rol_id" number(8) NOT NULL,
  "formulario_id" number(8) NOT NULL,
  "estado" varchar(1)
)
;



INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 1, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 2, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 3, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 4, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 5, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 6, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 10, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 11, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 12, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 13, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 14, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 21, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 22, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 31, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 32, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 33, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 34, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 35, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 36, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 41, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 42, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 43, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 44, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 45, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 47, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 48, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 49, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 50, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 51, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 61, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 62, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 73, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 74, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 75, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (1, 77, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 1, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 2, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 3, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 4, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 5, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 6, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 10, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 11, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 12, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 13, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 14, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 21, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 22, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 31, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 32, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 33, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 34, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 35, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 36, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 41, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 42, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 43, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 44, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 45, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 47, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 51, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 61, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (4, 62, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 2, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 3, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 4, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 5, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 6, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 21, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 22, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 31, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 32, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 33, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 34, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 35, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 36, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 41, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 42, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 43, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 44, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 1, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 10, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 11, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 12, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 13, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 14, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 45, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 47, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 48, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 49, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 50, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 51, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 61, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 62, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 73, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 74, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 75, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (5, 77, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 75, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 74, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 73, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 14, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 22, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 35, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 44, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 62, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 61, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 6, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 51, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 5, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 50, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 49, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 77, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 48, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 47, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 45, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 43, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 42, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 41, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 4, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 36, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 34, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 33, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 32, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 31, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 3, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 21, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 2, 't');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 13, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 12, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 11, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 10, 'f');
INSERT INTO "CSU_LOCAL"."rol_formulario" VALUES (52, 1, 'f');



CREATE TABLE "CSU_LOCAL"."unit_type" (
  "unit_type_id" number(8) NOT NULL,
  "nombre" varchar(50) ,
  "descripcion" varchar(80) ,
  "estado" varchar(1) 
)
;



INSERT INTO "CSU_LOCAL"."unit_type" VALUES (2, 'CURRENCY', 'Currency', 't');
INSERT INTO "CSU_LOCAL"."unit_type" VALUES (3, 'SECONDS', 'Seconds', 't');
INSERT INTO "CSU_LOCAL"."unit_type" VALUES (137, 'MB', 'INTERNET', 't');
INSERT INTO "CSU_LOCAL"."unit_type" VALUES (138, 'SMS', 'SMS', 't');
INSERT INTO "CSU_LOCAL"."unit_type" VALUES (139, 'Todas', 'TODAS LAS UNIDADES', 't');
INSERT INTO "CSU_LOCAL"."unit_type" VALUES (161, 'Ilimitado', 'Servicios ilimitados', 't');




CREATE TABLE "CSU_LOCAL"."usuario" (
  "usuario_id" number(8) NOT NULL,
  "rol_id" number(8),
  "login" varchar(40) ,
  "nombre" varchar(50) ,
  "estado" varchar(1) 
)
;



INSERT INTO "CSU_LOCAL"."usuario" VALUES (1, 1, 'admin', 'admin', 't');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (3, 1, 'riveroju', 'Julio Cesar Rivero Oblitas', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (4, 1, 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (5, 4, 'osinagac', 'Carlos Rubirh Osinaga Chavez', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (8, 1, 'callisayar', 'Ronal Silvio Callisaya Merlo', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (9, 1, 'penama', 'Marco Augusto PeÃ±a  Alcocer', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (10, 1, 'guzmany', 'Ysaac Guzman', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (11, 1, 'vargasm', 'Marilin Vargas Perez', 't');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (12, 4, 'eguezc', 'Carlos Eduardo Eguez Rojas', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (13, 4, 'villarroellu', 'Luis Daniel Villarroel', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (14, 4, 'bordaj', 'Jimmy Alejandro Borda Villa', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (15, 4, 'monasteriok', 'Kevin Monasterio Mejia', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (16, 1, 'salazari', 'Iver Salazar Zorilla', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (17, 4, 'montecinosd', 'Denis Maribel Montecinos Coronado', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (18, 1, 'pedrozow', 'Wilfredo Pedrozo Guzman', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (19, 4, 'iralaj', 'Jose Luis Irala', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (20, 4, 'palomog', 'Giovana Palomo Sainz', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (21, 4, 'ayalah', 'Henrry Ayala', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (274, 4, 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (276, 4, 'bernalp', 'Pablo Bernal', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (277, 1, 'pedrozow', 'Wilfredo Pedrozo Guzman', 't');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (278, 4, 'bernalp', 'Pablo Bernal', 't');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (279, 4, 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (280, 4, 'paredesm', 'Maria Yuridia Paredes Mamani', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (281, 4, 'perezc', 'Cristhian Nelson Perez Benitez', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (282, 4, 'guzmany', 'Ysaac Guzman', 'f');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (291, 5, 'ramose', 'Erika Johana Ramos Peña', 't');
INSERT INTO "CSU_LOCAL"."usuario" VALUES (289, 52, 'crespoy', 'Yim Deivid Crespo Marquez', 't');


-- ----------------------------
-- Checks structure for table acumulador


-- ----------------------------
ALTER TABLE "CSU_LOCAL"."acumulador"
  ADD CONSTRAINT "acumulador_acumulador_id_check" CHECK (("acumulador_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table acumulador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."acumulador"
  ADD CONSTRAINT "acumulador_pkey" PRIMARY KEY ("acumulador_id");

-- ----------------------------
-- Checks structure for table billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."billetera"
  ADD CONSTRAINT "billetera_billetera_id_check" CHECK (("billetera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."billetera"
  ADD CONSTRAINT "billetera_pkey" PRIMARY KEY ("billetera_id");

-- ----------------------------
-- Checks structure for table bitacora
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."bitacora"
  ADD CONSTRAINT "bitacora_fecha_check" CHECK (("fecha" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table bitacora
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."bitacora"
  ADD CONSTRAINT "bitacora_pkey" PRIMARY KEY ("fecha");

-- ----------------------------
-- Checks structure for table cabecera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."cabecera"
  ADD CONSTRAINT "cabecera_cabecera_id_check" CHECK (("cabecera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table cabecera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."cabecera"
  ADD CONSTRAINT "cabecera_pkey" PRIMARY KEY ("cabecera_id");

-- ----------------------------
-- Checks structure for table campanna
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna"
  ADD CONSTRAINT "campanna_campanna_id_check" CHECK (("campanna_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table campanna
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna"
  ADD CONSTRAINT "campanna_pkey" PRIMARY KEY ("campanna_id");

-- ----------------------------
-- Checks structure for table campanna_clasificador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "campanna_clasificador_check" CHECK (("clasificador_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "campanna_nvalor_check" CHECK (("nombre_valor" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "campanna_campanna_id_check" CHECK (("campanna_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table campanna_clasificador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "campanna_clasificador_pkey" PRIMARY KEY ("campanna_id", "clasificador_id", "nombre_valor");

-- ----------------------------
-- Checks structure for table campanna_mensaje
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_mensaje"
  ADD CONSTRAINT "campanna_mensaje_id_check" CHECK (("mensaje_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."campanna_mensaje"
  ADD CONSTRAINT "campanna_campanna_id_check" CHECK (("campanna_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table campanna_mensaje
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_mensaje"
  ADD CONSTRAINT "campanna_mensaje_pkey" PRIMARY KEY ("campanna_id", "mensaje_id");

-- ----------------------------
-- Checks structure for table clasificador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."clasificador"
  ADD CONSTRAINT "clasificador_id_check" CHECK (("clasificador_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table clasificador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."clasificador"
  ADD CONSTRAINT "clasificador_pkey" PRIMARY KEY ("clasificador_id");

-- ----------------------------
-- Checks structure for table clasificador_valor
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."clasificador_valor"
  ADD CONSTRAINT "clasificador_v_nombre_v_check" CHECK (("nombre_valor" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."clasificador_valor"
  ADD CONSTRAINT "clasificador_valor_id_check" CHECK (("clasificador_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table clasificador_valor
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."clasificador_valor"
  ADD CONSTRAINT "clasificador_valor_pkey" PRIMARY KEY ("clasificador_id", "nombre_valor");

-- ----------------------------
-- Checks structure for table composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."composicion_billetera"
  ADD CONSTRAINT "composicion_b_b_id_check" CHECK (("composicion_billetera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."composicion_billetera"
  ADD CONSTRAINT "composicion_billetera_pkey" PRIMARY KEY ("composicion_billetera_id");

-- ----------------------------
-- Checks structure for table config
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config"
  ADD CONSTRAINT "config_config_id_check" CHECK (("config_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table config
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config"
  ADD CONSTRAINT "config_pkey" PRIMARY KEY ("config_id");

-- ----------------------------
-- Primary Key structure for table config_acumulado
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_acumulado"
  ADD CONSTRAINT "config_acumulado_pkey" PRIMARY KEY ("config_acumulado_id");

-- ----------------------------
-- Checks structure for table config_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_billetera"
  ADD CONSTRAINT "config_b_id_check" CHECK (("config_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."config_billetera"
  ADD CONSTRAINT "config_billetera_id_check" CHECK (("billetera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table config_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_billetera"
  ADD CONSTRAINT "config_billetera_pkey" PRIMARY KEY ("billetera_id", "config_id");

-- ----------------------------
-- Checks structure for table config_composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_composicion_billetera"
  ADD CONSTRAINT "config_composicion_b_id_check" CHECK (("config_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."config_composicion_billetera"
  ADD CONSTRAINT "config_c_b_c_b_id_check" CHECK (("composicion_billetera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table config_composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_composicion_billetera"
  ADD CONSTRAINT "config_cn_bi_pkey" PRIMARY KEY ("composicion_billetera_id", "config_id");

-- ----------------------------
-- Checks structure for table configuracion
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."configuracion"
  ADD CONSTRAINT "configuracion_id_check" CHECK (("id_configuracion" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table configuracion
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."configuracion"
  ADD CONSTRAINT "configuracion_pkey" PRIMARY KEY ("id_configuracion");

-- ----------------------------
-- Checks structure for table corto
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."corto"
  ADD CONSTRAINT "corto_corto_id_check" CHECK (("corto_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table corto
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."corto"
  ADD CONSTRAINT "corto_pkey" PRIMARY KEY ("corto_id");

-- ----------------------------
-- Checks structure for table cos
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."cos"
  ADD CONSTRAINT "cos_cos_id_check" CHECK (("cos_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table cos
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."cos"
  ADD CONSTRAINT "cos_pkey" PRIMARY KEY ("cos_id");

-- ----------------------------
-- Checks structure for table detalle_cabecera_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."detalle_cabecera_billetera"
  ADD CONSTRAINT "detalle_c_b_detalle_id_check" CHECK (("detalle_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table detalle_cabecera_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."detalle_cabecera_billetera"
  ADD CONSTRAINT "d_cabecera_b_pkey" PRIMARY KEY ("detalle_id");

-- ----------------------------
-- Primary Key structure for table detalle_cabecera_menu
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."detalle_cabecera_menu"
  ADD CONSTRAINT "detalle_cabecera_menu_pkey" PRIMARY KEY ("detalle_id");

-- ----------------------------
-- Checks structure for table formulario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."formulario"
  ADD CONSTRAINT "formulario_formulario_id_check" CHECK (("formulario_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table formulario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."formulario"
  ADD CONSTRAINT "formulario_pkey" PRIMARY KEY ("formulario_id");

-- ----------------------------
-- Checks structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_ad"
  ADD CONSTRAINT "grupo_ad_grupo_id_check" CHECK (("grupo_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_ad"
  ADD CONSTRAINT "grupo_ad_pkey" PRIMARY KEY ("grupo_id");

-- ----------------------------
-- Checks structure for table grupo_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_billetera"
  ADD CONSTRAINT "grupo_billetera_id_check" CHECK (("billetera_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."grupo_billetera"
  ADD CONSTRAINT "grupo_b_composicion_b_id_check" CHECK (("composicion_billetera_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table grupo_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_billetera"
  ADD CONSTRAINT "grupo_billetera_pkey" PRIMARY KEY ("billetera_id", "composicion_billetera_id");

-- ----------------------------
-- Checks structure for table lista_datamart
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."lista_datamart"
  ADD CONSTRAINT "lista_datamart_fecha_check" CHECK (("fecha" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table lista_datamart
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."lista_datamart"
  ADD CONSTRAINT "lista_datamart_pkey" PRIMARY KEY ("fecha");

-- ----------------------------
-- Checks structure for table lista_guias
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."lista_guias"
  ADD CONSTRAINT "lista_guias_fecha_check" CHECK (("fecha" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table lista_guias
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."lista_guias"
  ADD CONSTRAINT "lista_guias_pkey" PRIMARY KEY ("fecha");

-- ----------------------------
-- Primary Key structure for table mapeo_ofertas
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."mapeo_ofertas"
  ADD CONSTRAINT "pk_mapeoofertas" PRIMARY KEY ("id_mapeo");

-- ----------------------------
-- Checks structure for table mensaje
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."mensaje"
  ADD CONSTRAINT "mensaje_mensaje_id_check" CHECK (("mensaje_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table mensaje


-- ----------------------------
ALTER TABLE "CSU_LOCAL"."mensaje"
  ADD CONSTRAINT "mensaje_mensaje_id_pk" PRIMARY KEY ("mensaje_id");

-- ----------------------------
-- Checks structure for table mensaje_defecto
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."mensaje_defecto"
  ADD CONSTRAINT "mensaje_defecto_id_check" CHECK (("mensaje_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table mensaje_defecto
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."mensaje_defecto"
  ADD CONSTRAINT "mensaje_defecto_pkey" PRIMARY KEY ("mensaje_id");

-- ----------------------------
-- Primary Key structure for table menu
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."menu"
  ADD CONSTRAINT "menu_pkey" PRIMARY KEY ("menu_id");

-- ----------------------------
-- Checks structure for table occ
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_occ_id_check" CHECK (("occ_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table occ
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_pkey" PRIMARY KEY ("occ_id");

-- ----------------------------
-- Checks structure for table origen
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."origen"
  ADD CONSTRAINT "origen_origen_id_check" CHECK (("origen_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table origen
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."origen"
  ADD CONSTRAINT "origen_pkey" PRIMARY KEY ("origen_id");

-- ----------------------------
-- Checks structure for table parametro
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."parametro"
  ADD CONSTRAINT "parametro_nombre_check" CHECK (("nombre" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table parametro
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."parametro"
  ADD CONSTRAINT "parametro_pkey" PRIMARY KEY ("nombre");

-- ----------------------------
-- Checks structure for table quota
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."quota"
  ADD CONSTRAINT "quota_nombre_check" CHECK (("nombre" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table quota
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."quota"
  ADD CONSTRAINT "quota_pkey" PRIMARY KEY ("nombre");

-- ----------------------------
-- Indexes structure for table reporte_consulta
-- ----------------------------
CREATE INDEX "reporte_consulta_"
  ON "CSU_LOCAL"."reporte_consulta" (
  "msisdn" ASC   ,
  "fecha" ASC  ,
  "sessionId" ASC
  );

-- ----------------------------
-- Checks structure for table reporte_consulta
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."reporte_consulta"
  ADD CONSTRAINT "rc_id_check" CHECK (("identificador_servicio" IS NOT NULL));


ALTER TABLE "CSU_LOCAL"."reporte_consulta"
  ADD CONSTRAINT "reporte_consulta_fecha_check" CHECK (("fecha" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."reporte_consulta"
  ADD CONSTRAINT "reporte_consulta_msisdn_check" CHECK (("msisdn" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table reporte_consulta
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."reporte_consulta"
  ADD CONSTRAINT "reporte_consulta_pkey" PRIMARY KEY ("fecha", "msisdn", "identificador_servicio");

-- ----------------------------
-- Checks structure for table rol
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."rol"
  ADD CONSTRAINT "rol_rol_id_check" CHECK (("rol_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table rol
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."rol"
  ADD CONSTRAINT "rol_pkey" PRIMARY KEY ("rol_id");

-- ----------------------------
-- Checks structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."rol_formulario"
  ADD CONSTRAINT "rol_f_id_check" CHECK (("formulario_id" IS NOT NULL));
ALTER TABLE "CSU_LOCAL"."rol_formulario"
  ADD CONSTRAINT "rol_formulario_rol_id_check" CHECK (("rol_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."rol_formulario"
  ADD CONSTRAINT "rol_formulario_pkey" PRIMARY KEY ("rol_id", "formulario_id");

-- ----------------------------
-- Checks structure for table unit_type
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."unit_type"
  ADD CONSTRAINT "unit_type_unit_type_id_check" CHECK (("unit_type_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table unit_type
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."unit_type"
  ADD CONSTRAINT "unit_type_pkey" PRIMARY KEY ("unit_type_id");

-- ----------------------------
-- Checks structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."usuario"
  ADD CONSTRAINT "usuario_usuario_id_check" CHECK (("usuario_id" IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."usuario"
  ADD CONSTRAINT "usuario_pkey" PRIMARY KEY ("usuario_id");

-- ----------------------------
-- Foreign Keys structure for table billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."billetera"
  ADD CONSTRAINT "billetera_acumulador_id_fkey" FOREIGN KEY ("acumulador_id") REFERENCES "CSU_LOCAL"."acumulador" ("acumulador_id") ON DELETE CASCADE ;



ALTER TABLE "CSU_LOCAL"."billetera"
  ADD CONSTRAINT "billetera_unit_type_id_fkey" FOREIGN KEY ("unit_type_id") REFERENCES "CSU_LOCAL"."unit_type" ("unit_type_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table cabecera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."cabecera"
  ADD CONSTRAINT "cabecera_confi_id_fkey" FOREIGN KEY ("confi_id") REFERENCES "CSU_LOCAL"."config" ("config_id") ON DELETE CASCADE ;


ALTER TABLE "CSU_LOCAL"."cabecera"
  ADD CONSTRAINT "cabecera_unit_type_id_fkey" FOREIGN KEY ("unit_type_id") REFERENCES "CSU_LOCAL"."unit_type" ("unit_type_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table campanna_clasificador
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "ca_clasificador_ca_id_fkey" FOREIGN KEY ("campanna_id") REFERENCES "CSU_LOCAL"."campanna" ("campanna_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."campanna_clasificador"
  ADD CONSTRAINT "campanna_clasificador_id_fkey" FOREIGN KEY ("clasificador_id", "nombre_valor") REFERENCES "CSU_LOCAL"."clasificador_valor" ("clasificador_id", "nombre_valor") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table campanna_mensaje
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."campanna_mensaje"
  ADD CONSTRAINT "campanna_mensaje_id_fkey" FOREIGN KEY ("mensaje_id") REFERENCES "CSU_LOCAL"."mensaje" ("mensaje_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."composicion_billetera"
  ADD CONSTRAINT "com_b_unit_type_id_fkey" FOREIGN KEY ("unit_type_id") REFERENCES "CSU_LOCAL"."unit_type" ("unit_type_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table config_acumulado
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_acumulado"
  ADD CONSTRAINT "config_acu_billetera_id_fkey" FOREIGN KEY ("billetera_id") REFERENCES "CSU_LOCAL"."billetera" ("billetera_id") ON DELETE CASCADE;
ALTER TABLE "CSU_LOCAL"."config_acumulado"
  ADD CONSTRAINT "config_acu_config_id_fkey" FOREIGN KEY ("config_id") REFERENCES "CSU_LOCAL"."config" ("config_id") ON DELETE CASCADE;
-- ----------------------------
-- Foreign Keys structure for table config_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_billetera"
  ADD CONSTRAINT "config_billetera_id_fkey" FOREIGN KEY ("billetera_id") REFERENCES "CSU_LOCAL"."billetera" ("billetera_id") ON DELETE CASCADE;
ALTER TABLE "CSU_LOCAL"."config_billetera"
  ADD CONSTRAINT "config_bi_config_id_fkey" FOREIGN KEY ("config_id") REFERENCES "CSU_LOCAL"."config" ("config_id") ON DELETE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table config_composicion_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."config_composicion_billetera"
  ADD CONSTRAINT "config_c_b_c_b_id_fkey" FOREIGN KEY ("composicion_billetera_id") REFERENCES "CSU_LOCAL"."composicion_billetera" ("composicion_billetera_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."config_composicion_billetera"
  ADD CONSTRAINT "config_c_b_config_id_fkey" FOREIGN KEY ("config_id") REFERENCES "CSU_LOCAL"."config" ("config_id") ON DELETE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table detalle_cabecera_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."detalle_cabecera_billetera"
  ADD CONSTRAINT "detalle_cabecera_b_id_fkey" FOREIGN KEY ("billetera_id") REFERENCES "CSU_LOCAL"."billetera" ("billetera_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table detalle_cabecera_menu
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."detalle_cabecera_menu"
  ADD CONSTRAINT "detalle_cab_menu_b_id_fkey" FOREIGN KEY ("billetera_id") REFERENCES "CSU_LOCAL"."billetera" ("billetera_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table grupo_ad
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_ad"
  ADD CONSTRAINT "grupo_ad_rol_id_fkey" FOREIGN KEY ("rol_id") REFERENCES "CSU_LOCAL"."rol" ("rol_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table grupo_billetera
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."grupo_billetera"
  ADD CONSTRAINT "grupo_billetera__id_fkey" FOREIGN KEY ("billetera_id") REFERENCES "CSU_LOCAL"."billetera" ("billetera_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."grupo_billetera"
  ADD CONSTRAINT "grupo_b_composicion_b_id_fkey" FOREIGN KEY ("composicion_billetera_id") REFERENCES "CSU_LOCAL"."composicion_billetera" ("composicion_billetera_id") ON DELETE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table menu
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."menu"
  ADD CONSTRAINT "menu_unit_type_id_fkey" FOREIGN KEY ("unit_type_id") REFERENCES "CSU_LOCAL"."unit_type" ("unit_type_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table occ
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_config_id_fkey" FOREIGN KEY ("config_id") REFERENCES "CSU_LOCAL"."config" ("config_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_corto_id_fkey" FOREIGN KEY ("corto_id") REFERENCES "CSU_LOCAL"."corto" ("corto_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_cos_id_fkey" FOREIGN KEY ("cos_id") REFERENCES "CSU_LOCAL"."cos" ("cos_id") ON DELETE CASCADE ;

ALTER TABLE "CSU_LOCAL"."occ"
  ADD CONSTRAINT "occ_origen_id_fkey" FOREIGN KEY ("origen_id") REFERENCES "CSU_LOCAL"."origen" ("origen_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table rol_formulario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."rol_formulario"
  ADD CONSTRAINT "rol_f_f_id_fkey" FOREIGN KEY ("formulario_id") REFERENCES "CSU_LOCAL"."formulario" ("formulario_id") ON DELETE CASCADE;


ALTER TABLE "CSU_LOCAL"."rol_formulario"
  ADD CONSTRAINT "rol_formulario_rol_id_fkey" FOREIGN KEY ("rol_id") REFERENCES "CSU_LOCAL"."rol" ("rol_id") ON DELETE CASCADE ;

-- ----------------------------
-- Foreign Keys structure for table usuario
-- ----------------------------
ALTER TABLE "CSU_LOCAL"."usuario"
  ADD CONSTRAINT "usuario_rol_id_fkey" FOREIGN KEY ("rol_id") REFERENCES "CSU_LOCAL"."rol" ("rol_id") ON DELETE CASCADE ;






