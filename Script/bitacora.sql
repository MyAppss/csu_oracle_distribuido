/*
 Navicat Premium Data Transfer

 Source Server         : oddo
 Source Server Type    : PostgreSQL
 Source Server Version : 100010
 Source Host           : localhost:5432
 Source Catalog        : CONSULTA
 Source Schema         : consulta

 Target Server Type    : PostgreSQL
 Target Server Version : 100010
 File Encoding         : 65001

 Date: 18/02/2020 13:37:17
*/


-- ----------------------------
-- Table structure for bitacora
-- ----------------------------
DROP TABLE IF EXISTS "consulta"."bitacora";
CREATE TABLE "consulta"."bitacora" (
  "fecha" timestamp(6) NOT NULL,
  "usuario" varchar(50) COLLATE "pg_catalog"."default",
  "formulario" varchar(50) COLLATE "pg_catalog"."default",
  "accion" varchar(200) COLLATE "pg_catalog"."default",
  "direccion_ip" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Checks structure for table bitacora
-- ----------------------------
ALTER TABLE "consulta"."bitacora" ADD CONSTRAINT "bitacora_fecha_check" CHECK ((fecha IS NOT NULL));

-- ----------------------------
-- Primary Key structure for table bitacora
-- ----------------------------
ALTER TABLE "consulta"."bitacora" ADD CONSTRAINT "bitacora_pkey" PRIMARY KEY ("fecha");
