
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge____ implements Serializable {
    private final static long serialVersionUID = 4235420288097455286L;
    @SerializedName("node")
    @Expose
    private Node____ node;

    public Node____ getNode() {
        return node;
    }

    public void setNode(Node____ node) {
        this.node = node;
    }

}
