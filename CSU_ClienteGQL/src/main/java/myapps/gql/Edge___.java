
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge___ implements Serializable {
    private final static long serialVersionUID = -3398418738171484520L;
    @SerializedName("node")
    @Expose
    private Node___ node;

    public Node___ getNode() {
        return node;
    }

    public void setNode(Node___ node) {
        this.node = node;
    }

}
