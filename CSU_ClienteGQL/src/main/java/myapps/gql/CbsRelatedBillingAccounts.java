
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsRelatedBillingAccounts implements Serializable {
    private final static long serialVersionUID = 4457753178704566894L;
    @SerializedName("edges")
    @Expose
    private List<Edge___> edges= new ArrayList<>();;

    public List<Edge___> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge___> edges) {
        this.edges = edges;
    }

}
