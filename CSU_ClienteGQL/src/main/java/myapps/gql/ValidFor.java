
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidFor implements Serializable {
    private final static long serialVersionUID = 3135704222037597452L;
    @SerializedName("startDatetime")
    @Expose
    private String startDatetime;
    @SerializedName("endDatetime")
    @Expose
    private String endDatetime;

    public String getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(String startDatetime) {
        this.startDatetime = startDatetime;
    }

    public String getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(String endDatetime) {
        this.endDatetime = endDatetime;
    }

}
