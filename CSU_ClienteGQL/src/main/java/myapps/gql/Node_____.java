
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node_____ implements Serializable {
    private final static long serialVersionUID = 5304082422283206178L;
    @SerializedName("balanceId")
    @Expose
    private String balanceId;
    @SerializedName("reservationValue")
    @Expose
    private float reservationValue;
    @SerializedName("validFor")
    @Expose
    private ValidFor__ validFor;

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public float getReservationValue() {
        return reservationValue;
    }

    public void setReservationValue(float reservationValue) {
        this.reservationValue = reservationValue;
    }

    public ValidFor__ getValidFor() {
        return validFor;
    }

    public void setValidFor(ValidFor__ validFor) {
        this.validFor = validFor;
    }

}
