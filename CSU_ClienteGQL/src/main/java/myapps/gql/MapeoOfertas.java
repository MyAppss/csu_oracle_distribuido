package myapps.gql;

import java.io.Serializable;

public class MapeoOfertas implements Serializable {
    private Integer offeringid;
    private Integer offeringparent;
    private String offeringname;
    private String type;
    private String offeringcode;
    private Integer paymentmode;

    public Integer getOfferingid() {
        return offeringid;
    }

    public void setOfferingid(Integer offeringid) {
        this.offeringid = offeringid;
    }

    public Integer getOfferingparent() {
        return offeringparent;
    }

    public void setOfferingparent(Integer offeringparent) {
        this.offeringparent = offeringparent;
    }

    public String getOfferingname() {
        return offeringname;
    }

    public void setOfferingname(String offeringname) {
        this.offeringname = offeringname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOfferingcode() {
        return offeringcode;
    }

    public void setOfferingcode(String offeringcode) {
        this.offeringcode = offeringcode;
    }

    public Integer getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(Integer paymentmode) {
        this.paymentmode = paymentmode;
    }
}
