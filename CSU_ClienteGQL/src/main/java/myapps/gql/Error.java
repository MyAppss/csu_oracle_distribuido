
package myapps.gql;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error implements Serializable {
    private static final  long serialVersionUID = 2401583578947802085L;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("path")
    @Expose
    private List<String> path;
    @SerializedName("locations")
    @Expose
    private List<Location> locations;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
