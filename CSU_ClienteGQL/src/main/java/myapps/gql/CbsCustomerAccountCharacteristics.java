
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsCustomerAccountCharacteristics implements Serializable {
    private final static long serialVersionUID = 177600324186917718L;
    @SerializedName("edges")
    @Expose
    private List<Edge_______> edges= new ArrayList<>();;


    public List<Edge_______> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge_______> edges) {
        this.edges = edges;
    }

}
