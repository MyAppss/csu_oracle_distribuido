
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable {
    private final static long serialVersionUID = 1696619651499864150L;
    @SerializedName("cbsResources")
    @Expose
    private CbsResources_ cbsResources;

    public CbsResources_ getCbsResources() {
        return cbsResources;
    }

    public void setCbsResources(CbsResources_ cbsResources) {
        this.cbsResources = cbsResources;
    }

}
