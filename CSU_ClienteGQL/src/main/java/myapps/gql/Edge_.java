
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge_ implements Serializable {
    private final static long serialVersionUID = 7261232116577084302L;
    @SerializedName("node")
    @Expose
    private Node_ node;

    public Node_ getNode() {
        return node;
    }

    public void setNode(Node_ node) {
        this.node = node;
    }

}
