
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsBucketBalancesFreeUnits implements Serializable {
    private static final long serialVersionUID = -2129882710152264999L;
    @SerializedName("edges")
    @Expose
    private List<Edge> edges= new ArrayList<>();

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }
}
