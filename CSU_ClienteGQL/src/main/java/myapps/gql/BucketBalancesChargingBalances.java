
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BucketBalancesChargingBalances implements Serializable {
    private static final long serialVersionUID = 1930726157046909268L;
    @SerializedName("edges")
    @Expose
    private List<Edge____> edges = new ArrayList<>();

    public List<Edge____> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge____> edges) {
        this.edges = edges;
    }
}
