
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node_ implements Serializable {
    private final static long serialVersionUID = 8063242494433084948L;
    @SerializedName("balanceId")
    @Expose
    private String balanceId;
    @SerializedName("reservationValue")
    @Expose
    private float reservationValue;
    @SerializedName("currentAmount")
    @Expose
    private float currentAmount;
    @SerializedName("validFor")
    @Expose
    private ValidFor validFor;

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public float getReservationValue() {
        return reservationValue;
    }

    public void setReservationValue(float reservationValue) {
        this.reservationValue = reservationValue;
    }

    public float getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(float currentAmount) {
        this.currentAmount = currentAmount;
    }

    public ValidFor getValidFor() {
        return validFor;
    }

    public void setValidFor(ValidFor validFor) {
        this.validFor = validFor;
    }

}
