
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsResources_ implements Serializable {
    private final static long serialVersionUID = 6343434690677373227L;
    @SerializedName("bssLifecycleStatus")
    @Expose
    private String bssLifecycleStatus;
    @SerializedName("primaryOfferingId")
    @Expose
    private String primaryOfferingId;
    @SerializedName("primaryOfferingStatus")
    @Expose
    private String primaryOfferingStatus;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("cbsBucketBalancesFreeUnits")
    @Expose
    private CbsBucketBalancesFreeUnits cbsBucketBalancesFreeUnits;
    @SerializedName("cbsChildProducts")
    @Expose
    private CbsChildProducts cbsChildProducts;
    @SerializedName("cbsRelatedBillingAccounts")
    @Expose
    private CbsRelatedBillingAccounts cbsRelatedBillingAccounts;
    @SerializedName("cbsCustomerInfoAccounts")
    @Expose
    private CbsCustomerInfoAccounts cbsCustomerInfoAccounts;
    @SerializedName("productInstances")
    @Expose
    private ProductInstances productInstances;


    public String getBssLifecycleStatus() {
        return bssLifecycleStatus;
    }

    public void setBssLifecycleStatus(String bssLifecycleStatus) {
        this.bssLifecycleStatus = bssLifecycleStatus;
    }

    public String getPrimaryOfferingId() {
        return primaryOfferingId;
    }

    public void setPrimaryOfferingId(String primaryOfferingId) {
        this.primaryOfferingId = primaryOfferingId;
    }

    public String getPrimaryOfferingStatus() {
        return primaryOfferingStatus;
    }

    public void setPrimaryOfferingStatus(String primaryOfferingStatus) {
        this.primaryOfferingStatus = primaryOfferingStatus;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public CbsBucketBalancesFreeUnits getCbsBucketBalancesFreeUnits() {
        return cbsBucketBalancesFreeUnits;
    }

    public void setCbsBucketBalancesFreeUnits(CbsBucketBalancesFreeUnits cbsBucketBalancesFreeUnits) {
        this.cbsBucketBalancesFreeUnits = cbsBucketBalancesFreeUnits;
    }

    public CbsChildProducts getCbsChildProducts() {
        return cbsChildProducts;
    }

    public void setCbsChildProducts(CbsChildProducts cbsChildProducts) {
        this.cbsChildProducts = cbsChildProducts;
    }

    public CbsRelatedBillingAccounts getCbsRelatedBillingAccounts() {
        return cbsRelatedBillingAccounts;
    }

    public void setCbsRelatedBillingAccounts(CbsRelatedBillingAccounts cbsRelatedBillingAccounts) {
        this.cbsRelatedBillingAccounts = cbsRelatedBillingAccounts;
    }

    public CbsCustomerInfoAccounts getCbsCustomerInfoAccounts() {
        return cbsCustomerInfoAccounts;
    }

    public void setCbsCustomerInfoAccounts(CbsCustomerInfoAccounts cbsCustomerInfoAccounts) {
        this.cbsCustomerInfoAccounts = cbsCustomerInfoAccounts;
    }

    public ProductInstances getProductInstances() {
        return productInstances;
    }

    public void setProductInstances(ProductInstances productInstances) {
        this.productInstances = productInstances;
    }

}
