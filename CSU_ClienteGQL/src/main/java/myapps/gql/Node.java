
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node implements Serializable {
    private final static long serialVersionUID = -7305054989067064971L;
    @SerializedName("value")
    @Expose
    private float value;
    @SerializedName("cbsSpecification")
    @Expose
    private CbsSpecification cbsSpecification;
    @SerializedName("freeUnitsDetails")
    @Expose
    private FreeUnitsDetails freeUnitsDetails;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public CbsSpecification getCbsSpecification() {
        return cbsSpecification;
    }

    public void setCbsSpecification(CbsSpecification cbsSpecification) {
        this.cbsSpecification = cbsSpecification;
    }

    public FreeUnitsDetails getFreeUnitsDetails() {
        return freeUnitsDetails;
    }

    public void setFreeUnitsDetails(FreeUnitsDetails freeUnitsDetails) {
        this.freeUnitsDetails = freeUnitsDetails;
    }

}
