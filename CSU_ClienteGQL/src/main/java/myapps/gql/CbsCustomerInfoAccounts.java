
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsCustomerInfoAccounts implements Serializable {
    private final static long serialVersionUID = 9080427387949803992L;
    @SerializedName("edges")
    @Expose
    private List<Edge______> edges= new ArrayList<>();;

    public List<Edge______> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge______> edges) {
        this.edges = edges;
    }

}
