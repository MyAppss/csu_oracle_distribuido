package myapps.gql;

import java.io.Serializable;

public class ResponseProperties implements Serializable {
    private static final  long serialVersionUID = 6200230298386089665L;
    private String result;
    private int code;
    private String descripcion;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
