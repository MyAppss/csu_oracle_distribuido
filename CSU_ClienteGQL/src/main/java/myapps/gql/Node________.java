
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node________ implements Serializable {
    private final static long serialVersionUID = 705123232082309694L;
    @SerializedName("productID")
    @Expose
    private String productID;
    @SerializedName("networkType")
    @Expose
    private String networkType;
    @SerializedName("packageFlag")
    @Expose
    private String packageFlag;
    @SerializedName("primaryFlag")
    @Expose
    private String primaryFlag;
    @SerializedName("productType")
    @Expose
    private String productType;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getPackageFlag() {
        return packageFlag;
    }

    public void setPackageFlag(String packageFlag) {
        this.packageFlag = packageFlag;
    }

    public String getPrimaryFlag() {
        return primaryFlag;
    }

    public void setPrimaryFlag(String primaryFlag) {
        this.primaryFlag = primaryFlag;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

}
