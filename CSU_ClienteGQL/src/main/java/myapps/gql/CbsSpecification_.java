
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsSpecification_ implements Serializable {
    private final static long serialVersionUID = -5846859374135615531L;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("externalId")
    @Expose
    private String externalId;
    @SerializedName("specification")
    @Expose
    private Specification_ specification;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Specification_ getSpecification() {
        return specification;
    }

    public void setSpecification(Specification_ specification) {
        this.specification = specification;
    }

}
