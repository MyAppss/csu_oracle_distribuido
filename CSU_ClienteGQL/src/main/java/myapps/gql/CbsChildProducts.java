
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CbsChildProducts implements Serializable {
    private final static long serialVersionUID = 2917600573554361903L;
    @SerializedName("edges")
    @Expose
    private List<Edge__> edges= new ArrayList<>();;

    public List<Edge__> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge__> edges) {
        this.edges = edges;
    }

}
