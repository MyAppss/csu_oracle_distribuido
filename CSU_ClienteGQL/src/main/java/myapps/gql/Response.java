package myapps.gql;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class Response implements Serializable {
    private static final long serialVersionUID = 6200230298386089665L;
    private HttpStatus httpStatus;
    private CbsResources cbsResources;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public CbsResources getCbsResources() {
        return cbsResources;
    }

    public void setCbsResources(CbsResources cbsResources) {
        this.cbsResources = cbsResources;
    }
}
