
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge_____ implements Serializable {
    private final static long serialVersionUID = 2718028915021633824L;
    @SerializedName("node")
    @Expose
    private Node_____ node;

    public Node_____ getNode() {
        return node;
    }

    public void setNode(Node_____ node) {
        this.node = node;
    }

}
