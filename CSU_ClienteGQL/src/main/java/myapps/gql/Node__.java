
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node__ implements Serializable {
    private final static long serialVersionUID = -2889611224837296251L;
    @SerializedName("offeringId")
    @Expose
    private String offeringId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("bundleFlag")
    @Expose
    private String bundleFlag;
    @SerializedName("offeringClass")
    @Expose
    private String offeringClass;
    @SerializedName("validFor")
    @Expose
    private ValidFor_ validFor;

    public String getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBundleFlag() {
        return bundleFlag;
    }

    public void setBundleFlag(String bundleFlag) {
        this.bundleFlag = bundleFlag;
    }

    public String getOfferingClass() {
        return offeringClass;
    }

    public void setOfferingClass(String offeringClass) {
        this.offeringClass = offeringClass;
    }

    public ValidFor_ getValidFor() {
        return validFor;
    }

    public void setValidFor(ValidFor_ validFor) {
        this.validFor = validFor;
    }

}
