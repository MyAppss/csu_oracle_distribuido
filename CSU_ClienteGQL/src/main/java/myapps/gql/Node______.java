
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node______ implements Serializable {
    private final static long serialVersionUID = -7097624512587119570L;
    @SerializedName("accountKey")
    @Expose
    private String accountKey;
    @SerializedName("customerClass")
    @Expose
    private String customerClass;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("customerType")
    @Expose
    private String customerType;
    @SerializedName("cbsCustomerAccountCharacteristics")
    @Expose
    private CbsCustomerAccountCharacteristics cbsCustomerAccountCharacteristics;

    public String getAccountKey() {
        return accountKey;
    }

    public void setAccountKey(String accountKey) {
        this.accountKey = accountKey;
    }

    public String getCustomerClass() {
        return customerClass;
    }

    public void setCustomerClass(String customerClass) {
        this.customerClass = customerClass;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public CbsCustomerAccountCharacteristics getCbsCustomerAccountCharacteristics() {
        return cbsCustomerAccountCharacteristics;
    }

    public void setCbsCustomerAccountCharacteristics(CbsCustomerAccountCharacteristics cbsCustomerAccountCharacteristics) {
        this.cbsCustomerAccountCharacteristics = cbsCustomerAccountCharacteristics;
    }

}
