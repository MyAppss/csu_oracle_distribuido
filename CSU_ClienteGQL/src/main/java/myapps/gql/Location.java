
package myapps.gql;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location implements Serializable
{

    @SerializedName("line")
    @Expose
    private Integer line;
    @SerializedName("column")
    @Expose
    private Integer column;
    private final static long serialVersionUID = -3935430984113481995L;

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

}
