
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidFor__ implements Serializable {
    private static final long serialVersionUID = 3699538245680193420L;
    @SerializedName("startDatetime")
    @Expose
    private String startDatetime;
    @SerializedName("endDatetime")
    @Expose
    private String endDatetime;

    public String getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(String startDatetime) {
        this.startDatetime = startDatetime;
    }

    public String getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(String endDatetime) {
        this.endDatetime = endDatetime;
    }

}
