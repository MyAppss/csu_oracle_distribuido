
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeUnitsDetails implements Serializable {
    private static final  long serialVersionUID = -2694982537553610094L;
    @SerializedName("edges")
    @Expose
    private List<Edge_> edges= new ArrayList<>();

    public List<Edge_> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge_> edges) {
        this.edges = edges;
    }

}
