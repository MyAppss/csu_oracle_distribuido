
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node___ implements Serializable {
    private final static long serialVersionUID = -5154416139690815260L;
    @SerializedName("accountKey")
    @Expose
    private String accountKey;
    @SerializedName("bucketBalancesChargingBalances")
    @Expose
    private BucketBalancesChargingBalances bucketBalancesChargingBalances;

    public String getAccountKey() {
        return accountKey;
    }

    public void setAccountKey(String accountKey) {
        this.accountKey = accountKey;
    }

    public BucketBalancesChargingBalances getBucketBalancesChargingBalances() {
        return bucketBalancesChargingBalances;
    }

    public void setBucketBalancesChargingBalances(BucketBalancesChargingBalances bucketBalancesChargingBalances) {
        this.bucketBalancesChargingBalances = bucketBalancesChargingBalances;
    }

}
