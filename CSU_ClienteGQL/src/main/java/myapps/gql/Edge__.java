
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge__ implements Serializable {
    private final static long serialVersionUID = -462270102947268048L;
    @SerializedName("node")
    @Expose
    private Node__ node;

    public Node__ getNode() {
        return node;
    }

    public void setNode(Node__ node) {
        this.node = node;
    }

}
