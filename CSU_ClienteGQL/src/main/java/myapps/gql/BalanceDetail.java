
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BalanceDetail implements Serializable {
    private static final long serialVersionUID = 547122103165379280L;
    @SerializedName("edges")
    @Expose
    private List<Edge_____> edges= new ArrayList<>();


    public List<Edge_____> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge_____> edges) {
        this.edges = edges;
    }

}
