
package myapps.gql;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edge______ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node______ node;
    private final static long serialVersionUID = -6388269467874154757L;

    public Node______ getNode() {
        return node;
    }

    public void setNode(Node______ node) {
        this.node = node;
    }

}
