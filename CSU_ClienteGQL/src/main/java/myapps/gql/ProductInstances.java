
package myapps.gql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductInstances implements Serializable {
    private final static long serialVersionUID = -450958847237932168L;
    @SerializedName("edges")
    @Expose
    private List<Edge________> edges= new ArrayList<>();


    public List<Edge________> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge________> edges) {
        this.edges = edges;
    }

}
