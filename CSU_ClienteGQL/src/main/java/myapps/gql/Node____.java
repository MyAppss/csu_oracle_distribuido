
package myapps.gql;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node____ implements Serializable {
    private final static long serialVersionUID = -8597557615434632692L;
    @SerializedName("value")
    @Expose
    private float value;
    @SerializedName("cbsSpecification")
    @Expose
    private CbsSpecification_ cbsSpecification;
    @SerializedName("balanceDetail")
    @Expose
    private BalanceDetail balanceDetail;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public CbsSpecification_ getCbsSpecification() {
        return cbsSpecification;
    }

    public void setCbsSpecification(CbsSpecification_ cbsSpecification) {
        this.cbsSpecification = cbsSpecification;
    }

    public BalanceDetail getBalanceDetail() {
        return balanceDetail;
    }

    public void setBalanceDetail(BalanceDetail balanceDetail) {
        this.balanceDetail = balanceDetail;
    }

}
