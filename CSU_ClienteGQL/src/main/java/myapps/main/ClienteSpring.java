package myapps.main;


import myapps.dao.MapeoOfertasSingleton;
import myapps.gql.CbsResources;
import myapps.cmn.vo.ResponseCCWS;
import myapps.gql.Response;
import myapps.utils.ImprimirBilleteras;
import myapps.utils.Operaciones;
import myapps.utils.PropiedadesGQL;
import myapps.utils.UtilJava;
import com.google.gson.Gson;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.*;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import java.util.ArrayList;
import java.util.Collections;


public class ClienteSpring {
    private static final Logger log = LogManager.getLogger(ClienteSpring.class);


    /**
     * El metodo  requestCallDetail es el encargado de realizar la peticion al servicio rest de GraphQl y devolver un objeto de tipo CbsResources
     *
     * @param apiRestAuthURL: Es la url del End Point
     * @param message:        Es el request que se enviara a la peticion
     * @param timeout:        Tiempo maximo que esperara la respuesta del EndPoint
     * @return
     */
    public static Response requestCallDetail(String apiRestAuthURL, String message, int timeout, String isdn, String session) {
        long tiempoRCD = System.currentTimeMillis();
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::| INICIANDO METODO requestCallDetail |:::...");
        RestTemplate rest = new RestTemplate(UtilJava.getClientHttpRequestFactory(timeout));
        HttpHeaders headers = new HttpHeaders();
        Response response = new Response();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> request = new HttpEntity<>(message, headers);

        try {
            long tInicio = System.currentTimeMillis();
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "INICIANDO PETICION HACIA SERVICIO GQL: " + apiRestAuthURL);
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + " REQUEST: " + message);
            ResponseEntity<CbsResources> responseEntity = rest.exchange(apiRestAuthURL, HttpMethod.POST, request, CbsResources.class);
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "TIEMPO DE RESPUESTA DEL SERVIDOR GQL: " + (System.currentTimeMillis() - tInicio) + " ms");
            switch (responseEntity.getStatusCode()) {
                case OK:
                    log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "REQUEST EXITOSO CODE: " + HttpStatus.OK.value() + " " + HttpStatus.OK.getReasonPhrase());
                    break;
                case BAD_REQUEST:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "REQUEST CODE " + HttpStatus.BAD_REQUEST.value() + " " + HttpStatus.BAD_REQUEST.getReasonPhrase());
                    break;
                case NOT_FOUND:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "REQUEST CODE " + HttpStatus.NOT_FOUND.value() + " " + HttpStatus.NOT_FOUND.getReasonPhrase());
                    break;
                case ACCEPTED:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "REQUEST CODE " + HttpStatus.ACCEPTED.value() + " " + HttpStatus.ACCEPTED.getReasonPhrase());
                    break;
                case BAD_GATEWAY:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "REQUEST CODE " + HttpStatus.BAD_GATEWAY.value() + " " + HttpStatus.BAD_GATEWAY.getReasonPhrase());
                    break;
                default:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "STATUS CODE NO DEFINIDO: " + responseEntity.getStatusCode());
                    break;
            }

            response.setCbsResources(responseEntity.getBody());
            response.setHttpStatus(responseEntity.getStatusCode());
        } catch (RestClientException e) {
            log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "OCURRIO UN ERROR AL CONSUMIR EL SERVICIO: " + e.getMessage());
            response.setCbsResources(null);
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::| FINALIZO EL METODO requestCallDetail: " + (System.currentTimeMillis() - tiempoRCD) + " ms |:::...");
        return response;
    }


    /**
     * Este Metodo es el inicial para consultar al servicio GraphQl, llama al metodo requestCallDetail y procesa el response y devuelve un objeto de tipo ResponseCCWS
     *
     * @param isdn:    numero del anonado
     * @param session: session del abonado con el cual esta haciendo su consulta
     * @return
     */
    public Object obtenerResponseCCWS(String isdn, String session) {
        long toRCCWS = System.currentTimeMillis();
        log.info("");
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::* INICIAR METODO obtenerResponseCCWS  *:::...");
        ResponseCCWS responseCCWS = new ResponseCCWS();
        try {
            Response response = requestCallDetail(PropiedadesGQL.getGqlENDPOINT(), PropiedadesGQL.getGqlMESSAGE().replace("%TELEFONO%", isdn), PropiedadesGQL.getGqlTIMEOUT(), isdn, session);
            MapeoOfertasSingleton.getInstance().setConsulta(true);
            if (response.getCbsResources() != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(response.getCbsResources());
                log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + " RESPONSE: " + json);

                if (response.getCbsResources().getData() != null && response.getCbsResources().getData().getCbsResources() != null) {
                    log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "INICIANDO CONVERSION A  OBJETO CCWS");
                    responseCCWS.setNameCOS(Operaciones.obtenerCosName(response.getCbsResources().getData().getCbsResources().getCbsChildProducts(), response.getCbsResources().getData().getCbsResources().getPrimaryOfferingId(), isdn, session));
                    responseCCWS.setErrorDescription("TransactionCorrect");
                    responseCCWS.setListWallet(new ArrayList<>());
                    responseCCWS.getListWallet().addAll(Operaciones.obtenerBilleterasPrimarias(response.getCbsResources().getData().getCbsResources().getCbsRelatedBillingAccounts(), isdn, session));
                    responseCCWS.getListWallet().addAll(Operaciones.obtenerBilleterasSecundarias(response.getCbsResources().getData().getCbsResources().getCbsBucketBalancesFreeUnits(), isdn, session));

                    responseCCWS.setListOffer(Operaciones.obtenerListOffer(response.getCbsResources().getData().getCbsResources().getCbsChildProducts(), isdn, session));
                    responseCCWS.setListAcumulador(new ArrayList<>());
                    if (responseCCWS.getNameCOS().equals(PropiedadesGQL.getGqlNofound())) {
                        responseCCWS.setErrorDescription(PropiedadesGQL.getGqlNofound());
                    }
                    if (PropiedadesGQL.isImprimirBilleteras()) {
                        ImprimirBilleteras imprimirBilleteras = new ImprimirBilleteras(responseCCWS.getListWallet(), responseCCWS.getListOffer(), session, isdn);
                        imprimirBilleteras.start();
                    }
                    log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + " FIN DE CONVERSION A CCWS");
                } else if (!response.getCbsResources().getErrors().isEmpty()) {
                    responseCCWS.setNameCOS("ERROR");
                    responseCCWS.setErrorDescription("Subscriber not found");
                    responseCCWS.setListWallet(new ArrayList<>());
                    responseCCWS.setListOffer(new ArrayList<>());
                    responseCCWS.setListAcumulador(new ArrayList<>());
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "NO SE OBTUVIERON DATOS GQL PARA LA LINEA: " + isdn);
                    log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "MENSAJE DE ERROR DEL ENDPOINT: " + response.getCbsResources().getErrors());

                }
            } else {
                responseCCWS.setNameCOS("ERROR");
                responseCCWS.setErrorDescription("ConnectionErrorTimeOut");
                responseCCWS.setListWallet(new ArrayList<>());
                responseCCWS.setListOffer(new ArrayList<>());
                responseCCWS.setListAcumulador(new ArrayList<>());

                log.error("[SESSION: " + session + " LINEA: " + isdn + "] " +"ERROR ASOCIADO AL ENDPOINT");
            }
            MapeoOfertasSingleton.getInstance().setConsulta(false);
        } catch (Exception e) {
            responseCCWS = null;
            log.error("[SESSION: " + session + " LINEA: " + isdn + "] " +"ERROR " + e.getMessage());
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::* TIEMPO TOTAL DEL obtenerResponseCCWS " + (System.currentTimeMillis() - toRCCWS) + " ms *:::...");
        return responseCCWS;
    }

    public static void main(String[] args) {
        Gson gson = new Gson();

        PropiedadesGQL.setGqlTIMEOUT(1000);
        // PropiedadesGQL.setGqlENDPOINT("https://gql-api-qvantel.tigo.net.bo/graphql");
        //PropiedadesGQL.setGqlENDPOINT("https://csr-staging.digitalback.tigo.net.bo:8180/graphql");

        PropiedadesGQL.setGqlENDPOINT("http://localhost:9091/graphql/");

        PropiedadesGQL.setGqlDATEFORMAT("yyyy-MM-dd'T'HH:mm:ss");
        //PropiedadesGQL.setGqlMESSAGE("");
        PropiedadesGQL.setGqlMESSAGE("{\"query\":\"query cbs {\\n  cbsResources(msisdn: \\\"%TELEFONO%\\\") {\\n    bssLifecycleStatus\\n    primaryOfferingId\\n    primaryOfferingStatus\\n    paymentMode\\n    cbsBucketBalancesFreeUnits {\\n      edges {\\n        node {\\n          value\\n          cbsSpecification {\\n            externalId\\n            name\\n            unitOfMeasure\\n            specification {\\n              category\\n              name\\n            }\\n          }\\n          freeUnitsDetails {\\n            edges {\\n              node {\\n                balanceId\\n                reservationValue\\n                currentAmount\\n                validFor {\\n                  startDatetime\\n                  endDatetime\\n                }\\n              }\\n            }\\n          }\\n        }\\n      }\\n    }\\n    cbsChildProducts {\\n      edges {\\n        node {\\n          offeringId\\n          status\\n          bundleFlag\\n          offeringClass\\n          validFor {\\n            startDatetime\\n            endDatetime\\n          }\\n        }\\n      }\\n    }\\n    cbsRelatedBillingAccounts {\\n      edges {\\n        node {\\n          accountKey\\n          bucketBalancesChargingBalances {\\n            edges {\\n              node {\\n                value\\n                cbsSpecification {\\n                  name\\n                  \\n                  currency\\n                  externalId\\n                  specification {\\n                    category\\n                    name\\n                  }\\n                }\\n                balanceDetail {\\n                  edges {\\n                    node {\\n                      balanceId\\n                      reservationValue\\n                      validFor {\\n                        startDatetime\\n                        endDatetime\\n                      }\\n                    }\\n                  }\\n                }\\n              }\\n            }\\n          }\\n        }\\n      }\\n    }\\n    cbsCustomerInfoAccounts {\\n      edges {\\n        node {\\n          accountKey\\n          customerClass\\n          customerId\\n          customerType\\n          cbsCustomerAccountCharacteristics {\\n            edges {\\n              node {\\n                key\\n                value\\n              }\\n            }\\n          }\\n        }\\n      }\\n    }\\n    productInstances {\\n      edges {\\n        node {\\n          productID\\n          networkType\\n          packageFlag\\n          primaryFlag\\n          productType\\n          \\n        }\\n      }\\n    }\\n  }\\n}\\n\\n\\n\",\"operationName\":\"cbs\"}");
        PropiedadesGQL.setGqlFECHAMINIMA("1990-01-01T20:00:00");
        PropiedadesGQL.setUrlBD("jdbc:oracle:thin:@localhost:1521:ORCL");
        PropiedadesGQL.setUsuarioBD("CSUORACLE");
        PropiedadesGQL.setPasswordBD("csu");


      /*  PropiedadesGQL.setUrlBD("jdbc:oracle:thin:@mtitdb-scan.tigo.net.bo:1521/pcsutstdb.tigo.net.bo");
        PropiedadesGQL.setUsuarioBD("csuoracle");
        PropiedadesGQL.setPasswordBD("tstcsuoracle");*/


        PropiedadesGQL.setSetminevictableidletimemillisBD(60000);
        PropiedadesGQL.setSetmaxactiveBD(200);
        PropiedadesGQL.setSetminidleBD(100);
        PropiedadesGQL.setSetMaxIdleBD(105);
        PropiedadesGQL.setSetMaxWaitBD(30000);
        PropiedadesGQL.setGqlBOB(1000000);
        PropiedadesGQL.setGqlKB(1024);
        PropiedadesGQL.setPrecioKB(0.15);
        PropiedadesGQL.setGqlItem(0.20);
        PropiedadesGQL.setGqlDecimales(4);
        PropiedadesGQL.setGqlNofound("Subscriber not found");
        PropiedadesGQL.setGqlTYPE("COS");
        PropiedadesGQL.setGqlChilProdutActivo("2");
        PropiedadesGQL.setGqlChilProdutSuspendido("4");
        PropiedadesGQL.setImprimirBilleteras(true);
        PropiedadesGQL.setNombreReseva("RESERVED");
        PropiedadesGQL.setFechaImprimirBIlleteras("dd/MM/YYYY HH:mm:ss:ms");


        MapeoOfertasSingleton.getInstance();
        // MapeoOfertasSingleton.getInstance();
        MapeoOfertasSingleton ac = MapeoOfertasSingleton.getInstance();
        //ac.getCosName(3590);

        ClienteSpring a = new ClienteSpring();
        ResponseCCWS resultado = (ResponseCCWS) a.obtenerResponseCCWS("69185458", "112346888");
        log.info(gson.toJson(resultado));

    }


}
