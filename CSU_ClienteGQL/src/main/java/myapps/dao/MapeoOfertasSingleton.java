package myapps.dao;

import myapps.gql.MapeoOfertas;
import myapps.utils.PropiedadesGQL;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.List;

public class MapeoOfertasSingleton implements Serializable {
    private static MapeoOfertasSingleton INSTANCE = null;
    private boolean consulta;
    private boolean actualizando;
    private List<MapeoOfertas> mapeoOfertasSingletonList;

    private static final Logger log = LogManager.getLogger(MapeoOfertasSingleton.class);

    private MapeoOfertasSingleton() {
        cargarMapeoOfertas();
        consulta = false;
        actualizando = false;
    }


    private static synchronized void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MapeoOfertasSingleton();
        }
    }

    public static  MapeoOfertasSingleton getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }


    public MapeoOfertas getCosName(int valor) {
        MapeoOfertas mapeoOfertas = new MapeoOfertas();
        try {
            while (INSTANCE.actualizando) {
                log.info("Esperando la actualizacion de la lista Mapeo Ofertas");
            }
            for (MapeoOfertas mo : INSTANCE.getMapeoOfertasSingletonList()) {
                if (mo.getOfferingid() == valor) {
                    mapeoOfertas = mo;
                    break;
                }
            }
        } catch (Exception e) {
            log.error("LA LISTA DE OFERTAS ESTA VACIA O ES NULLO " + e.getMessage());
        }
        return mapeoOfertas;
    }


    private synchronized void cargarMapeoOfertas() {
        try {
            if (PropiedadesGQL.getUrlBD() != null && PropiedadesGQL.getGqlENDPOINT() != null) {
                long tiempoIni = System.currentTimeMillis();
                log.info("INICIANDO A CARGAR LISTA DE OFERTAS DE LA BASE DE DATOS");
                mapeoOfertasSingletonList = MapeoOfertasDAO.getLista();
                log.info("FINALIZO DE CARGAR LISTA DE OFERTAS DE LA BASE DE DATOS EN " + (System.currentTimeMillis() - tiempoIni) + " ms");
            }
        } catch (Exception e) {
            log.error("[cargarMapeoOfertas]", e);
        }
    }


    public synchronized void reset() {
        try {
            while (INSTANCE.consulta) {
                log.info("Esperando aque finalize la consulta para Recargar la lista Mapeo Ofertas ");
            }
            long tiempoIni = System.currentTimeMillis();
            log.info("INICIANDO A REALIZAR RESET DE LISTA DE OFERTAS DE LA BASE DE DATOS");
            actualizando = true;
            cargarMapeoOfertas();
            actualizando = false;
            log.info("FINALIZANDO DE REALIZAR RESET DE LISTA DE OFERTAS DE LA BASE DE DATOS EN " + (System.currentTimeMillis() - tiempoIni) + " ms");
        } catch (Exception e) {
            log.error("[reset]", e);
        }
    }

    public List<MapeoOfertas> getMapeoOfertasSingletonList() {
        return mapeoOfertasSingletonList;
    }

    public void setMapeoOfertasSingletonList(List<MapeoOfertas> mapeoOfertasSingletonList) {
        this.mapeoOfertasSingletonList = mapeoOfertasSingletonList;
    }

    public boolean isConsulta() {
        return consulta;
    }

    public void setConsulta(boolean consulta) {
        this.consulta = consulta;
    }

    public boolean isActualizando() {
        return actualizando;
    }

    public void setActualizando(boolean actualizando) {
        this.actualizando = actualizando;
    }
}
