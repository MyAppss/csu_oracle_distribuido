package myapps.dao;

import myapps.gql.MapeoOfertas;
import myapps.utils.ConexionBDSource;
import myapps.utils.PropiedadesGQL;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MapeoOfertasDAO implements Serializable {
    private static final Logger log = LogManager.getLogger(MapeoOfertasDAO.class);

    @SuppressWarnings("empty-statement")
    public static MapeoOfertas getCosName(int valor) {
        MapeoOfertas mapeoOfertas = new MapeoOfertas();
        PreparedStatement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            ConexionBDSource bdSource = new ConexionBDSource();
            conn = bdSource.getConnection();
            if (conn != null) {
                String sql = "select offeringid,offeringparent,offeringname,type,offeringcode,paymentmode from mapeo_ofertas where estado='t' and offeringid=? ";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, valor);
                rs = stmt.executeQuery();
                if (rs.isBeforeFirst()) {

                    while (rs.next()) {
                        mapeoOfertas.setOfferingid(rs.getInt("offeringid"));
                        mapeoOfertas.setOfferingparent(rs.getInt("offeringparent"));
                        mapeoOfertas.setOfferingname(rs.getString("offeringname"));
                        mapeoOfertas.setType(rs.getString("type"));
                        mapeoOfertas.setOfferingcode(rs.getString("offeringcode"));
                        mapeoOfertas.setPaymentmode(rs.getInt("paymentmode"));
                    }
                } else {
                    log.error("NO SE ENCONTRO VALORES PARA EL OFFERINGID: " + valor);
                    mapeoOfertas.setOfferingname(PropiedadesGQL.getGqlNofound());
                }
                conn.close();
            }
        } catch (SQLException e) {
            log.error("[getCosName] Error al procesar bloque " + " -> SqlException: " + e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    log.warn("[getCosName] Error al intentar cerrar PreparedStatement : " + ex.getMessage());
                }
            }

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    log.warn("[getCosName] Error al intentar recuperar : " + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    log.warn("[getCosName] Error al intentar cerrar Connection : " + ex.getMessage());
                }
            }

        }
        return mapeoOfertas;
    }


    @SuppressWarnings("empty-statement")
    public static List<MapeoOfertas> getLista() {
        List<MapeoOfertas> mapeoOfertasList = new ArrayList<>();
        String sql = "select offeringid,offeringparent,offeringname,type,offeringcode,paymentmode from mapeo_ofertas where estado='t'";
        ConexionBDSource bdSource = new ConexionBDSource();
        try (
                Connection conn = bdSource.getConnection();
                Statement stmt = conn.prepareStatement(sql);
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            if (rs != null) {
                if (rs.isBeforeFirst()) {

                    while (rs.next()) {
                        MapeoOfertas mapeoOfertas = new MapeoOfertas();
                        mapeoOfertas.setOfferingid(rs.getInt("offeringid"));
                        mapeoOfertas.setOfferingparent(rs.getInt("offeringparent"));
                        mapeoOfertas.setOfferingname(rs.getString("offeringname"));
                        mapeoOfertas.setType(rs.getString("type"));
                        mapeoOfertas.setOfferingcode(rs.getString("offeringcode"));
                        mapeoOfertas.setPaymentmode(rs.getInt("paymentmode"));
                        mapeoOfertasList.add(mapeoOfertas);
                    }
                } else {
                    log.error("NO SE ENCONTRO VALORES PARA EL OFFERINGID: ");
                }
            }

        } catch (SQLException e) {
            log.error("[getCosName] Error al procesar bloque " + " -> SqlException: " + e.getMessage(), e);
        }
        return mapeoOfertasList;
    }


}
