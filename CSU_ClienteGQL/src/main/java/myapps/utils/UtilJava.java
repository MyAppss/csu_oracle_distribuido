package myapps.utils;


import java.text.ParseException;


import myapps.cmn.vo.Offer;
import myapps.gql.*;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.CheckForNull;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UtilJava {
    private static final Logger log = LogManager.getLogger(UtilJava.class);
    private static final String SESSION = "[SESSION: ";
    private static final String LINEA = " LINEA: ";

    /**
     * @param time tiempo en milisegundos que se usara como timeout para hacer las peticiones Rest
     * @return Objeto de tipo HttpComponentsClientHttpRequestFactory cargado con el timeout
     */
    public static HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory(int time) {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        //Connect timeout
        clientHttpRequestFactory.setConnectTimeout(time);

        //Read timeout
        clientHttpRequestFactory.setReadTimeout(time);
        return clientHttpRequestFactory;
    }

    public static RestTemplate commonsRestTemplate(int timeout) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(timeout);
        factory.setReadTimeout(timeout);
        return new RestTemplate(factory);
    }


    /**
     * El metodo  convertirFecha convierte un String a Calendar verificando que el String de la fecha no este vacia o sea null
     *
     * @param fechaString La fecha String que se desea convertir a Calendar
     * @param formato     El formato que tiene la fecha String pra que se convierta a Calendar, este debe una parametro cargado desde la Properties
     * @return
     */
    public static Calendar convertirFecha(String fechaString, String formato) {
        Calendar calendar = null;
        if (fechaString != null && !fechaString.trim().isEmpty()) {
            try {
                SimpleDateFormat format = new SimpleDateFormat(formato, Locale.getDefault());
                format.parse(fechaString);
                calendar = format.getCalendar();
            } catch (ParseException e) {
                log.error("Error al formatear la fecha: " + fechaString);
            }
        } else {
            log.error("La fecha obenida es null o esta vacia" + fechaString);
        }
        return calendar;
    }

    /**
     * Metodo Auxiliar para igual el nombre de la billetera primaria de GQL al de TSB
     *
     * @param valor nombre de las billeteras primarias a convertir
     * @return
     */
    public static String validarCore(String valor) {
        if (valor.equals("CORE_BALANCE")) {
            valor = "CORE BALANCE";
        }
        return valor;
    }

    /**
     * El metodo maxNodeFreeUnitsDetail obtiene el nodo con la fechas mas alta para sacar el valor de Reservation value y la fecha de expiracion
     *
     * @param free Es unn objeto que tiene la lista de Nodos
     * @return node con la fecha de expiracion mas alta
     */
    @CheckForNull
    public static Node_ maxNodeFreeUnitsDetail(FreeUnitsDetails free, String isdn, String session) {
        Calendar calendar = convertirFecha(PropiedadesGQL.getGqlFECHAMINIMA(), PropiedadesGQL.getGqlDATEFORMAT());
        Node_ node = new Node_();
        if (free != null) {
            for (Edge_ edges : free.getEdges()) {
                Calendar cal2 = convertirFecha(edges.getNode().getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT());
                if (cal2 != null && calendar != null && calendar.before(cal2)) {
                    calendar = cal2;
                    node = edges.getNode();
                }
            }
        } else {
            log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "FreeUnitsDetails es Null");
        }
        return node;
    }


    /**
     * El metodo maxNodeBalanceDetail obtiene el nodo con la fechas mas alta para sacar el valor de Reservation value y la fecha de expiracion
     *
     * @param balance Es unn objeto que tiene la lista de Nodos
     * @return node con la fecha de expiracion mas alta
     */
    public static Node_____ maxNodeBalanceDetail(BalanceDetail balance, String isdn, String session) {
        Calendar calendar = convertirFecha(PropiedadesGQL.getGqlFECHAMINIMA(), PropiedadesGQL.getGqlDATEFORMAT());
        Node_____ node = new Node_____();
        if (balance != null) {
            for (Edge_____ edges : balance.getEdges()) {
                Calendar cal2 = convertirFecha(edges.getNode().getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT());
                if (cal2 != null && calendar != null && calendar.before(cal2)) {
                    calendar = cal2;
                    node = edges.getNode();
                }
            }
        } else {
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "Node es Null para BalanceDetail");
        }
        return node;
    }

    /**
     * Este metodo obtiene el valor de una billetera si el valor de la billetera es menor a 0, set tomara los valores de los nodos
     * especificamente el valor de CurrentAmount
     *
     * @param node node de la billetera con todos sus datos
     * @return Retorna el saldo convertido
     */
    public static double obtenerValue(Node node, String isdn, String session) {
        double valor = 0;
        if (node != null && node.getFreeUnitsDetails() != null) {
            for (Edge_ edge : node.getFreeUnitsDetails().getEdges()) {
                valor += UtilJava.formatearValor(edge.getNode().getCurrentAmount(), node.getCbsSpecification().getUnitOfMeasure(), isdn, session);
            }

        } else {
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "Node es null");
        }
        return valor;
    }


    public static double obtenerReserva(Node node, String isdn, String session) {
        double valor = 0;
        if (node != null && node.getFreeUnitsDetails() != null) {
            for (Edge_ edge : node.getFreeUnitsDetails().getEdges()) {
                valor += UtilJava.formatearValor(edge.getNode().getReservationValue(), node.getCbsSpecification().getUnitOfMeasure(), isdn, session);
            }
        } else {
            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "Node es null");
        }
        return valor;
    }

    /**
     * Metodo Auxliar para redondear
     *
     * @param numero          numero a redondear
     * @param numeroDecimales cantidad de decimales que se quiere tener
     * @return nummero redondeado
     */
    private static double redondear(double numero, int numeroDecimales) {
        long mult = (long) Math.pow(10, numeroDecimales);
        return (Math.round(numero * mult)) / (double) mult;
    }


    /**
     * EL metodo formatearValor formatea los saldos de las billeteras de GQL que vienen en dinero al valor que viene en TSB a Unidades
     *
     * @param valor  Valor de la billetera
     * @param medida Medida de la Billetera
     * @return Devuelde el Saldo en las unidades Correspondientes
     */
    public static Double formatearValor(float valor, String medida, String isdn, String session) {
        double resultado = 0;
        if (medida != null) {
            switch (medida) {
                case "BOB":
                    resultado = valor / PropiedadesGQL.getGqlBOB();
                    break;
                case "KB":
                    resultado = (valor / PropiedadesGQL.getGqlKB()) * PropiedadesGQL.getPrecioKB();
                    break;
                case "Item":
                    resultado = valor * PropiedadesGQL.getGqlItem();
                    break;
                case "Second":
                    resultado = valor;
                    break;
                default:
                    log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "No existe la Unidad de Medida " + medida + " Se enviara " + resultado + " como valor por defecto");
                    break;
            }
        }
        return redondear(resultado, PropiedadesGQL.getGqlDecimales());
    }


    /**
     * Metodo que imprimi las billeteras del Objeto ResponceCCWS
     *
     * @param walletComverses lista de billeteras
     * @param offerList       lista de billeteras Offer
     * @param session         session con la que se HIzo la peticion
     * @param isdn            numero del abonado o msisdn
     */
    public static void imprimirBilleteras(List<WalletComverse> walletComverses, List<Offer> offerList, String session, String isdn) {
        long a = System.currentTimeMillis();
        String strDateFormat = PropiedadesGQL.getFechaImprimirBIlleteras();
        log.info(SESSION + session + LINEA + isdn + "] " + "INICIAR IMPRIMIR BILLETREAS");
        if (walletComverses != null && !walletComverses.isEmpty()) {
            log.info(SESSION + session + LINEA + isdn + "] " + "BILLETERAS WALLET");
            for (WalletComverse wallet : walletComverses) {
                SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                log.info(SESSION + session + LINEA + isdn + "] " + " WALLET: " + wallet.getNameWallet() + " SALDO: " + wallet.getAvailableBalance() + " " + wallet.getUnitOfMeasure() + "  FECHA EXP: " + objSDF.format(wallet.getExpiration().getTime()));
            }
        }
        if (offerList != null && !offerList.isEmpty()) {
            log.info(SESSION + session + LINEA + isdn + "] " + "BILLETERAS OFFER");
            for (Offer offer : offerList) {
                SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                log.info(SESSION + session + LINEA + isdn + "] " + " OFFER: " + offer.getName() + " ESTADO: " + offer.getState() + "  FECHA INIT: " + objSDF.format(offer.getInstantiationTime().getTime()));
            }
        }
        log.info(SESSION + session + LINEA + isdn + "] " + "FINALIZAR IMPRIMIR BILLETERAS EN " + (System.currentTimeMillis() - a) + " ms.");

    }

    /**
     * Metodo Auxiliar para oobtener los estado de las billeteras en ListOffer
     *
     * @param numero status en numero
     * @return stattus en String
     */
    public static String obtenerState(String numero, String isdn, String session) {
        String valor = "INACTIVE";
        if (numero.equals(PropiedadesGQL.getGqlChilProdutActivo())) {
            valor = "ENABLED";
        } else if (numero.equals(PropiedadesGQL.getGqlChilProdutSuspendido())) {
            valor = "DISABLED";
        } else {
            log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "Estado " + numero + " No Definico");
        }
        return valor;
    }


    public static Calendar smYear(Date fecha, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.YEAR, year);
        return calendar;
    }


}
