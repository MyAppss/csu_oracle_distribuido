package myapps.utils;

import myapps.dao.MapeoOfertasSingleton;
import myapps.gql.*;
import myapps.cmn.vo.Offer;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class Operaciones {
    private static final Logger log = LogManager.getLogger(Operaciones.class);


    /**
     * Metodo que procesa y extrae las billeteras primarias del response obtenido de la peticion al endpoint GraphQl
     * Recibe un objeto de tipo  CbsRelatedBillingAccounts, lo recorre y obtiene las billeteras de los Node,
     * Se itera 2 veces, por que se especifico que cada billetera tiene una reserva, la cual no viene con nombre pero si hereda
     *
     * @param cbsRBA: objeto que contiene las billeteras primarias
     * @return Lista wallet con las billeteras primarias
     */
    public static List<WalletComverse> obtenerBilleterasPrimarias(CbsRelatedBillingAccounts cbsRBA, String isdn, String session) {
        long tInicio = System.currentTimeMillis();
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::||| INICIANDO CONVERTIR BILLETERAS PRIMARIAS |||:::...");
        List<WalletComverse> listWallet = new ArrayList<>();
        if (cbsRBA != null && cbsRBA.getEdges() != null) {
            for (Edge___ edges : cbsRBA.getEdges()) {
                for (Edge____ edges1 : edges.getNode().getBucketBalancesChargingBalances().getEdges()) {
                    for (int i = 1; i <= 2; i++) {
                        WalletComverse wallet = new WalletComverse();
                        Node_____ node = UtilJava.maxNodeBalanceDetail(edges1.getNode().getBalanceDetail(), isdn, session);
                        if (node != null) {
                            if (i == 1) {
                                wallet.setAvailableBalance(UtilJava.formatearValor(edges1.getNode().getValue(), edges1.getNode().getCbsSpecification().getCurrency(), isdn, session) - UtilJava.formatearValor(node.getReservationValue(), edges1.getNode().getCbsSpecification().getCurrency(), isdn, session));
                                wallet.setNameWallet(UtilJava.validarCore(edges1.getNode().getCbsSpecification().getName()));
                                wallet.setExpiration(UtilJava.convertirFecha(node.getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                                wallet.setUnitOfMeasure(edges1.getNode().getCbsSpecification().getCurrency());
                            } else {
                                wallet.setAvailableBalance(UtilJava.formatearValor(node.getReservationValue(), edges1.getNode().getCbsSpecification().getCurrency(), isdn, session));
                                wallet.setNameWallet(UtilJava.validarCore(edges1.getNode().getCbsSpecification().getName()) + " " + PropiedadesGQL.getNombreReseva());
                                wallet.setExpiration(UtilJava.convertirFecha(node.getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                                wallet.setUnitOfMeasure(edges1.getNode().getCbsSpecification().getCurrency());
                            }
                            listWallet.add(wallet);
                        }
                    }
                }
            }
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::||| TIEMPO DEL PROCESO EN TERMINAR BILLETERAS PRIMARIAS: " + (System.currentTimeMillis() - tInicio) + " ms |||:::...");
        return listWallet;
    }


    /**
     * Metodo que procesa y devuelve las billeteras secundarias
     * Recibe un objeto de tipo  CbsBucketBalancesFreeUnits, lo recorre y obtiene las billeteras de los Node,
     * Se itera 2 veces, por que se especifico que cada billetera tiene una reserva, la cual no viene con nombre pero si hereda
     *
     * @param cbsBBFU: Objeto de tipo  CbsBucketBalancesFreeUnits que contiene las billeteras secudarias
     * @return Lista wallet con las billeteras secundarias
     */
    public static List<WalletComverse> obtenerBilleterasSecundarias(CbsBucketBalancesFreeUnits cbsBBFU, String isdn, String session) {
        long tInicio = System.currentTimeMillis();
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|||| INICIANDO CONVERTIR BILLETERAS SECUNDARIAS ||||:::...");
        List<WalletComverse> listWallet = new ArrayList<>();
        if (cbsBBFU != null && cbsBBFU.getEdges() != null) {
            for (Edge edgesFree : cbsBBFU.getEdges()) {
                for (int i = 1; i <= 2; i++) {
                    WalletComverse walletComverseFree = new WalletComverse();
                    Node_ node = UtilJava.maxNodeFreeUnitsDetail(edgesFree.getNode().getFreeUnitsDetails(), isdn, session);
                    if (node != null) {
                        if (i == 1) {
                            /**walletComverseFree.setAvailableBalance(UtilJava.formatearValor(edgesFree.getNode().getValue(), edgesFree.getNode().getCbsSpecification().getUnitOfMeasure()));*/
                            /** walletComverseFree.setAvailableBalance(UtilJava.obtenerValue(edgesFree.getNode()) - UtilJava.formatearValor(node.getReservationValue(), edgesFree.getNode().getCbsSpecification().getUnitOfMeasure()));*/
                            /** walletComverseFree.setAvailableBalance(UtilJava.formatearValor(edgesFree.getNode().getValue(), edgesFree.getNode().getCbsSpecification().getUnitOfMeasure()) - UtilJava.formatearValor(node.getReservationValue(), edgesFree.getNode().getCbsSpecification().getUnitOfMeasure()));*/

                            walletComverseFree.setAvailableBalance(UtilJava.obtenerValue(edgesFree.getNode(), isdn, session) - UtilJava.obtenerReserva(edgesFree.getNode(), isdn, session));
                            walletComverseFree.setNameWallet(edgesFree.getNode().getCbsSpecification().getName());
                            walletComverseFree.setExpiration(UtilJava.convertirFecha(node.getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                            walletComverseFree.setUnitOfMeasure(edgesFree.getNode().getCbsSpecification().getUnitOfMeasure());
                        } else {
                            //  walletComverseFree.setAvailableBalance(UtilJava.formatearValor(node.getReservationValue(), edgesFree.getNode().getCbsSpecification().getUnitOfMeasure()));
                            walletComverseFree.setAvailableBalance(UtilJava.obtenerReserva(edgesFree.getNode(), isdn, session));
                            walletComverseFree.setNameWallet(UtilJava.validarCore(edgesFree.getNode().getCbsSpecification().getName()) + " " + PropiedadesGQL.getNombreReseva());
                            walletComverseFree.setExpiration(UtilJava.convertirFecha(node.getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                            walletComverseFree.setUnitOfMeasure(edgesFree.getNode().getCbsSpecification().getUnitOfMeasure());
                        }
                        listWallet.add(walletComverseFree);
                    }
                }
            }
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|||| TIEMPO DEL PROCESO EN TERMINAR BILLETERAS SECUNDARIAS: " + (System.currentTimeMillis() - tInicio) + " ms ||||:::...");
        return listWallet;
    }


    /**
     * Metodo encargado de obtener el CosName
     * Itera los Node en busca de un offeringID valido dentro de CbsChildProducts (Activo o Suspendido), si no lo encuentra usara el offerinID principal pra obtener el CosName
     *
     * @param products:  objeto  CbsChildProducts
     * @param offeringId :OfferingId principal del objeto CbsResources
     * @return retornara el CosName pera el objeto ResponseCCWS
     */
    public static String obtenerCosName(CbsChildProducts products, String offeringId, String isdn, String session) {
        long tInicio = System.currentTimeMillis();
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|| INICIANDO OBTENCION DEL COSNAME ||:::...");
        String cosName = "";
        if (products != null && products.getEdges() != null) {
            for (Edge__ edge : products.getEdges()) {
                if (edge.getNode().getStatus().equals(PropiedadesGQL.getGqlChilProdutActivo())) {
                    //  MapeoOfertas mapeoOfertasaux = MapeoOfertasDAO.getCosName(Integer.parseInt(edge.getNode().getOfferingId()));
                    MapeoOfertas mapeoOfertasaux = MapeoOfertasSingleton.getInstance().getCosName(Integer.parseInt(edge.getNode().getOfferingId()));
                    if (mapeoOfertasaux.getOfferingid() != null && mapeoOfertasaux.getOfferingname() != null && mapeoOfertasaux.getType().equals(PropiedadesGQL.getGqlTYPE())) {
                        cosName = mapeoOfertasaux.getOfferingname();
                        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "El OFFERINGID DE CHILDPRODUCT " + edge.getNode().getOfferingId() + " ESTA ACTIVO Y SU OFFERINGPARENT ES: " + mapeoOfertasaux.getOfferingname());
                        break;
                    }
                } else if (edge.getNode().getStatus().equals(PropiedadesGQL.getGqlChilProdutSuspendido())) {
                    // MapeoOfertas aux = MapeoOfertasDAO.getCosName(Integer.parseInt(edge.getNode().getOfferingId()));
                    MapeoOfertas aux = MapeoOfertasSingleton.getInstance().getCosName(Integer.parseInt(edge.getNode().getOfferingId()));

                    if (aux.getOfferingparent() != null && aux.getType().equals(PropiedadesGQL.getGqlTYPE())) {
                        //cosName = MapeoOfertasDAO.getCosName(aux.getOfferingparent()).getOfferingname();
                        cosName = MapeoOfertasSingleton.getInstance().getCosName(aux.getOfferingparent()).getOfferingname();

                        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "EL OFFERINGID DE CHILDPRODUCT " + edge.getNode().getOfferingId() + " ESTA SUSPENDIDO Y HEREDARA EL OFFERINGPARENT: " + cosName);
                        break;
                    }
                }

            }
        }
        if (cosName.isEmpty()) {
            try {
                // MapeoOfertas aux = MapeoOfertasDAO.getCosName(Integer.parseInt(offeringId));
                MapeoOfertas aux = MapeoOfertasSingleton.getInstance().getCosName(Integer.parseInt(offeringId));

                if (aux != null && aux.getOfferingname() != null) {
                    cosName = aux.getOfferingname();
                } else {
                    cosName = PropiedadesGQL.getGqlNofound();
                }
            } catch (Exception e) {
                log.error("[SESSION: " + session + " LINEA: " + isdn + "] " + "No Numerico");
            }

            log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "NO SE OBTUVO UN OFFERINGID VALIDO EN CHILDPRODUCT, COSNAME " + cosName + " SE OBTUVO DE PRIMARYOFFERINGID " + offeringId);
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|| TIEMPO DE OBTENCION DEL COSNAME: " + (System.currentTimeMillis() - tInicio) + " ms ||:::...");
        return cosName;
    }

    /**
     * El Metodo obtenerListOffer obtiene los nombres para las billeteras en ListOffer las cuales se obtinene de los OferringID cel objeto CbsChildProducts
     *
     * @param products Objeto que contiene las billeteras para ListOffer
     * @return ListOffer para el objeto ResponseCCWS
     */
    public static List<Offer> obtenerListOffer(CbsChildProducts products, String isdn, String session) {
        long tInicio = System.currentTimeMillis();
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|| INICIANDO OBTENCION DE LISTOFFER ||:::...");
        List<Offer> listaOffer = new ArrayList<>();
        if (products != null && products.getEdges() != null) {
            for (Edge__ edges : products.getEdges()) {
                //MapeoOfertas mapeoOfertas = MapeoOfertasDAO.getCosName(Integer.parseInt(edges.getNode().getOfferingId()));
                MapeoOfertas mapeoOfertas = MapeoOfertasSingleton.getInstance().getCosName(Integer.parseInt(edges.getNode().getOfferingId()));
                if (mapeoOfertas.getOfferingname() != PropiedadesGQL.getGqlNofound()) {
                    Offer offer = new Offer();
                    offer.setName(mapeoOfertas.getOfferingname());
                    offer.setServiceStart(UtilJava.convertirFecha(edges.getNode().getValidFor().getStartDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setServiceEnd(UtilJava.convertirFecha(edges.getNode().getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setState(UtilJava.obtenerState(edges.getNode().getStatus(), isdn, session));
                    offer.setInstantiationTime(UtilJava.convertirFecha(edges.getNode().getValidFor().getStartDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setUsada(false);
                    listaOffer.add(offer);
                } else {
                    log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "EL OfferingId " + edges.getNode().getOfferingId() + " no se encuentra an la tabla Mapeo Ofertas");
                    Offer offer = new Offer();
                    offer.setName("OFFER_" + edges.getNode().getOfferingId());
                    offer.setServiceStart(UtilJava.convertirFecha(edges.getNode().getValidFor().getStartDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setServiceEnd(UtilJava.convertirFecha(edges.getNode().getValidFor().getEndDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setState(UtilJava.obtenerState(edges.getNode().getStatus(), isdn, session));
                    offer.setInstantiationTime(UtilJava.convertirFecha(edges.getNode().getValidFor().getStartDatetime(), PropiedadesGQL.getGqlDATEFORMAT()));
                    offer.setUsada(false);
                    listaOffer.add(offer);
                }
            }
        }
        log.info("[SESSION: " + session + " LINEA: " + isdn + "] " + "...:::|| TIEMPO DE OBTENCION DEL LISTOFFER: " + (System.currentTimeMillis() - tInicio) + " ms ||:::...");
        return listaOffer;
    }
}
