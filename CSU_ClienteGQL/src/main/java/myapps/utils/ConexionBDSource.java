package myapps.utils;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.sql.*;

public class ConexionBDSource {
    private static final Logger log = LogManager.getLogger(ConexionBDSource.class);
    private static final String URL_BD = PropiedadesGQL.getUrlBD();
    private static final String USUARIO_BD = PropiedadesGQL.getUsuarioBD();
    private static final String PASSWORD_BD = PropiedadesGQL.getPasswordBD();
    private final DataSource datasource;
    private static ConexionBDSource instance;

    /**
     * Metodo que crea la conexion y devuelve un datasource
     *
     * @return: Datasource
     */
    private DataSource setupDataSource() {
        PoolProperties poolProperties = new PoolProperties();

        //  poolProperties.setDriverClassName("org.postgresql.Driver");
        poolProperties.setDriverClassName("oracle.jdbc.OracleDriver");

       // poolProperties.setInitSQL("SET application_name = 'cliente-GQL';");
        poolProperties.setUrl(URL_BD);
        poolProperties.setUsername(USUARIO_BD);
        poolProperties.setPassword(PASSWORD_BD);
        poolProperties.setInitialSize(2);
        poolProperties.setJmxEnabled(true);
        poolProperties.setTestWhileIdle(false);
        poolProperties.setTestOnBorrow(true);
        poolProperties.setTestOnReturn(false);
        poolProperties.setValidationQuery("SELECT 1 FROM DUAL");

        poolProperties.setValidationInterval(30000);
        poolProperties.setTimeBetweenEvictionRunsMillis(30000);
        poolProperties.setMinEvictableIdleTimeMillis(PropiedadesGQL.getSetminevictableidletimemillisBD());
        poolProperties.setMaxActive(PropiedadesGQL.getSetmaxactiveBD());
        poolProperties.setMinIdle(PropiedadesGQL.getSetminidleBD());
        poolProperties.setMaxIdle(PropiedadesGQL.getSetMaxIdleBD());
        poolProperties.setMaxWait(PropiedadesGQL.getSetMaxWaitBD());
        poolProperties.setRemoveAbandonedTimeout(60);
        poolProperties.setLogAbandoned(true);
        poolProperties.setRemoveAbandoned(true);
        poolProperties.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        DataSource dataSource = new DataSource();
        dataSource.setPoolProperties(poolProperties);

        return dataSource;

    }

    public ConexionBDSource() {
        datasource = setupDataSource();
    }

    /**
     * getConnection devuelve las conexiones para hacer las consultas
     *
     * @return: Devuelve una conexion
     */
    public synchronized Connection getConnection() {
        Connection conexion = null;
        try {
            if (instance == null) {
                instance = new ConexionBDSource();
            }
            // log.debug("...::: ESTABLECIENDO CONEXION HACIA: " + URL_BD);
            conexion = instance.datasource.getConnection();
        } catch (SQLException ex) {
            log.error("...::: NO SE PUDO ESTABLECER CONEXION HACIA: " + URL_BD + " Exception: " + ex.getMessage() + " :::...");
        }
        return conexion;
    }
}
