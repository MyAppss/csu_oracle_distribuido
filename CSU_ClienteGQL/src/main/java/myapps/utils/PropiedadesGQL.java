package myapps.utils;

import java.io.Serializable;

public class PropiedadesGQL implements Serializable {

    private static String gqlENDPOINT;
    private static String gqlMESSAGE;
    private static String gqlDATEFORMAT;
    private static Integer gqlTIMEOUT;
    private static String gqlFECHAMINIMA;
    private static String urlBD;
    private static String usuarioBD;
    private static String passwordBD;
    private static int setminevictableidletimemillisBD;
    private static int setmaxactiveBD;
    private static int setminidleBD;
    private static int setMaxIdleBD;
    private static int setMaxWaitBD;
    private static double gqlBOB;
    private static double gqlKB;
    private static double precioKB;
    private static double gqlItem;
    private static int gqlDecimales;

    private static String gqlNofound;
    private static String gqlTYPE;
    private static String gqlChilProdutActivo;
    private static String gqlChilProdutSuspendido;
    private static boolean imprimirBilleteras;
    private static String nombreReseva;
    private static String fechaImprimirBIlleteras;


    public static String getGqlENDPOINT() {
        return gqlENDPOINT;
    }

    public static void setGqlENDPOINT(String gqlENDPOINT) {
        PropiedadesGQL.gqlENDPOINT = gqlENDPOINT;
    }

    public static String getGqlMESSAGE() {
        return gqlMESSAGE;
    }

    public static void setGqlMESSAGE(String gqlMESSAGE) {
        PropiedadesGQL.gqlMESSAGE = gqlMESSAGE;
    }

    public static String getGqlDATEFORMAT() {
        return gqlDATEFORMAT;
    }

    public static void setGqlDATEFORMAT(String gqlDATEFORMAT) {
        PropiedadesGQL.gqlDATEFORMAT = gqlDATEFORMAT;
    }

    public static Integer getGqlTIMEOUT() {
        return gqlTIMEOUT;
    }

    public static void setGqlTIMEOUT(Integer gqlTIMEOUT) {
        PropiedadesGQL.gqlTIMEOUT = gqlTIMEOUT;
    }

    public static String getGqlFECHAMINIMA() {
        return gqlFECHAMINIMA;
    }

    public static void setGqlFECHAMINIMA(String gqlFECHAMINIMA) {
        PropiedadesGQL.gqlFECHAMINIMA = gqlFECHAMINIMA;
    }


    public static String getUrlBD() {
        return urlBD;
    }

    public static void setUrlBD(String urlBD) {
        PropiedadesGQL.urlBD = urlBD;
    }

    public static String getUsuarioBD() {
        return usuarioBD;
    }

    public static void setUsuarioBD(String usuarioBD) {
        PropiedadesGQL.usuarioBD = usuarioBD;
    }

    public static String getPasswordBD() {
        return passwordBD;
    }

    public static void setPasswordBD(String passwordBD) {
        PropiedadesGQL.passwordBD = passwordBD;
    }

    public static int getSetminevictableidletimemillisBD() {
        return setminevictableidletimemillisBD;
    }

    public static void setSetminevictableidletimemillisBD(int setminevictableidletimemillisBD) {
        PropiedadesGQL.setminevictableidletimemillisBD = setminevictableidletimemillisBD;
    }

    public static int getSetmaxactiveBD() {
        return setmaxactiveBD;
    }

    public static void setSetmaxactiveBD(int setmaxactiveBD) {
        PropiedadesGQL.setmaxactiveBD = setmaxactiveBD;
    }

    public static int getSetminidleBD() {
        return setminidleBD;
    }

    public static void setSetminidleBD(int setminidleBD) {
        PropiedadesGQL.setminidleBD = setminidleBD;
    }

    public static int getSetMaxIdleBD() {
        return setMaxIdleBD;
    }

    public static void setSetMaxIdleBD(int setMaxIdleBD) {
        PropiedadesGQL.setMaxIdleBD = setMaxIdleBD;
    }

    public static int getSetMaxWaitBD() {
        return setMaxWaitBD;
    }

    public static void setSetMaxWaitBD(int setMaxWaitBD) {
        PropiedadesGQL.setMaxWaitBD = setMaxWaitBD;
    }

    public static double getGqlBOB() {
        return gqlBOB;
    }

    public static void setGqlBOB(double gqlBOB) {
        PropiedadesGQL.gqlBOB = gqlBOB;
    }

    public static double getGqlKB() {
        return gqlKB;
    }

    public static void setGqlKB(double gqlKB) {
        PropiedadesGQL.gqlKB = gqlKB;
    }

    public static double getPrecioKB() {
        return precioKB;
    }

    public static void setPrecioKB(double precioKB) {
        PropiedadesGQL.precioKB = precioKB;
    }

    public static double getGqlItem() {
        return gqlItem;
    }

    public static void setGqlItem(double gqlItem) {
        PropiedadesGQL.gqlItem = gqlItem;
    }

    public static int getGqlDecimales() {
        return gqlDecimales;
    }

    public static void setGqlDecimales(int gqlDecimales) {
        PropiedadesGQL.gqlDecimales = gqlDecimales;
    }

    public static String getGqlNofound() {
        return gqlNofound;
    }

    public static void setGqlNofound(String gqlNofound) {
        PropiedadesGQL.gqlNofound = gqlNofound;
    }

    public static String getGqlTYPE() {
        return gqlTYPE;
    }

    public static void setGqlTYPE(String gqlTYPE) {
        PropiedadesGQL.gqlTYPE = gqlTYPE;
    }

    public static String getGqlChilProdutActivo() {
        return gqlChilProdutActivo;
    }

    public static void setGqlChilProdutActivo(String gqlChilProdutActivo) {
        PropiedadesGQL.gqlChilProdutActivo = gqlChilProdutActivo;
    }

    public static String getGqlChilProdutSuspendido() {
        return gqlChilProdutSuspendido;
    }

    public static void setGqlChilProdutSuspendido(String gqlChilProdutSuspendido) {
        PropiedadesGQL.gqlChilProdutSuspendido = gqlChilProdutSuspendido;
    }

    public static boolean isImprimirBilleteras() {
        return imprimirBilleteras;
    }

    public static void setImprimirBilleteras(boolean imprimirBilleteras) {
        PropiedadesGQL.imprimirBilleteras = imprimirBilleteras;
    }

    public static String getNombreReseva() {
        return nombreReseva;
    }

    public static void setNombreReseva(String nombreReseva) {
        PropiedadesGQL.nombreReseva = nombreReseva;
    }

    public static String getFechaImprimirBIlleteras() {
        return fechaImprimirBIlleteras;
    }

    public static void setFechaImprimirBIlleteras(String fechaImprimirBIlleteras) {
        PropiedadesGQL.fechaImprimirBIlleteras = fechaImprimirBIlleteras;
    }
}
