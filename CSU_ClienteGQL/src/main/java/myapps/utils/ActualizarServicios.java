package myapps.utils;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

public class ActualizarServicios {
    private static final Logger log = LogManager.getLogger(ActualizarServicios.class);


    public static String actualizarServicio(String url, boolean ssl) {
        String valor = "Fallido";
        try {
            // while, contador, boleano
            RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory(ssl));
            MultiValueMap<String, String> varia = new LinkedMultiValueMap<>();
            ResponseEntity<String> result = restTemplate.postForEntity(url, varia, String.class);
            switch (result.getStatusCode()) {
                case OK:
                    log.info("ESTATUS CODE " + HttpStatus.OK.getReasonPhrase());
                    valor = result.getStatusCode().toString();
                    break;
                case ACCEPTED:
                    break;
                case BAD_REQUEST:
                    log.info("ESTATUS CODE " + HttpStatus.BAD_REQUEST.getReasonPhrase());
                    break;
                default:
                    log.info("ESTATUS CODE NO DEFINIDO " + result.getStatusCode());
            }

        } catch (Exception e) {
            log.error(url + " Exception: " + e.getMessage());
        }
        return valor;
    }

    public static HttpComponentsClientHttpRequestFactory getClientHttpRequestFactory(boolean ssl) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        try {
            TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) {
                    return true;
                }
            };
            /**
             * TODO SP2
             * CID 35971 (#1 of 1): Resource leak (RESOURCE_LEAK)4. leaked_resource:
             * Variable httpClient going out of scope leaks the resource it refers to.
             */
            requestFactory.setConnectTimeout(20000);
            if (!ssl) {
                SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
                SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
                CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
                requestFactory.setHttpClient(httpClient);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return requestFactory;
    }
}
