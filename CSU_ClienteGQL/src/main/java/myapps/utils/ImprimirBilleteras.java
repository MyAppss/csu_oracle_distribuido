package myapps.utils;

import myapps.cmn.vo.Offer;
import myapps.cmn.vo.WalletComverse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.List;

public class ImprimirBilleteras extends Thread {
    private static final Logger log = LogManager.getLogger(ImprimirBilleteras.class);
    private static final String SESSIONLOG = "[SESSION: ";
    private static final String LINEA = " LINEA: ";

    private List<WalletComverse> walletComverses;
    private List<Offer> offerList;
    private String session;
    private String isdn;

    public ImprimirBilleteras(List<WalletComverse> walletComverses, List<Offer> offerList, String session, String isdn) {
        this.walletComverses = walletComverses;
        this.offerList = offerList;
        this.session = session;
        this.isdn = isdn;
    }

    @Override
    public void run() {
        long a = System.currentTimeMillis();
        String strDateFormat = PropiedadesGQL.getFechaImprimirBIlleteras();
        log.info(SESSIONLOG + session + LINEA + isdn + "] " + "INICIAR IMPRIMIR BILLETREAS");
        if (walletComverses != null && !walletComverses.isEmpty()) {
            log.info(SESSIONLOG + session + LINEA + isdn + "] " + "BILLETERAS WALLET");
            for (WalletComverse wallet : walletComverses) {
                SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                log.info(SESSIONLOG + session + LINEA + isdn + "] " + " WALLET: " + wallet.getNameWallet() + " SALDO: " + wallet.getAvailableBalance() + " " + wallet.getUnitOfMeasure() + "  FECHA EXP: " + objSDF.format(wallet.getExpiration().getTime()));
            }
        }
        if (offerList != null && !offerList.isEmpty()) {
            log.info(SESSIONLOG + session + LINEA + isdn + "] " + "BILLETERAS OFFER");
            for (Offer offer : offerList) {
                SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                log.info(SESSIONLOG + session + LINEA + isdn + "] " + " OFFER: " + offer.getName() + " ESTADO: " + offer.getState() + "  FECHA INIT: " + objSDF.format(offer.getInstantiationTime().getTime()));
            }
        }
        log.info(SESSIONLOG + session + LINEA + isdn + "] " + "FINALIZAR IMPRIMIR BILLETERAS EN " + (System.currentTimeMillis() - a) + " ms.");

    }
}
