package myapps.cmn.vo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;

/**
 *
 * @author User
 */
public class Acumulador implements Serializable {
    private static final long serialVersionUID = 1234L;


    private String accumulatorName;
    private double amount;


    public Acumulador(String accumulatorName, double amount) {
        this.accumulatorName = accumulatorName;
        this.amount = amount;
    }

    public String getAccumulatorName() {
        return accumulatorName;
    }

    public void setAccumulatorName(String accumulatorName) {
        this.accumulatorName = accumulatorName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
