package myapps.cmn.vo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Vehimar
 */
public class Offer implements Serializable {
    private static final long serialVersionUID = 1234L;


    private String name;
    private Calendar serviceStart;
    private Calendar serviceEnd;
    private String state;
    private Calendar instantiationTime;
    private Boolean usada;

    public Offer() {
        super();
    }

    public Offer(String name, Calendar serviceStart, Calendar serviceEnd, String state, Calendar instantiationTime) {
        super();
        this.name = name;
        this.serviceStart = serviceStart;
        this.serviceEnd = serviceEnd;
        this.state = state;
        this.instantiationTime = instantiationTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getServiceStart() {
        return serviceStart;
    }

    public void setServiceStart(Calendar serviceStart) {
        this.serviceStart = serviceStart;
    }

    public Calendar getServiceEnd() {
        return serviceEnd;
    }

    public void setServiceEnd(Calendar serviceEnd) {
        this.serviceEnd = serviceEnd;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Calendar getInstantiationTime() {
        return instantiationTime;
    }

    public void setInstantiationTime(Calendar instantiationTime) {
        this.instantiationTime = instantiationTime;
    }

    public Boolean getUsada() {
        return usada;
    }

    public void setUsada(Boolean usada) {
        this.usada = usada;
    }
}
