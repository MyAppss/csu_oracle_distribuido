package myapps.cmn.vo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Vehimar
 */
public class WalletComverse implements Serializable {
    private static final long serialVersionUID = 1234L;


    private String nameWallet;
    private double availableBalance;
    private Calendar Expiration;
    private boolean usada;
    private transient String unitOfMeasure;

    public Calendar getExpiration() {
        return Expiration;
    }

    public void setExpiration(Calendar Expiration) {
        this.Expiration = Expiration;
    }

    public WalletComverse() {
        nameWallet = "";
        availableBalance = 0;
        Expiration = null;
    }

    /**
     * @return the nameWallet
     */
    public String getNameWallet() {
        return nameWallet;
    }

    /**
     * @param nameWallet the nameWallet to set
     */
    public void setNameWallet(String nameWallet) {
        this.nameWallet = nameWallet;
    }

    /**
     * @return the availableBalance
     */
    public double getAvailableBalance() {
        return availableBalance;
    }

    /**
     * @param availableBalance the availableBalance to set
     */
    public void setAvailableBalance(double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public boolean isUsada() {
        return usada;
    }

    public void setUsada(boolean usada) {
        this.usada = usada;
    }


    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    @Override
    public String toString() {
        return "WalletComverse{" + "nameWallet=" + nameWallet + ", availableBalance=" + availableBalance + ", Expiration=" + Expiration + ", usada=" + usada + '}';
    }
}
