/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author usuario
 */
public class ReporteConsulta implements Serializable {

    private static final long serialVersionUID = 1234L;

    private Timestamp fecha;
    private String sessionId;
    private String msisdn;
    private String identificadorServicio;
    private String nombreCanal;
    private String publicidadSolicitada;
    private int longitudMaxSolicitada;
    private String ipCiente;
    private String textoGenerado;
    private String opcion;

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getIdentificadorServicio() {
        return identificadorServicio;
    }

    public void setIdentificadorServicio(String identificadorServicio) {
        this.identificadorServicio = identificadorServicio;
    }

    public String getNombreCanal() {
        return nombreCanal;
    }

    public void setNombreCanal(String nombreCanal) {
        this.nombreCanal = nombreCanal;
    }

    public String getPublicidadSolicitada() {
        return publicidadSolicitada;
    }

    public void setPublicidadSolicitada(String publicidadSolicitada) {
        this.publicidadSolicitada = publicidadSolicitada;
    }

    public int getLongitudMaxSolicitada() {
        return longitudMaxSolicitada;
    }

    public void setLongitudMaxSolicitada(int longitudMaxSolicitada) {
        this.longitudMaxSolicitada = longitudMaxSolicitada;
    }

    public String getIpCiente() {
        return ipCiente;
    }

    public void setIpCiente(String ipCiente) {
        this.ipCiente = ipCiente;
    }

    public String getTextoGenerado() {
        return textoGenerado;
    }

    public void setTextoGenerado(String textoGenerado) {
        this.textoGenerado = textoGenerado;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    @Override
    public String toString() {
        return "ReporteConsulta{" + "fecha=" + fecha + ", sessionId=" + sessionId + ", msisdn=" + msisdn + ", identificadorServicio=" + identificadorServicio + ", nombreCanal=" + nombreCanal + ", publicidadSolicitada=" + publicidadSolicitada + ", longitudMaxSolicitada=" + longitudMaxSolicitada + ", ipCiente=" + ipCiente + ", textoGenerado=" + textoGenerado + ", opcion=" + opcion + '}';
    }
    
    

}
