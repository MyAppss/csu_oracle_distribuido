/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.cmn.vo;

/**
 *
 * @author hp
 */
public class ResponseUpdate {

    private String session;
    private String isdn;
    private Integer code;
    private String message;

    public ResponseUpdate() {
    }

    public ResponseUpdate(String session, String isdn, Integer code, String message) {
        this.session = session;
        this.isdn = isdn;
        this.code = code;
        this.message = message;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
