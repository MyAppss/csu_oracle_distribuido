/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import myapps.csu.util.ConexionBD;
import myapps.cmn.vo.ReporteConsulta;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ReporteConsultaDAO {

    public static final Logger LOG = Logger.getLogger(ReporteConsultaDAO.class);

    public static void saveReporteConsulta(ReporteConsulta rep) {
        long z = System.currentTimeMillis();
        LOG.info("Registrando la consulta ISDN: " + rep.getMsisdn() + "| CANAL: " + rep.getNombreCanal());
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                String sql = "insert  into REPORTE_CONSULTA (FECHA,MSISDN,IDENTIFICADOR_SERVICIO,NOMBRE_CANAL,PUBLICIDAD_SOLICITADA,LONGITUD_MAX_SOLICITADA,IP_CLIENTE,TEXTO_GENERADO,SESSIONID,OPCION) values (?,?,?,?,?,?,?,?,?,?)";

                stmt = conn.prepareStatement(sql);
                stmt.setTimestamp(1, rep.getFecha());
                stmt.setString(2, rep.getMsisdn());
                stmt.setString(3, rep.getIdentificadorServicio());
                stmt.setString(4, rep.getNombreCanal());
                stmt.setString(5, rep.getPublicidadSolicitada());
                stmt.setInt(6, rep.getLongitudMaxSolicitada());
                stmt.setString(7, rep.getIpCiente());
                stmt.setString(8, rep.getTextoGenerado());
                stmt.setString(9, rep.getSessionId());
                stmt.setString(10, rep.getOpcion());
                stmt.execute();
                LOG.info("[saveReporteConsulta]:nuevo registro guardado correctamente");
            }
        } catch (SQLException e) {
            LOG.error("[saveReporteConsulta]:error al intentar guardar:" + e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    LOG.warn("[saveReporteConsulta] Error al cerrar PreparedStatement", ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("[getCabecerasByConfigId] Error al intentar cerrar Connection "
                            + " SQLException:" + e.getMessage());
                }
            }
            LOG.info("...::: Session ID: " + rep.getSessionId() + " TERMINO GUARDAR CONSULTA TABLA RESPORTE_CONSULTA - " + (System.currentTimeMillis() - z) + " ms :::...");
            System.out.println("...::: Session ID: " + rep.getSessionId() + " TERMINO GUARDAR CONSULTA TABLA RESPORTE_CONSULTA - " + (System.currentTimeMillis() - z) + " ms :::...");
        }
    }

    /*
    UPDATE reporte_consulta
    SET opcion = 1
    WHERE "sessionId" = '000000001' AND msisdn ='76316685'
     */
    public static String updateReporteConsulta(ReporteConsulta reporte) {
        Connection conn = null;
        Statement st = null;
        String respuesta = "";
        try {
            //  String sql = "UPDATE reporte_consulta SET opcion = '" + reporte.getOpcion() + "' WHERE \"sessionId\" = '" + reporte.getSessionId() + "' AND msisdn ='" + reporte.getMsisdn() + "'";
            String sql = "UPDATE REPORTE_CONSULTA SET OPCION='" + reporte.getOpcion() + "' WHERE SESSIONID='" + reporte.getSessionId() + "' AND MSISDN='" + reporte.getMsisdn() + "'";

            LOG.debug("[updateReporteConsulta] SQL: " + sql);
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                st = conn.createStatement();
                int cant = st.executeUpdate(sql);
                if (cant == 0) {
                    respuesta = "sessionId o isdn no existe en la db";
                }
            }

        } catch (SQLException e) {
            LOG.warn("[updateReporteConsulta] Error al intentar actualizar al resporte con id" + reporte.getSessionId() + "SQLException:", e);
            respuesta = e.getMessage();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateReporteConsulta] Error al intentar cerrar Statement despues de actualizar reporte con id:" + reporte.getSessionId() + ". SQLException:", ex);
                    respuesta = ex.getMessage();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    LOG.warn("[updateReporteConsulta] Error al intentar cerrar Connection despues de actualizar user SQLException:", ex);
                    respuesta = ex.getMessage();
                }
            }
        }
        return respuesta;
    }

    /*public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement st = null;
        String respuesta = "";
        List<ReporteConsulta> lista = new ArrayList<>();
        try {

            String sesion = "0051";
            //  String sql = "UPDATE reporte_consulta SET opcion = '" + reporte.getOpcion() + "' WHERE \"sessionId\" = '" + reporte.getSessionId() + "' AND msisdn ='" + reporte.getMsisdn() + "'";
            String sql = "SELECT * FROM REPORTE_CONSULTA WHERE SESSIONID='" + sesion + "' AND  MSISDN=?";

            LOG.debug("[updateReporteConsulta] SQL: " + sql);
            conn = ConexionBD.openDBConnection();
            if (conn != null) {
                st = conn.prepareStatement(sql);
                // st.setInt(1, 0051);
                st.setString(1, "77816006");
                // st.setString(2, sql);
                ResultSet resultSet = st.executeQuery();
                while (resultSet.next()) {
                    ReporteConsulta reporteConsulta = new ReporteConsulta();
                    reporteConsulta.setMsisdn(resultSet.getString("MSISDN"));
                    lista.add(reporteConsulta);
                }
            }

        } catch (SQLException e) {
            respuesta = e.getMessage();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    respuesta = ex.getMessage();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    respuesta = ex.getMessage();
                }
            }
        }
        System.out.println(lista.size());
    }*/
}
