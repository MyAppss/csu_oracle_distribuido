/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.restful;

import com.google.gson.Gson;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import myapps.activeMQ.ColaJMS;
import myapps.activeMQ.ColaThreadMax;
import myapps.csu.bl.ActualizarBL;
import myapps.csu.util.Contantes;
import myapps.cmn.vo.ResponseUpdate;
import myapps.csu.util.Propiedades;
import myapps.csu.util.UtilJava;

/**
 * REST Web Service
 *
 * @author hp
 */
@Path("update")
public class ActualizarCSU {

    @Context
    private UriInfo context;

    /**
     * Retrieves representation of an instance of myapps.csu.restfull.updateCsu
     *
     * @param httpHeaders
     * @param requestContext
     * @param sessionId
     * @param isdn
     * @param valor
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("general")
    public Response updateCSU(@Context HttpHeaders httpHeaders,
            @Context HttpServletRequest requestContext,
            @QueryParam("sessionId") String sessionId,
            @QueryParam("isdn") String isdn,
            @QueryParam("valor") String valor) {
        String logPrefijo = " [sessionId: " + sessionId + "][isdn: " + isdn + "][valor: " + valor + "] ";

        String requisitos = UtilJava.requestValido(isdn, sessionId, valor, logPrefijo);
        if (requisitos.isEmpty()) {

            ActualizarBL business = new ActualizarBL();

            ResponseUpdate respuesta = new ResponseUpdate();
            String validar = business.validar(sessionId, isdn, valor);
            if ("".equals(validar)) {
                respuesta = business.encolar(sessionId, isdn, valor);
            } else {
                respuesta.setSession(sessionId);
                respuesta.setIsdn(isdn);
                respuesta.setCode(Contantes.CODE_ERROR_VALIDACION);
                respuesta.setMessage(Contantes.MSG_ERROR_VALIDACION.replace("%", validar));
            }
            Gson gson = new Gson();
            String representacionJSON = gson.toJson(respuesta);

            return Response.ok(representacionJSON, MediaType.APPLICATION_JSON).build();
        } else {
            String requestInvalido = UtilJava.devolverRespuestaFallida(isdn, sessionId, requisitos, logPrefijo);
            return Response.ok(requestInvalido, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * Retrieves representation of an instance of myapps.csu.restfull.status
     *
     * @param httpHeaders
     * @param requestContext
     *
     * @return an instance of java.lang.String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("status")
    public Response status(@Context HttpHeaders httpHeaders,
            @Context HttpServletRequest requestContext) {
        HashMap map = new HashMap();
        map.put("cola_size", ColaJMS.size());
        map.put("nro_hilos", Propiedades.MQ_HILOS);
        map.put("max_thread_disponiblbe", ColaThreadMax.size());
        Gson gson = new Gson();
        String representacionJSON = gson.toJson(map);

        return Response.ok(representacionJSON, MediaType.APPLICATION_JSON).build();
    }
}
