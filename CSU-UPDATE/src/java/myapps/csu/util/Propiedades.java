package myapps.csu.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Propiedades {

    private static final Logger log = Logger.getLogger(Propiedades.class);
    protected static final String CONFIG_FILE = "myapps/csu/properties/CSU.properties";
    protected static final Properties prop = new Properties();
    private static URL CONFIG_URL;
    private static InputStream CONFIG_FILE_STREAM;

    static {
        init();
    }

    public static final String DB_URL = prop.getProperty("bd.url");
    public static final String DB_USER = prop.getProperty("bd.usuario");
    public static final String DB_PASSWORD = prop.getProperty("bd.contrasenna");
    public static final String DB_MIN_EVICTABLE_IDLE_TIEM_MILLIS = prop.getProperty("bd.setminevictableidletimemillis");
    public static final String DB_MAX_ACTIVE = prop.getProperty("bd.setmaxactive");
    public static final String DB_MIN_IDLE = prop.getProperty("bd.setminidle");
    public static final String DB_MAX_IDLE = prop.getProperty("bd.setMaxIdle");
    public static final String DB_MAX_WAIT = prop.getProperty("bd.setMaxWait");

    public static final String MQ_USER = prop.getProperty("MQ.user");
    public static final String MQ_PASSWORD = prop.getProperty("MQ.password");
    public static final String MQ_COLA = prop.getProperty("MQ.nombreCola");
    public static final String MQ_IP = prop.getProperty("MQ.servidorIP");
    public static final int MQ_PUERTO = Integer.parseInt(prop.getProperty("MQ.servidorPuerto"));
    public static final String MQ_TIPO_CONEXION = prop.getProperty("MQ.tipoConexion");
    public static final int MQ_TIMEOUT = Integer.parseInt(prop.getProperty("MQ.timeout"));
    public static final int MQ_HILOS = Integer.parseInt(prop.getProperty("MQ.hilos"));
    public static final long MQ_HILOS_SLEEP = Long.parseLong(prop.getProperty("MQ.hilos.sleep"));
    public static final int MQ_MAX_THREAD = Integer.parseInt(prop.getProperty("MQ.maxThread"));
    
    public static final String PATTERN_TELEFONO = prop.getProperty("PATTERN.TELEFONO");
     public static final String   PATTERN_SESSION = prop.getProperty("PATTERN.SESSION");
      public static final String  PATTERN_VALOR = prop.getProperty("PATTERN.VALOR");

    public static void init() {
        Thread thr = Thread.currentThread();
        ClassLoader cL;
        if (thr != null) {
            cL = thr.getContextClassLoader();
            if (cL != null) {
                CONFIG_URL = cL.getResource(CONFIG_FILE);
                try {
                    CONFIG_FILE_STREAM = new FileInputStream(CONFIG_URL.getFile());
                    prop.load(CONFIG_FILE_STREAM);
                } catch (IOException e) {
                    log.error("ERROR: " + e.getMessage());
                }

            }
        }
    }

}
