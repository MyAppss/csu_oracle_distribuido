/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.util;

import com.google.gson.Gson;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import myapps.cmn.vo.ReporteConsulta;
import myapps.cmn.vo.ResponseUpdate;
import org.apache.log4j.Logger;

/**
 *
 * @author HP
 */
public class UtilJava {

    public static final Logger LOG = Logger.getLogger(ConexionBD.class);

    public static boolean probarConexion(String url, int puerto) {
        boolean valor = false;
        Socket socket = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(url, puerto), Propiedades.MQ_TIMEOUT);
            valor = socket.isConnected();
            socket.close();
        } catch (IOException e) {
            LOG.error("[probarConexion] No se pudo establecer conexion hacia " + url + ":" + puerto);
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException ex) {
                LOG.error("[probarConexion] Error al cerrar sSocket");
            }
        }

        return valor;
    }

    public static String maptoString(HashMap<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, Object> mapa;
        try {
            if (map != null) {
                if (map.containsKey("DETALLE_CONSULTA")) {
                    ReporteConsulta rc = (ReporteConsulta) map.get("DETALLE_CONSULTA");
                    stringBuilder.append(rc.toString());
                } else {
                    mapa = map;
                    mapa.entrySet().forEach((entry) -> {
                        stringBuilder.append(entry.getKey()).append(" = ").append(entry.getValue()).append("; ");
                    });
                }

            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        return stringBuilder.toString();
    }

    public static String requestValido(String telefono, String session, String valor, String logPrefijo) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!validarCadena(telefono, Propiedades.PATTERN_TELEFONO)) {
            stringBuilder.append(" -Formato Telefono no Valido ").append(Propiedades.PATTERN_TELEFONO);
            LOG.info(logPrefijo + "Formato Telefono no Valido, " + telefono);
        }
        if (!validarCadena(session, Propiedades.PATTERN_SESSION)) {
            stringBuilder.append(" -Formato Session no Valido ").append(Propiedades.PATTERN_SESSION);
            LOG.info(logPrefijo + "Formato Session no Valido, " + session);
        }
        if (!validarCadena(valor, Propiedades.PATTERN_VALOR)) {
            stringBuilder.append(" -Formato Valor no Valido").append(Propiedades.PATTERN_VALOR);
            LOG.info(logPrefijo + "Formato Valor no Valido, " + valor);
        }

        return stringBuilder.toString();
    }

    public static boolean validarCadena(String cadena, String patter) {
        boolean valor = false;
        if (cadena != null && patter != null) {
            Pattern pat = Pattern.compile(patter);
            Matcher mat = pat.matcher(cadena);
            valor = mat.matches();
        }
        return valor;
    }

    public static String devolverRespuestaFallida(String isdn, String session, String requisitos, String logPrefijo) {
        String json = "";
        try {
            Gson gson = new Gson();
            ResponseUpdate respuesta = new ResponseUpdate();
            respuesta.setCode(300);
            respuesta.setIsdn(isdn != null ? isdn : "UNKNOWN");
            respuesta.setMessage(requisitos);
            respuesta.setSession(session != null ? session : "UNKNOWN");
            json = gson.toJson(respuesta);
        } catch (Exception e) {
            LOG.info(logPrefijo + "Exception:" + e.getMessage(), e);
        }
        return json;
    }
    /*public static void main(String[] args) {
        long tiempo = System.currentTimeMillis();
        System.out.println(probarConexion("172.28.10.96", 61616));
        System.out.println("tardo " + (System.currentTimeMillis() - tiempo) + " ms");
    }*/
}
