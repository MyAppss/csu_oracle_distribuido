/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.util;

/**
 *
 * @author hp
 */
public class Contantes {

    public static final int CODE_UPDATE_SATIFACTORIA = 100;
    public static final String MSG_UPDATE_SATISFACTORIA = "Satisfactory update";

    public static final int CODE_ERROR_VALIDACION = 200;
    public static final String MSG_ERROR_VALIDACION = "The field % is required";

    public static final int CODE_ERROR_CONEX_BD = 300;
    public static final String MSG_ERROR_CONEX_BD = "Error trying to connect to BD";

    public static final int CODE_ERROR_NOT_FOUND = 400;
    public static final String MSG_ERROR_NOT_FOUND = "SessionId or isdn it does not exist in db";

    public static final int CODE_ERROR_NO_SATISFACTORIO = 500;
    public static final String MSG_ERROR_NO_SATISFACTORIO = "Not Satisfactory";

    public static final int CODE_ERROR_CONEX_QUEUES_JMS = 600;
    public static final String MSG_ERROR_CONEX_QUEUES_JMS = "Error trying to connect to QUEUES JMS";
    
    
  
}
