/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.csu.bl;

import java.sql.Timestamp;
import java.util.HashMap;
import myapps.activeMQ.ColaJMS;
import myapps.activeMQ.ColaThreadMax;
import myapps.activeMQ.EnviarMensajeJMS;
import myapps.cmn.vo.ReporteConsulta;
import myapps.cmn.vo.ResponseUpdate;
import myapps.csu.dao.ReporteConsultaDAO;
import myapps.csu.util.Contantes;
import myapps.csu.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author hp
 */
public class ActualizarBL {

    public static final Logger LOG = Logger.getLogger(ActualizarBL.class);

    public String validar(String session, String isdn, String valor) {
        String resp = "";
        if (session == null || "".equals(session)) {
            resp = "sessionId";
            LOG.error("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "] Campo session esta en blanco o nulo");
        } else if (isdn == null || "".equals(isdn)) {
            resp = "isdn";
            LOG.error("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "] Campo isdn esta en blanco o nulo");
        } else if (valor == null || "".equals(valor)) {
            resp = "valor";
            LOG.debug("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "] Campo valor esta en blanco o nulo");
        }
        return resp;
    }

    public ResponseUpdate actualizar(String session, String isdn, String valor) {
        ReporteConsulta reporte = new ReporteConsulta();
        reporte.setSessionId(session);
        reporte.setMsisdn(isdn);
        reporte.setOpcion(valor);

        String resultado = ReporteConsultaDAO.updateReporteConsulta(reporte);
        ResponseUpdate response = new ResponseUpdate();
        switch (resultado) {
            case "":
                response.setSession(session);
                response.setIsdn(isdn);
                response.setCode(Contantes.CODE_UPDATE_SATIFACTORIA);
                response.setMessage(Contantes.MSG_UPDATE_SATISFACTORIA);
                LOG.info("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "] Actualizacion satisfactoria");
                break;
            case "sessionId o isdn no existe en la db":
                response.setSession(session);
                response.setIsdn(isdn);
                response.setCode(Contantes.CODE_ERROR_NOT_FOUND);
                response.setMessage(Contantes.MSG_ERROR_NOT_FOUND);
                LOG.info("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]  error: " + resultado);
                break;
            default:
                response.setSession(session);
                response.setIsdn(isdn);
                response.setCode(Contantes.CODE_ERROR_NO_SATISFACTORIO);
                response.setMessage(Contantes.MSG_ERROR_NO_SATISFACTORIO);
                LOG.info("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]  error: " + resultado);
                break;
        }
        return response;
    }

    public ResponseUpdate encolar(String session, String isdn, String valor) {
        ReporteConsulta reporte = new ReporteConsulta();
        reporte.setSessionId(session);
        reporte.setMsisdn(isdn);
        reporte.setFecha(new Timestamp(System.currentTimeMillis()));
        reporte.setOpcion(valor);

        ResponseUpdate response = new ResponseUpdate();

        //  if (UtilJava.probarConexion(Propiedades.MQ_IP, Propiedades.MQ_PUERTO)) {
        HashMap<String, Object> mapa = new HashMap<>();
        mapa.put("DETALLE_CONSULTA", reporte);
        String prefijo = "[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]";

        if (ColaThreadMax.size() > 0) {
            LOG.info("Iniciando Hilo");
            EnviarMensajeJMS jms = new EnviarMensajeJMS(mapa, prefijo);
            jms.start();
        } else {
            LOG.warn("Mandando a la Cola");
            ColaJMS.put(mapa);
        }

        // Cola.put(mapa);
        LOG.info("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]  EVIANDO A LA COLA " + Propiedades.MQ_COLA);
        response.setSession(session);
        response.setIsdn(isdn);
        response.setCode(Contantes.CODE_UPDATE_SATIFACTORIA);
        response.setMessage(Contantes.MSG_UPDATE_SATISFACTORIA);
        LOG.info("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]  DETALLE CONSULTA ENVIADO A LA COLA");
        /*} else {
            response.setSession(session);
            response.setIsdn(isdn);
            response.setCode(Contantes.CODE_ERROR_CONEX_QUEUES_JMS);
            response.setMessage(Contantes.MSG_ERROR_CONEX_QUEUES_JMS);
            LOG.warn("[SessionId: " + session + "][isdn: " + isdn + "][valor: " + valor + "]  error: EL SERVICIO QUEUES NO ESTA DISPONIBLE");
        }*/
        return response;
    }

}
