/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.activeMQ;

import java.util.HashMap;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import myapps.context.InitContextListener;
import myapps.csu.util.Propiedades;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ThreadJms extends Thread {

    public static final Logger log = Logger.getLogger(ThreadJms.class);
    private final long SLEEP = Propiedades.MQ_HILOS_SLEEP;
    private boolean live;
    private int idThread;
    private String uuidThread;
    private InitContextListener contextSession;

    public ThreadJms(int idThread, String uuidThread, InitContextListener contextSession) {
        live = true;
        this.idThread = idThread;
        this.uuidThread = uuidThread;
        this.contextSession = contextSession;

    }

    @Override
    public void run() {
        super.run();
        log.info("[idThread: " + idThread + "][uuidThread: " + uuidThread + "] Iniciando ThreadJms");

        try {
            while (live) {
                HashMap<String, Object> consultaServicio;
                if (Boolean.TRUE.equals(contextSession.getIniciado())) {
                    consultaServicio = ColaJMS.poll();
                    if (consultaServicio == null) {
                        try {
                            Thread.sleep(SLEEP);
                        } catch (InterruptedException e) {
                            log.error("[idThread: " + idThread + "][uuidThread: " + uuidThread + "] InterruptedException: ", e);
                            Thread.currentThread().interrupt();
                        }
                    } else {
                        sendMessages(consultaServicio);
                        log.debug("[idThread: " + idThread + "][uuidThread: " + uuidThread + "] Correcto");
                    }
                } else {
                    live = false;
                }
            }
        } catch (Exception e) {
            log.error("[idThread: " + idThread + "][uuidThread: " + uuidThread + "] [Exception]" + e.getMessage(), e);
        }

        log.info("[idThread: " + idThread + "][uuidThread: " + uuidThread + "] ThreadJms finalizado!");
    }

    public synchronized boolean sendMessages(HashMap<String, Object> mensaje) {
        long tiempo = System.currentTimeMillis();
        boolean valor = false;
        try {
            Connection connection = ConexionJMS.getInstance().getConnection();
            if (connection != null) {
                Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
                Destination destination = session.createQueue(Propiedades.MQ_COLA);
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.PERSISTENT);
                sendMessage(mensaje, session, producer);
                session.commit();
                session.close();

                log.info(" Mensaje enviado correctamentea la Cola");
                valor = true;
            } else {
                log.warn("La conexion es null, se procedera a reencolar la consulta");
                ColaJMS.put(mensaje);
                ConexionJMS.getInstance().reConexion(connection);
            }
        } catch (JMSException e) {
            log.error("[sendMessages]  Error al enviar al Mensaje a la Cola " + e.getMessage(), e);
        }

        log.info("[sendMessages] sendMessages se ejecuto en " + (System.currentTimeMillis() - tiempo) + " ms");
        return valor;
    }

    private synchronized void sendMessage(HashMap<String, Object> message, Session session, MessageProducer producer) throws JMSException {
        ObjectMessage mensaje = session.createObjectMessage(message);
        producer.send(mensaje);
    }
}
