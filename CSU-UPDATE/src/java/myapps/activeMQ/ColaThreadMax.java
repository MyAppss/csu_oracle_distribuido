/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myapps.activeMQ;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.log4j.Logger;

/**
 *
 * @author GENOSAURER
 */
public class ColaThreadMax implements Serializable{
 public static final Logger LOG = Logger.getLogger(ColaThreadMax.class);

    private static final long serialVersionUID = 1L;
    private static final Queue<Integer> LIST_POOL;
    private static int cantidadThread;

    static {
        LIST_POOL = new LinkedList<>();
        cantidadThread = 0;
    }


    public static synchronized boolean put(Integer cantidadThread) {
        return LIST_POOL.offer(cantidadThread);
    }

    public static synchronized boolean contains(Integer cantidadThread) {
        return LIST_POOL.contains(cantidadThread);
    }

    public static synchronized Integer poll() {
        if (!LIST_POOL.isEmpty()) {
            return LIST_POOL.poll();
        }
        return null;
    }


    public static synchronized boolean removed(Integer cantidadThread) {
        return LIST_POOL.remove(cantidadThread);
    }

    public static synchronized long size() {
        return LIST_POOL.size();
    }

    
}

